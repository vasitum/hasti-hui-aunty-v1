import React, { Component, lazy, Suspense } from "react";
import Loadable from "react-loadable";
import { Route, Switch } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Loading from "./containers/MainLoader";
import Footer from "./components/Footer";
import Navbar from "./components/Navbar";
// import ChatbotComponent from "./components/ChatBoatComponent";
import UserFeedbackModal from "./components/UserFeedBackModal";
import { HOME } from "./constants/paths";
import userExpApiGet from "./api/user/userExpFeedbackGet";
import isLoggedIn from "./utils/env/isLoggedin";
import { baseUrl, USER } from "./constants/api";
import NotificationService from "./services/NotificationService";
import MessagingService from "./services/MessagingService";
import { Helmet } from "react-helmet";
import { withUserContextProvider } from "./contexts/UserContext";
import {
  CBContextProvider,
  withChatbotContext
} from "./contexts/ChatbotContext";

import { AlgoliaContextProvider } from "./contexts/AlgoliaContext";

import MeonChatbot from "./containers/Chatbot";
import CBCircle from "./components/ChatbotV2/cbcircle";
import cx from "classnames";
import ReactGA from "react-ga";

const HomePageContainer = lazy(() => import("./components/HomePage"));

// const HomePageContainer = Loadable({
//   loader: () => import("./components/HomePage"),
//   loading() {
//     return <Loading />;
//   }
// });

const JobPageContainer = lazy(() =>
  import("./containers/pages/JobPageContainer")
);

// const JobPageContainer = Loadable({
//   loader: () => import("./containers/pages/JobPageContainer"),
//   loading() {
//     return <Loading />;
//   }
// });

const SearchPageContainer = lazy(() =>
  import("./containers/pages/SearchPageContainer")
);

// const SearchPageContainer = Loadable({
//   loader: () => import("./containers/pages/SearchPageContainer"),
//   loading() {
//     return <Loading />;
//   }
// });

const UserPageContainer = lazy(() =>
  import("./containers/pages/UserPageContainer")
);

// const UserPageContainer = Loadable({
//   loader: () => import("./containers/pages/UserPageContainer"),
//   loading() {
//     return <Loading />;
//   }
// });

const ManageCompaniesPage = lazy(() =>
  import("./containers/pages/CompanyPageContainer")
);
// const ManageCompaniesPage = Loadable({
//   loader: () => import("./containers/pages/CompanyPageContainer"),
//   loading() {
//     return <Loading />;
//   }
// });

const MessagePageContainer = lazy(() =>
  import("./containers/pages/MessagePageContainer")
);

// const MessagePageContainer = Loadable({
//   loader: () => import("./containers/pages/MessagePageContainer"),
//   loading() {
//     return <Loading />;
//   }
// });

const PublicPageContainer = lazy(() =>
  import("./containers/pages/PublicViewContainer")
);

// const PublicPageContainer = Loadable({
//   loader: () => import("./containers/pages/PublicViewContainer"),
//   loading() {
//     return <Loading />;
//   }
// });

const CreatePageContainer = lazy(() =>
  import("./containers/pages/CreatePageContainer")
);

// const CreatePageContainer = Loadable({
//   loader: () => import("./containers/pages/CreatePageContainer"),
//   loading() {
//     return <Loading />;
//   }
// });

const NotificationViewContainer = lazy(() =>
  import("./components/NotificationView")
);

// const NotificationViewContainer = Loadable({
//   loader: () => import("./components/NotificationView"),
//   loading() {
//     return <Loading />;
//   }
// });

const EmailVerificationContainer = lazy(() =>
  import("./components/EmailVerification")
);

// const EmailVerificationContainer = Loadable({
//   loader: () => import("./components/EmailVerification"),
//   loading() {
//     return <Loading />;
//   }
// });

const PrivacyPolicyContainer = lazy(() =>
  import("./components/StaticPage/PrivacyPolicy")
);

// const PrivacyPolicyContainer = Loadable({
//   loader: () => import("./components/StaticPage/PrivacyPolicy"),
//   loading() {
//     return <Loading />;
//   }
// });

const TermAndConditionContainer = lazy(() =>
  import("./components/StaticPage/TermAndCondition")
);

// const TermAndConditionContainer = Loadable({
//   loader: () => import("./components/StaticPage/TermAndCondition"),
//   loading() {
//     return <Loading />;
//   }
// });

const ContactFeedbackContainer = lazy(() =>
  import("./components/StaticPage/ContactFeedback")
);

// const ContactFeedbackContainer = Loadable({
//   loader: () => import("./components/StaticPage/ContactFeedback"),
//   loading() {
//     return <Loading />;
//   }
// });

// Advance Search
const AdvanceSearchPageContainer = lazy(() =>
  import("./components/AdvanceSearch")
);
// const AdvanceSearchPageContainer = Loadable({
//   loader: () => import("./components/AdvanceSearch"),
//   loading() {
//     return <Loading />;
//   }
// });
// Advance Search end

// CALENDAR
const CalendarContainer = lazy(() => import("./components/Calendar"));
// const CalendarContainer = Loadable({
//   loader: () => import("./components/Calendar"),
//   loading() {
//     return <Loading />;
//   }
// });
// Cv Template
const CvTemplateContainer = lazy(() =>
  import("./containers/pages/CVCreateContainer")
);
// const CvTemplateContainer = Loadable({
//   loader: () => import("./containers/pages/CVCreateContainer"),
//   loading() {
//     return <Loading />;
//   }
// });

// New UI Design Components
// const NewDesignContainer = lazy(() => import("./styleComponent"));

// Resume Basic
// const RBasicContainer = lazy(() => import("./components/CvCreate/Cv1"));

// RCLASSIC
// const RClassicContainer = lazy(() => import("./components/CvCreate/Cv2"));

// RMODERN
// const RModernContainer = lazy(() => import("./components/CvCreate/Cv3"));

const AboutUsContainer = lazy(() => import("./components/StaticPage/AboutUs"));
// const AboutUsContainer = Loadable({
//   loader: () => import("./components/StaticPage/AboutUs"),
//   loading() {
//     return <Loading />;
//   }
// });
// const Loading = props => {
//   return (
//     <div
//       style={{
//         fontSize: "50px"
//       }}>
//       Loading ...
//     </div>
//   );
// };

const Application = withChatbotContext(({ cb, actions }) => {
  const leftClasses = cx({
    left_container: cb.visibility === "MINIMIZE", // MINIMIZE
    left_containerChange: cb.visibility === "EXPAND", // EXPAND
    left_containerFullWidth: cb.visibility === "MAXIMIZE" // MAXIMIZIE
  });

  const rightClasses = cx({
    right_container: cb.visibility === "MINIMIZE",
    right_containerChange: cb.visibility === "EXPAND",
    right_containerFullWidth: cb.visibility === "MAXIMIZE"
  });

  return (
    <div className="mainPage_container">
      <CBCircle />
      {/* <div className={leftClasses}>
      </div> */}
      <MeonChatbot />
      <div className={"right_container"}>
        <Navbar isUserLoggedIn={isLoggedIn()} />

        <Switch>
          {/* <Route path={HOME.MESSAGING} component={MessagePageContainer} /> */}
          <Route
            path={HOME.MESSAGING}
            render={props => (
              <Suspense fallback={<Loading />}>
                <MessagePageContainer {...props} />
              </Suspense>
            )}
          />
          {/* <Route path={HOME.JOB} component={JobPageContainer} /> */}
          <Route
            path={HOME.JOB}
            render={props => (
              <Suspense fallback={<Loading />}>
                <JobPageContainer {...props} />
              </Suspense>
            )}
          />
          {/* <Route path={HOME.USER} component={UserPageContainer} /> */}
          <Route
            path={HOME.USER}
            render={props => (
              <Suspense fallback={<Loading />}>
                <UserPageContainer {...props} />
              </Suspense>
            )}
          />
          {/* <Route path={HOME.SEARCH} component={SearchPageContainer} /> */}
          <Route
            path={HOME.SEARCH}
            render={props => (
              <Suspense fallback={<Loading />}>
                <SearchPageContainer {...props} />
              </Suspense>
            )}
          />
          {/* <Route path={HOME.COMPANY} component={ManageCompaniesPage} /> */}
          <Route
            path={HOME.COMPANY}
            render={props => (
              <Suspense fallback={<Loading />}>
                <ManageCompaniesPage {...props} />
              </Suspense>
            )}
          />

          {/* EMAIL_VERIFICATION */}
          {/* <Route
            path={HOME.EMAIL_VERIFICATION}
            component={EmailVerificationContainer}
          /> */}
          <Route
            path={HOME.EMAIL_VERIFICATION}
            render={props => (
              <Suspense fallback={<Loading />}>
                <EmailVerificationContainer {...props} />
              </Suspense>
            )}
          />

          <Route
            path={HOME.EMAIL_VERIFICATION_ID}
            render={props => (
              <Suspense fallback={<Loading />}>
                <EmailVerificationContainer {...props} />
              </Suspense>
            )}
          />
          {/* <Route
            path={HOME.NOTIFICATION}
            component={NotificationViewContainer}
          /> */}
          <Route
            path={HOME.NOTIFICATION}
            render={props => (
              <Suspense fallback={<Loading />}>
                <NotificationViewContainer {...props} />
              </Suspense>
            )}
          />
          {/* <Route path={HOME.VIEW} component={PublicPageContainer} /> */}
          <Route
            path={HOME.VIEW}
            render={props => (
              <Suspense fallback={<Loading />}>
                <PublicPageContainer {...props} />
              </Suspense>
            )}
          />
          {/* <Route path={HOME.CREATE} component={CreatePageContainer} /> */}
          <Route
            path={HOME.CREATE}
            render={props => (
              <Suspense fallback={<Loading />}>
                <CreatePageContainer {...props} />
              </Suspense>
            )}
          />
          {/* calendar */}
          {/* <Route path={HOME.CALENDAR} component={CalendarContainer} /> */}
          <Route
            path={HOME.CALENDAR}
            render={props => (
              <Suspense fallback={<Loading />}>
                <CalendarContainer {...props} />
              </Suspense>
            )}
          />
          {/* cvtemplate */}
          {/* <Route path={HOME.CVTEMPLATE} component={CvTemplateContainer} /> */}
          <Route
            path={HOME.CVTEMPLATE}
            render={props => (
              <Suspense fallback={<Loading />}>
                <CvTemplateContainer {...props} />
              </Suspense>
            )}
          />

          {/* newdesign */}
          {/* <Route
            path={HOME.NEWDESIGN}
            render={props => (
              <Suspense fallback={<Loading />}>
                <NewDesignContainer {...props} />
              </Suspense>
            )}
          /> */}

          {/* Advance Search */}
          {/* <Route
            path={HOME.ADVANCE_SEARCH}
            component={AdvanceSearchPageContainer}
          /> */}
          <Route
            path={HOME.ADVANCE_SEARCH}
            render={props => (
              <Suspense fallback={<Loading />}>
                <AdvanceSearchPageContainer {...props} />
              </Suspense>
            )}
          />
          {/* Advance Search  end*/}

          {/* StaticPage */}
          {/* <Route path={HOME.ABOUT_US} component={AboutUsContainer} /> */}
          <Route
            path={HOME.ABOUT_US}
            render={props => (
              <Suspense fallback={<Loading />}>
                <AboutUsContainer {...props} />
              </Suspense>
            )}
          />
          {/* <Route
            path={HOME.PRIVACY_POLICY}
            component={PrivacyPolicyContainer}
          /> */}
          <Route
            path={HOME.PRIVACY_POLICY}
            render={props => (
              <Suspense fallback={<Loading />}>
                <PrivacyPolicyContainer {...props} />
              </Suspense>
            )}
          />
          {/* <Route
            path={HOME.TERM_AND_CONDITION}
            component={TermAndConditionContainer}
          /> */}
          <Route
            path={HOME.TERM_AND_CONDITION}
            render={props => (
              <Suspense fallback={<Loading />}>
                <TermAndConditionContainer {...props} />
              </Suspense>
            )}
          />
          {/* <Route
            path={HOME.CONTACT_FEEDBACK}
            component={ContactFeedbackContainer}
          /> */}
          <Route
            path={HOME.CONTACT_FEEDBACK}
            render={props => (
              <Suspense fallback={<Loading />}>
                <ContactFeedbackContainer {...props} />
              </Suspense>
            )}
          />
          {/* <Route exact path={HOME.INDEX} component={HomePageContainer} /> */}
          <Route
            exact
            path={HOME.INDEX}
            render={props => (
              <Suspense fallback={<Loading />}>
                <HomePageContainer {...props} />
              </Suspense>
            )}
            component={HomePageContainer}
          />
        </Switch>
        <Footer isUserLoggedIn={isLoggedIn()} />
      </div>
    </div>
  );
});

// TODO: Async Component Loading
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      leftContainer: false,
      NavbarClass: "mainPage_container",
      modalFeedbackIcon: ""
    };
  }

  leftContainerOpen = () => {
    this.setState({
      leftContainer: true
    });
  };

  async componentDidMount() {
    ReactGA.initialize("UA-132677704-1", { standardImplementation: true });
    NotificationService();
    MessagingService();
    this.handleGetFeedback();
  }

  handleGetFeedback = async () => {
    try {
      const res = await userExpApiGet();
      if (res.status === 200) {
        const data = await res.json();
        this.setState({ modalFeedbackIcon: data[0].feedbackIcon });
      }
    } catch (error) {
      console.error(error);
    }
  };

  // handleUserFeedbackModal = () => {
  //   this.setState({ modalFeedback: false });
  // };

  render() {
    const { modalFeedbackIcon } = this.state;
    return (
      // TODO: Render Home Page Here
      <Suspense fallback={() => <div />}>
        <Helmet>
          <meta charSet="utf-8" />
          {/* <title>
            Job - Resume Format - Job Vacancies - Recruitment - Career - Vasitum
          </title>
          <meta
            name="title"
            content="Job - Resume Format - Job Vacancies - Recruitment - Career - Vasitum"
          />
          <meta
            name="twitter:title"
            content="Job - Resume Format - Job Vacancies - Recruitment - Career - Vasitum"
          />
          <meta
            property="og:title"
            content="Job - Resume Format - Job Vacancies - Recruitment - Career - Vasitum"
          />
          <meta
            property="og:description"
            content={`Vasitum is the most advanced AI-enabled recruitment platform that provides you free job
                      alert, free job posting, resume template, job search a boost and helps you to browse and apply online
                      job.`}
          />
          <meta
            property="description"
            content={`Vasitum is the most advanced AI-enabled recruitment platform that provides you free job
                      alert, free job posting, resume template, job search a boost and helps you to browse and apply online
                      job.`}
          />
          <meta
            property="twitter:description"
            content={`Vasitum is the most advanced AI-enabled recruitment platform that provides you free job
                      alert, free job posting, resume template, job search a boost and helps you to browse and apply online
                      job.`}
          /> */}
          <link rel="canonical" href="https://vasitum.com" />
        </Helmet>
        <CBContextProvider>
          <AlgoliaContextProvider>
            <Application />
            <ToastContainer autoClose={15000} />
          </AlgoliaContextProvider>
        </CBContextProvider>
        {window.localStorage.getItem(USER.UID) ? (
          <UserFeedbackModal
            // open={modalFeedback}
            // handleModal={this.handleUserFeedbackModal}
            handleGetFeedback={this.handleGetFeedback}
            icon={modalFeedbackIcon}
          />
        ) : null}
      </Suspense>
    );
  }
}

export default withUserContextProvider(App);
