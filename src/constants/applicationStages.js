export const RECR_STAGES = {
  Yettoscreen: "YET TO SCREEN",
  ShortList: "SHORTLIST",
  Interview: "INTERVIEW",
  Offered: "OFFERED",
  Joinend: "JOINED",
  Hold: "HOLD",
  Rejected: "REJECTED"
};

export const CAND_STAGES = {
  Yettoscreen: "PENDING SCREENING",
  ShortList: "SHORTLIST",
  Interview: "INTERVIEW",
  Offered: "OFFERED",
  Joinend: "EXPIRED/CLOSED",
  Hold: "EXPIRED/CLOSED",
  Rejected: "APPLICATION REJECT"
};

export const SCREENING_STATUS = {
  "Screening Pending": "Screening Pending",
  "Screening Done": "Screening Done",
  "Interview pending": "Interview pending",
  "Interview done": "Interview done"
};
