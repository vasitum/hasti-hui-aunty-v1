export const CUSTOM_TYPES = {
  STRING: "string",
  NUMBER: {
    LESS_THAN: "gt",
    GREATER_THAN: "lt",
    EQUALS: "eq"
  }
};
