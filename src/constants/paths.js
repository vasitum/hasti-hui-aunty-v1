export const HOME = {
  INDEX: "/",
  JOB: "/job",
  JOBOLD: "/jobold", // TODO: Remove After Developement of my jobs page
  USER: "/user",
  SEARCH: "/search",
  COMPANY: "/company",
  MESSAGING: "/messaging",
  NOTIFICATION: "/notification",
  VIEW: "/view",
  CALENDAR: "/calendar",
  CVTEMPLATE: "/cv-template",
  NEWDESIGN: "/new-design",
  RCLASSIC: "/resume-classic",
  RMODERN: "/resume-modern",
  RBASIC: "/resume-professional",
  CREATE: "/create",
  EMAIL_VERIFICATION: "/emailVerification",
  EMAIL_VERIFICATION_ID: "/emailVerification/:id",
  ADVANCE_SEARCH: "/advance-search",
  ABOUT_US: "/about-us",
  PRIVACY_POLICY: "/privacy-policy",
  TERM_AND_CONDITION: "/term-and-condition",
  CONTACT_FEEDBACK: "/contact-feedback"
};

export const JOB = {
  HOME_PAGE: "/homePage",
  JOB_DRAFTS: "/drafts",
  POST_JOB: "/new",
  REPOST_JOB: "/new/:id",
  PUBLIC_VIEW: "/public/:id",
  VIEW: "/view/:id",
  PREVIEW: "/preview",
  DASHBOARD: "/recruiter/:id",
  CANDIDATE_DASHBOARD: "/candidate/:id",
  CANDIDATE_DETAIL_PAGE: "/candidateDetail",
  MYJOBS_DETAIL_PAGE: "/myJobsDetail",
  MY_CANDIDATE_DETAIL: "/:jobId/application/:applicationId",
  MYJOBS_DETAIL_CONDIDATE_PAGE: "/candidate/:jobId/application/:applicationId",
  INTERVIEW: "/interview",
  INTERVIEW_QUESTIONS: "/interviewQuestions",
  SCREENING_QUESTIONS: "/edit/screening/:id",
  SCREENING_QUESTIONS_EDIT: "/edit/screening/:id/edit",
  SCREENING_INTERVIEW_SELECTION: "/edit/interview/:id",
  POST_JOB_EDIT: "/edit/:id",

  // Manage Template path
  MANAGE_TEMPLATE: "/managetemplate"
  // MYJOBS_DETAIL_CONDIDATE_PAGE: "/myJobsDetailCondidate"
};

export const VIEW = {
  JOB: "/job/:id",
  JOB_WITH_NAME: "/job/:jobname/:id",
  PROFILE: "/user/:id",
  PREVIEW: "/preview/user"
};

export const CREATE = {
  PROFILE: "/profile"
};

export const SEARCH = {
  JOB: "/job",
  PEOPLE: "/people"
};

export const USER = {
  CREATE_PROFILE: "/new",
  PUBLIC_VIEW: "/public/:id",
  VIEW: "/view/:id",
  PROFILE_PREVIEW: "/preview"
};

export const CV_TEMPLATE = {
  CVTEMPLATE: "/cv-template",
  RCLASSIC: "/cv-template/resume-classic",
  RMODERN: "/cv-template/resume-modern",
  RBASIC: "/cv-template/resume-professional"
};
