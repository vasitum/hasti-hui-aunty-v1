// export const baseUrl = "http://13.232.153.244:8080/v2/";
export const baseUrl = "https://api.vasitum.com:8443/v2/";

export const USER = {
  X_AUTH_ID: "__xaht__id",
  UID: "__xua_id",
  EMAIL: "__xem_li",
  NAME: "__xem_nem",
  LOC: "__xem_olc",
  CTC: "__xem_tct",
  EXP: "__xlem_exp",
  IMG_EXT: "__xExt_xem",
  PHONE: "___xPonh",
  TITLE: "___xemxjkti",
  BASE_JSON: "__xrm_jsbn",
  HAS_PROFILE: "__x_mpfiel",
  LAST_NOTIFICATION: "___mNot_ID",
  TOKEN: "___mDot__Token",
  VERIFIED: "__x_m_verf"
};
