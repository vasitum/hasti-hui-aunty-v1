import React from "react";
import PropTypes from "prop-types";

const IcRecommendedApplicants = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter
          id="filter"
          x="62.188"
          y="508"
          width="20.063"
          height="18.969"
          filterUnits="userSpaceOnUse">
          <feFlood result="flood" floodColor="#acaeb5" />
          <feComposite result="composite" operator="in" in2="SourceGraphic" />
          <feBlend result="blend" in2="SourceGraphic" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          className="cls-1"
          d="M81.356,514.445l-5.516-.521a0.952,0.952,0,0,1-.79-0.584l-1.976-4.783a0.946,0.946,0,0,0-1.754,0l-1.96,4.783a0.932,0.932,0,0,1-.79.584l-5.516.521a0.952,0.952,0,0,0-.537,1.657l4.157,3.647a0.942,0.942,0,0,1,.3.931l-1.249,5.114a0.953,0.953,0,0,0,1.407,1.042l4.6-2.7a0.956,0.956,0,0,1,.964,0l4.6,2.7a0.95,0.95,0,0,0,1.407-1.042l-1.233-5.114a0.942,0.942,0,0,1,.3-0.931l4.157-3.647A0.964,0.964,0,0,0,81.356,514.445Z"
          transform="translate(-62.188 -508)"
        />
      </g>
    </svg>
  );
};

IcRecommendedApplicants.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string,
  rectcolor: PropTypes.string
};

IcRecommendedApplicants.defaultProps = {
  width: "20.063",
  height: "18.969",
  viewbox: "0 0 20.063 18.969"
};

export default IcRecommendedApplicants;
