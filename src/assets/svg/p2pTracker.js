import React from "react";
import PropTypes from "prop-types";

const p2pTracker = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      preserveAspectRatio="xMidYMid"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter id="color-overlay-1" filterUnits="userSpaceOnUse">
          <feFlood floodColor="#acaeb5" />
          <feComposite operator="in" in2="SourceGraphic" />
          <feBlend in2="SourceGraphic" result="solidFill" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          d="M4.828,4.489 C6.280,2.849 8.507,1.800 11.004,1.800 C14.696,1.800 17.789,4.096 18.671,7.200 L20.713,7.200 C19.795,3.092 15.797,-0.000 11.004,-0.000 C8.043,-0.000 5.387,1.181 3.572,3.052 C2.087,4.500 4.068,5.400 4.828,4.489 ZM18.509,14.872 C19.425,13.950 17.939,12.600 17.203,13.485 C15.751,15.140 13.514,16.200 11.004,16.200 C7.312,16.200 4.219,13.903 3.338,10.800 L1.296,10.800 C2.214,14.908 6.211,18.000 11.004,18.000 C14.005,18.000 16.692,16.787 18.509,14.872 ZM16.959,7.995 L18.890,10.925 C19.172,11.351 19.629,11.352 19.912,10.927 L21.882,7.970 C22.165,7.545 21.951,7.202 21.404,7.204 L17.441,7.220 C16.894,7.221 16.678,7.568 16.959,7.995 ZM5.039,10.030 L3.070,7.073 C2.786,6.648 2.329,6.649 2.047,7.075 L0.117,10.004 C-0.164,10.431 0.051,10.779 0.598,10.781 L4.561,10.796 C5.109,10.798 5.322,10.454 5.039,10.030 Z"
          className="cls-1"
        />
      </g>
    </svg>
  );
};

p2pTracker.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

p2pTracker.defaultProps = {
  width: "22",
  height: "18",
  viewbox: "0 0 22 18"
};

export default p2pTracker;
