import React from "react";
import PropTypes from "prop-types";

const IcUserIcon = props => {
  return (
    <svg
      id="people"
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}
      style={props.style}>
      <defs />
      <rect
        fill={props.rectcolor}
        id="Rectangle_5_copy"
        data-name="Rectangle 5 copy"
        className="cls-1"
        width="64"
        height="64"
      />
      <g fill={props.pathcolor}>
        <path
          className="cls-2"
          d="M602,1168c0-6.04,4.925-9.9,11-9.9s11,3.86,11,9.9H602Zm11-12.92a6.04,6.04,0,1,1,6.069-6.04A6.058,6.058,0,0,1,613,1155.08Z"
          transform="translate(-581 -1123)"
        />
      </g>
    </svg>
  );
};

IcUserIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string,
  rectcolor: PropTypes.string
};

IcUserIcon.defaultProps = {
  width: "64",
  height: "64",
  viewbox: "0 0 64 64"
};

export default IcUserIcon;
