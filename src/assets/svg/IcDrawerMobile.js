
import React from "react";
import PropTypes from 'prop-types';

const IcDrawerMobile = props => {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width={props.width} height={props.height} 
    viewBox={props.viewbox}>
      <defs>
        <filter id='filter' x='1840.72' y='1338.03' width='15.97' height='15.97'
          filterUnits='userSpaceOnUse'>
          <feFlood result='flood' floodColor='#c9c9c9' />
          <feComposite result='composite' operator='in' in2='SourceGraphic' />
          <feBlend result='blend' in2='SourceGraphic' />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path className='cls-1' d='M1856.32,1350.66l-2.46-2.48a1.28,1.28,0,0,0-1.81.04l-1.24,1.25c-0.08-.05-0.16-0.09-0.24-0.14a11.725,11.725,0,0,1-5.15-5.18c-0.04-.08-0.09-0.16-0.13-0.24l0.83-.84,0.41-.41a1.292,1.292,0,0,0,.03-1.82l-2.46-2.48a1.28,1.28,0,0,0-1.81.04l-0.69.7,0.02,0.02a4,4,0,0,0-.57,1.01,4.1,4.1,0,0,0-.26,1.03c-0.32,2.71.91,5.19,4.25,8.55,4.62,4.65,8.34,4.3,8.5,4.28a3.8,3.8,0,0,0,1.03-.26,3.514,3.514,0,0,0,1-.57l0.02,0.02,0.7-.7A1.292,1.292,0,0,0,1856.32,1350.66Z'
          transform='translate(-1840.72 -1338.03)' />
      </g>
    </svg>
  );
};

IcDrawerMobile.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string,
}

IcDrawerMobile.defaultProps = {
  width: '15.97',
  height: '15.97',
  viewbox: '0 0 15.97 15.97'
}

export default IcDrawerMobile;