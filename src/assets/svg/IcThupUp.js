import React from "react";
import PropTypes from 'prop-types';

const IcUploadIcon = props => {
  return (

    <svg xmlns='http://www.w3.org/2000/svg' preserveAspectRatio='xMidYMid'
      width={props.width} height={props.height}
      viewBox={props.viewbox}>
      <defs />
      <g fill={props.pathcolor}>
        <path d='M18.162,9.134 C18.642,9.973 18.362,11.066 17.542,11.585 C18.022,12.424 17.741,13.517 16.921,14.035 C17.609,15.237 16.695,16.776 15.300,16.775 L4.962,16.775 L4.962,6.973 C4.962,6.973 7.954,4.297 8.917,3.369 C9.469,2.837 9.826,2.128 9.969,1.383 C10.025,1.089 10.009,0.779 10.167,0.514 C10.305,0.284 10.537,0.112 10.800,0.046 C11.258,-0.068 11.827,0.040 12.235,0.255 C13.485,0.915 13.777,2.772 13.327,3.987 L12.673,5.748 C12.673,5.748 17.351,5.731 17.450,5.748 C19.104,6.024 19.577,8.239 18.162,9.134 ZM3.101,18.000 L0.620,18.000 C0.278,18.000 -0.000,17.726 -0.000,17.387 L-0.000,6.360 C-0.000,6.022 0.279,5.748 0.620,5.748 L3.101,5.748 C3.442,5.748 3.722,6.022 3.722,6.360 L3.722,17.387 C3.722,17.725 3.444,18.000 3.101,18.000 Z'
          className='cls-1' />
      </g>
    </svg>
  );
};

IcUploadIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
}

IcUploadIcon.defaultProps = {
  width: '18.97',
  height: '18',
  viewbox: '0 0 18.97 18',
}

export default IcUploadIcon;