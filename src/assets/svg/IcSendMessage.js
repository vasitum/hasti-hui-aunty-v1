import React from "react";
import PropTypes from "prop-types";

const IcSendMessage = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      preserveAspectRatio="xMidYMid"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter id="color-overlay-1" filterUnits="userSpaceOnUse">
          <feFlood floodColor="#acaeb5" />
          <feComposite operator="in" in2="SourceGraphic" />
          <feBlend in2="SourceGraphic" result="solidFill" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          d="M-0.000,13.000 L16.000,6.500 L-0.000,-0.000 L-0.000,5.055 L11.428,6.500 L-0.000,7.944 L-0.000,13.000 Z"
          className="cls-1"
        />
      </g>
    </svg>
  );
};

IcSendMessage.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

IcSendMessage.defaultProps = {
  width: "16",
  height: "13",
  viewbox: "0 0 16 13"
};

export default IcSendMessage;
