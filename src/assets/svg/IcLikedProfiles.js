import React from "react";
import PropTypes from "prop-types";

const IcLikedProfiles = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter
          id="filter"
          x="61"
          y="407"
          width="20.031"
          height="19"
          filterUnits="userSpaceOnUse">
          <feFlood result="flood" floodColor="#acaeb5" />
          <feComposite result="composite" operator="in" in2="SourceGraphic" />
          <feBlend result="blend" in2="SourceGraphic" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          className="cls-1"
          d="M80.149,416.642a48026.249,48026.249,0,0,0-1.308,5.173,1.958,1.958,0,0,1-1.709,2.892h-10.9V414.36s3.154-2.824,4.17-3.8a3.941,3.941,0,0,0,1.109-2.1,2.659,2.659,0,0,1,.209-0.917,1.091,1.091,0,0,1,.666-0.494,2.291,2.291,0,0,1,1.513.22c1.319,0.7,1.627,2.657,1.151,3.94l-0.689,1.858s4.932-.018,5.037,0A1.966,1.966,0,0,1,80.149,416.642ZM64.27,426H61.654A0.651,0.651,0,0,1,61,425.353V413.714a0.652,0.652,0,0,1,.654-0.647H64.27a0.652,0.652,0,0,1,.654.647v11.639A0.651,0.651,0,0,1,64.27,426Z"
          transform="translate(-61 -407)"
        />
      </g>
    </svg>
  );
};

IcLikedProfiles.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string,
  rectcolor: PropTypes.string
};

IcLikedProfiles.defaultProps = {
  width: "20.031",
  height: "19",
  viewbox: "0 0 20.031 19"
};

export default IcLikedProfiles;
