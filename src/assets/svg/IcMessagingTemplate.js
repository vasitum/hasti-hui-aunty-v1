import React from "react";
import PropTypes from "prop-types";

const IcMessagingTemplate = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      preserveAspectRatio="xMidYMid"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}
      onClick={props.onClick}>
      <defs>
        <filter id="color-overlay-1" filterUnits="userSpaceOnUse">
          <feFlood floodColor="#acaeb5" />
          <feComposite operator="in" in2="SourceGraphic" />
          <feBlend in2="SourceGraphic" result="solidFill" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          d="M15.500,19.000 L0.499,19.000 C0.224,19.000 -0.001,18.775 -0.001,18.499 L-0.001,0.501 C-0.001,0.224 0.224,-0.000 0.499,-0.000 L15.500,-0.000 C15.775,-0.000 15.999,0.224 15.999,0.501 L15.999,18.499 C15.999,18.775 15.775,19.000 15.500,19.000 ZM14.999,4.837 L0.999,4.837 L0.999,17.998 L14.999,17.998 L14.999,4.837 ZM2.999,6.594 L12.999,6.594 C13.137,6.594 13.250,6.707 13.250,6.845 L13.250,8.548 C13.250,8.686 13.137,8.798 12.999,8.798 L2.999,8.798 C2.861,8.798 2.750,8.686 2.750,8.548 L2.750,6.845 C2.750,6.707 2.861,6.594 2.999,6.594 ZM2.999,10.903 L7.549,10.903 C7.687,10.903 7.799,11.014 7.799,11.153 L7.799,15.912 C7.799,16.050 7.687,16.162 7.549,16.162 L2.999,16.162 C2.861,16.162 2.750,16.050 2.750,15.912 L2.750,11.153 C2.750,11.014 2.861,10.903 2.999,10.903 ZM9.499,10.903 L12.999,10.903 C13.137,10.903 13.250,11.014 13.250,11.153 L13.250,12.606 C13.250,12.744 13.137,12.856 12.999,12.856 L9.499,12.856 C9.361,12.856 9.249,12.744 9.249,12.606 L9.249,11.153 C9.249,11.014 9.361,10.903 9.499,10.903 ZM9.499,14.143 L12.999,14.143 C13.137,14.143 13.250,14.255 13.250,14.393 L13.250,15.846 C13.250,15.984 13.137,16.096 12.999,16.096 L9.499,16.096 C9.361,16.096 9.249,15.984 9.249,15.846 L9.249,14.393 C9.249,14.255 9.361,14.143 9.499,14.143 Z"
          className="cls-1"
        />
      </g>
    </svg>
  );
};

IcMessagingTemplate.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

IcMessagingTemplate.defaultProps = {
  width: "16",
  height: "19",
  viewbox: "0 0 16 19"
};

export default IcMessagingTemplate;
