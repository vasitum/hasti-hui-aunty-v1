import React from "react";
import PropTypes from 'prop-types';

const IcProfileViewIcon = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter
          id="filter"
          x="1311"
          y="357"
          width="18"
          height="11"
          filterUnits="userSpaceOnUse">
          <feFlood result="flood" floodColor="#c8c8c8" />
          <feComposite
            result="composite"
            operator="in"
            in2="SourceGraphic"
          />
          <feBlend result="blend" in2="SourceGraphic" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          className="cls-1"
          d="M1320,357c-3.44,0-6.56,1.929-8.86,5.062a0.738,0.738,0,0,0,0,.872c2.3,3.137,5.42,5.066,8.86,5.066s6.56-1.929,8.86-5.062a0.738,0.738,0,0,0,0-.872C1326.56,358.929,1323.44,357,1320,357Zm0.25,9.373a3.882,3.882,0,1,1,3.53-3.62A3.839,3.839,0,0,1,1320.25,366.373Zm-0.12-1.79a2.09,2.09,0,1,1,1.91-1.951A2.084,2.084,0,0,1,1320.13,364.583Z"
          transform="translate(-1311 -357)"
        />
      </g>
    </svg>
  );
};


IcProfileViewIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
}

IcProfileViewIcon.defaultProps = {
  width: '18',
  height: '11',
  viewbox: '0 0 18 11',
}

export default IcProfileViewIcon;