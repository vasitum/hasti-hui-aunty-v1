import React from "react";
import PropTypes from "prop-types";

const IcMyCandidates = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter
          id="filter"
          x="63"
          y="306"
          width="17"
          height="20"
          filterUnits="userSpaceOnUse">
          <feFlood result="flood" floodColor="#acaeb5" />
          <feComposite result="composite" operator="in" in2="SourceGraphic" />
          <feBlend result="blend" in2="SourceGraphic" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          className="cls-1"
          d="M63,326a8.5,8.5,0,0,1,17,0H63Zm8.5-10.725a4.638,4.638,0,1,1,4.69-4.637A4.664,4.664,0,0,1,71.5,315.275Z"
          transform="translate(-63 -306)"
        />
      </g>
    </svg>
  );
};

IcMyCandidates.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string,
  rectcolor: PropTypes.string
};

IcMyCandidates.defaultProps = {
  width: "17",
  height: "20",
  viewbox: "0 0 17 20"
};

export default IcMyCandidates;
