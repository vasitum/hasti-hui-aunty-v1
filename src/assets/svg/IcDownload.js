import React from "react";
import PropTypes from 'prop-types';

const IcDownloadIcon = props => {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width={props.width} height={props.height} 
    viewBox={props.viewbox}>
      <defs>
        <filter id='filter' x='1613.94' y='351' width='17.12' height='18' filterUnits='userSpaceOnUse'>
          <feFlood result='flood' floodColor='#ceeefe' />
          <feComposite result='composite' operator='in' in2='SourceGraphic' />
          <feBlend result='blend' in2='SourceGraphic' />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path className='cls-1' d='M1629.57,369h-14.14a1.466,1.466,0,0,1-1.48-1.454V364.5a1.466,1.466,0,0,1,1.48-1.454h3.89a0.324,0.324,0,0,1,.32.291,2.876,2.876,0,0,0,5.71,0,0.321,0.321,0,0,1,.32-0.291v0h3.9a1.466,1.466,0,0,1,1.48,1.454v3.044A1.466,1.466,0,0,1,1629.57,369Zm-3.35-9.254-3.09,3.048a0.9,0.9,0,0,1-1.25,0l-3.1-3.048a1.021,1.021,0,0,1,.17-1.6,1.084,1.084,0,0,1,1.34.162l1.16,1.138v-7.414a1.05,1.05,0,0,1,2.1,0v7.414l1.16-1.138a1.087,1.087,0,0,1,1.35-.162A1.02,1.02,0,0,1,1626.22,359.746Z'
          transform='translate(-1613.94 -351)' />
      </g>
    </svg>
  );
};

IcDownloadIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
}

IcDownloadIcon.defaultProps = {
  width: '17.12',
  height: '18',
  viewbox: '0 0 17.12 18',
}

export default IcDownloadIcon;