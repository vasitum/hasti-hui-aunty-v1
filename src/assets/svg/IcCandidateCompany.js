import React from "react";
import PropTypes from "prop-types";

const IcCandidateCompany = props => {
  return (
    
    <svg xmlns='http://www.w3.org/2000/svg' preserveAspectRatio='xMidYMid'
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter id='color-overlay-1' filterUnits='userSpaceOnUse'>
          <feFlood floodColor='#acaeb5' />
          <feComposite operator='in' in2='SourceGraphic' />
          <feBlend in2='SourceGraphic' result='solidFill' />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path d='M13.177,15.987 C11.625,15.987 10.369,14.780 10.369,13.272 C10.369,12.118 11.152,11.134 12.097,10.741 L12.097,4.052 C12.097,3.829 11.989,3.685 11.760,3.685 L10.221,3.685 L9.600,3.685 L10.248,4.328 C10.410,4.485 10.504,4.708 10.504,4.931 C10.504,5.154 10.410,5.364 10.248,5.521 C10.086,5.679 9.870,5.757 9.640,5.757 C9.411,5.757 9.195,5.665 9.033,5.508 C9.033,5.508 6.980,3.515 6.967,3.502 C6.791,3.344 6.697,3.147 6.683,2.938 C6.670,2.702 6.751,2.479 6.913,2.308 C6.913,2.308 6.913,2.308 9.019,0.262 C9.181,0.105 9.397,0.013 9.627,0.013 C9.856,0.013 10.072,0.092 10.234,0.249 C10.396,0.406 10.491,0.590 10.491,0.813 C10.491,1.036 10.396,1.180 10.234,1.338 L9.600,1.849 L10.221,1.849 L12.219,1.849 L13.731,1.849 C13.961,1.849 14.123,2.059 14.123,2.282 L14.123,10.702 C15.203,11.082 15.999,12.092 15.999,13.272 C15.999,14.767 14.717,15.987 13.177,15.987 ZM9.073,12.485 C9.249,12.643 9.343,12.839 9.357,13.049 C9.370,13.285 9.289,13.508 9.127,13.679 C9.127,13.679 9.127,13.692 7.007,15.751 C6.845,15.908 6.629,16.000 6.400,16.000 C6.170,16.000 5.954,15.921 5.792,15.764 C5.630,15.607 5.536,15.423 5.536,15.200 C5.536,14.977 5.630,14.846 5.792,14.688 L6.427,14.190 L5.806,14.190 L3.808,14.190 L2.309,14.190 C2.079,14.190 1.837,13.941 1.837,13.718 L1.837,5.298 C0.756,4.918 0.000,3.908 0.000,2.728 C0.000,1.220 1.283,-0.000 2.822,-0.000 C4.375,-0.000 5.630,1.207 5.630,2.715 C5.630,3.869 4.807,4.852 3.862,5.246 L3.862,11.934 C3.862,12.170 4.051,12.341 4.280,12.341 L5.806,12.341 L6.427,12.341 L5.792,11.685 C5.630,11.528 5.536,11.292 5.536,11.069 C5.536,10.846 5.630,10.636 5.792,10.479 C5.954,10.321 6.170,10.229 6.400,10.229 C6.629,10.229 6.845,10.321 7.007,10.479 C7.007,10.479 9.073,12.472 9.073,12.485 Z'
          className='cls-1' />
      </g>

    </svg>
  );
};

IcCandidateCompany.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string,
  pathcolorSecond: PropTypes.string
};

IcCandidateCompany.defaultProps = {
  width: "16",
  height: "16",
  viewbox: "0 0 16 16"
};

export default IcCandidateCompany;
