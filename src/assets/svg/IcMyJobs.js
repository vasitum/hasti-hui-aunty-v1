import React from "react";
import PropTypes from "prop-types";

const IcMyJobs = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter
          id="filter"
          x="61.875"
          y="208.75"
          width="20.25"
          height="17.5"
          filterUnits="userSpaceOnUse">
          <feFlood result="flood" floodColor="#acaeb5" />
          <feComposite result="composite" operator="in" in2="SourceGraphic" />
          <feBlend result="blend" in2="SourceGraphic" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          id="Shape_22_copy"
          data-name="Shape 22 copy"
          className="cls-1"
          d="M81.588,225.705a1.735,1.735,0,0,1-1.276.535H63.686a1.735,1.735,0,0,1-1.276-.535,1.76,1.76,0,0,1-.531-1.286v-5.463h7.59v1.82a0.7,0.7,0,0,0,.215.513,0.693,0.693,0,0,0,.508.216h3.614a0.692,0.692,0,0,0,.508-0.216,0.7,0.7,0,0,0,.215-0.513v-1.82h7.59v5.463A1.759,1.759,0,0,1,81.588,225.705Zm-11.035-5.293v-1.456h2.891v1.456H70.554Zm-8.674-6.92a1.76,1.76,0,0,1,.531-1.286,1.731,1.731,0,0,1,1.276-.535h3.976V209.85a1.057,1.057,0,0,1,.316-0.774,1.044,1.044,0,0,1,.768-0.319h6.506a1.043,1.043,0,0,1,.767.319,1.057,1.057,0,0,1,.316.774v1.821h3.976a1.732,1.732,0,0,1,1.276.535,1.759,1.759,0,0,1,.531,1.286v4.371H61.879v-4.371Zm7.229-1.821h5.783v-1.457H69.108v1.457Z"
          transform="translate(-61.875 -208.75)"
        />
      </g>
    </svg>
  );
};

IcMyJobs.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string,
  rectcolor: PropTypes.string
};

IcMyJobs.defaultProps = {
  width: "20.25",
  height: "17.5",
  viewbox: "0 0 20.25 17.5"
};

export default IcMyJobs;
