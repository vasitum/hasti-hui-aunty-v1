import React from "react";
import PropTypes from "prop-types";

const IcShareFacebook = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      preserveAspectRatio="xMidYMid"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter id="color-overlay-1" filterUnits="userSpaceOnUse">
          <feFlood floodColor="#3b5998" />
          <feComposite operator="in" in2="SourceGraphic" />
          <feBlend in2="SourceGraphic" result="solidFill" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          d="M6.624,6.219 L6.624,8.282 L11.392,8.282 L10.742,12.395 L6.624,12.395 L6.624,24.809 L2.524,24.809 L2.524,12.395 L-0.011,12.395 L-0.011,8.280 L2.524,8.280 L2.524,5.791 C2.524,2.435 3.533,0.015 7.228,0.015 L11.622,0.015 L11.622,4.121 L8.527,4.121 C6.978,4.121 6.624,5.145 6.624,6.219 Z"
          className="cls-1"
        />
      </g>
    </svg>
  );
};

IcShareFacebook.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

IcShareFacebook.defaultProps = {
  width: "11.625",
  height: "24.813",
  viewbox: "0 0 11.625 24.813",
  pathcolor: "#3b5998"
};

export default IcShareFacebook;
