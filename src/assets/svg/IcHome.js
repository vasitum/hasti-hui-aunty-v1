import React from "react";
import PropTypes from "prop-types";

const IcHome = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter
          id="filter"
          x="1272.09"
          y="27.188"
          width="19.82"
          height="21.625"
          filterUnits="userSpaceOnUse">
          <feFlood result="flood" floodColor="#6e768a" />
          <feComposite result="composite" operator="in" in2="SourceGraphic" />
          <feBlend result="blend" in2="SourceGraphic" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          className="cls-1"
          d="M1291.78,36.889l-3.3-3.271v-4.1h-2.73v1.4L1282,27.2l-9.79,9.692a0.388,0.388,0,0,0-.02.514,0.336,0.336,0,0,0,.49.019l1.47-1.46V48.8h5.46V40.044a0.68,0.68,0,0,1,.66-0.705h3.45a0.688,0.688,0,0,1,.67.705V48.8h5.45V35.962l1.48,1.46a0.32,0.32,0,0,0,.23.1,0.337,0.337,0,0,0,.25-0.117A0.378,0.378,0,0,0,1291.78,36.889Z"
          transform="translate(-1272.09 -27.188)"
        />
      </g>
    </svg>
  );
};

IcHome.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

IcHome.defaultProps = {
  width: "19.82",
  height: "21.625",
  viewbox: "0 0 19.82 21.625"
};

export default IcHome;
