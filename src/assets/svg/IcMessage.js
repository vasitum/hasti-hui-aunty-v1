import React from "react";
import PropTypes from "prop-types";

const IcMessage = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter
          id="filter"
          x="1477.09"
          y="30.063"
          width="20.82"
          height="15.844"
          filterUnits="userSpaceOnUse">
          <feFlood result="flood" floodColor="#6e768a" />
          <feComposite result="composite" operator="in" in2="SourceGraphic" />
          <feBlend result="blend" in2="SourceGraphic" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          className="cls-1"
          d="M1497.35,45.392a1.808,1.808,0,0,1-1.31.529h-17.08a1.808,1.808,0,0,1-1.31-.529,1.726,1.726,0,0,1-.55-1.272V35.186a6.177,6.177,0,0,0,1.18.979q4.2,2.768,5.76,3.882,0.66,0.472,1.08.737a6.281,6.281,0,0,0,1.09.54,3.439,3.439,0,0,0,1.28.275h0.02a3.439,3.439,0,0,0,1.28-.275,6.667,6.667,0,0,0,1.09-.54q0.42-.264,1.08-0.737c1.31-.923,3.24-2.217,5.77-3.882a6.293,6.293,0,0,0,1.16-.979V44.12A1.716,1.716,0,0,1,1497.35,45.392Zm-1.44-10.431q-4.365,2.937-5.43,3.657c-0.08.053-.24,0.167-0.49,0.343s-0.46.319-.63,0.428-0.37.23-.6,0.366a3.677,3.677,0,0,1-.67.3,1.9,1.9,0,0,1-.58.1h-0.02a1.9,1.9,0,0,1-.58-0.1,3.677,3.677,0,0,1-.67-0.3q-0.36-.2-0.6-0.366-0.255-.163-0.63-0.428c-0.25-.176-0.42-0.291-0.49-0.343-0.71-.48-1.72-1.164-3.04-2.053s-2.12-1.424-2.38-1.6a5.77,5.77,0,0,1-1.36-1.3,2.622,2.622,0,0,1-.64-1.536,2.185,2.185,0,0,1,.49-1.463,1.663,1.663,0,0,1,1.37-.585h17.08a1.8,1.8,0,0,1,1.3.529,1.689,1.689,0,0,1,.55,1.271,2.965,2.965,0,0,1-.56,1.7A5.532,5.532,0,0,1,1495.91,34.961Z"
          transform="translate(-1477.09 -30.063)"
        />
      </g>
    </svg>
  );
};

IcMessage.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

IcMessage.defaultProps = {
  width: "20.82",
  height: "15.844",
  viewbox: "0 0 20.82 15.844"
};

export default IcMessage;
