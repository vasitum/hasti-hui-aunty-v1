import React from "react";
import PropTypes from 'prop-types';

const IcJobIcon = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter
          id="filter"
          x="1379.88"
          y="29.75"
          width="20.25"
          height="17.5"
          filterUnits="userSpaceOnUse">
          <feFlood result="flood" floodColor="#6e768a" />
          <feComposite result="composite" operator="in" in2="SourceGraphic" />
          <feBlend result="blend" in2="SourceGraphic" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          className="cls-1"
          d="M1399.59,46.706a1.748,1.748,0,0,1-1.28.535h-16.62a1.732,1.732,0,0,1-1.28-.535,1.775,1.775,0,0,1-.53-1.286V39.956h7.59v1.821a0.728,0.728,0,0,0,.72.729h3.62a0.746,0.746,0,0,0,.72-0.729V39.956h7.59V45.42A1.775,1.775,0,0,1,1399.59,46.706Zm-11.04-5.293V39.956h2.9v1.456h-2.9Zm-8.67-6.92a1.774,1.774,0,0,1,.53-1.286,1.73,1.73,0,0,1,1.28-.535h3.97V30.851a1.064,1.064,0,0,1,.32-0.774,1.047,1.047,0,0,1,.77-0.319h6.5a1.047,1.047,0,0,1,.77.319,1.064,1.064,0,0,1,.32.774v1.821h3.97a1.746,1.746,0,0,1,1.28.535,1.774,1.774,0,0,1,.53,1.286v4.371h-20.24V34.493Zm7.23-1.821h5.78V31.215h-5.78v1.457Z"
          transform="translate(-1379.88 -29.75)"
        />
      </g>
    </svg>
  );
};

IcJobIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
}

IcJobIcon.defaultProps = {
  width: '20.25',
  height: '17.5',
  viewbox: '0 0 20.25 17.5',
}

export default IcJobIcon;
