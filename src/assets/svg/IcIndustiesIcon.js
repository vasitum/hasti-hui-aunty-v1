import React from "react";
import PropTypes from 'prop-types';

const IcIndustiesIcon = props => {
  return (
    
    <svg xmlns='http://www.w3.org/2000/svg' preserveAspectRatio='xMidYMid'
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter id='color-overlay-1' filterUnits='userSpaceOnUse'>
          <feFlood floodColor='#acaeb5' />
          <feComposite operator='in' in2='SourceGraphic' />
          <feBlend in2='SourceGraphic' result='solidFill' />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path d='M7.315,12.529 L7.315,13.101 C7.315,13.557 6.968,13.936 6.517,14.000 L6.370,14.520 C6.291,14.802 6.028,15.000 5.723,15.000 L4.273,15.000 C3.968,15.000 3.705,14.802 3.625,14.520 L3.483,14.000 C3.028,13.932 2.681,13.557 2.681,13.097 L2.681,12.525 C2.681,12.218 2.936,11.973 3.253,11.973 L6.742,11.973 C7.060,11.977 7.315,12.222 7.315,12.529 ZM10.001,4.813 C10.001,6.115 9.466,7.296 8.597,8.163 C7.937,8.824 7.511,9.667 7.369,10.566 C7.306,10.953 6.960,11.239 6.550,11.239 L3.446,11.239 C3.040,11.239 2.689,10.957 2.631,10.570 C2.485,9.671 2.054,8.816 1.394,8.159 C0.542,7.304 0.011,6.143 -0.001,4.865 C-0.031,2.181 2.184,0.008 4.967,-0.012 C7.745,-0.033 10.001,2.136 10.001,4.813 Z'
          className='cls-1' />
      </g>

    </svg>
  );
};

IcIndustiesIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
}

IcIndustiesIcon.defaultProps = {
  width: '10',
  height: '15',
  viewbox: '0 0 10 15',
}

export default IcIndustiesIcon;