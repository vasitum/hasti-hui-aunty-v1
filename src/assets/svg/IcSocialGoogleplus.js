import React from "react";
import PropTypes from 'prop-types';

const IcSocialGoogleplus = props => {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' preserveAspectRatio='xMidYMid'
      width={props.width} height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter id='color-overlay-1' filterUnits='userSpaceOnUse'>
          <feFlood floodColor='#727b8f' />
          <feComposite operator='in' in2='SourceGraphic' />
          <feBlend in2='SourceGraphic' result='solidFill' />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path d='M9.503,3.355 L8.815,3.355 L8.815,2.028 L7.469,2.028 L7.469,1.341 L8.815,1.341 L8.815,0.017 L9.503,0.017 L9.503,1.341 L10.826,1.341 L10.826,2.028 L9.503,2.028 L9.503,3.355 ZM5.289,0.433 C5.289,0.433 6.245,1.022 6.245,2.173 C6.245,3.324 5.686,3.826 4.980,4.446 C4.274,5.066 4.685,5.434 5.789,6.216 C6.892,6.998 7.201,8.727 5.774,9.981 C4.348,11.234 0.007,11.066 0.007,8.740 C0.007,6.413 3.273,6.378 3.539,6.378 C3.803,6.378 3.848,6.304 3.627,6.068 C3.405,5.834 3.052,5.375 3.465,4.874 C2.728,4.904 0.699,4.505 0.699,2.646 C0.699,0.786 2.656,-0.010 3.360,-0.010 L6.878,-0.010 C6.878,-0.010 6.717,0.123 6.348,0.329 C5.980,0.536 5.289,0.433 5.289,0.433 ZM1.257,8.312 C1.257,9.389 2.037,10.112 3.492,10.112 C4.947,10.112 5.803,9.625 5.803,8.622 C5.803,7.617 4.841,7.116 4.303,6.703 C2.317,6.703 1.257,7.234 1.257,8.312 ZM3.214,0.345 C1.154,0.345 1.948,4.416 3.700,4.416 C6.024,4.416 4.817,0.285 3.214,0.345 Z'
          className='cls-1' />
      </g>
    </svg>
  );
};

IcSocialGoogleplus.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
}

IcSocialGoogleplus.defaultProps = {
  width: '10.812',
  height: '10.78',
  viewbox: '0 0 10.812 10.78',
}

export default IcSocialGoogleplus;