import React from "react";
import PropTypes from "prop-types";

const IcCompanyNocircle = props => {
  return (

    <svg xmlns='http://www.w3.org/2000/svg' preserveAspectRatio='xMidYMid'
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}
      style={props.style}>
      <defs />
      <g fill={props.pathcolor}>
        <path d='M7.930,18.991 L-0.006,18.991 L-0.006,2.210 L2.534,2.210 L2.534,-0.006 L17.451,-0.006 L17.451,2.210 L19.991,2.210 L19.991,18.991 L11.897,18.991 L7.930,18.991 ZM8.240,16.617 L12.049,16.617 L12.049,14.084 L8.240,14.084 L8.240,16.617 ZM8.240,11.867 L12.049,11.867 L12.049,9.334 L8.240,9.334 L8.240,11.867 ZM8.240,7.118 L12.049,7.118 L12.049,4.585 L8.240,4.585 L8.240,7.118 Z'
          className='cls-1' />
      </g>
    </svg>
  );
};

IcCompanyNocircle.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string,
  rectcolor: PropTypes.string
};

IcCompanyNocircle.defaultProps = {
  width: "20",
  height: "19",
  viewbox: "0 0 20 19"
};

export default IcCompanyNocircle;
