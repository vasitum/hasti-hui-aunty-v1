import React from "react";
import PropTypes from "prop-types";

const IcRelationship = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter
          id="filter"
          x="1053"
          y="375.031"
          width="42"
          height="63.938"
          filterUnits="userSpaceOnUse">
          <feFlood result="flood" floodColor="#0b9ed0" />
          <feComposite result="composite" operator="in" in2="SourceGraphic" />
          <feBlend result="blend" in2="SourceGraphic" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          id="Forma_1"
          data-name="Forma 1"
          className="cls-1"
          d="M1094.99,381.452a6.261,6.261,0,0,0-6.4-6.415,6.446,6.446,0,0,0-6.51,6.418,5.876,5.876,0,0,0,1.94,4.576,5.808,5.808,0,0,0,4.57,1.831,6.136,6.136,0,0,0,4.57-1.831A6.227,6.227,0,0,0,1094.99,381.452Zm-21.86,28.286c2.55,0,5.8-1.021,9.55-3.05v32.275h8.45V392.742q-17.52,21.225-35.06,0v46.221h8.83V406.688C1067.95,408.717,1070.6,409.738,1073.13,409.738Zm-18.3-32.865a6.568,6.568,0,0,0-1.82,4.579,6.24,6.24,0,0,0,6.4,6.407,5.878,5.878,0,0,0,4.59-1.831,5.961,5.961,0,0,0,1.92-4.576,6.409,6.409,0,0,0-6.51-6.415A6.573,6.573,0,0,0,1054.83,376.873Z"
          transform="translate(-1053 -375.031)"
        />
      </g>
    </svg>
  );
};

IcRelationship.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string,
  rectcolor: PropTypes.string
};

IcRelationship.defaultProps = {
  width: "42",
  height: "63.938",
  viewbox: "0 0 42 63.938"
};

export default IcRelationship;
