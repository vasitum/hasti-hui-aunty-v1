import React from "react";
import PropTypes from "prop-types";

const IcHamburgerMenu = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs />
      <g fill={props.pathcolor}>
        <path
          className="cls-1"
          d="M3669.06,1060h-22.1a0.9,0.9,0,0,1-.96-0.95v-1.9a0.9,0.9,0,0,1,.96-0.95h22.1a0.9,0.9,0,0,1,.95.95v1.9A0.9,0.9,0,0,1,3669.06,1060Zm0-7.6h-22.1a0.9,0.9,0,0,1-.96-0.95v-1.9a0.9,0.9,0,0,1,.96-0.95h22.1a0.9,0.9,0,0,1,.95.95v1.9A0.9,0.9,0,0,1,3669.06,1052.4Zm0-7.6h-22.1a0.9,0.9,0,0,1-.96-0.95v-1.9a0.9,0.9,0,0,1,.96-0.95h22.1a0.9,0.9,0,0,1,.95.95v1.9A0.9,0.9,0,0,1,3669.06,1044.8Z"
          transform="translate(-3646 -1041)"
        />
      </g>
    </svg>
  );
};

IcHamburgerMenu.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

IcHamburgerMenu.defaultProps = {
  width: "24",
  height: "19",
  viewbox: "0 0 24 19"
};

export default IcHamburgerMenu;
