import React from "react";
import PropTypes from "prop-types";

const IcDashboard = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter
          id="filter"
          x="62"
          y="106"
          width="20"
          height="20"
          filterUnits="userSpaceOnUse">
          <feFlood result="flood" floodColor="#acaeb5" />
          <feComposite result="composite" operator="in" in2="SourceGraphic" />
          <feBlend result="blend" in2="SourceGraphic" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          className="cls-1"
          d="M73.111,126V114.889H82V126H73.111Zm0-20H82v6.667H73.111V106ZM62,119.333h8.889V126H62v-6.667ZM62,106h8.889v11.111H62V106Z"
          transform="translate(-62 -106)"
        />
      </g>
    </svg>
  );
};

IcDashboard.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string,
  rectcolor: PropTypes.string
};

IcDashboard.defaultProps = {
  width: "20",
  height: "20",
  viewbox: "0 0 20 20"
};

export default IcDashboard;
