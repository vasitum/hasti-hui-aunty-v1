import React from "react";
import PropTypes from "prop-types";

const ResumeDownloadIcon = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      preserveAspectRatio="xMidYMid"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter id="color-overlay-1" filterUnits="userSpaceOnUse">
          <feFlood floodColor="#0b9ed0" />
          <feComposite operator="in" in2="SourceGraphic" />
          <feBlend in2="SourceGraphic" result="solidFill" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          d="M9.340,11.000 L0.660,11.000 C0.292,11.000 -0.008,10.693 -0.008,10.312 L-0.008,7.562 L1.328,7.562 L1.328,9.625 L8.673,9.625 L8.673,7.562 L10.008,7.562 L10.008,10.312 C10.008,10.693 9.709,11.000 9.340,11.000 ZM5.251,8.132 C5.188,8.206 5.096,8.250 5.000,8.250 C4.904,8.250 4.813,8.207 4.749,8.132 L2.412,5.382 C2.326,5.280 2.305,5.137 2.360,5.014 C2.414,4.891 2.532,4.812 2.663,4.812 L3.999,4.812 L3.999,0.344 C3.999,0.154 4.148,-0.000 4.332,-0.000 L5.668,-0.000 C5.852,-0.000 6.002,0.154 6.002,0.344 L6.002,4.812 L7.337,4.812 C7.468,4.812 7.587,4.891 7.641,5.014 C7.695,5.137 7.675,5.281 7.588,5.382 L5.251,8.132 Z"
          className="cls-1"
        />
      </g>
    </svg>
  );
};

ResumeDownloadIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

ResumeDownloadIcon.defaultProps = {
  width: "22",
  height: "18",
  viewbox: "0 0 22 18"
};

export default ResumeDownloadIcon;
