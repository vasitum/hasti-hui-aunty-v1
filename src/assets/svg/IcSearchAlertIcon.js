
import React from "react";
import PropTypes from 'prop-types';

const IcSearchAlertIcon = props => {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width={props.width} height={props.height} 
      viewBox={props.viewbox}>
      <defs />
      <g fill={props.pathcolor}>
        <path className='cls-1' d='M1926.18,139.926a1.379,1.379,0,1,0,0,2.756h11.51a1.379,1.379,0,1,0,0-2.756l-0.57.041a4.6,4.6,0,0,1-.86-2.747v-1.387a4.213,4.213,0,0,0-2.89-3.943v-0.712a1.436,1.436,0,0,0-2.87,0v0.727a4.4,4.4,0,0,0-2.88,4.084v1.231a4.609,4.609,0,0,1-.87,2.747Z'
          transform='translate(-1924.75 -129.813)' />
      </g>
      <rect className='cls-2' x='4.25' y='15.187' width='6' height='1' fill={props.rectcolor}/>
    </svg>
  );
};

IcSearchAlertIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
}

IcSearchAlertIcon.defaultProps = {
  width: '14.38',
  height: '16.187',
  viewbox: '0 0 14.38 16.187',
}

export default IcSearchAlertIcon;