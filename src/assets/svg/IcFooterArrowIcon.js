import React from "react";
import PropTypes from "prop-types";

const IcFooterArrowIcon = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      onClick={props.onClick}
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter
          id="filter"
          x="173.313"
          y="2375.94"
          width="16.156"
          height="10.78"
          filterUnits="userSpaceOnUse">
          <feFlood result="flood" floodColor="#3f4654" />
          <feComposite result="composite" operator="in" in2="SourceGraphic" />
          <feBlend result="blend" in2="SourceGraphic" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          id="Shape_41_copy"
          data-name="Shape 41 copy"
          className="cls-1"
          d="M184.185,2386.52a0.583,0.583,0,0,1-.813,0,0.52,0.52,0,0,1,0-.76l4.123-3.92H173.89a0.553,0.553,0,0,1-.577-0.54,0.559,0.559,0,0,1,.577-0.54H187.5l-4.123-3.91a0.532,0.532,0,0,1,0-.77,0.583,0.583,0,0,1,.813,0l5.1,4.84a0.52,0.52,0,0,1,0,.76Z"
          transform="translate(-173.313 -2375.94)"
        />
      </g>
    </svg>
  );
};

IcFooterArrowIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

IcFooterArrowIcon.defaultProps = {
  width: "16.156",
  height: "10.78",
  viewbox: "0 0 16.156 10.78"
};

export default IcFooterArrowIcon;
