import React from "react";
import PropTypes from 'prop-types';

const IcEditIcon = props => {
	return (
		<svg xmlns='http://www.w3.org/2000/svg' width={props.width} height={props.height}
			viewBox={props.viewbox}>
			<defs>
				<filter id='filter' x='525.75' y='435.75' width='11.688' height='11.5'
					filterUnits='userSpaceOnUse'>
					<feFlood result='flood' floodColor='#c8c8c8' />
					<feComposite result='composite' operator='in' in2='SourceGraphic' />
					<feBlend result='blend' in2='SourceGraphic' />
				</filter>
			</defs>
			<g fill={props.pathcolor}>
				<path id='Shape_17_copy_2' data-name='Shape 17 copy 2' className='cls-1'
					d='M536.861,436.3a1.917,1.917,0,0,0-2.684,0l-7.605,7.507-0.009.012-0.012.016a0.183,0.183,0,0,0-.026.05,0.094,0.094,0,0,0-.006.015l0,0.007L525.771,447a0.151,0.151,0,0,0,0,.042s0,0.006,0,.008a0.229,0.229,0,0,0,.014.069c0,0.006,0,.01.006,0.015a0.23,0.23,0,0,0,.041.062,0.21,0.21,0,0,0,.07.045,0.2,0.2,0,0,0,.128.01l3.132-.736a0.076,0.076,0,0,0,.016-0.006l0.021-.008a0.226,0.226,0,0,0,.042-0.026l0.016-.011a0.022,0.022,0,0,1,0,0l7.605-7.508A1.859,1.859,0,0,0,536.861,436.3Z'
					transform='translate(-525.75 -435.75)' />
			</g>
		</svg>
	);
};

IcEditIcon.propTypes = {
	width: PropTypes.string,
	height: PropTypes.string,
	viewbox: PropTypes.string,
	pathcolor: PropTypes.string
}

IcEditIcon.defaultProps = {
	width: '11.688',
	height: '11.5',
	viewbox: '0 0 11.688 11.5',
}

export default IcEditIcon;