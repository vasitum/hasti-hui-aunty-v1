import React from "react";
import PropTypes from "prop-types";

const IcDownloadSecond = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter
          id="filter"
          x="1142"
          y="111"
          width="19"
          height="20"
          filterUnits="userSpaceOnUse">
          <feFlood result="flood" floodColor="#acaeb5" />
          <feComposite result="composite" operator="in" in2="SourceGraphic" />
          <feBlend result="blend" in2="SourceGraphic" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          id="Forma_1"
          data-name="Forma 1"
          className="cls-1"
          d="M1156.51,120.116a0.632,0.632,0,0,0-.58-0.366h-2.53v-8.125a0.626,0.626,0,0,0-.63-0.625h-2.54a0.626,0.626,0,0,0-.63.625v8.125h-2.53a0.624,0.624,0,0,0-.48,1.036l4.43,5a0.645,0.645,0,0,0,.96,0l4.43-5A0.631,0.631,0,0,0,1156.51,120.116Zm1.96,4.634v3.75h-13.94v-3.75H1142v5a1.261,1.261,0,0,0,1.27,1.25h16.46a1.261,1.261,0,0,0,1.27-1.25v-5h-2.53Z"
          transform="translate(-1142 -111)"
        />
      </g>
    </svg>
  );
};

IcDownloadSecond.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string,
  rectcolor: PropTypes.string
};

IcDownloadSecond.defaultProps = {
  width: "19",
  height: "20",
  viewbox: "0 0 19 20"
};

export default IcDownloadSecond;
