import React from "react";
import PropTypes from "prop-types";

const IcDocument = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter
          id="filter"
          x="589"
          y="161"
          width="17"
          height="21"
          filterUnits="userSpaceOnUse">
          <feFlood result="flood" floodColor="#acaeb5" />
          <feComposite result="composite" operator="in" in2="SourceGraphic" />
          <feBlend result="blend" in2="SourceGraphic" />
        </filter>
      </defs>

      <g fill={props.pathcolor}>
        <path
          id="Forma_1"
          data-name="Forma 1"
          className="cls-1"
          d="M605.861,165.5l-4.721-4.373A0.49,0.49,0,0,0,600.8,161h-9.916A1.825,1.825,0,0,0,589,162.75v17.5a1.825,1.825,0,0,0,1.889,1.75h13.222A1.826,1.826,0,0,0,606,180.25V165.812A0.422,0.422,0,0,0,605.861,165.5Zm-7.889,13H593.25a0.439,0.439,0,1,1,0-.875h4.722A0.439,0.439,0,1,1,597.972,178.5Zm3.778-2.625h-8.5a0.439,0.439,0,1,1,0-.875h8.5A0.439,0.439,0,1,1,601.75,175.875Zm0-2.625h-8.5a0.439,0.439,0,1,1,0-.875h8.5A0.439,0.439,0,1,1,601.75,173.25Zm0-2.625h-8.5a0.439,0.439,0,1,1,0-.875h8.5A0.439,0.439,0,1,1,601.75,170.625Zm0.472-5.25a0.913,0.913,0,0,1-.945-0.875v-2.007l3.111,2.882h-2.166Z"
          transform="translate(-589 -161)"
        />
      </g>
    </svg>
  );
};

IcDocument.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

IcDocument.defaultProps = {
  width: "17",
  height: "21",
  viewbox: "0 0 17 21"
};

export default IcDocument;
