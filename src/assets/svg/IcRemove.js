import React from "react";
import PropTypes from "prop-types";

const IcPlusIcon = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter
          id="filter"
          x="844"
          y="90"
          width="12"
          height="16"
          filterUnits="userSpaceOnUse">
          <feFlood result="flood" floodColor="#acaeb5" />
          <feComposite result="composite" operator="in" in2="SourceGraphic" />
          <feBlend result="blend" in2="SourceGraphic" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          className="cls-1"
          d="M854.667,91.9h-2V91.267A1.3,1.3,0,0,0,851.333,90h-2.666a1.3,1.3,0,0,0-1.334,1.267V91.9h-2A1.3,1.3,0,0,0,844,93.167v0.95a0.325,0.325,0,0,0,.333.317h11.334A0.325,0.325,0,0,0,856,94.117v-0.95A1.3,1.3,0,0,0,854.667,91.9Zm-3.334,0h-2.666V91.267h2.666V91.9ZM855,95.867H845a0.325,0.325,0,0,0-.333.317v8.55A1.3,1.3,0,0,0,846,106h8a1.3,1.3,0,0,0,1.333-1.267v-8.55A0.325,0.325,0,0,0,855,95.867Z"
          transform="translate(-844 -90)"
        />
      </g>
    </svg>
  );
};

IcPlusIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

IcPlusIcon.defaultProps = {
  width: "12",
  height: "16",
  viewbox: "0 0 12 16"
};

export default IcPlusIcon;
