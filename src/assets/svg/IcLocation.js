import React from "react";
import PropTypes from "prop-types";
const IcLocationIcon = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      preserveAspectRatio="xMidYMid"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter id="color-overlay-1" filterUnits="userSpaceOnUse">
          <feFlood floodColor="#0bd0bb" />
          <feComposite operator="in" in2="SourceGraphic" />
          <feBlend in2="SourceGraphic" result="solidFill" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          d="M10.228,1.651 C9.096,0.577 7.591,-0.014 5.990,-0.014 C4.389,-0.014 2.884,0.577 1.752,1.651 C-0.343,3.637 -0.603,6.234 1.188,8.498 C1.188,8.498 2.198,9.641 3.292,10.879 C6.890,14.952 5.087,14.956 8.700,10.867 C9.786,9.638 10.785,8.507 10.785,8.507 C12.584,6.234 12.323,3.637 10.228,1.651 ZM6.045,7.691 C4.839,7.691 3.858,6.760 3.858,5.617 C3.858,4.473 4.839,3.542 6.045,3.542 C7.252,3.542 8.233,4.473 8.233,5.617 C8.233,6.760 7.252,7.691 6.045,7.691 Z"
          className="cls-1"
        />
      </g>
    </svg>
    // <svg
    //   xmlns="http://www.w3.org/2000/svg"
    //   width={props.width}
    //   height={props.height}
    //   viewBox={props.viewbox}>
    //   <defs>
    //     <filter
    //       id="filter"
    //       x="560"
    //       y="1037"
    //       width="12"
    //       height="14"
    //       filterUnits="userSpaceOnUse">
    //       <feFlood result="flood" floodColor="#0bd0bb" />
    //       <feComposite result="composite" operator="in" in2="SourceGraphic" />
    //       <feBlend result="blend" in2="SourceGraphic" />
    //     </filter>
    //   </defs>
    //   <g fill={props.pathcolor}>
    //     <path
    //       id="Shape_52_copy_2"
    //       data-name="Shape 52 copy 2"
    //       className="cls-1"
    //       d="M570.228,1038.66a6.212,6.212,0,0,0-8.476,0,6.261,6.261,0,0,0-.564,8.02l4.8,4.32,4.795-4.31A6.263,6.263,0,0,0,570.228,1038.66Zm-4.183,6.07a2.087,2.087,0,1,1,2.188-2.09A2.144,2.144,0,0,1,566.045,1044.73Z"
    //       transform="translate(-560 -1037)"
    //     />
    //   </g>
    // </svg>
  );
};

IcLocationIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

IcLocationIcon.defaultProps = {
  width: "12",
  height: "13.938",
  viewbox: "0 0 12 13.938"
};

export default IcLocationIcon;
