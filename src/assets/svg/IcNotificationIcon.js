import React from "react";
import PropTypes from "prop-types";

const IcNotificationIcon = props => {
  return (

    <svg xmlns='http://www.w3.org/2000/svg' preserveAspectRatio='xMidYMid'
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs />
      <g fill={props.pathcolor}>
        <path d='M-0.000,16.923 L-0.000,15.897 L2.000,13.846 L2.000,8.718 C2.000,5.564 4.635,2.933 7.500,2.236 L7.500,1.538 C7.500,0.687 8.170,0.000 9.000,0.000 C9.830,0.000 10.500,0.687 10.500,1.538 L10.500,2.236 C13.365,2.933 16.000,5.564 16.000,8.718 L16.000,13.846 L18.000,15.897 L18.000,16.923 L-0.000,16.923 ZM9.000,20.000 C7.895,20.000 7.000,19.082 7.000,17.949 L11.000,17.949 C11.000,19.082 10.105,20.000 9.000,20.000 Z'
          className='cls-1' />
      </g>
    </svg>
  );
};

IcNotificationIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string,
  rectcolor: PropTypes.string
};

IcNotificationIcon.defaultProps = {
  width: "18",
  height: "20",
  viewbox: "0 0 18 20"
};

export default IcNotificationIcon;