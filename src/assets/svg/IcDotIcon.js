import React from "react";
import PropTypes from "prop-types";

const IcDotIcon = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs />
      <g fill={props.pathcolor}>
        <ellipse
          id="Ellipse_2_copy_3"
          data-name="Ellipse 2 copy 3"
          className="cls-1"
          cx="2.844"
          cy="2.797"
          rx="2.844"
          ry="2.797"
        />
      </g>
    </svg>
  );
};

IcDotIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string,
  rectcolor: PropTypes.string,
  pathcolor: PropTypes.string
};

IcDotIcon.defaultProps = {
  width: "5.687",
  height: "5.594",
  viewbox: "0 0 5.687 5.594",
  pathcolor: "#c8c8c8"
};

export default IcDotIcon;
