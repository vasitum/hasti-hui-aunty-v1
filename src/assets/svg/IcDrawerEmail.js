
import React from "react";
import PropTypes from 'prop-types';

const IcDrawerEmail = props => {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width={props.width} height={props.height} 
    viewBox={props.viewbox}>
      <defs>
        <filter id='filter' x='1839.75' y='1369' width='15.94' height='16' filterUnits='userSpaceOnUse'>
          <feFlood result='flood' floodColor='#c9c9c9' />
          <feComposite result='composite' operator='in' in2='SourceGraphic' />
          <feBlend result='blend' in2='SourceGraphic' />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path className='cls-1' d='M1853.49,1371.02a7.79,7.79,0,0,0-5.47-2.02,8.2,8.2,0,0,0-5.86,2.29,7.592,7.592,0,0,0-2.42,5.72,7.794,7.794,0,0,0,2.3,5.63,8.306,8.306,0,0,0,6.21,2.36,11.737,11.737,0,0,0,4.62-.96,0.984,0.984,0,0,0,.53-1.27h0a0.989,0.989,0,0,0-1.3-.53,9.622,9.622,0,0,1-3.85.85,5.945,5.945,0,0,1-4.64-1.82,6.141,6.141,0,0,1-1.64-4.25,6.046,6.046,0,0,1,1.76-4.43,5.855,5.855,0,0,1,4.32-1.78,5.75,5.75,0,0,1,3.98,1.46,4.731,4.731,0,0,1,1.62,3.67,4.158,4.158,0,0,1-.74,2.52,2.034,2.034,0,0,1-1.55,1.01,0.4,0.4,0,0,1-.43-0.47,7.635,7.635,0,0,1,.06-0.88l0.61-4.97h-2.1l-0.14.49a2.74,2.74,0,0,0-1.76-.65,3.362,3.362,0,0,0-2.59,1.2,5.03,5.03,0,0,0-.12,6.07,2.9,2.9,0,0,0,2.3,1.13,2.6,2.6,0,0,0,2.06-1,2.15,2.15,0,0,0,1.9.96,4.032,4.032,0,0,0,3.19-1.6,5.761,5.761,0,0,0,1.35-3.85A6.361,6.361,0,0,0,1853.49,1371.02Zm-4.84,7.51a1.631,1.631,0,0,1-1.33.74,0.972,0.972,0,0,1-.85-0.54,2.722,2.722,0,0,1-.32-1.37,2.68,2.68,0,0,1,.45-1.62,1.359,1.359,0,0,1,1.12-.62,1.406,1.406,0,0,1,1.04.46,1.7,1.7,0,0,1,.45,1.24A2.8,2.8,0,0,1,1848.65,1378.53Z'
          transform='translate(-1839.75 -1369)' />
      </g>
    </svg>
  );
};

IcDrawerEmail.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
}

IcDrawerEmail.defaultProps = {
  width: '15.94',
  height: '16',
  viewbox: '0 0 15.94 16',
}

export default IcDrawerEmail;