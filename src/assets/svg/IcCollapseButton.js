import React from "react";
import PropTypes from "prop-types";

const IcCollapseButton = ({ width, height, pathcolor, viewbox, ...props }) => {
  return (
    // <svg
    //   xmlns="http://www.w3.org/2000/svg"
    //   width={width}
    //   height={height}
    //   viewBox={viewbox}
    //   {...props}>
    //   <defs />
    //   <g fill={pathcolor}>
    //     <path
    //       id="Shape_53_copy_3"
    //       data-name="Shape 53 copy 3"
    //       className="cls-1"
    //       d="M1627.36,1049l4.14-4.14a1.679,1.679,0,0,0-2.37-2.38l-4.15,4.14-4.14-4.14a1.679,1.679,0,0,0-2.37,2.38l4.14,4.14-4.14,4.14a1.676,1.676,0,0,0,2.37,2.37l4.14-4.14,4.15,4.14a1.633,1.633,0,0,0,1.18.49,1.675,1.675,0,0,0,1.19-2.86Z"
    //       transform="translate(-1618 -1042)"
    //     />
    //   </g>
    // </svg>
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={width}
      height={height}
      viewBox={viewbox}
      shapeRendering="geometricPrecision"
      textRendering="geometricPrecision"
      imageRendering="optimizeQuality"
      fillRule="evenodd"
      clipRule="evenodd">
      <defs />
      <g id="Layer_x0020_1" fill={pathcolor}>
        <path
          className="fil0"
          d="M174 72l-31 0 52 -53c5,-4 5,-11 0,-16l0 0c-4,-4 -11,-4 -15,0l-53 53 0 -31c0,-7 -5,-12 -11,-12l0 0c-6,0 -11,5 -11,12l0 57c0,3 1,6 3,8 2,3 5,4 8,4l58 0c6,0 11,-5 11,-11l0 0c0,-6 -5,-11 -11,-11z"
        />
        <path
          className="fil0"
          d="M25 127l31 0 -53 53c-4,4 -4,11 0,15l0 0c5,5 12,5 16,0l53 -52 0 31c0,6 5,11 11,11l0 0c6,0 11,-5 11,-11l0 -58c0,-3 -1,-6 -4,-8 -2,-2 -5,-3 -8,-3l-57 0c-7,0 -12,5 -12,11l0 0c0,6 5,11 12,11z"
        />
      </g>
    </svg>
  );
};

IcCollapseButton.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

IcCollapseButton.defaultProps = {
  width: "35.601",
  height: "35.601",
  viewbox: "0 0 199 199"
};

export default IcCollapseButton;
