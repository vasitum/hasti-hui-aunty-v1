import React from "react";
import PropTypes from "prop-types";

const IcCertification = props => {
  return (
    <svg
      id="certificate_icon"
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter
          id="filter"
          x="0"
          y="0"
          width="48.875"
          height="49.53"
          filterUnits="userSpaceOnUse">
          <feFlood result="flood" floodColor="#f7f7fb" />
          <feComposite result="composite" operator="in" in2="SourceGraphic" />
          <feBlend result="blend" in2="SourceGraphic" />
        </filter>
        <filter
          id="filter-2"
          x="156"
          y="1589"
          width="23.031"
          height="25"
          filterUnits="userSpaceOnUse">
          <feFlood result="flood" floodColor="#acaeb5" />
          <feComposite result="composite" operator="in" in2="SourceGraphic" />
          <feBlend result="blend" in2="SourceGraphic" />
        </filter>
      </defs>
      <circle
        fill={props.circleColor}
        id="Ellipse_1_copy_20"
        data-name="Ellipse 1 copy 20"
        className="cls-1"
        cx="24.438"
        cy="24.765"
        r="24.438"
      />
      <g fill={props.pathcolor}>
        <path
          className="cls-2"
          d="M177.922,1611.2l-3.328-1.23-0.25.11-1.211,3.38a0.8,0.8,0,0,1-1.481.07l-2.363-5.15a10.448,10.448,0,0,0,6.81-4.44l2.824,6.15A0.813,0.813,0,0,1,177.922,1611.2Zm-10.423-4.28a8.961,8.961,0,1,1,8.825-8.96A8.9,8.9,0,0,1,167.5,1606.92Zm3.85-11.01-2.163-.32-0.967-1.99a0.8,0.8,0,0,0-1.439,0l-0.967,1.99-2.163.32a0.819,0.819,0,0,0-.444,1.39l1.564,1.55-0.369,2.19a0.808,0.808,0,0,0,1.164.86l1.934-1.04,1.935,1.04a0.806,0.806,0,0,0,1.164-.86l-0.369-2.19,1.564-1.55A0.819,0.819,0,0,0,171.349,1595.91Zm-5.639,12.47-2.363,5.15a0.8,0.8,0,0,1-1.481-.07l-1.211-3.38-0.25-.11-3.328,1.23a0.812,0.812,0,0,1-1-1.11l2.824-6.15A10.446,10.446,0,0,0,165.71,1608.38Z"
          transform="translate(-143.469 -1577)"
        />
      </g>
    </svg>
  );
};

IcCertification.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string,
  circleColor: PropTypes.string
};

IcCertification.defaultProps = {
  width: "48.875",
  height: "49.53",
  viewbox: "0 0 48.875 49.53"
};

export default IcCertification;
