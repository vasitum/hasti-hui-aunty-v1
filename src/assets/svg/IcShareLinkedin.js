import React from "react";
import PropTypes from "prop-types";

const IcShareLinkedin = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      preserveAspectRatio="xMidYMid"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter id="color-overlay-1" filterUnits="userSpaceOnUse">
          <feFlood floodColor="#0077b5" />
          <feComposite operator="in" in2="SourceGraphic" />
          <feBlend in2="SourceGraphic" result="solidFill" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          d="M18.000,10.423 L18.000,17.000 L14.142,17.000 L14.142,10.863 C14.142,9.322 13.584,8.269 12.187,8.269 C11.121,8.269 10.487,8.979 10.208,9.664 C10.106,9.910 10.080,10.251 10.080,10.594 L10.080,17.000 L6.221,17.000 C6.221,17.000 6.273,6.606 6.221,5.529 L10.080,5.529 L10.080,7.155 C10.072,7.167 10.062,7.181 10.054,7.192 L10.080,7.192 L10.080,7.155 C10.592,6.375 11.508,5.260 13.557,5.260 C16.096,5.260 18.000,6.899 18.000,10.423 ZM2.184,-0.000 C0.864,-0.000 -0.000,0.856 -0.000,1.981 C-0.000,3.082 0.839,3.964 2.133,3.964 L2.158,3.964 C3.504,3.964 4.341,3.083 4.341,1.981 C4.316,0.856 3.504,-0.000 2.184,-0.000 ZM0.229,17.000 L4.087,17.000 L4.087,5.529 L0.229,5.529 L0.229,17.000 Z"
          className="cls-1"
        />
      </g>
    </svg>
  );
};

IcShareLinkedin.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

IcShareLinkedin.defaultProps = {
  width: "18",
  height: "17",
  viewbox: "0 0 18 17",
  pathcolor: "#0077b5"
};

export default IcShareLinkedin;
