
import React from "react";
import PropTypes from 'prop-types';

const IcMoreOptionIcon = props => {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width={props.width} height={props.height} viewBox={props.viewbox}>
      <defs>
        <filter id='filter' x='986' y='350.594' width='5' height='18.406' filterUnits='userSpaceOnUse'>
          <feFlood result='flood' floodColor='#c8c8c8' />
          <feComposite result='composite' operator='in' in2='SourceGraphic' />
          <feBlend result='blend' in2='SourceGraphic' />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path className='cls-1' d='M991,353.18v0.032a2.5,2.5,0,1,1-5,0V353.18A2.5,2.5,0,1,1,991,353.18Zm0,6.608v0.032a2.5,2.5,0,1,1-5,0v-0.032A2.5,2.5,0,1,1,991,359.788Zm0,6.6v0.014a2.5,2.5,0,1,1-5,0v-0.014A2.5,2.5,0,1,1,991,366.393Z'
          transform='translate(-986 -350.594)' />
      </g>
    </svg>
  );
};

IcMoreOptionIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
}

IcMoreOptionIcon.defaultProps = {
  width: '5',
  height: '18.406',
  viewbox: '0 0 5 18.406',
}

export default IcMoreOptionIcon;