import React from "react";
import PropTypes from "prop-types";

const IcYrsOfExpIcon = props => {
  return (
    
    <svg xmlns='http://www.w3.org/2000/svg' preserveAspectRatio='xMidYMid'
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter id='color-overlay-1' filterUnits='userSpaceOnUse'>
          <feFlood floodColor='#acaeb5' />
          <feComposite operator='in' in2='SourceGraphic' />
          <feBlend in2='SourceGraphic' result='solidFill' />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path d='M13.344,13.318 L11.318,12.580 L11.166,12.651 L10.429,14.678 C10.281,15.086 9.711,15.113 9.528,14.718 L8.089,11.630 C9.804,11.331 11.286,10.346 12.235,8.963 L13.953,12.652 C14.137,13.046 13.750,13.467 13.344,13.318 ZM7.000,10.753 C4.038,10.753 1.628,8.341 1.628,5.376 C1.628,2.412 4.038,-0.000 7.000,-0.000 C9.962,-0.000 12.371,2.412 12.371,5.376 C12.371,8.341 9.962,10.753 7.000,10.753 ZM9.343,4.148 L8.027,3.956 L7.438,2.762 C7.259,2.399 6.740,2.400 6.562,2.762 L5.973,3.956 L4.657,4.148 C4.257,4.206 4.097,4.699 4.386,4.981 L5.339,5.911 L5.114,7.223 C5.045,7.622 5.465,7.926 5.822,7.738 L7.000,7.119 L8.177,7.738 C8.533,7.925 8.955,7.624 8.886,7.223 L8.661,5.911 L9.613,4.981 C9.903,4.699 9.742,4.206 9.343,4.148 ZM5.911,11.630 L4.472,14.718 C4.288,15.113 3.719,15.085 3.571,14.678 L2.833,12.651 L2.681,12.580 L0.656,13.318 C0.252,13.467 -0.138,13.048 0.046,12.652 L1.765,8.963 C2.714,10.346 4.196,11.331 5.911,11.630 Z'
          className='cls-1' />
      </g>
    </svg>
  );
};

IcYrsOfExpIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

IcYrsOfExpIcon.defaultProps = {
  width: "14",
  height: "15",
  viewbox: "0 0 14 15"
};

export default IcYrsOfExpIcon;