import React from "react";
import PropTypes from "prop-types";

const IcShareTwitter = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      preserveAspectRatio="xMidYMid"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter id="color-overlay-1" filterUnits="userSpaceOnUse">
          <feFlood floodColor="#1da1f2" />
          <feComposite operator="in" in2="SourceGraphic" />
          <feBlend in2="SourceGraphic" result="solidFill" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          d="M20.891,5.005 C20.901,5.218 20.904,5.433 20.904,5.650 C20.904,12.249 16.101,19.855 7.318,19.852 C4.619,19.852 2.110,19.013 -0.005,17.570 C0.368,17.616 0.750,17.641 1.135,17.641 C3.371,17.648 5.431,16.842 7.067,15.492 C4.977,15.443 3.212,13.972 2.605,11.956 C2.896,12.018 3.195,12.051 3.503,12.053 C3.937,12.056 4.360,11.997 4.761,11.883 C2.575,11.402 0.928,9.342 0.928,6.883 C0.928,6.861 0.928,6.839 0.928,6.818 C1.573,7.205 2.308,7.441 3.092,7.476 C1.811,6.553 0.968,4.989 0.968,3.230 C0.968,2.300 1.204,1.431 1.614,0.687 C3.969,3.788 7.489,5.845 11.461,6.100 C11.378,5.730 11.337,5.346 11.337,4.952 C11.337,2.174 13.475,-0.044 16.113,-0.000 C17.487,0.022 18.729,0.651 19.599,1.636 C20.687,1.429 21.709,1.030 22.632,0.473 C22.275,1.632 21.518,2.598 20.531,3.203 C21.499,3.095 22.418,2.839 23.275,2.455 C22.634,3.447 21.825,4.312 20.891,5.005 Z"
          className="cls-1"
        />
      </g>
    </svg>
  );
};

IcShareTwitter.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

IcShareTwitter.defaultProps = {
  width: "23.281",
  height: "19.844",
  viewbox: "0 0 23.281 19.844",
  pathcolor: "#1da1f2"
};

export default IcShareTwitter;
