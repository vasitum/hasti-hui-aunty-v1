import React from "react";
import PropTypes from "prop-types";

const IcShareIcon = props => {
  return (
    // <svg
    //   xmlns="http://www.w3.org/2000/svg"
    //   width={props.width}
    //   height={props.height}
    //   viewBox={props.viewbox}>
    //   <defs>
    //     <filter
    //       id="filter"
    //       x="1485.06"
    //       y="351.844"
    //       width="15.07"
    //       height="17.156"
    //       filterUnits="userSpaceOnUse">
    //       <feFlood result="flood" floodColor="#ceeefe" />
    //       <feComposite result="composite" operator="in" in2="SourceGraphic" />
    //       <feBlend result="blend" in2="SourceGraphic" />
    //     </filter>
    //   </defs>
    //   <g fill={props.pathcolor}>
    //     <path
    //       id="Shape_39_copy_3"
    //       data-name="Shape 39 copy 3"
    //       className="cls-1"
    //       d="M1497.61,363.961a2.379,2.379,0,0,0-1.64.661l-5.97-3.574a2.316,2.316,0,0,0,0-1.207l5.9-3.541a2.474,2.474,0,0,0,1.71.7,2.585,2.585,0,1,0-2.51-2.585,3.022,3.022,0,0,0,.07.6l-5.9,3.543a2.442,2.442,0,0,0-1.7-.7,2.585,2.585,0,0,0,0,5.167,2.45,2.45,0,0,0,1.7-.7l5.96,3.582a2.652,2.652,0,0,0-.07.562A2.446,2.446,0,1,0,1497.61,363.961Z"
    //       transform="translate(-1485.06 -351.844)"
    //     />
    //   </g>
    // </svg>
    <svg
      xmlns="http://www.w3.org/2000/svg"
      preserveAspectRatio="xMidYMid"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter id="color-overlay-1" filterUnits="userSpaceOnUse">
          <feFlood floodColor="#acaeb5" />
          <feComposite operator="in" in2="SourceGraphic" />
          <feBlend in2="SourceGraphic" result="solidFill" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          d="M15.830,4.978 L11.259,0.179 C11.146,0.060 11.012,0.001 10.857,0.001 C10.702,0.001 10.568,0.060 10.455,0.179 C10.342,0.298 10.286,0.439 10.286,0.601 L10.286,3.001 L8.286,3.001 C4.041,3.001 1.437,4.260 0.473,6.778 C0.158,7.616 -0.000,8.656 -0.000,9.900 C-0.000,10.937 0.378,12.346 1.134,14.127 C1.152,14.171 1.183,14.246 1.228,14.352 C1.272,14.458 1.312,14.552 1.348,14.633 C1.384,14.714 1.423,14.783 1.464,14.839 C1.536,14.946 1.619,14.999 1.714,14.999 C1.804,14.999 1.874,14.968 1.924,14.905 C1.975,14.843 2.000,14.765 2.000,14.671 C2.000,14.615 1.993,14.532 1.978,14.423 C1.963,14.313 1.955,14.240 1.955,14.202 C1.925,13.777 1.911,13.393 1.911,13.049 C1.911,12.418 1.963,11.853 2.067,11.353 C2.171,10.853 2.315,10.420 2.500,10.055 C2.684,9.689 2.922,9.373 3.214,9.108 C3.506,8.842 3.820,8.625 4.156,8.457 C4.492,8.288 4.888,8.155 5.344,8.058 C5.799,7.961 6.257,7.894 6.719,7.857 C7.180,7.819 7.702,7.800 8.286,7.800 L10.286,7.800 L10.286,10.200 C10.286,10.362 10.342,10.503 10.455,10.622 C10.568,10.740 10.702,10.800 10.857,10.800 C11.012,10.800 11.146,10.740 11.259,10.622 L15.830,5.822 C15.943,5.704 16.000,5.563 16.000,5.401 C16.000,5.238 15.943,5.097 15.830,4.978 Z"
          className="cls-1"
        />
      </g>
    </svg>
  );
};

IcShareIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

IcShareIcon.defaultProps = {
  // width: '15.07',
  // height: '17.156',
  // viewbox: '0 0 15.07 17.156',
  width: "16",
  height: "15",
  viewbox: "0 0 16 15"
};

export default IcShareIcon;
