import React from "react";
import PropTypes from 'prop-types';

const IcUploadIcon = props => {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width={props.width} height={props.height}
      viewBox={props.viewbox}>
      <defs />
      <g fill={props.pathcolor}>
        <path className='cls-1' d='M1171.7,278h-12.4a1.3,1.3,0,0,1-1.3-1.292V274a1.3,1.3,0,0,1,1.3-1.293h3.41a0.278,0.278,0,0,1,.28.259,2.52,2.52,0,0,0,5.01,0,0.283,0.283,0,0,1,.28-0.259v0h3.42A1.3,1.3,0,0,1,1173,274v2.706A1.3,1.3,0,0,1,1171.7,278Zm-3.08-11.645a0.946,0.946,0,0,1-1.18-.144l-1.02-1.011v6.59a0.92,0.92,0,0,1-1.84,0V265.2l-1.02,1.011a0.946,0.946,0,0,1-1.18.144,0.922,0.922,0,0,1-.15-1.42l2.72-2.709a0.783,0.783,0,0,1,1.1,0l2.71,2.709A0.915,0.915,0,0,1,1168.62,266.354Z'
          transform='translate(-1158 -262.031)' />
      </g>
    </svg>
  );
};

IcUploadIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
}

IcUploadIcon.defaultProps = {
  width: '15',
  height: '15.969',
  viewbox: '0 0 15 15.969',
}

export default IcUploadIcon;