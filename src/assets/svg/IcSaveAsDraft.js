import React from "react";
import PropTypes from 'prop-types';

const IcSaveAsDraftIcon = props => {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width={props.width} height={props.height} 
    viewBox={props.viewbox}>
      <defs>
        <filter id='filter' x='873' y='3071.03' width='17' height='15.97' filterUnits='userSpaceOnUse'>
          <feFlood result='flood' floodColor='#c8c8c8' />
          <feComposite result='composite' operator='in' in2='SourceGraphic' />
          <feBlend result='blend' in2='SourceGraphic' />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path id='Shape_45_copy' data-name='Shape 45 copy' className='cls-1' d='M889.261,3076.45l-4.682-.44a0.8,0.8,0,0,1-.671-0.49l-1.677-4.02a0.8,0.8,0,0,0-1.489,0l-1.664,4.02a0.777,0.777,0,0,1-.671.49l-4.682.44a0.8,0.8,0,0,0-.456,1.4l3.528,3.07a0.786,0.786,0,0,1,.255.78l-1.06,4.31a0.8,0.8,0,0,0,1.194.87l3.9-2.27a0.849,0.849,0,0,1,.818,0l3.9,2.27a0.8,0.8,0,0,0,1.194-.87l-1.046-4.31a0.786,0.786,0,0,1,.255-0.78l3.528-3.07A0.813,0.813,0,0,0,889.261,3076.45Z'
          transform='translate(-873 -3071.03)' />
      </g>
    </svg>
  );
};

IcSaveAsDraftIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
}

IcSaveAsDraftIcon.defaultProps = {
  width: '17',
  height: '15.97',
  viewbox: '0 0 17 15.97'
}

export default IcSaveAsDraftIcon;