import React from "react";
import PropTypes from 'prop-types';

const IcRightCheckIcon = props => {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' 
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}
      >
      <defs />
      <g fill={props.pathcolor}>
        <path id='Shape_2_copy_20' data-name='Shape 2 copy 20' className='cls-1'
          d='M199.51,977.3a0.513,0.513,0,0,1,0-.737l0.73-.737a0.5,0.5,0,0,1,.729,0l0.052,0.053,1.862,2.089a0.251,0.251,0,0,0,.365,0l4.975-5.284h0.052a0.5,0.5,0,0,1,.729,0l0.729,0.737a0.512,0.512,0,0,1,0,.737h0L203.4,980.86a0.5,0.5,0,0,1-.729,0l-3.061-3.4Z'
          transform='translate(-199.344 -972.531)' />
      </g>
    </svg>
  );
};


IcRightCheckIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
}

IcRightCheckIcon.defaultProps = {
  width: '10.562',
  height: '8.5',
  viewbox: '0 0 10.562 8.5',
}

export default IcRightCheckIcon;