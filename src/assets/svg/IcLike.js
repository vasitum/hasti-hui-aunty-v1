import React from "react";
import PropTypes from 'prop-types';

const IcLikeIcon = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter
          id="filter"
          x="1111.03"
          y="353"
          width="17"
          height="16"
          filterUnits="userSpaceOnUse">
          <feFlood result="flood" floodColor="#c8c8c8" pathcolor="#c8c8c8" />
          <feComposite result="composite" operator="in" in2="SourceGraphic" />
          <feBlend result="blend" in2="SourceGraphic" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          className="cls-1"
          d="M1128,357.77c-0.24-2.766-2.18-4.773-4.6-4.773a4.582,4.582,0,0,0-3.92,2.286,4.383,4.383,0,0,0-3.82-2.286c-2.42,0-4.35,2.007-4.6,4.772a5.03,5.03,0,0,0,.14,1.814,7.846,7.846,0,0,0,2.36,3.977l5.92,5.442,6.03-5.442a7.828,7.828,0,0,0,2.35-3.977A5.025,5.025,0,0,0,1128,357.77Z"
          transform="translate(-1111.03 -353)"
        />
      </g>
    </svg>
  );
};


IcLikeIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
}

IcLikeIcon.defaultProps = {
  width: '17',
  height: '16',
  viewbox: '0 0 17 16',
}

export default IcLikeIcon;