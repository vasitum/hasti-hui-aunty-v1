import React from "react";
import PropTypes from 'prop-types';

const IcInfoIcon = props => {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width={props.width} height={props.height} 
    viewBox={props.viewbox}>
      <defs>
        <filter id='filter' x='378.844' y='548' width='16' height='16' filterUnits='userSpaceOnUse'>
          <feFlood result='flood' floodColor='#c8c8c8' />
          <feComposite result='composite' operator='in' in2='SourceGraphic' />
          <feBlend result='blend' in2='SourceGraphic' />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path className='cls-1' d='M386.831,548a8,8,0,1,0,8,8A8.025,8.025,0,0,0,386.831,548Zm0.8,12h-1.6v-1.6h1.6V560Zm0-3.2h-1.6V552h1.6v4.8Z'
          transform='translate(-378.844 -548)' />
      </g>
    </svg>
  );
};

IcInfoIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
}

IcInfoIcon.defaultProps = {
  width: '16',
  height: '16',
  viewbox: '0 0 16 16',
}

export default IcInfoIcon;