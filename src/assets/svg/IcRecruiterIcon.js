import React from "react";
import PropTypes from "prop-types";

const IcRecruiterIcon = props => {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' preserveAspectRatio='xMidYMid'
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter id='color-overlay-1' filterUnits='userSpaceOnUse'>
          <feFlood floodColor='#acaeb5' />
          <feComposite operator='in' in2='SourceGraphic' />
          <feBlend in2='SourceGraphic' result='solidFill' />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path d='M5.649,8.452 C4.289,7.538 3.357,5.749 3.357,4.130 C3.357,1.851 5.211,0.004 7.498,0.004 C9.785,0.004 11.638,1.851 11.638,4.130 C11.638,5.749 10.706,7.541 9.348,8.452 C12.592,9.271 14.996,12.077 14.996,14.159 C14.996,15.387 11.249,16.002 7.498,16.002 L9.599,13.910 L7.831,9.650 L7.849,9.650 L8.536,8.866 C8.203,8.986 7.858,9.055 7.498,9.055 C7.139,9.055 6.792,8.982 6.460,8.864 L7.148,9.648 L7.166,9.648 L5.397,13.908 L7.498,16.000 C3.749,16.000 0.000,15.387 0.000,14.157 C0.000,12.077 2.405,9.269 5.649,8.452 Z'
          className='cls-1' />
      </g>

    </svg>
    
  );
};

IcRecruiterIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string,
  rectcolor: PropTypes.string
};

IcRecruiterIcon.defaultProps = {
  width: "15",
  height: "16",
  viewbox: "0 0 15 16"
};

export default IcRecruiterIcon;