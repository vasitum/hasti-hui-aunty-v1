import React from "react";
import PropTypes from "prop-types";

const IcAttachment = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter
          id="filter"
          x="782"
          y="910"
          width="16"
          height="17"
          filterUnits="userSpaceOnUse">
          <feFlood result="flood" floodColor="#acaeb5" />
          <feComposite result="composite" operator="in" in2="SourceGraphic" />
          <feBlend result="blend" in2="SourceGraphic" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          id="attachment"
          className="cls-1"
          d="M786.2,927a4.14,4.14,0,0,1-2.971-1.244,4.315,4.315,0,0,1,0-6.028l8.69-8.815a3.081,3.081,0,0,1,4.493.133,3.189,3.189,0,0,1,.132,4.559l-8.164,8.281a1.949,1.949,0,0,1-2.782,0,2.019,2.019,0,0,1,0-2.822l5.53-5.61a0.474,0.474,0,0,1,.676,0,0.49,0.49,0,0,1,0,.685l-5.531,5.61a1.04,1.04,0,0,0,0,1.452,1,1,0,0,0,1.432,0l8.164-8.281a2.234,2.234,0,0,0-.132-3.189,2.158,2.158,0,0,0-3.143-.133l-8.69,8.815a3.337,3.337,0,0,0,0,4.658,3.224,3.224,0,0,0,4.591,0l8.69-8.816a0.473,0.473,0,0,1,.675,0,0.488,0.488,0,0,1,0,.685l-8.689,8.816A4.143,4.143,0,0,1,786.2,927Z"
          transform="translate(-782 -910)"
        />
      </g>
    </svg>
  );
};

IcAttachment.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string,
  rectcolor: PropTypes.string
};

IcAttachment.defaultProps = {
  width: "16",
  height: "17",
  viewbox: "0 0 16 17"
};

export default IcAttachment;
