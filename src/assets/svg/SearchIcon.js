import React from "react";
import PropTypes from "prop-types";

/**
 * default width: 17.093
 * default height: 15.438
 */
const SearchIcon = ({
  width,
  height,
  viewbox,
  pathcolor,
  rectcolor,
  ...props
}) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={width}
      height={height}
      viewBox={viewbox}
      {...props}>
      <defs />
      <g fill={pathcolor}>
        <path
          className="cls-1"
          d="M597.5,30.877c0.06,0.121.116,0.246,0.169,0.371a7.572,7.572,0,0,1,0,5.842,7.643,7.643,0,0,1-.494.973,7.551,7.551,0,0,1-1.1,1.413c-0.053.052-.107,0.1-0.161,0.155a7.485,7.485,0,0,1-12-8.384c0.052-.125.11-0.25,0.168-0.371a7.487,7.487,0,0,1,3.8-3.621,7.45,7.45,0,0,1,5.81,0A7.508,7.508,0,0,1,597.5,30.877Zm-2.626-.127c-0.1-.123-0.211-0.242-0.325-0.357a5.288,5.288,0,0,0-7.51,0c-0.108.106-.209,0.218-0.3,0.333-0.042.049-.082,0.1-0.122,0.149a5.311,5.311,0,1,0,8.365,0C594.94,30.833,594.905,30.792,594.871,30.75Zm2.7,7.619,2.467,1.9a1.011,1.011,0,0,1,.185,1.413,1,1,0,0,1-1.407.187l-2.507-1.931c0.038-.036.076-0.072,0.112-0.109A7.921,7.921,0,0,0,597.573,38.369Z"
          transform="translate(-583.313 -26.656)"
        />
      </g>
    </svg>
  );
};

SearchIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string,
  rectcolor: PropTypes.string
};

SearchIcon.defaultProps = {
  width: "17.093",
  height: "15.438",
  viewbox: "0 0 17.093 15.438"
};

export default SearchIcon;
