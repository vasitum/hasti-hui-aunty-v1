import React from "react";
import PropTypes from "prop-types";

const IcRaiseAwareness = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter
          id="filter"
          x="278"
          y="405"
          width="36"
          height="35"
          filterUnits="userSpaceOnUse">
          <feFlood result="flood" floodColor="#0b9ed0" />
          <feComposite result="composite" operator="in" in2="SourceGraphic" />
          <feBlend result="blend" in2="SourceGraphic" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          id="Shape_34_copy"
          data-name="Shape 34 copy"
          className="cls-1"
          d="M292.283,440.012H278V409.084h4.571V405h26.859v4.085H314v30.928H292.283Zm0.559-4.376H299.7v-4.668h-6.857v4.668Zm0-8.753H299.7v-4.668h-6.857v4.668Zm0-8.753H299.7v-4.669h-6.857v4.669Z"
          transform="translate(-278 -377.844)"
        />
      </g>
      <g fill={props.pathcolorSecond}>
        <path
          className="cls-2"
          d="M294.765,396.013l7.329,4.438-1.939-8.369,6.469-5.629-8.528-.734-3.331-7.891-3.331,7.891-8.528.734,6.469,5.629-1.939,8.369Z"
          transform="translate(-278 -377.844)"
        />
      </g>
    </svg>
  );
};

IcRaiseAwareness.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string,
  rectcolor: PropTypes.string
};

IcRaiseAwareness.defaultProps = {
  width: "36",
  height: "62.156",
  viewbox: "0 0 36 62.156"
};

export default IcRaiseAwareness;
