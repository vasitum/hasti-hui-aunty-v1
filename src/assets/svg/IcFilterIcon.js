
import React from "react";
import PropTypes from 'prop-types';

const IcFilterIcon = props => {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width={props.width} height={props.height}
      viewBox={props.viewbox}>
      <defs />
      <g fill={props.pathcolor}>
        <path className='cls-1' d='M1813.12,130.2a0.616,0.616,0,0,0-.6-0.4h-13.07a0.6,0.6,0,0,0-.6.4,0.586,0.586,0,0,0,.14.716l5.03,5.043v4.972a0.632,0.632,0,0,0,.2.46l2.61,2.619a0.612,0.612,0,0,0,.46.195,0.751,0.751,0,0,0,.26-0.052,0.612,0.612,0,0,0,.39-0.6v-7.591l5.04-5.043A0.6,0.6,0,0,0,1813.12,130.2Z'
          transform='translate(-1798.78 -129.813)' />
      </g>
    </svg>
  );
};

IcFilterIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
}

IcFilterIcon.defaultProps = {
  width: '14.41',
  height: '14.375',
  viewbox: '0 0 14.41 14.375',
}

export default IcFilterIcon;