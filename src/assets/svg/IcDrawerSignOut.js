import React from "react";
import PropTypes from "prop-types";

const IcDrawerSignOut = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter
          id="filter"
          x="1837"
          y="1474"
          width="16"
          height="17"
          filterUnits="userSpaceOnUse">
          <feFlood result="flood" floodColor="#797979" />
          <feComposite result="composite" operator="in" in2="SourceGraphic" />
          <feBlend result="blend" in2="SourceGraphic" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          className="cls-1"
          d="M1845,1491a8.107,8.107,0,0,1-3.24-15.49v2.25a6.032,6.032,0,1,0,6.01-.28v-2.16A8.1,8.1,0,0,1,1845,1491Zm-0.16-7.04c-0.87,0-.96-0.53-0.96-1.18v-7.61c0-.65.09-1.18,0.96-1.18s0.97,0.53.97,1.18v7.61C1845.81,1483.43,1845.72,1483.96,1844.84,1483.96Z"
          transform="translate(-1837 -1474)"
        />
      </g>
    </svg>
  );
};

IcDrawerSignOut.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

IcDrawerSignOut.defaultProps = {
  width: "16",
  height: "17",
  viewbox: "0 0 16 17"
};

export default IcDrawerSignOut;
