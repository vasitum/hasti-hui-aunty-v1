import React from "react";
import PropTypes from "prop-types";

const IcTempateApplication = props => {
  const { 
    width, 
    height, 
    viewbox, 
    pathcolor1, 
    pathcolor2, 
    ...resProps 
  } = props;
  return (

    <svg xmlns='http://www.w3.org/2000/svg' preserveAspectRatio='xMidYMid'
      width={width}
      height={height}
      viewBox={viewbox}
      {...resProps}
    >
      <defs />
      <g fill={pathcolor1}>
        <path d='M15.246,19.000 L0.754,19.000 C0.338,19.000 -0.000,18.667 -0.000,18.257 L-0.000,0.742 C-0.000,0.332 0.338,-0.000 0.754,-0.000 L15.246,-0.000 C15.662,-0.000 16.000,0.332 16.000,0.742 L16.000,18.257 C16.000,18.667 15.662,19.000 15.246,19.000 ZM13.333,14.183 L2.667,14.183 C2.354,14.183 2.101,14.433 2.101,14.740 C2.101,15.048 2.354,15.297 2.667,15.297 L13.333,15.297 C13.646,15.297 13.899,15.048 13.899,14.740 C13.899,14.433 13.646,14.183 13.333,14.183 Z'
          className='cls-1' />
      </g>
      <g fill={pathcolor2}>
        <path d='M4.400,11.307 C4.400,9.433 5.922,7.913 7.800,7.913 C9.678,7.913 11.200,9.433 11.200,11.307 L4.400,11.307 ZM7.800,6.976 C6.764,6.976 5.924,6.138 5.924,5.103 C5.924,4.069 6.764,3.231 7.800,3.231 C8.836,3.231 9.676,4.069 9.676,5.103 C9.676,6.138 8.836,6.976 7.800,6.976 Z'
          className='cls-2' />
      </g>
    </svg>
  );
};

IcTempateApplication.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string,
  rectcolor: PropTypes.string
};

IcTempateApplication.defaultProps = {
  width: "16",
  height: "19",
  viewbox: "0 0 16 19"
};

export default IcTempateApplication;