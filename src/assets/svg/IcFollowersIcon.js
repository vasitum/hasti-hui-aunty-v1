import React from "react";
import PropTypes from "prop-types";

const IcFollowersIcon = props => {
  return (
    
    <svg xmlns='http://www.w3.org/2000/svg' preserveAspectRatio='xMidYMid'
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs />
      <g fill={props.pathcolor1}>
        <path d='M18.526,14.000 L8.175,14.000 C7.909,14.000 7.692,13.778 7.692,13.504 C7.692,10.299 10.231,8.515 13.350,8.515 C16.470,8.515 19.009,10.299 19.009,13.504 C19.009,13.778 18.793,14.000 18.526,14.000 ZM13.350,6.318 C11.657,6.318 10.280,4.903 10.280,3.163 C10.280,1.424 11.657,0.009 13.350,0.009 C15.044,0.009 16.421,1.424 16.421,3.163 C16.421,4.903 15.044,6.318 13.350,6.318 Z'
          className='cls-1' />
      </g>
      <g fill={props.pathcolor2}>
        <path d='M-0.003,6.495 C-0.002,6.946 0.396,7.312 0.887,7.312 L5.942,7.312 L4.575,8.567 C4.205,8.907 4.186,9.442 4.534,9.761 C4.881,10.079 5.463,10.062 5.833,9.722 L8.687,7.100 C9.058,6.760 9.077,6.226 8.729,5.906 C8.719,5.899 8.709,5.893 8.700,5.885 C8.689,5.874 8.680,5.860 8.668,5.848 L5.852,3.262 C5.486,2.927 4.909,2.914 4.562,3.233 C4.214,3.552 4.229,4.083 4.594,4.419 L5.965,5.678 L0.887,5.678 C0.396,5.678 -0.003,6.043 -0.003,6.495 Z'
          className='cls-1' />
      </g>

    </svg>
  );
};

IcFollowersIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

IcFollowersIcon.defaultProps = {
  width: "19",
  height: "14",
  viewbox: "0 0 19 14"
};

export default IcFollowersIcon;