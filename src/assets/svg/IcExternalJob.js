import React from "react";
import PropTypes from "prop-types";

const IcExternalJob = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      preserveAspectRatio="xMidYMid"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter id="color-overlay-1" filterUnits="userSpaceOnUse">
          <feFlood floodColor="#99d9f9" />
          <feComposite operator="in" in2="SourceGraphic" />
          <feBlend in2="SourceGraphic" result="solidFill" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          d="M9.367,-0.002 L10.834,1.504 L0.781,1.504 C0.351,1.504 0.002,1.862 0.002,2.303 L0.002,14.205 C0.002,14.646 0.351,15.004 0.781,15.004 L12.384,15.004 C12.814,15.004 13.163,14.646 13.163,14.205 L13.163,4.012 L13.221,3.953 L14.997,5.775 L14.998,-0.002 L9.367,-0.002 ZM11.604,13.405 L1.561,13.405 L1.561,3.102 L9.892,3.102 L4.280,8.860 L6.359,10.992 L11.604,5.611 L11.604,13.405 L11.604,13.405 Z"
          className="cls-1"
        />
      </g>
    </svg>
  );
};

IcExternalJob.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

IcExternalJob.defaultProps = {
  width: "15",
  height: "15",
  viewbox: "0 0 15 15"
};

export default IcExternalJob;
