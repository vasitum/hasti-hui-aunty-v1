import React from "react";
import PropTypes from "prop-types";

const IcMissionIcon = props => {
  return (
    
    <svg xmlns='http://www.w3.org/2000/svg' viewBox={props.viewbox}>
      <defs />
      <g id='Layer_2' fill={props.pathcolor} data-name='Layer 2'>
        <path className='cls-1' d='M412.5,80.05,377.73,6.3a11,11,0,0,0-14.62-5.25L179.45,87.65a11,11,0,0,0-5.25,14.62l.15.32L87.67,143.46a11,11,0,0,0-6,12.58L6.3,191.59a11,11,0,0,0-5.25,14.62l17.57,37.27a11,11,0,0,0,14.62,5.25l75.4-35.55a11,11,0,0,0,5.11,3.79,10.74,10.74,0,0,0,3.71.65,10.94,10.94,0,0,0,4.68-1.05l30.21-14.24v2.23a11,11,0,0,0,11,11h28L110,384.24a11,11,0,0,0,5.12,14.66,10.83,10.83,0,0,0,4.76,1.1,11,11,0,0,0,9.9-6.21l66-136.85V389a11,11,0,1,0,22,0V256.94l66,136.85a11,11,0,0,0,9.9,6.21,10.86,10.86,0,0,0,4.76-1.1,11,11,0,0,0,5.12-14.66l-81.39-168.7h28a11,11,0,0,0,11-11v-41L407.26,94.67a11,11,0,0,0,5.24-14.62ZM33.8,224.18l-8.2-17.41L90.87,176l8.2,17.4ZM122.71,192,107,158.64l76.75-36.18,15.74,33.37Zm116.53,1.56H174.31V192l34.51-16.27.15.32a11,11,0,0,0,14.62,5.25l15.65-7.38Zm-15.09-36.86-.14-.31h0L198.9,103.15h0l-.15-.32L362.55,25.6,388,79.48Z'
          id='Layer_1-2' data-name='Layer 1' />
      </g>
    </svg>
  );
};

IcMissionIcon.propTypes = {
  // width: PropTypes.string,
  // height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

IcMissionIcon.defaultProps = {
  // width: "20.82",
  // height: "15.844",
  viewbox: "0 0 413.55 400"
};

export default IcMissionIcon;