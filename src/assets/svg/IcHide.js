import React from "react";
import PropTypes from "prop-types";

const IcHide = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      preserveAspectRatio="xMidYMid"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter id="color-overlay-1" filterUnits="userSpaceOnUse">
          <feFlood floodColor="#acaeb5" />
          <feComposite operator="in" in2="SourceGraphic" />
          <feBlend in2="SourceGraphic" result="solidFill" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          d="M17.898,6.747 C17.994,6.892 17.985,7.085 17.876,7.220 C17.703,7.432 13.594,12.413 8.982,12.413 C7.664,12.413 6.388,12.003 5.232,11.417 L2.885,13.855 C2.812,13.931 2.716,13.969 2.620,13.969 C2.524,13.969 2.429,13.931 2.356,13.855 C2.210,13.703 2.210,13.456 2.356,13.305 L15.081,0.082 C15.226,-0.070 15.464,-0.070 15.610,0.082 C15.756,0.234 15.756,0.480 15.610,0.632 L13.542,2.781 C16.213,4.279 17.802,6.603 17.898,6.747 ZM11.338,5.071 L7.156,9.416 C7.664,9.826 8.292,10.080 8.982,10.080 C10.634,10.080 11.976,8.685 11.976,6.969 C11.976,6.251 11.732,5.598 11.338,5.071 ZM8.982,3.857 C7.331,3.857 5.988,5.253 5.988,6.969 C5.988,7.400 6.074,7.809 6.226,8.183 L3.881,10.620 C1.702,9.169 0.192,7.347 0.089,7.220 C-0.030,7.074 -0.030,6.862 0.089,6.717 C0.262,6.505 4.371,1.524 8.982,1.524 C10.107,1.524 11.140,1.751 12.080,2.100 L10.151,4.105 C9.792,3.946 9.397,3.857 8.982,3.857 Z"
          className="cls-1"
        />
      </g>
    </svg>
  );
};

IcHide.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

IcHide.defaultProps = {
  width: "18",
  height: "13.969",
  viewbox: "0 0 18 13.969"
};

export default IcHide;
