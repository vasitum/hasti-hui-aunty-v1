import React from "react";
import PropTypes from "prop-types";

const IcEmploymentType = props => {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' preserveAspectRatio='xMidYMid'
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter id='color-overlay-1' filterUnits='userSpaceOnUse'>
          <feFlood floodColor='#acaeb5' />
          <feComposite operator='in' in2='SourceGraphic' />
          <feBlend in2='SourceGraphic' result='solidFill' />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path d='M0.003,11.639 C0.003,12.011 0.134,12.330 0.397,12.595 C0.658,12.860 0.974,12.992 1.342,12.992 L13.657,12.992 C14.025,12.992 14.341,12.860 14.602,12.595 C14.865,12.330 14.996,12.011 14.996,11.639 L14.996,3.522 C14.996,3.150 14.865,2.832 14.602,2.567 C14.341,2.302 14.025,2.170 13.657,2.170 L10.712,2.170 L10.712,0.817 C10.712,0.591 10.634,0.400 10.478,0.242 C10.322,0.084 10.132,0.005 9.909,0.005 L5.090,0.005 C4.867,0.005 4.677,0.084 4.521,0.242 C4.365,0.400 4.287,0.591 4.287,0.817 L4.287,2.170 L1.342,2.170 C0.974,2.170 0.658,2.302 0.397,2.567 C0.134,2.832 0.003,3.150 0.003,3.522 L0.003,11.639 ZM5.358,2.170 L9.641,2.170 L9.641,1.087 L5.358,1.087 L5.358,2.170 Z'
          className='cls-1' />
      </g>

      <g fill={props.pathcolor1}>
        <path d='M11.000,6.713 C11.000,6.604 10.922,6.536 10.764,6.510 L8.653,6.188 L7.706,4.181 C7.653,4.061 7.584,4.000 7.500,4.000 C7.416,4.000 7.347,4.061 7.294,4.181 L6.347,6.188 L4.235,6.510 C4.078,6.536 4.000,6.604 4.000,6.713 C4.000,6.774 4.035,6.845 4.105,6.924 L5.636,8.486 L5.274,10.691 C5.269,10.732 5.266,10.761 5.266,10.779 C5.266,10.841 5.281,10.893 5.310,10.936 C5.340,10.978 5.384,11.000 5.443,11.000 C5.493,11.000 5.549,10.982 5.611,10.947 L7.500,9.906 L9.389,10.947 C9.448,10.982 9.504,11.000 9.557,11.000 C9.613,11.000 9.656,10.978 9.686,10.936 C9.715,10.893 9.730,10.841 9.730,10.779 C9.730,10.741 9.728,10.711 9.725,10.691 L9.364,8.486 L10.891,6.924 C10.964,6.848 11.000,6.777 11.000,6.713 Z'
          className='cls-2' />
      </g>


    </svg>
    
  );
};

IcEmploymentType.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

IcEmploymentType.defaultProps = {
  width: "15",
  height: "13",
  viewbox: "0 0 15 13"
};

export default IcEmploymentType;