import React from "react";
import PropTypes from "prop-types";

const IcDownArrowIcon = props => {
  const { ...resProps } = props;
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}
      {...resProps}>
      <defs />
      <g fill={props.pathcolor}>
        <path
          className="cls-1"
          d="M1730.88,31.722l-0.6-.6A0.378,0.378,0,0,0,1730,31a0.373,0.373,0,0,0-.27.12L1725,35.846l-4.73-4.725A0.372,0.372,0,0,0,1720,31a0.377,0.377,0,0,0-.28.12l-0.6.6a0.378,0.378,0,0,0,0,.553l5.6,5.6a0.386,0.386,0,0,0,.56,0l5.6-5.6A0.379,0.379,0,0,0,1730.88,31.722Z"
          transform="translate(-1719 -31)"
        />
      </g>
    </svg>
  );
};

IcDownArrowIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

IcDownArrowIcon.defaultProps = {
  width: "12",
  height: "7",
  viewbox: "0 0 12 7"
};

export default IcDownArrowIcon;
