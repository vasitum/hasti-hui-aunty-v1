import React from "react";
import PropTypes from 'prop-types';

const IcPlusIcon = props => {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width={props.width} height={props.height} viewBox={props.viewbox} >
      <defs />
      <g fill={props.pathcolor}>
        <path className='cls-1' d='M1672,738l-5,0V733h-2v5.007l-5,0v2l5,0.015V745h2v-4.987l5-.015v-2Z'
          transform='translate(-1660 -733)' />
      </g>
    </svg>
  );
};

IcPlusIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
}

IcPlusIcon.defaultProps = {
  width: '12',
  height: '12',
  viewbox: '0 0 12 12',
}

export default IcPlusIcon;