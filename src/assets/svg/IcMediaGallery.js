import React from "react";
import PropTypes from "prop-types";

const IcMediaGallery = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter
          id="filter"
          x="449"
          y="164"
          width="20"
          height="14"
          filterUnits="userSpaceOnUse">
          <feFlood result="flood" floodColor="#0b9ed0" />
          <feComposite result="composite" operator="in" in2="SourceGraphic" />
          <feBlend result="blend" in2="SourceGraphic" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          id="Forma_1"
          data-name="Forma 1"
          className="cls-1"
          d="M468.851,178a0.134,0.134,0,0,0,.149-0.112V164.114a0.133,0.133,0,0,0-.149-0.113h-19.7a0.135,0.135,0,0,0-.149.113v13.772a0.135,0.135,0,0,0,.149.112h19.7Zm-4.624-11.664a1.223,1.223,0,1,1-1.591,1.166A1.429,1.429,0,0,1,464.227,166.334Zm-13.109,9.806,6.063-8.571,0.34-.482a0.174,0.174,0,0,1,.293,0l0.955,4.374a0.17,0.17,0,0,0,.2.032l2.1-1.511a0.356,0.356,0,0,1,.412.059l5.326,6.112c0.09,0.1.025,0.187-.134,0.187h-15.4C451.111,176.337,451.04,176.249,451.118,176.14Z"
          transform="translate(-449 -164)"
        />
      </g>
    </svg>
  );
};

IcMediaGallery.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

IcMediaGallery.defaultProps = {
  width: "20",
  height: "14",
  viewbox: "0 0 20 14"
};

export default IcMediaGallery;
