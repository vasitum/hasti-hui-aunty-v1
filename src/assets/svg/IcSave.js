import React from "react";
import PropTypes from 'prop-types';

const IcSaveIcon = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter
          id="filter"
          x="920"
          y="507"
          width="17"
          height="15"
          filterUnits="userSpaceOnUse">
          <feFlood result="flood" floodColor="#c8c8c8" />
          <feComposite result="composite" operator="in" in2="SourceGraphic" />
          <feBlend result="blend" in2="SourceGraphic" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          className="cls-1"
          d="M935.91,519.666h0l-0.3,1.452a1.1,1.1,0,0,1-1.081.882h-12.8a1.1,1.1,0,0,1-1.081-1.324l1.231-6.03a1.165,1.165,0,0,1,.784-0.874h0c0.028-.009.057-0.017,0.085-0.024h0c0.027-.007.054-0.012,0.081-0.017l0.027,0,0.064-.008c0.031,0,.063,0,0.1,0h13.421a0.555,0.555,0,0,1,.555.538,0.612,0.612,0,0,1-.011.127Zm-14.868-5.191a2.031,2.031,0,0,1,1.625-1.586v-2.363h-1.521A1.147,1.147,0,0,0,920,511.671v7.909Zm12.588-6.993v5.375h-10.1v-5.375a0.482,0.482,0,0,1,.481-0.482h9.141A0.482,0.482,0,0,1,933.63,507.482ZM931.988,510a0.894,0.894,0,0,0-.448-0.98h-6.08a1.3,1.3,0,0,0,0,1.961h6.08A0.9,0.9,0,0,0,931.988,510Z"
          transform="translate(-920 -507)"
        />
      </g>
    </svg>
  );
};

IcSaveIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
}

IcSaveIcon.defaultProps = {
  width: '17',
  height: '15',
  viewbox: '0 0 17 15',
}

export default IcSaveIcon;