import React from "react";
import PropTypes from "prop-types";

const IcChatbotIcon = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs />
      <g fill={props.pathcolor}>
        <path
          id="Ellipse_1_copy_7"
          data-name="Ellipse 1 copy 7"
          className="cls-1"
          d="M1882.08,18.084A17.916,17.916,0,1,1,1864.17,36,17.916,17.916,0,0,1,1882.08,18.084Z"
          transform="translate(-1863.16 -17.094)"
        />
      </g>

      <g fill={props.pathcolorSecond}>
        <path
          id="v"
          className="cls-2"
          d="M1883.2,41.136l3.96-10.508h-1.7c-1.29,3.579-2.13,5.9-2.5,6.965a21.806,21.806,0,0,0-.66,2.1h-0.08a26.118,26.118,0,0,0-.89-2.838l-2.25-6.232h-1.69l3.96,10.508h1.85Z"
          transform="translate(-1863.16 -17.094)"
        />
      </g>
    </svg>
  );
};

IcChatbotIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string,
  pathcolorSecond: PropTypes.string
};

IcChatbotIcon.defaultProps = {
  width: "37.84",
  height: "37.813",
  viewbox: "0 0 37.84 37.813"
};

export default IcChatbotIcon;
