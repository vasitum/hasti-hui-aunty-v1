import React from "react";
import PropTypes from 'prop-types';

const EducationNoCircleIcon = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter
          id="filter"
          x="169.281"
          y="658.063"
          width="17.032"
          height="13.875"
          filterUnits="userSpaceOnUse">
          <feFlood result="flood" floodColor="#acaeb5" />
          <feComposite
            result="composite"
            operator="in"
            in2="SourceGraphic"
          />
          <feBlend result="blend" in2="SourceGraphic" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          id="Shape_32_copy_2"
          data-name="Shape 32 copy 2"
          className="cls-1"
          d="M177.8,667.488l-8.516-4.5V662.57l8.516-4.5,8.523,4.5v0.411Zm5.565-2.119v2.663c0,0.012-.009.024-0.013,0.035l-5.12,2.706h-0.863l-5.135-2.713v-2.691l5.566,2.94Zm1.387-.746,0.674-.373v5.236l0.439,1.467-0.664.975h-0.225l-0.663-.975,0.439-1.467v-4.863Z"
          transform="translate(-169.281 -658.063)"
        />
      </g>
    </svg>
  );
};

EducationNoCircleIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
}

EducationNoCircleIcon.defaultProps = {
  width: '17.032',
  height: '13.875',
  viewbox: '0 0 17.032 13.875',
}

export default EducationNoCircleIcon;
