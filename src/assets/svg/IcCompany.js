import React from "react";
import PropTypes from "prop-types";

const IcCompanyIcon = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}
      style={props.style}>
      <defs>
        <filter
          id="filter"
          x="599.813"
          y="1142.34"
          width="25.75"
          height="24.79"
          filterUnits="userSpaceOnUse">
          <feFlood result="flood" floodColor="#acaeb5" />
          <feComposite result="composite" operator="in" in2="SourceGraphic" />
          <feBlend result="blend" in2="SourceGraphic" />
        </filter>
      </defs>
      <rect className="cls-1" width="64" height="64" fill={props.rectcolor} />
      <g fill={props.pathcolor}>
        <path
          id="Shape_34_copy"
          data-name="Shape 34 copy"
          className="cls-2"
          d="M610.02,1167.12H599.8v-21.87h3.269v-2.89h19.209v2.89h3.27v21.87H610.02Zm0.4-3.09h4.9v-3.31h-4.9v3.31Zm0-6.19h4.9v-3.31h-4.9v3.31Zm0-6.2h4.9v-3.3h-4.9v3.3Z"
          transform="translate(-581 -1123)"
        />
      </g>
    </svg>
  );
};

IcCompanyIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string,
  rectcolor: PropTypes.string
};

IcCompanyIcon.defaultProps = {
  width: "64",
  height: "64",
  viewbox: "0 0 64 64"
};

export default IcCompanyIcon;
