import React from "react";
import PropTypes from "prop-types";

const IcYrsofGradution = props => {
  return (


    <svg xmlns='http://www.w3.org/2000/svg' preserveAspectRatio='xMidYMid'
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter id='color-overlay-1' filterUnits='userSpaceOnUse'>
          <feFlood floodColor='#acaeb5' />
          <feComposite operator='in' in2='SourceGraphic' />
          <feBlend in2='SourceGraphic' result='solidFill' />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path d='M0.631,4.823 L2.532,4.823 C2.882,4.823 3.166,5.111 3.166,5.466 L3.166,8.357 C3.166,8.712 2.882,9.000 2.532,9.000 L0.631,9.000 C0.281,9.000 -0.003,8.712 -0.003,8.357 L-0.003,5.466 C-0.003,5.111 0.280,4.823 0.631,4.823 ZM5.545,2.575 L7.447,2.575 C7.797,2.575 8.081,2.863 8.081,3.217 L8.081,8.357 C8.081,8.713 7.797,9.000 7.447,9.000 L5.545,9.000 C5.195,9.000 4.911,8.713 4.911,8.357 L4.911,3.217 C4.911,2.863 5.195,2.575 5.545,2.575 ZM10.461,0.005 L12.362,0.005 C12.712,0.005 12.996,0.293 12.996,0.648 L12.996,8.357 C12.996,8.712 12.712,9.000 12.362,9.000 L10.461,9.000 C10.111,9.000 9.827,8.712 9.827,8.357 L9.827,0.647 C9.827,0.293 10.111,0.005 10.461,0.005 Z'
          className='cls-1' />
      </g>

    </svg>
  );
};

IcYrsofGradution.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

IcYrsofGradution.defaultProps = {
  width: "13",
  height: "9",
  viewbox: "0 0 13 9"
};

export default IcYrsofGradution;