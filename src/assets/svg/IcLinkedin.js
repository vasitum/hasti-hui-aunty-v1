import React from "react";
import PropTypes from "prop-types";

const IcLinkedinIcon = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      width="16"
      height="16"
      viewBox="0 0 16 16"
      className={props.className}>
      <image
        width="16"
        height="16"
        xlinkHref="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAB2lBMVEUAAAAtk8YujscrjcYpjMgmicUkiMUih8QfhMQchcQahMMXgcAPfL02k8wXgsMbhsMwjskPe78Mfb4Je8AJer4Gd7sDdrwiisgliMYFd7wAcbQag74egLsbfrobfbgYdrEWd7ATdrIRdrYOc7MLdLMJc7UGcbQAba8xkMkujsgtjcgqjMcoi8clicYjiMUghsUehcQbhMMZg8MWgcIykckwkMlKndBHnM8pi8cnisYkicYiiMUfhsQdhcQag8MYgsIVgcITgMFLndD9/v5EmMochMMXgsIUgMESf8FJndBAkMAkhcAQfsAvj8lGmctBkcEmgbgjf7chg78PfcAtjshys9r///9oqM7d7fbc7Pb0+fzB3u41kssRfsEOfcBxstpoqM/Y6vUTfsANfMAsjchnqM/o8vk1jcKpzuQ+lccMfL4rjMdvsdlmqM+01egXfLlip9FYo9ALeLtmqNCx1OgWfLpfpdBapNEJd7tusNlmqdEVfLpeptEIeLptsNllqNGw0+gUfLtdptJapdIHeLwmisY8lctnqdJmqdI0jMJjqNFhp9FGmckSe7spiMJcpdIihcMGeL0ghsMegLwbfrsZfrsWfbsUfLwRfLwPe7wMersKer0HeL0AAACpNuWtAAAAKHRSTlMAKF9eXV1cW1taWVkj/f0mX1dWVVZWVCX9/SInXl1eX15dW1taWVgjSWgebwAAAAFiS0dEAIgFHUgAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAD9SURBVBjTLcplVwIBFEXRBzNjYGJ3YeCgKCgKto6B3ddusbu7sFvs+LE+43zc6xCp1IIoSW7uHp4ajZc3kcpHnyob0tKNGZkmc5avH/lnW3Jyrba8/ILCouKSUi2p9WWKUv4zVJgrq+wMlmpFqan9H+q0JNTLDY1NzS1/Q2sACW3tQAfQ2dXd09vXzyAPAIP4bWh4JJDEUQYHMDY+AUxOMUzPALPA3PwCsLgURKKBYRlYWV0D1jeCSbJuAlsM2zsMuwy2PWAfODg8ApzHDCenZ+cXl1fXN7d3zvuHEAoNM7oen55fXt/ePz6/wiOIIqOiY2Lj4hN0usSk5BT6BrZnRwAOQMJJAAAAAElFTkSuQmCC"
      />
    </svg>
  );
};

IcLinkedinIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

IcLinkedinIcon.defaultProps = {
  width: "16",
  height: "16",
  viewbox: "0 0 16 16"
};

export default IcLinkedinIcon;
