import React from "react";
import PropTypes from "prop-types";

const ResumePreviewIcon = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      preserveAspectRatio="xMidYMid"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs />
      <path
        d="M11.837,11.077 L9.656,8.896 C10.426,7.958 10.890,6.755 10.890,5.445 C10.890,2.439 8.451,-0.000 5.445,-0.000 C2.436,-0.000 -0.000,2.439 -0.000,5.445 C-0.000,8.451 2.436,10.890 5.445,10.890 C6.755,10.890 7.955,10.428 8.894,9.658 L11.075,11.837 C11.286,12.048 11.626,12.048 11.837,11.837 C12.048,11.628 12.048,11.286 11.837,11.077 ZM5.445,9.806 C3.037,9.806 1.081,7.850 1.081,5.445 C1.081,3.040 3.037,1.081 5.445,1.081 C7.850,1.081 9.809,3.040 9.809,5.445 C9.809,7.850 7.850,9.806 5.445,9.806 Z"
        className="cls-1"
      />
      <g fill={props.pathcolor}>
        <path
          d="M7.844,4.875 L6.125,4.875 L6.125,3.156 C6.125,3.070 6.055,3.000 5.969,3.000 L5.031,3.000 C4.945,3.000 4.875,3.070 4.875,3.156 L4.875,4.875 L3.156,4.875 C3.070,4.875 3.000,4.945 3.000,5.031 L3.000,5.969 C3.000,6.055 3.070,6.125 3.156,6.125 L4.875,6.125 L4.875,7.844 C4.875,7.930 4.945,8.000 5.031,8.000 L5.969,8.000 C6.055,8.000 6.125,7.930 6.125,7.844 L6.125,6.125 L7.844,6.125 C7.930,6.125 8.000,6.055 8.000,5.969 L8.000,5.031 C8.000,4.945 7.930,4.875 7.844,4.875 Z"
          className="cls-1"
        />
      </g>
    </svg>
  );
};

ResumePreviewIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

ResumePreviewIcon.defaultProps = {
  width: "22",
  height: "18",
  viewbox: "0 0 22 18"
};

export default ResumePreviewIcon;
