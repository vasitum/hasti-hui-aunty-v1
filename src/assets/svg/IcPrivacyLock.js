import React from "react";
import PropTypes from "prop-types";

const IcPrivacyLockIcon = props => {
  return (
    <svg
      className={props.className}
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter
          id="filter"
          x="690"
          y="621"
          width="10"
          height="14"
          filterUnits="userSpaceOnUse">
          <feFlood result="flood" floodColor="#acaeb5" />
          <feComposite result="composite" operator="in" in2="SourceGraphic" />
          <feBlend result="blend" in2="SourceGraphic" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          className="cls-1"
          d="M699.6,626.614h-0.691v-1.709a3.9,3.9,0,0,0-7.81,0v1.709H690.4a0.4,0.4,0,0,0-.4.4V634.6a0.4,0.4,0,0,0,.4.4H699.6a0.4,0.4,0,0,0,.4-0.4v-7.578A0.4,0.4,0,0,0,699.6,626.614Zm-3.966,4.612v1.613a0.255,0.255,0,0,1-.255.255h-0.75a0.255,0.255,0,0,1-.255-0.255v-1.613A1.091,1.091,0,1,1,695.63,631.226Zm1.411-4.612h-4.082v-1.709a2.041,2.041,0,0,1,4.082,0v1.709Z"
          transform="translate(-690 -621)"
        />
      </g>
    </svg>
  );
};

IcPrivacyLockIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

IcPrivacyLockIcon.defaultProps = {
  width: "10",
  height: "14",
  viewbox: "0 0 10 14"
};

export default IcPrivacyLockIcon;
