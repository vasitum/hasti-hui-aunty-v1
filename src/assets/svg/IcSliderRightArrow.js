import React from "react";
import PropTypes from "prop-types";

const IcSliderRightArrow = props => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={props.viewbox}>
      <defs>
        <filter
          id="filter"
          x="1236.03"
          y="183"
          width="16"
          height="30.031"
          filterUnits="userSpaceOnUse">
          <feFlood result="flood" floodColor="#acaeb5" />
          <feComposite result="composite" operator="in" in2="SourceGraphic" />
          <feBlend result="blend" in2="SourceGraphic" />
        </filter>
      </defs>
      <g fill={props.pathcolor}>
        <path
          className="cls-1"
          d="M1249.93,198l-13.65,13.548a0.852,0.852,0,0,0,1.21,1.2l14.26-14.152a0.852,0.852,0,0,0,0-1.2l-14.26-14.146a0.843,0.843,0,0,0-1.2,0,0.835,0.835,0,0,0,0,1.2Z"
          transform="translate(-1236.03 -183)"
        />
      </g>
    </svg>
  );
};

IcSliderRightArrow.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

IcSliderRightArrow.defaultProps = {
  width: "16",
  height: "30.031",
  viewbox: "0 0 16 30.031"
};

export default IcSliderRightArrow;
