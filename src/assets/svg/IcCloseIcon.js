import React from "react";
import PropTypes from "prop-types";

const IcCloseIcon = ({ width, height, pathcolor, viewbox, ...props }) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={width}
      height={height}
      viewBox={viewbox}
      {...props}>
      <defs />
      <g fill={pathcolor}>
        <path
          id="Shape_53_copy_3"
          data-name="Shape 53 copy 3"
          className="cls-1"
          d="M1627.36,1049l4.14-4.14a1.679,1.679,0,0,0-2.37-2.38l-4.15,4.14-4.14-4.14a1.679,1.679,0,0,0-2.37,2.38l4.14,4.14-4.14,4.14a1.676,1.676,0,0,0,2.37,2.37l4.14-4.14,4.15,4.14a1.633,1.633,0,0,0,1.18.49,1.675,1.675,0,0,0,1.19-2.86Z"
          transform="translate(-1618 -1042)"
        />
      </g>
    </svg>
  );
};

IcCloseIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  viewbox: PropTypes.string,
  pathcolor: PropTypes.string
};

IcCloseIcon.defaultProps = {
  width: "14",
  height: "14",
  viewbox: "0 0 14 14"
};

export default IcCloseIcon;
