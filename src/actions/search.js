import actionTypes from "./actionTypes";

export function onInputChange(data) {
  return {
    type: actionTypes.ON_SEARCH_SUBMIT,
    payload: data
  };
}

export function onCountChange(data) {
  return {
    type: actionTypes.ON_SEARCH_COUNT_CHANGE,
    payload: data
  };
}

export function onCountJobChange(data) {
  return {
    type: actionTypes.ON_SEARCH_COUNT_JOB_CHANGE,
    payload: data
  };
}

export function onCountPeopleChange(data) {
  return {
    type: actionTypes.ON_SEARCH_COUNT_PEOPLE_CHANGE,
    payload: data
  };
}
