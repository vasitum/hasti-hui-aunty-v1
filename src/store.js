import { createStore, applyMiddleware, compose } from "redux";
import logger from "redux-logger";

import RootReducer from "./reducers";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middlewares = [logger];

export default createStore(
  RootReducer,
  {},
  composeEnhancers(applyMiddleware(...logger))
);
