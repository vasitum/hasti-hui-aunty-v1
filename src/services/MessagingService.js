import getUserById from "../api/user/getUserById";
import { USER } from "../constants/api";

const customEvent = new CustomEvent("MESSAGE");

function checkMessages() {
  const ID = window.localStorage.getItem(USER.UID);
  if (!ID || ID === "undefined") {
    return;
  }

  getUserById(ID)
    .then(res => res.json())
    .then(data => {
      const cnt = data.messageCount;
      if (cnt && cnt > 0) {
        document.dispatchEvent(customEvent);
      }
    })
    .catch(err => console.error(err));
}

export default function() {
  console.log("Messaging Service Started");
  setInterval(checkMessages, 100000);
}
