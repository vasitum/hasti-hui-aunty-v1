import fetchNotifications from "../api/notifications/notifications";
import { USER } from "../constants/api";

const notifyEvent = new CustomEvent("NOTIF");

function getFirstNotificationID(data) {
  // console.log("data is", data);
  return data[0]._id;
}

function checkNotifications() {
  const ID = window.localStorage.getItem(USER.UID);
  if (!ID || ID === "undefined") {
    return;
  }

  const lastNotificationOpened = window.localStorage.getItem(
    USER.LAST_NOTIFICATION
  );

  fetchNotifications()
    .then(data => data.json())
    .then(data => {
      if (getFirstNotificationID(data) === lastNotificationOpened) {
        return;
      }

      document.dispatchEvent(notifyEvent);
    })
    .catch(err => console.error(err));
}

export default function() {
  setInterval(checkNotifications, 100000);
}
