import React, { Component } from "react";
import { VasiButton } from "./VasiButton";
import styled, { ThemeProvider } from "styled-components";
import { Zap } from "styled-icons/octicons/Zap";

import "./index.scss";

const Wrapper = styled.section`
  margin-top: 8em;
`;
const theme = {
  colors: {
    primary: "yellow",
    secondry: "black"
  }
};

const RedZap = styled(Zap)`
  color: #e10059;
  height: ${props => props.height || "20px"};
  :hover {
    color: #fff;
  }
`;

// const Button = styled.button`
//   /* Adapt the colors based on primary prop */
//   background: ${props => props.primary ? "$secondaryColor" : "$primaryColor"};
//   color: ${props => props.primary ? `$primaryColor`: "$secondaryColor"};

//   font-size: 1em;
//   margin: 1em;
//   padding: 0.25em 1em;
//   border: 2px solid palevioletred;
//   border-radius: 3px;
// `;

export default class StyleComponents extends Component {
  render() {
    return (
      <React.Fragment>
        <ThemeProvider theme={theme}>
          <Wrapper>
            <VasiButton outline>Primary</VasiButton>
          </Wrapper>
        </ThemeProvider>
        

        {/* <Button>Normal</Button>
        <Button primary>Primary</Button> */}

      </React.Fragment>
    );
  }
}
