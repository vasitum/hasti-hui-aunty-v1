import React from "react";
import styled, { css } from "styled-components";

const style = ({ theme, ...rest }) => css`
  border-color: ${props =>
    props.contained
      ? theme.colors.primary
      : props.text
      ? "transparent"
      : props.outline
      ? theme.colors.primary
      : "transparent"};
  background-color: ${props =>
    props.contained
      ? theme.colors.primary
      : props.text
      ? "transparent"
      : props.outline
      ? "transparent"
      : "transparent"};
  color: ${props =>
    props.contained
      ? theme.colors.secondry
      : props.text
      ? theme.colors.primary
      : props.outline
      ? theme.colors.primary
      : "transparent"};
  border-width: 2px;
  border-style: solid;
  padding: 0.25em 1em;
  font-size: 1rem;
  box-sizing: border-box;
  line-height: 1.75;
  font-weight: 500;
  border-radius: ${props => (props.circle ? "50%" : "24px")};
  width: ${props => (props.circle ? "50px" : "auto")};
  height: ${props => (props.circle ? "50px" : "auto")};
  letter-spacing: 0.02857em;
  cursor: pointer;
  display: inline-flex;
  outline: none;
  position: relative;
  align-items: center;
  user-select: none;
  vertical-align: middle;
  justify-content: center;
  transition: background-color 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,
    box-shadow 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,
    border 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  :hover {
    color: ${theme.colors.secondry};
    background-color: ${theme.colors.primary};
  }
  :active {
    color: ${theme.colors.secondry};
    background-color: ${theme.colors.primary};
  }
  :focus {
    color: ${theme.colors.secondry};
    background-color: ${theme.colors.primary};
  }
`;

const ButtonBase = styled.button([style]);

export const VasiButton = ({
  onClick,
  children,
  as,
  href,
  outline,
  text,
  contained,
  circle
}) => (
  <ButtonBase
    onClick={onClick}
    as={as}
    href={href}
    outline={outline}
    text={text}
    contained={contained}
    circle={circle}>
    {children}
  </ButtonBase>
);
