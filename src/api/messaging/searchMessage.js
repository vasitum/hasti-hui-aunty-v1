import { baseUrl, USER } from "../../constants/api";

const searchMessage = (conId, text) => {
  return fetch(baseUrl + `user/chat/${conId}/tex/${text}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};

export default searchMessage;
