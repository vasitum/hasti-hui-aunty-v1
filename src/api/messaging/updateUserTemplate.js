import { baseUrl, USER } from "../../constants/api";

const ATTACHMENT_TYPES = {
  FILE: "file",
  IMG: "img"
};

function getFileExtn(filename) {
  if (!filename) {
    return null;
  }

  return filename.split(".").pop();
}

function getFileType(filename) {
  const extn = getFileExtn(filename);
  const IMG_EXTNS = ["jpg", "png", "tiff", "jpeg", "bmp", "gif"];

  if (!extn) {
    return extn;
  }

  if (IMG_EXTNS.indexOf(extn) === -1) {
    return ATTACHMENT_TYPES.FILE;
  }

  return ATTACHMENT_TYPES.IMG;
}

export default (tempName, tempText, attachment, templateType, id) => {
  let userMedia = null;
  const userId = window.localStorage.getItem(USER.UID);
  if (attachment) {
    userMedia = {
      ext: attachment.ext,
      fileName: attachment.name,
      mediaType: getFileType(attachment.name),
      size: attachment.size
    };
  }

  return fetch(baseUrl + `user/update/template`, {
    method: "PUT",
    body: JSON.stringify({
      _id: id,
      userMedia: userMedia,
      tempName: tempName,
      text: tempText,
      userId: userId,
      tempType: templateType
    }),
    headers: {
      "Content-Type": "application/json",
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};
