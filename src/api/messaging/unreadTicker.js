import { baseUrl, USER } from "../../constants/api";

const UnreadTicker = conId => {
  return fetch(
    baseUrl +
      `user/senId/${window.localStorage.getItem(USER.UID)}/chat/${conId}`,
    {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
      }
    }
  );
};

export default UnreadTicker;
