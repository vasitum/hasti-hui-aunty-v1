import { baseUrl, USER } from "../../constants/api";
import genChatId from "../../utils/messaging/genChatId";

export default (chatid, type) => {
  return fetch(baseUrl + `user/chat/${chatid}/media/${type}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};
