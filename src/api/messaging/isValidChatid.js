import { baseUrl, USER } from "../../constants/api";

export default (chatid1, chatid2) => {
  return fetch(baseUrl + "match/chatId", {
    method: "POST",
    body: JSON.stringify({
      Id: chatid1,
      secId: chatid2
    }),
    headers: {
      "Content-Type": "application/json",
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};
