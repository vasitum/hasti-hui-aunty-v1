import { baseUrl, USER } from "../../constants/api";

export default id => {
  return fetch(baseUrl + `template/${id}/delete`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};
