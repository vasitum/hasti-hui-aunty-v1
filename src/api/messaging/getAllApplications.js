import { baseUrl, USER } from "../../constants/api";

const getAllApplications = (id, reqId) => {
  return fetch(baseUrl + `job/applyinguser/${id}/recruiter/${reqId}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};

export default getAllApplications;
