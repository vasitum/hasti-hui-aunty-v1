import { baseUrl, USER } from "../../constants/api";

export default fileUploade => {
  return fetch("http://192.168.1.64/" + "v2/imageUpload ", {
    method: "POST",
    body: JSON.stringify({
      fileAttachment: {
        multipartFile: fileUploade
      }
    }),
    headers: {
      "Content-Type": "application/json",
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};
