import { baseUrl, USER } from "../../constants/api";

const msgCounter = () => {
  return fetch(
    baseUrl + `user/${window.localStorage.getItem(USER.UID)}/messageCount`,
    {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
      }
    }
  );
};

export default msgCounter;
