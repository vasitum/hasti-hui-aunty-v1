import { baseUrl, USER } from "../../constants/api";

const searchUser = (id, name) => {
  return fetch(baseUrl + `user/${id}/searchName/${name}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};

export default searchUser;
