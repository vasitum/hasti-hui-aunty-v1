import { baseUrl, USER } from "../../constants/api";

const AllUser = (chatId, page = 0) => {
  return fetch(baseUrl + `chat/${chatId}/${page}/10/history`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};

export default AllUser;
