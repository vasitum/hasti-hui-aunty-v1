import { baseUrl, USER } from "../../constants/api";
import genChatId from "../../utils/messaging/genChatId";

export default (chatid, revId, sendId, text, userMedia) => {
  let message;
  if (!userMedia) {
    message = {
      recId: revId,
      senId: sendId,
      text: text
    };
  } else {
    message = {
      recId: revId,
      senId: sendId,
      text: text,
      userMedia: userMedia
    };
  }

  return fetch(baseUrl + "user/chat", {
    method: "POST",
    body: JSON.stringify({
      chatMessage: message,
      id: chatid,
      userIds: [revId, sendId]
    }),
    headers: {
      "Content-Type": "application/json",
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};
