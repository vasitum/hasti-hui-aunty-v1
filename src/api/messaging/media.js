import { baseUrl, USER } from "../../constants/api";

export default userId => {
  return fetch(baseUrl + `user/${userId}/media`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};
