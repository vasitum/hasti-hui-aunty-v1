import { baseUrl, USER } from "../../constants/api";

export default () => {
  return fetch(
    baseUrl + `user/${window.localStorage.getItem(USER.UID)}/template`,
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
      }
    }
  );
};
