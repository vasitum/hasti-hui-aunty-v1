import { baseUrl, USER } from "../../constants/api";
// import fetchPoll from "../../utils/async/fetchWithPromise";

const ConnectAndPoll = (chatId, signal) => {
  return fetch(baseUrl + `chat/update/${chatId}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    },
    signal
  });
};

export default ConnectAndPoll;
