import { baseUrl, USER } from "../../constants/api";

export default (userId, type) => {
  let url = `user/${userId}/template`;
  if (type) {
    url = `user/${userId}/template/${type}`;
  }

  return fetch(baseUrl + url, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};
