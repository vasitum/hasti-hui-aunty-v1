import { baseUrl, USER } from "../../constants/api";

const AllUser = (page = 0) => {
  return fetch(baseUrl + `user/${window.localStorage.getItem(USER.UID)}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};

export default AllUser;
