import { baseUrl, USER } from "../../constants/api";

/**
 * @class
 * "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
 */
export default (email, searchKey) => {
  return fetch(baseUrl + `emailMe`, {
    method: "POST",
    body: JSON.stringify({
      emailId: [`${email}`],
      searchKey: searchKey
    }),
    headers: {
      "Content-Type": "application/json"
    }
  });
};
