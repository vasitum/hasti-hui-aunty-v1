import { baseUrl, USER } from "../../constants/api";

/**
 * @class
 * "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
 */
export default key => {
  return fetch(baseUrl + `autocompleteTypes/${key}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json"
    }
  });
};
