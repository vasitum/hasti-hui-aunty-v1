// const baseAuthUrl = "http://13.232.153.244:5050";
// const baseAuthUrl = "http://localhost:5050";
const baseAuthUrl = "https://vasitum.com:5050";

export function getAuthUrl(url) {
  return fetch(baseAuthUrl + "/api/authlink", {
    method: "POST",
    body: JSON.stringify({
      redirectUrl: url
    }),
    headers: {
      "Content-Type": "application/json"
    }
  });
}

export function getAuthToken(code, url) {
  return fetch(baseAuthUrl + "/api/getToken", {
    method: "POST",
    body: JSON.stringify({
      redirectUrl: url,
      code: code
    }),
    headers: {
      "Content-Type": "application/json"
    }
  });
}
