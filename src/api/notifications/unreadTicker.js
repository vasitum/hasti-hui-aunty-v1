import { baseUrl, USER } from "../../constants/api";

export default ids => {
  return fetch(baseUrl + `user/update/vbItem/Status`, {
    method: "PUT",
    body: JSON.stringify({
      ids: ids,
      userId: window.localStorage.getItem(USER.UID)
    }),
    headers: {
      "Content-Type": "application/json",
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};
