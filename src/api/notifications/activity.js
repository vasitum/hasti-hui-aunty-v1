import { baseUrl, USER } from "../../constants/api";

export default (id, page = 0, size = 20) => {
  return fetch(
    baseUrl +
      `user/${window.localStorage.getItem(
        USER.UID
      )}/activity/page/${page}/size/${size}`,
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
      }
    }
  );
};
