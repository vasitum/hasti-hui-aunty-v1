import { baseUrl, USER } from "../../constants/api";

/**
 * @class
 */

export default text => {
  return fetch(
    baseUrl +
      `user/${window.localStorage.getItem(USER.UID)}/preference/${text}`,
    {
      method: "PUT",
      headers: {
        "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
      }
    }
  );
};
