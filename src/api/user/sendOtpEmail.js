import { baseUrl, USER } from "../../constants/api";

/**
 * Fetch User By Id
 */
export default email => {
  return fetch(baseUrl + `user/sendotp/${email}`, {
    method: "POST",
    headers: {
      // "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};
