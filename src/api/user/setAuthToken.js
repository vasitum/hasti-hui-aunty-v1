import { baseUrl, USER } from "../../constants/api";

/**
 * @class
 */
export default data => {
  return fetch(baseUrl + `user/googleToken`, {
    method: "POST",
    body: JSON.stringify({
      ...data,
      client_id:
        "928872874052-03k3jcfu8b5aossap8boift1m7jm8656.apps.googleusercontent.com",
      client_key: "VSRJ18Rrl8tQxg5k-O_VK8JS",
      userId: window.localStorage.getItem(USER.UID)
    }),
    headers: {
      "Content-Type": "application/json",
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};
