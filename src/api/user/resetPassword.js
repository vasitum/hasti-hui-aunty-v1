import { baseUrl, USER } from "../../constants/api";

/**
 * Fetch User By Id
 */
export default (email, otp, password) => {
  return fetch(baseUrl + `user/pwd`, {
    method: "POST",
    body: JSON.stringify({
      email: email,
      new_password: password,
      otp: otp,
      timeInMillis: +new Date()
    }),
    headers: {
      // "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
      "Content-Type": "application/json"
    }
  });
};
