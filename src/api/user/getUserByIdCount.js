import { baseUrl, USER } from "../../constants/api";

/**
 * Fetch User By Id
 */
export default userId => {
return fetch(baseUrl + `user/${userId}/view/count`, {
    method: "PUT",
    headers: {
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};


