import { baseUrl, USER } from "../../constants/api";

export function getLikedJobs(page = 1, size = 10) {
  return fetch(baseUrl + "get/favourite/item", {
    method: "POST",
    body: JSON.stringify({
      page: page,
      size: size,
      type: "job",
      userId: window.localStorage.getItem(USER.UID)
    }),
    headers: {
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID),
      "Content-Type": "application/json"
    }
  });
}

export function getSavedJobs(page = 1, size = 10) {
  return fetch(baseUrl + "get/favourite/item", {
    method: "POST",
    body: JSON.stringify({
      favtype: 4,
      page: page,
      size: size,
      type: "job",
      userId: window.localStorage.getItem(USER.UID)
    }),
    headers: {
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID),
      "Content-Type": "application/json"
    }
  });
}

export function getLikedUser(page = 1, size = 10) {
  return fetch(baseUrl + "get/favourite/item", {
    method: "POST",
    body: JSON.stringify({
      page: page,
      size: size,
      type: "user",
      userId: window.localStorage.getItem(USER.UID)
    }),
    headers: {
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID),
      "Content-Type": "application/json"
    }
  });
}

export function getSavedUser(page = 1, size = 10) {
  return fetch(baseUrl + "get/favourite/item", {
    method: "POST",
    body: JSON.stringify({
      favtype: 4,
      page: page,
      size: size,
      type: "user",
      userId: window.localStorage.getItem(USER.UID)
    }),
    headers: {
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID),
      "Content-Type": "application/json"
    }
  });
}

export function getLikedJobsFilter(text = "") {
  return fetch(baseUrl + "user/favItem", {
    method: "POST",
    body: JSON.stringify({
      favtype: 3,
      search_Text: text,
      type: "job",
      userId: window.localStorage.getItem(USER.UID)
    }),
    headers: {
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID),
      "Content-Type": "application/json"
    }
  });
}

export function getSavedJobsFilter(text = "", tag = null) {
  return fetch(baseUrl + "user/favItem", {
    method: "POST",
    body: JSON.stringify({
      favtype: 4,
      search_Text: text,
      tag: tag ? tag : null,
      type: "job",
      userId: window.localStorage.getItem(USER.UID)
    }),
    headers: {
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID),
      "Content-Type": "application/json"
    }
  });
}

export function getLikedUserFilter(text = "") {
  return fetch(baseUrl + "user/favItem", {
    method: "POST",
    body: JSON.stringify({
      favtype: 1,
      search_Text: text,
      type: "user",
      userId: window.localStorage.getItem(USER.UID)
    }),
    headers: {
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID),
      "Content-Type": "application/json"
    }
  });
}

export function getSavedUserFilter(text = "", tag = null) {
  return fetch(baseUrl + "user/favItem", {
    method: "POST",
    body: JSON.stringify({
      favtype: 2,
      search_Text: text,
      tag: tag ? tag : null,
      type: "user",
      userId: window.localStorage.getItem(USER.UID)
    }),
    headers: {
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID),
      "Content-Type": "application/json"
    }
  });
}
