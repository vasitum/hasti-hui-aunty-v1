import { baseUrl, USER } from "../../constants/api";

/**
 * @class
 */
export default data => {
  return fetch(baseUrl + `user`, {
    method: "PUT",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json",
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};
