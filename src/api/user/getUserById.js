import { baseUrl, USER } from "../../constants/api";

/**
 * Fetch User By Id
 */
export default userId => {
return fetch(baseUrl + `view/user/${userId}`, {
    method: "GET",
    headers: {
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};
