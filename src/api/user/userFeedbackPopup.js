import { baseUrl, USER } from "../../constants/api";

/**
 * Fetch User By Id
 */
export default (status, page, size) => {
  return fetch(
    baseUrl +
      `job/jobapplication/status/${status}/user/${window.localStorage.getItem(
        USER.UID
      )}/page/${page}/size/${size}`,
    {
      method: "GET",
      headers: {
        "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
      }
    }
  );
};
