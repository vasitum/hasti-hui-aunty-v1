import { baseUrl, USER } from "../../constants/api";

/**
 * Fetch User By Id
 */
export default id => {
  return fetch(baseUrl + `user/${id}/verifyEmailOtp`, {
    method: "POST",
    headers: {
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};
