import { baseUrl, USER } from "../../constants/api";

/**
 * Fetch User By Id
 */
export default (type = "job") => {
  return fetch(
    baseUrl + `/interview/user/${window.localStorage.getItem(USER.UID)}`,
    {
      method: "GET",
      headers: {
        "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
      }
    }
  );
};
