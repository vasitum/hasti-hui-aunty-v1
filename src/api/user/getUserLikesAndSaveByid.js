import { baseUrl, USER } from "../../constants/api";

/**
 * Fetch User By Id
 */
export default ids => {
  return fetch(baseUrl + `user/like/saveJob`, {
    method: "POST",
    body: JSON.stringify({
      userId: window.localStorage.getItem(USER.UID),
      ids: ids
    }),
    headers: {
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};
