import { baseUrl, USER } from "../../constants/api";

/**
 * Fetch User By Id
 */
export default (id, otp) => {
  return fetch(baseUrl + `user/${id}/otp/${otp}/verify`, {
    method: "POST",
    headers: {
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID),
      "Content-Type": "application/json"
    }
  });
};
