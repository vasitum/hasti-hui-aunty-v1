import { baseUrl, USER } from "../../constants/api";
// const uuidv1 = require("uuid/v1");
/**
 * Fetch User By Id
 */
export default (text, iconText) => {
  const userId = window.localStorage.getItem(USER.UID);
  const authToken = window.localStorage.getItem(USER.X_AUTH_ID);
  return fetch(baseUrl + `feedback`, {
    method: "POST",
    body: JSON.stringify({
      memberId: userId,
      feedbackText: text,
      feedbackIcon: iconText,
      feedbackTime: +new Date()
    }),
    headers: {
      "X-Maven-REST-AUTH-TOKEN": authToken,
      "Content-Type": "application/json"
    }
  });
};
