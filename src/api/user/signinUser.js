import { baseUrl } from "../../constants/api";

export default data => {
  return fetch(baseUrl + "login", {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json"
    }
  });
};
