import { baseUrl, USER } from "../../constants/api";

export default data => {
  // console.log(data);
  const userId = window.localStorage.getItem(USER.UID);
  return fetch(baseUrl + `job/user/myJobs`, {
    method: "POST",
    body: JSON.stringify({
      status: "Draft",
      userId: userId
    }),
    headers: {
      "Content-Type": "application/json",
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};
