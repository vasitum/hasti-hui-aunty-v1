import { baseUrl, USER } from "../../constants/api";

/**
 * Fetch Job By Id
 */
export default jobid => {
  return fetch(baseUrl + `job/${jobid}`, {
    method: "GET",
    headers: {
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID),
      "Content-Type": "application/json"
    }
  });
};
