import { baseUrl, USER } from "../../constants/api";

export default data => {
  // console.log(data);

  return fetch(baseUrl + "job", {
    method: "PUT",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json",
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};
