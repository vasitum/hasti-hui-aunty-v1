import { baseUrl, USER } from "../../constants/api";

export default (data, jobId, application_id) => {
  // console.log(data);

  return fetch(
    baseUrl + `job/${jobId}/jobapplication/${application_id}/interview`,
    {
      method: "POST",
      body: JSON.stringify({
        ...data,
        interViewScheduleBy: "user"
      }),
      headers: {
        "Content-Type": "application/json",
        "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
      }
    }
  );
};
