import { baseUrl, USER } from "../../constants/api";

/**
 * Fetch Job By Id
 */
export default jobid => {
  return fetch(baseUrl + `view/job/${jobid}`, {
    method: "GET",
    headers: {
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};
