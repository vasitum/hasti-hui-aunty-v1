import { baseUrl, USER } from "../../constants/api";
import { startOfDay, endOfYear } from "date-fns";

export default (
  userId,
  start = startOfDay(new Date()).getTime(),
  end = endOfYear(new Date()).getTime()
) => {
  return fetch(
    baseUrl +
      `interview/user/${window.localStorage.getItem(
        USER.UID
      )}/start/${start}/end/${end}`,
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
      }
    }
  );
};
