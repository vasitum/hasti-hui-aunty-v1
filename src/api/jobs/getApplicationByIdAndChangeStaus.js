import { baseUrl, USER } from "../../constants/api";

export default (jobId, application_id) => {
  // console.log(data);
  return fetch(
    baseUrl + `job/${jobId}/jobapplication/${application_id}/chnageStatus`,
    {
      method: "GET",
      headers: {
        "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
      }
    }
  );
};
