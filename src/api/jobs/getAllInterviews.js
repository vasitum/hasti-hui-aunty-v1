import { baseUrl, USER } from "../../constants/api";

export default (jobId, application_id) => {
  return fetch(
    baseUrl + `job/${jobId}/jobapplication/${application_id}/interview`,
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
      }
    }
  );
};
