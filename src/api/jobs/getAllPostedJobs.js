import { baseUrl, USER } from "../../constants/api";

export default data => {
  // console.log(data);
  const userId = window.localStorage.getItem(USER.UID);
  return fetch(baseUrl + `user/${userId}/type/posted`, {
    method: "GET",
    headers: {
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};
