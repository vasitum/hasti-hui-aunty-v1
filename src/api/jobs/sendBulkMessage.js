import { baseUrl, USER } from "../../constants/api";

export default (ids, text) => {
  return fetch(baseUrl + `user/bulk/message`, {
    method: "POST",
    body: JSON.stringify({
      ids: ids,
      test: text,
      sendId: window.localStorage.getItem(USER.UID)
    }),
    headers: {
      "Content-Type": "application/json",
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};
