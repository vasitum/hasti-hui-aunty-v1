const PARSER_URL = "https://secret-brushlands-85205.herokuapp.com/parse";

export default url => {
  return fetch(PARSER_URL, {
    method: "POST",
    body: JSON.stringify({
      parseUrl: url
    }),
    headers: {
      "Content-Type": "application/json"
    }
  }).then(res => res.json());
};
