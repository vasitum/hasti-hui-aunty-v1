import { baseUrl, USER } from "../../constants/api";

export default (jobId, userid) => {
  // console.log(data);
  return fetch(baseUrl + `job/${jobId}/applyingUser/${userid}`, {
    method: "GET",
    headers: {
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};
