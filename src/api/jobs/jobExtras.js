/**
 * All the job related extra functions
 * will land here.
 *
 * Such as Job/user Apply  | Like | Save
 *
 * */
import { USER, baseUrl } from "../../constants/api";

export function jobApply(jobId, jobName) {
  return fetch(baseUrl + `job/${jobId}/apply`, {
    method: "POST",
    body: JSON.stringify({
      candidateCtc: Number(window.localStorage.getItem(USER.CTC)),
      candidateEmail: window.localStorage.getItem(USER.EMAIL),
      candidateExp: Number(window.localStorage.getItem(USER.EXP)),
      candidateId: window.localStorage.getItem(USER.UID),
      candidateImageExt: window.localStorage.getItem(USER.IMG_EXT),
      candidateLocation: window.localStorage.getItem(USER.LOC),
      candidateName: window.localStorage.getItem(USER.NAME),
      candidatePhone: window.localStorage.getItem(USER.PHONE),
      candidateProfileTitle: window.localStorage.getItem(USER.TITLE),
      jobName: jobName,
      pId: jobId
    }),
    headers: {
      "Content-Type": "application/json",
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
}

export function jobSave(jobid, currentLabels, type) {
  return fetch(baseUrl + `user/job/save`, {
    method: "POST",
    body: JSON.stringify({
      id: jobid,
      saveJoblabel: [...currentLabels],
      userId: window.localStorage.getItem(USER.UID),
      type: type
    }),
    headers: {
      "Content-Type": "application/json",
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
}

export function jobFollow(id) {
  return fetch(baseUrl + `user/job/follower`, {
    method: "POST",
    body: JSON.stringify({
      id: id,
      userId: window.localStorage.getItem(USER.UID),
      type: "user"
    }),
    headers: {
      "Content-Type": "application/json",
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
}

export function jobLike(id, type) {
  return fetch(baseUrl + `user/job/like`, {
    method: "POST",
    body: JSON.stringify({
      userId: window.localStorage.getItem(USER.UID),
      id: id,
      type: type
    }),
    headers: {
      "Content-Type": "application/json",
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
}

/**
 * Fetch User By Id
 */
export function getLikedAndSavedJobs(ids, type) {
  return fetch(baseUrl + `user/like/saveJob`, {
    method: "POST",
    body: JSON.stringify({
      userId: window.localStorage.getItem(USER.UID),
      ids: ids,
      type: type
    }),
    headers: {
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID),
      "Content-Type": "application/json"
    }
  });
}
