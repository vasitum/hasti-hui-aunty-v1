import { baseUrl, USER } from "../../constants/api";

export default (jobId, status) => {
  return fetch(baseUrl + `job/${jobId}/status/${status}`, {
    method: "POST",
    // body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json",
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};
