import { baseUrl, USER } from "../../constants/api";

/**
 * Fetch Job By Id
 */
export default data => {
  return fetch(baseUrl + `linkedinCallBack`, {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID),
      "Content-Type": "application/json"
    }
  });
};
