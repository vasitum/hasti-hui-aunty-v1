import { baseUrl, USER } from "../../constants/api";

/**
 * Fetch Job By Id
 */
export default (jobid, userid) => {
  return fetch(baseUrl + `chatbot/user/${userid}/job/${jobid}`, {
    method: "GET",
    headers: {
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};
