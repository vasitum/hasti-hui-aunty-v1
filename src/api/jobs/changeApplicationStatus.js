import { baseUrl, USER } from "../../constants/api";

export default (ids, status, reason = null, text = null, rejectText = null) => {
  return fetch(baseUrl + `job/jobapplication/update/applicationStatus`, {
    method: "PUT",
    body: JSON.stringify({
      ids: ids,
      rejectReson: reason,
      rejectText: rejectText,
      status: status,
      statusChangeBy: "user",
      text: text,
      userId: window.localStorage.getItem(USER.UID)
    }),
    headers: {
      "Content-Type": "application/json",
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};
