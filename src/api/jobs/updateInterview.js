import { baseUrl, USER } from "../../constants/api";

export default (data, jobId, application_id, interview_id) => {
  // console.log(data);

  return fetch(
    baseUrl +
      `job/${jobId}/jobapplication/${application_id}/interview/${interview_id}`,
    {
      method: "PUT",
      body: JSON.stringify({
        _id: interview_id,
        interViewScheduleBy: "user",
        ...data
      }),
      headers: {
        "Content-Type": "application/json",
        "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
      }
    }
  );
};
