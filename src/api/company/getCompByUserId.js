import { baseUrl, USER } from "../../constants/api";

/**
 * Fetch Job By Id
 */
export default () => {
  return fetch(
    baseUrl +
      `company/admin/${window.localStorage.getItem(USER.EMAIL)}/page/0/size/100`,
    {
      method: "GET",
      headers: {
        "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
      }
    }
  );
};
