import { baseUrl, USER } from "../../constants/api";

/**
 * Fetch Job By Id
 */
export default (id) => {
  return fetch(
    baseUrl +
      `company/${id}`,
    {
      method: "GET",
      headers: {
        "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
      }
    }
  );
};
