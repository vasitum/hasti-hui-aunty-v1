import { baseUrl, USER } from "../../constants/api";

export default (id, data) => {
  return fetch(baseUrl + `company/${id}`, {
    method: "PUT",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json",
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};
