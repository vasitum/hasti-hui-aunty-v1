import { baseUrl, USER } from "../../constants/api";

export default data => {
  return fetch(baseUrl + `company/${data}`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
      "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
    }
  });
};
