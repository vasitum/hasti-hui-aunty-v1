// Home page
export const HOMEPAGE = {
  FEATURES: {
    TITLE: "Features",
    SUBTITLE:
      "Here’s why Vasitum should be your one-stop shop for job search and recruitment",
    FIRST_FEATURE_TITLE: "Real Time Application Updates",
    FIRST_FEATURE_DESCR:
      "Tired of manually screening scores of resumes? Vasitum not only makes it easier to select quality candidates, it also digitizes interview process and automates candidate communication.",
    SECND_FEATURE_TITLE: "Smart Job Recommendation",
    SECND_FEATURE_SUBTITLE:
      "Vasitum runs on data! It understands, learns and adapts from inputs and feedback.",
    THIRD_FEATURE_TITLE: "Faster Search",
    THIRD_FEATURE_SUBTITLE:
      "When everything in the world is now just a click away, why waste time digging for the perfect candidate? Trust Vasitum to find the one in real time.",
    FORTH_FEATURE_TITLE: "Automatic Pre-screening and Interview Scheduling",
    FORTH_FEATURE_SUBTITLE:
      "Vasitum goes beyond plain keyword matching and it reduces the hiring process time by up to 75%."
  },
  RECENT_PICKS: {
    SUBTITLE: "Your ideal candidate/job is a just click away!"
  }
};

// Post job page
export const POST_JOB = {
  COVER_TITLE:
    "Vasitum hand picks the right candidate from tons of assessed profiles",
  SECTION_TOP_FEATURE_TITLE:
    "First step to hire the right candidate? Know your requirements",
  SECTION_TOP_FEATURE_FIRST: "Select from your recent job postings",
  SECTION_TOP_FEATURE_FIRST_BUBBLE:
    "Following options are based on your previous job",
  SECTION_TOP_FEATURE_SECND: "Share job posting link here",

  // Post Job Form
  JOB_TITLE_BUBBLE: "Mention specific position or at least three keywords",
  COMPANY_NAME_BUBBLE: "Mention the name of organization you’re hiring for",
  LOCATION_BUBBLE: "Mention the state/city/district",
  ROLE_BUBBLE: "Mention the role of prospective employee",
  JOB_TYPE_BUBBLE: "Mention the type of role you’re looking for",
  DESCRIPTION_PLACEHOLDER: "In 100 words or more, share your expectation for this position",
  EDUCATION_BUBBLE: "Set minimum qualification for this job"
};

export const SCREENING = {
  TITLE: "Just a few more details and we’re good to go…"
};

export const INTERVIEW_PREFERENCE = {
  HEADER: {
    HEADER_TITLE: "Choose interview preference",
    SUB_TITLE:
      "Stop rummaging through your appointment diary. Let Vasitum do the heavy-lifting for you",
    CHECKBOX_1: "Yes, help me schedule interviews",
    CHECKBOX_2: "No thanks, I’ll manage"
  },

  INTERVIEW_SCHEDUL_CARD: {
    RADIO_BTN_1: {
      TITLE: "Auto-schedule",
      CARD_TITLE:
        "Click on the Google icon below to sync your calendar with Vasitum (FYI, it’s privacy-friendly!)",
      SCHEDUL_CALENDAR: {
        TITLE: " Your Google calendar synced.",
        // SUBTITLE:
        //   "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod",
        BTN_TEXT: "Disconnect Google calendar"
      }
    },
    RADIO_BTN_2: {
      TITLE: "Manual",
      CARD_TITLE:
        "Candidates’ availability will be notified to you shortly in your inbox"
    },
    RADIO_BTN_3: {
      TITLE: "Private note",
      CARD_TITLE: "Add in the content you’d like to send"
    },
    RADIO_BTN_4: {
      TITLE: "Notify others",
      CARD_TITLE: "Add in their email id"
    },
    DROPDOWN_HEADER: {
      TITLE: "Select average interview time",
      CARD_TITLE: "Mention in a few words"
    }
  },

  APPLICATION_WORKFLOW: {
    TITLE: "Application workflow",
    SUBTITLE: "A window to your candidates' screening process.",
    QUALIFIED_CARD: {
      HEADER_TITLE: "Candidates who",
      SPAN_TITLE: ": Get scheduled for interview",
      INPUT_LABEL: "Application status will be",
      FOOTER_SUB_TITLE: 'Applications will feature under "Interview” bucket.'
    },
    DISQUALIFIED_CARD: {
      HEADER_TITLE: "Candidates who",
      SPAN_TITLE: " Failed screening",
      INPUT_LABEL: "Application status will be",
      FOOTER_SUB_TITLE: 'Applications will feature under “Rejected" bucket.'
    }
  }
};

// ReviewScreeningQuestions page
export const REVIEW_SCREENING = {
  HEADER: {
    HEADER_TITLE: "BOT SCREENING DETAILS",
    SUB_TITLE:
      "Enabling Vasitum’s pre-screening questions will allow you to cut down the time of your interview process, connecting you with qualified candidate."
  },

  REMOTE_LOCATION: {
    HEADER_TITLE: "Job location",
    SUB_TITLE: "Match job and candidate’s location"
  },

  MATCH_SKILLS: {
    HEADER_TITLE: "Skills",
    SPAN_TITLE: "(You may select up to five skills as mandatory)",
    SKILL_INPUT_LABEL: "SKILL NAME",
    EXP_INPUT_LABEL: "YEARS OF EXP."
  },

  WORK_EXP: {
    HEADER_TITLE: "Work experience",
    SUB_TITLE: "Specify required work experience in years.",
    // SPAN_TITLE: "(You may add upto 5 skills as mandatory)",
    MIN_EXP_INPUT_LABEL: "Min. experince",
    MAX_EXP_INPUT_LABEL: "Max. experince"
  },

  EDUCATION_ELIGIBILITY: {
    HEADER_TITLE: "Education qualification",
    SUB_TITLE: "Specify minimum level of qualification"
  },

  WORKING_SHIFT_PREFERENCE: {
    HEADER_TITLE: "Working shift preference",
    SUB_TITLE: "Specify work timings for this job",
    START_TIME_INPUT_LABEL: "Start Time",
    END_TIME_INPUT_LABEL: "End Timing",

    PLACEHOLDER: {
      START_TIME: "Eenter start time",
      END_TIME: "Eenter end time"
    }
  },

  JOINING_TIME: {
    HEADER_TITLE: "Joining time",
    SUB_TITLE: "How early must the candidate join?"
    // START_TIME_INPUT_LABEL: "Start Time",
    // END_TIME_INPUT_LABEL: "End Timing",

    // PLACEHOLDER:{
    //   START_TIME: "Eenter start time",
    //   END_TIME: "Eenter end time"
    // }
  },

  FOOTER_NOTE: {
    HEADER_TITLE: "Note:",
    SPAN_TITLE:
      "Applications rejected by Vasi, Vasitum’s BOT, will be shifted under the *Rejected* section in your Recruiter Dashboard. You can access these profiles any time. Rejected candidates won't be notified on their application status until we have your permission."
  }
};

// Search page
export const SEARCH_PAGE = {
  BANNER: {
    JOB:
      "Boost your recruiter visibility with Vasitum’s free personalized assistance",
    PEOPLE:
      "Hire the best candidate every time with Vasitum’s free personalized assistance"
  },
  EMAIL_ME: {
    JOB: "Email me jobs like this",
    PEOPLE: "Email me profiles like this"
  }
};

// Create profile page
export const CREATE_PROFILE = {
  PAGE_TITLE: "We just need a few details from you.",
  LINKEDIN_BANNER_BUBBLE:
    "Auto-fill with LinkedIn",

  PLACEHOLDER: {
    // SUMMARY:
    // "Write a brief description in at least 100 words mentioning your current job profile, role and responsibilities and experiences so far.",
    SUMMARY: "Write a bit about yourself in 100 plus words. This is your opportunity to tell your story. This is your moment to shine.",
    EDUCATION_LEVEL: "Describe the category of this degree"
  },

  PERSONAL_DETAILS: {
    CARD_HEADER_TITLE: {
      RIGHT_TEXT: "fields are mandatory"
    }
  },

  BUBBLE: {
    FNAME: "As per official documents.",
    LNAME: "As per official documents.",
    MOBILE: "10-digit phone number with country code",
    LOCATION: "Current residential location",
    JOB_TITLE: "Current job title, it highlights your value as candidate",
    SUMMARY:
      "Write a bit about yourself in 100 plus words. This is your opportunity to tell your story. This is your moment to shine.",

    UPLOAD_RESUME: "Upload your latest resume",
    UPLOAD_IMAGE: "Upload a high quality photo",
    SKILLS: "Mention at least three keywords highlighting your profile.",
    DEGREE: "Start with the highest degree",
    STREAM: "Mention the field of study",
    LEVEL: "Describe the category of this degree",
    START_YEAR: "Year of starting the course",
    EXPERIENCE_CURRENT_EMPLOYER: "Name of your present organization",
    EXPERIENCE_PREVIOUS_EMPLOYER: "Name of your previous organization",
    EXPERIENCE_DESIGNATION: "Mention your job title",
    EXPERIENCE_INDUSTRY: "Select your job industry from the list",
    EXPERIENCE_LOCATION: "Mention the exact geographical location",
    EXPERIENCE_JOB_PRO_DESC:
      "Let others know that you handle in your current role. What are your responsible for?  For ex: Worked on languages such as Java, C++, etc.",
    EXPERIENCE_DESCR:
      "In detail explain your job profile and role with your current employer",

    DESIRED_ROLE:
      "Mention the profile you are looking for",
    DESIRED_TYPE: "Wages may vary based on the type of job",
    DESIRED_SALARY: "Your expectation in terms of CTC",
    DESIRED_TAG:
      "Your salary expectation will be shared with the recruiters when you apply for a job.",
    CERTIFICATE_NAME: "Mention the certified course",
    CERITIFCATE_ISSUE_AUTHORITY:
      "Mention the governing body which assigned the above certificate",
    CERTIFICATE_YEAR:
      "Mention the year in which you completed the certified program"
  }
};

// Create company page
export const CREATE_COMPANY = {
  MODAL: {
    HEADER:
      "By creating this page, I concede that I have the rights to act on behalf of this company.",
    NAME: "Mention the registered company name",
    INDUSTRY: "Mention the field of industry on basis of goods and services",
    WEBSITE: "Share company’s official website link",
    LOCATION: "Mention the exact location of the organization",
    ABOUT: "Share a description of the company in 250 plus words.",
    LOGO: "Please share only verified logos",
    LOGO_IMP: "Supported formats PNG and JPEG, Preferred dimensions 200x200",
    SIZE: "Select the specific employee range of the organization",
    TYPE: "Select the suitable option that categorizes your company",
    YEAR: "Mention the year of establishment",
    NOTES: "Add identification notes, if any."
  },
  PAGE: {
    MAST_1: "",
    MAST_2: "",
    MAST_3: ""
  }
};

// Edit job page
export const EDIT_JOB = {
  PAGE_TITLE:
    "Missed out an important detail? No worries, you can fix it any time!"
};

export const DESIRED_JOB = {
  PAGE_TITLE1:
    "Tell us what you’re looking for. The more you share, the easiest it will be for us to match you with the  ideal job.",
  PAGE_TITLE2:
    "Highlight your expectations regarding the job role, type of employment, salary etc., and we'll search the closest match for you."
};

// Manage company page
export const MANAGE_COMPANY = {
  HEADER_TITLE: "Manage your companies",

  CARD_1: {
    HEADER_TITLE: "Awareness",
    DESC:
      "Short on time and resources? Build a unique template and save valuable time in your day."
  },
  CARD_2: {
    HEADER_TITLE: "Promote",
    DESC: "Be a magnet for top talent. Hire only the best for the BEST."
  },
  CARD_3: {
    HEADER_TITLE: "Strengthen",
    DESC:
      "Build a trustworthy relationship with engaging content and career opportunities."
  }
};

// Manage template page
export const MANAGE_TEMPLATE = {
  HEADER_TITLE: "Manage your templates",
  CARD_1: {
    HEADER_TITLE: "Create Your Own Template",
    DESC:
      "Short on time and resources? Build a unique template and save valuable time in your day."
  },
  CARD_2: {
    HEADER_TITLE: "Modify Your Template",
    DESC:
      "Simply edit and send away personalized emails to as many job seekers/recruiters."
  },
  CARD_3: {
    HEADER_TITLE: "Benefits of Message Templates",
    DESC:
      "A well-designed email template can save time, remain consistent & personalize communication."
  }
};

// Manage resume template page
export const MANAGE_RESUME_TEMPLATE = {
  HEADER_TITLE: "Pick your resume template",
  CARD_1: {
    HEADER_TITLE: "Create Your Own Template",
    DESC:
      "Short on time and resources? Build a unique template and save valuable time in your day."
  },
  CARD_2: {
    HEADER_TITLE: "Modify Your Template",
    DESC:
      "Simply edit and send away personalized emails to as many job seekers/recruiters."
  },
  CARD_3: {
    HEADER_TITLE: "Benefits of Message Templates",
    DESC:
      "A well-designed email template can save time, remain consistent & personalize communication."
  }
};


// About us Page
export const ABOUT_US_PAGE = {
  HEADER_TITLE: "About Us",
  SUB_TITLE: "You don’t hire characters. You hire people. Real people. However, sometimes it’s hard to find them and other times it’s hard to find the job that sparks a dream.",
  BODY_SECTION_TITLE:{
    TITLE_1: "Vasitum, an AI-based Platform, is focused on solving the challenges of recruiters and job seekers alike. Founded from almost two decades’ worth of experience in Recruitment, Vasitum was launched by Vikram Wadhawan (Founder & CEO), in January 2019.",
    TITLE_2: "Vikram and his team’s background in recruiting and software development served as the backbone. Together, they built a single solution as the most efficient career platform, Vasitum, providing transparency and efficiency with minimal effort from both candidates and employers.",
    TITLE_3: "Meaning ‘infinite’ in Sanskrit, Vasitum fills all the gaps throughout the entire recruitment process. The platform was created to foster everyone’s ability to reach their full potential.",
    TITLE_4: "At Vasitum, we come to work every day because we know there’s a better way to find people and find career for better tomorrow. We’re innovators, encouragers, and go-getters on a mission to bring efficiency and transparency in Recruitment while enabling technology.",
  },

  BODY_SECTION_CARD: {

    CARD_1: {
      HEAD_TITLE: "Mission",
      SUB_TITLE: "To be the most efficient career platform"
    },

    CARD_2: {
      HEAD_TITLE: "Vision",
      SUB_TITLE: "Provide transparency and increase efficiency within the hiring process via minimal effort"
    },
    
    CARD_3: {
      HEAD_TITLE: "Values",
      SUB_TITLE: "Confident, Inclusive, Transparent, Efficient"
    }

  }
};

// PrivacyPolicy Page
export const PrivacyPolicy = {
  HEADER_TITLE: "Privacy Policy",
  MAIN_TITLE1:
    "Vasitum Inc. ('us', 'we', or 'our') operates the www.vasitum.com website and the Vasitum mobile application (hereinafter referred to as the 'Service').",
  MAIN_TITLE2:
    "This page informs you of our policies regarding the collection, use and disclosure of personal data when you use our Service and the choices you have associated with that data.",
  MAIN_TITLE3:
    "We use your data to provide and improve the Service. By using the Service, you agree to the collection and use of information in accordance with this policy. Unless otherwise defined in this Privacy Policy, the terms used in this Privacy Policy have the same meanings as in our Terms and Conditions.",
  DEFINITION: {
    HEADER_TITLE: "Definition",
    SERVICE:
      "Service means the www.vasitum.com website and the Vasitum mobile application operated by Vasitum Inc.",
    PERSONAL_DATA:
      "Personal Data means data about a living individual who can be identified from those data (or from those and other information either in our possession or likely to come into our possession).",
    USAGE_DATA:
      "Usage Data is data collected automatically either generated by the use of the Service or from the Service infrastructure itself (for example, the duration of a page visit).",
    COOKIES:
      "Cookies are small files stored on your device (computer or mobile device).",
    DATA_CONTROLLER:
      "Data Controller means the natural or legal person who (either alone or jointly or in common with other persons) determines the purposes for which and the manner in which any personal information are, or are to be, processed. For the purpose of this Privacy Policy, we are a Data Controller of your Personal Data.",
    DATA_PROCESSOR:
      "Data Processor (or Service Provider) means any natural or legal person who processes the data on behalf of the Data Controller. We may use the services of various Service Providers in order to process your data more effectively.",
    DATA_SUBJECT:
      "Data Subject is any living individual who is using our Service and is the subject of Personal Data."
  },
  INFO_COLLECTION: {
    HEADING: "Information Collection and Use",
    MAIN:
      "We collect several different types of information for various purposes to provide and improve our Service to you.",
    SUB_HEADING: "Types of Data Collected",
    PERSONAL_DATA: {
      HEADING: "Personal Data",
      MAIN:
        "While using our Service, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you ('Personal Data'). Personally identifiable information may Inc.lude, but is not limited to:",
      A: "Email address",
      B: "First name and last name",
      C: "Phone number",
      D: "Address, State, ProvInc.e, ZIP/Postal code, City",
      E: "Date of Birth",
      F: "Job Title",
      G: "Cookies and Usage Data",
      H: "Financial Information",
      I: "Demographic Information such as postcode, preferences and interest",
      MAIN1:
        "We may use your Personal Data to contact you with newsletters, marketing or promotional materials and other information that may be of interest to you. You may opt out of receiving any, or all, of these communications from us by following the unsubscribe link or instructions provided in any email we send or by contacting us."
    },
    USAGE_DATA: {
      HEADING: "Usage Data",
      MAIN_1:
        "We may also collect information that your browser sends whenever you visit our Service or when you access the Service by or through a mobile device ('Usage Data').",
      MAIN_2:
        "This Usage Data may Inc.lude information such as your computer's Internet Protocol address (e.g. IP address), browser type, browser version, the pages of our Service that you visit, the time and date of your visit, the time spent on those pages, unique device identifiers and other diagnostic data.",
      MAIN_3:
        "When you access the Service with a mobile device, this Usage Data may Inc.lude information such as the type of mobile device you use, your mobile device unique ID, the IP address of your mobile device, your mobile operating system, the type of mobile Internet browser you use, unique device identifiers and other diagnostic data."
    },
    LOCATION_DATA: {
      HEADING: "Location Data",
      MAIN_1:
        "We may use and store information about your location if you give us permission to do so ('Location Data'). We use this data to provide features of our Service, to improve and customise our Service.",
      MAIN_2:
        "You can enable or disable location services when you use our Service at any time by way of your device settings."
    },
    TRACKING_DATA: {
      HEADING: "Tracking Data",
      MAIN_1:
        "We use cookies and similar tracking technologies to track the activity on our Service and we hold certain information.",
      MAIN_2:
        "Cookies are files with a small amount of data which may Inc.lude an anonymous unique identifier. Cookies are sent to your browser from a website and stored on your device. Other tracking technologies are also used such as beacons, tags and scripts to collect and track information and to improve and analyse our Service.",
      MAIN_3:
        "You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. However, if you do not accept cookies, you may not be able to use some portions of our Service.",
      MAIN_4: "Examples of Cookies we use:",
      A: "Session Cookies. We use Session Cookies to operate our Service.",
      B:
        "Preference Cookies. We use Preference Cookies to remember your preferences and various settings.",
      C: "Security Cookies. We use Security Cookies for security purposes."
    },
    USE_OF_DATA: {
      HEADING: "Use of Data",
      MAIN_1: "Vasitum Inc. uses the collected data for various purposes:",
      A: "To provide and maintain our Service",
      B: "To notify you about changes to our Service",
      C:
        "To allow you to participate in interactive features of our Service when you choose to do so",
      D: "To provide customer support",
      E:
        "To gather analysis or valuable information so that we can improve our Service",
      F: "To monitor the usage of our Service",
      G: "To detect, prevent and address technical issues",
      H:
        "To provide you with news, special offers and general information about other goods, services and events which we offer that are similar to those that you have already purchased or enquired about unless you have opted not to receive such information"
    },
    LEGAL_DATA: {
      HEADING:
        "Legal Basis for Processing Personal Data under the General Data Protection Regulation (GDPR)",
      MAIN_1:
        "If you are from the European Economic Area (EEA), Vasitum Inc. legal basis for collecting and using the personal information described in this Privacy Policy depends on the Personal Data we collect and the specific context in which we collect it.",
      MAIN_2: "Vasitum Inc. may process your Personal Data because:",
      A: "We need to perform a contract with you",
      B: "You have given us permission to do so",
      C:
        "The processing is in our legitimate interests and it is not overridden by your rights",
      D: "For payment processing purposes",
      E: "To comply with the law"
    },
    RETAIN_DATA: {
      HEADING: "Retention of Data",
      MAIN_1:
        "Vasitum Inc. will retain your Personal Data only for as long as it’s necessary for the purposes set out in this Privacy Policy. We will retain and use your Personal Data to the extent necessary to comply with our legal obligations (for example, if we are required to retain your data to comply with applicable laws), resolve disputes and enforce our legal agreements and policies.",
      MAIN_2:
        "Vasitum Inc. will also retain Usage Data for internal analysis purposes. Usage Data is generally retained for a shorter period of time, except when this data is used to strengthen the security or to improve the functionality of our Service, or we are legally obligated to retain this data for longer periods."
    },
    TRANSFER_DATA: {
      HEADING: "Transfer of Data",
      MAIN_1:
        "Your information, Inc.luding Personal Data, may be transferred to — and maintained on — computers located outside of your state, provInc.e, country or other governmental jurisdiction where the data protection laws may differ from those of your jurisdiction.",
      MAIN_2:
        "If you are located outside United States and choose to provide information to us, please note that we transfer the data, Inc.luding Personal Data, to United States and process it there.",
      MAIN_3:
        "Your consent to this Privacy Policy followed by your submission of such information represents your agreement to that transfer.",
      MAIN_4:
        "Vasitum Inc. will take all the steps reasonably necessary to ensure that your data is treated securely and in accordance with this Privacy Policy and no transfer of your Personal Data will take place to an organisation or a country unless there are adequate controls in place Inc.luding the security of your data and other personal information."
    },
    DISCLOSURE_DATA: {
      HEADING: "Disclosure of Data",
      BUSINESS_TRANS: {
        HEADING: "Business Transaction",
        MAIN:
          "If Vasitum Inc. is involved in a merger, acquisition or asset sale, your Personal Data may be transferred. We will, however, duly notify you before transferring your Personal Data,which (thereon) becomes subject to a different Privacy Policy."
      },
      LAW_ENFOR: {
        HEADING: "Disclosure for Law Enforcement",
        MAIN:
          "Under certain circumstances, Vasitum Inc.. may be required to disclose your Personal Data if required to do so by law or in response to valid requests by public authorities (e.g. a court or a government agency)."
      },
      LEGAL_ENFOR: {
        HEADING: "Legal Requirements",
        MAIN:
          "Vasitum Inc. may disclose your Personal Data in the good faith belief that such action is necessary to:",
        A: "To comply with a legal obligation",
        B: "To protect and defend the rights or property of Vasitum Inc.",
        C:
          "To prevent or investigate possible wrongdoing in connection with the Service",
        D:
          "To protect the personal safety of users of the Service or the public",
        E: "To protect against legal liability"
      }
    },
    SECURE_DATA: {
      HEADING: "Security of Data",
      MAIN:
        "The security of your data is important to us but remember that no method of transmission over the Internet or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal Data, we cannot guarantee its absolute security."
    }
  },
  CALOPPA: {
    HEADING:
      "Our Policy on 'Do Not Track' Signals under the California Online Protection Act (CalOPPA)",
    MAIN_1:
      "We do not support Do Not Track ('DNT'). Do Not Track is a preference you can set in your web browser to inform websites that you do not want to be tracked.",
    MAIN_2:
      "You can enable or disable Do Not Track by visiting the Preferences or Settings page of your web browser."
  },
  GDPR: {
    HEADING:
      "Your Data Protection Rights under the General Data Protection Regulation (GDPR)",
    MAIN_1:
      "If you are a resident of the European Economic Area (EEA), you have certain data protection rights. Vasitum Inc. aims to take reasonable steps to allow you to correct, amend, delete or limit the use of your Personal Data.",
    MAIN_2:
      "If you wish to be informed about what Personal Data we hold about you and if you want it to be removed from our systems, please contact us.",
    MAIN_3:
      "In certain circumstances, you have the following data protection rights:",
    SUB_MAIN_3: {
      HEADING1:
        "The right to access, update or delete the information we have on you : ",
      CONTENT1:
        "Whenever made possible, you can access, update or request deletion of your Personal Data directly within your account settings section. If you are unable to perform these actions yourself, please contact us to assist you.",
      HEADING2: "The right of rectification : ",
      CONTENT2:
        "You have the right to have your information rectified if that information is inaccurate or incomplete.",
      HEADING3: "The right to object : ",
      CONTENT3:
        "You have the right to object to our processing of your Personal Data.",
      HEADING4: "The right of restriction : ",
      CONTENT4:
        "You have the right to request that we restrict the processing of your personal information.",
      HEADING5: "The right to data portability : ",
      CONTENT5:
        "You have the right to be provided with a copy of the information we have on you in a structured, machine-readable and commonly used format.",
      HEADING6: "The right to withdraw consent : ",
      CONTENT6:
        "You also have the right to withdraw your consent at any time where Vasitum Inc. relied on your consent to process your personal information."
    },
    MAIN_4:
      "Please note that we may ask you to verify your identity before responding to such requests.",
    MAIN_5:
      "You have the right to complain to a Data Protection Authority about our collection and use of your Personal Data. For more information, please contact your local data protection authority in the European Economic Area (EEA)."
  },
  SERVICE_PROVIDER: {
    HEADING: "Service Providers",
    MAIN_1:
      "We may employ third party companies and individuals to facilitate our Service ('Service Providers'), provide the Service on our behalf, perform Service-related services or assist us in analysing how our Service is used.",
    MAIN_2:
      "These third parties have access to your Personal Data only to perform these tasks on our behalf and are obligated not to disclose or use it for any other purpose.",
    ANALYTIC: {
      HEADING: "Analytics",
      MAIN_1:
        "We may use third-party Service Providers to monitor and analyse the use of our Service.",
      SUB_HEADING_1: "Google Analytics",
      SUB_HE_CONTENT_1:
        "Google Analytics is a web analytics service offered by Google that tracks and reports website traffic. Google uses the data collected to track and monitor the use of our Service. This data is shared with other Google services. Google may use the collected data to contextualise and personalise the ads of its own advertising network. For more information on the privacy practices of Google, please visit the Google Privacy Terms web page: ",
      SUB_HE_CO_LINK1: " https://policies.google.com/privacy?hl=en",
      SUB_HEADING_2: "Firebase",
      SUB_HE_CONTENT_2:
        "Firebase is analytics service provided by Google Inc. You may opt-out of certain Firebase features through your mobile device settings, such as your device advertising settings or by following the instructions provided by Google in their Privacy Policy: ",

      SUB_HE_CO2_LINK1: "https://policies.google.com/privacy?hl=en",
      SUB_HE_CONTENT_21:
        ". We also encourage you to review the Google's policy for safeguarding your data: ",
      SUB_HE_CO2_LINK2: " https://support.google.com/analytics/answer/6004245.",
      SUB_HE_CONTENT_23:
        ". For more information on what type of information Firebase collects, please visit the Google Privacy Terms web page: ",
      SUB_HE_CO2_LINK3: " https://policies.google.com/privacy?hl=en",
      SUB_HEADING_3: "Statcounter",
      SUB_HE_CONTENT_3:
        "Statcounter is a web traffic analysis tool. You can read the Privacy Policy for Statcounter here: ",
      SUB_HE_CO3_LINK1: " https://statcounter.com/about/legal/",
      SUB_HEADING_4: "Flurry Analytics",
      SUB_HE_CONTENT_4:
        "Flurry Analytics service is provided by Yahoo! Inc. You can opt-out from Flurry Analytics service to prevent Flurry Analytics from using and sharing your information by visiting the Flurry's Opt-out page: ",

      SUB_HE_CO4_LINK1: " https://dev.flurry.com/secure/optOut.do",
      SUB_HE_CONTENT_41:
        ". For more information on the privacy practices and policies of Yahoo!, please visit their Privacy Policy page: ",
      SUB_HE_CO4_LINK2:
        " https://policies.yahoo.com/us/en/yahoo/privacy/policy/index.htm",
      SUB_HEADING_5: "Mixpanel",
      SUB_HE_CONTENT_5:
        "Mixpanel is provided by Mixpanel Inc. You can prevent Mixpanel from using your information for analytics purposes by opting-out. To opt-out of Mixpanel service, please visit this page: ",

      SUB_HE_CO5_LINK1: " https://mixpanel.com/optout/",
      SUB_HE_CONTENT_51:
        ". For more information on what type of information Mixpanel collects, please visit the Terms of Use page of Mixpanel: ",
      SUB_HE_CO5_LINK2: " https://mixpanel.com/terms/",
      SUB_HEADING_6: "Unity Analytics",
      SUB_HE_CONTENT_6:
        "Unity Analytics is provided by Unity Technologies. For more information on what type of information Unity Analytics collects, please visit their Privacy Policy page: ",
      SUB_HE_CO6_LINK1: " https://unity3d.com/legal/privacy-policy"
    },
    BREMARKETING: {
      HEADING: "Behavioral Remarketing",
      MAIN_1:
        "Vasitum Inc. uses remarketing services to advertise on third party websites to you after you visited our Service. We and our third-party vendors use cookies to inform, optimise and serve ads based on your past visits to our Service.",
      SUB_HEADING_1: "Google Ads (AdWords)",
      SUB_HE_CONTENT_1:
        "Google Ads (AdWords) remarketing service is provided by Google Inc. You can opt-out of Google Analytics for Display Advertising and customise the Google Display Network ads by visiting the Google Ads Settings page: ",

      SUB_HE_CO1_LINK1: " http://www.google.com/settings/ads",
      SUB_HE_CONTENT_11:
        ". Google also recommends installing the Google Analytics Opt-out Browser Add-on - ",
      SUB_HE_CO1_LINK2: "https://tools.google.com/dlpage/gaoptout",
      SUB_HE_CONTENT_12:
        " - for your web browser. Google Analytics Opt-out Browser Add-on provides visitors with the ability to prevent their data from being collected and used by Google Analytics. Privacy Terms web page: ",
      SUB_HE_CO1_LINK3: " https://policies.google.com/privacy?hl=en",
      SUB_HEADING_2: "Twitter",
      SUB_HE_CONTENT_2:
        "Twitter remarketing service is provided by Twitter Inc.You can opt-out from Twitter's interest-based ads by following their instructions: ",

      SUB_HE_CO2_LINK1: " https://support.twitter.com/articles/20170405",
      SUB_HE_CONTENT_21:
        "You can learn more about the privacy practices and policies of Twitter by visiting their Privacy Policy page: ",
      SUB_HE_CO2_LINK2: " https://twitter.com/privacy",
      SUB_HE_CONTENT_22:
        ". LinkedIn LinkedIn remarketing services is provided by LinkedIn Corporation You can opt out from LinkedIn’s internet based act by visiting this page ",
      SUB_HE_CO1_LINK3:
        "https://www.linkedin.com/help/linkedin/answer/68763?trk=microsites-frontend_legal_privacy-policy&amp;lang=en",
      SUB_HE_CONTENT_23:
        "You can learn about the privacy policy of LinkedIn by visiting this page ",
      SUB_HE_CO1_LINK4: " https://www.linkedin.com/legal/privacy-policy",

      SUB_HEADING_3: "YouTube",
      SUB_HE_CONTENT_3:
        "YouTube remarketing services is provided by YouTube LLC You can opt out from YouTube’s internet based act by visiting this page ",

      SUB_HE_CO3_LINK1:
        " https://support.google.com/google-ads/answer/2545661?hl=en ",

      SUB_HE_CO3_LINK2:
        " https://support.google.com/youtube/answer/7671399?p=privacy_guidelines&amp;hl=en&amp;visit _id=636803637041568206-3170965388&amp;rd=1",
      SUB_HE_CONTENT_31:
        "You can learn about the privacy policy of YouTube by visiting this page ",
      SUB_HE_CO3_LINK3: " https://policies.google.com/privacy?hl=en",
      SUB_HEADING_4: "Facebook",
      SUB_HE_CONTENT_4:
        "Facebook remarketing service is provided by Facebook Inc. You can learn more about interest-based advertising from Facebook by visiting this page: ",

      SUB_HE_CO4_LINK1: " https://www.facebook.com/help/164968693837950 ",
      SUB_HE_CONTENT_41:
        ". To opt-out from Facebook's interest-based ads, follow these instructions from Facebook: ",
      SUB_HE_CO4_LINK2: " http://www.aboutads.info/choices/ ",

      SUB_HE_CONTENT_42:
        "Facebook adheres to the Self-Regulatory Principles for Online Behavioural Advertising established by the Digital Advertising Alliance. You can also opt-out from Facebook and other participating companies through the Digital Advertising Alliance in the USA ",
      SUB_HE_CO4_LINK3: " https://www.facebook.com/help/568137493302217",

      SUB_HE_CONTENT_43:
        ", the Digital Advertising Alliance of Canada in Canada ",
      SUB_HE_CO4_LINK4: " http://youradchoices.ca/",

      SUB_HE_CONTENT_44:
        " or the European Interactive Digital Advertising Alliance in Europe ",
      SUB_HE_CO4_LINK5: " http://www.youronlinechoices.eu/",
      SUB_HE_CONTENT_45:
        ", or opt- out using your mobile device settings. For more information on the privacy practices of Facebook, please visit Facebook's Data Policy: ",
      SUB_HE_CO4_LINK6: " https://www.facebook.com/privacy/explanation",

      SUB_HEADING_5: "Pinterest",
      SUB_HE_CONTENT_5:
        "Pinterest remarketing service is provided by Pinterest Inc. You can opt-out from Pinterest's interest-based ads by enabling the 'Do Not Track' functionality of your web browser or by following Pinterest instructions: ",

      SUB_HE_CO5_LINK1:
        " http://help.pinterest.com/en/articles/personalization-and-data ",
      SUB_HE_CONTENT_51: "their Privacy Policy page: ",
      SUB_HE_CO5_LINK2: " https://about.pinterest.com/en/privacy-policy"
    },
    PAYMENT: {
      HEADING: "Payments",
      MAIN_1:
        "We may provide paid products and/or services within the Service. In that case, we use third-party services for payment processing (e.g. payment processors).",
      MAIN_2:
        "We will not store or collect your payment card details. That information is provided directly to our third-party payment processors whose use of your personal information is governed by their Privacy Policy. These payment processors adhere to the standards set by PCI-DSS as managed by the PCI Security Standards Council, which is a joint effort of brands like Visa, MasterCard, American Express and Discover. PCI-DSS requirements help ensure the secure handling of payment information.",
      MAIN_3: "The payment processors we work with are:",
      SUB_HEADING_1: "Mastercard",
      SUB_HE_CONTENT_1: "Their Privacy Policy can be viewed at ",
      link1:
        "https://www.mastercard.com/en-ke/about- mastercard/what-we-do/privacy.html",
      SUB_HEADING_2: "Visa",
      SUB_HE_CONTENT_2: "Their Privacy Policy can be viewed at ",
      link2: "https://usa.visa.com/legal/global-privacy-notice.html",
      SUB_HEADING_3: "American Express",
      SUB_HE_CONTENT_3: "Their Privacy Policy can be viewed at ",
      link3:
        "https://www.americanexpress.com/us/legal- disclosures/privacy-center.html",
      SUB_HEADING_4: "Apple Store In-App Payments",
      SUB_HE_CONTENT_4: "Their Privacy Policy can be viewed at ",
      link4: "https://www.apple.com/legal/privacy/en-ww/",
      SUB_HEADING_5: "Google Play In-App Payments",
      SUB_HE_CONTENT_5: "Their Privacy Policy can be viewed at ",
      link5: "https://www.google.com/policies/privacy/",
      SUB_HEADING_6: "Stripe",
      SUB_HE_CONTENT_6: "Their Privacy Policy can be viewed at ",
      link6: "https://stripe.com/us/privacy",
      SUB_HEADING_7: "PayPal / Braintree",
      SUB_HE_CONTENT_7: "Their Privacy Policy can be viewed at ",
      link7: "https://www.paypal.com/webapps/mpp/ua/privacy-full",
      SUB_HEADING_8: "FastSpring",
      SUB_HE_CONTENT_8: "Their Privacy Policy can be viewed at ",
      link8: "http://fastspring.com/privacy/",
      SUB_HEADING_9: "Authorize.net",
      SUB_HE_CONTENT_9: "Their Privacy Policy can be viewed at ",
      link9: "https://www.authorize.net/company/privacy/",
      SUB_HEADING_10: "2Checkout",
      SUB_HE_CONTENT_10: "Their Privacy Policy can be viewed at ",
      link10: "https://www.2checkout.com/policies/privacy-policy",
      SUB_HEADING_11: "Sage Pay",
      SUB_HE_CONTENT_11: "Their Privacy Policy can be viewed at ",
      link11: "https://www.sagepay.co.uk/policies",
      SUB_HEADING_12: "Square",
      SUB_HE_CONTENT_12: "Their Privacy Policy can be viewed at ",
      link12: "https://squareup.com/legal/privacy-no-account",
      SUB_HEADING_13: "Go Cardless",
      SUB_HE_CONTENT_13: "Their Privacy Policy can be viewed at ",
      link13: "https://gocardless.com/en-eu/legal/privacy/",
      SUB_HEADING_14: "Elavon",
      SUB_HE_CONTENT_14: "Their Privacy Policy can be viewed at ",
      link14: "https://www.elavon.com/privacy-pledge.html",
      SUB_HEADING_15: "Verifone",
      SUB_HE_CONTENT_15: "Their Privacy Policy can be viewed at ",
      link15: "https://www.verifone.com/en/us/legal",
      SUB_HEADING_16: "WeChat",
      SUB_HE_CONTENT_16: "Their Privacy Policy can be viewed at ",
      link16: "https://www.wechat.com/en/privacy_policy.html",
      SUB_HEADING_17: "Alipay",
      SUB_HE_CONTENT_17: "Their Privacy Policy can be viewed at ",
      link17:
        "https://render.alipay.com/p/f/agreementpages/alipayglobalprivacypolicy.html"
    }
  },
  LINK_OTHER_SITE: {
    HEADING: "Links to Other Sites",
    MAIN_1:
      "Our Service may contain links to other sites that are not operated by us. If you click a third party link, you will be directed to that third party's site. We strongly advise you to review the Privacy Policy of every site you visit",
    MAIN_2:
      "We have no control over and assume no responsibility for the content, privacy policies or practices of any third party sites or services."
  },
  CHILDREN_PRIVACY: {
    HEADING: "Children's Privacy",
    MAIN_1:
      "Our Service does not address anyone under the age of 18 ('Children').",
    MAIN_2:
      "We do not knowingly collect personally identifiable information from anyone under the age of 18. If you are a parent or guardian and you are aware that your Child has provided us with Personal Data, please contact us. If we become aware that we have collected Personal Data from children without verification of parental consent, we take steps to remove that information from our servers."
  },
  CHANGE_PRIVACY: {
    HEADING: "Changes to This Privacy Policy",
    MAIN_1:
      "We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new Privacy Policy on this page. ",
    MAIN_2:
      "We will let you know via email and/or a prominent notice on our Service, prior to the change becoming effective and update the 'effective date' at the top of this Privacy Policy.",
    MAIN_3:
      "You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when they are posted on this page."
  },
  CONTACT_US: {
    HEADING: "Contact Us",
    MAIN_1:
      "If you have any questions about this Privacy Policy, please contact us by email: meet@vasitum.com"
  }
};
