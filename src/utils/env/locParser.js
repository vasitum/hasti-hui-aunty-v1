/**
 * @param addrArray: String
 * @param addr: String
 *
 * @returns Object || String
 */
export default (addrArray, addr) => {
  // default result
  let res = {
    country: "",
    primary: ""
  };

  // check flags
  let isLocalityFound = false,
    isCountryFound = false;

  // no address component found
  if (!addrArray || !Array.isArray(addrArray)) {
    return addr;
  }

  addrArray.map(loc => {
    // if type not found
    if (!loc.types || !Array.isArray(loc.types)) {
      return;
    }

    // country
    if (!isCountryFound) {
      if (loc.types.indexOf("country") !== -1) {
        res["country"] = loc.long_name;
        isCountryFound = true;
      }
    }

    // locality
    if (!isLocalityFound) {
      // locality exists
      if (loc.types.indexOf("locality") !== -1) {
        res["primary"] = loc.long_name;
        isLocalityFound = true;
      }

      // admin area "1" exists
      if (loc.types.indexOf("administrative_area_level_2") !== -1) {
        res["primary"] = loc.long_name;
        isLocalityFound = true;
      }

      // admin area "2" exists
      if (loc.types.indexOf("administrative_area_level_1") !== -1) {
        res["primary"] = loc.long_name;
        isLocalityFound = true;
      }
    }
  });

  // if we found nothing
  if (!res.country && !res.primary) {
    return addr;
  }

  // return parsed location
  return res;
};
