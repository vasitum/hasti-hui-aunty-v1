export default val => {
  return !val || val === "undefined" || val === undefined;
};
