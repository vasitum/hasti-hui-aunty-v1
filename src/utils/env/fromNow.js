import { distanceInWordsStrict } from "date-fns";
import locale from "./locale";

export default time => {
  return distanceInWordsStrict(+new Date(), time, {
    locale: locale,
    addSuffix: true
  });
};
