import { USER } from "../../constants/api";
import isNull from "./isNull";
import isUndefined from "./isUndefined";

export default () => {
  const auth = window.localStorage.getItem(USER.X_AUTH_ID);
  const id = window.localStorage.getItem(USER.UID);

  if (
    (auth || !isNull(auth) || !isUndefined(auth)) &&
    (id || !isNull(id) || !isUndefined(id))
  ) {
    return false;
  } else {
    return true;
  }
};
