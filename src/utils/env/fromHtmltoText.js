export default html => {
  const d = document.createElement("div");
  d.innerHTML = html;
  return d.textContent;
};

export function toHtml(html) {
  const d = document.createElement("div");
  d.innerHTML = html;
  return d.innerHTML;
}
