function buildDistanceInWordsLocale() {
  var distanceInWordsLocale = {
    lessThanXSeconds: {
      one: "less than a second",
      other: "less than {{count}} seconds"
    },

    xSeconds: {
      // one: "1s",
      // other: "{{count}}s"
      one: "Just now",
      other: "Just now"
    },

    halfAMinute: "half a minute",

    lessThanXMinutes: {
      one: "less than a minute",
      other: "less than {{count}} minutes"
    },

    xMinutes: {
      // one: "1m",
      // other: "{{count}}m"
      one: "Just now",
      other: "Just now"
    },

    aboutXHours: {
      one: "about 1 hour",
      other: "about {{count}} hours"
    },

    xHours: {
      one: "1h",
      other: "{{count}}h"
    },

    xDays: {
      one: "1d",
      other: "{{count}}d"
    },

    aboutXMonths: {
      one: "about 1 month",
      other: "about {{count}} months"
    },

    xMonths: {
      one: "1 month",
      other: "{{count}} months"
    },

    aboutXYears: {
      one: "about 1 year",
      other: "about {{count}} years"
    },

    xYears: {
      one: "1y",
      other: "{{count}}y"
    },

    overXYears: {
      one: "over 1 year",
      other: "over {{count}} years"
    },

    almostXYears: {
      one: "almost 1 year",
      other: "almost {{count}} years"
    }
  };

  function localize(token, count, options) {
    options = options || {};

    var result;
    if (typeof distanceInWordsLocale[token] === "string") {
      result = distanceInWordsLocale[token];
    } else if (count === 1) {
      result = distanceInWordsLocale[token].one;
    } else {
      result = distanceInWordsLocale[token].other.replace("{{count}}", count);
    }

    if (options.addSuffix) {
      if (options.comparison > 0) {
        return "in " + result;
      } else {
        if (token === "xMinutes" || token === "xSeconds") {
          return result;
        }
        return result + " ago";
      }
    }

    return result;
  }

  return {
    localize: localize
  };
}

module.exports = buildDistanceInWordsLocale;
