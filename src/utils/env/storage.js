function getStorage() {
  return window.localStorage;
}

export function getItem(key) {
  getStorage().getItem(key);
}

export function setItem(key, value) {
  getStorage().setItem(key, value);
}
