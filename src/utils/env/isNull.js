export default val => {
  return !val || val === "null" || val === null;
};
