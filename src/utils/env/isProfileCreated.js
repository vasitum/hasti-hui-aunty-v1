import { USER } from "../../constants/api";

const storage = window.localStorage;

export default function() {
  const fName_storage = storage.getItem(USER.NAME);
  // check if name is available
  if (!fName_storage) {
    return false;
  }

  const [fName, lName] = fName_storage.split(" ");
  if (fName && fName !== "undefined") {
    return true;
  }

  return false;
}
