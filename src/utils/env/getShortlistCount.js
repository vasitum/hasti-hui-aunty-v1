export default status => {
  if (!status) {
    return 0;
  }

  for (let idx = 0; idx < status.length; idx++) {
    const element = status[idx];
    if (element.status === "ShortList") {
      return element.count;
    }
  }

  return 0;
};
