import qs from "qs";

export default function() {
  const queryStringJson = qs.parse(window.location.search);
  console.log(queryStringJson);
  if (Object.prototype.hasOwnProperty.call(queryStringJson, "utm_campaign")) {
    if (queryStringJson["utm_campaign"]) {
      return true;
    } else {
      return false;
    }
  }

  return false;
}
