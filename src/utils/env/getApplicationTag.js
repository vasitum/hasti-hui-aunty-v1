function getRejectedTag(application) {
  if (application.rejectBy === "ai") {
    return "Rejected by Vasi";
  } else {
    return "Rejected by You";
  }
}

function getShortListTag(application) {
  if (!application.interviewScheduledDate) {
    return "Yet to schedule interview";
  }
}

function getInterviewTag(application) {
  if (!application.interviewScheduledDate) {
    return "Yet to schedule interview";
  }

  const currentTime = new Date().getTime();
  if (application.interviewScheduledDate > currentTime) {
    return "Interview Scheduled";
  } else {
    return "Feedback Pending";
  }
}

export default application => {
  switch (application.status) {
    case "Interview":
      return getInterviewTag(application);

    case "ShortList":
      return getShortListTag(application);

    case "Rejected":
      return getRejectedTag(application);

    default:
      return null;
  }
};
