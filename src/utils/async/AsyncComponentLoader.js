import React, { Component } from "react";

/**
 *
 * componentLoader: a basic chunker/importer uses webpack import() syntax to
 * dynamically load the custom components whenever needed
 */

/**
 * TODO: Enable proper handlers in case of component not getting loaded
 */
export default getComponent => {
  return class extends Component {
    state = {
      AsyncComponent: null
    };

    // eslint-disable-next-line
    constructor(props) {
      super(props);
    }

    async componentDidMount() {
      if (!this.state.AsyncComponent) {
        try {
          const component = await getComponent;
          this.setState({ AsyncComponent: component });
        } catch (error) {
          console.error(error);
        }
      }
    }

    render() {
      const { AsyncComponent } = this.state;
      return AsyncComponent ? (
        <AsyncComponent {...this.props} />
      ) : (
        //TODO: add proper page loader
        <div> loading... </div>
      );
    }
  };
};
