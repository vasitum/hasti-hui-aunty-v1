const isPromiseRunning = false;

export default (url, options, timeout, signal) => {
  console.log("Promise Race called");

  // const controller = new window.AbortController();
  // const signal = controller.signal;

  return Promise.race([
    fetch(url, { ...options, signal }),
    new Promise((_, reject) => {
      setTimeout(() => {
        // controller.abort();
        reject(new Error("timeout"));
      }, timeout);
    })
  ]);
};
