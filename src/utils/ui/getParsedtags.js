export default function getParsedtags(tagItems, id) {
  let ret = [];
  if (!tagItems || !Array.isArray(tagItems) || tagItems.length === 0) {
    return ret;
  }

  tagItems.map(val => {
    if (val.itemId === id) {
      ret = val.labels ? val.labels : [];
    }
  });

  return ret;
}
