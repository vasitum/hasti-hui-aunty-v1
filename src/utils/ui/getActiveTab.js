/**
 * @param {string} param
 * @param {Object} tabs
 */
export default function(param, tabs) {
  const DEFAULT = 0;

  if (Object.prototype.hasOwnProperty.call(tabs, param)) {
    return tabs[param];
  }

  return DEFAULT;
}
