import { S3 } from "aws-sdk";
import { AWS as AWS_ERROR } from "../../constants/error";
import isProduction from "../env/is-production";

const bucketTypes = {
  IMAGE: "IMAGE",
  RES: "RES"
};

function getBucketName(type) {
  if (type === bucketTypes.IMAGE) {
    return process.env.REACT_APP_AWS_BUCKET_IMAGE;
    // return isProduction()
    //   ? process.env.REACT_APP_AWS_BUCKET_IMAGE
    //   : process.env.REACT_APP_AWS_BUCKET_TEST_IMAGE;
  }

  return process.env.REACT_APP_AWS_BUCKET_RESUME;
  // return isProduction()
  //   ? process.env.REACT_APP_AWS_BUCKET_RESUME
  //   : process.env.REACT_APP_AWS_BUCKET_TEST_RESUME;
}

function getBucket(name) {
  return new S3({
    accessKeyId: process.env.REACT_APP_AWS_S3_ACCESS_ID,
    secretAccessKey: process.env.REACT_APP_AWS_S3_SECRET_ID,
    region: process.env.REACT_APP_AWS_BASE_URL,
    sslEnabled: true,
    maxRetries: 0,
    params: {
      Bucket: name
    }
  });
}

function putObject(bucket, bucketName, key, body) {
  return new Promise((resolve, reject) => {
    if (!body) {
      reject(new Error(AWS_ERROR.E_FILETYPE));
    }

    bucket.putObject(
      {
        Bucket: bucketName,
        Key: key,
        Body: body
      },
      (err, data) => {
        if (err) {
          reject(new Error(AWS_ERROR.E_AWSINTERNAL));
          return;
        }

        resolve(data);
      }
    );
  });
}

function deleteObject(bucket, bucketName, key) {
  return new Promise((resolve, reject) => {
    bucket.putObject(
      {
        Bucket: bucketName,
        Key: key
      },
      (err, data) => {
        if (err) {
          reject(new Error(AWS_ERROR.E_AWSINTERNAL));
          return;
        }

        resolve(data);
      }
    );
  });
}

/**
 * Uploads Image to Our S3 Bucket
 *
 *
 * @param {String} key
 * @param {File} image
 */
export function uploadImage(key, image) {
  const bucket = getBucket(getBucketName(bucketTypes.IMAGE));
  return putObject(bucket, getBucketName(bucketTypes.IMAGE), key, image);
}

/**
 *
 * @param {String} key
 */
export function deleteImage(key) {
  const bucket = getBucket(getBucketName(bucketTypes.IMAGE));
  return deleteObject(bucket, getBucketName(bucketTypes.IMAGE), key);
}

/**
 *
 * @param {String} key
 * @param {File} file
 */
export function uploadResume(key, file) {
  const bucket = getBucket(getBucketName(bucketTypes.RES));
  return putObject(bucket, getBucketName(bucketTypes.RES), key, file);
}

/**
 *
 * @param {String} key
 */
export function deleteResume(key) {
  const bucket = getBucket(getBucketName(bucketTypes.RES));
  return deleteObject(bucket, getBucketName(bucketTypes.RES), key);
}
