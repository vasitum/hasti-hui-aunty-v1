export const HomePageString = {
  HomeEvent: {
    category: "Home",
    action: "View"
  },

  messaging: {
    category: "Messaging",
    action: "View"
  },

  notification: {
    category: "Notifications",
    action: "View"
  },
  
  Search: {
    category: "Search",
    action: "Search"
  },

  EventJob: {
    category: "Jobs",
    action: "View"
  },
  
  PostJob: {
    category: "Job",
    action: "Post"
  },
  

  EventPeople: {
    category: "People",
    action: "View"
  },

  userProfile: {
    category: "UserProfile",
    action: "View"
  },

  Candidate: {
    category: "CandidateDashboard",
    action: "View"
  },

  Recruiter: {
    category: "RecruiterDashboard",
    action: "View"
  },

  ManageTemplates: {
    category: "ManageTemplates",
    action: "View"
  },

  Calendar: {
    category: "ManageCalendar",
    action: "View"
  },

  SignOut: {
    category: "SignInOut",
    action: "SignOut"
  },
  
  UserSignUpModal: {
    category: "SignInOut",
    action: "SignIn"
  },

  UserSignInModal: {
    category: "SignInOut",
    action: "Register"
  },

  ForgotPassword: {
    category: "SignInOut",
    action: "ForgotPassword"
  }

}