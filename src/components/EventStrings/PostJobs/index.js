export const PostJobPages = {
  PostJobsEvent: {
    category: "Job",
    action: "Post"
  },
  
  EditPostJobsEvent: {
    category: "Job",
    action: "Edit"
  },

}