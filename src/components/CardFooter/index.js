import React from "react";
import {
  Grid,
  Image,
  Checkbox,
  Dropdown,
  Button,
  List,
  Form,
  Input
} from "semantic-ui-react";
import "./index.scss";

const CardOptions = [
  {
    text: "Basic screening",
    value: "Basic screening"
  },
  {
    text: "Interview",
    value: "Interview"
  },
  {
    text: "New applicants",
    value: "New applicants"
  },
  {
    text: "Pending applicants",
    value: "Pending applicants"
  }
];

const MoreOptions = [
  {
    text: "More actions",
    value: "More actions"
  },
  {
    text: "Option 1",
    value: "Option 1"
  },
  {
    text: "Option 2",
    value: "Option 2"
  },
  {
    text: "Option 3",
    value: "Option 3"
  }
];
const CardFooter = () => (
  <div className="Main_Card">
    <Grid className="Main_Card_Grid">
      <Grid.Row className="Main_Card_Grid_Row">
        <Grid.Column width="5">
          <h4>Application current status</h4>
        </Grid.Column>

        <Grid.Column width="5" className="Basic_Section">
          <Dropdown
            placeholder="Basic screening"
            selection
            options={CardOptions}
          />
          <Button size="small">Fail</Button>
        </Grid.Column>

        <Grid.Column width="4" />
        <Grid.Column width="2">
          <span>
            {" "}
            <Dropdown
              inline
              options={MoreOptions}
              defaultValue={MoreOptions[0].value}
            />
          </span>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  </div>
);

export default CardFooter;
