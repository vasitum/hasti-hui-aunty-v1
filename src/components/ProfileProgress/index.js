import React from 'react';

import { Progress, Card, Header, Button, Grid } from "semantic-ui-react";
import FlatFileuploadBtn from "../Buttons/FlatFileUploadeBtn";
import PropTypes from 'prop-types';


import "./index.scss";

const ProfileProgress = props => {
  return (

    <div>
      {/* <Grid>
        <Grid.Row>
          <Grid.Column mobile></Grid.Column>
        </Grid.Row>
      </Grid> */}
      <Card className="ProfileProgress" fluid>
        <Card.Content>
          <Card.Header>
            <div className="Progressbar">
              <Progress percent={80} size='small' />
              <Header as="h3">80% profile completed</Header>
            </div>
          </Card.Header>
          <Card.Description>
            <p className="title">Add certification to increase to <span>20%</span></p>
            <p className="subTitle">orem ipsum dolor sit amet, consectetur adipisicing elit, sed do</p>
          </Card.Description>
        </Card.Content>
        <Card.Content className="completeProfileBtn">

          <Grid>
            <Grid.Row>
              <Grid.Column width={8}>
                <Button primary compact className="addBtn" fluid>
                  Add
              </Button>
              </Grid.Column>
              <Grid.Column width={8}>
                <Button compact className="skipBtn" fluid>
                  Skip for now
              </Button>
              </Grid.Column>
            </Grid.Row>
          </Grid>

        </Card.Content>
      </Card>

      <div className="UploadeFile" style={{padding: "20px"}}>
        <FlatFileuploadBtn btnText="Update resume"/>
      </div>
    </div>
  )
}

// ProfileProgress.propTypes = {
//   placeholder: PropTypes.string
// }

export default ProfileProgress;