import React from "react";

import { Dropdown } from "semantic-ui-react";
import PropTypes from "prop-types";

// import IcDownArrowIcon from "../../assets/svg/IcDownArrow";

import "./index.scss";

// const trigger = (
//   <span className="SalaryDropdown">
//     INR <IcDownArrowIcon width="9" pathcolor="#c8c8c8" />
//   </span>
// );

const options = [
  { key: "INR", text: "INR", value: "INR" },
  { key: "EUR", text: "EUR", value: "EUR" },
  { key: "USD", text: "USD", value: "USD" },
  { key: "JPY", text: "JPY", value: "JPY" },
  { key: "CNY", text: "CNY", value: "CNY" }
];

const DropdownMenu = ({ onChange, value, name }) => {
  // console.log("DROPDOWN VALUE is", value);
  return (
    <Dropdown
      // trigger={trigger}
      inline
      onChange={onChange}
      value={value}
      placeholder="INR"
      options={options}
      className="SalaryDropdown"
      name={name}
    />
  );
};

DropdownMenu.propTypes = {
  placeholder: PropTypes.string
};

export default DropdownMenu;
