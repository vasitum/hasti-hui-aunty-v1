import React, { Component } from "react";

import { Grid, Header, List, Button, Image } from "semantic-ui-react";
import { withSearch, withHits } from "../../../AlgoliaContainer";

import { Configure } from "react-instantsearch-dom";

import ReactGA from "react-ga";
import { USER } from "../../../../constants/api";
import isLoggedIn from "../../../../utils/env/isLoggedin";
import { HomePageString } from "../../../EventStrings/Homepage";

import IcCompany from "../../../../assets/svg/IcCompany";
import IcMyJobs from "../../../../assets/svg/IcMyJobs";
import IcLocation from "../../../../assets/svg/IcLocation";
import SkillList from "../../../CardElements/SkillList";
import QuillText from "../../../CardElements/QuillText";

import IcUser from "../../../../assets/svg/IcUser";

import IcFooterArrowIcon from "../../../../assets/svg/IcFooterArrowIcon";

import { withRouter, Link } from "react-router-dom";
import fromNow from "../../../../utils/env/fromNow";

// import "./index.scss";

const UserId = window.localStorage.getItem(USER.UID);

const EventPeople = () =>{
  ReactGA.event({
    category: HomePageString.EventPeople.category,
    action: HomePageString.EventPeople.action,
    label: isLoggedIn() ? "" : UserId
  });
}

const SearchCard = withHits(({ hits }) => {
  return hits.map((val, index) => (
    <Grid.Column width={5} key={index}>
      <div className="PicksCard">
        {/* <div className="cardPicks_time">{fromNow(val.uTime)}</div> */}
        <div className="PicksCard_header">
          <div className="PicksCard_logUser">
            {val.imgExt ? (
              <Image
                verticalAlign="top"
                // style={{
                //   // height: "40px",
                //   // width: "40px",
                //   marginTop: "5px",
                //   borderRadius: "6px",
                //   position: "absolute",
                //   width: "40px",
                //   height: "40px"
                // }}
                src={`https://s3-us-west-2.amazonaws.com/img-mwf/${
                  val.objectID
                }/image.jpg`}
                alt="people logo"
              />
            ) : (
              <IcUser
                rectcolor="#f7f7fb"
                height="62"
                width="62"
                pathcolor="#c8c8c8"
              />
            )}
            {/* <Image src="" /> */}
          </div>
          <div className="PicksCard_titleContainer">
            <div className="PicksCard_title">
              <Link to={`/view/user/${val.objectID}`}>
                <Header as="h2">
                  {" "}
                  {val.fName} {val.lName}
                </Header>
              </Link>
              <p className="head_title">
                {/* <IcMyJobs height="10" width="12" pathcolor="#0b9ed0" /> */}
                {val.title}
              </p>
            </div>
            <div className="sub_title">
              {val.com ? <p className="exp">Current: {val.com}</p> : null}

              <p className="loctaion">
                <IcLocation height="10" width="12" pathcolor="#0bd0bb" />
                {val.locCity}
                {val.locCountry ? ", " + val.locCountry : ""}
              </p>
            </div>
          </div>
        </div>

        <div className="PicksCard_footer">
          <div className="SkillS filterSkill">
            {/* <SkillList skills={Skills}/> */}
            <List
              bulleted
              horizontal
              className="skills"
              style={{
                display: !val.skills ? "none" : "block"
              }}>
              {(() => {
                if (!val.skills) {
                  return null;
                }

                const ListKeys = val.skills.map((skill, idx) => {
                  if (idx > 2) return null;

                  if (idx === 0) {
                    return (
                      <List.Item key={idx}>
                        <span className="text-nobel">Skills:</span> {skill}
                      </List.Item>
                    );
                  }

                  return <List.Item key={idx}>{skill}</List.Item>;
                });

                return ListKeys;
              })()}
            </List>
          </div>
          {/* <div
            className="foot_desc"
            style={{
              display: val.summry ? "block" : "none"
            }}>
            <QuillText value={val.summry ? val.summry : ""} readOnly />
          </div> */}
        </div>
      </div>
    </Grid.Column>
  ));
});

class RecentPicksCardPeople extends Component {
  render() {
    const skills = [
      {
        name: "Java"
      },
      {
        name: "Photoshop"
      },
      {
        name: "Scala"
      }
    ];

    return (
      <div className="RecentPicksCard">
        <Configure hitsPerPage={3} />
        <Grid>
          <Grid.Row>
            <SearchCard />
          </Grid.Row>
        </Grid>
        <div className="RecentPicksCard_footer">
          <Button
            as={Link}
            to={`/search/people?query=${encodeURIComponent("")}`}
            onClick={EventPeople}
            >
            View more people{" "}
            <IcFooterArrowIcon pathcolor="#0b9ed0" height="8" width="12" />
          </Button>
        </div>
      </div>
    );
  }
}

export default withRouter(
  withSearch(RecentPicksCardPeople, {
    apiKey: process.env.REACT_APP_ALGOLIA_API_KEY,
    appId: process.env.REACT_APP_ALGOLIA_APP_ID,
    indexName: "user",
    noRouting: true
  })
);
