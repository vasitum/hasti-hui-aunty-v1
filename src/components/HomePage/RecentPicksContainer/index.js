import React, { Component } from "react";
import { Tab, Container } from "semantic-ui-react";
import CategoryTitle from "../CategoryTitle";

import RecentPicksCardJob from "./RecentPicksCardJob";
import RecentPicksCardPeople from "./RecentPicksCardPeople";
import {HOMEPAGE} from "../../../strings";
import "./index.scss";

const RecentPickTab = [
  {
    menuItem: "Jobs",
    render: () => {
      return (
        <Tab.Pane attached={false}>
          <RecentPicksCardJob />
        </Tab.Pane>
      );
    }
  },
  {
    menuItem: "People",
    render: () => {
      return (
        <Tab.Pane attached={false}>
          <RecentPicksCardPeople />
        </Tab.Pane>
      );
    }
  }
];

export default class RecentPicksContainer extends Component {
  render() {
    return (
      <div className="RecentPicksContainer">
        <div className="Category_headerTitle">
          <CategoryTitle
            headTitle="Recent picks"
            subTitle={HOMEPAGE.RECENT_PICKS.SUBTITLE}
          />
        </div>

        <div className="RecentPick_category">
          <Container>
            <Tab menu={{ secondary: true }} panes={RecentPickTab} />
          </Container>
        </div>
      </div>
    );
  }
}
