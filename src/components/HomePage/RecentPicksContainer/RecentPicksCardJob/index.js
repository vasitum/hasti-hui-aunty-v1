import React, { Component } from "react";

import { Grid, Header, List, Button, Image } from "semantic-ui-react";

import IcCompany from "../../../../assets/svg/IcCompany";
import IcMyJobs from "../../../../assets/svg/IcMyJobs";
import IcLocation from "../../../../assets/svg/IcLocation";
// import SkillList from "../../../CardElements/SkillList";
import QuillText from "../../../CardElements/QuillText";

import { USER } from "../../../../constants/api";

import IcFooterArrowIcon from "../../../../assets/svg/IcFooterArrowIcon";
import isLoggedIn from "../../../../utils/env/isLoggedin";
import { withRouter, Link } from "react-router-dom";

import { HomePageString } from "../../../EventStrings/Homepage";

import { withSearch, withHits } from "../../../AlgoliaContainer";

import { Configure } from "react-instantsearch-dom";
import ReactGA from "react-ga";
import "./index.scss";
import fromNow from "../../../../utils/env/fromNow";

const UserId = window.localStorage.getItem(USER.UID);

const EventJob = () =>{
  ReactGA.event({
    category: HomePageString.EventJob.category,
    action: HomePageString.EventJob.action,
    label: isLoggedIn() ? "" : UserId
  });
}

const SearchCard = withHits(({ hits }) => {
  
  return hits.map((val, index) => (
    <Grid.Column width={5} key={index}>
      <div className="PicksCard">
        <div className="cardPicks_time">{fromNow(val.datePosted)}</div>
        <div className="PicksCard_header">
          <div className="PicksCard_log">
            {val.comImgExt ? (
              <Image
                verticalAlign="top"
                // style={{
                //   // height: "40px",
                //   // width: "40px",
                //   marginTop: "5px",
                //   borderRadius: "6px",
                //   position: "absolute",
                //   width: "40px",
                //   height: "40px"
                // }}
                src={`https://s3-us-west-2.amazonaws.com/img-mwf/company/${
                  val.comId
                }/image.jpg`}
                alt="job log"
              />
            ) : (
              <IcCompany
                rectcolor="#f7f7fb"
                height="62"
                width="62"
                pathcolor="#c8c8c8"
              />
            )}
            {/* <Image src="" /> */}
          </div>
          <div className="PicksCard_titleContainer">
            <div className="PicksCard_title">
              <Link to={`/view/job/${val.objectID}`}>
                <Header as="h2">{val.title}</Header>
              </Link>
              <p className="head_title">
                <IcMyJobs height="10" width="12" pathcolor="#0b9ed0" />
                {!val.com ? val.comName : val.com}
              </p>
            </div>
            <div className="sub_title">
              <p className="loctaion">
                <IcLocation height="10" width="12" pathcolor="#0bd0bb" />
                {val.locCity}
                {val.locCountry ? ", " + val.locCountry : ""}
              </p>
              <p className="exp">
                <span>
                  {" "}
                  {val.expMin} - {val.expMax}
                </span>{" "}
                yrs exp
              </p>
            </div>
          </div>
        </div>

        <div className="PicksCard_footer">
          <div className="SkillS filterSkill">
            {/* <SkillList skills={Skills}/> */}
            <List
              bulleted
              horizontal
              className="skills"
              style={{
                display: !val.skills ? "none" : "block"
              }}>
              {val.skills
                ? val.skills.map((skill, idx) => {
                    if (idx > 2) return null;

                    if (idx === 0) {
                      return (
                        <List.Item key={idx}>
                          <span className="text-nobel">Skills:</span> {skill}
                        </List.Item>
                      );
                    }
                    return <List.Item key={idx}>{skill}</List.Item>;
                  })
                : null}
            </List>
          </div>
          {/* <div className="foot_desc">
            <QuillText
              value={val.desc ? val.desc : ""}
              readOnly
              noReadMore
              readMoreLength={100}
            />
          </div> */}
        </div>
      </div>
    </Grid.Column>
  ));
});

class RecentPicksCard extends Component {
  render() {
    const skills = [
      {
        name: "Java"
      },
      {
        name: "Photoshop"
      },
      {
        name: "Scala"
      }
    ];

    return (
      <div className="RecentPicksCard">
        <Configure hitsPerPage={3} />
        <Grid>
          <Grid.Row>
            <SearchCard />
          </Grid.Row>
        </Grid>

        <div className="RecentPicksCard_footer">
          <Button 
            as={Link} 
            to={`/search/job?query=${encodeURIComponent("")}`}
            onClick={EventJob}
            >
            View more jobs

            <IcFooterArrowIcon pathcolor="#0b9ed0" height="8" width="12" />
          </Button>
        </div>
      </div>
    );
  }
}

export default withRouter(
  withSearch(RecentPicksCard, {
    apiKey: process.env.REACT_APP_ALGOLIA_API_KEY,
    appId: process.env.REACT_APP_ALGOLIA_APP_ID,
    indexName: "job",
    noRouting: true
  })
);
