import React, { Component } from "react";

import "./index.scss";
import CategoryTitle from "../CategoryTitle";
import { Container } from "semantic-ui-react";
import IcWebDeveloper from "../../../assets/svg/ic_category_web_developer.svg";
import IcArtDesign from "../../../assets/svg/ic_category_art_design.svg";
import IcCategoryHealth from "../../../assets/svg/ic_category_health.svg";
import IcTelecommicatuion from "../../../assets/svg/ic_category_telecomunication.svg";
import IcHumanResources from "../../../assets/svg/ic_category_human_resource.svg";
import IcFooterArrowIcon from "../../../assets/svg/IcFooterArrowIcon";

export default class HomePageCategories extends Component {
  render() {
    const CardData = [
      { heading: "Web Developer", subHeading: "80 open positions" },
      {
        heading: "Arts, design",
        subHeading: "80 open positions"
      },
      { heading: "Human resource", subHeading: "80 open positions" },
      { heading: "Health", subHeading: "80 positions open" },
      { heading: "Telecommunication", subHeading: "80 open  position" }
    ];
    return (
      <div
        className="HomePageCategories"
        style={{
          display: "none"
        }}>
        <CategoryTitle
          headTitle="Choose category, you want"
          subTitle="Find jobs, employment &amp; career opportunities"
        />
        <Container>
          <div className="scrolling-wrapper">
            <div className="categoryCard">
              <img
                src={IcWebDeveloper}
                alt=""
                className="large screen only computer only"
              />
              <span className="categoryCardHeading">Web Developer</span>
              <span className="categoryCardSubHeading">80 Open positions</span>
            </div>
            <div className="categoryCard">
              <img
                src={IcArtDesign}
                alt=""
                className="large screen only computer only"
              />
              <span className="categoryCardHeading">Arts, design</span>
              <span className="categoryCardSubHeading">80 Open positions</span>
            </div>
            <div className="categoryCard">
              <img
                src={IcHumanResources}
                alt=""
                className="large screen only computer only"
              />
              <span className="categoryCardHeading">Human resource</span>
              <span className="categoryCardSubHeading">80 Open positions</span>
            </div>
            <div className="categoryCard">
              <img
                src={IcCategoryHealth}
                alt=""
                className="large screen only computer only"
              />
              <span className="categoryCardHeading">Health</span>
              <span className="categoryCardSubHeading">80 Open positions</span>
            </div>
            <div className="categoryCard">
              <img
                src={IcTelecommicatuion}
                alt=""
                className="large screen only computer only"
              />
              <span className="categoryCardHeading">Web Developer</span>
              <span className="categoryCardSubHeading">80 Open positions</span>
            </div>

            <div className="categoryCard categoryCardBlack">
              <span className="categoryCardHeading">All Cateogories</span>
            </div>
          </div>
          <div className="mobile only grid MobileHomepageViewAllCt">
            View All Category{" "}
            <IcFooterArrowIcon pathcolor="#0b9ed0" height="7px" width="11px" />
          </div>
        </Container>
      </div>
    );
  }
}
