import React, { Component } from "react";

import "./index.scss";

import HomePageContainer from "../HomePageContainer";
import MobileHomePageFooterUpper from "../MobileHomePageFooterUpper";
import HomePageSubFooter from "../HomePageFooter/HomePageSubFooter";
import AboutCompanyContainer from "../HomePageFooter/AboutCompanyContainer";

export default class HomePageMobile extends Component {
  render() {
    return (
      <div className="HomePageMobile">
        <div className="HomePageMobile_header" />
        <div className="HomePageMobile_body">
          <HomePageContainer isShowTrandingKey />
        </div>
        <div className="HomePageMobile_footer">
          <MobileHomePageFooterUpper />
          <HomePageSubFooter/>
          <AboutCompanyContainer />
        </div>
      </div>
    );
  }
}
