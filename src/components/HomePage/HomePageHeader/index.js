import React from "react";

import { Header, Button } from "semantic-ui-react";
import { Form } from "formsy-semantic-ui-react";
import InputField from "../../Forms/FormFields/InputField";

import SearchIcon from "../../../assets/svg/SearchIcon";

import { withRouter, Link } from "react-router-dom";

import "./index.scss";

class HomePageHeader extends React.Component {
  state = {
    inputVal: ""
  };

  onInputChange = (e, { value }) => {
    this.setState({
      inputVal: value
    });
  };

  onSubmit = e => {
    const { inputVal } = this.state;

    if (!inputVal) {
      return;
    }

    this.props.history.push(`/search?query=${encodeURIComponent(inputVal)}`);
  };

  render() {
    
    const { trandingKey } = this.props;

    return (
      <div className="HomePageHeader">
        <div className="HomePageHeaderContainer">
          <div className="headerTitle">
            <Header as="h1" className="">
              Most advanced
              <br />
              {/* <span>AI based</span>  */}
              recruitment platform
            </Header>
          </div>
          <div className="headerSearchField">
            {/* <Header as="h3">I am looking for</Header> */}
            <div className="header_inputFeild">
              <Form onSubmit={this.onSubmit}>
                <InputField
                  required
                  value={this.state.inputVal}
                  onChange={this.onInputChange}
                  placeholder="Search Jobs/ People/ Companies"
                  name="search_header"
                />
                <Button className="searchIconBtn">
                  <SearchIcon
                    pathcolor={this.state.inputVal ? "#1f2532" : "#eaeaee"}
                  />
                </Button>
              </Form>
            </div>

            <div className="recentSearch_tag">
              {/* <div className="recentSearch_tagLeft">
                <p>Recent Search</p>
                <div className="tag_btn">
                  <Button
                    as={Link}
                    to={`/search?query=${encodeURIComponent("UX designer")}`}
                    className="rounded_btn">
                    UX designer
                  </Button>
                  <Button
                    as={Link}
                    to={`/search?query=${encodeURIComponent("UX designer")}`}
                    className="rounded_btn">
                    UX designer
                  </Button>
                  <Button
                    as={Link}
                    to={`/search?query=${encodeURIComponent("")}`}
                    className="circle_btn">
                    5+
                  </Button>
                </div>
              </div> */}
              {trandingKey}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(HomePageHeader);
