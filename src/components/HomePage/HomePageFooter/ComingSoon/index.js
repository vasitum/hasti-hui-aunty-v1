import React from "react";

import { Button } from "semantic-ui-react";
import IcFooterArrowIcon from "../../../../assets/svg/IcFooterArrowIcon";
import IcClockIcon from "../../../../assets/svg/IcClockIcon";

class ComingSoon extends React.Component {
  constructor(props) {
    super(props);
  }

  onScrllTopClick = () => {
    window.scroll({
      top: 0,
      left: 0,
      behavior: "smooth"
    });
  };

  render() {
    return (
      <div className="container_right">
        <div className="topArrowFooter">
          <Button onClick={this.onScrllTopClick}>
            <IcFooterArrowIcon pathcolor="#ffffff" />
          </Button>
        </div>
        <div className="appComing_soon">
          <p>
            Apps for your mobile
            <br />
            <span>coming soon</span>
            <br />
            <IcClockIcon pathcolor="#acaeb5" />
          </p>
        </div>
      </div>
    );
  }
}

export default ComingSoon;
