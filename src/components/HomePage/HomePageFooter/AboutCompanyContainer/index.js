import React from "react";

import { List, Container } from "semantic-ui-react";

import { Link } from "react-router-dom";

import "./index.scss";

const AboutCompanyContainer = () => {
  return (
    <div className="aboutCompany_container">
      <Container>
        <List className="text-align" horizontal>
          {/* {HomeAboutList.map((item, idx) => {
                return(
                  <List.Item  as='a' key={idx}>{item.list}</List.Item>
                )
              })} */}
          <List.Item as={Link} to="/cv-template">
            Cv template
          </List.Item>
          <List.Item as={Link} to="/about-us">
            About
          </List.Item>
          <List.Item href="http://blog.vasitum.com/" target="_blank">
            Blogs
          </List.Item>
          {/* {isUserLoggedIn() ? (
                <List.Item className="asLink" as='a'><UserAccessModal/></List.Item>
              ):(<List.Item className="asLink" as={Link} to="/job/new">Post job</List.Item>)} */}

          {/* <List.Item >Upload resume</List.Item> */}
          {/* <List.Item >Help center</List.Item> */}
          <List.Item as={Link} to="/privacy-policy">
            Privacy policy
          </List.Item>
          {/* <List.Item >Copyright policy</List.Item> */}
        </List>
      </Container>
    </div>
  );
};

export default AboutCompanyContainer;
