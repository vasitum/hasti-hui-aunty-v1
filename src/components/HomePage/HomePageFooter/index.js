import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Container, Grid } from "semantic-ui-react";
import IcFooterArrowIcon from "../../../assets/svg/IcFooterArrowIcon.js";

import "./index.scss";

export default class HomePageFooter extends Component {
  render() {
    const { isShowSubFooter, isShowSubFooterTop } = this.props;

    return (
      <div className="HomePageFooterUpper large screen only computer only widescreen screen only">
        {isShowSubFooterTop}
        <Container>
          <Grid>
            <Grid.Row className="HomePageFooterUpper_Row_Container">
              {/* First Column */}
              <Grid.Column width={8}>
                <h3>Top cities</h3>
                <Grid.Row className="HomePageFooterUpper_Row">
                  <Grid.Column
                    width={5}
                    className="HomePageFooterUpper_Row_Left">
                    <Link to="/search?query=gurugram">
                      <span>
                        {" "}
                        <IcFooterArrowIcon
                          pathcolor="#727b8f"
                          height="7px"
                          width="11px"
                        />{" "}
                        Gurugram
                      </span>
                    </Link>
                    <Link to="/search?query=delhi">
                      <span>
                        {" "}
                        <IcFooterArrowIcon
                          pathcolor="#727b8f"
                          height="7px"
                          width="11px"
                        />{" "}
                        Delhi
                      </span>
                    </Link>
                    <Link to="/search?query=noida">
                      <span>
                        {" "}
                        <IcFooterArrowIcon
                          pathcolor="#727b8f"
                          height="7px"
                          width="11px"
                        />{" "}
                        Noida
                      </span>
                    </Link>
                    <Link
                      to={`/search?query=${encodeURIComponent("delhi/ncr")}`}>
                      <span>
                        {" "}
                        <IcFooterArrowIcon
                          pathcolor="#727b8f"
                          height="7px"
                          width="11px"
                        />{" "}
                        Delhi/NCR
                      </span>
                    </Link>
                  </Grid.Column>

                  <Grid.Column
                    className="HomePageFooterUpper_Row_Right"
                    width={5}>
                    <Link to="/search?query=hyderabad">
                      <span>
                        {" "}
                        <IcFooterArrowIcon
                          pathcolor="#727b8f"
                          height="7px"
                          width="11px"
                        />{" "}
                        Hyderabad
                      </span>
                    </Link>
                    <Link to="/search?query=pune">
                      <span>
                        {" "}
                        <IcFooterArrowIcon
                          pathcolor="#727b8f"
                          height="7px"
                          width="11px"
                        />{" "}
                        Pune
                      </span>
                    </Link>
                    <Link to="/search?query=chandigarh">
                      <span>
                        {" "}
                        <IcFooterArrowIcon
                          pathcolor="#727b8f"
                          height="7px"
                          width="11px"
                        />{" "}
                        Chandigarh
                      </span>
                    </Link>
                  </Grid.Column>
                  <Grid.Column
                    width={5}
                    className="HomePageFooterUpper_Row_Left">
                    <Link to="/search?query=bangalore">
                      <span>
                        {" "}
                        <IcFooterArrowIcon
                          pathcolor="#727b8f"
                          height="7px"
                          width="11px"
                        />{" "}
                        Bangalore
                      </span>
                    </Link>
                    <Link to="/search?query=maharashtra">
                      <span>
                        {" "}
                        <IcFooterArrowIcon
                          pathcolor="#727b8f"
                          height="7px"
                          width="11px"
                        />{" "}
                        Maharashtra
                      </span>
                    </Link>
                    <Link to="/search?query=mumbai">
                      <span>
                        {" "}
                        <IcFooterArrowIcon
                          pathcolor="#727b8f"
                          height="7px"
                          width="11px"
                        />{" "}
                        Mumbai
                      </span>
                    </Link>
                  </Grid.Column>
                </Grid.Row>
              </Grid.Column>

              {/* Second Column */}
              <Grid.Column width={8} className="top_skills">
                <h3>Top skills</h3>
                <Grid.Row className="HomePageFooterUpper_Row">
                  <Grid.Column
                    width={5}
                    className="HomePageFooterUpper_Row_Left">
                    <Link to="/search?query=mechanical">
                      <span>
                        {" "}
                        <IcFooterArrowIcon
                          pathcolor="#727b8f"
                          height="7px"
                          width="11px"
                        />{" "}
                        Mechanical
                      </span>
                    </Link>
                    <Link to="/search?query=bpo">
                      <span>
                        {" "}
                        <IcFooterArrowIcon
                          pathcolor="#727b8f"
                          height="7px"
                          width="11px"
                        />{" "}
                        BPO
                      </span>
                    </Link>
                    <Link to="/search?query=networking">
                      <span>
                        {" "}
                        <IcFooterArrowIcon
                          pathcolor="#727b8f"
                          height="7px"
                          width="11px"
                        />{" "}
                        Networking
                      </span>
                    </Link>
                  </Grid.Column>

                  <Grid.Column
                    className="HomePageFooterUpper_Row_Right"
                    width={5}>
                    <Link
                      to={`/search?query=${encodeURIComponent(
                        "design engineer"
                      )}`}>
                      <span>
                        {" "}
                        <IcFooterArrowIcon
                          pathcolor="#727b8f"
                          height="7px"
                          width="11px"
                        />{" "}
                        Design engineer
                      </span>
                    </Link>
                    <Link to="/search?query=analytics">
                      <span>
                        {" "}
                        <IcFooterArrowIcon
                          pathcolor="#727b8f"
                          height="7px"
                          width="11px"
                        />{" "}
                        Analytics
                      </span>
                    </Link>
                    <Link to={`/search?query=${encodeURIComponent("ui/ux")}`}>
                      <span>
                        {" "}
                        <IcFooterArrowIcon
                          pathcolor="#727b8f"
                          height="7px"
                          width="11px"
                        />{" "}
                        UI/UX
                      </span>
                    </Link>
                  </Grid.Column>

                  <Grid.Column
                    className="HomePageFooterUpper_Row_Right"
                    width={5}>
                    <Link to="/search?query=animation">
                      <span>
                        {" "}
                        <IcFooterArrowIcon
                          pathcolor="#727b8f"
                          height="7px"
                          width="11px"
                        />{" "}
                        Animation
                      </span>
                    </Link>
                    <Link
                      to={`/search?query=${encodeURIComponent(
                        "online marketing"
                      )}`}>
                      <span>
                        {" "}
                        <IcFooterArrowIcon
                          pathcolor="#727b8f"
                          height="7px"
                          width="11px"
                        />{" "}
                        Online Marketing
                      </span>
                    </Link>
                    <Link to="/search?query=teaching">
                      <span>
                        {" "}
                        <IcFooterArrowIcon
                          pathcolor="#727b8f"
                          height="7px"
                          width="11px"
                        />{" "}
                        Teaching
                      </span>
                    </Link>
                  </Grid.Column>
                </Grid.Row>
              </Grid.Column>

              {/* Third Column */}
              {/* <Grid.Column width={5} className="trending_jobs">
                <h3>Trending jobs</h3>
                <Grid.Row className="HomePageFooterUpper_Row">
                  <Grid.Column
                    width={8}
                    className="HomePageFooterUpper_Row_Left">
                    <Link
                      to={`/search?query=${encodeURIComponent(
                        "androind developer"
                      )}`}>
                      <span>
                        {" "}
                        <IcFooterArrowIcon
                          pathcolor="#727b8f"
                          height="7px"
                          width="11px"
                        />{" "}
                        Androind developer
                      </span>
                    </Link>
                    <Link
                      to={`/search?query=${encodeURIComponent(
                        "marketing executive"
                      )}`}>
                      <span>
                        {" "}
                        <IcFooterArrowIcon
                          pathcolor="#727b8f"
                          height="7px"
                          width="11px"
                        />{" "}
                        Marketing executive
                      </span>
                    </Link>

                    <Link
                      to={`/search?query=${encodeURIComponent(
                        "full stack developer"
                      )}`}>
                      <span>
                        {" "}
                        <IcFooterArrowIcon
                          pathcolor="#727b8f"
                          height="7px"
                          width="11px"
                        />{" "}
                        Full stack developer
                      </span>
                    </Link>

                    <Link
                      to={`/search?query=${encodeURIComponent(
                        "frontend developer"
                      )}`}>
                      <span>
                        {" "}
                        <IcFooterArrowIcon
                          pathcolor="#727b8f"
                          height="7px"
                          width="11px"
                        />{" "}
                        Frontend developer
                      </span>
                    </Link>

                    <Link
                      to={`/search?query=${encodeURIComponent(
                        "data scientist"
                      )}`}>
                      <span>
                        {" "}
                        <IcFooterArrowIcon
                          pathcolor="#727b8f"
                          height="7px"
                          width="11px"
                        />{" "}
                        Data scientist
                      </span>
                    </Link>
                  </Grid.Column>

                  <Grid.Column
                    className="HomePageFooterUpper_Row_Right"
                    width={8}>
                    <Link to="/search?query=analyst">
                      <span>
                        {" "}
                        <IcFooterArrowIcon
                          pathcolor="#727b8f"
                          height="7px"
                          width="11px"
                        />{" "}
                        Analyst
                      </span>
                    </Link>

                    <Link
                      to={`/search?query=${encodeURIComponent(
                        "ux architecture"
                      )}`}>
                      <span>
                        {" "}
                        <IcFooterArrowIcon
                          pathcolor="#727b8f"
                          height="7px"
                          width="11px"
                        />{" "}
                        UX architecture
                      </span>
                    </Link>

                    <Link
                      to={`/search?query=${encodeURIComponent(
                        "ui/ux designer"
                      )}`}>
                      <span>
                        {" "}
                        <IcFooterArrowIcon
                          pathcolor="#727b8f"
                          height="7px"
                          width="11px"
                        />{" "}
                        UI/UX designer
                      </span>
                    </Link>

                    <Link
                      to={`/search?query=${encodeURIComponent("ppc expert")}`}>
                      <span>
                        {" "}
                        <IcFooterArrowIcon
                          pathcolor="#727b8f"
                          height="7px"
                          width="11px"
                        />{" "}
                        PPC expert
                      </span>
                    </Link>
                    <Link to="/search?query=blogger">
                      <span>
                        {" "}
                        <IcFooterArrowIcon
                          pathcolor="#727b8f"
                          height="7px"
                          width="11px"
                        />{" "}
                        Blogger
                      </span>
                    </Link>
                  </Grid.Column>
                </Grid.Row>
              </Grid.Column> */}
            </Grid.Row>
          </Grid>
        </Container>
        {isShowSubFooter}
      </div>
    );
  }
}
