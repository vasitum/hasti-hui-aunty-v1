import React, { Component } from 'react';

import { Container, Header, Button, List, Icon } from "semantic-ui-react";



import CardInputButton from "../../../Cards/CardInputButton";

import IcSocialGoogleplus from "../../../../assets/svg/IcSocialGoogleplus";

import ComingSoon from "../ComingSoon";
import { withRouter, Link } from "react-router-dom";
import UserAccessModal from "../../../ModalPageSection/UserAccessModal";
import isUserLoggedIn from "../../../../utils/env/isLoggedin";
// import Ic from "../../../../assets/svg"

import "./index.scss";

const HomeAboutList = [
  {
    list: "About"
  },
  {
    list: "Careers"
  },
  {
    list : "Sign up"
  },
  {
    list: "Post job"
  },

  {
    list: "Upload resume"
  },
  {
    list: "Help center"
  },
  {
    list: "Privacy policy"
  },
  {
    list: "Copyright policy"
  }
]
 

export default class HomePageSubFooter extends Component {
  render() {
    
    const {FootComingSoon} = this.props;

    return (
      <div className="HomePageSubFooter">
        <Container>
          <div className="SubFooter_container">
            <div className="container_left">
              <div className="subscribeShare_container">
                <div className="subscribeShare_containerLeft">
                  <CardInputButton
                    label="Subscribe now"
                    // subTitleHead="subscribe now"
                    subTitle="jobs"
                    sendMessageTitle="for subscribing!"
                  />
                </div>
                <div className="subscribeShare_containerRight">
                  <Header as="h3">Connect with us</Header>
                  <p>We are always here for you.. </p>

                  <div className="SocialBtn">
                    <Button as="a" href={`https://www.facebook.com/vasitum/`} target="_blank"><Icon name='facebook f' /></Button>
                    <Button as="a" href={`https://twitter.com/Vasitum`} target="_blank"><Icon name='twitter' /></Button>
                    <Button as="a" href={`https://www.linkedin.com/company/vasitum/`} target="_blank"><Icon name='linkedin' /></Button>
                    <Button as="a" href={`https://www.youtube.com/channel/UCYbOspBTRIoFWUIFudkHVTw`} target="_blank"><Icon name='youtube' /></Button>
                    
                  </div>                 
                </div>
              </div>
              
            </div>
            
            {FootComingSoon}

          </div>
        </Container>
      </div>
    )
  }
}
