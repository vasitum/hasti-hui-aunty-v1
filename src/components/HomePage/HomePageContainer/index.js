import React from "react";

import { Button } from "semantic-ui-react";
import HomePageHeader from "../HomePageHeader";
import HomePageFeatures from "../HomePageFeatures";
import RecentPicksContainer from "../RecentPicksContainer";

import HomePageCategories from "../HomePageCategories";
import HomePageFooter from "../HomePageFooter";

import AboutCompanyContainer from "../HomePageFooter/AboutCompanyContainer";

import HomePageSubFooter from "../HomePageFooter/HomePageSubFooter";
import ComingSoon from "../HomePageFooter/ComingSoon";
import { withRouter, Link } from "react-router-dom";
import "./index.scss";

class HomePageContainer extends React.Component {
  render() {
    const TrendingKeywords = () => {
      return (
        <div className="recentSearch_tagRight">
          {/* <p>Trending Keywords</p>
          <div className="tag_btn">
            <Button  
              as={Link}
              to={`/search?query=${encodeURIComponent("IOS Developer")}`} 
              className="rounded_btn">IOS Developer</Button>
            <Button 
              as={Link}
              to={`/search?query=${encodeURIComponent("Android Developer")}`} className="rounded_btn">Android Developer</Button>
            <Button 
              as={Link}
              to={`/search?query=${encodeURIComponent("AI")}`} className="rounded_btn">AI </Button>

            <Button 
              as={Link}
              to={`/search?query=${encodeURIComponent("Architecture Designer")}`} className="rounded_btn">Architecture Designer</Button>
          </div> */}
        </div>
      );
    };

    const { isShowTrandingKey, showComingSoon, isShowSubFooter } = this.props;

    return (
      <div className="HomePageContainer">
        <HomePageHeader
          trandingKey={!isShowTrandingKey ? <TrendingKeywords /> : null}
        />
        <HomePageFeatures />
        {/* <HomePageCategories /> */}
        <RecentPicksContainer />
        <HomePageFooter
          isShowSubFooterTop={
            <HomePageSubFooter
            // FootComingSoon={<ComingSoon />}
            />
          }
          isShowSubFooter={!isShowSubFooter ? <AboutCompanyContainer /> : null}
        />
      </div>
    );
  }
}

export default HomePageContainer;
