import React from "react";
import { Responsive, Container } from "semantic-ui-react";
import HomePageContainer from "./HomePageContainer";
import HomePageMobile from "./HomePageMobile";
import ReactGA from "react-ga";
import "./index.scss";

class HomePage extends React.Component {
  componentDidMount() {
    ReactGA.pageview("/");
  }

  render() {
    return (
      <div className="HomePage">
        <Responsive maxWidth={1024}>
          <HomePageMobile />
        </Responsive>
        <Responsive minWidth={1025}>
          <HomePageContainer />
        </Responsive>
      </div>
    );
  }
}

export default HomePage;
