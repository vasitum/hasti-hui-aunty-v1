import React, { Component } from 'react';
import { Header } from "semantic-ui-react";
import PropTypes from "prop-types";

import "./index.scss";

class CategoryTitle extends Component {
  render() {
    
    const { headTitle, subTitle } = this.props;

    return (
      <div className="CategoryTitle">
        <p className="headTitle">{ headTitle }</p>
        <p>{ subTitle }</p>
      </div>
    )
  }
}

CategoryTitle.propTypes = {
  headTitle: PropTypes.string,
  subTitle: PropTypes.string
}

CategoryTitle.defaultProps = {
  headTitle:"headTitle",
  subTitle: "subTitle"
}

export default CategoryTitle;
