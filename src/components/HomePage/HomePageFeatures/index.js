import React, { Component } from "react";
import CategoryTitle from "../CategoryTitle";
import icFeature1 from "../../../assets/homePageAssets/ic_feature_1.png";
import icFeature2 from "../../../assets/homePageAssets/ic_feature_2.png";
import icFeature3 from "../../../assets/homePageAssets/ic_feature_3.png";
import IcFeatureDefaultImg from "../../../assets/homePageAssets/img_expl_1.jpg"
import IcFooterArrowIcon from "../../../assets/svg/IcFooterArrowIcon.js";
import { HOMEPAGE } from "../../../strings";


import RealtimeApplicationStatus from "../../../assets/img/Features/RealtimeApplicationStatus.png";
import SmartJobRecommendation from "../../../assets/img/Features/SmartJobRecommendation.png";
import FasterSearch from "../../../assets/img/Features/FasterSearch.png";
import AutomaticPreScreeningSchedulingTransparent from "../../../assets/img/Features/AutomaticPreScreeningSchedulingTransparent bg.png";

import "./index.scss";
import { Grid, Container } from "semantic-ui-react";

export default class HomePageFeatures extends Component {
  render() {
    const { FEATURES } = HOMEPAGE;
    return (
      <Container>
        <div className="HomePageFeaturesClass">
          {/* <div className="HomePageFeaturesHeadingContainer">
            <h2 className="HomePageFeaturesHeading">Features</h2>
            <span className="HomepageFeaturesSubHeading">
              {FEATURES.subTitle}
            </span>
          </div> */}
          <CategoryTitle
            headTitle={FEATURES.TITLE}
            subTitle={FEATURES.SUBTITLE}
          />

          <Grid>
            <Grid.Row className="HomePageFeaturesRow">
              <Grid.Column
                computer={6}
                mobile={16}
                className="HomePageFeaturesRowLeft">
                <img src={RealtimeApplicationStatus} alt="Free job alert" />
              </Grid.Column>
              <Grid.Column
                computer={10}
                mobile={16}
                className="HomePageFeaturesRowRight">
                <div className="HomePageFeaturesRowRightContent">
                  <div className="featuresHeader">
                    {/* <div className="numberCircle">
                      <span>1</span>
                    </div> */}
                    <div className="featuresHeader_left">
                      <h2>{FEATURES.FIRST_FEATURE_TITLE}</h2>
                      <p>{FEATURES.FIRST_FEATURE_DESCR}</p>
                      {/* <button className="viewMoreBtn">
                          View More{" "}
                          <IcFooterArrowIcon
                            pathcolor="#0b9ed0"
                            height="7px"
                            width="11px"
                          />
                        </button> */}
                    </div>
                  </div>
                </div>
              </Grid.Column>
            </Grid.Row>

            <Grid.Row className="HomePageFeaturesRow2">
              <Grid.Column
                computer={10}
                mobile={16}
                className="HomePageFeaturesRowRight2">
                <div className="HomePageFeaturesRowRightContent2">
                  <div className="featuresHeader">
                    <div className="featuresHeader_left">
                      <h2>{FEATURES.SECND_FEATURE_TITLE}</h2>
                      <p>{FEATURES.SECND_FEATURE_SUBTITLE}</p>
                      {/* <button className="viewMoreBtn">
                          View More{" "}
                          <IcFooterArrowIcon
                            pathcolor="#0b9ed0"
                            height="7px"
                            width="11px"
                          />
                        </button> */}
                    </div>
                    {/* <div className="numberCircle2">
                      <span>2</span>
                    </div> */}
                  </div>
                </div>
              </Grid.Column>
              <Grid.Column
                computer={6}
                mobile={16}
                className="HomePageFeaturesRowLeft2">
                <img src={SmartJobRecommendation} alt="job" />
              </Grid.Column>
            </Grid.Row>

            <Grid.Row className="HomePageFeaturesRow">
              <Grid.Column
                computer={6}
                mobile={16}
                className="HomePageFeaturesRowLeft">
                <img src={FasterSearch} alt="resume maker" />
              </Grid.Column>
              <Grid.Column
                computer={10}
                mobile={16}
                className="HomePageFeaturesRowRight">
                <div className="HomePageFeaturesRowRightContent">
                  <div className="featuresHeader">
                    {/* <div className="numberCircle">
                      <span>3</span>
                    </div> */}
                    <div className="featuresHeader_left">
                      <h2>{FEATURES.THIRD_FEATURE_TITLE}</h2>
                      <p>{FEATURES.THIRD_FEATURE_SUBTITLE}</p>
                      {/* <button className="viewMoreBtn">
                          View More{" "}
                          <IcFooterArrowIcon
                            pathcolor="#0b9ed0"
                            height="7px"
                            width="11px"
                          />
                        </button> */}
                    </div>
                  </div>
                </div>
              </Grid.Column>
            </Grid.Row>

            <Grid.Row className="HomePageFeaturesRow2">
              <Grid.Column
                computer={10}
                mobile={16}
                className="HomePageFeaturesRowRight2">
                <div className="HomePageFeaturesRowRightContent2">
                  <div className="featuresHeader">
                    <div className="featuresHeader_left">
                      <h2>{FEATURES.FORTH_FEATURE_TITLE}</h2>
                      <p>{FEATURES.FORTH_FEATURE_SUBTITLE}</p>
                      {/* <button className="viewMoreBtn">
                          View More{" "}
                          <IcFooterArrowIcon
                            pathcolor="#0b9ed0"
                            height="7px"
                            width="11px"
                          />
                        </button> */}
                    </div>
                    {/* <div className="numberCircle2">
                      <span>4</span>
                    </div> */}
                  </div>
                </div>
              </Grid.Column>
              <Grid.Column
                computer={6}
                mobile={16}
                className="HomePageFeaturesRowLeft2">
                <img src={AutomaticPreScreeningSchedulingTransparent} alt="free job" />
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>
      </Container>
    );
  }
}
