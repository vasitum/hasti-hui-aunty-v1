import React, { Component } from "react";
import "./index.scss";
import AccordionCardContainer from "../../Cards/AccordionCardContainer";
import DownArrowCollaps from "../../utils/DownArrowCollaps";
import MobileHomePageFooterUpperOne from "./MobileHomePageFooterUpperOne";
import MobileHomePageFooterUpperTwo from "./MobileHomePageFooterUpperTwo";
import MobileHomePageFooterUpperThree from "./MobileHomePageFooterUpperThree";

export default class MobileHomePageFooterUpper extends Component {
  render() {
    const MobileHomePageFooterUpperHeader = (
      <div className="MobileHomePageFooterUpperHeader">
        <div className="MobileHomePageFooterUpperHeaderLeft">
          <span>Top Cities</span>
        </div>
        <div className="MobileHomePageFooterUpperHeaderRight">
          <DownArrowCollaps />
        </div>
      </div>
    );

    const MobileHomePageFooterUpperHeaderTwo = (
      <div className="MobileHomePageFooterUpperHeader">
        <div className="MobileHomePageFooterUpperHeaderLeft">
          <span>Top Skills</span>
        </div>
        <div className="MobileHomePageFooterUpperHeaderRight">
          <DownArrowCollaps />
        </div>
      </div>
    );

    // const MobileHomePageFooterUpperHeaderThree = (
    //   <div className="MobileHomePageFooterUpperHeader">
    //     <div className="MobileHomePageFooterUpperHeaderLeft">
    //       <span>Trending Jobs</span>
    //     </div>
    //     <div className="MobileHomePageFooterUpperHeaderRight">
    //       <DownArrowCollaps />
    //     </div>
    //   </div>
    // );

    return (
      <div className="MobileHomePageFooterUpper">
        <AccordionCardContainer cardHeader={MobileHomePageFooterUpperHeader}>
          <MobileHomePageFooterUpperOne />
        </AccordionCardContainer>
        <AccordionCardContainer cardHeader={MobileHomePageFooterUpperHeaderTwo}>
          <MobileHomePageFooterUpperTwo />
        </AccordionCardContainer>
        {/* <AccordionCardContainer
          cardHeader={MobileHomePageFooterUpperHeaderThree}>
          <MobileHomePageFooterUpperThree />
        </AccordionCardContainer> */}
      </div>
    );
  }
}
