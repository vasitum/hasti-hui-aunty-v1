import React from "react";
import { Link } from "react-router-dom";
import "./index.scss";

export default function MobileHomePageFooterUpperOne() {
  return (
    <div className="MobileHomePageFooterUpperOne">
      <div className="MobileHomePageFooterItem">
        <Link
          to={`/search?query=${encodeURIComponent("Gurugram")}`}>
          Gurugram
        </Link>
      </div>
      <div className="MobileHomePageFooterItem">
        <Link
          to={`/search?query=${encodeURIComponent("Hyderabad")}`}>
          Hyderabad
        </Link>
      </div>
      <div className="MobileHomePageFooterItem">
        <Link
          to={`/search?query=${encodeURIComponent("Delhi")}`}>
          Delhi
        </Link>
        
      </div>
      <div className="MobileHomePageFooterItem">
        <Link
          to={`/search?query=${encodeURIComponent("Pune")}`}>
          Pune
        </Link>
      </div>
      <div className="MobileHomePageFooterItem">
        <Link
          to={`/search?query=${encodeURIComponent("Noida")}`}>
          Noida
        </Link>
      </div>
      <div className="MobileHomePageFooterItem">
        <Link
          to={`/search?query=${encodeURIComponent("Chandigarh")}`}>
          Chandigarh
        </Link>
      </div>
      <div className="MobileHomePageFooterItem">
        <Link
          to={`/search?query=${encodeURIComponent("Delhi/NCR")}`}>
          Delhi/NCR
        </Link>
      </div>
      <div className="MobileHomePageFooterItem">
        <Link
          to={`/search?query=${encodeURIComponent("Maharashtra")}`}>
          Maharashtra
        </Link>
      </div>
      <div className="MobileHomePageFooterItem">
        <Link
          to={`/search?query=${encodeURIComponent("Banglore")}`}>
          Banglore
        </Link>
      </div>
      <div className="MobileHomePageFooterItem">
        <Link
          to={`/search?query=${encodeURIComponent("Mumbai")}`}>
          Mumbai
        </Link>
      </div>
    </div>
  );
}
