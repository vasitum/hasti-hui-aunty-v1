import React from "react";
import { Link } from "react-router-dom";
import "./index.scss";

export default function MobileHomePageFooterUpperTwo() {
  return (
    <div className="MobileHomePageFooterUpperTwo">
      <div className="MobileHomePageFooterItem">
        <Link
          to={`/search?query=${encodeURIComponent("Mechanical")}`}>
          Mechanical
        </Link>
        
      </div>
      <div className="MobileHomePageFooterItem">
        <Link
          to={`/search?query=${encodeURIComponent("Design Engineer")}`}>
          Design Engineer
        </Link>
        
      </div>
      <div className="MobileHomePageFooterItem">
        <Link
          to={`/search?query=${encodeURIComponent("BPO")}`}>
          BPO
        </Link>
        
      </div>
      <div className="MobileHomePageFooterItem">
        <Link
          to={`/search?query=${encodeURIComponent("Analytics")}`}>
          Analytics
        </Link>
        
      </div>
      <div className="MobileHomePageFooterItem">
        <Link
          to={`/search?query=${encodeURIComponent("Networking")}`}>
          Networking
        </Link>
        
      </div>
      <div className="MobileHomePageFooterItem">
        <Link
          to={`/search?query=${encodeURIComponent("UI/UX")}`}>
          UI/UX
        </Link>
        
      </div>
      <div className="MobileHomePageFooterItem">
        <Link
          to={`/search?query=${encodeURIComponent("Online Marketing")}`}>
          Online Marketing
        </Link>
        
      </div>
      <div className="MobileHomePageFooterItem">
        <Link
          to={`/search?query=${encodeURIComponent("Banking")}`}>
          Banking
        </Link>
        
      </div>
      <div className="MobileHomePageFooterItem">
        <Link
          to={`/search?query=${encodeURIComponent("Animation")}`}>
          Animation
        </Link>
        
      </div>
      <div className="MobileHomePageFooterItem">
        <Link
          to={`/search?query=${encodeURIComponent("Teaching")}`}>
          Teaching
        </Link>
        
      </div>
    </div>
  );
}
