import React from "react";
import "./index.scss";

export default function MobileHomePageFooterUpperThree() {
  return (
    <div className="MobileHomePageFooterUpperThree">
      <div className="MobileHomePageFooterItem">Android Developer</div>
      <div className="MobileHomePageFooterItem">Analyst</div>
      <div className="MobileHomePageFooterItem">Marketing Executive</div>
      <div className="MobileHomePageFooterItem">UX Architecture</div>
      <div className="MobileHomePageFooterItem">Full stack developer</div>
      <div className="MobileHomePageFooterItem">UI/UX designer</div>
      <div className="MobileHomePageFooterItem">Frontend developer</div>
      <div className="MobileHomePageFooterItem">PPC expert</div>
      <div className="MobileHomePageFooterItem">Data scientist</div>
      <div className="MobileHomePageFooterItem">Blogger</div>
    </div>
  );
}
