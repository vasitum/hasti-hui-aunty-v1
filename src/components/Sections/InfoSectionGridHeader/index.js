import React from "react";
import PropTypes from "prop-types";
import { Header, Grid, Button } from "semantic-ui-react";

import IcEditIcon from "../../../assets/svg/IcEdit";

import "./index.scss";

const InfoSectionGridHeader = props => {
  const {
    headerClasses,
    color,
    headerText,
    children,
    rightHeaderText
  } = props;

  // Our svgs are react elements hence needs captialization
  

  return (
    <div className="InfoSectionGridHeader">
      <div className="header-container">
        <Grid>
          <Grid.Row>
            <Grid.Column width="10">
              <Header
                className={headerClasses + " header-item"}
                color={color}
                as="h2">
                {headerText}
              </Header>
            </Grid.Column>
            <Grid.Column width="6" textAlign="right">
              <Button className="header_editBtn">
                {rightHeaderText}
              </Button>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
      <div>{children}</div>
    </div>
  );
};

// InfoSectionGridHeader.propTypes = {
//   rightHeaderText: PropTypes.any
// };

// InfoSectionGridHeader.defaultProps = {};

export default InfoSectionGridHeader;
