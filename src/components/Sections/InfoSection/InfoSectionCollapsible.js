import React from "react";
import PropTypes from "prop-types";
import { Accordion, Header, Grid } from "semantic-ui-react";

class InfoSectionCollapsible extends React.Component {
  state = {
    isActive: this.props.isActive || false // default false;
  };

  onAccordionClick = (e, titleProps) => {
    this.setState({
      isActive: !this.state.isActive
    });
  };

  render() {
    const { isActive } = this.state;
    const {
      color,
      headerIcon,
      headerClasses,
      headerText,
      children,
      subHeaderIcon,
      subHeaderText,
      rightIcon,
      InfoSecLeftColumn,
      InfoSecRightColumn,
      rightIconSubHeader
    } = this.props;

    if (subHeaderText) {
      // Our svgs are react elements hence needs captialization
      const IcSectionHeader = subHeaderIcon;

      return (
        <div className="InfoSection is-collapsible is-subheader">
          <Accordion>
            <Accordion.Title active={isActive} onClick={this.onAccordionClick}>
              {/* <div className="subheader-container"> */}
              {/* <Header
                  className={headerClasses + " header-item"}
                  color={color}
                  as="h2">
                  {headerText}
                </Header>
                <IcSectionHeader className="header-item" pathcolor="#acaeb5" />
                <Header.Subheader className="header-item">
                  {subHeaderText}
                </Header.Subheader> */}

              <Grid>
                <Grid.Row>
                  <Grid.Column computer={InfoSecLeftColumn}>
                    <div className="subheader-container">
                      <Header
                        className={headerClasses + " header-item"}
                        color={color}
                        as="h2">
                        {headerText}
                      </Header>
                      <IcSectionHeader
                        className="header-item"
                        pathcolor="#acaeb5"
                      />
                      <Header.Subheader className="header-item">
                        {subHeaderText}
                      </Header.Subheader>
                    </div>
                  </Grid.Column>

                  <Grid.Column computer={InfoSecRightColumn} textAlign="right">
                    {rightIconSubHeader}
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Accordion.Title>
            <Accordion.Content active={isActive}>{children}</Accordion.Content>
          </Accordion>
        </div>
      );
    }

    return (
      <div className="InfoSection is-collapsible">
        <Accordion>
          <Accordion.Title active={isActive} onClick={this.onAccordionClick}>
            <Header color={color} className={headerClasses} as="h2">
              <Grid>
                <Grid.Row>
                  <Grid.Column computer={InfoSecLeftColumn}>
                    {headerIcon}
                    {headerText}
                  </Grid.Column>
                  <Grid.Column
                    
                    computer={InfoSecRightColumn}
                    textAlign="right"
                    style={{ top: "-4px" }}>
                    {rightIcon}
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Header>
          </Accordion.Title>
          <Accordion.Content active={isActive}>{children}</Accordion.Content>
        </Accordion>
      </div>
    );
  }
}

InfoSectionCollapsible.propTypes = {
  InfoSecLeftColumn:PropTypes.string,
  InfoSecRightColumn: PropTypes.string
};

InfoSectionCollapsible.defaultProps = {
  InfoSecLeftColumn: "12",
  InfoSecRightColumn: "4"
};

export default InfoSectionCollapsible;
