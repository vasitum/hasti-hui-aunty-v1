import React from "react";
import PropTypes from "prop-types";
import { Header, Grid } from "semantic-ui-react";

const InfoSectionSubHeader = props => {
  const {
    headerClasses,
    color,
    headerText,
    subHeaderIcon,
    subHeaderText,
    children,
    rightIcon
  } = props;

  // Our svgs are react elements hence needs captialization
  const IcSectionHeader = subHeaderIcon;

  return (
    <div className="InfoSection is-subheader">
      <div className="subheader-container">
        <Header
          className={headerClasses + " header-item"}
          color={color}
          as="h2">
          {headerText}
        </Header>
        <IcSectionHeader className="header-item" pathcolor="#acaeb5" />
        <Header.Subheader className="header-item">
          {subHeaderText}
        </Header.Subheader>

        {/* <Grid>
          <Grid.Row>
            <Grid.Column mobile={10} computer={12}>
              <Header
                className={headerClasses + " header-item"}
                color={color}
                as="h2">
                {headerText}
              </Header>
              <IcSectionHeader className="header-item" pathcolor="#acaeb5" />
              <Header.Subheader className="header-item">
                {subHeaderText}
              </Header.Subheader>
            </Grid.Column>
            <Grid.Column mobile={6} computer={4} textAlign="right">
              {rightIcon}
            </Grid.Column>
          </Grid.Row>
        </Grid> */}
      </div>
      <div>{children}</div>
    </div>
  );
};

InfoSectionSubHeader.propTypes = {};

InfoSectionSubHeader.defaultProps = {};

export default InfoSectionSubHeader;
