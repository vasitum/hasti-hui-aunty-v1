import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";

import { Header, Grid } from "semantic-ui-react";

import InfoSectionCollapsible from "./InfoSectionCollapsible";
import InfoSectionSubHeader from "./InfoSectionSubHeader";

import "./index.scss";

/**
 *
 * @Component
 * @defines
 * {
 *  headerSize: 'small'| 'medium' | 'large',
 *  headerText: 'Any Text',
 *  subHeaderText: 'Any Text',
 *  subHeaderIcon: 'Vasitum.Svg'
 *  collpasible: Boolean,
 *  color: 'color of header'
 *  children: React.Component | Any;
 * }
 */
const InfoSection = ({
  collapsible,
  headerSize,
  headerText,
  color,
  subHeaderText,
  rightHeader,
  rightIcon,
  InfoSecLeftColumn,
  InfoSecRightColumn,
  ...props
}) => {
  const headerClasses = cx({
    [`is-${headerSize}`]: true
  });

  if (collapsible) {
    return (
      <InfoSectionCollapsible
        headerClasses={headerClasses}
        headerText={headerText}
        subHeaderText={subHeaderText}
        InfoSecLeftColumn={InfoSecLeftColumn}
        InfoSecLeftColumn={InfoSecRightColumn}
        color="blue"
        {...props}
      />
    );
  }

  if (subHeaderText) {
    return (
      <InfoSectionSubHeader
        headerClasses={headerClasses}
        headerText={headerText}
        color="blue"
        subHeaderText={subHeaderText}
        {...props}
      />
    );
  }

  return (
    <div className="InfoSection">
      {/* <Header className={headerClasses} color={color} as="h2">
        {props.headerIcon}
        {headerText}
        
      </Header> */}
      <Grid>
        <Grid.Row>
          <Grid.Column width={12}>
            <Header className={headerClasses} color={color} as="h2">
              {props.headerIcon}
              {headerText}
            </Header>
          </Grid.Column>
          <Grid.Column width={4}>{rightHeader}</Grid.Column>
        </Grid.Row>
      </Grid>
      {props.children}
    </div>
  );
};

InfoSection.propTypes = {
  headerSize: PropTypes.string,
  headerText: PropTypes.string,
  subHeaderText: PropTypes.string,
  subHeaderIcon: PropTypes.any,
  headerIcon: PropTypes.any,
  collapsible: PropTypes.bool,
  color: PropTypes.string,
  children: PropTypes.any,
  InfoSecLeftColumn:PropTypes.string,
  InfoSecRightColumn: PropTypes.string
};

InfoSection.defaultProps = {
  headerSize: "small",
  headerText: "Job Description",
  color: "blue",
  // InfoSecLeftColumn:"12",
  // InfoSecRightColumn: "4"
};

export default InfoSection;
