import React from 'react'
import { Grid, Image ,Button } from 'semantic-ui-react';
import IcCloseIcon from "../../assets/svg/IcCloseIcon";
import "./index.scss";

const HeaderApply = props =>{ 
    return(
  <Grid columns={2} className="Grid_Main">
    <Grid.Row className="Grid_Row1">
      <Grid.Column className="Grid_Column1">
        <p>Filter jobs</p>
      </Grid.Column>
      <Grid.Column className="Grid_Column2">
      <Button className="Button_Apply" basic color='rgba(11, 158, 208, 1)'>
        <p>Apply</p>
        </Button>
        <Button compact inverted className="Button_Close" color="#1f2532">
            <IcCloseIcon pathcolor="#6e768a" height="14" width="14" />
        </Button>
      </Grid.Column>
    </Grid.Row>

    <Grid.Row className="Grid_Row2">
           <Button compact inverted className="Button_Close">
            <IcCloseIcon pathcolor="#0b9ed0" height="10" width="10" />
          </Button>
          <p>Clear all applied filters</p>
    </Grid.Row>      
</Grid>
)
}

export default HeaderApply
