import React from "react";

import InputField from "../../Forms/FormFields/InputField";
// import { Form } from "semantic-ui-react";
import { Form } from "formsy-semantic-ui-react";
import EmailVerificationFooter from "../EmailVerificationFooter";

import ReactGA from "react-ga";
import { USER } from "../../../constants/api";
import { EmailVerificationString } from "../../EventStrings/EmailVerificationString";

import "./index.scss";

class EmailVerificationBody extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: ""
    };
  }

  ontextChange = (e, { value }) => {
    this.setState({ text: value }, () => {
      const { text } = this.state;
      const { onSubmit } = this.props;
      if (text.length === 4 && !isNaN(Number(text))) {
        onSubmit(text);
      }
    });
  };

  onSubmit = e => {
    const UserId = window.localStorage.getItem(USER.UID);
    const { onSubmit } = this.props;
    const { text } = this.state;
    if (!text) {
      return;
    }

    onSubmit(text);
    ReactGA.event({
      category: EmailVerificationString.EmailVerificationEvent.category,
      action: EmailVerificationString.EmailVerificationEvent.action,
      value: UserId,
    });
  };

  render() {
    const { props } = this;
    
    const { isShowResendOtp, email, onSubmit } = props;
    return (
      <div className="EmailVerificationBody">
        <div className="EmailVerification_innerBody">
          <p>
            A verification code has been sent to you “<span>{email}</span>”
          </p>
          <p className="subTitle">Copy &amp; Paste code here to proceed. </p>

          <div className="emailInputField">
            <Form onSubmit={this.onSubmit}>
              <InputField
                placeholder="Enter OTP"
                name="enter_otp"
                maxLength="4"
                onChange={this.ontextChange}
                value={this.state.text}
              />
              <button type={"submit"} hidden 
                
              />
            </Form>
          </div>

          {isShowResendOtp ? (
            <div className="resend_opt">
              <EmailVerificationFooter isShowBobackBtn />
            </div>
          ) : null}
        </div>
      </div>
    );
  }
}

export default EmailVerificationBody;
