import React, { Component } from "react";

import { Responsive } from "semantic-ui-react";

import EmailVerificationContainer from "./EmailVerificationContainer";
import EmailVerificationMobile from "./EmailVerificationMobile";
import ScreenMainContainer from "../ScreenMainContainer";
import { withUserContextConsumer } from "../../contexts/UserContext";
import sendOtpEmailVerify from "../../api/user/sendOtpEmailVerify";
import verifyEmailOtp from "../../api/user/verifyEmailOtp";
import "./index.scss";
import { toast } from "react-toastify";
import { parse } from "qs";

class EmailVerification extends Component {
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.onResend = this.onResend.bind(this);
  }

  state = {
    isVerified: false
  };

  async onSubmit(otp) {
    const { user } = this.props;
    if (!user) {
      return;
    }
    const { id } = user;
    try {
      const res = await verifyEmailOtp(id, otp);
      if (res.status === 200) {
        this.setState(
          {
            isVerified: true
          },
          () => {
            this.props.onUserChange({
              isVerified: true
            });

            const qs = parse(window.location.search);

            setTimeout(() => {
              if (Object.prototype.hasOwnProperty.call(qs, "?action")) {
                switch (qs["?action"]) {
                  case "cv-template":
                    this.props.history.push("/cv-template");
                    break;
                  default:
                    this.props.history.push(
                      `/create/profile${
                        window.location.search ? window.location.search : ""
                      }`
                    );
                }
                return;
              }

              this.props.history.push(
                `/create/profile${
                  window.location.search ? window.location.search : ""
                }`
              );
            }, 5000);
          }
        );
      }
    } catch (error) {
      console.error(error);
    }
  }

  onGoBack = e => {
    // signout
    window.localStorage.clear();
    this.props.history.push("/?action=sign_up");
  };

  async onResend() {
    const { user } = this.props;
    if (!user) {
      return;
    }
    const { id } = user;
    try {
      const res = await sendOtpEmailVerify(id);
      if (res.status === 200) {
        toast("OPT sent on your email");
      }
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const { user } = this.props;
    return (
      <div className="EmailVerification screenMain_container">
        <Responsive maxWidth={1024}>
          <EmailVerificationMobile
            verified={this.state.isVerified}
            onSubmit={this.onSubmit}
            onResend={this.onResend}
            email={user.email}
            goBack={this.onGoBack}
          />
        </Responsive>
        <Responsive minWidth={1025}>
          <ScreenMainContainer>
            <EmailVerificationContainer
              verified={this.state.isVerified}
              onSubmit={this.onSubmit}
              onResend={this.onResend}
              email={user.email}
              goBack={this.onGoBack}
            />
          </ScreenMainContainer>
        </Responsive>
      </div>
    );
  }
}

export default withUserContextConsumer(EmailVerification);
