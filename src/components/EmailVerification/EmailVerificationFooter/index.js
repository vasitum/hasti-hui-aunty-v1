import React from "react";

import FlatDefaultBtn from "../../Buttons/FlatDefaultBtn";

import "./index.scss";

const EmailVerificationFooter = props => {
  const { isShowBobackBtn, isShowResendBtn, onResend, onGoBack } = props;
  return (
    <div className="EmailVerificationFooter">
      {!isShowBobackBtn ? (
        <div className="VerificationFooter_left">
          <p>
            Wrong email?
            <FlatDefaultBtn
              onClick={onGoBack}
              className="noPaddingBtn"
              btntext="Go back"
            />
          </p>
        </div>
      ) : null}

      {!isShowResendBtn ? (
        <div className="VerificationFooter_right">
          <FlatDefaultBtn
            onClick={onResend}
            className="noPaddingBtn"
            btntext="Resend verification code"
          />
        </div>
      ) : null}
    </div>
  );
};

export default EmailVerificationFooter;
