import React, { Component } from "react";

import { Button, Container } from "semantic-ui-react";
import MobileHeader from "../../MobileComponents/MobileHeader";

import IcFooterArrowIcon from "../../../assets/svg/IcFooterArrowIcon";

import EmailVerificationBody from "../EmailVerificationBody";

import EmailVerificationFooter from "../EmailVerificationFooter";

import EmailVerificationContainer from "../EmailVerificationContainer";
// import MobileFooter from "../../MobileComponents/MobileFooter";
import "./index.scss";

export default class EmailVerificationMobile extends Component {
  render() {
    return (
      <div className="EmailVerificationMobile">
        <div className="EmailVerificationMobile_header">
          <MobileHeader
            className="isMobileHeader_fixe"
            headerLeftIcon={
              <Button
                // onClick={e => this.props.history.go(-1)}
                className="backArrowBtn">
                <IcFooterArrowIcon pathcolor="#6e768a" />
              </Button>
            }
            headerTitle={"Email verification"}
            mobileHeaderLeftColumn={16}
          />
        </div>
        <div className="EmailVerificationMobile_body">
          {/* <Container>
            <EmailVerificationBody 
              isShowResendOtp 
            />
          </Container> */}
          <EmailVerificationContainer {...this.props} isShowHeader />
        </div>

        {/* <div className="EmailVerificationMobile_footer">
          <EmailVerificationFooter 
            isShowResendBtn 
          />
        </div> */}
      </div>
    );
  }
}
