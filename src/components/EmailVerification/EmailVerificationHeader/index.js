import React from "react";

import "./index.scss";

import IcEmailConfimation from "../../../assets/svg/IcEmailConfimation";

const EmailVerificationHeader = () => {
  return(
    <div className="EmailVerificationHeader">
      <IcEmailConfimation 
        pathcolor1="#ffffff"
        pathcolor2="#ceeefe"
        pathcolor3="#ceeefe"
        pathcolor4="#ceeefe"
        pathcolor5="#ceeefe"
        pathcolor6="#ceeefe"
        pathcolor7="#ceeefe"
        pathcolor8="#acaeb5"
        pathcolor9="#ceeefe"
        pathcolor10="#ceeefe"
        pathcolor11="#ceeefe"
        pathcolor12="#acaeb5"
        pathcolor13="#acaeb5"
      />
    </div>
  )
}

export default EmailVerificationHeader;