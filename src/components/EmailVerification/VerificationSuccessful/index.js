import React from "react";

import { Header } from "semantic-ui-react";
import IcEmailConfimerd from "../../../assets/svg/IcEmailConfimerd";

import "./index.scss";

const VerificationSuccessful = () => {
  return(
    <div className="VerificationSuccessful">
      <div className="VerificationSuccessful_header">
        <IcEmailConfimerd 
          pathcolor1="#0b9ed0"
          pathcolor2="rgba(50, 50, 50, 0.2)"
          pathcolor3="rgba(50, 50, 50, 0.3)"
          pathcolor4="rgba(50, 50, 50, 0.2)"
          pathcolor5="rgba(50, 50, 50, 0.3)"
          pathcolor6="rgba(50, 50, 50, 0.2)"
          pathcolor7="rgba(50, 50, 50, 0.3)"
          pathcolor8="#ffffff"
          pathcolor9="#ffffff"
          pathcolor10="#ffffff"
          pathcolor11="rgba(50, 50, 50, 0.2)"
          pathcolor12="rgba(50, 50, 50, 0.3)"
        />
        <div class="checkmark draw"></div>
      </div>
      <div className="VerificationSuccessful_footer">
        <Header>Verification successful</Header>
      </div>
    </div>
  )
}

export default VerificationSuccessful;