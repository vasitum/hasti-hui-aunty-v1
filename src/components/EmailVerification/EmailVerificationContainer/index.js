import React, { Component } from "react";

import { Container } from "semantic-ui-react";
import EmailVerificationHeader from "../EmailVerificationHeader";
import EmailVerificationBody from "../EmailVerificationBody";
import EmailVerificationFooter from "../EmailVerificationFooter";
import VerificationSuccessful from "../VerificationSuccessful";

import "./index.scss";

export default class EmailVerificationContainer extends Component {
  render() {
    const {
      isShowHeader,
      email,
      onSubmit,
      onResend,
      goBack,
      verified
    } = this.props;
    return (
      <div className="EmailVerificationContainer">
        <Container>
          {!verified ? (
            <div className="EmailVerification_innerContainer">
              {!isShowHeader ? <EmailVerificationHeader /> : null}

              <EmailVerificationBody
                email={email}
                onSubmit={onSubmit}
                onResend={onResend}
              />
              <EmailVerificationFooter onGoBack={goBack} onResend={onResend} />
            </div>
          ) : (
            <VerificationSuccessful />
          )}
        </Container>
      </div>
    );
  }
}
