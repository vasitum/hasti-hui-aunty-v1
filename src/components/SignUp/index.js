import React from "react";

import  { Form, List, Button, Icon, Input, Image, Checkbox} from "semantic-ui-react";

import InputField from "../Forms/FormFields/InputField";

import facebook from "../../assets/img/facebook.png";
import google from "../../assets/img/google.png"
import CheckboxField from "../Forms/FormFields/CheckboxField";
import "./index.scss";


const MyUserSignUp = props =>{
  
  return(
    <div className="UserSignUp">
      <List className="social_login">
        <List.Content className="social_loginContent">
          <List.Description >
            <List horizontal>
              <List.Item as="a">
                <Image src={facebook} />
                <List.Content className="">
                  Facebook
                </List.Content>
              </List.Item>
              <List.Item as="a" className="">
                <Image src={google} />
                <List.Content className="">
                  Google
                </List.Content>
              </List.Item>
            </List>
          </List.Description>
        </List.Content>
        <List.Content>
          <Form className="UserSignUp_form">
          <InputField placeholder ="Email"/>
            
            <InputField placeholder ="password"/>
            

            <CheckboxField checkboxLabel="Hello"/>

            <Button primary type="submit" className="" fluid>
              {props.siginbtntext} <Icon name="long arrow right" />
            </Button>
          </Form>
        </List.Content>
      </List>

    </div>
  )
}

export default MyUserSignUp;