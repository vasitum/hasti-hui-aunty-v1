import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";

import { Header } from "semantic-ui-react";

import FilterAcordian from "./FilterAcordian";
import "./index.scss";

/**
 *
 * @Component
 * @defines
 * {
 *  headerSize: 'small'| 'medium' | 'large',
 *  headerText: 'Any Text',
 *  subHeaderText: 'Any Text',
 *  subHeaderIcon: 'Vasitum.Svg'
 *  collpasible: Boolean,
 *  color: 'color of header'
 *  children: React.Component | Any;
 * }
 */
const FilterCollapsible = ({
  collapsible,
  headerSize,
  headerText,
  color,
  subHeaderText,
  ...props
}) => {
  const headerClasses = cx({
    [`is-${headerSize}`]: true
  });

  if (collapsible) {
    return (
      <FilterAcordian
        headerClasses={headerClasses}
        headerText={headerText}
        subHeaderText={subHeaderText}
        // color="blue"
        {...props}
      />
    );
  }


  return (
    <div className="FilterCollapsible">
      <Grid>
        <Grid.Row>
          <Grid.Column width={10}>
            <Header className={headerClasses} color={color} as="h3">
              {headerText}
            </Header>
          </Grid.Column>
          <Grid.Column width={6} textAlign="right">
            {/* <Icon name="angle down" className="vas_accordionArrow" circular /> */}
            {props.headerIcon}
          </Grid.Column>
        </Grid.Row>
      </Grid>
      
      {props.children}
    </div>
  );
};

FilterCollapsible.propTypes = {
  headerSize: PropTypes.string,
  headerText: PropTypes.string,
  subHeaderText: PropTypes.string,
  subHeaderIcon: PropTypes.any,
  headerIcon: PropTypes.any,
  collapsible: PropTypes.bool,
  color: PropTypes.string,
  children: PropTypes.any
};

FilterCollapsible.defaultProps = {
  headerSize: "small",
  headerText: "Job Description",
  color: "blue"
};

export default FilterCollapsible;
