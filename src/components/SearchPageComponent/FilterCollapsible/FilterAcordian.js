import React from "react";
import PropTypes from "prop-types";
import { Accordion, Header, Grid } from "semantic-ui-react";

class FilterAcordian extends React.Component {
  state = {
    isActive: false // default false;
  };

  onAccordionClick = (e, titleProps) => {
    this.setState({
      isActive: !this.state.isActive
    });
  };

  render() {
    const { isActive } = this.state;
    const {
      color,
      headerIcon,
      headerClasses,
      headerText,
      children,
      subHeaderIcon,
      subHeaderText
    } = this.props;

    return (
      <div className="FilterAcordian">
        <Accordion>
          <Accordion.Title active={isActive} onClick={this.onAccordionClick}>
            <Grid>
              <Grid.Row>
                <Grid.Column width={10}>
                  <Header color={color} className={headerClasses} as="h3">
                    { headerText }
                  </Header>
                </Grid.Column>
                <Grid.Column width={6} textAlign="right">
                  { headerIcon }
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Accordion.Title>
          <Accordion.Content active={isActive}>{children}</Accordion.Content>
        </Accordion>
      </div>
    );
  }
}

FilterAcordian.propTypes = {};

FilterAcordian.defaultProps = {};

export default FilterAcordian;
