import React from "react";
import PropTypes from "prop-types";

import { Responsive } from "semantic-ui-react";

import PostLoginMobileFooter from "../MobileComponents/PostLoginMobileFooter";
import { Switch, Route } from "react-router-dom";
// import "./index.scss";

/**
 * @augments {Component<{isUserLoggedIn:boolean.isRequired>}
 */
const Footer = props => (
  <React.Fragment>
    {/* Desktop */}
    <Responsive minWidth={1025}>
      <div />
    </Responsive>

    {/* Mobile */}
    <Responsive maxWidth={1024}>
      <Switch>
        <Route path={"/messaging"} component={() => <div />} />
        <Route exact path="/job/dashboard" component={() => <div />} />
        <Route exact path="/job/candidate/:id" component={() => <div />} />
        <Route exact path="/job/recruiter/:id" component={() => <div />} />
        <Route exact path="/job/candidateDetail" component={() => <div />} />
        <Route exact path="/job/myJobsDetail" component={() => <div />} />
        <Route
          exact
          path="/job/myJobsDetailCondidate"
          component={() => <div />}
        />
        <Route exact path="/job/view/:id" component={() => <div />} />

        <Route
          exact
          path="/job/candidate/:jobId/application/:applicationId"
          component={() => <div />}
        />
        <Route
          exact
          path="/job/:jobId/application/:applicationId"
          component={() => <div />}
        />
        <Route component={() => <PostLoginMobileFooter />} />
      </Switch>
    </Responsive>
  </React.Fragment>
);

Footer.propTypes = {
  isUserLoggedIn: PropTypes.bool.isRequired
};

Footer.defaultProps = {
  isUserLoggedIn: false
};

export default Footer;
