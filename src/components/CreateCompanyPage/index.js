import React from "react";

import { Button, Grid, Popup } from "semantic-ui-react";
import { Form } from "formsy-semantic-ui-react";
import PropTypes from "prop-types";

// import IcDownArrow from '../../../assets/svg/IcDownArrow';
import ModalBanner from "../ModalBanner";
import IcInfoIcon from "../../assets/svg/IcInfo";
import IcDownArrow from "../../assets/svg/IcDownArrow";

import CompanyDetails from "./CompanyDetails";
import { geocodeByAddress, getLatLng } from "react-places-autocomplete";
import { ToastContainer, toast } from "react-toastify";

import pushCompanytoServer from "../../api/company/createCompany";
import { uploadImage } from "../../utils/aws";
import { USER } from "../../constants/api";
import uriToBlob from "../../utils/env/uriToBlob";

import "react-toastify/dist/ReactToastify.css";

import "./index.scss";

const infoicon = <IcInfoIcon pathcolor="#c8c8c8" />;

class CreateCompanyPage extends React.Component {
  state = {
    about: "",
    admin: [
      {
        email: window.localStorage.getItem(USER.EMAIL)
      }
    ],
    clientName: "",
    companyType: "",
    headquarter: {
      latitude: 0,
      location: "",
      longitude: 0
    },
    imgExt: "",
    industry: "",
    name: "",
    size: "",
    website: "",
    year: 0,
    isLoading: false,
    canSubmit: false,
    imgFile: false,
    imgUrl: false,
    imagePickerOpen: false
  };

  constructor(props) {
    super(props);
    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.sendImageToAws = this.sendImageToAws.bind(this);
  }

  onInputChange = (ev, { value, name }) => {
    this.setState({
      [name]: value
    });
  };

  onAutoCompleteChange = ({ name, value }) => {
    this.setState({
      [name]: value
    });
  };

  onLocSelect = address => {
    this.setState({
      headquarter: {
        ...this.state.headquarter,
        location: address
      }
    });
    geocodeByAddress(address)
      .then(results => getLatLng(results[0]))
      .then(latLng => {
        this.setState({
          headquarter: {
            ...this.state.headquarter,
            latitude: latLng.lat,
            longitude: latLng.lng
          }
        });
      })
      .catch(error => console.error("Error", error));
  };

  onLocChange = address => {
    this.setState({
      headquarter: {
        ...this.state.headquarter,
        location: address
      }
    });
  };

  onAboutChange = html => {
    this.setState({
      about: html
    });
  };

  onFileChange = e => {
    const file = e.target.files[0];

    // reset
    e.target.value = "";

    const extn = file.name
      .split(".")
      .pop()
      .toLowerCase();
    const fr = new FileReader();

    fr.onload = e => {
      this.setState({
        imgExt: "jpg",
        imgFile: file,
        imgUrl: e.target.result,
        imagePickerOpen: true
      });
    };

    fr.readAsDataURL(file);
  };

  onImagePickerCancelClick = ev => {
    this.setState({
      imagePickerOpen: false,
      imgExt: "",
      imgFile: "",
      imgUrl: ""
    });
  };

  onImagePickerSaveClick = image => {
    this.setState({
      imgUrl: image,
      imagePickerOpen: false
    });
  };

  onFileRemove = e => {
    this.setState({
      imgExt: "",
      imgFile: "",
      imgUrl: ""
    });
  };

  async sendImageToAws(compId) {
    const file = uriToBlob(this.state.imgUrl);
    const userId = window.localStorage.getItem(USER.UID);
    try {
      const acceptance = await uploadImage(`company/${compId}/image.jpg`, file);
      // console.log(acceptance);
    } catch (error) {
      console.log(error);
    }
  }

  async onFormSubmit(ev) {
    if (this.props.onEnableFormLoader) {
      this.props.onEnableFormLoader("Creating Company ...");
    }

    try {
      const res = await pushCompanytoServer(this.state);
      const data = await res.json();
      if (this.state.imgFile) {
        const imageUpload = await this.sendImageToAws(data._id);
      }
      // console.log(data);
      this.props.onCompanyCreate(data);

      if (this.props.onModalClose) {
        this.props.onModalClose();
      }

      if (this.props.onDisableFormLoader) {
        this.props.onDisableFormLoader();
      }
    } catch (error) {
      console.error(error);
      this.setState({ isLoading: false });
      if (this.props.onDisableFormLoader) {
        this.props.onDisableFormLoader();
      }
    }
  }

  onFormValid = ev => {
    this.setState({
      canSubmit: true
    });
  };

  onFormInValid = ev => {
    this.setState({
      canSubmit: false
    });
  };

  render() {
    return (
      <div className="CreateCompanyPage">
        <Form
          noValidate
          onValidSubmit={this.onFormSubmit}
          onValid={this.onFormValid}
          onInvalid={this.onFormInValid}>
          <ModalBanner
            headerText="Company Page"
            headerIcon={
              <Popup
                trigger={
                  <div className="InfoIconLabel">
                    <IcInfoIcon pathcolor="#ceeefe" />
                  </div>
                }
                content="Add users to your feed"
              />
            }>
            <Button type="button" compact className="close_btn" />
          </ModalBanner>

          <CompanyDetails
            data={this.state}
            onLocChange={this.onLocChange}
            onLocSelect={this.onLocSelect}
            onInputChange={this.onInputChange}
            onAutoCompleteChange={this.onAutoCompleteChange}
            onAboutChange={this.onAboutChange}
            onFileChange={this.onFileChange}
            onFileRemove={this.onFileRemove}
            onCancelClick={this.props.onModalClose}
            onImagePickerCancelClick={this.onImagePickerCancelClick}
            onImagePickerSaveClick={this.onImagePickerSaveClick}
            isCreate
          />
        </Form>
      </div>
    );
  }
}

export default CreateCompanyPage;
