import React from "react";

import { Button, Form, Grid, Image } from "semantic-ui-react";
import PropTypes from "prop-types";

// import IcDownArrow from '../../../assets/svg/IcDownArrow';

import IcInfoIcon from "../../../assets/svg/IcInfo";
import IcUploadIcon from "../../../assets/svg/IcUpload";
import IcDotIcon from "../../../assets/svg/IcDotIcon";
import InputFieldLabel from "../../Forms/FormFields/InputFieldLabel";
import InfoSection from "../../Sections/InfoSection";
import InputContainer from "../../Forms/InputContainer";
import InputField from "../../Forms/FormFields/InputField";
import SelectOptionField from "../../Forms/FormFields/SelectOptionField";
import QuillText from "../../CardElements/QuillText";
import FileuploadBtn from "../../Buttons/FileuploadBtn";
import FlatDefaultBtn from "../../Buttons/FlatDefaultBtn";
import getPlaceholder from "../../Forms/FormFields/Placeholder";
import SearchFilterInput from "../../Forms/FormFields/SearchFilterInput";
import ImgPickerField from "../../Forms/FormFields/ImgPickerField";

import InfoIconLabel from "../../utils/InfoIconLabel";
import DownArrowCollaps from "../../utils/DownArrowCollaps";

import ProfileImagePicker from "../../Pickers/ProfileImagePicker";

import { USER } from "../../../constants/api";
import ReactGA from "react-ga";
import isLoggedIn from "../../../utils/env/isLoggedin";
import { CreateCompany } from "../../EventStrings/CreateCompany";

import AutoCompleteSearch from "../../Forms/FormFields/AutoCompleteSearch";

import "./index.scss";

import LocationInputField from "../../Forms/FormFields/LocationInputField";
import ModalFooterBtn from "../../Buttons/ModalFooterBtn";
import { CREATE_COMPANY } from "../../../strings";

const autoSearchOptionsFeild = [
  { key: "afq2", value: "", flag: "af", text: "Select" },
  { key: "af", value: "af", flag: "af", text: "Afghanistan" },
  { key: "india", value: "india", flag: "india", text: "india" },
  { key: "us", value: "us", flag: "us", text: "US" }
  // { key: 'af', value: 'af', flag: 'af', text: 'Afghanistan' },
  // { key: 'af', value: 'af', flag: 'af', text: 'Afghanistan' },
  // { key: 'af', value: 'af', flag: 'af', text: 'Afghanistan' },
];

const labelStar = str => {
  return (
    <div>
      <span>{str}</span>
      <span style={{ color: "#0b9ed0" }}>*</span>
    </div>
  );
};

const infoicon = <IcInfoIcon pathcolor="#c8c8c8" />;

class CompanyDetails extends React.Component {
  
  render() {
    const UserId = window.localStorage.getItem(USER.UID);
    const {
      data,
      onLocChange,
      onLocSelect,
      onInputChange,
      onAutoCompleteChange,
      onAboutChange,
      onFileChange,
      onFileRemove,
      onCancelClick,
      onEnableFormLoader,
      onDisableFormLoader,
      onImagePickerCancelClick,
      onImagePickerSaveClick,
      isCreate
    } = this.props;

    return (
      <div className="panel-cardBody">
        <InfoSection
          headerSize="large"
          color="blue"
          headerText="Company details"
          subHeaderText={CREATE_COMPANY.MODAL.HEADER}
          subHeaderIcon={IcDotIcon}>
          {/* {
              data.map(({label, info_icon, placeholder, type}, i) => {
                return (
                  <InputContainer label={label} infoicon={info_icon}>
                    {this.createInput({type,placeholder})}
                  </InputContainer>
                )
              })
            } */}
          <InputContainer
            label={labelStar(`Name`)}
            infoicon={<InfoIconLabel text={CREATE_COMPANY.MODAL.NAME} />}>
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only="mobile">
                  <InputFieldLabel
                    label={labelStar(`Name`)}
                    infoicon={
                      <InfoIconLabel text={CREATE_COMPANY.MODAL.NAME} />
                    }
                  />
                </Grid.Column>
                <Grid.Column computer={16}>
                  <AutoCompleteSearch
                    dataType={"COMPNAME"}
                    placeholder={getPlaceholder(
                      "For Eg: Google, Microsoft, Wipro",
                      "For Eg: Google, Microsoft, Wipro"
                    )}
                    value={data.name}
                    onInputChange={onInputChange}
                    name="name"
                    required
                  />
                  {/* <InputField
                    placeholder={getPlaceholder(
                      "For Eg: Google, Microsoft, Wipro",
                      "For Eg: Google, Microsoft, Wipro"
                    )}
                    value={data.name}
                    onChange={onInputChange}
                    name="name"
                    required
                  /> */}
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>

          <InputContainer
            label={labelStar(`Industry type`)}
          // infoicon={<InfoIconLabel text={CREATE_COMPANY.MODAL.TYPE} />}
          >
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only="mobile">
                  <InputFieldLabel
                    label={labelStar(`Industry type`)}
                  // infoicon={
                  //   <InfoIconLabel text={CREATE_COMPANY.MODAL.TYPE} />
                  // }
                  />
                </Grid.Column>
                <Grid.Column computer={16}>
                  <AutoCompleteSearch
                    dataType={"INDUSTRY"}
                    placeholder={getPlaceholder(
                      "Industry",
                      "For Eg: IT/Software industry"
                    )}
                    value={data.industry}
                    onInputChange={onInputChange}
                    name="industry"
                    required
                  />
                  {/* <SearchFilterInput
                    placeholder={getPlaceholder(
                      "Industry",
                      "For Eg: IT/Software industry"
                    )}
                    results={[
                      {
                        value: "IT/Software industry",
                        text: "IT/Software industry"
                      },
                      {
                        value: "Manufacturing",
                        text: "Manufacturing"
                      }
                    ]}
                    value={data.industry}
                    onChange={onAutoCompleteChange}
                    name="industry"
                    required
                  /> */}
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>

          <InputContainer
            label="Website URL"
            infoicon={<InfoIconLabel text={CREATE_COMPANY.MODAL.WEBSITE} />}>
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only="mobile">
                  <InputFieldLabel
                    label="Website URL"
                    infoicon={
                      <InfoIconLabel text={CREATE_COMPANY.MODAL.WEBSITE} />
                    }
                  />
                </Grid.Column>
                <Grid.Column computer={16}>
                  <InputField
                    value={data.website}
                    onChange={onInputChange}
                    name="website"
                    placeholder="For Eg: www.companyname.com"
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>

          <InputContainer
            label="Location"
            infoicon={<InfoIconLabel text={CREATE_COMPANY.MODAL.LOCATION} />}>
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only="mobile">
                  <InputFieldLabel
                    label="Location"
                    infoicon={
                      <InfoIconLabel text={CREATE_COMPANY.MODAL.LOCATION} />
                    }
                  />
                </Grid.Column>
                <Grid.Column computer={16}>
                  <LocationInputField
                    placeholder={getPlaceholder(
                      "Company location",
                      "For Eg: Delhi, New York, Sydney."
                    )}
                    value={data.headquarter.location}
                    onSelect={onLocSelect}
                    onChange={onLocChange}
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>

          <InputContainer
            label="About company"
          // infoicon={<InfoIconLabel text={CREATE_COMPANY.MODAL.ABOUT} />}
          >
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only="mobile">
                  <InputFieldLabel
                    label="About company"
                  // infoicon={
                  //   <InfoIconLabel text={CREATE_COMPANY.MODAL.ABOUT} />
                  // }
                  />
                </Grid.Column>
                <Grid.Column computer={16}>
                  <QuillText
                    placeholder={getPlaceholder(
                      "Share a description of the company in 250 plus words.",
                      "Share a description of the company in 250 plus words."
                    )}
                    defaultValue={data.about}
                    value={data.about}
                    onChange={onAboutChange}
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>

          {/* <InputContainer>
              <CheckboxField checkboxLabel="I verify that i have the right to act on behalf of the company in the creation of this page." />
            </InputContainer> */}

          <InputContainer
            label="Logo"
            infoicon={<InfoIconLabel text=" Please share only verified logos" />}
            className="fileuploadBtn_label">
            <ImgPickerField
              imgUrl={data.imgUrl}
              onRemoveClick={onFileRemove}
              onImgChange={onFileChange}
            />
          </InputContainer>

          <InputContainer
            label="Company size"
            infoicon={<InfoIconLabel text={CREATE_COMPANY.MODAL.SIZE} />}>
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only="mobile">
                  <InputFieldLabel
                    label="Company size"
                    infoicon={
                      <InfoIconLabel text={CREATE_COMPANY.MODAL.SIZE} />
                    }
                  />
                </Grid.Column>
                <Grid.Column computer={16}>
                  {/* <InputField
                    inputtype="number"
                    value={data.size}
                    onChange={onInputChange}
                    name="size"
                    placeholder="For Eg: 500"
                  /> */}

                  <SelectOptionField
                    placeholder="For Eg: 201 - 500"
                    optionsItem={[
                      {
                        value: "0 - 1",
                        text: "0 - 1 employees"
                      },
                      {
                        value: "2 - 10",
                        text: "2 - 10 employees"
                      },
                      {
                        value: "11 - 50",
                        text: "11 - 50 employees"
                      },
                      {
                        value: "51 - 200",
                        text: "51 - 200 employees"
                      },
                      {
                        value: "201 - 500",
                        text: "201 - 500 employees"
                      },
                      {
                        value: "501 - 1,000",
                        text: "501 - 1,000 employees"
                      },
                      {
                        value: "1,001 - 5,000 ",
                        text: "1,001 - 5,000 employees "
                      },
                      {
                        value: "5001 - 10,000",
                        text: "5001 - 10,000 employees "
                      },

                      {
                        value: "10,000+",
                        text: "10,000+ employees"
                      }
                    ]}
                    name="size"
                    value={data.size}
                    onChange={onInputChange}
                    name="size"
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>

          <InputContainer
            label="Company type"
          // infoicon={<InfoIconLabel text={CREATE_COMPANY.MODAL.TYPE} />}
          >
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only="mobile">
                  <InputFieldLabel
                    label="Company type"
                  // infoicon={
                  //   <InfoIconLabel text={CREATE_COMPANY.MODAL.TYPE} />
                  // }
                  />
                </Grid.Column>
                <Grid.Column computer={16}>
                  <SelectOptionField
                    placeholder={getPlaceholder(
                      "Company Type",
                      "Please select your company type"
                    )}
                    optionsItem={[
                      {
                        value: "Public company",
                        text: "Public company"
                      },
                      {
                        value: "Sole Proprietor",
                        text: "Sole Proprietor"
                      },
                      {
                        value: "Partnership",
                        text: "Partnership"
                      },
                      {
                        value: "Educational institude",
                        text: "Educational institude"
                      },
                      {
                        value: "Privately Held",
                        text: "Privately Held"
                      },
                      {
                        value: "Government Agency",
                        text: "Government Agency"
                      },
                      {
                        value: "Nonprofit",
                        text: "Nonprofit"
                      }
                    ]}
                    onChange={onInputChange}
                    value={data.companyType}
                    name="companyType"
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>

          <InputContainer
            label="Founding year"
            infoicon={<InfoIconLabel text={CREATE_COMPANY.MODAL.YEAR} />}>
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only="mobile">
                  <InputFieldLabel
                    label="Founding year"
                    infoicon={
                      <InfoIconLabel text={CREATE_COMPANY.MODAL.YEAR} />
                    }
                  />
                </Grid.Column>
                <Grid.Column computer={16}>
                  <InputField
                    inputtype="number"
                    placeholder={getPlaceholder("For Eg: 2000", "For Eg: 2018")}
                    onChange={onInputChange}
                    value={data.year}
                    name="year"
                    validations={{
                      customYearValidation: (values, value) => {
                        if (value === "" || value === 0) {
                          return true;
                        }

                        return Number(value) > 1799 && Number(value) < 2019;
                      }
                    }}
                    validationErrors={{
                      customYearValidation:
                        "You need to select at least FOUR products"
                    }}
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>

          <InputContainer label="Internal notes">
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only="mobile">
                  <InputFieldLabel
                    label="Internal notes"
                  // infoicon={
                  //   <InfoIconLabel text={CREATE_COMPANY.MODAL.NOTES} />
                  // }
                  />
                </Grid.Column>
                <Grid.Column computer={16}>
                  <InputField
                    placeholder={getPlaceholder(
                      "Add identification notes, if any.",
                      "Add identification notes, if any."
                    )}
                    onChange={onInputChange}
                    value={data.clientName}
                    name="clientName"
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>
        </InfoSection>

        <ModalFooterBtn
          actioaBtnText={isCreate ? "Create" : "Save"}
          disabled={!data.canSubmit}
          onCancelClick={onCancelClick}
          submitBtnType={"submit"}
          onClick={
            ReactGA.event({
              category: CreateCompany.CreateCompanyEvent.category,
              action: CreateCompany.CreateCompanyEvent.action,
              value: isLoggedIn() ? "" : UserId,
            })
          }
        />

        <ProfileImagePicker
          open={data.imagePickerOpen}
          src={data.imgUrl}
          onSave={onImagePickerSaveClick}
          onCancel={onImagePickerCancelClick}
        />
      </div>
    );
  }
}

export default CompanyDetails;
