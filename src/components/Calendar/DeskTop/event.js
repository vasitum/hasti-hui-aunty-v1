export default [
  {
    id: 0,
    title: "Rajesh Jain",
    inP: true,
    tP: false,
    vC: false,
    start: new Date(2018, 12, 0, 14, 0, 0, 0),
    end: new Date(2018, 12, 0, 15, 0, 0, 0)
  },
  {
    id: 16,
    title: "Rajesh Jain",
    inP: false,
    tP: false,
    vC: true,
    start: new Date(2018, 12, 0, 16, 0, 0, 0),
    end: new Date(2018, 12, 0, 17, 0, 0, 0)
  },
  {
    id: 15,
    title: "Vijay Jain",
    inP: false,
    tP: true,
    vC: false,
    start: new Date(2018, 12, 0, 15, 0, 0, 0),
    end: new Date(2018, 12, 0, 16, 0, 0, 0)
  },
  {
    id: 16,
    title: "Vijay Jain",
    inP: false,
    tP: true,
    vC: false,
    start: new Date(2018, 12, 1, 15, 0, 0, 0),
    end: new Date(2018, 12, 1, 16, 0, 0, 0)
  }
];
