import React, { Component } from "react";
import { Grid, List, Icon, Image, Dropdown, Menu } from "semantic-ui-react";
import Calendar from "react-calendar";
import events from "../DeskTop/event";
import moment from "moment";
import { format, addDays } from "date-fns";
import getHighlightClassName from "../utils/getHighlightClass";
import Arrow from "../../../assets/svg/IcFooterArrowIcon";

moment.updateLocale("en", { weekdaysMin: "U_M_T_W_R_F_S".split("_") });
const options = [
  { key: "month", text: "Month" },
  { key: "week", text: "Week" },
  { key: "day", text: "Day" }
];

function Event({ event }) {
  return (
    <span>
      {event.inP ? (
        <span className="in-persion" />
      ) : event.tP ? (
        <span className="telephone" />
      ) : event.vC ? (
        <span className="video" />
      ) : (
        <span />
      )}
      {event.title}
    </span>
  );
}
class SideCalendar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mdata: {}
    };
  }

  handleEvent = value => {
    const finalData = events.map(item => {
      // console.log(value);
      console.warn(item.start);
      if (item.start === value) {
        console.log(events);
      }
    });
  };

  render() {
    const { activeDate, dates, onMove } = this.props;
    // console.log("date", dates, "active date", activeDate);
    return (
      <div className="calender-event-sidecalendar calendar">
        <Grid columns={1}>
          <Grid.Column>
            <Calendar
              onActiveDateChange={({ activeStartDate }) =>
                onMove(activeStartDate)
              }
              activeStartDate={new Date(activeDate)}
              onClickDay={value => this.handleEvent(value)}
              prevLabel={
                <Icon name="angle left" className="calendar-icon-nav" />
              }
              nextLabel={
                <Icon name="angle right" className="calendar-icon-nav" />
              }
              tileClassName={({ date, view }) => {
                return getHighlightClassName({ date, view }, dates);
              }}
            />
            <div
              className="indicator-block"
              style={{
                margin: "8px 0",
                padding: "10px 0",
                borderTop: "1px solid #efefef"
              }}>
              <div className="calender-indicator-name">
                <span className="telephone" />
                Telephonic
              </div>
              <div className="calender-indicator-name">
                <span className="video" />
                Video call
              </div>
              <div className="calender-indicator-name">
                <span className="in-persion" />
                In person
              </div>
            </div>
            <div style={{ marginTop: 50 }}>
              <div className="text-center">
                <span className="calendar-event-count-sidecaledar">
                  {dates.length}
                </span>
              </div>
              <div className="text-center">
                <span className="calendar-event-tag-sidecaledar">Events</span>
              </div>
              {/* <div className="text-center pt">
                <span className="calendar-event-tag-in-sidecaledar">in</span>
              </div> */}
              <div className="text-center pt">
                <span className="calendar-event-tag-month-sidecaledar">
                  {format(activeDate, "MMMM YYYY")}
                </span>
              </div>
            </div>
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

export default SideCalendar;
