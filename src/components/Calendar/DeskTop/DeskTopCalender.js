import React, { Component } from "react";
import BigCalendar from "react-big-calendar";
import moment from "moment";
import events from "./event";
import "react-big-calendar/lib/css/react-big-calendar.css";
import SideCalender from "./SideCalendar";
import IcUser from "../../../assets/svg/IcUser";
import {
  Grid,
  Button,
  Card,
  Feed,
  TransitionablePortal,
  List,
  Icon,
  Tab
} from "semantic-ui-react";
import { Link } from "react-router-dom";
import { startOfMonth, endOfMonth } from "date-fns";
import getInterviews from "../../../api/jobs/getInterviews";
import { USER } from "../../../constants/api";
import formatInterviews from "../utils/formatInterviews";
import IcEdit from "../../../assets/svg/IcEdit";

// const localizer = BigCalendar.momentLocalizer(moment);

function EventMonth({ event }) {
  return (
    <span>
      {event.inP ? (
        <span className="in-persion" />
      ) : event.tP ? (
        <span className="telephone" />
      ) : event.vC ? (
        <span className="video" />
      ) : (
        <span />
      )}
      {moment(event.start).format("LT") + " - "}
      {event.title}
    </span>
  );
}

function Event({ event }) {
  return (
    <span>
      {event.inP ? (
        <span className="in-persion" />
      ) : event.tP ? (
        <span className="telephone" />
      ) : event.vC ? (
        <span className="video" />
      ) : (
        <span />
      )}
      {event.title}
    </span>
  );
}

class DeskTopCalender extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      data: {},
      activeIndex: 1,
      dates: [],
      currentDate: new Date().getTime()
    };

    // bind it!
    this.getInterviewByDate = this.getInterviewByDate.bind(this);
  }
  handleClick = () => this.setState({ open: !this.state.open });
  handleClose = () => this.setState({ open: false });
  handleEvent = event => {
    this.setState({ data: event });
    this.handleClick();
  };

  onBigCalenderNavigate = date => {
    this.setState(
      {
        currentDate: date,
        open: false
      },
      () => {
        this.componentDidMount();
      }
    );
  };

  async getInterviewByDate(start, end) {
    try {
      const res = await getInterviews(null, start, end);

      switch (res.status) {
        case 404:
          return null;

        case 200:
          const data = await res.json();
          return data;

        default:
          return new Error("Unable to get Job");
      }
    } catch (error) {
      return new Error(error);
    }
  }

  async componentDidMount() {
    const { currentDate } = this.state;
    try {
      const res = await this.getInterviewByDate(
        startOfMonth(currentDate).getTime(),
        endOfMonth(currentDate).getTime()
      );

      if (!res) {
        this.setState({
          dates: []
        });

        return;
      }

      this.setState({
        dates: formatInterviews(res)
      });
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const { open, data, dates, currentDate } = this.state;
    const urlEdit = !data.iscand
      ? `/job/${data.jobId}/application/${data.jbAppId}?screen=interview`
      : `/job/candidate/${data.jobId}/application/${
          data.jbAppId
        }?screen=interview`;
    return (
      <div className="calender-event-desktop calendar">
        <Grid columns="equal">
          <Grid.Column>
            <SideCalender
              dates={dates}
              activeDate={currentDate}
              onMove={this.onBigCalenderNavigate}
            />
          </Grid.Column>
          <Grid.Column width={12}>
            <Grid columns={1}>
              <Grid.Column>
                <BigCalendar
                  popup
                  selectable
                  events={dates}
                  date={new Date(currentDate)}
                  defaultDate={new Date(currentDate)}
                  localizer={BigCalendar.momentLocalizer(moment)}
                  style={{ height: "100vh" }}
                  onSelectEvent={event => this.handleEvent(event)}
                  onNavigate={this.onBigCalenderNavigate}
                  components={{
                    month: {
                      event: EventMonth
                    },
                    week: {
                      event: Event
                    },
                    day: {
                      event: Event
                    },
                    agenda: {
                      event: Event
                    }
                  }}
                />
                <TransitionablePortal onClose={this.handleClose} open={open}>
                  <Card className="calendar-popup-card">
                    <Card.Content
                      className="calendar-popup-cross"
                      style={{
                        backgroundColor: data.isCand ? "#3a6473" : "#0b9ed0"
                      }}>
                      <div style={{ float: "right" }}>
                        {/* <Icon name="trash" className="icon-pop" />{" "}
                        <Icon name="ellipsis horizontal" className="icon-pop" />{" "} */}
                        <Icon
                          name="cancel"
                          className="icon-pop"
                          onClick={this.handleClick}
                        />
                      </div>
                    </Card.Content>
                    <Card.Content
                      className="calendar-popup-header"
                      style={{
                        backgroundColor: data.isCand ? "#3a6473" : "#0b9ed0"
                      }}>
                      <Feed>
                        <Feed.Event>
                          {data.img ? (
                            <Feed.Label
                              image={`https://s3-us-west-2.amazonaws.com/img-mwf/${
                                data.isCand ? data.idData : data.idRe
                              }/image.jpg`}
                            />
                          ) : (
                            <span className="label">
                              <IcUser
                                rectcolor="#f7f7fb"
                                height="50"
                                width="50"
                                pathcolor="#c8c8c8"
                              />
                            </span>
                          )}

                          <Feed.Content>
                            <span className="headTitle">Interviewer</span>
                            <Feed.Date
                              className="calender-popup-title"
                              // content={data ? data.recuiterName : "NA"}
                              content={data.isCand ? data.title : "You"}
                            />
                            {/* <Feed.Summary className="calender-popup-subtitle">
                              {data ? data.subTitle : "NA"}
                            </Feed.Summary> */}
                          </Feed.Content>
                        </Feed.Event>

                        <Feed.Event>
                          {/* {data.isCand ? (
                            data.img ? (
                              <Feed.Label
                                image={`https://s3-us-west-2.amazonaws.com/img-mwf/${
                                  data.idRe
                                }/image.jpg`}
                              />
                            ) : (
                              <span className="label">
                                <IcUser
                                  rectcolor="#f7f7fb"
                                  height="50"
                                  width="50"
                                  pathcolor="#c8c8c8"
                                />
                              </span>
                            )
                          ) : data.img ? (
                            <Feed.Label
                              image={`https://s3-us-west-2.amazonaws.com/img-mwf/${
                                data.idCa
                              }/image.jpg`}
                            />
                          ) : (
                            <span className="label">
                              <IcUser
                                rectcolor="#f7f7fb"
                                height="50"
                                width="50"
                                pathcolor="#c8c8c8"
                              />
                            </span>
                          )} */}

                          {data.img ? (
                            <Feed.Label
                              image={`https://s3-us-west-2.amazonaws.com/img-mwf/${
                                !data.isCand ? data.idData : data.idCa
                              }/image.jpg`}
                            />
                          ) : (
                            <span className="label">
                              <IcUser
                                rectcolor="#f7f7fb"
                                height="50"
                                width="50"
                                pathcolor="#c8c8c8"
                              />
                            </span>
                          )}

                          <Feed.Content>
                            <span className="headTitle">Interviewee</span>
                            <Feed.Date
                              className="calender-popup-title"
                              // content={data ? data.candidateName : "NA"}
                              content={!data.isCand ? data.title : "You"}
                            />
                            <div className="calender-popup-PhoneNumber">
                              {/* {data ? data.candidateNumber : ""} */}
                              {!data.isCand ? data.candidateNumber : ""}
                            </div>

                            {/* <Feed.Summary className="calender-popup-subtitle">
                              {data ? data.subTitle : "NA"}
                            </Feed.Summary> */}
                          </Feed.Content>
                        </Feed.Event>
                      </Feed>
                    </Card.Content>
                    <Card.Content className="calendar-popup-body">
                      <Button
                        circular
                        className="floating-button"
                        as={Link}
                        to={urlEdit}>
                        <IcEdit pathcolor="#acaeb5" height="14" width="14" />
                      </Button>
                      <List>
                        <List.Item className="calendar-popup-list">
                          <List.Icon
                            name="briefcase"
                            className="calender-popup-icon"
                          />
                          <List.Content className="calender-popup-font">
                            {data ? data.interviewJobName : "NA"}
                          </List.Content>
                        </List.Item>

                        <List.Item className="calendar-popup-list">
                          <List.Icon
                            name="calendar"
                            className="calender-popup-icon"
                          />
                          <List.Content className="calender-popup-font">
                            {data ? moment(data.start).format("LL") : "NA"}
                          </List.Content>
                        </List.Item>
                        <List.Item className="calendar-popup-list">
                          <List.Icon
                            name="clock"
                            className="calender-popup-icon"
                          />
                          <List.Content className="calender-popup-font">
                            {data
                              ? moment(data.start).format("LT") +
                                " - " +
                                moment(data.end).format("LT")
                              : "NA"}
                          </List.Content>
                        </List.Item>
                        <List.Item className="calendar-popup-list">
                          <List.Content className="calender-popup-font">
                            {/* <span className="in-persion" /> Skype Interview
                            (Round 2) */}
                            {data.inP ? (
                              <span className="in-persion" />
                            ) : data.tP ? (
                              <span className="telephone" />
                            ) : data.vC ? (
                              <span className="video" />
                            ) : (
                              <span />
                            )}{" "}
                            {data.interviewType === "telephone"
                              ? "Telephonic"
                              : data.interviewType}
                          </List.Content>
                        </List.Item>
                      </List>
                    </Card.Content>
                  </Card>
                </TransitionablePortal>
              </Grid.Column>
            </Grid>
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

export default DeskTopCalender;
