import React, { Component } from "react";
import "./index.scss";
import Mobile from "./Mobile";
import DeskTop from "./DeskTop/DeskTopCalender";
import { Container } from "semantic-ui-react";
import isMobile from "../../utils/env/isMobile";
import withInterviews from "../wrappers/withInterview";
import ReactGA from "react-ga";

class Calendar extends Component {

  componentDidMount() {
    ReactGA.pageview('/calendar');
  }

  render() {
    const { data } = this.props;

    return (
      <div>
        {isMobile() ? (
          <Mobile data={data} />
        ) : (
            <Container>
              <div className="calender-body-container">
                <DeskTop data={data} />
              </div>
            </Container>
          )}
      </div>
    );
  }
}

export default withInterviews(Calendar);
