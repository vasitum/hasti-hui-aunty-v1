import { isDate, startOfDay } from "date-fns";

const VIEW = "month";
const EMPTY_STR = "";

export default ({ date, view }, dates) => {
  if (view !== VIEW) {
    return EMPTY_STR;
  }

  if (!dates || !Array.isArray(dates)) {
    return EMPTY_STR;
  }

  if (!isDate(date)) {
    return EMPTY_STR;
  }

  const dtcalender = startOfDay(date).getTime();

  for (let idx = 0; idx < dates.length; idx++) {
    const date = dates[idx];
    if (startOfDay(date.start).getTime() === dtcalender) {
      return "vas_rc__highlight";
    }
  }

  return EMPTY_STR;
};
