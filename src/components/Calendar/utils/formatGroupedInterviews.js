import { startOfYear, startOfMonth, startOfDay } from "date-fns";

export default interviews => {
  if (!interviews || !Array.isArray(interviews)) {
    return [];
  }

  const ret = {};

  interviews.map(interview => {
    const soy = startOfYear(interview.start).getTime(); /* start of year */
    const som = startOfMonth(interview.start).getTime(); /* start of month */
    const sod = startOfDay(interview.start).getTime(); /* start of day */

    // check if year exists
    if (ret[soy]) {
      // check if month exists
      if (ret[soy][som]) {
        if (ret[soy][som][sod]) {
          ret[soy][som][sod].push(interview);
        } else {
          ret[soy][som][sod] = [];
          ret[soy][som][sod].push(interview);
        }
      } else {
        ret[soy][som] = {};
        ret[soy][som][sod] = [];
        ret[soy][som][sod].push(interview);
      }
    } else {
      ret[soy] = {};
      ret[soy][som] = {};
      ret[soy][som][sod] = [];
      ret[soy][som][sod].push(interview);
    }
  });

  return {
    total: interviews.length,
    data: ret
  };
};
