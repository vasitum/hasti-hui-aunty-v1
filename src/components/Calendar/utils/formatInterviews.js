import { USER } from "../../../constants/api";

export default data => {
  if (!data || !Array.isArray(data)) {
    return [];
  }

  const userId = window.localStorage.getItem(USER.UID);
  const isCandidate = id => id === userId;

  // map and switch
  return data.map(interview => {
    const isCand = isCandidate(interview.candidateId);
    console.log(isCand);
    console.log("Caledar Data1", interview);
    return {
      id: interview._id,
      candidateName: interview.candidateName,
      candidateNumber: interview.candidateNumber,
      isCand: isCand,
      recuiterName: interview.interviewerName,
      recuiterNumber: interview.interviewContactNumber,
      recruiterId: interview.recruiterId,
      interviewJobName: interview.jobName,
      interviewImgExt: interview.reqImagExt,

      title: isCand ? interview.interviewerName : interview.candidateName,
      idRe: interview.recruiterId,
      idCa: interview.candidateId,
      idData: isCand ? interview.recruiterId : interview.candidateId,
      imgCa: interview.candidateImageExt,
      imgRe: interview.reqImagExt,
      img: isCand ? interview.reqImagExt : interview.candidateImageExt,
      number: isCand ? interview.interviewerName : interview.candidateName,

      inP: interview.interviewMode === "inPerson",
      iscand: userId === interview.candidateId,
      tP: interview.interviewMode === "telephone",
      vC: interview.interviewMode === "videoCall",
      start: new Date(interview.scheduledDate),
      end: new Date(interview.closedDate),
      jbAppId: interview.jobApplicationId,
      // img: interview.candidateImageExt,
      cdId: interview.candidateId,
      jobId: interview.jobId,
      subTitle: interview.candidateProfileTitle,
      interviewType: interview.interviewMode,
      allDay: false
    };
  });
};
