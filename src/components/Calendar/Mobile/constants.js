export const VIEW_STATES = {
  MOBILE_DEFAULT: "MOBILE_DEFAULT",
  MOBILE_DAY: "MOBILE_DAY",
  MOBILE_WEEK: "MOBILE_WEEK",
  MOBILE_DETAIL: "MOBILE_DETAIL",
  MOBILE_ALLVIEW: "MOBILE_ALLVIEW"
};
