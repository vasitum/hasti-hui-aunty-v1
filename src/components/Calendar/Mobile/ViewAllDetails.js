import React, { Component } from "react";
import { Grid, List, Icon, Button, Container } from "semantic-ui-react";
import Calendar from "react-calendar";
import { addDays } from "date-fns";
import IcFooterArrowIcon from "../../../assets/svg/IcFooterArrowIcon";
import { VIEW_STATES } from "./constants";
import { startOfYear, startOfMonth, format } from "date-fns";
import MobileHeader from "../../MobileComponents/MobileHeader";

class MonthViewDetail extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      groupedDates,
      currentDate,
      onCurrentDateChange,
      onViewChange
    } = this.props;

    return (
      <div className="calender-event-month-view calendar">
        <MobileHeader
          headerLeftIcon={
            <Button
              onClick={e => onViewChange(VIEW_STATES.MOBILE_DEFAULT)}
              compact
              className="backArrowBtn">
              <IcFooterArrowIcon pathcolor="#6e768a" />
            </Button>
          }
          className="isMobileHeader_fixe"
          headerTitle="My Calendar">
          {/* <ViewDropdown
            currentView={currentView}
            onViewChange={onViewChange}
          /> */}
        </MobileHeader>
        {/* <Grid columns={1}>
          <Grid.Column className="calendar-header-strip">
            <List divided verticalAlign="middle">
              <List.Item
                onClick={e => onViewChange(VIEW_STATES.MOBILE_DEFAULT)}>
                <Arrow />{" "}
              </List.Item>
            </List>
          </Grid.Column>
        </Grid> */}
        <div className="viewAllMobile_container">
          <Container>
            <Calendar
              view="month"
              activeStartDate={currentDate}
              // onClickMonth={value => this.handleEvent(value)}
              className="react-calender-1"
              prevLabel={
                <Icon name="angle left" className="calendar-icon-nav" />
              }
              nextLabel={
                <Icon name="angle right" className="calendar-icon-nav" />
              }
              onActiveDateChange={({ activeStartDate, view }) => {
                onCurrentDateChange(activeStartDate);
              }}
            />
            <div>
              <h4 className="total-event-count">
                {groupedDates.total}{" "}
                {groupedDates.total === 1 ? "Event" : "Events"}
              </h4>
              {(function(data) {
                console.log(data);
                const soy = startOfYear(currentDate).getTime();
                const som = startOfMonth(currentDate).getTime();

                if (!data[soy]) {
                  console.log("SOY");
                  return "";
                }

                if (!data[soy][som]) {
                  console.log("SOM");
                  return "";
                }

                const sodKeys = Object.keys(data[soy][som]);
                if (!sodKeys.length) {
                  return "";
                }

                const soym = data[soy][som];
                return sodKeys.map(sod => {
                  return (
                    <React.Fragment key={sod}>
                      <div className="event-day-detail-block">
                        <div>
                          <h5 className="event-day-date">
                            {format(Number(sod), "D ddd")}
                          </h5>
                        </div>
                        <div>
                          <h5 className="event-count">
                            {soym[sod].length} Events
                          </h5>
                        </div>
                      </div>
                      <div>
                        {Array.from(soym[sod]).map(interview => {
                          return (
                            <div className="event-details" key={interview.id}>
                              <div>
                                <h5 className="event-time">
                                  {format(interview.start, "hh:mm A")} -{" "}
                                  {format(interview.start, "hh:mm A")}
                                </h5>
                              </div>
                              <div style={{ padding: "15px" }}>
                                <div style={{ paddingBottom: "10px" }}>
                                  <span className="telephone" />
                                  <span className="for-event-name">
                                    {interview.title}
                                  </span>
                                </div>
                                <div style={{ paddingBottom: "10px" }}>
                                  <span className="video" />
                                  <span className="for-event-name">
                                    {interview.title}
                                  </span>
                                </div>
                              </div>
                            </div>
                          );
                        })}
                      </div>
                    </React.Fragment>
                  );
                });
              })(groupedDates.data)}
            </div>
          </Container>
          <div className="indicator-block">
            <div className="calender-indicator-name">
              <span className="telephone" />
              Telephonic
            </div>
            <div className="calender-indicator-name">
              <span className="video" />
              Video call
            </div>
            <div className="calender-indicator-name">
              <span className="in-persion" />
              In person
            </div>
          </div>
        </div>
        {/* <Grid columns={1}>
          <Grid.Column>

          </Grid.Column>
        </Grid> */}
        {/* <Grid columns={1}>
          <Grid.Column>
            
          </Grid.Column>
        </Grid> */}
      </div>
    );
  }
}

export default MonthViewDetail;
