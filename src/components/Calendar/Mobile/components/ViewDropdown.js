import React from "react";
import { Dropdown } from "semantic-ui-react";
import { VIEW_STATES } from "../constants";

function maskValue(constant) {
  switch (constant) {
    case VIEW_STATES.MOBILE_ALLVIEW:
      return "All";
    case VIEW_STATES.MOBILE_DAY:
      return "Day";
    case VIEW_STATES.MOBILE_DEFAULT:
      return "Month";
    case VIEW_STATES.MOBILE_WEEK:
      return "Week";
    case VIEW_STATES.MOBILE_DETAIL:
      return "User Detail";

    default:
      break;
  }
}

export default ({ currentView, onViewChange }) => {
  return (
    <Dropdown
      className="calendar-dropdown"
      options={Object.keys(VIEW_STATES)
        .map(val => {
          return {
            text: maskValue(val),
            value: val,
            key: val
          };
        })
        .filter(val => {
          if (
            val.value === VIEW_STATES.MOBILE_ALLVIEW ||
            val.value === VIEW_STATES.MOBILE_DETAIL
          ) {
            return false;
          }

          return true;
        })}
      value={currentView}
      onChange={(e, { value }) => {
        onViewChange(value);
      }}
      // selection
      compact
      // wrapSelection={false}
    />
  );
};
