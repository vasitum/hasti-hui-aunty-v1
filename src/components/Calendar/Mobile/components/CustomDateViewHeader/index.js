import React from "react";
import { Icon } from "semantic-ui-react";
import {
  addDays,
  subDays,
  startOfWeek,
  endOfWeek,
  addWeeks,
  subWeeks,
  format
} from "date-fns";

import "./index.scss";

/**
 * Returns Custom Navigation for dates as per our calender design
 *
 * @param onChange = Function
 * @param value = Date
 * @param type = OneOf("day" | "week")
 */
export default ({ value, onChange, type = "day" }) => {
  const formatter = type === "day" ? "DD MMM, YYYY" : "";

  return (
    <div className="CustomDateViewHeader">
      <span
        className={"CustomDateViewHeader__icon"}
        onClick={e =>
          onChange(type === "day" ? subDays(value, 1) : subWeeks(value, 1))
        }>
        <Icon name="angle left" />
      </span>
      {type === "day" ? (
        <span>{format(value, formatter)}</span>
      ) : (
        <span>
          {format(startOfWeek(value), "DD MMM")} -{" "}
          {format(endOfWeek(value), "DD MMM")},{" "}
          {format(endOfWeek(value), "YYYY")}
        </span>
      )}
      <span
        className={"CustomDateViewHeader__icon"}
        onClick={e =>
          onChange(type === "day" ? addDays(value, 1) : addWeeks(value, 1))
        }>
        <Icon name="angle right" />
      </span>
    </div>
  );
};
