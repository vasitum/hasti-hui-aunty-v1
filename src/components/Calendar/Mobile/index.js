import React from "react";
import DefaultView from "./Calendar";
import DayView from "./DaysView";
import WeekView from "./WeekView";
import DetailView from "./UserEventsDetails";
import AllView from "./ViewAllDetails";
import { VIEW_STATES } from "./constants";
import { startOfMonth, endOfMonth } from "date-fns";
import getInterviews from "../../../api/jobs/getInterviews";
import formatInterviews from "../utils/formatInterviews";
import formatGroupedInterview from "../utils/formatGroupedInterviews";

import "./index.scss";

export default class extends React.Component {
  state = {
    view: VIEW_STATES.MOBILE_DEFAULT,
    dates: [],
    currentDate: new Date()
  };

  onViewChange = value => {
    this.setState({
      view: value
    });
  };

  async getInterviewByDate(start, end) {
    try {
      const res = await getInterviews(null, start, end);

      switch (res.status) {
        case 404:
          return null;

        case 200:
          const data = await res.json();
          return data;

        default:
          return new Error("Unable to get Job");
      }
    } catch (error) {
      return new Error(error);
    }
  }

  async componentDidMount() {
    const { currentDate } = this.state;
    try {
      const res = await this.getInterviewByDate(
        startOfMonth(currentDate).getTime(),
        endOfMonth(currentDate).getTime()
      );

      if (!res) {
        this.setState({
          dates: []
        });

        return;
      }

      this.setState({
        dates: formatInterviews(res)
      });
    } catch (error) {
      console.error(error);
    }
  }

  onCurrentDateChange = date => {
    this.setState(
      {
        currentDate: date
      },
      () => {
        this.componentDidMount();
      }
    );
  };

  render() {
    const { view, currentDate, dates } = this.state;
    const { data } = this.props;
    const groupedData = formatGroupedInterview(dates);

    switch (view) {
      case VIEW_STATES.MOBILE_DEFAULT:
        return (
          <DefaultView
            currentDate={currentDate}
            dates={dates}
            currentView={view}
            onViewChange={this.onViewChange}
            groupedDates={groupedData}
            onCurrentDateChange={this.onCurrentDateChange}
            data={data}
          />
        );
      case VIEW_STATES.MOBILE_DAY:
        return (
          <DayView
            groupedDates={groupedData}
            currentDate={currentDate}
            onCurrentDateChange={this.onCurrentDateChange}
            currentView={view}
            onViewChange={this.onViewChange}
            data={data}
          />
        );
      case VIEW_STATES.MOBILE_WEEK:
        return (
          <WeekView
            groupedDates={groupedData}
            currentDate={currentDate}
            onCurrentDateChange={this.onCurrentDateChange}
            currentView={view}
            onViewChange={this.onViewChange}
            data={data}
          />
        );
      case VIEW_STATES.MOBILE_DETAIL:
        return (
          <DetailView
            currentView={view}
            onViewChange={this.onViewChange}
            data={data}
          />
        );
      case VIEW_STATES.MOBILE_ALLVIEW:
        return (
          <AllView
            currentDate={currentDate}
            groupedDates={groupedData}
            currentView={view}
            onViewChange={this.onViewChange}
            onCurrentDateChange={this.onCurrentDateChange}
            data={data}
          />
        );
      default:
        return (
          <DefaultView
            currentView={view}
            onViewChange={this.onViewChange}
            data={data}
          />
        );
    }
  }
}
