import React, { Component } from "react";
import {
  Grid,
  List,
  Dropdown,
  Icon,
  Button,
  Container
} from "semantic-ui-react";
import Calendar from "react-calendar";
import IcFooterArrowIcon from "../../../assets/svg/IcFooterArrowIcon";
import ViewDropdown from "./components/ViewDropdown";
import CustomDateHeader from "./components/CustomDateViewHeader";
import { startOfYear, startOfMonth, startOfDay, format } from "date-fns";
import MobileHeader from "../../MobileComponents/MobileHeader";
import DashBoardNavigationDrawer from "../../MobileComponents/DashBoardNavigationDrawer";
const options = [
  { key: "month", text: "Month" },
  { key: "week", text: "Week" },
  { key: "day", text: "Day" }
];

function getCurrentInterviews(data, currentDate) {
  const soy = startOfYear(currentDate).getTime();
  const som = startOfMonth(currentDate).getTime();
  const sod = startOfDay(currentDate).getTime();
  console.log(data[soy], data[som], data[sod]);
  if (!data[soy]) {
    console.log("NOT FOUND SOY");
    return [];
  }

  if (!data[soy][som]) {
    console.log("NOT FOUND SOM");
    return [];
  }

  const ret = data[soy][som][sod];
  return ret ? ret : [];
}

class DaysView extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      currentView,
      onViewChange,
      currentDate,
      onCurrentDateChange,
      groupedDates
    } = this.props;

    const currentDateData = getCurrentInterviews(
      groupedDates.data,
      currentDate
    );

    // console.log("CURRENT DATE DATA", currentDateData);

    return (
      <div className="calender-event-day-view calendar">
        <MobileHeader
          // headerLeftIcon={<Button compact className="backArrowBtn">
          //   <IcFooterArrowIcon pathcolor="#6e768a" />
          // </Button>}
          headerLeftIcon={<DashBoardNavigationDrawer />}
          className="isMobileHeader_fixe"
          headerTitle="My Calendar">
          <ViewDropdown currentView={currentView} onViewChange={onViewChange} />
        </MobileHeader>
        {/* <Grid columns={1}>
          <Grid.Column className="calendar-header-strip">
            <List divided verticalAlign="middle">
              <List.Item>
                <List.Content floated="right">
                  <ViewDropdown
                    currentView={currentView}
                    onViewChange={onViewChange}
                  />
                </List.Content>
                <Arrow />{" "}
                <span className="calendar-header-font">My Calendar</span>
              </List.Item>
            </List>
          </Grid.Column>
        </Grid> */}

        <div className="dayMobile_container">
          <Container>
            <div>
              <CustomDateHeader
                value={currentDate}
                onChange={onCurrentDateChange}
                type={"day"}
              />
              <div>
                <h4 className="total-event-count">
                  {currentDateData.length}{" "}
                  {currentDateData.length === 1 ? "Event" : "Events"}
                </h4>
                {(function(data) {
                  return data.map(interview => {
                    return (
                      <div className="event-details" key={interview.id}>
                        <div>
                          <h5 className="event-time">
                            {format(interview.start, "hh:mm A")} -{" "}
                            {format(interview.start, "hh:mm A")}
                          </h5>
                        </div>
                        <div style={{ padding: "15px" }}>
                          <div style={{ paddingBottom: "10px" }}>
                            <span className="telephone" />
                            <span className="for-event-name">
                              {interview.title}
                            </span>
                          </div>
                          <div style={{ paddingBottom: "10px" }}>
                            <span className="video" />
                            <span className="for-event-name">
                              {interview.title}
                            </span>
                          </div>
                        </div>
                      </div>
                    );
                  });
                })(currentDateData)}
              </div>
            </div>
            <div>
              <div className="indicator-block">
                <div className="calender-indicator-name">
                  <span className="telephone" />
                  Telephonic
                </div>
                <div className="calender-indicator-name">
                  <span className="video" />
                  Video call
                </div>
                <div className="calender-indicator-name">
                  <span className="in-persion" />
                  In person
                </div>
              </div>
            </div>
          </Container>
        </div>
      </div>
    );
  }
}

export default DaysView;
