import React, { Component } from "react";
import { Grid, Card, Feed, List, Dropdown, Icon } from "semantic-ui-react";
import Arrow from "../../../assets/svg/IcFooterArrowIcon";

const options = [
  { key: "edit", text: "Edit" },
  { key: "delete", text: "Delete" },
  { key: "share", text: "Share" }
];

class EventDetails extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="calender-event-details calendar">
        <Grid columns={1}>
          <Grid.Column className="calendar-header-strip">
            <List divided verticalAlign="middle">
              <List.Item>
                <List.Content floated="right">
                  <Dropdown
                    trigger={<List.Icon name="ellipsis vertical" />}
                    options={options}
                    pointing="right"
                    icon={null}
                  />
                </List.Content>
                <Arrow />{" "}
                <span className="calendar-header-font">March, 2018</span>
              </List.Item>
            </List>
          </Grid.Column>
        </Grid>
        <Grid columns={1}>
          <Grid.Column>
            <Card style={{ width: "100%", boxShadow: "none" }}>
              <Card.Content>
                <Feed>
                  <Feed.Event>
                    <Feed.Label image="https://react.semantic-ui.com/images/avatar/small/jenny.jpg" />
                    <Feed.Content>
                      <Feed.Date
                        className="event-user-name"
                        content="Rajesh jain"
                      />
                      <Feed.Summary className="event-user-title">
                        Software Engineer
                      </Feed.Summary>
                    </Feed.Content>
                  </Feed.Event>
                </Feed>
              </Card.Content>
              <Card.Content style={{ padding: "1rem 1rem 2rem" }}>
                <List>
                  <List.Item className="calendar-list-item">
                    <List.Icon name="calendar" className="user-event-icon" />
                    <List.Content className="user-event-props">
                      26 February
                    </List.Content>
                  </List.Item>
                  <List.Item className="calendar-list-item">
                    <List.Icon name="clock" className="user-event-icon" />
                    <List.Content className="user-event-props">
                      9 AM - 10 AM
                    </List.Content>
                  </List.Item>
                  <List.Item className="calendar-list-item">
                    <List.Content>
                      <span className="video" />{" "}
                      <span className="user-event-props">
                        Skype Interview (Round 2)
                      </span>
                    </List.Content>
                  </List.Item>
                  <List.Item className="calendar-list-item">
                    <List.Content>
                      <span className="user-event-props-small">
                        .... name@gmail.com
                      </span>
                    </List.Content>
                  </List.Item>
                  <List.Item className="calendar-list-item">
                    <List.Content>
                      <span className="user-event-props-small">
                        .... +91 - 9450626352
                      </span>
                    </List.Content>
                  </List.Item>
                </List>
              </Card.Content>
            </Card>
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

export default EventDetails;
