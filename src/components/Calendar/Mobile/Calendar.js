import React, { Component } from "react";
import {
  Grid,
  List,
  Icon,
  Image,
  Dropdown,
  Menu,
  Button,
  Container
} from "semantic-ui-react";
import Calendar from "react-calendar";
import events from "../DeskTop/event";
import moment from "moment";
import IcFooterArrowIcon from "../../../assets/svg/IcFooterArrowIcon";
import ViewDropdown from "./components/ViewDropdown";
import getHighlightClassName from "../utils/getHighlightClass";
import { VIEW_STATES } from "./constants";
import { startOfYear, startOfMonth, format } from "date-fns";

import DashBoardNavigationDrawer from "../../MobileComponents/DashBoardNavigationDrawer";

import MobileHeader from "../../MobileComponents/MobileHeader";

// locale handler
moment.updateLocale("en", { weekdaysMin: "U_M_T_W_R_F_S".split("_") });

const options = [
  { key: "month", text: "Month" },
  { key: "week", text: "Week" },
  { key: "day", text: "Day" }
];
function Event({ event }) {
  return (
    <span>
      {event.inP ? (
        <span className="in-persion" />
      ) : event.tP ? (
        <span className="telephone" />
      ) : event.vC ? (
        <span className="video" />
      ) : (
        <span />
      )}
      {event.title}
    </span>
  );
}
class MobileCalendar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mdata: {}
    };
  }

  handleEvent = value => {
    const finalData = events.map(item => {
      // console.log(value);
      console.warn(item.start);
      if (item.start === value) {
        console.log(events);
      }
    });
  };

  render() {
    const {
      currentView,
      onViewChange,
      dates,
      currentDate,
      onCurrentDateChange,
      groupedDates
    } = this.props;

    return (
      <div className="calender-event-full calenderEvent-fullMobile calendar">
        <MobileHeader
          headerLeftIcon={<DashBoardNavigationDrawer />}
          className="isMobileHeader_fixe"
          headerTitle="My Calendar">
          <ViewDropdown currentView={currentView} onViewChange={onViewChange} />
        </MobileHeader>
        {/* <Grid columns={1}>
          <Grid.Column className="calendar-header-strip">
            <List divided verticalAlign="middle">
              <List.Item>
                <List.Content floated="right">
                  <ViewDropdown
                    currentView={currentView}
                    onViewChange={onViewChange}
                  />
                </List.Content>
                <Arrow />{" "}
                <span className="calendar-header-font">My Calendar</span>
              </List.Item>
            </List>
          </Grid.Column>
        </Grid> */}
        <div className="monthMobile_container">
          <Container>
            <Calendar
              tileClassName={({ date, view }) => {
                return getHighlightClassName({ date, view }, dates);
              }}
              onClickDay={value => this.handleEvent(value)}
              prevLabel={
                <Icon name="angle left" className="calendar-icon-nav" />
              }
              nextLabel={
                <Icon name="angle right" className="calendar-icon-nav" />
              }
              onActiveDateChange={({ activeStartDate, view }) => {
                onCurrentDateChange(activeStartDate);
              }}
            />
            <div
              className="indicator-block"
              style={{
                margin: "8px 0",
                padding: "10px 0",
                borderTop: "1px solid #efefef"
              }}>
              <div className="calender-indicator-name">
                <span className="telephone" />
                Telephonic
              </div>
              <div className="calender-indicator-name">
                <span className="video" />
                Video call
              </div>
              <div className="calender-indicator-name">
                <span className="in-persion" />
                In person
              </div>
            </div>
          </Container>
        </div>
        <Container>
          <div className="monthMobile_eventBody">
            <h4 className="total-event-count">
              {groupedDates.total}{" "}
              {groupedDates.total === 1 ? "Event" : "Events"}
            </h4>
            {(function(data) {
              // console.log(data);
              const soy = startOfYear(currentDate).getTime();
              const som = startOfMonth(currentDate).getTime();

              if (!data[soy]) {
                console.log("SOY");
                return "";
              }

              if (!data[soy][som]) {
                console.log("SOM");
                return "";
              }

              const sodKeys = Object.keys(data[soy][som]);
              if (!sodKeys.length) {
                return "";
              }

              const soym = data[soy][som];
              return sodKeys.map((sod, idx) => {
                if (idx > 2) {
                  return null;
                }
                return (
                  <React.Fragment key={sod}>
                    <div className="event-day-detail-block">
                      <div>
                        <h5 className="event-day-date">
                          {format(Number(sod), "D ddd")}
                        </h5>
                      </div>
                      <div>
                        <h5 className="event-count">
                          {soym[sod].length} Events
                        </h5>
                      </div>
                    </div>
                    <div>
                      {Array.from(soym[sod]).map(interview => {
                        return (
                          <div className="event-details" key={interview.id}>
                            <div>
                              <h5 className="event-time">
                                {format(interview.start, "hh:mm A")} -{" "}
                                {format(interview.start, "hh:mm A")}
                              </h5>
                            </div>
                            <div style={{ padding: "15px" }}>
                              <div style={{ paddingBottom: "10px" }}>
                                <span className="telephone" />
                                <span className="for-event-name">
                                  {interview.title}
                                </span>
                              </div>
                              <div style={{ paddingBottom: "10px" }}>
                                <span className="video" />
                                <span className="for-event-name">
                                  {interview.title}
                                </span>
                              </div>
                            </div>
                          </div>
                        );
                      })}
                    </div>
                  </React.Fragment>
                );
              });
            })(groupedDates.data)}

            {groupedDates.total > 1 ? (
              <div
                className="calendar-view-all"
                onClick={e => onViewChange(VIEW_STATES.MOBILE_ALLVIEW)}>
                <a href="javascript:;">View all events</a>
              </div>
            ) : null}
          </div>
        </Container>
        {/* <Grid columns={1}>
          <Grid.Column>
            
            
          </Grid.Column>
        </Grid> */}
      </div>
    );
  }
}

export default MobileCalendar;
