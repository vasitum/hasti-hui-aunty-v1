import React, { Component } from "react";
import {
  Grid,
  List,
  Dropdown,
  Header,
  Table,
  Rating,
  Icon,
  Button,
  Container
} from "semantic-ui-react";
import Calendar from "react-calendar";
import MobileHeader from "../../MobileComponents/MobileHeader";
import IcFooterArrowIcon from "../../../assets/svg/IcFooterArrowIcon";
import ViewDropdown from "./components/ViewDropdown";
import CustomDateHeader from "./components/CustomDateViewHeader";
import DashBoardNavigationDrawer from "../../MobileComponents/DashBoardNavigationDrawer";
import {
  eachDay,
  startOfWeek,
  endOfWeek,
  startOfYear,
  startOfMonth,
  startOfDay,
  format
} from "date-fns";

const options = [
  { key: "month", text: "Month" },
  { key: "week", text: "Week" },
  { key: "day", text: "Day" }
];

function formatForWeeks(data, currentDate) {
  const soy = startOfYear(currentDate).getTime();
  const som = startOfMonth(currentDate).getTime();
  const sow = startOfWeek(currentDate).getTime();
  const eow = endOfWeek(currentDate).getTime();
  const daysbtw = eachDay(sow, eow).map(date => startOfDay(date).getTime());

  if (!data[soy]) {
    return [];
  }

  if (!data[soy][som]) {
    return [];
  }

  const sodKeys = Object.keys(data[soy][som]);
  if (!sodKeys.length) {
    return [];
  }

  let ret = {};
  let globalTotal = 0;

  const soym = data[soy][som];
  sodKeys.map(sod => {
    if (Number(sod) > eow || Number(sod) < sow) {
      return;
    }

    const interviews = soym[sod];
    let counter = {
      telephonic: 0,
      inperson: 0,
      videoCall: 0,
      total: 0
    };
    interviews.map(interview => {
      if (interview.inP) {
        counter.inperson = counter.inperson + 1;
        counter.total = counter.total + 1;
        globalTotal = globalTotal + 1;
        return;
      }

      if (interview.tP) {
        counter.telephonic = counter.telephonic + 1;
        counter.total = counter.total + 1;
        globalTotal = globalTotal + 1;
        return;
      }
      if (interview.vC) {
        counter.videoCall = counter.videoCall + 1;
        counter.total = counter.total + 1;
        globalTotal = globalTotal + 1;
        return;
      }
    });

    ret[sod] = counter;
  });

  const retKeys = Object.keys(ret);
  return {
    total: globalTotal,
    data: daysbtw.map(date => {
      if (retKeys.indexOf(String(date)) < 0) {
        // console.log("not found");
        return {
          date: date,
          telephonic: 0,
          inperson: 0,
          videoCall: 0,
          total: 0
        };
      }

      return { date: date, ...ret[date] };
    })
  };
}

class WeekView extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      groupedDates,
      currentView,
      onViewChange,
      currentDate,
      onCurrentDateChange
    } = this.props;

    const dates = formatForWeeks(groupedDates.data, currentDate);
    // console.log("DATES ARE", dates);
    return (
      <div className="calender-event-week-view calendar">
        <MobileHeader
          // headerLeftIcon={
          //   <Button compact className="backArrowBtn">
          //     <IcFooterArrowIcon pathcolor="#6e768a" />
          //   </Button>
          // }
          headerLeftIcon={<DashBoardNavigationDrawer />}
          className="isMobileHeader_fixe"
          headerTitle="My Calendar">
          <ViewDropdown currentView={currentView} onViewChange={onViewChange} />
        </MobileHeader>
        {/* <Grid columns={1}>
          <Grid.Column className="calendar-header-strip">
            <List divided verticalAlign="middle">
              <List.Item>
                <List.Content floated="right">
                  <ViewDropdown
                    currentView={currentView}
                    onViewChange={onViewChange}
                  />
                </List.Content>
                <Arrow />{" "}
                <span className="calendar-header-font">My Calendar</span>
              </List.Item>
            </List>
          </Grid.Column>
        </Grid> */}

        <div className="weekMobile_container">
          <Container>
            <CustomDateHeader
              value={currentDate}
              onChange={onCurrentDateChange}
              type={"week"}
            />
            <div>
              <h4 className="total-event-count">
                {dates.total}{" "}
                <span className="calendar-event-tag">
                  {dates.total === 1 ? "Event" : "Events"}
                </span>
              </h4>
              {dates.total ? (
                <Table>
                  {dates.data.map(interview => {
                    return (
                      <Table.Body>
                        <Table.Row>
                          <Table.Cell>
                            <Table.Cell className="week-day-name">
                              {format(interview.date, "dd")}
                            </Table.Cell>
                          </Table.Cell>
                          <Table.Cell className="week-date">
                            {format(interview.date, "DD")}{" "}
                          </Table.Cell>
                          <Table.Cell>
                            <Table.Cell className="week-date-event-count">
                              {interview.total}{" "}
                              {interview.total === 1 ? "Event" : "Events"}
                            </Table.Cell>
                          </Table.Cell>
                          <Table.Cell>
                            <span className="week-date-event-type">
                              <span className="in-persion" />
                              <span className="week-date-event-type-indi">
                                {interview.inperson}
                              </span>
                            </span>
                          </Table.Cell>
                          <Table.Cell>
                            <span className="week-date-event-type">
                              <span className="video" />
                              <span className="week-date-event-type-indi">
                                {interview.videoCall}
                              </span>
                            </span>
                          </Table.Cell>
                          <Table.Cell>
                            <span className="week-date-event-type">
                              <span className="telephone" />
                              <span className="week-date-event-type-indi">
                                {interview.telephonic}
                              </span>
                            </span>
                          </Table.Cell>
                        </Table.Row>
                      </Table.Body>
                    );
                  })}
                </Table>
              ) : null}
            </div>
            <div className="indicator-block">
              <div className="calender-indicator-name">
                <span className="telephone" />
                Telephonic
              </div>
              <div className="calender-indicator-name">
                <span className="video" />
                Video call
              </div>
              <div className="calender-indicator-name">
                <span className="in-persion" />
                In person
              </div>
            </div>
          </Container>
        </div>
      </div>
    );
  }
}

export default WeekView;
