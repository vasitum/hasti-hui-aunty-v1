import React from "react";

export default class extends React.Component {
  componentWillMount() {
    if (this.context.setCanRefine) {
      this.context.setCanRefine(this.props.canRefine);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.context.setCanRefine &&
      this.props.canRefine !== nextProps.canRefine
    ) {
      this.context.setCanRefine(nextProps.canRefine);
    }
  }

  render() {
    return this.props.children;
  }
}
