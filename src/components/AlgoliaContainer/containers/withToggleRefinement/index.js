import React from "react";
import { connectToggleRefinement } from "react-instantsearch-dom";

function withToggleRefinement(WrappedComponent) {
  return connectToggleRefinement(
    class extends React.Component {
      render() {
        return <WrappedComponent {...this.props} />;
      }
    }
  );
}

export default withToggleRefinement;
