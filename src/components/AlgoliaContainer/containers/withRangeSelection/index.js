import React from "react";

import { connectRange } from "react-instantsearch-dom";

function withRangeSelection(WrappedComponent) {
  return connectRange(
    class extends React.Component {
      render() {
        return <WrappedComponent {...this.props} />;
      }
    }
  );
}

export default withRangeSelection;
