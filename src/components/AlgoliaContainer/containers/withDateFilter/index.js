import React from "react";
import { Configure } from "react-instantsearch-dom";

function withDateFilter(WrappedComponent) {
  return class extends React.Component {
    state = {
      from: 0,
      to: 0
    };

    refine = (min, max) => this.setState({ from: min, to: max });

    reset = () => this.setState({ from: 0, to: 0 });

    render() {
      const { attribute } = this.props;
      const { from, to } = this.state;
      return (
        <React.Fragment>
          {from > 0 && to > 0 ? (
            <Configure
              filters={`${attribute}>=${from} AND ${attribute}<=${to}`}
            />
          ) : null}
          <WrappedComponent {...this.props} refine={this.refine} />
        </React.Fragment>
      );
    }
  };
}

export default withDateFilter;
