import React from "react";
import { connectMenu } from "react-instantsearch-dom";

import PanelCallbackHandler from "../../utils/PanelCallbackHandler";

function withSelect(WrappedComponent) {
  return connectMenu(
    class extends React.Component {
      onSelectItem = value => {
        this.props.refine(value);
      };

      render() {
        return (
          <PanelCallbackHandler {...this.props}>
            <WrappedComponent
              {...this.props}
              filter={this.props.attribute}
              onSelect={this.onSelectItem}
            />
          </PanelCallbackHandler>
        );
      }
    }
  );
}

export default withSelect;
