import React from "react";
import { InstantSearch } from "react-instantsearch-dom";
import qs from "qs";

import { withAlgoliaContext } from "../../../../contexts/AlgoliaContext";

const urlToSearchState = location => qs.parse(location.search.slice(1));
const createURL = state => `?${qs.stringify(state)}`;

function withSearch(
  WrappedComponent,
  { appId, apiKey, indexName, noRouting, syncNavSearch, ...algoliaProps }
) {
  return withAlgoliaContext(
    class extends React.Component {
      state = {
        searchState: urlToSearchState(this.props.location)
      };

      componentDidMount() {
        if (
          this.state.searchState.query &&
          this.props.searchQuery !== this.state.searchState.query &&
          syncNavSearch
        ) {
          this.props.onSearhContextChange(this.state.searchState.query);
        }
      }

      componentWillReceiveProps(props) {
        if (props.location !== this.props.location) {
          this.setState({
            searchState: urlToSearchState(props.location)
          });
        }
      }

      onSearchStateChange = searchState => {
        if (!noRouting) {
          this.props.history.push({
            pathname: this.props.location.pathname,
            search: qs.stringify(searchState),
            state: searchState
          });
        }

        this.setState({ searchState });
      };

      render() {
        return (
          <InstantSearch
            {...algoliaProps}
            appId={appId}
            apiKey={apiKey}
            indexName={indexName}
            searchState={this.state.searchState}
            onSearchStateChange={this.onSearchStateChange}
            createURL={createURL}>
            <WrappedComponent {...this.props} />
          </InstantSearch>
        );
      }
    }
  );
}

export default withSearch;
