import React from "react";
import { Configure } from "react-instantsearch-dom";

function withCustomFilter(WrappedComponent) {
  return class extends React.Component {
    state = {
      filter: ""
    };

    refine = filter => {
      this.setState({ filter: filter });
    };

    render() {
      const { attribute } = this.props;
      const { filter } = this.state;
      return (
        <React.Fragment>
          {filter ? <Configure filters={filter} /> : null}
          <WrappedComponent {...this.props} refine={this.refine} />
        </React.Fragment>
      );
    }
  };
}

export default withCustomFilter;
