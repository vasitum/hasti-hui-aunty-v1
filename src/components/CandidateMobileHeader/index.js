import React from "react";
import { Grid, Dropdown, Header, Icon } from "semantic-ui-react";
import MobileHeader from "../MobileComponents/MobileHeader";

import "./index.scss";

const HeaderOption = [
  { text: "All jobs", value: "All jobs" },
  { text: "Pending jobs", value: "Pending jobs" },
  { text: "Interview jobs", value: "Interview jobs" },
  { text: "Rejected jobs", value: "Rejected jobs" },
  { text: "Completed", value: "Completed" }
];
const CandidateMobileHeader = () => (
  <div className="Main_Header">
    <Grid>
      <Grid.Row>
        <Grid.Column>
          <MobileHeader
            mobileHeaderTitle="hello"
            mobileHeaderIcon={<Icon name="arrow left" />}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <Dropdown
            fluid
            scrolling
            options={HeaderOption}
            defaultValue={HeaderOption[0].value}
          />
        </Grid.Column>
      </Grid.Row>
    </Grid>
  </div>
);

export default CandidateMobileHeader;
