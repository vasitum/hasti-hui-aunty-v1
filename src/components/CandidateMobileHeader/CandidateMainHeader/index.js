import React from 'react'
import  {Grid,Dropdown,Header } from 'semantic-ui-react'
import MobileHeader from "../MobileComponents/MobileHeader";

import "./index.scss";

const CandidateMainHeader = () => (
    <div className="Main_Header">
        <Grid>
            <Grid.Row>
                <Grid.Column>
                   <MobileHeader mobileHeaderTitle="hello"
                   mobileHeaderIcon="Icon"
                   />
                </Grid.Column>
            </Grid.Row>
        </Grid>
    </div>
  )
  
  export default CandidateMainHeader
