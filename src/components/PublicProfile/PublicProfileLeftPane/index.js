import React from 'react';

// import { Link } from 'react-router-dom';
import { Grid,Header, Button  } from "semantic-ui-react";
import CardSummary from "../../Cards/CardSummary";
import SkillList from "../../CardElements/SkillList";
import IcLocation from "../../../assets/svg/IcLocation";
import ActionBtn from "../../Buttons/ActionBtn";

import "./index.scss";

class PublicProfileLeftPane extends React.Component {
  

  render() {
    return (
      <div className="PublicProfileLeftPane">
        <div className="publicProfileView">
          <div className="publicprofileHeader">
            <Grid>
              <Grid.Row>
                <Grid.Column  computer={8} mobile={16}>
                  <div>
                    <Header as="h2">
                      Java Developer
                    </Header>
                    <p>
                      <IcLocation height="12px" width="10px" pathcolor="#0b9ed0" />
                      Noida, Uttar Pradesh, India
                    </p>
                  </div>
                </Grid.Column>
                <Grid.Column computer={8} only="computer" textAling="right">
                  <div className="HeaderBtn">
                    <Button>Save Job</Button>
                    <ActionBtn actioaBtnText="Apply" />
                  </div>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </div>
        </div>
      </div>
    );
  }
}
  
  export default PublicProfileLeftPane;