import React from 'react';

// import { Link } from 'react-router-dom';
import { Container, Grid } from "semantic-ui-react";

import PageBanner from "../../components/Banners/PageBanner";
import aunty from "../../assets/banners/aunty.png";
import PublicProfileLeftPane from "./PublicProfileLeftPane";
import PublicProfileRightPane from "./PublicProfileRightPane";

import "./index.scss";

class PublicProfile extends React.Component {
  

  render() {
    return (
      <div className="PublicProfile">
        <PageBanner size="medium" image={aunty}/>

        <div className="publicprofile">
          <Container>
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} computer={12}>
                  <PublicProfileLeftPane />
                </Grid.Column>
                <Grid.Column width={4} className="mobile hidden">
                  <PublicProfileRightPane />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Container>
        </div>
      </div>
    );
  }
}
  
  export default PublicProfile;