import React from "react";
import "./index.scss";
import { withChatbotContext } from "../../../contexts/ChatbotContext";
import img from "./img/vasi_img_new.png";
import { withRouter } from "react-router-dom";

class CBSmallView extends React.Component {
  state = {};

  componentDidMount() {
    setTimeout(() => {
      if (this.props.actions) {
        if (window.screen.availWidth <= 1024) {
          // Don't auto trigger vasi in mobile
          // this.props.actions.onMaximize();
        } else {
          this.props.actions.onExpand();
        }
      }
    }, 7000);
  }

  render() {
    const { actions, location } = this.props;

    return (
      <div
        onClick={e => {
          if (window.screen.availWidth <= 1024) {
            actions.onMaximize();
          } else {
            actions.onExpand();
          }
        }}
        className="Chatbot__smallView">
        <img src={img} alt={"Chatbot Text"} />
      </div>
    );
  }
}

export default withChatbotContext(withRouter(CBSmallView));
