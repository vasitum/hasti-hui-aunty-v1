import React, { Component } from 'react';
import Company from './images/wipro.png';
import getRelativeTime from './Time.js';
import Com from './images/com.png';
import loc from './images/loc.png';
import assistant from './images/assistant2.png';




class Card2 extends Component{
  componentDidMount(){
		this.props.disableInput();
	}

  render(){
    
   const alltext =  this.props.msg.content.map(
      text =>{
        return(
  <span className="chatbot__sub-text">
  {text}
  </span>)


      }
    )
    return(
  <div className="chatbot__card2">
  <div className="chatbot__card2-main">
  <div className="chatbot__card2-icon"><img src={this.props.msg.image} alt="Card2  not available" /></div>
  <div className="chatbot__card2-details">
  <span className="chatbot__card2-heading">{this.props.msg.heading}</span>

  <span className="chatbot__card2-item"><img src={Com} alt="not available" className="chatbot__card2-sm-icon" />&nbsp;&nbsp; {this.props.msg.compname}</span>
  <span className="chatbot__card2-item"><img src={loc} alt="not available" />&nbsp;&nbsp;&nbsp;&nbsp; {this.props.msg.location}</span>

  </div>

  
  </div>
  {alltext}
  <div className="chatbot__time" style={{paddingRight:"10px",top:"2px"}} >{getRelativeTime(this.props.msg.time)} </div>

  </div>





    )
  }
}

export default Card2;
