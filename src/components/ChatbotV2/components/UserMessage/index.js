import React from "react";
import vasi from "../../cbcircle/img/vasi_img_new.png";
import UserLogo from "../../../../assets/svg/IcUser";
import "./index.scss";

function compileText(text) {
  return text.split("<br>");
}

export default class extends React.Component {
  state = {};

  render() {
    const { text, time, hasImage, userId } = this.props;
    // const finalText = compileText(text);
    return (
      <div className="UserMessage__Vasi">
        <div className="UserMessage_image">
          {hasImage ? (
            <img
              style={{
                borderRadius: "50%"
              }}
              src={`https://s3-us-west-2.amazonaws.com/img-mwf/${userId}/image.jpg`}
              alt="Vasi"
            />
          ) : (
            <UserLogo
              width="35"
              height="35"
              rectcolor="#ffffff"
              pathcolor="#c8c8c8"
            />
          )}
        </div>
        <div className={"UserMessage_bodyContainer"}>
          <div className="UserMessage_body">
            {text}
            <div className="UserMessage_time">{time}</div>
          </div>
        </div>
      </div>
    );
  }
}
