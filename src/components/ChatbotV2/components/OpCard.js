import React, { Component } from "react";
import assistant from "./images/assistant2.png";
import getCurrentTime from "./Time.js";

function renderText(text) {
  const mtext = document.createElement("p");
  mtext.innerHTML = text;
  return mtext.innerText;
}

class OpCard extends Component {
  state = {
    active: "chatbot__first-button-active",
    inactive: "chatbot__first-button",
    count: 0,
    copy: this.props.msg.option,
    clicked: false
  };
  clickHandler(param, e) {
    this.props.addParams(param, this.props.id);
  }
  changeButtonState = data => {
    if (this.props.active) {
      // let keys = Object.keys(this.refs);
      // keys.map(key => {
      //   this.refs[key.toString()].className = this.state.inactive;
      // });
      // this.refs[data].className = this.state.active;
    } else {
      this.clickHandler(data);
      let keys = Object.keys(this.refs);

      keys.map(key => {
        this.refs[key.toString()].className = this.state.inactive;
      });
      this.refs[data].className = this.state.active;
      this.buttonContainer.className = "chatbot__hide_buttons";
    }
  };

  componentDidMount() {
    this.props.disableInput();
    if (this.props.active) {
      this.buttonContainer.className = "chatbot__hide_buttons";
    }
  }

  render() {
    console.log(this.props.msg.content);
    const msgList = Array.from(this.props.msg.content)
      .reverse()
      .map(msgtext => {
        return (
          <div className="chatbot__message-bot" key={this.props.msg.id}>
            {renderText(msgtext)}
            <div
              className="chatbot__bot-time"
              style={{
                paddingRight: "10px",
                marginTop: "3px",
                "font-size": "10px"
              }}>
              {getCurrentTime()}{" "}
            </div>
          </div>
        );
      });

    const buttonList = this.state.copy.map(button => {
      if (this.props.active === button) {
        return (
          <div
            className={this.state.active}
            ref={button}
            onClick={e => this.changeButtonState(button)}>
            {button}
          </div>
        );
      } else {
        return (
          <div
            className={this.state.inactive}
            ref={button}
            onClick={e => this.changeButtonState(button)}>
            {button}
          </div>
        );
      }
    });

    return (
      <div>
        {msgList}
        <div className="chatbot__op-container">
          <div className="chatbot__op-card">
            <div
              className="chatbot__bot-time"
              style={{
                paddingRight: "10px",
                marginTop: "3px",
                "font-size": "10px",
                bottom: "0px",
                color: "rgba(87, 87, 87, 1)"
              }}>
              {getCurrentTime()}{" "}
            </div>
            <div className="chatbot__op-text">
              {renderText(this.props.msg.text)}
            </div>
            <div className="chatbot__bot-assistant" style={{ left: "-35px" }}>
              <img src={assistant} alt="assistant pic not available" />
            </div>
          </div>
        </div>
        <div
          className="chatbot__buttons"
          ref={el => {
            this.buttonContainer = el;
          }}>
          {buttonList}
        </div>
      </div>
    );
  }
}

export default OpCard;
