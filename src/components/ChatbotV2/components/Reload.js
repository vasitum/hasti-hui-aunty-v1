import React from "react";

export default ({ show, onClick, children }) => {
  if (!show) {
    return null;
  }

  return (
    <div className={"Chatbot__reload"}>
      <button className={"Chatbot__reload--btn"} onClick={onClick}>
        {children}
      </button>
    </div>
  );
};
