import React from "react";
import vasi from "../../cbcircle/img/vasi_img_new.png";
import "./index.scss";

function compileText(text) {
  return text.split("<br>");
}

export default class extends React.Component {
  state = {};

  render() {
    const { text, time, loading } = this.props;

    if (loading) {
      return (
        <div className="VasiMessage">
          <div className="VasiMessage_image">
            <img src={vasi} alt="Vasi" />
          </div>
          <div className={"VasiMessage_bodyContainer"}>
            <div className={"VasiMessage_loading"}>
              <span>.</span>
              <span>.</span>
              <span>.</span>
            </div>
          </div>
        </div>
      );
    }

    const finalText = compileText(text);
    return (
      <div className="VasiMessage">
        <div className="VasiMessage_image">
          <img src={vasi} alt="Vasi" />
        </div>
        <div className={"VasiMessage_bodyContainer"}>
          {finalText.map(txt => {
            return (
              <div className="VasiMessage_body">
                {txt}
                <div className="VasiMessage_time">{time}</div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}
