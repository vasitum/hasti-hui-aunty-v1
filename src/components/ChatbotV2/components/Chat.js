import React, { Component } from "react";
import Title from "./Title.js";
import Messages from "./Messages.js";
import Input from "./Input.js";
import ReloadBtn from "./Reload";

class Chat extends Component {
  state = {
    vis: this.props.vis.display,
    style: this.props.style.width
  };
  componentWillUnmount() {
    console.log("chat unmounted");
    this.props.saveState();
    // axios
    //       .get(
    //         "http://149.129.137.48:5009/api/SaveInstance/" +
    //           this.props.userdata.userId +
    //           ":" +
    //           this.props.userdata.jobId
    //       )
    //       .then(res => {
    //         console.log("updated server state");

    //       });
  }
  componentWillMount() {
    if (this.props.mode === "screening" || this.props.mode === "interview") {
      this.props.changeBackStyle("block");
    }
  }
  render() {
    const { mUser } = this.props;

    return (
      <div className="chatbot__chat" style={this.props.style}>
        {/* {this.props.mode === "screening" ? (
          <ReloadBtn
            onClick={() => alert("Clicked")}
            show={this.props.messages && this.props.messages.length > 2}>
            Restart Screening
          </ReloadBtn>
        ) : null} */}
        <Title
          restartProcess={this.props.restartProcess}
          search_key={this.props.search_key}
          input={this.props.input}
          incCounter={this.props.incCounter}
          decCounter={this.props.decCounter}
          searchResult={this.props.searchResult}
          enableInput={this.props.enableInput}
          disableInput={this.props.disableInput}
          setSearch={this.props.setSearch}
          text_search={this.props.text_search}
          back_style={this.props.back_style}
          mode={this.props.mode}
          vis={this.props.vis}
          style={this.props.style}
          login={this.props.login}
          title={this.props.title}
          user_data={this.props.user_data}
          mobile={this.props.mobile}
          status={this.props.changeChatStatus}
          data={"chat"}
          changeGrid={this.props.changeGrid}
          catStatus={this.props.changeCatStatus}
          grid={this.props.grid}
        />
        <Messages
          mUser={mUser}
          onMinimize={this.props.onMinimize}
          height={this.props.height}
          changeBackStyle={this.props.changeBackStyle}
          loader={this.props.loader}
          resetkey={this.props.resetkey}  
          eof={this.props.eof}
          text_search={this.props.search}
          enableInput={this.props.enableInput}
          disableInput={this.props.disableInput}
          setButton={this.props.setButton}
          addDummy={this.props.addDummy}
          reset={this.props.reset}
          messages={this.props.messages}
          addParams={this.props.addParams}
          addMessages={this.props.addMessages}
          refs="messages"
          search_key={this.props.search_key}
        />
        <Input
          input={this.props.input}
          text_search={this.props.search}
          option_button={this.props.button}
          messages={this.props.messages}
          addMessages={this.props.addMessages}
          style={this.props.style}
        />
      </div>
    );
  }
}

export default Chat;
