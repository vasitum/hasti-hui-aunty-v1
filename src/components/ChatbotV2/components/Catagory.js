import React, { Component } from "react";
// import Recommend from './images/card_recommendation.png';
// import Screen from './images/card_screening.png';
// import Interview from './images/card_interview.png';
import Recommend from "./images/recommend.png";
import Screen from "./images/screening.png";
import Interview from "./images/interview.png";
import Forwardarrow from "./images/forward.png";
import Card2 from "./Card2.js";
import Card from "./Card.js";
import axios from "axios";
import getCurrentTime from "./Time.js";
import getRelativeTime from "./Time.js";
import assistant1 from "./images/messages.png";
import assistant from "./images/assistant2.png";
import BirthCard from "./BirthCard";
import Button from "./Button";
import SpringScrollbars from "./SpringScrollbars";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

function shouldShowBack(user) {
  if (!user) {
    return false;
  }

  if (!user.userId) {
    return false;
  }

  return true;
}

class Catagory extends Component {
  state = {
    cards: [],
    screencard: [],
    interviewcard: [],
    Recommendation: true,
    Screening: false,
    Interview: false
  };

  componentWillUnmount() {
    this.props.saveState();
    console.log("catagory unmounted");
    axios
      .get(
        "https://vasitum.com:5009/api/SaveInstance/" +
          this.props.userdata.userId +
          ":" +
          this.props.userdata.jobId
      )
      .then(res => {
        console.log("updated server state");
      });
  }
  changeFunction = param => {};
  recClick = (param, JobId) => {};
  cardClick = (param, jobId) => {
    if (this.props.grid) {
      if (param === "screening") {
        this.props.showScreening(jobId);

        this.props.changeMode("screening");
      } else if (param === "interview") {
        this.props.showInterview(jobId);
      }
    } else {
      if (this.props.mode === "screening") {
        this.props.showScreening(jobId);
        this.props.changeJobId(jobId, this.props.mode);
      } else if (this.props.mode === "interview") {
        this.props.showInterview(jobId);
        this.props.changeJobId(jobId, this.props.mode);
      }
    }
  };
  componentDidUpdate() {
    const { scrollbars } = this.refs;
    if (scrollbars) {
      const height = scrollbars.getScrollHeight();
      scrollbars.scrollTop(height);
    }
  }
  componentDidMount() {
    const { scrollbars } = this.refs;
    if (scrollbars) {
      const height = scrollbars.getScrollHeight();
      scrollbars.scrollTop(height);
    }
  }

  componentWillMount() {
    //////console.log(this.props.mode);
    if (this.props.mode === "general") {
      this.setState({
        style: {
          display: ""
        }
      });
    }

    //     axios.get("http://35.154.164.193:5009/api/Recommendation/shivam").then(res => {
    //       let cards = res.data;
    //       //////console.log(cards);
    //       this.setState({
    //         cards: cards
    //       });
    //     });

    // if(this.props.mode !== "interview"){
    //     axios
    //       .get(
    //         "http://35.154.164.193:5009/api/send_initial_screening_message/" +
    //           this.props.userdata.jobId +
    //           "/" +
    //           this.props.userdata.userId +
    //           ":" +
    //           "first"
    //       )
    //       .then(res => {
    //         let cards = res.data;
    //         //////console.log(cards);
    //         this.setState({
    //           screencard: cards,
    //           interviewcard: cards
    //         });
    //       });
    //     }
    //     else{
    //       axios
    //       .get(
    //         "http://35.154.164.193:5009/api/send_initial_interview_message/" +
    //           this.props.userdata.jobId +
    //           "/" +
    //           this.props.userdata.userId +
    //           ":" +
    //           "first"
    //       )
    //       .then(res => {
    //         let cards = res.data;
    //         //////console.log(cards);
    //         this.setState({

    //           interviewcard: cards
    //         });
    //       });

    //     }
  }

  clickFunction = () => {
    //////console.log(this.props.mode);
    // if (this.props.mode === "general") {
    //   this.props.showGeneral();
    // } else if (this.props.mode === "Recommendation") {
    //   axios.get("http://35.154.164.193:5009/api/Recommendation/shivam").then(res => {
    //     let cards = res.data;
    //     //////console.log(cards);
    //     this.setState({
    //       cards: cards
    //     });
    //   });
    // }
    // if (this.props.mode === "screening") {
    //   axios
    //     .get(
    //       "http://35.154.164.193:5009/api/send_initial_screening_message/" +
    //         this.props.userdata.jobId +
    //         "/" +
    //         this.props.userdata.userId +
    //         ":" +
    //         "first"
    //     )
    //     .then(res => {
    //       let cards = res.data;
    //       //////console.log(cards);
    //       this.setState({
    //         screencard: cards
    //       });
    //     });
    // } else if (this.props.mode === "interview") {
    // } else {
    //   this.setState({
    //     cards: []
    //   });
    // }
  };

  render() {
    const cardList = this.props.recommendation_status_card.length
      ? this.props.recommendation_status_card
          .slice(0)
          .reverse()
          .map((card, index) => {
            if (card.type === "Card2") {
              return (
                <Link to={"/view/job/" + card.jobId}>
                  {" "}
                  <Card2 msg={card} key={index} />{" "}
                </Link>
              );
            } else if (card.type === "Card1") {
              return (
                <Link to={"/view/job/" + card.jobId}>
                  {" "}
                  <Card msg={card} key={index} click={this.recClick} />{" "}
                </Link>
              );
            } else if (card.type === "birth") {
              return <BirthCard msg={card} key={index} />;
            } else if (card.type === "") {
              return <Button msg={card} key={index} />;
            } else {
              return (
                <div className="chatbot__message-bot_card" key="192jsdk">
                  {card.content[0]}
                  <div
                    className="chatbot__bot-time"
                    style={{
                      paddingRight: "10px",
                      marginTop: "3px",
                      "font-size": "10px"
                    }}>
                    {getCurrentTime()}{" "}
                  </div>
                </div>
              );
            }
          })
      : "";

    const screencardList = this.props.screening_status_card.length
      ? this.props.screening_status_card
          .slice(0)
          .reverse()
          .map(card => {
            if (card.type === "Card2") {
              return <Card2 msg={card} key={card.id} />;
            }
            if (card.type === "Card1") {
              return <Card msg={card} key={card.id} click={this.cardClick} />;
            }
          })
      : "";

    const interviewcardList = this.props.interview_status_card.length
      ? this.props.interview_status_card
          .slice(0)
          .reverse()
          .map(card => {
            if (card.type === "Card2") {
              return <Card2 msg={card} key={card.id} />;
            }
            if (card.type === "Card1") {
              return <Card msg={card} key={card.id} click={this.cardClick} />;
            }
          })
      : "";

    const recommendationCard = this.props.catcard.RecommendationCard ? (
      <div
        className={`chatbot__catagory-item ${
          shouldShowBack(this.props.userdata) ? "" : "is-hidden"
        }`}
        onClick={this.props.changeToRecommendCard}>
        <div className="chatbot__process-icon" style={this.props.catstyle}>
          <img
            src={Recommend}
            alt="icon not available"
            style={{ paddingBottom: "14px" }}
          />
        </div>
        <div className="chatbot__process-detail" style={this.props.catstyle}>
          <div className="chatbot__process-name">Recommendation</div>
          <div
            className="chatbot_forward"
            style={this.props.catstyle}
            onClick={e => this.changeFunction("recommendation", e)}>
            <img
              src={Forwardarrow}
              alt="no forward icon"
              class="chatbot_forward_icon"
            />
          </div>
          <div className="chatbot__process-time">
            {this.props.time.rec_time}
          </div>
        </div>

        {/* {cardList} */}
      </div>
    ) : (
      ""
    );

    const screeningCard = this.props.catcard.ScreeningCard ? (
      <div
        className={`chatbot__catagory-item ${
          shouldShowBack(this.props.userdata) ? "" : "is-hidden"
        }`}
        onClick={this.props.changeToScreenCard}>
        <div className="chatbot__process-icon" style={this.props.catstyle}>
          <img
            src={Screen}
            alt="icon not available"
            style={{ paddingBottom: "12px" }}
          />
        </div>
        <div className="chatbot__process-detail" style={this.props.catstyle}>
          <div className="chatbot__process-name">Screening </div>
          <div
            className="chatbot_forward"
            style={this.props.catstyle}
            onClick={e => this.changeFunction("screening", e)}>
            <img
              src={Forwardarrow}
              alt="no forward icon"
              class="chatbot_forward_icon"
            />
          </div>
          <div className="chatbot__process-time">
            {this.props.time.screen_time}
          </div>
        </div>

        {/* {screencardList} */}
      </div>
    ) : (
      ""
    );

    const interviewCard = this.props.catcard.InterviewCard ? (
      <div
        className={`chatbot__catagory-item ${
          shouldShowBack(this.props.userdata) ? "" : "is-hidden"
        }`}
        onClick={this.props.changeToInterviewCard}>
        <div className="chatbot__process-icon" style={this.props.catstyle}>
          <img
            src={Interview}
            alt="icon not available"
            style={{ paddingBottom: "12px" }}
          />
        </div>
        <div className="chatbot__process-detail" style={{ border: "none" }}>
          <div className="chatbot__process-name">Interview </div>
          <div
            className="chatbot_forward"
            style={this.props.catstyle}
            onClick={e => this.changeFunction("interview", e)}>
            <img
              src={Forwardarrow}
              alt="no forward icon"
              class="chatbot_forward_icon"
            />
          </div>
          <div className="chatbot__process-time">
            {this.props.time.interview_time}
          </div>
        </div>

        {/* {interviewcardList} */}
      </div>
    ) : (
      ""
    );

    const chatCard = this.props.catcard.ChatCard ? (
      <div className="chatbot__catagory-item" onClick={this.props.showGeneral}>
        <div className="chatbot__process-icon">
          <img
            src={assistant1}
            style={{ paddingBottom: "12px" }}
            alt="icon not available"
          />
        </div>
        <div className="chatbot__process-detail">
          <div className="chatbot__process-name">Chat </div>
          <div className="chatbot__process-time">
            {this.props.time.general_time}
          </div>
        </div>
        <div
          className="chatbot_forward"
          style={this.props.catstyle}
          onClick={e => this.changeFunction("chat", e)}>
          <img
            src={Forwardarrow}
            alt="no forward icon"
            class="chatbot_forward_icon"
          />
        </div>
      </div>
    ) : (
      ""
    );

    if (this.props.grid) {
      return (
        <SpringScrollbars
          ref="scrollbars"
          style={{ height: "calc(100% - 60px)", marginTop: "-20px" }}>
          <div className="chatbot__catagory" style={this.props.vis}>
            {this.props.tic.tic_general ? (
              <div className="chatbot__general-tic">
                <span className="chatbot__number1">
                  {this.props.tic.tic_general}
                </span>
              </div>
            ) : (
              ""
            )}
            {chatCard}

            {this.props.tic.tic_recommendation ? (
              <div className="chatbot__rec-tic">
                <span className="chatbot__number1">
                  {this.props.tic.tic_recommendation}
                </span>
              </div>
            ) : (
              ""
            )}
            {recommendationCard}
            {this.props.tic.tic_screening ? (
              <div className="chatbot__screen-tic">
                <span className="chatbot__number1">
                  {this.props.tic.tic_screening}
                </span>
              </div>
            ) : (
              ""
            )}
            {screeningCard}
            {this.props.tic.tic_interview ? (
              <div className="chatbot__interview-tic">
                <span className="chatbot__number1">
                  {this.props.tic.tic_interview}
                </span>
              </div>
            ) : (
              ""
            )}

            {interviewCard}
          </div>
        </SpringScrollbars>
      );
    } else {
      if (this.props.mode === "screening") {
        if (this.props.screening_status_card.length === 0) {
          return (
            <div style={{ height: "calc(100% - 60px)", background: "#f7f7fb" }}>
              <div
                className="chatbot__message-left"
                style={{ paddingLeft: "10px" }}>
                <div className="chatbot__bot-assistant">
                  <img
                    style={{ marginLeft: "10px" }}
                    src={assistant}
                    alt="assistant pic not available"
                  />
                </div>
                <div
                  className="chatbot__message-bot"
                  key="a1b2c3"
                  style={{ paddingBottom: "15px", paddingRight: "38px" }}>
                  There is no Screening
                  <div
                    className="chatbot__bot-time"
                    style={{
                      paddingRight: "10px",
                      marginTop: "3px",
                      fontSize: "10px"
                    }}>
                    {getCurrentTime()}{" "}
                  </div>
                </div>
              </div>
            </div>
          );
        } else {
          return (
            <SpringScrollbars
              ref="scrollbars"
              style={{ height: "calc(100% - 60px)", background: "#f7f7fb" }}>
              <div>{this.props.grid ? null : screencardList}</div>
            </SpringScrollbars>
          );
        }
      } else if (this.props.mode === "interview") {
        if (this.props.interview_status_card.length === 0) {
          return (
            <div style={{ height: "calc(100% - 60px)", background: "#f7f7fb" }}>
              <div
                className="chatbot__message-left"
                style={{ paddingLeft: "10px" }}>
                <div className="chatbot__bot-assistant">
                  <img
                    style={{ marginLeft: "10px" }}
                    src={assistant}
                    alt="assistant pic not available"
                  />
                </div>
                <div
                  className="chatbot__message-bot"
                  key="a1b2c3"
                  style={{ paddingBottom: "15px", paddingRight: "38px" }}>
                  There is no Interview
                  <div
                    className="chatbot__bot-time"
                    style={{
                      paddingRight: "10px",
                      marginTop: "3px",
                      fontSize: "10px"
                    }}>
                    {getCurrentTime()}{" "}
                  </div>
                </div>
              </div>
            </div>
          );
        } else {
          return (
            <SpringScrollbars
              ref="scrollbars"
              style={{ height: "calc(100% - 60px)", background: "#f7f7fb" }}>
              <div>{interviewcardList}</div>
            </SpringScrollbars>
          );
        }
      } else {
        if (this.props.recommendation_status_card.length === 0) {
          return (
            <div style={{ height: "calc(100% - 60px)", background: "#f7f7fb" }}>
              <div
                className="chatbot__message-left"
                style={{ paddingLeft: "10px" }}>
                <div className="chatbot__bot-assistant">
                  <img
                    style={{ marginLeft: "10px" }}
                    src={assistant}
                    alt="assistant pic not available"
                  />
                </div>
                <div
                  className="chatbot__message-bot"
                  key="a1b2c3"
                  style={{ paddingBottom: "15px", paddingRight: "38px" }}>
                  There is no Recommendation
                  <div
                    className="chatbot__bot-time"
                    style={{
                      paddingRight: "10px",
                      marginTop: "3px",
                      fontSize: "10px"
                    }}>
                    {getCurrentTime()}
                  </div>
                </div>
              </div>
            </div>
          );
        } else {
          return (
            <SpringScrollbars
              ref="scrollbars"
              style={{ height: "calc(100% - 60px)", background: "#f7f7fb" }}>
              <div style={{ height: "500px" }}>{cardList}</div>
            </SpringScrollbars>
          );
        }
      }
    }
  }
}

export default Catagory;
