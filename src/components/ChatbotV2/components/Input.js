import React, { Component } from "react";
import Send from "./images/send.png";
import { toast } from "react-toastify";

class Input extends Component {
  state = {
    content: [],
    sendIcon: "none"
  };
  submitHandler = e => {
    this.setState({
      sendIcon: "none"
    });
    e.preventDefault();

    const messages = [...this.props.messages];
    if (messages) {
      const lastMessage = messages.pop();
      if (lastMessage.validate) {
        if (/^[0]?[789]\d{9}$/.test(this.state.content[0])) {
          this.props.addMessages(this.state);

          this.setState({
            content: []
          });
          return;
        } else {
          toast("Please enter a correct number");
          return;
        }
      }
    }

    if (this.state.content.length === 0) {
      // console.log(this.state.content);
    } else {
      this.props.addMessages(this.state);

      this.setState({
        content: []
      });
    }
  };
  changeHandler = e => {
    this.setState({
      sendIcon: "",
      content: [e.target.value]
    });
  };

  // getMessageClass = (messages) => {
  //   const message = messages.reverse().pop();
  //   if (!message) {
  //     return "chatbot__message-box"
  //   }

  //   const { type } = message;
  //   if (type === "bot" || type === "option") {
  //     return "chatbot__message-box"
  //   } else {
  //     return "chatbot__message-box-alt"
  //   }
  // }

  render() {
    // const { messages } = this.props;
    // const messageClass = this.getMessageClass(messages)

    return (
      <div
        className={this.props.input}
        style={
          this.props.style["width"] === "0px"
            ? { width: "0px", display: "none" }
            : this.props.style
        }>
        <form onSubmit={this.submitHandler} autoComplete="off">
          <input
            className="chatbot__input"
            style={this.props.style}
            type="text"
            placeholder="Type here .."
            id="message-input"
            onChange={this.changeHandler}
            value={this.state.content}
          />
          <div
            onClick={this.submitHandler}
            className="chatbot__submit"
            style={{ display: this.state.sendIcon }}>
            <img src={Send} alt="submit-icon not available" />
          </div>
        </form>
      </div>
    );
  }
}

export default Input;
