import React from "react";
import vasi from "../../../cbcircle/img/vasi_img_new.png";
import collapse from "../../images/icon-collapse.png";
import Back from "../../images/backarrow.png";
import "./index.scss";

function shouldShowBack(user) {
  if (!user) {
    return false;
  }

  if (!user.userId) {
    return false;
  }

  return true;
}

export default class extends React.Component {
  constructor(props) {
    super(props);

    // bind
    this.restartChatOnServer = this.restartChatOnServer.bind(this);
  }

  state = {
    loading: true,
    loadingBtn: true
  };

  async restartChatOnServer() {
    const { userId } = this.props;

    try {
      const res = await fetch(
        `https://chatbot.vasitum.com/conversations/${userId}/respond`,
        {
          method: "POST",
          body: JSON.stringify({
            query: "restart"
          })
        }
      );
    } catch (error) {
      // chatbot unable to restart chat
      console.error(error);
    }
  }

  async componentDidMount() {
    const { user_data } = this.props;
    const showBack = shouldShowBack(user_data);
    this.restartChatOnServer();

    setTimeout(
      () => {
        this.setState({
          loading: false
        });

        setTimeout(() => {
          this.setState({
            loadingBtn: false
          });
        }, 1000);
      },
      showBack ? 3000 : 10000
    );
  }

  onCloseClick = (e, type) => {
    if (this.props.status) {
      this.props.status(type);
    }
  };

  onButtonClick = (e, type) => {
    e.preventDefault();
    if (this.props.onClick) {
      this.props.onClick(type);
    }
  };

  render() {
    const { loading, loadingBtn } = this.state;
    const { onClick, hidden, user_data } = this.props;
    const showBack = shouldShowBack(user_data);
    return (
      <div className={`Vasi__InitScreen ${hidden ? "is--hidden" : ""}`}>
        {showBack && (
          <img
            src={Back}
            onClick={e => this.onCloseClick(e, "cat")}
            className="Vasi__initBack"
            alt="close button"
          />
        )}
        <img
          src={collapse}
          onClick={e => this.onCloseClick(e, "close")}
          className="Vasi__initClose"
          alt="close button"
        />
        <img src={vasi} alt="vasi avatar" />
        <h2
          style={{
            marginTop: "0.5rem"
          }}>
          Hi! I am
        </h2>
        <h2 className="focused">Vasi</h2>
        {loading ? (
          <div className={"VasiIntialMessage_loading"}>
            <span>.</span>
            <span>.</span>
            <span>.</span>
          </div>
        ) : (
          <React.Fragment>
            <h2
              style={{
                marginBottom: "0px",
                marginTop: "10px"
              }}>
              i help people find jobs
            </h2>
            <h2
              style={{
                marginBottom: "0px",
                marginTop: "10px"
              }}>
              and recruiter find candidates
            </h2>
            <h2
              style={{
                marginBottom: "0px",
                marginTop: "10px"
              }}>
              How can i help you today?
            </h2>

            {!loadingBtn && (
              <div className="Vasi__InitScreen--footer">
                <p>Search for</p>
                <div className="Vasi__InitScreen--footer_child">
                  <button onClick={e => this.onButtonClick(e, "Jobs")}>
                    Jobs
                  </button>
                  <button onClick={e => this.onButtonClick(e, "Candidates")}>
                    Candidates
                  </button>
                </div>
              </div>
            )}
          </React.Fragment>
        )}
      </div>
    );
  }
}
