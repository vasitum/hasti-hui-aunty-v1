import React from "react";
import vasi from "../../../cbcircle/img/vasi_img_new.png";
import { Link } from "react-router-dom";
import fromNow from "../../../../../utils/env/fromNow";

import IcUser from "../../../../../assets/svg/IcUser"
import "./index.scss";

export default class extends React.Component {
  state = {};

  render() {
    const { data } = this.props;
    if (!data) {
      return null;
    }

    console.log("check data people", data.imgExt, data);
    return (
      <div className={"Vasi_JobCard"}>
        <div className={"Vasi_Body"}>
          <div className={"Vasi_BodyImage"}>
            {data.imgExt ? (<img src={`https://s3-us-west-2.amazonaws.com/img-mwf/${data.objectID}/image.jpg`} alt={"Vasi Image"} />) : (
              <IcUser
                width="35"
                height="35"
                rectcolor="#ffffff"
                pathcolor="#c8c8c8"
              />
            )}
            {/* <img src={vasi} alt={"Vasi Image"} /> */}
          </div>
          <div className={"Vasi_BodyData"}>
            <div className={"Vasi_BodyDataHeading"}>
              <Link to={`/view/user/${data.objectID}`}>
                <h2>{String(data.fName) + " " + data.lName}</h2>
              </Link>
            </div>
            <div className={"Vasi_BodyExtraData"}>
              <p>{data.title}</p>
            </div>
            <div className={"Vasi_BodyExtraData"}>
              <p>{data.locCity}</p>
            </div>
          </div>
        </div>
        <div className={"Vasi_Footer"}>
          <div className={"Vasi_FooterLeft"}>
            <img
              src={
                "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAMAAABhq6zVAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAb1BMVEUAAAALntALntALntALntALntALntALntALntALntALntALntALntALntALntALntALntALntALntALntALntALntALntALntALntALntALntALntALntALntALntALntALntALntALntALntAAAAAAdEQwAAAAI3RSTlMAjocK8OwHXlkFHDjOyhvV/mr9a1r6QXx6t+VY5rbHkQ4PktRPL0kAAAABYktHRACIBR1IAAAACXBIWXMAAAsSAAALEgHS3X78AAAAWklEQVQI1z3MVxaAIAxE0bEXVBSxK5bsf48eiPK+5n4kgC0I4YviJPXIiHJeRSkqolo0BSBb+molOvVD9YAeeA/aHuUMfjEyRoeJMTsstG56p8PBnBdwPwZ4AVjbCWStSpF9AAAAAElFTkSuQmCC"
              }
            />
            <span>Suggestion</span>
          </div>
          <div className={"Vasi_FooterRight"}>
            {/* Last Updated {fromNow(data.uTime)} */}
          </div>
        </div>
      </div>
    );
  }
}
