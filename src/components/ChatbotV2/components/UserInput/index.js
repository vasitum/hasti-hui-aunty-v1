import React from "react";
import Back from "../images/backarrow.png";

import "./index.scss";

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.textInput = React.createRef();
    this.state = {
      value: ""
    };
  }

  onChange = e => {
    this.setState({
      value: e.target.value
    });
  };

  componentDidUpdate(prevProps, prevState) {
    this.textInput.current.focus();
  }

  onSubmit = e => {
    if (e.preventDefault) {
      e.preventDefault();
    }

    const { value } = this.state;
    const { onSubmit } = this.props;
    if (!value) return;
    onSubmit(value);
    this.setState({
      value: ""
    });
  };

  render() {
    const { value } = this.state;
    const { disabled, hidden } = this.props;
    return (
      <div className={`UserInput ${hidden ? "is-hidden" : ""}`}>
        <form onSubmit={this.onSubmit}>
          <input
            disabled={disabled}
            placeholder={"Type your query..."}
            value={value}
            className={"UserInput__input"}
            onChange={this.onChange}
            ref={this.textInput}
          />
          <button
            disabled={disabled}
            className={"UserInput__button"}
            type={"submit"}>
            <img src={Back} alt={"Back Button"} />
          </button>
        </form>
      </div>
    );
  }
}
