import React, { Component } from "react";
import Company from "./images/wipro.png";
import Com from "./images/com.png";
import loc from "./images/loc.png";
import Jobs from "./images/jobs.svg";
import Candidates from "./images/candidates.svg";
import interview from "./images/card_interview.png";
import Recommend from "./images/card_recommendation.png";
import Screen from "./images/card_screening.png";
import { getRelativeTime } from "./Time";

class Card extends Component {
  state = {
    icon: interview
  };
  comIconDecider = () => {
    if (this.props.msg.image) {
      return this.props.msg.image;
    } else {
      if (this.props.msg.jobkey) {
        if (this.props.msg.jobkey === "jobs") {
          return Jobs;
        }
        if (this.props.msg.jobkey === "candidates") {
          return Candidates;
        }
      }
    }
  };

  iconDecider = () => {
    if (this.props.msg.process.toLowerCase() === "recommendation") {
      return Recommend;
    }
    if (this.props.msg.process.toLowerCase() === "suggestion") {
      return Recommend;
    } else if (this.props.msg.process.toLowerCase() === "screening") {
      return Screen;
    } else {
      return interview;
    }
  };
  click = (param1, param2) => {
    if (this.props.click) {
      this.props.click(param1, param2);
    }
  };
  render() {
    return (
      <div
        className="chatbot__card"
        onClick={e =>
          this.click(this.props.msg.process.toLowerCase(), this.props.msg.jobId)
        }>
        <div className="chatbot__main">
          <div className="chatbot__details">
            <div className="chatbot__icon">
              <img
                src={this.comIconDecider()}
                alt="detail-icon not available"
                style={{
                  overflow: "hidden",
                  width: "37px",
                  height: "37px",
                  borderRadius: "50%",
                  position: "absolute",
                  top: "0px"
                }}
              />
            </div>
            <div className="chatbot__other-details">
              <div className="chatbot__heading">{this.props.msg.heading}</div>
              <div className="chatbot__org-detail">
                <div className="chatbot__com">
                  {/* <span>
                    <img
                      src={Com}
                      alt="org-detail-icon not available"
                      style={{ paddingLeft: "6px" }}
                    />
                  </span> */}
                  <div className="chatbot__org-name">
                    {this.props.msg.compname
                      ? this.props.msg.compname
                      : "Not Available"}
                  </div>
                </div>
                <div className="chatbot__com">
                  {/* <span>
                    <img
                      src={loc}
                      className="chatbot__small-icon"
                      alt="loc-icon not available"
                    />
                  </span> */}
                  <div className="chatbot__org-loc">
                    {this.props.msg.location}
                  </div>
                </div>
              </div>
            </div>
          </div>
          {this.props.msg.percentage !== null ? (
            <div className="chatbot__confidence">
              <span className="chatbot__percentage">
                {this.props.msg.percentage}
              </span>
              <span className="chatbot__confidence-text">matched</span>
            </div>
          ) : (
            ""
          )}
        </div>

        <div className="chatbot__process">
          <div style={{ display: "inline" }}>
            <img
              src={this.iconDecider()}
              style={{ position: "absolute", paddingTop: "4px" }}
              alt="interview-icon not available"
            />
          </div>

          <div className="chatbot__org-process" style={{ display: "inline" }}>
            {this.props.msg.process}{" "}
          </div>
          <div className="chatbot__time" style={{ color: "#6e768a" }}>
            {getRelativeTime(this.props.msg.time)}
          </div>
        </div>
      </div>
    );
  }
}

export default Card;
