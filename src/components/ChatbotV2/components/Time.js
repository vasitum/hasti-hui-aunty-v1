import { format } from "date-fns";
var dateFormat = require("dateformat");
var moment = require("moment");
// import {} from "date-fns";

var now = new Date();
export default function getCurrentTime(dateobj) {
  return format(new Date().getTime(), "hh:mm A");
}

export function getRelativeTime(prevdate) {
  // console.log("DATE PASSED", prevdate);

  if (prevdate) {
    dateFormat.masks.mytime = "hh:MM TT";
    var daysdiff = moment(moment().format()).diff(prevdate, "days");
    if (daysdiff === 0) {
      var time = "Today " + dateFormat(prevdate, "mytime");
    } else if (daysdiff === 1) {
      var time = "Yesterday " + dateFormat(prevdate, "mytime");
    } else {
      var time = dateFormat(prevdate, "mmmm dS, yyyy hh:MM");
    }
    // var  time = moment().subtract(daysdiff, 'days').calendar()
  } else {
    prevdate = dateFormat(now, "isoFormat");

    dateFormat.masks.mytime = "hh:MM TT";
    var daysdiff = moment(moment().format()).diff(prevdate, "days");
    if (daysdiff === 0) {
      var time = "Today " + dateFormat(prevdate, "mytime");
    } else if (daysdiff === 1) {
      var time = "Yesterday " + dateFormat(prevdate, "mytime");
    } else {
      var time = dateFormat(prevdate, "mmmm dS, yyyy hh:MM");
    }
  }

  return time;
}
