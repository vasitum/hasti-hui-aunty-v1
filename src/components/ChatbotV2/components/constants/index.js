export const MSG_TYPE = {
  VASI: "vasi",
  USER_MSG: "user_msg",
  JOB: "job",
  USER: "user"
};
