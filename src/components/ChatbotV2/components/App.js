import React, { Component } from "react";
import "./App.css";
import Title from "./Title.js";
import Secondary from "./Secondary.js";
import Catagory from "./Catagory.js";
import Chat from "./Chat.js";
import axios from "axios";
import io from "socket.io-client";

import getCurrentTime from "./Time";
import getRelTime from "./Time";

import Vasi from "./Vasi";

const ip = "https://vasitum.com";
const soc_port = "5002";
const port = "5009";
const socket = io(ip + ":" + soc_port, { transports: ["websocket"] });

const VISIBILITY_STYLE_MAP = {
  // EXPAND: window.screen.availWidth <= 1024 ? "100%" : "320px",
  // MINIMIZE: window.screen.availWidth <= 1024 ? "0px" : "43px",
  // MAXIMIZE: window.screen.availWidth <= 1024 ? "100%" : "320px"
  EXPAND: window.screen.availWidth <= 1024 ? "100%" : "320px",
  MINIMIZE: window.screen.availWidth <= 1024 ? "0px" : "0px",
  MAXIMIZE: window.screen.availWidth <= 1024 ? "100%" : "320px"
};

class App extends Component {
  state = {
    ip: "https://vasitum.com",
    soc_port: "5002",
    port: "5009",
    height: "84%",
    loader: "none",
    systemId: null,
    reload: true,
    eofmessage: "chatbot__message_end",
    input: "chatbot__message-box",
    back_style: "none",
    tic: {
      tic_screening: 0,
      tic_recommendation: 0,
      tic_interview: 0,
      tic_general: 1
    },
    search_key: "@nbsdh930",
    jobIds: [],
    temp: [],
    vis: { display: "" },
    grid: true,
    login: false,
    style: {
      width: "320px"
    },
    chatBoxWindow: "showBox",
    search_counter: 0,
    current_counter: 1,
    user_data: {
      userId: "",
      appId: "",
      appliStatus: "pending",
      jobId: ""
    },
    mobile: false,
    general: [
      {
        type: "bot",
        content: [
          "Hi, welcome to Vasitum. I am Vasi",
          "Your AI-powered virtual assistant. What can I help you with today?"
        ],
        time: getCurrentTime()
      },
      {
        type: "button",
        content: ["Jobs", "Candidates"],
        id: "abcd",
        time: getCurrentTime()
      }
    ],
    screeningLoad: true,
    process: "running",
    screeningStatus: "start",
    interviewLoad: true,
    interviewStatus: "start",
    screening_status_card: [],
    interview_status_card: [],
    recommendation_status_card: [],
    buffer: [],
    screening: [],
    interview: [
      {
        type: "bot",
        content: ["Hii, I am Vino your Interview Scheduling assistant"]
      }
    ],
    messages: [],
    mode: "general",
    catagory: false,
    chat: true,
    cat_card: {
      RecommendationCard: true,
      ScreeningCard: true,
      InterviewCard: true,
      ChatCard: true
    },
    restartInterview: true,
    cat_style: {
      display: "none"
    },
    time: {
      general_time: "No chat yet",
      rec_time: "No Recommendations yet",
      screen_time: "No Screening yet",
      interview_time: "No Interview yet"
    },
    back_page: {},
    general_msgs: [],
    randomId: new Date().getTime() + "xx_xx"
  };

  vasi__messagePush = (messages, cb) => {
    this.setState(
      {
        general_msgs: [...this.state.general_msgs, ...messages]
      },
      cb
    );
  };

  backToPrevious = () => {
    const back_page = this.state.back_page;
    if (back_page.style) {
      this.setState({
        chat: back_page.chat,
        catagory: back_page.catagory,
        mode: back_page.mode,
        cat_card: back_page.cat_card,
        back_style: back_page.back_style,
        input: back_page.input,
        cat_style: back_page.cat_style,
        style: back_page.style,
        message: back_page.messages,
        grid: back_page.grid
      });
    }
  };
  recordPage = () => {
    const record = this.state;
    this.setState({
      back_page: {
        ...this.state.back_page,
        chat: record.chat,
        catagory: record.catagory,
        mode: record.mode,
        cat_card: record.cat_card,
        back_style: record.back_style,
        input: record.input,
        cat_style: record.cat_style,
        style: record.style,
        messages: record.messages,
        grid: record.grid
      }
    });
  };

  changeJobId = (param, process) => {
    this.getStatus(this.state.user_data.userId, param);
    if (process === "screening") {
      axios
        .get(
          ip +
            ":" +
            port +
            "/api/Screeningdata/" +
            this.state.user_data.userId +
            ":" +
            param +
            ":" +
            process
        )
        .then(res => {
          let data = res.data.data;
          data.map(message => {
            if (message.type === "button" || message.type === "option") {
              message.active = "Yes";
            }
          });
          this.setState({
            messages: []
          });
          if (
            data[data.length - 1].type === "button" ||
            data[data.length - 1].type === "option"
          ) {
            data[data.length - 1].active = null;
          }
          this.setState({
            screening: data,
            messages: data
          });
          let type = data.slice(0).reverse()[0].type;

          if (this.state.screeningStatus !== "completed") {
            if (type === "button" || type === "option") {
              this.setState({
                input: "chatbot__message-box-alt",
                height: "90%"
              });
            } else {
              this.enableInput();
            }
          } else {
            this.setState({
              input: "chatbot__message-box-alt",
              height: "90%"
            });
          }
        });
    }
    if (process === "interview") {
      axios
        .get(
          ip +
            ":" +
            port +
            "/api/Screeningdata/" +
            this.state.user_data.userId +
            ":" +
            param +
            ":" +
            process
        )
        .then(res => {
          let data = res.data.data;
          data.map(message => {
            if (message.type === "button" || message.type === "option") {
              message.active = "Yes";
            }
          });
          this.setState({
            messages: []
          });
          if (
            data[data.length - 1].type === "button" ||
            data[data.length - 1].type === "option"
          ) {
            data[data.length - 1].active = null;
          }
          this.setState({
            interview: data,
            messages: data
          });
          let type = data.slice(0).reverse()[0].type;
          if (this.state.interviewStatus !== "completed") {
            if (type === "button" || type === "option") {
              this.setState({
                input: "chatbot__message-box-alt",
                height: "90%"
              });
            } else {
              this.enableInput();
            }
          } else {
            this.setState({
              input: "chatbot__message-box-alt",
              height: "90%"
            });
          }
        });
      if (!this.state.restartInterview) {
        axios
          .get(
            ip +
              ":" +
              port +
              "/api/start_interview/" +
              this.state.user_data.userId +
              "/" +
              this.state.user_data.jobId
          )
          .then(res => {
            let message = res.data.multiple[0];
            message.id = this.keyGenrator();

            let messages = [...this.state.messages, message];
            console.log(message);

            this.setState({
              interview: [...this.state.interview, message],
              messages: messages
            });

            this.setState({
              loader: "none"
            });
            this.saveState();

            this.addDummy();
          });
      }
    }
  };

  changeBackStyle = param => {
    // alert("back changed");
    this.setState({
      back_style: param
    });
  };
  getRelativeTime = prevdate => {
    var dateFormat = require("dateformat");
    var moment = require("moment");
    if (prevdate) {
      dateFormat.masks.mytime = "hh:MM TT";
      var daysdiff = moment(moment().format()).diff(prevdate, "days");
      if (daysdiff === 0) {
        var time = "Today " + dateFormat(prevdate, "mytime");
      } else if (daysdiff === 1) {
        var time = "Yesterday " + dateFormat(prevdate, "mytime");
      } else {
        var time = dateFormat(prevdate, "mmmm dS, yyyy hh:MM");
      }
      // var  time = moment().subtract(daysdiff, 'days').calendar()
    }

    return time;
  };
  resetkey = () => {
    this.setState({
      search_key: "$owjd0"
    });
  };
  addTicker = param => {
    if (param === "screening") {
      this.setState({
        tic: {
          ...this.state.tic,
          tic_screening: this.state.tic.tic_screening + 1
        }
      });
    } else if (param === "interview") {
      this.setState({
        tic: {
          ...this.state.tic,
          tic_interview: this.state.tic.tic_interview + 1
        }
      });
    } else if (param === "general") {
      this.setState({
        tic: {
          ...this.state.tic,
          tic_general: this.state.tic.tic_general + 1
        }
      });
    } else {
      this.setState({
        tic: {
          ...this.state.tic,
          tic_recommendation: this.state.tic.tic_recommendation + 1
        }
      });
    }
  };

  subtaractTicker = param => {
    if (param === "screening") {
      if (this.state.tic.tic_screening !== 0) {
        this.setState({
          tic: {
            ...this.state.tic,
            tic_screening: this.state.tic.tic_screening - 1
          }
        });
      }
    } else if (param === "interview") {
      if (this.state.tic.tic_interview !== 0) {
        this.setState({
          tic: {
            ...this.state.tic,
            tic_interview: this.state.tic.tic_interview - 1
          }
        });
      }
    } else if (param === "general") {
      if (this.state.tic.tic_general !== 0) {
        this.setState({
          tic: {
            ...this.state.tic,
            tic_general: this.state.tic.tic_general - 1
          }
        });
      }
    } else {
      if (this.state.tic.tic_recommendation !== 0) {
        this.setState({
          tic: {
            ...this.state.tic,
            tic_recommendation: this.state.tic.tic_recommendation - 1
          }
        });
      }
    }
    this.saveState();
  };

  setSearch = param => {
    this.setState({
      search: false
    });
  };

  setButton = param => {
    this.setState({
      button: false
    });
  };
  setEof = () => {
    this.setState({
      eofmessage: "chatbot__message_end"
    });
  };
  resetEof = () => {
    this.setState({
      eofmessage: ""
    });
  };

  addDummy = () => {
    // let message = { id: '1ml0kdls7', type: 'test', content: ['test'] };
    // let messages = [...this.state.messages, message];
    // this.setState({
    // 	messages: messages,
    // });
  };
  removeDummy = () => {
    let left = this.state.messages;

    for (var i = left.length; i--; ) {
      if (left[i].type === "test") left.splice(i, 1);
    }

    this.setState({
      messages: left
    });
  };

  incCounter = () => {
    if (this.state.current_counter < this.state.search_counter) {
      this.setState({
        current_counter: this.state.current_counter + 1
      });
    } else {
      this.setState({
        current_counter: 1
      });
    }
  };
  decCounter = () => {
    if (this.state.current_counter > 2) {
      this.setState({
        current_counter: this.state.current_counter - 1
      });
    } else {
      this.setState({
        current_counter: this.state.search_counter
      });
    }
  };

  searchResult = param => {
    this.setState({
      search_key: param
    });

    // console.log("search is happening");

    this.removeDummy();

    var count = 0;
    var messages = this.state.messages;
    for (var i = 0; i < messages.length; i++) {
      for (var j = 0; j < messages[i].content.length; j++) {
        //console.log( messages[i].content[j].includes(param));
        if (messages[i].content[j].includes(param)) {
          count++;
        }
      }
    }
    this.setState({
      search_counter: count
    });

    let newcount = 0;
    for (var i = 0; i < messages.length; i++) {
      for (var j = 0; j < messages[i].content.length; j++) {
        //console.log( messages[i].content[j].includes(param));
        if (messages[i].content[j].includes(param)) {
          newcount++;
          if (this.state.current_counter === newcount) {
            let left = messages.slice(0, i);
            let right = messages.slice(i, this.state.messages.length);
            left.push({
              id: "hod2n39sld8ays6s9",
              content: ["test"],
              type: "test"
            });
            left.push.apply(left, right);
            this.setState({
              messages: left
            });
            // console.log(left);
            break;
          }
        }
      }
    }

    // console.log("searches found ", count);
  };

  enableInput = () => {
    this.setState({
      input: "chatbot__message-box",
      height: "84%"
    });
    this.saveState();
  };
  disableInput = () => {
    // this.setState({
    //   input: "chatbot__message-box-alt",
    //   height: "90%"
    // });
    // this.saveState();
  };

  getState = () => {
    // alert("getState Called");

    this.initialProcess();
  };

  checkRoom = () => {
    axios
      .get(
        ip +
          ":" +
          port +
          "/api/AssignInstance/" +
          this.state.user_data.userId +
          ":" +
          this.state.user_data.jobId
      )
      .then(res => {
        console.log(res.data);
      });
  };

  saveStatus = () => {
    axios
      .post(
        ip + ":" + port + "/api/SaveStatus",
        {
          data: {
            screeningLoad: this.state.screeningLoad,
            interviewLoad: this.state.interviewLoad,
            screeningStatus: this.state.screeningStatus,
            interviewStatus: this.state.interviewStatus,
            reload: this.state.reload,
            userId: this.state.user_data.userId,
            jobId: this.state.user_data.jobId,
            restartInterview: this.state.restartInterview
          }
        },
        {
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
      .then(res => {
        //no action
      });
  };

  getStatus = (userId, jobId) => {
    axios
      .post(
        ip + ":" + port + "/api/GetStatus",
        {
          data: {
            userId: userId,
            jobId: jobId
          }
        },
        {
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
      .then(res => {
        let data = res.data;
        this.setState({
          screeningLoad: data.screeningLoad,
          interviewLoad: data.interviewLoad,
          screeningStatus: data.screeningStatus,
          interviewStatus: data.interviewStatus,
          reload: data.reload,
          restartInterview: data.restartInterview,
          user_data: {
            ...this.state.user_data,
            userId: data.userId,
            jobId: data.jobId
          }
        });
      });
  };

  saveStateNetwork = state => {
    axios
      .post(
        ip + ":" + port + "/api/SaveState",
        {
          data: {
            ...state,
            general_msgs: [],
            randomId: ""
          }
        },
        {
          headers: {
            "Content-Type": "application/json"
            //            'Content-Type': 'multipart/form-data'
            //  'Content-Type': 'application/x-www-form-urlencoded'
          }
        }
      )
      .then(res => {
        // console.log(res.data.data);
        console.log("data saved sucessfully");
      })
      .catch(err => {
        console.log("Post Data has error", err);
      });
  };

  saveState = () => {
    this.saveStateNetwork(this.state);
  };

  // saveState = () => {
  //   let found = false;
  //   let jobs = this.state;
  //   if (this.state.mode === "screening" || this.state.mode === "interview") {
  //     jobs.jobIds.map(jobdata => {
  //       //console.log(jobdata);

  //       if (
  //         jobdata.jobId === this.state.user_data.jobId &&
  //         this.state.user_data.jobId
  //       ) {
  //         console.log("matched");
  //         found = true;
  //         jobdata["screeningStatus"] = this.state.screeningStatus;
  //         jobdata["interviewStatus"] = this.state.interviewStatus;
  //         jobdata["screening"] = this.state.screening;
  //         jobdata["screeningLoad"] = this.state.screeningLoad;
  //         jobdata["screening_status_card"] = this.state.screening_status_card;
  //         jobdata["interview"] = this.state.interview;
  //         jobdata["interviewLoad"] = this.state.interviewLoad;
  //         jobdata["interview_status_card"] = this.state.interview_status_card;
  //         jobdata["process"] = this.state.process;
  //         jobdata["reload"] = this.state.reload;
  //       }
  //     });

  //     if (found === true) {
  //       this.setState({
  //         jobIds: jobs.jobIds
  //       });
  //     } else {
  //       console.log(" not matched");
  //       if (this.state.user_data.jobId) {
  //         let job = {
  //           jobId: this.state.user_data.jobId,
  //           screeningStatus: this.state.screeningStatus,
  //           interviewStatus: this.state.interviewStatus,
  //           screening: this.state.screening,
  //           screeningLoad: this.state.screeningLoad,
  //           screening_status_card: this.state.screening_status_card,
  //           interview: this.state.interview,
  //           interviewLoad: this.state.interviewLoad,
  //           interview_status_card: this.state.interview_status_card,
  //           process: this.state.process,
  //           reload: this.state.reload
  //         };
  //         jobs.jobIds.push(job);
  //         console.log(job);
  //         this.setState({
  //           jobIds: jobs.jobIds
  //         });
  //       }
  //     }
  //   }

  //   axios
  //     .post(
  //       ip+":"+port+"/api/SaveState",
  //       {
  //         data: this.state
  //       },
  //       {
  //         headers: {
  //           "Content-Type": "application/json"
  //           //            'Content-Type': 'multipart/form-data'
  //           //	'Content-Type': 'application/x-www-form-urlencoded'
  //         }
  //       }
  //     )
  //     .then(res => {
  //       console.log(res.data.data);
  //       console.log("data saved sucessfully");
  //     })
  //     .catch(err => {
  //       console.log("Post Data has error", err);
  //     });
  // };

  changeToScreenCard = param => {
    // this.subtaractTicker("general");
    this.setState({
      back_style: "block",
      chat: false,
      catagory: true,
      mode: "screening",
      grid: false,
      cat_card: {
        RecommendationCard: false,
        ScreeningCard: false,
        InterviewCard: false
      }
    });
    this.saveState();
  };
  changeToInterviewCard = param => {
    // this.subtaractTicker("interview");
    this.setState({
      back_style: "block",
      chat: false,
      catagory: true,
      mode: "interview",
      grid: false,
      cat_card: {
        RecommendationCard: false,
        ScreeningCard: false,
        InterviewCard: false
      }
    });
    this.saveState();
  };
  changeToRecommendCard = param => {
    this.subtaractTicker();

    this.setState({
      back_style: "block",
      chat: false,
      catagory: true,
      mode: "Recommendation",
      grid: false,
      cat_card: {
        RecommendationCard: false,
        ScreeningCard: false,
        InterviewCard: false
      }
    });
    this.saveState();
  };
  changeMode = mode => {
    this.setState({
      mode: mode
    });
  };

  changeGrid = () => {
    this.setState({
      grid: !this.state.grid
    });
    this.setState({
      mode: "category"
    });
  };
  reset = () => {
    this.setState({
      general: []
    });
  };
  keyGenrator = () => {
    const uuidv4 = require("uuid/v4");
    return uuidv4();
  };

  changeCatStatus = (param, param2) => {
    this.setState({
      loader: "none"
    });
    if (param === "Recommendation") {
      if (this.props.actions) {
        this.props.actions.onExpand();
        this.setState({
          vis: { display: "" }
        });
      }
      this.setState({
        cat_style: {
          display: "none"
        },
        cat_card: {
          RecommendationCard: true,
          ScreeningCard: false,
          InterviewCard: false,
          ChatCard: false
        },
        mode: "Recommendation",
        catagory: true
      });
    } else if (param === "screening") {
      if (this.props.actions) {
        this.props.actions.onExpand();
        this.setState({
          vis: { display: "" }
        });
      }
      this.setState({
        cat_style: {
          display: "none"
        },
        cat_card: {
          RecommendationCard: false,
          ScreeningCard: true,
          InterviewCard: false,
          ChatCard: false
        },
        mode: "screening",
        messages: this.state.screening,
        catagory: true,
        grid: false
      });
    } else if (param === "interview") {
      if (this.props.actions) {
        this.props.actions.onExpand();
        this.setState({
          vis: { display: "" }
        });
      }
      this.setState({
        cat_style: {
          display: "none"
        },
        cat_card: {
          RecommendationCard: false,
          ScreeningCard: false,
          InterviewCard: true,
          ChatCard: false
        },
        mode: "interview",
        messages: this.state.interview,
        catagory: true,
        grid: false
      });
    } else {
      this.recordPage();
      this.setState({
        cat_style: {
          display: ""
        },
        cat_card: {
          RecommendationCard: true,
          ScreeningCard: true,
          InterviewCard: true,
          ChatCard: true
        },
        catagory: true,
        messages: this.state.general,
        chat: false,
        back_style: "block"
      });
    }
  };

  changeChatStatus = param => {
    this.setState({
      loader: "none"
    });
    if (param === "cat") {
      if (this.state.mode === "general") {
        if (this.state.grid) {
          this.setState({
            chat: true,
            catagory: false,
            grid: false
          });
        } else {
          if (this.props.actions) {
            this.props.actions.onExpand();
            this.setState({
              vis: { display: "" }
            });
          }
          this.setState({
            chat: false,
            catagory: true,
            grid: true,
            cat_style: {
              display: ""
            },
            mode: "category"
          });
        }
      } else if (this.state.mode === "screening") {
        // this.setState({
        //   catagory: false,
        //   chat: false
        // });
        if (this.state.catagory) {
          this.setState({
            cat_style: {
              display: ""
            },
            cat_card: {
              RecommendationCard: true,
              ScreeningCard: true,
              InterviewCard: true,
              ChatCard: true
            },

            catagory: true,
            chat: false,

            mode: "category",

            back_style: "block",
            grid: true
          });
          if (this.props.actions) {
            this.props.actions.onExpand();
          }
        }
        if (this.state.chat) {
          if (this.props.actions) {
            this.props.actions.onExpand();
            this.setState({
              vis: { display: "" }
            });
          }
          this.setState({
            cat_style: {
              display: "none"
            },
            cat_card: {
              RecommendationCard: false,
              ScreeningCard: true,
              InterviewCard: false,
              ChatCard: false
            },
            mode: "screening",
            catagory: true,
            chat: false,
            grid: false
          });
        }
      } else if (this.state.mode === "interview") {
        if (this.state.catagory) {
          this.setState({
            cat_style: {
              display: ""
            },
            cat_card: {
              RecommendationCard: true,
              ScreeningCard: true,
              InterviewCard: true,
              ChatCard: true
            },
            catagory: true,
            mode: "category",
            chat: false,
            back_style: "block",
            grid: true
          });
          if (this.props.actions) {
            this.props.actions.onExpand();
          }
        }
        if (this.state.chat) {
          if (this.props.actions) {
            this.props.actions.onExpand();
            this.setState({
              vis: { display: "" }
            });
          }
          this.setState({
            cat_style: {
              display: "none"
            },
            cat_card: {
              RecommendationCard: false,
              ScreeningCard: true,
              InterviewCard: false,
              ChatCard: false
            },
            mode: "interview",
            catagory: true,
            chat: false,
            grid: false
          });
        }

        // this.setState({
        //   catagory: false,
        //   chat: false
        // });
        // if (this.props.actions) {
        //   this.props.actions.onMinimize();
        // }
      } else if (this.state.mode === "Recommendation") {
        this.setState({
          cat_style: {
            display: ""
          },
          cat_card: {
            RecommendationCard: true,
            ScreeningCard: true,
            InterviewCard: true,
            ChatCard: true
          },
          catagory: true,
          mode: "category",
          chat: false,
          back_style: "block",
          grid: true
        });
        if (this.props.actions) {
          this.props.actions.onExpand();
        }
      } else {
        this.backToPrevious();
      }
      // else {
      //   this.setState({
      //     catagory: false
      //   });
      // }
      // if (window.screen.availWidth < 1024) {
      // }
      if (this.props.actions) {
        this.props.actions.onMaximize();
      }
    } else if (param === "close") {
      // if (window.screen.availWidth < 1024) {
      // } else {
      //   this.setState({
      //     catagory: false,
      //     chat: false
      //   });
      //   if (this.props.actions) {
      //     this.props.actions.onMinimize();
      //   }
      // }
      if (this.props.actions) {
        this.props.actions.onMinimize();
      }
      this.setState({
        vis: { display: "none" }
      });
    } else if (param === "expand") {
      if (this.state.style.width === "100%") {
        /*this.setState({
          style: {
            width: "320px"
          }
        }); */
        //console.log('needed to expand here');
        if (this.props.actions) {
          this.props.actions.onExpand();
          this.setState({
            vis: { display: "" }
          });
        }
      } else {
        // this.setState({
        //   style: {
        //     width: "100%"
        //   }
        // });
        //console.log('needed to maximize here');

        if (this.props.actions) {
          this.props.actions.onMaximize();
          this.setState({
            vis: { display: "" }
          });
        }
      }
    } else {
      if (this.props.actions) {
        this.props.actions.onExpand();
        this.setState({
          vis: { display: "" }
        });
      }

      this.setState({
        chat: !this.state.chat,
        mode: "general",
        back_style: "none"
      });

      this.showGeneral();
    }
  };

  checkAndUpdateVisibility = (visibility, currentStyle) => {
    //	console.log('---------------------------', visibility);

    if (currentStyle === VISIBILITY_STYLE_MAP[visibility]) {
      //	console.log('currentStyle  is returned', VISIBILITY_STYLE_MAP[visibility], currentStyle);
      return;
    }
    //console.log('checkAndUpdateVisibility is called');

    this.setState(
      {
        style: {
          width: VISIBILITY_STYLE_MAP[visibility]
        }
      },
      function() {
        console.log("state changed");
      }
    );
  };

  restartProcess = () => {
    // alert("I'm Called")
    this.saveStatus();

    axios
      .get(
        `${ip}:${port}/api/DeleteData/${this.state.user_data.userId}:${
          this.state.user_data.jobId
        }:screening`
      )
      .then(res => {
        this.setState(
          {
            messages: [],
            reload: true,
            screeningLoad: true,
            process: "running",
            screeningStatus: "start",
            interviewLoad: true,
            interviewStatus: "start",
            restartInterview: true,
            interview: [
              {
                type: "bot",
                content: ["Hii, I am Vino your Interview Scheduling assistant"]
              }
            ],
            screening: []
          },
          () => {
            this.initialProcess(true);
          }
        );
      })
      .catch(err => console.error(err));

    // api call
    // axios.get(
    //   `${ip}:${port}/api/send_initial_screening_message/${
    //     this.state.user_data.jobId
    //   }:${this.state.user_data.userId}:first`
    // ).then(res => {
    //   const messages = res.data;
    //   this.setState({
    //     messages: [],
    //     screeningLoad: true,
    //     process: "running",
    //     screeningStatus: "start",
    //     interviewLoad: true,
    //     interviewStatus: "start",
    //     restartInterview: true,
    //     interview: [
    //       {
    //         type: "bot",
    //         content: ["Hii, I am Vino your Interview Scheduling assistant"]
    //       }
    //     ],
    //     screening: [],

    //   }, () => {
    //     this.addApiMessages(messages);
    //   })
    // });
  };

  initialProcess = (reload = false) => {
    // alert("initial process called");
    this.saveStatus();
    if (
      this.state.screeningLoad === true ||
      this.state.screeningStatus === "start"
    ) {
      this.setState({
        mode: "screening",
        chat: true,
        catagory: false,
        grid: false
      });

      axios
        .get(
          ip +
            ":" +
            port +
            "/api/data/" +
            this.state.user_data.userId +
            ":" +
            this.state.user_data.jobId +
            ":" +
            this.state.user_data.appId
        )
        .then(res => {
          let cards = res.data;
          let time = "";
          cards.map(data => {
            time = data.time;
          });

          if (!reload) {
            this.setState({
              screening_status_card: [
                ...this.state.screening_status_card,
                cards[0]
              ]
            });
          }

          this.addTicker("screening");
          var dateFormat = require("dateformat");
          var now = new Date();
          this.setState({
            time: {
              ...this.state.time,
              screen_time: this.getRelativeTime(dateFormat(now, "isoDateTime"))
            }
          });
          this.saveState();

          // console.log("first api called", cards);

          axios
            .get(
              ip +
                ":" +
                port +
                "/api/send_initial_screening_message/" +
                this.state.user_data.jobId +
                "/" +
                this.state.user_data.userId +
                ":" +
                "first"
            )
            .then(res => {
              let messages = res.data;
              this.setState({
                messages: []
              });
              this.addApiMessages(messages);
              console.log("second api calls", messages);
              this.setState({
                screeningStatus: "running"
              });
              this.saveStatus();
              this.saveState();

              console.log(this.state.screeningLoad, this.state.reload);
              if (this.state.reload === true) {
                if (this.state.screeningLoad === true) {
                  console.log("third api calls");
                  axios
                    .get(
                      ip +
                        ":" +
                        port +
                        "/api/send_message/" +
                        this.state.user_data.userId +
                        "/" +
                        this.state.user_data.jobId +
                        "/yes"
                    )
                    .then(res => {
                      let messages = res.data;
                      this.addApiMessages(messages);
                      this.setState({
                        reload: false
                      });
                    });
                  this.saveStatus();

                  this.setState({
                    reload: false
                  });
                  this.saveState();
                  this.saveStatus();
                }
              }
              this.setState({
                chat: true,
                mode: "screening"
              });

              this.setState({
                screeningLoad: true,
                interviewLoad: true,
                grid: false
              });
              this.saveStatus();
            });
        });
    }
  };

  checkAndUpdateApplication = application => {
    if (!application) {
      console.log("application not found");
      return;
    }
    const { jobId, applicationId, userId } = application;
    const { user_data } = this.state;

    //	console.log('Application ad user data ----', user_data, jobId, applicationId, userId);
    if (!jobId || !applicationId || !userId) {
      return;
    }

    if (user_data.appId === applicationId) {
      return;
    }

    this.setState(
      {
        user_data: {
          ...user_data,
          userId: userId,
          appId: applicationId,
          jobId: jobId
        },
        screeningLoad: true,
        interviewLoad: true,
        screeningStatus: "start",
        interviewStatus: "start",
        screening: [],
        interview: [],
        reload: true
      },
      () => {
        this.saveStatus();
        this.getState();
        // if (window.screen.availWidth < 1024) {
        // } else {
        //   if (this.props.actions) {
        //     this.props.actions.onExpand();
        //     this.setState({
        //       vis: { display: "" }
        //     });
        //   }
        // }
        if (this.props.actions) {
          this.props.actions.onMaximize();
        }
        console.log(
          "-----------------------------get state finished---------------"
        );
      }
    );
  };

  reloadState = () => {
    axios
      .get(
        ip +
          ":" +
          port +
          "/api/GetState/" +
          this.state.user_data.userId +
          ":" +
          this.state.user_data.jobId
      )
      .then(res => {
        let altstate = res.data;

        if (!altstate.data) {
          // alert("NO DATA FOUND RELOAD STATE");

          // if (window.screen.availWidth > 1024) {
          //   this.setState({
          //     ...this.state,
          //     ...altstate,
          //     chat: false,
          //     catagory: false,
          //     mode: "general",
          //     input: "chatbot__message-box"
          //     // tic: {
          //     //   ...this.state.tic,
          //     //   tic_general: 1
          //     // }
          //   });
          // } else {

          // }
          this.setState({
            ...this.state,
            ...altstate
            // Changed shit here!! Ref: Show general in login
            // chat: false,
            // catagory: true,
            // cat_style: {
            //   display: ""
            // },
            // cat_card: {
            //   RecommendationCard: true,
            //   ScreeningCard: true,
            //   InterviewCard: true,
            //   ChatCard: true
            // },
            // grid: true,

            // mode: "category"
          });
        } else {
          // if (window.screen.availWidth > 1024) {
          //   var dateFormat = require("dateformat");
          //   var now = new Date();
          //   let time = {
          //     general_time: "No chat yet",
          //     rec_time: "No Recommendation yet",
          //     screen_time: "No Screening yet",
          //     interview_time: "No Interview yet"
          //   };
          //   if (altstate.screening_status_card.length) {
          //     time.screen_time = this.getRelativeTime(
          //       altstate.screening_status_card[0].time
          //     );
          //   }
          //   if (altstate.interview_status_card.length) {
          //     time.interview_time = this.getRelativeTime(
          //       altstate.interview_status_card[0].time
          //     );
          //   }
          //   if (altstate.recommendation_status_card.length) {
          //     time.rec_time = this.getRelativeTime(
          //       altstate.recommendation_status_card[0].time
          //     );
          //   }

          //   this.setState({
          //     ...this.state,
          //     ...altstate,
          //     chat: false,
          //     catagory: false,
          //     mode: "general",
          //     input: "chatbot__message-box",
          //     // tic: {
          //     //   ...this.state.tic,
          //     //   tic_general: 1
          //     // },
          //     time: time
          //   });
          // } else {

          // }
          var dateFormat = require("dateformat");
          var now = new Date();
          let time = {
            general_time: "No chat yet",
            rec_time: "No Recommendation yet",
            screen_time: "No Screening yet",
            interview_time: "No Interview yet"
          };
          if (altstate.screening_status_card.length) {
            time.screen_time = this.getRelativeTime(
              altstate.screening_status_card[0].time
            );
          }
          if (altstate.interview_status_card.length) {
            time.interview_time = this.getRelativeTime(
              altstate.interview_status_card[0].time
            );
          }
          if (altstate.recommendation_status_card.length) {
            time.rec_time = this.getRelativeTime(
              altstate.recommendation_status_card[0].time
            );
          }

          this.setState({
            screeningLoad: altstate.screeningLoad,
            screening: altstate.screening,
            screeningStatus: altstate.screeningStatus,
            interview: altstate.interview,
            interviewLoad: altstate.interviewLoad,
            interviewStatus: altstate.interview_messages,
            reload: altstate.reload,
            process: altstate.process,
            screening_status_card: altstate.screening_status_card,
            interview_status_card: altstate.interview_status_card,
            recommendation_status_card: altstate.recommendation_status_card,
            general: altstate.general,
            input: "chatbot__message-box",
            grid: true,
            chat: false,
            catagory: true,
            cat_style: {
              display: ""
            },
            cat_card: {
              RecommendationCard: true,
              ScreeningCard: true,
              InterviewCard: true,
              ChatCard: true
            },
            // tic: {
            //   ...this.state.tic,
            //   tic_general: 1
            // },
            mode: "category",
            jobIds: altstate.jobIds,
            time: {
              ...time,
              general_time: altstate.time.general_time
            }
          });
        }
      })
      .catch(err => {
        console.log("ERROR CAUGHT IN RELOAD STATE  API");
      });
  };

  reloadData = () => {
    axios
      .get(
        ip +
          ":" +
          port +
          "/api/GetState/" +
          this.state.user_data.userId +
          ":" +
          this.state.user_data.jobId
      )
      .then(res => {
        let altstate = res.data;
        if (altstate.data) {
          alert("NO DATA FOUND");
        } else {
          // if (window.screen.availWidth > 1024) {

          // } else {
          //   this.setState({
          //     ...this.state,
          //     ...this.altstate
          //   });
          // }
          this.setState({
            ...this.state,
            ...this.altstate
          });
        }
      })
      .catch(err => {
        console.log("ERROR CAUGHT IN RELOAD  DATA  API");
      });
  };
  componentWillUnmount() {
    this.props.saveState();
    console.log("catagory unmounted");
    axios
      .get(
        ip +
          ":" +
          port +
          "/api/SaveInstance/" +
          this.props.userdata.userId +
          ":" +
          this.props.userdata.jobId
      )
      .then(res => {
        console.log("updated server state");
      });
  }

  componentWillMount() {
    // if (window.screen.availWidth < 1024) {
    //   // this.setState({
    //   //   mobile:true,
    //   //   catagory:true,
    //   //   chat:false,
    //   //   cat_card: {
    //   //     ChatCard:true,
    //   //     RecommendationCard: true,
    //   //     ScreeningCard: true,
    //   //     InterviewCard: true
    //   //   },
    //   //   grid:true,
    //   //   mode:"catagory"

    //   //  })

    // }
    this.setState({
      cat_style: {
        display: ""
      },
      cat_card: {
        RecommendationCard: true,
        ScreeningCard: true,
        InterviewCard: true,
        ChatCard: true
      },
      catagory: true,
      // messages: this.state.general,,
      grid: false,
      chat: false,
      mode: "category"
    });
  }
  componentDidUnmount() {
    console.log("category unmounted");
    this.setState({
      process: "aborted"
    });
  }
  componentDidUpdate() {
    console.log("component update called");
    const { cb } = this.props;
    const { style } = this.state;
    const { user } = this.props;

    //		console.log('Component Did Update Called', cb, style);

    if (!cb) {
      //		console.log('cb object is returned ');
      return;
    }
    if (user.id && !this.state.user_data.userId) {
      this.setState(
        {
          login: true,
          user_data: {
            ...this.state.user_data,
            userId: user.id
          }
        },
        () => {
          this.reloadState();
          //console.log(this.state);

          //this.getState(false);
        }
      );
    }
    if (!user.id && this.state.login) {
      this.setState({
        login: false
      });
    }

    this.checkAndUpdateVisibility(cb.visibility, style.width);
    this.checkAndUpdateApplication(cb.applicationData);
    // if (window.screen.availWidth <= 1024) {

    // }
    if (cb.visibility === "MINIMIZE") {
      if (this.state.vis.display === "none") {
        return;
      }

      this.setState({
        vis: {
          display: "none"
        }
      });
      return;
    }
    if (cb.visibility === "MAXIMIZE") {
      if (this.state.vis.display === "") {
        return;
      }
      this.setState({
        vis: {
          display: ""
        }
      });
      return;
    }

    if (cb.visibility === "EXPAND") {
      if (this.state.vis.display === "") {
        return;
      }
      this.setState({
        vis: {
          display: ""
        }
      });
      return;
    }
  }

  componentDidMount() {
    const { cb, user } = this.props;

    // if (cb) {
    // 	this.setState({
    // 		systemId: cb.systemId,
    // 	});
    // }

    if (user.id) {
      this.setState(
        {
          login: true,
          user_data: {
            ...this.state.user_data,
            userId: user.id
          }
        },
        () => {
          //this.getState(false);
          console.log("Component Did Mount Get State Called");
        }
      );
    } else {
      this.setState({
        login: false
      });
    }

    console.log("component mount called");

    socket.on("connect", function(data) {
      // console.log(data);
    });

    socket.on(
      "Recommendation",
      function(message) {
        var dateFormat = require("dateformat");
        var now = new Date();
        console.log(dateFormat(now, "isoDateTime"));

        this.setState({
          time: {
            ...this.state.time,
            rec_time: this.getRelativeTime(dateFormat(now, "isoDateTime"))
          }
        });
        message = JSON.parse(message);
        let userId = message.userId;
        message = message.data;
        if (this.state.user_data.userId === userId) {
          let data = [];
          if (Array.isArray(message)) {
            data = this.state.recommendation_status_card;
            message.map(one => {
              data.push(one);
              this.addTicker("Recommendation");
            });
            this.setState({
              recommendation_status_card: data
            });
          } else {
            message = JSON.parse(message);
            data = [...this.state.recommendation_status_card, message];
            this.addTicker("Recommendation");
          }

          //	console.log(data);

          this.setState({
            recommendation_status_card: data
          });

          this.saveState();
        }
        //	console.log(message);
      }.bind(this)
    );

    socket.on(
      "Engagement",
      function(message) {
        message = JSON.parse(message);
        console.log(message);
        let userId = message.userId;
        message = message.data;

        //console.log(Array.isArray(message),message)
        if (this.state.user_data.userId === userId) {
          if (Array.isArray(message)) {
            let data = this.state.general;
            message.map(one => {
              data.push(one);
              this.addTicker("general");
            });
            if (this.state.mode === "general" && this.state.grid === false) {
              this.setState({
                general: data,
                messages: data
              });
            } else {
              this.setState({
                general: data
              });
            }
          } else {
            let data = [...this.state.general, message];
            if (this.state.mode === "general" && this.state.grid === false) {
              this.setState({
                general: data,
                messages: data
              });
            } else {
              this.setState({
                general: data
              });
            }
          }

          this.saveState();
        }
      }.bind(this)
    );
    this.saveState();
    console.log(
      "window width ------------------------" + window.screen.availWidth
    );
    // if (window.screen.availWidth < 1024) {
    //   // this.setState({
    //   //   mobile:true,
    //   //   catagory:true,
    //   //   chat:false,
    //   //   cat_card: {
    //   //     ChatCard:true,
    //   //     RecommendationCard: true,
    //   //     ScreeningCard: true,
    //   //     InterviewCard: true
    //   //   },
    //   //   grid:true,
    //   //   mode:"catagory"

    //   //  })
    // }
    if (this.state.user_data.user_id) {
      this.setState({
        // You can change to inital chat here
        cat_style: {
          display: ""
        },
        cat_card: {
          RecommendationCard: true,
          ScreeningCard: true,
          InterviewCard: true,
          ChatCard: true
        },
        catagory: true,
        messages: this.state.general,
        chat: false,
        mode: "catagory",
        grid: true
      });
    } else {
      this.setState({
        cat_style: {
          display: ""
        },
        cat_card: {
          RecommendationCard: true,
          ScreeningCard: true,
          InterviewCard: true,
          ChatCard: true
        },
        catagory: false,
        messages: this.state.general,
        chat: true,
        mode: "general"
      });
    }

    if (cb) {
      this.setState({
        style: {
          width: VISIBILITY_STYLE_MAP[cb.visibility]
        }
      });
    }
  }
  startScreeening = jobId => {};
  startInterview = jobId => {};

  startRecommendation = () => {
    this.setState({
      grid: false
    });
  };

  addApiMessages = message => {
    this.checkRoom();
    this.setState({
      loader: "none"
    });

    message.id = this.keyGenrator();
    console.log("adding api messages", message.type);
    if (
      message.type === "button" ||
      message.type === "option" ||
      message.restartInterview === false
    ) {
      this.setState({
        input: "chatbot__message-box-alt",
        height: "90%"
      });
      this.addDummy();
    } else {
      this.enableInput();
    }
    if (this.state.mode === "screening") {
      if (
        this.state.screeningLoad === false &&
        this.state.interviewLoad === true
      ) {
        let interview_messages = [...this.state.interview, message];
        let messages = [...this.state.messages, message];

        this.setState({
          interview: interview_messages,

          messages: messages
        });
        this.addDummy();
        this.saveState();
      } else {
        let messages = [...this.state.screening, message];
        this.setState({
          screening: messages,
          messages: messages
        });
        this.addDummy();
        this.saveState();
      }
    } else if (this.state.mode === "interview") {
      let messages = [...this.state.interview, message];
      this.setState({
        interview: messages,
        messages: messages
      });
      this.addDummy();
      this.saveState();
    } else if (this.state.mode === "general") {
      if (Array.isArray(message)) {
        let messages = this.state.messages;
        message.map(one => {
          messages.push(one);
        });
        this.setState({
          general: messages,
          messages: messages
        });
        this.addDummy();
        this.saveState();
      } else {
        if (message.mode === "screening") {
          let messages = [...this.state.screening, message];
          this.setState({
            screening: messages,
            messages: messages
          });
          this.addDummy();
          this.saveState();
        } else if (message.mode === "interview") {
          let messages = [...this.state.interview, message];
          this.setState({
            interview: messages,
            messages: messages
          });
          this.addDummy();
          this.saveState();
        } else {
          let messages = [...this.state.general, message];
          this.setState({
            general: messages,
            messages: messages
          });
          this.addDummy();
          this.saveState();
        }
      }
    }
  };
  addParams = (param, param2) => {
    let messages = this.state.messages;
    messages.map(message => {
      // console.log(message.id === param2);
      if (message.id === param2) {
        message.active = param;
      }
    });
    this.setState({
      messages: messages
    });
    this.saveState();
    // console.log("---add param", messages, param2);
    let message = {};
    message.content = [param];
    this.addMessages(message);
    if (this.state.mode === "screening") {
      /* axios.get('http://192.168.0.4:5006/api/send_message/5b58c436ec17c509a498f43b/'+param)
        .then(res => {

          let message = res.data;
          this.addApiMessages(message);
        })*/
    }
  };
  addProcessing = () => {
    let process = { id: "nsbdbnsi2", type: "processing", content: [] };
    this.setState({
      messages: [...this.state.messages, process]
    });
  };
  removeProcessing = () => {
    // let messages = this.state.messages;
    // messages
  };
  addMessages = (message, dir) => {
    this.checkRoom();
    this.setState({
      loader: "block"
    });
    console.log("adding messages");
    // if (message.type === 'button' || message.type === 'option' ||  message.type === 'Card1' || message.type === 'Card2') {
    // 	this.disableInput();
    // 	this.addDummy();
    // } else {
    // 	this.enableInput();
    // }

    // setTimeout(function(){

    // }.bind(this),2000)
    // let 	process = {id:"nsbdbnsi2",type:"processing",content:[]}
    // this.setState({
    // 	messages:[...this.state.messages,process]
    // }) ;
    if (message.type === "test") {
      message.id = this.keyGenrator() + message.type;
    } else {
      message.id = this.keyGenrator() + message.type;
      message.type = "user";
      message.time = getCurrentTime();
    }

    /*let messages = [...this.state.messages,message];
    this.setState({
      messages
    })*/

    if (this.state.mode === "screening") {
      if (
        this.state.screeningLoad === false &&
        this.state.interviewLoad === true
      ) {
        let interview_messages = [...this.state.interview, message];
        let messages = [...this.state.messages, message];
        this.setState({
          interview: interview_messages,
          messages: messages
        });
        this.saveState();
      } else {
        let messages = [...this.state.screening, message];
        this.setState({
          screening: messages,
          messages: messages
        });
        this.saveState();
      }
    } else if (this.state.mode === "interview") {
      let messages = [...this.state.interview, message];
      this.setState({
        interview: messages,
        messages: messages
      });
      this.saveState();
    } else if (this.state.mode === "general") {
      if (dir === "up") {
        let messages = [message, ...this.state.general];
        this.setState({
          general: messages,
          messages: messages
        });
      } else {
        let messages = [...this.state.general, message];
        this.setState({
          general: messages,
          messages: messages
        });
        this.saveState();
      }
    }
    if (this.state.mode === "screening") {
      if (
        this.state.screeningLoad === false &&
        this.state.interviewLoad === true
      ) {
        console.log(message);
        axios
          .get(
            ip +
              ":" +
              port +
              "/api/send_interview_message/" +
              this.state.user_data.userId +
              "/" +
              this.state.user_data.jobId +
              "/" +
              message.content
          )
          .then(res => {
            let message = res.data;
            if (message.restartInterview === false) {
              this.setState({
                restartInterview: false
              });
              this.saveStatus();
            }
            if (message.restartInterview === true) {
              this.setState({
                restartInterview: true
              });
              this.saveStatus();
            }

            let interview_messages = [...this.state.interview, message];
            let messages = [...this.state.messages, message];
            this.setState({
              loader: "none"
            });

            console.log(message);
            this.removeDummy();
            this.addDummy();
            if (
              message.type === "button" ||
              message.type === "option" ||
              message.restartInterview === false
            ) {
              this.setState({
                input: "chatbot__message-box-alt",
                height: "90%"
              });
            } else {
              this.enableInput();
            }

            this.saveState();
            if ("change" in message) {
              console.log(message);
              this.setState({
                interviewLoad: message.change.interviewLoad,
                interview_status_card: this.state.interview_status_card.filter(
                  val => val.jobId !== this.state.user_data.jobId
                ),
                input: "chatbot__message-box-alt",
                height: "90%"
              });
              this.saveStatus();
            }

            this.setState({
              interview: interview_messages,
              messages: messages
            });
            this.addDummy();
            this.saveState();
          });
      } else {
        axios
          .get(
            ip +
              ":" +
              port +
              "/api/send_message/" +
              this.state.user_data.userId +
              "/" +
              this.state.user_data.jobId +
              "/" +
              message.content
          )
          .then(res => {
            let message = res.data;

            this.addApiMessages(message);
            this.saveState();
            if ("change" in message) {
              this.setState({
                screeningStatus: "completed",
                interviewStatus: "running"
              });
              this.saveStatus();
              this.subtaractTicker("screening");

              console.log("interview is starting ", message);

              axios
                .get(
                  ip +
                    ":" +
                    port +
                    "/api/send_initial_interview_message/" +
                    this.state.user_data.jobId +
                    "/" +
                    this.state.user_data.userId +
                    ":" +
                    "first"
                )
                .then(res => {
                  let cards = res.data;
                  let time = "";
                  cards.map(data => {
                    time = data.time;
                  });
                  var dateFormat = require("dateformat");
                  var now = new Date();
                  this.setState({
                    time: {
                      ...this.state.time,
                      interview_time: time
                    }
                  });

                  this.addTicker("interview");
                  this.setState({
                    interview_status_card: [
                      ...this.state.interview_status_card,
                      cards[0]
                    ],
                    buffer: this.state.screening
                  });
                  this.saveState();

                  this.setState({
                    time: {
                      ...this.state.time,
                      interview_time: this.getRelativeTime(
                        dateFormat(now, "isoDateTime")
                      )
                    }
                  });
                  this.addDummy();
                  this.saveState();
                  axios
                    .get(
                      ip +
                        ":" +
                        port +
                        "/api/start_interview/" +
                        this.state.user_data.userId +
                        "/" +
                        this.state.user_data.jobId
                    )
                    .then(res => {
                      let message = res.data.multiple[0];
                      message.id = this.keyGenrator();

                      let messages = [...this.state.messages, message];
                      console.log(message);

                      this.setState({
                        interview: [message],
                        messages: messages
                      });

                      this.setState({
                        loader: "none"
                      });
                      this.saveState();
                      this.addDummy();
                    });
                });

              this.setState({
                screeningLoad: message.change.screeningLoad,
                screening_status_card: this.state.screening_status_card.filter(
                  val => val.jobId !== this.state.user_data.jobId
                )
              });

              this.saveState();
              this.saveStatus();
            }
          });
      }
    } else if (this.state.mode === "interview") {
      axios
        .get(
          ip +
            ":" +
            port +
            "/api/send_interview_message/" +
            this.state.user_data.userId +
            "/" +
            this.state.user_data.jobId +
            "/" +
            message.content
        )
        .then(res => {
          let message = res.data;
          this.setState({
            loader: "none"
          });
          if (message.restartInterview === false) {
            this.setState({
              restartInterview: false,
              input: "chatbot__message-box-alt",
              height: "90%"
            });
            this.saveStatus();
          }
          if (message.restartInterview === true) {
            this.setState({
              restartInterview: true
            });
            this.saveStatus();
          }

          console.log(message);
          if ("change" in message) {
            console.log(message);
            this.setState({
              interviewLoad: message.change.interviewLoad,
              interview_status_card: this.state.interview_status_card.filter(
                val => val.jobId !== this.state.user_data.jobId
              )
            });

            this.saveStatus();
            this.subtaractTicker("interview");
          }

          this.addApiMessages(message);
        });
    } else if (this.state.mode === "general") {
      let id = "";
      if (this.state.login === true) {
        id = this.state.user_data.userId;
      } else {
        id = this.props.cb.systemId;
      }

      axios
        .get(ip + ":" + port + "/api/search/" + id + "/" + message.content)
        .then(res => {
          let message = res.data;
          // this.setState({
          //   loader: "none"
          // });
          console.log(message);
          // setTimeout(function(){
          //   this.addApiMessages(message);
          // },2000).bind(this);
          setTimeout(() => {
            this.addApiMessages(message);
            this.saveState();
          }, 2000);
        });
    }

    // setTimeout(function(){
    // 	let len  = this.state.messages.length;
    // 	this.setState({
    // 	   items: this.state.messages.filter((x,i) => i != len -1 )
    // 	 });
    // },2000).bind(this);
  };
  showScreening = jobId => {
    if (this.state.screeningLoad === true) {
      // this.enableInput();
    }
    console.log(this.state.mode);
    this.setState({
      //	messages: this.state.screening,
      chat: true,
      catagory: false
    });
    this.startScreeening(jobId);
  };
  showInterview = jobId => {
    console.log(this.state.mode);
    if (this.state.interviewLoad === true) {
      // this.enableInput();
    }
    this.setState({
      messages: this.state.interview,
      chat: true,
      catagory: false
    });
    this.startInterview(jobId);
  };
  showGeneral = () => {
    this.subtaractTicker("general");
    let type = this.state.general.slice(0).reverse()[0].type;
    let time = this.state.general.slice(0).reverse()[0].time;
    //alert(type);

    if (type === "button" || type === "option") {
      this.setState({
        input: "chatbot__message-box-alt",
        height: "90%"
      });
    } else {
      this.enableInput();
    }

    this.setState({
      messages: this.state.general,
      chat: true,
      catagory: false,
      grid: false,
      mode: "general",
      back_style: "block",
      time: {
        ...this.state.time,
        general_time: time
      }
    });
  };

  render() {
    if (this.state.chat) {
      const chat_style = {
        display: this.state.vis.display,
        width: this.state.style.width
      };

      if (this.state.mode === "general") {
        return (
          <Vasi
            mUser={this.props.user}
            randomId={this.state.randomId}
            onGeneralPush={this.vasi__messagePush}
            general_msgs={this.state.general_msgs}
            onMinimize={this.props.actions.onMinimize}
            height={this.state.height}
            changeBackStyle={this.changeBackStyle}
            loader={this.state.loader}
            resetkey={this.resetkey}
            search_key={this.state.search_key}
            input={this.state.input}
            incCounter={this.incCounter}
            decCounter={this.decCounter}
            addDummy={this.addDummy}
            eof={this.state.eofmessage}
            searchResult={this.searchResult}
            enableInput={this.enableInput}
            disableInput={this.disableInput}
            input={this.state.input}
            saveState={this.saveState}
            back_style={this.state.back_style}
            vis={this.state.vis}
            mobile={this.state.mobile}
            reset={this.reset}
            style={chat_style}
            login={this.state.login}
            title={this.state.mode}
            changeChatStatus={this.changeChatStatus}
            style={this.state.style}
            addMessages={this.addMessages}
            messages={this.state.messages}
            changeGrid={this.changeGrid}
            addParams={this.addParams}
            userdata={this.state.user_data}
            changeCatStatus={this.changeCatStatus}
            grid={this.state.grid}
            changemode={this.changeMode}
            mode={this.state.mode}
            text_search={this.state.search}
            option_button={this.state.button}
            setButton={this.setButton}
            setSearch={this.setSearch}
          />
        );
      }

      return (
        <Chat
          restartProcess={this.restartProcess}
          mUser={this.props.user}
          onMinimize={this.props.actions.onMinimize}
          height={this.state.height}
          changeBackStyle={this.changeBackStyle}
          loader={this.state.loader}
          resetkey={this.resetkey}
          search_key={this.state.search_key}
          input={this.state.input}
          incCounter={this.incCounter}
          decCounter={this.decCounter}
          addDummy={this.addDummy}
          eof={this.state.eofmessage}
          searchResult={this.searchResult}
          enableInput={this.enableInput}
          disableInput={this.disableInput}
          input={this.state.input}
          saveState={this.saveState}
          back_style={this.state.back_style}
          vis={this.state.vis}
          mobile={this.state.mobile}
          reset={this.reset}
          style={chat_style}
          login={this.state.login}
          title={this.state.mode}
          changeChatStatus={this.changeChatStatus}
          style={this.state.style}
          addMessages={this.addMessages}
          messages={this.state.messages}
          changeGrid={this.changeGrid}
          addParams={this.addParams}
          userdata={this.state.user_data}
          changeCatStatus={this.changeCatStatus}
          grid={this.state.grid}
          changemode={this.changeMode}
          mode={this.state.mode}
          text_search={this.state.search}
          option_button={this.state.button}
          setButton={this.setButton}
          setSearch={this.setSearch}
        />
      );
    } else if (this.state.catagory) {
      const chat_style = {
        display: this.state.vis.display,
        width: this.state.style.width
      };
      // console.log(chat_style);
      return (
        <div
          className={`chatbot__chat ${this.state.chatBoxWindow}`}
          refs="chat"
          style={chat_style}
          onScroll={this.scrollHandler}>
          <Title
            input={this.state.input}
            incCounter={this.state.incCounter}
            decCounter={this.state.decCounter}
            searchResult={this.searchResult}
            enableInput={this.enableInput}
            disableInput={this.disableInput}
            text_search={this.state.text_search}
            setSearch={this.setSearch}
            back_style={this.state.back_style}
            mode={this.state.mode}
            vis={this.state.vis}
            mobile={this.state.mobile}
            style={this.state.style}
            catstyle={this.state.cat_style}
            login={this.state.login}
            title={this.state.mode}
            data={"cat"}
            user_data={this.state.user_data}
            status={this.changeChatStatus}
            key={this.keyGenrator}
            catStatus={this.changeCatStatus}
            changeGrid={this.changeGrid}
            grid={this.state.grid}
          />
          <Catagory
            showGeneral={this.showGeneral}
            changeBackStyle={this.changeBackStyle}
            tic={this.state.tic}
            resetkey={this.resetkey}
            changeJobId={this.changeJobId}
            saveState={this.saveState}
            time={this.state.time}
            changeToRecommendCard={this.changeToRecommendCard}
            changeToInterviewCard={this.changeToInterviewCard}
            changeToScreenCard={this.changeToScreenCard}
            recommendation_status_card={this.state.recommendation_status_card}
            screening_status_card={this.state.screening_status_card}
            interview_status_card={this.state.interview_status_card}
            vis={this.state.vis}
            grid={this.state.grid}
            catstyle={this.state.cat_style}
            login={this.state.login}
            mode={this.state.mode}
            userdata={this.state.user_data}
            showInterview={this.showInterview}
            showScreening={this.showScreening}
            showGeneral={this.showGeneral}
            key={this.keyGenrator}
            changeMode={this.changeMode}
            catcard={this.state.cat_card}
            mobile={this.state.mobile}
          />
        </div>
      );
    } else {
      return (
        <Secondary
          changeBackStyle={this.changeBackStyle}
          login={this.state.login}
          subtaractTicker={this.subtaractTicker}
          tic={this.state.tic}
          login={this.state.login}
          startRecommendation={this.startRecommendation}
          startInterview={this.startInterview}
          startScreeening={this.startScreeening}
          mode={this.state.mode}
          status={this.changeChatStatus}
          catstatus={this.changeCatStatus}
          key={this.keyGenrator}
        />
      );
    }
  }
}

export default App;
