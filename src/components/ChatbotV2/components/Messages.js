import React, { Component } from "react";
import Card from "./Card.js";

import Card2 from "./Card2.js";
import Card3 from "./Card3.js";
import OpCard from "./OpCard.js";
import assistant from "../cbcircle/img/vasi_img_new.png";
import UserLogo from "../../../assets/svg/IcUser";
import BirthCard from "./BirthCard.js";
import Button from "./Button.js";

import axios from "axios";
import {
  BrowserRouter as Router,
  Route,
  Link,
  withRouter
} from "react-router-dom";

import getCurrentTime from "./Time.js";
import SpringScrollbars from "./SpringScrollbars";

class Messages extends Component {
  state = {
    hasmore: true,
    reverse: null,
    windowsize: 7,
    firstId: null,
    lastId: null,
    counter: 0,
    counter1: 0,
    default: true,
    buttondata: ["Jobs", "Candidates"],

    fontstyle: { backgroundColor: "yellow" }
  };

  scrollHandler = () => {
    //console.log(this.refs);
  };
  upward = () => {
    //console.log("------------------------------------------top of section------------------------------ ");
    // axios
    // .get(
    //   "http://35.154.164.193:5007/api/infinitescroll/5bd84889ec17c511976640b2:general:" +
    //     this.state.firstId +
    //     ":" +
    //     this.state.lastId +
    //     ":" +
    //     "backward"
    // )
    // .then(res => {
    //   this.props.reset()
    //   let alldata = res.data.data;
    //   //console.log(alldata);
    //   this.setState({
    //     hasmore: res.data.hasmore,
    //     firstId: res.data.firstId,
    //     lastId: res.data.lastId
    //   });
    //   alldata.reverse().map(text => {
    //     this.props.addMessages(text,"up");
    //   });
    // });
  };

  backward = () => {
    //console.log("------------------------------------------end of section------------------------------ ");

    axios
      .get(
        "https://vasitum.com:5007/api/infinitescroll/5bd84889ec17c511976640b2:general:" +
          this.state.firstId +
          ":" +
          this.state.lastId +
          ":" +
          "forward"
      )
      .then(res => {
        this.props.reset();
        let alldata = res.data.data;
        //console.log(alldata);
        this.setState({
          hasmore: res.data.hasmore,
          firstId: res.data.firstId,
          lastId: res.data.lastId
        });
        alldata.map(text => {
          this.props.addMessages(text);
        });
      });
  };
  updateScroll = () => {
    let containerHeight =
      this.messageContainer.scrollHeight - this.messageContainer.scrollTop;
    let divHeight = Math.ceil(window.innerHeight * 0.88);
    //console.log("containerHeight----------------- " + containerHeight);
    //console.log("divHeight------------------------" + divHeight);

    if (this.messageContainer.scrollTop === 0) {
      this.upward();
    } else if (containerHeight === divHeight) {
      this.backward();
    }
  };

  scrollToBottom = () => {
    const el = document.getElementById("search");

    const { scrollbars } = this.refs;

    if (el) {
      let size = el.getBoundingClientRect().bottom;
      console.log(el.getBoundingClientRect());
      scrollbars.scrollTop(el.getBoundingClientRect().top);
      console.log("element exists");
    } else {
      const height = scrollbars.getScrollHeight();
      // scrollbars.scrollTop(height);
      scrollbars.scrollToBottom();
    }

    // if(this.messagesEnd){
    //   window.scrollTo({
    //     top:this.messagesEnd.current.offsetTop,
    //     behavior: "smooth"  // Optional, adds animation
    // })
    // if(this.messagesEnd){
    //   this.messagesEnd.scrollIntoView({ behavior: "smooth" });

    // }
  };
  componentDidUpdate() {
    this.scrollToBottom();
    //  this.props.enableInput();

    if (this.props.text_search && this.props.option_button) {
      if (this.state.height === "80%") {
        this.setState({
          height: "86%"
        });
      }
    }
  }
  componentWillUnmount() {
    // this.props.resetkey();
  }

  componentDidMount() {
    this.props.resetkey();
    const { scrollbars } = this.refs;
    console.log(scrollbars);
    this.scrollToBottom();

    // if(this.state.counter === 0){
    // axios
    //   .get(
    //     "http://35.154.164.193:5007/api/infinitescroll/5bd84889ec17c511976640b2:general:" +
    //       this.state.firstId +
    //       ":" +
    //       this.state.lastId +
    //       ":" +
    //       "null"
    //   )
    //   .then(res => {
    //     this.props.reset();
    //     let alldata = res.data.data;
    //     //console.log(alldata);
    //     this.setState({
    //       hasmore: res.data.hasmore,
    //       firstId: res.data.firstId,
    //       lastId: res.data.lastId
    //     });
    //     alldata.map(text => {
    //       this.props.addMessages(text);
    //     });
    //   });
    //   this.setState({
    //     counter:1
    //   })
    // }
  }

  changeRoute = route => {
    this.props.history.push(route);
    if (window.screen.availWidth < 1024) {
      this.props.onMinimize();
    }
  };

  render() {
    const { mUser } = this.props;
    const hasImage = mUser ? (mUser.userImg ? true : false) : false;
    const userId = mUser ? (mUser.id ? mUser.id : false) : false;
    const eof = (
      <div
        ref={el => {
          this.messagesEnd = el;
        }}
      />
    );
    const messageList = this.props.messages.length ? (
      this.props.messages.map((message, idx, message_array) => {
        if (message.content) {
        } else {
          message.content = [];
        }

        const msgList = message.content.map(text => {
          let newtext = text.toLowerCase();
          if (newtext.includes(this.props.search_key.toLowerCase())) {
            var i = newtext.indexOf(this.props.search_key.toLowerCase());
            let left = text.slice(0, i);
            let key = text.slice(i, i + this.props.search_key.length);
            let right = text.slice(
              i + this.props.search_key.length,
              text.length
            );

            return (
              <div className="chatbot__message-bot" key={message.id}>
                {left}
                <span style={this.state.fontstyle}>{key}</span>
                {right}
                <div
                  className="chatbot__bot-time"
                  style={{
                    paddingRight: "10px",
                    marginTop: "3px",
                    "font-size": "10px",
                    bottom: "-"
                  }}>
                  {message.time}{" "}
                </div>
              </div>
            );
          } else {
            return (
              <div className="chatbot__message-bot" key={message.id}>
                {text}
                <div
                  className="chatbot__bot-time"
                  style={{
                    paddingRight: "10px",
                    marginTop: "3px",
                    "font-size": "10px",
                    bottom: "-"
                  }}>
                  {message.time}{" "}
                </div>
              </div>
            );
          }
        });

        if (message.type === "bot") {
          return (
            <div className="chatbot__message-left">
              <div className="chatbot__bot-assistant">
                <img src={assistant} alt="assistant pic not available" />
              </div>
              {msgList}
            </div>
          );
        } else if (message.type === "button") {
          //console.log(message)

          return (
            <Button
              buttondata={message.content}
              id={message.id}
              active={message.active}
              disableInput={this.props.disableInput}
              addParams={this.props.addParams}
            />
          );
        } else if (message.type === "Card1") {
          return (
            <div
              onClick={e => {
                e.preventDefault();
                e.stopPropagation();
                this.changeRoute("/view/job/" + message.jobId);
              }}>
              <Card msg={message} key={message.id} />
            </div>
          );
        } else if (message.type === "Card2") {
          return (
            <Card2
              msg={message}
              key={message.id}
              disableInput={this.props.disableInput}
            />
          );
        } else if (message.type === "birth") {
          return (
            <BirthCard
              msg={message}
              key={message.id}
              disableInput={this.props.disableInput}
            />
          );
        } else if (message.type === "popup") {
          return (
            <div className="chatbot__message-right">
              <div className="chatbot__personal-assistant">
                {hasImage ? (
                  <img
                    style={{
                      borderRadius: "50%"
                    }}
                    src={`https://s3-us-west-2.amazonaws.com/img-mwf/${userId}/image.jpg`}
                    alt="assistant pic not available"
                  />
                ) : (
                  <UserLogo
                    width="35"
                    height="35"
                    rectcolor="#f7f7fb"
                    pathcolor="#c8c8c8"
                  />
                )}
              </div>
              <div
                className="chatbot__message-personal"
                key={message.id}
                disableInput={this.props.disableInput}>
                {message.content}
              </div>
            </div>
          );
        } else if (message.type === "Card3") {
          return <Card3 disableInput={this.props.disableInput} />;
        } else if (message.type === "option") {
          return (
            <OpCard
              search_key={this.props.search_key}
              key={message.id}
              id={message.id}
              msg={message}
              disableInput={this.props.disableInput}
              addParams={this.props.addParams}
              active={message.active}
              disableInput={this.props.disableInput}
            />
          );
        } else if (message.type === "test") {
          return (
            <div
              id="search"
              ref={el => {
                this.messagesEnd = el;
              }}
            />
          );
        } else if (message.type === "processing") {
          return (
            <div className="chatbot__message-left">
              <div
                className="chatbot__bot-assistant"
                style={{ bottom: "-45px" }}>
                <img src={assistant} alt="assistant pic not available" />
              </div>

              <div>
                <span class="chatbot__saving">
                  <span>.</span>
                  <span>.</span>
                  <span>.</span>
                </span>
              </div>
            </div>
          );
        } else {
          return (
            <div className="chatbot__message-right">
              <div className="chatbot__personal-assistant">
                {hasImage ? (
                  <img
                    style={{
                      borderRadius: "50%"
                    }}
                    src={`https://s3-us-west-2.amazonaws.com/img-mwf/${userId}/image.jpg`}
                    alt="assistant pic not available"
                  />
                ) : (
                  <UserLogo
                    width="35"
                    height="35"
                    rectcolor="#f7f7fb"
                    pathcolor="#c8c8c8"
                  />
                )}
              </div>
              <div className="chatbot__message-personal" key={message.id}>
                {message.content}
              </div>
              <div
                className="chatbot__bot-time-right"
                style={{
                  paddingRight: "10px",
                  marginTop: "3px",
                  "font-size": "10px"
                }}>
                {message.time}{" "}
              </div>
            </div>
          );
        }
      })
    ) : (
      <div className="chatbot__message-left">
        <div className="chatbot__bot-assistant" style={{ bottom: "-45px" }}>
          <img src={assistant} alt="assistant pic not available" />
        </div>

        <div>
          <span class="chatbot__saving">
            <span>.</span>
            <span>.</span>
            <span>.</span>
          </span>
        </div>
      </div>
    );

    return (
      <div
        style={{ height: this.props.height }}
        className="chatbot__messages "
        id="chatbot__container"
        // id="chatbot__auto"
        onScroll={this.updateScroll}
        ref={el => {
          this.messageContainer = el;
        }}>
        <SpringScrollbars ref="scrollbars" style={{ height: "100%" }}>
          {/* <InfiniteScroll
          pageStart={0}
          loadMore={this.loadFunc}
          hasMore={this.state.hasmore}
          loader={<div className="loader" key={0}></div> }
          useWindow={false}
          isReverse={this.state.reverse}
        >
        </InfiniteScroll> */}

          {messageList}

          <div
            className="chatbot__message-left"
            style={{ display: this.props.loader }}>
            <div className="chatbot__bot-assistant" style={{ bottom: "-67px" }}>
              <img src={assistant} alt="assistant pic not available" />
            </div>
            <div>
              <span class="chatbot__saving">
                <span>.</span>
                <span>.</span>
                <span>.</span>
              </span>
            </div>
          </div>
        </SpringScrollbars>
      </div>
    );
  }
}
export default withRouter(Messages);
