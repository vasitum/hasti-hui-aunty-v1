import React from "react";
import Title from "../Title.js";
import VasiMessage from "../VasiMessage";
import UserMessage from "../UserMessage";
import UserInput from "../UserInput";
import JobCard from "../common/JobCard";
import PeopleCard from "../common/PeopleCard";
import InitialScreen from "../common/IntialScreen";
import { MSG_TYPE } from "../constants";
import { format } from "date-fns";
import "./index.scss";

function compileHidden(style) {
  if (!style) {
    return "";
  }

  if (!style.width) {
    return "";
  }

  if (style.width !== "0px") {
    return "";
  }

  return "is-hidden";
}

function getUserId(userdata, randomId) {
  if (!userdata) {
    return randomId;
  }

  if (!userdata.userId) {
    return randomId;
  }

  return userdata.userId;
}

export default class extends React.Component {
  constructor(props) {
    super(props);

    // autobind
    this.getAnswerFromChatbot = this.getAnswerFromChatbot.bind(this);
    this.startInitalChat = this.startInitalChat.bind(this);
    this.scrollRef = React.createRef();
    // this.textInput = React.createRef();
  }

  state = {
    messages: [],
    loading: true,
    showInit: true
  };

  scrollToBottom = () => {
    if (!this.scrollRef) {
      return;
    }

    if (!this.scrollRef.current) {
      return;
    }

    this.scrollRef.current.scrollTop = this.scrollRef.current.scrollHeight;
    // console.log("Scroll bottom called");
  };

  async sendChatbotMsgToLog(text) {
    const { userdata, randomId } = this.props;
    const userId = getUserId(userdata, randomId);
    try {
      try {
        const res_temp = await fetch(
          `https://chatbot.vasitum.com/en/api/v1/chatlogs?user_id=${encodeURIComponent(
            userId
          )}&dialogue=${encodeURIComponent(text)}&time=${new Date().getTime()}`
        );
      } catch (error) {
        console.error(error);
      }
    } catch (error) {
      console.error(error);
      return null;
    }
  }

  async getAnswerFromChatbot(text) {
    const { userdata, randomId } = this.props;
    const userId = getUserId(userdata, randomId);
    try {
      try {
        const res_temp = await fetch(
          `https://chatbot.vasitum.com/en/api/v1/chatlogs?user_id=${encodeURIComponent(
            userId
          )}&dialogue=${encodeURIComponent(text)}&time=${new Date().getTime()}`
        );
      } catch (error) {
        console.error(error);
      }
      const textRes = await fetch(
        `https://chatbot.vasitum.com/en/api/v1/autocorrect?query=${encodeURIComponent(
          text
        )}`
      );
      if (textRes.status === 200) {
        const finalText = await textRes.text();
        const res = await fetch(
          `https://chatbot.vasitum.com/conversations/${userId}/respond`,
          {
            method: "POST",
            body: JSON.stringify({
              query: finalText
            })
          }
        );
        if (res.status === 200) {
          const data = await res.json();
          // console.log("Chatbot Bolta hai", data);
          if (!data || !Array.isArray(data)) {
            return null;
          }

          if (!data.length) {
            return "EMPTY";
          }

          return data;
        } else {
          return null;
        }
      }
    } catch (error) {
      console.error(error);
      return null;
    }
  }

  componentWillUnmount() {
    this.props.saveState();
  }

  async startInitalChat(starter) {
    try {
      const data = await this.getAnswerFromChatbot(starter);
      if (!data) {
        this.onMessagePush([
          {
            type: MSG_TYPE.VASI,
            message:
              "I’m sorry. I don’t understand. can you please repeat that?",
            time: new Date().getTime()
          }
        ]);
        return;
      }

      this.onMessagePush(
        data.map(val => {
          return {
            type: MSG_TYPE.VASI,
            message: val.text,
            time: new Date().getTime()
          };
        })
      );
    } catch (e) {
      console.error(e);
      this.onMessagePush([
        {
          type: MSG_TYPE.VASI,
          message: "I’m sorry. I don’t understand. can you please repeat that?",
          time: new Date().getTime()
        }
      ]);
    }
  }

  onInitalButtonClick = type => {
    this.setState(
      {
        showInit: false
      },
      () => {
        this.onMessagePush([
          {
            type: MSG_TYPE.USER_MSG,
            message: type,
            time: new Date().getTime()
          }
        ]);

        this.startInitalChat(type);
      }
    );
  };

  async componentDidMount() {
    const { general_msgs } = this.props;
    if (general_msgs.length > 0) {
      this.setState(
        {
          loading: false,
          showInit: false
        },
        () => {
          this.scrollToBottom();
        }
      );
      return;
    }
    // call you api here
    // try {
    //   const data = await this.getAnswerFromChatbot("hello");
    //   if (!data) {
    //     this.onMessagePush([
    //       {
    //         type: MSG_TYPE.VASI,
    //         message:
    //           "I’m sorry. I don’t understand. can you please repeat that?",
    //         time: new Date().getTime()
    //       }
    //     ]);
    //     return;
    //   }

    //   this.onMessagePush(
    //     data.map(val => {
    //       return {
    //         type: MSG_TYPE.VASI,
    //         message: val.text,
    //         time: new Date().getTime()
    //       };
    //     })
    //   );
    // } catch (error) {
    //   this.onMessagePush([
    //     {
    //       type: MSG_TYPE.VASI,
    //       message: "I’m sorry. I don’t understand. can you please repeat that?",
    //       time: new Date().getTime()
    //     }
    //   ]);
    // }
  }

  onMessageLoad = () => {
    this.setState({
      loading: true
    });
  };

  onMessagePush = messages => {
    const { onGeneralPush } = this.props;

    setTimeout(() => {
      onGeneralPush(messages, () => {
        this.setState({
          loading: false
        });
        this.scrollToBottom();
        try {
          this.sendChatbotMsgToLog(JSON.stringify(messages));
        } catch (error) {
          console.error(error);
        }
      });
    }, 2500);
  };

  onMessageUserPush = messages => {
    const { onGeneralPush } = this.props;
    onGeneralPush(messages, () => {
      this.scrollToBottom();
    });
  };

  onUserMessageLoad = message => {
    this.onMessageLoad();

    const splitMsg = message.split(",");
    let finalMsg;
    if (splitMsg.length > 1) {
      finalMsg = message;
    } else {
      finalMsg = message + ",";
    }

    this.getAnswerFromChatbot(finalMsg)
      .then(data => {
        if (!data) {
          this.onMessagePush([
            {
              type: MSG_TYPE.VASI,
              message:
                "I’m sorry. I don’t understand. can you please repeat that?",
              time: new Date().getTime()
            }
          ]);
          return;
        }

        if (data === "EMPTY") {
          this.onMessagePush([
            {
              type: MSG_TYPE.VASI,
              message:
                "I’m sorry. I don’t understand. can you please repeat that?",
              time: new Date().getTime()
            }
          ]);
          return;
        }

        try {
          const jsonData = JSON.parse(String(data[0].text));
          const dataParsed = jsonData.json;

          if (!dataParsed.length) {
            this.onMessagePush([
              {
                type: MSG_TYPE.VASI,
                message:
                  jsonData.type === "user"
                    ? "I am afraid that there’s no candidate that rightly fits your requirement at the moment."
                    : "My apologies, there aren’t any job openings suiting your need.",
                time: new Date().getTime()
              }
            ]);
            return;
          }

          this.onMessagePush(
            []
              .concat([
                {
                  type: MSG_TYPE.VASI,
                  message: "Here is some data i found",
                  time: new Date().getTime()
                }
              ])
              .concat(
                dataParsed
                  .map(val => {
                    return {
                      type: jsonData.type,
                      message: val,
                      time: new Date().getTime()
                    };
                  })
                  .filter((val, idx) => idx < 5)
              )
          );
        } catch (error) {
          console.log(error);
          this.onMessagePush(
            data.map(val => {
              return {
                type: MSG_TYPE.VASI,
                message: val.text,
                time: new Date().getTime()
              };
            })
          );
        }
      })
      .catch(err => {
        console.error(err);
        this.onMessagePush([
          {
            type: MSG_TYPE.VASI,
            message:
              "I’m sorry. I don’t understand. can you please repeat that?",
            time: new Date().getTime()
          }
        ]);
      });
  };

  onInputSubmit = message => {
    this.onMessageUserPush([
      {
        type: MSG_TYPE.USER_MSG,
        message: message,
        time: new Date().getTime()
      }
    ]);
    this.onUserMessageLoad(message);
  };

  render() {
    const { loading, showInit } = this.state;
    const {
      general_msgs: messages,
      mUser,
      restartProcess,
      userdata,
      randomId
    } = this.props;
    const hasImage = mUser ? (mUser.userImg ? true : false) : false;
    const userId = getUserId(userdata, randomId);
    // console.log("CB Test Style", this.props.style);
    if (showInit) {
      return (
        <div className="chatbot__chat" style={this.props.style}>
          <InitialScreen
            userId={userId}
            user_data={this.props.userdata}
            onClick={this.onInitalButtonClick}
            hidden={compileHidden(this.props.style)}
            status={this.props.changeChatStatus}
          />
        </div>
      );
    }

    return (
      <div className="chatbot__chat" style={this.props.style}>
        <Title
          restartProcess={restartProcess}
          search_key={this.props.search_key}
          input={this.props.input}
          incCounter={this.props.incCounter}
          decCounter={this.props.decCounter}
          searchResult={this.props.searchResult}
          enableInput={this.props.enableInput}
          disableInput={this.props.disableInput}
          setSearch={this.props.setSearch}
          text_search={this.props.text_search}
          back_style={this.props.back_style}
          mode={this.props.mode}
          vis={this.props.vis}
          style={this.props.style}
          login={this.props.login}
          title={this.props.title}
          user_data={this.props.userdata}
          mobile={this.props.mobile}
          status={this.props.changeChatStatus}
          data={"chat"}
          changeGrid={this.props.changeGrid}
          catStatus={this.props.changeCatStatus}
          grid={this.props.grid}
        />
        <div
          ref={this.scrollRef}
          className={`Vasi__container ${compileHidden(this.props.style)}`}>
          {messages.map(message => {
            if (message.type === MSG_TYPE.VASI) {
              return (
                <VasiMessage
                  text={message.message}
                  time={format(Number(message.time), "hh:mm A")}
                />
              );
            } else if (message.type === MSG_TYPE.USER_MSG) {
              return (
                <UserMessage
                  userId={mUser ? (mUser.id ? mUser.id : null) : null}
                  hasImage={hasImage}
                  text={message.message}
                  time={format(Number(message.time), "hh:mm A")}
                />
              );
            } else if (message.type === MSG_TYPE.JOB) {
              return <JobCard data={message.message} />;
            } else if (message.type === MSG_TYPE.USER) {
              return <PeopleCard data={message.message} />;
            }
          })}

          {loading ? <VasiMessage text={""} loading /> : null}
          {/* <JobCard /> */}
        </div>
        <UserInput
          hidden={compileHidden(this.props.style)}
          onSubmit={this.onInputSubmit}
          disabled={loading}
        />
      </div>
    );
  }
}
