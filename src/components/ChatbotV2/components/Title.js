import React, { Component } from "react";
import { Icon } from "semantic-ui-react";
import Back from "./images/backarrow.png";
import Grid from "./images/grid.png";
import Search from "./Search.js";
import Search_Icon from "./images/search.png";
import Close from "./images/close.png";
import CollapseClose from "./images/icon-collapse.png";
import Expand from "./images/expand.png";
import Up from "./images/forward.png";
import Down from "./images/forward.png";
import Minimize from "./images/minimize.png";
import BackSend from "./images/send.png";
import RefreshButton from "./images/refresh-01-512.png";
import CollapseIcon from "../../../assets/svg/IcCollapseButton";
function shouldShowBack(user) {
  if (!user) {
    return false;
  }

  if (!user.userId) {
    return false;
  }

  return true;
}

class Title extends Component {
  state = {
    expimg: Expand,
    title: this.props.title,
    display: {
      display: "none"
    },
    search_value: "",
    search: false,
    option: false,
    previnput: "",
    sendIcon: "",
    updown: "none",
    content: []
  };
  Capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }
  componentDidMount() {
    if (this.props.login) {
      this.setState({
        display: {
          display: "block"
        }
      });
    } else {
      this.setState({
        display: {
          display: "none"
        }
      });
    }
    if (this.props.title === "screening") {
      this.setState({
        title: "Screening"
      });
    } else if (this.props.title === "interview") {
      this.setState({
        title: "Interview"
      });
    } else if (this.props.title === "general") {
      this.setState({
        title: ""
      });
    }
  }
  changeHandler = param => {
    this.props.status(param);
    console.log("------------------------------------" + param);
    if (param === "expand") {
      if (this.state.expimg === Expand) {
        this.setState({
          expimg: Minimize
        });
      } else {
        this.setState({
          expimg: Expand
        });
      }
    }
  };
  catHandler = (param, param2) => {
    if (param === "all") {
      this.props.changeGrid();
    }
    this.props.catStatus(param, param2);
  };
  // submitHandler = () => {

  // };

  searchHandler = () => {
    this.setState({
      search: true,
      previnput: this.props.input,
      updown: "none"
    });

    this.props.disableInput();
  };

  moreoption = () => {
    this.setState({
      option: true
    });
  };
  changeSeachHandler = e => {
    this.setState({
      sendIcon: ""
    });
    if (e.target) {
      this.setState({
        content: [e.target.value],
        search_value: e.target.value
      });
    }
  };
  submitHandler = e => {
    e.preventDefault();
    this.setState({
      search: false,
      option: false
    });
    if (this.state.previnput === "chatbot__message-box") {
      this.props.enableInput();
    }
    if (this.state.content.length === 0) {
    } else {
      // this.props.addMessages(this.state);
      this.setState({
        search_value: this.state.content[0]
      });
      // console.log(this.state.content[0]);
      if (this.state.content[0]) {
        this.props.searchResult(this.state.content[0]);
      }
    }
    this.setState({
      content: []
    });
  };
  searchSubmitHandler = e => {
    if (this.state.content[0]) {
      this.setState({
        updown: "block"
      });
    }

    e.preventDefault();
    // this.setState({
    //   search: false,
    //   option:false
    // });
    if (this.state.previnput === "chatbot__message-box") {
      this.props.enableInput();
    }
    if (this.state.content.length === 0) {
    } else {
      // this.props.addMessages(this.state);
      this.setState({
        search_value: this.state.content[0]
      });
      // console.log(this.state.content[0]);
      if (this.state.content[0]) {
        this.props.searchResult(this.state.content[0]);
      }
    }
    // this.setState({
    //   content:[]
    // })
  };

  downSubmitHandler = e => {
    this.props.decCounter();
    e.preventDefault();
    this.setState({
      search: true,
      option: false
    });
    if (this.state.previnput === "chatbot__message-box") {
      this.props.enableInput();
    }
    // if (this.state.content.length === 0) {

    // } else {
    //   // this.props.addMessages(this.state);
    //   console.log(this.state.content[0]);
    //   this.props.searchResult(this.state.content[0]);

    // }
    this.props.searchResult(this.state.search_value);
    // this.setState({
    //   content:[]
    // })
  };

  upSubmitHandler = e => {
    this.props.incCounter();
    e.preventDefault();
    this.setState({
      search: true,
      option: false
    });
    if (this.state.previnput === "chatbot__message-box") {
      this.props.enableInput();
    }

    // if (this.state.content.length === 0) {

    // } else {
    //   // this.props.addMessages(this.state);
    //   console.log(this.state.content[0]);
    //   this.props.searchResult(this.state.content[0]);

    // }
    this.props.searchResult(this.state.search_value);
    console.log(this.state.search_value);
    // this.setState({
    //   content:[]
    // })
  };

  render() {
    const { user_data, restartProcess } = this.props;
    const showBack = shouldShowBack(user_data);
    if (this.state.search) {
      return (
        <div className="chatbot__tittle_search" style={this.props.vis}>
          <form
            onSubmit={this.submitHandler}
            style={this.props.style}
            autoComplete="off">
            <input
              className="chatbot__search"
              style={this.props.style}
              type="text"
              placeholder="Type here"
              id="message-input"
              onChange={this.changeSeachHandler}
              value={this.state.content}
            />
            <div
              onClick={this.submitHandler}
              className="chatbot__search_submit"
              style={{ display: this.state.sendIcon }}>
              <img
                src={BackSend}
                className="chatbot__rotate"
                alt="submit-icon not available"
              />
            </div>
            <div
              className="chatbot__search_image"
              onClick={this.searchSubmitHandler}>
              <img
                src={Search_Icon}
                className="chatbot__rotate"
                alt="submit-icon not available"
              />
            </div>
            <div
              className="chatbot__search_up"
              style={{ display: this.state.updown }}
              onClick={this.downSubmitHandler}>
              <img
                src={Up}
                className="chatbot__rotate270"
                alt="submit-icon not available"
              />
            </div>
            <div
              className="chatbot__search_down"
              style={{ display: this.state.updown }}
              onClick={this.upSubmitHandler}>
              <img
                src={Down}
                className="chatbot__rotate90"
                alt="submit-icon not available"
              />
            </div>
          </form>
        </div>
      );
    } else {
      return (
        <div className="chatbot__chat-title" style={this.props.vis}>
          <div
            style={{
              paddingTop: "5px",
              // display:
              //   this.props.mode === "general" &&
              //   window.screen.availWidth > 1024 &&
              //   this.props.grid === false
              //     ? "none"
              //     : this.props.back_style
              display: this.props.grid ? "none" : "block"
            }}
            className="chatbot__title-image chatbot__back"
            onClick={e => this.changeHandler("cat", e)}>
            <img src={Back} alt="Back  not available" />
          </div>
          <div className="chatbot__title-image chatbot__back">
            <span style={{ "font-size": "15px" }}>
              {this.props.mode === "general" || this.props.mode === "category"
                ? ""
                : this.Capitalize(this.props.mode)}
            </span>
          </div>
          {/* <div
            title={"Close Chat"}
            className="chatbot__title-image chatbot__close"
            onClick={e => this.changeHandler("close", e)}>
            <img src={Close} alt=" Close  not available" />
          </div> */}
          <div
            title={"Minimize chat"}
            className="chatbot__title-image chatbot__close"
            onClick={e => this.changeHandler("close", e)}>
            <img
              src={CollapseClose}
              style={{
                width: "18px"
              }}
              alt=" Close  not available"
            />
            {/* <CollapseIcon /> */}
          </div>
          {this.props.mode === "screening" && this.props.data === "chat" ? (
            <div
              title={"Restart Screening"}
              style={{
                marginRight: "29px"
              }}
              className="chatbot__title-image chatbot__close"
              onClick={restartProcess}>
              <Icon
                style={{
                  color: "#6e768b"
                }}
                name="redo"
              />
            </div>
          ) : null}

          {this.props.grid
            ? ""
            : // <div
              //   className="chatbot__title-image chatbot__grid"
              //   style={this.state.display}
              //   onClick={e => this.catHandler("all", this.props.title, e)}>
              //   <img
              //     src={Grid}
              //     alt=" Grid not  available"
              //     className="chatbot__title-img"
              //   />
              // </div>
              ""}
          {/* <Search
            searchHandler={this.searchHandler}
            display={this.state.display}
            moreoption={this.moreoption}
            option={this.state.option}
          /> */}
        </div>
      );
    }
  }
}

export default Title;
