import React, { Component } from "react";
import MainHeader from "./MainHeader";
import { Document, Page, Text, View, StyleSheet } from "@react-pdf/renderer";
export default class Header extends Component {
  render() {
    return (
      <View>
        <Text>
          <MainHeader />
        </Text>
      </View>
    );
  }
}
