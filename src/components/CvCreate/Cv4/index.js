import React, { Component } from "react";
import { Icon } from "semantic-ui-react";
import Header from "./Header";
import Main from "./Main";
import "./index.scss";
import BottomAction from "../CvActionButton";
import { Document, Page, Text, View, StyleSheet } from "@react-pdf/renderer";
export default class CvFirst extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      // <React.Fragment>
      <View className="cv-template-first">
        <View className="resume-wrapper text-center position-relative">
          <View className="resume-wrapper-inner mx-auto">
            <View className="text-left bg-white shadow-lg">
              {/* <Header />
                <Main /> */}
              <Text>
                <Header />
              </Text>
            </View>
            {/* <BottomAction /> */}
          </View>
        </View>
      </View>
      // </React.Fragment>
    );
  }
}
