import React, { Component } from "react";
import EditButton from "../../CvEditAction";
export default class Language extends Component {
  render() {
    return (
      <div>
        <section className="resume-section education-section mb-5">
          <h2 className="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">
            Certifications{" "}
            <span className="float-right">
              <EditButton />
            </span>
          </h2>
          <div className="resume-section-content">
            <ul className="list-unstyled">
              <li className="mb-2">
                <div className="resume-degree font-weight-bold">
                  Certificate Name
                </div>
                <div className="resume-degree-org">
                  Award desc goes here, ultricies nec, pellentesque
                </div>
                <div className="resume-degree-time">2010 - 2011</div>
              </li>
              <li>
                <div className="resume-degree font-weight-bold">
                  Certificate Name
                </div>
                <div className="resume-degree-org">
                  Award desc goes here, ultricies nec, pellentesque
                </div>
                <div className="resume-degree-time">2007 - 2010</div>
              </li>
            </ul>
          </div>
        </section>
      </div>
    );
  }
}
