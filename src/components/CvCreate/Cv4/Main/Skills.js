import React, { Component } from "react";
import EditButton from "../../CvEditAction";
export default class Skills extends Component {
  render() {
    return (
      <div>
        <section className="resume-section skills-section mb-5">
          <h2 className="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">
            Skills &amp; Tools{" "}
            <span className="float-right">
              <EditButton />
            </span>
          </h2>
          <div className="resume-section-content">
            <div className="resume-skill-item">
              <ul className="list-unstyled mb-4">
                <li className="mb-2">
                  <div className="resume-skill-name">Angular</div>
                  <div className="progress resume-progress">
                    <div
                      className="progress-bar theme-progress-bar-dark"
                      role="progressbar"
                      style={{ width: "98%" }}
                      aria-valuenow="25"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    />
                  </div>
                </li>
                <li className="mb-2">
                  <div className="resume-skill-name">React</div>
                  <div className="progress resume-progress">
                    <div
                      className="progress-bar theme-progress-bar-dark"
                      role="progressbar"
                      style={{ width: "94%" }}
                      aria-valuenow="25"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    />
                  </div>
                </li>
                <li className="mb-2">
                  <div className="resume-skill-name">JavaScript</div>
                  <div className="progress resume-progress">
                    <div
                      className="progress-bar theme-progress-bar-dark"
                      role="progressbar"
                      style={{ width: "96%" }}
                      aria-valuenow="25"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    />
                  </div>
                </li>

                <li className="mb-2">
                  <div className="resume-skill-name">Node.js</div>
                  <div className="progress resume-progress">
                    <div
                      className="progress-bar theme-progress-bar-dark"
                      role="progressbar"
                      style={{ width: "92%" }}
                      aria-valuenow="25"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    />
                  </div>
                </li>
                <li className="mb-2">
                  <div className="resume-skill-name">HTML/CSS/SASS/LESS</div>
                  <div className="progress resume-progress">
                    <div
                      className="progress-bar theme-progress-bar-dark"
                      role="progressbar"
                      style={{ width: "96%" }}
                      aria-valuenow="25"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    />
                  </div>
                </li>
              </ul>
            </div>

            <div className="resume-skill-item">
              <ul className="list-unstyled">
                <li className="mb-2">
                  <div className="resume-skill-name">Python/Django</div>
                  <div className="progress resume-progress">
                    <div
                      className="progress-bar theme-progress-bar-dark"
                      role="progressbar"
                      style={{ width: "95%" }}
                      aria-valuenow="25"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    />
                  </div>
                </li>
                <li className="mb-2">
                  <div className="resume-skill-name">Ruby/Rails</div>
                  <div className="progress resume-progress">
                    <div
                      className="progress-bar theme-progress-bar-dark"
                      role="progressbar"
                      style={{ width: "92%" }}
                      aria-valuenow="25"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    />
                  </div>
                </li>
                <li className="mb-2">
                  <div className="resume-skill-name">PHP</div>
                  <div className="progress resume-progress">
                    <div
                      className="progress-bar theme-progress-bar-dark"
                      role="progressbar"
                      style={{ width: "86%" }}
                      aria-valuenow="25"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    />
                  </div>
                </li>
                <li className="mb-2">
                  <div className="resume-skill-name">WordPress/Shopify</div>
                  <div className="progress resume-progress">
                    <div
                      className="progress-bar theme-progress-bar-dark"
                      role="progressbar"
                      style={{ width: "82%" }}
                      aria-valuenow="25"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    />
                  </div>
                </li>
              </ul>
            </div>

            <div className="resume-skill-item">
              <h4 className="resume-skills-cat font-weight-bold">Others</h4>
              <ul className="list-inline">
                <li className="list-inline-item">
                  <span className="badge badge-light">DevOps</span>
                </li>
                <li className="list-inline-item">
                  <span className="badge badge-light">Code Review</span>
                </li>
                <li className="list-inline-item">
                  <span className="badge badge-light">Git</span>
                </li>
                <li className="list-inline-item">
                  <span className="badge badge-light">Unit Testing</span>
                </li>
                <li className="list-inline-item">
                  <span className="badge badge-light">Wireframing</span>
                </li>
                <li className="list-inline-item">
                  <span className="badge badge-light">Sketch</span>
                </li>
                <li className="list-inline-item">
                  <span className="badge badge-light">Balsamiq</span>
                </li>
                <li className="list-inline-item">
                  <span className="badge badge-light">WordPress</span>
                </li>
                <li className="list-inline-item">
                  <span className="badge badge-light">Shopify</span>
                </li>
              </ul>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
