import React, { Component } from "react";
import Summary from "./Summary";
import Experience from "./Experience";
import Skills from "./Skills";
import Educations from "./Education";
import Certification from "./Certification";
export default class Main extends Component {
  render() {
    return (
      <div>
        <div className="resume-body p-5">
          <Summary />
          <div className="row">
            <div className="col-lg-9">
              <Experience />
            </div>
            <div className="col-lg-3">
              <Skills />
              <Educations />
              <Certification />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
