import React, { Component } from "react";
import { Button, Responsive, Modal, Popup } from "semantic-ui-react";
import EditProfilePage from "../../EditProfilePage";
import EditIcon from "../../../assets/svg/IcEdit";
import "./index.scss";

export default class CvActionButton extends Component {
  constructor(props) {
    super(props);
    this.state = { modalOpen: false };
  }

  handleOpen = () => this.setState({ modalOpen: true });

  handleClose = () => this.setState({ modalOpen: false });

  componentDidMount() {
    const { user } = this.props;

    if (
      !user.fName &&
      !user.lName &&
      !window.localStorage.getItem("X__LOADED")
    ) {
      this.handleOpen();
      window.localStorage.setItem("X__LOADED", true);
    }
  }

  componentWillUnmount() {
    window.localStorage.removeItem("X__LOADED");
  }

  render() {
    let user = this.props.user;
    return (
      <React.Fragment>
        <Responsive minWidth={1025}>
          <Popup
            className="edit-action-button-block"
            trigger={
              <Button
                onClick={this.handleOpen}
                circular
                className="edit-action-button">
                <EditIcon
                  width="11.688"
                  height="11.5"
                  viewBox="0 0 11.688 11.5"
                  pathcolor="#c8c8c8"
                />
              </Button>
            }
            content="Edit"
            position="top center"
          />
        </Responsive>
        <Modal
          className="modal_form"
          onClose={this.handleClose}
          open={this.state.modalOpen}
          closeIcon>
          <EditProfilePage
            userData={user.fName && user.lName ? user : null}
            onCancelClick={this.handleClose}
            // activeSection={editModalActiveSection}
          />
        </Modal>
      </React.Fragment>
    );
  }
}
