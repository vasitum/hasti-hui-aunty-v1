import React, { Component } from "react";
import FirstCv from "../Cv1";
import SinglePage from "../GeneratePdf/SinglePage";
import PrintButton from "../GeneratePdf/PrintButton";
import MultiPage from "../GeneratePdf/MultiPage";
import RPdf from "./RPdf";
import { PDFViewer } from "@react-pdf/renderer";
export default class GeneratePdf extends Component {
  render() {
    return (
      <div style={{ marginTop: 100 }}>
        <div className="bg-black-80 w-100 pv5">
          <div className="white mt3 tc f3">Single Page Mode</div>
          <PrintButton id={"singlePage"} label={"Print single page"} />
          <SinglePage id={"singlePage"} />

          <div className="white mt5 tc f3">Multi Page Mode</div>
          <PrintButton id={"multiPage"} label={"Print multiplate pages"} />
          <MultiPage id={"multiPage"} />
        </div>
        {/* <PDFViewer>
          <RPdf />
        </PDFViewer> */}
      </div>
    );
  }
}
