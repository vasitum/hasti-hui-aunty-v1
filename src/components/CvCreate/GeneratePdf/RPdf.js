import React from "react";
import { Document, Page, Text, View, StyleSheet } from "@react-pdf/renderer";
import "./index.scss";
import FirstCv from "../Cv4";
// Create styles
const styles = StyleSheet.create({
  page: {
    flexDirection: "row",
    backgroundColor: "#E4E4E4"
  },
  section: {
    margin: 10,
    padding: 10,
    flexGrow: 1
  }
});

// Create Document Component
const RPdf = () => (
  <Document>
    <Page size="A4" style={styles.page} className="react-pdf">
      <View style={styles.section}>
        <FirstCv />
      </View>
    </Page>
  </Document>
);

export default RPdf;
