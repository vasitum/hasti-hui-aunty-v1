import React, { Component } from "react";
import { Grid, Card, Image, Button, Header } from "semantic-ui-react";
import CV1 from "../../../assets/img/ava_thumb.png";
import CV11 from "../../../assets/img/ava_full.png";
import CV2 from "../../../assets/img/abby_thumb.png";
import CV22 from "../../../assets/img/abby_full.png";
import CV3 from "../../../assets/img/gabby_thumb.png";
import CV33 from "../../../assets/img/gabby_full.png";
import { Link } from "react-router-dom";
import ResumeModal from "../ResumeModal";
import isLoggedOut from "../../../utils/env/isLoggedin";
import UserAccessModal from "../../ModalPageSection/UserAccessModal";

const images = [
  {
    src: CV11,
    slide: 1
  },
  {
    src: CV22,
    slide: 0
  },
  {
    src: CV33,
    slide: 2
  }
];

// const mImages = [CV22, CV11, CV33];

function getModalImage(id) {
  let ret;
  for (let i = 0; i < images.length; i++) {
    const element = images[i];
    if (element.slide === id) {
      return element;
    }
  }
}

export default class CvCard extends Component {
  state = {
    modalOpen: false,
    modalImage: {}
  };

  handleOpen = (e, { cvid }) => {
    this.setState({ modalOpen: true, modalImage: getModalImage(cvid) });
  };

  handleClose = () => this.setState({ modalOpen: false });

  onBtnClick = (e, { id }) => {};

  render() {
    const loggedOut = isLoggedOut();
    return (
      <div>
        <Grid>
          <Grid.Row className="resume-grid">
            <Grid.Column
              className="card-main-block"
              mobile={16}
              computer={5}
              largeScreen={5}
              widescreen={5}>
              <Card className="resume-images">
                <Image src={CV1} className="resume-card-image" />
                <Card.Content extra className="extra-resume-content">
                  <div className="resume-button-hover">
                    <Button
                      cvid={0}
                      className="resume-preview"
                      onClick={this.handleOpen}>
                      Preview
                    </Button>
                    {loggedOut ? (
                      <UserAccessModal
                        modalTrigger={
                          <Button className="resume-apply">Apply</Button>
                        }
                        isAction
                        actionText="create"
                        entity={""}
                      />
                    ) : (
                      <Button
                        className="resume-apply"
                        as={Link}
                        to="/cv-template/resume-professional">
                        Apply
                      </Button>
                    )}
                  </div>
                </Card.Content>
              </Card>
              <div className="card-header-resume">
                <Header as="h2">Professional</Header>
              </div>
            </Grid.Column>
            <Grid.Column
              className="card-main-block"
              mobile={16}
              computer={5}
              largeScreen={5}
              widescreen={5}>
              <Card className="resume-images">
                <Image src={CV2} className="resume-card-image" />
                <Card.Content extra className="extra-resume-content">
                  <div className="resume-button-hover">
                    <Button
                      cvid={1}
                      className="resume-preview"
                      onClick={this.handleOpen}>
                      Preview
                    </Button>

                    {loggedOut ? (
                      <UserAccessModal
                        modalTrigger={
                          <Button className="resume-apply">Apply</Button>
                        }
                        isAction
                        actionText="create"
                        entity={""}
                      />
                    ) : (
                      <Button
                        className="resume-apply"
                        as={Link}
                        to="/cv-template/resume-classic">
                        Apply
                      </Button>
                    )}
                  </div>
                </Card.Content>
              </Card>
              <div className="card-header-resume">
                <Header as="h2">Classsic</Header>
              </div>
            </Grid.Column>
            <Grid.Column
              className="card-main-block"
              mobile={16}
              computer={5}
              largeScreen={5}
              widescreen={5}>
              <div className="card-block">
                <Card className="resume-images">
                  <Image src={CV3} className="resume-card-image" />
                  <Card.Content extra className="extra-resume-content">
                    <div className="resume-button-hover">
                      <Button
                        cvid={2}
                        className="resume-preview"
                        onClick={this.handleOpen}>
                        Preview
                      </Button>

                      {loggedOut ? (
                        <UserAccessModal
                          modalTrigger={
                            <Button className="resume-apply">Apply</Button>
                          }
                          isAction
                          actionText="create"
                          entity={""}
                        />
                      ) : (
                        <Button
                          className="resume-apply"
                          as={Link}
                          to="/cv-template/resume-modern">
                          Apply
                        </Button>
                      )}
                    </div>
                  </Card.Content>
                </Card>
                <div className="card-header-resume">
                  <Header as="h2">Modern</Header>
                </div>
              </div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <ResumeModal
          loggedOut={loggedOut}
          handleClose={this.handleClose}
          handleOpen={this.state.modalOpen}
          images={images}
          currentImage={this.state.modalImage}
        />
      </div>
    );
  }
}
