import React, { Component } from "react";
import PageBanner from "../../Banners/PageBanner";
import manageCompany from "../../../assets/banners/manageCompany.jpg";
import { MANAGE_RESUME_TEMPLATE } from "../../../strings";
import {
  Header,
  Container,
  Card,
  Image,
  Grid,
  Button,
  Modal
} from "semantic-ui-react";
import "./index.scss";
import Cards from "./CvCard";
export default class CvTemplate extends Component {
  render() {
    return (
      <div className="manage-cv-template">
        <PageBanner compHack size="medium" image={manageCompany}>
          <Container>
            <div className="banner_header">
              <Header as="h1">{MANAGE_RESUME_TEMPLATE.HEADER_TITLE}</Header>
            </div>
          </Container>
        </PageBanner>
        <Container>
          <div className="manage_cv_template_header">
            <Cards />
          </div>
        </Container>
      </div>
    );
  }
}
