import React, { Component } from "react";
import { Button, Icon } from "semantic-ui-react";
// import EditButton from "../../CvEditAction";
import BorderButton from "../../../Buttons/BorderButton";
import { withRouter } from "react-router-dom";
class CvActionButtonDeskTop extends Component {
  render() {
    const { user, handleOpen } = this.props;
    return (
      <div className="resume-action-button">
        <div className="desktop-back-butto">
          <Icon name="angle left" className="back-icon" />
          <a
            href="javascript:;"
            className="go-back"
            onClick={e => this.props.history.go(-1)}>
            Go Back
          </a>
        </div>
        <div>
          {/* <Button user={user} className="pdf-download">
            Edit
          </Button> */}
          {/* <EditButton className="pdf-download" user={user} /> */}
        </div>
        <div>
          {/* <Button user={user} className="pdf-download" onClick={handleOpen}>
            Edit
          </Button>{" "} */}
          <BorderButton
            className="btn-border"
            btnText="Edit"
            onClick={handleOpen}
          />{" "}
          {/* <Button onClick={this.props.onDownloadClick} className="pdf-download">
            Download as PDF
          </Button> */}
          <BorderButton
            className="btn-border"
            btnText="Download as PDF"
            onClick={this.props.onDownloadClick}
          />
        </div>
      </div>
    );
  }
}

export default withRouter(CvActionButtonDeskTop);
