import React, { Component } from "react";
import "./index.scss";
import Desktop from "./CvActionButtonDesktop";
import Mobile from "./CvActionButtonMobile";
import { Responsive, Modal } from "semantic-ui-react";
import ScreenMainContainer from "../../ScreenMainContainer";
import EditProfilePage from "../../EditProfilePage";

export default class CvActionButton extends Component {
  constructor(props) {
    super(props);
    this.state = { modalOpen: false };
  }

  handleOpen = () => this.setState({ modalOpen: true });

  handleClose = () => this.setState({ modalOpen: false });

  render() {
    const { user } = this.props;
    return (
      <div style={{ marginTop: "20px" }}>
        <div className="resume-action-button-block">
          <Responsive maxWidth={1024}>
            <Mobile
              onDownloadClick={this.props.onDownloadClick}
              handleOpen={this.handleOpen}
              user={user}
            />
          </Responsive>
          <Responsive minWidth={1025}>
            <ScreenMainContainer>
              <Desktop
                onDownloadClick={this.props.onDownloadClick}
                handleOpen={this.handleOpen}
                user={user}
              />
            </ScreenMainContainer>
          </Responsive>
        </div>
        <Modal
          className="modal_form"
          onClose={this.handleClose}
          open={this.state.modalOpen}
          closeIcon>
          <EditProfilePage
            userData={user.fName && user.lName ? user : null}
            onCancelClick={this.handleClose}
            // activeSection={editModalActiveSection}
          />
        </Modal>
      </div>
    );
  }
}
