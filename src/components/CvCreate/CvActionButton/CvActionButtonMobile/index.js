import React, { Component } from "react";
import { Button, Icon } from "semantic-ui-react";
// import Download from "../../../../assets/svg/ResumeDownloadIcon";
// import Preview from "../../../../assets/svg/ResumePreviewIcon";
// import EditButton from "../../CvEditAction";

export default class CvActionButtonMobile extends Component {
  render() {
    const { user, handleOpen } = this.props;
    return (
      <div className="resume-action-button-block">
        <div className="resume-action-button">
          <div className="mobile-preview-butto">
            <Button className="pdf-download" onClick={handleOpen} user={user}>
              {/* <Preview
                width="12.03"
                height="12.031"
                viewBox="0 0 12.03 12.031"
              />{" "} */}
              <Icon name="pencil" />
              Edit
            </Button>
          </div>
          <div>
            <Button
              className="pdf-download"
              onClick={this.props.onDownloadClick}>
              {/* <Download /> */}
              <Icon name="download" />
              Download as PDF
            </Button>
          </div>
        </div>
      </div>
    );
  }
}
