import React, { Component } from "react";
import { Button, Header, Icon, Modal, Image } from "semantic-ui-react";
import Slider from "react-slick";
import "./index.scss";
import { Link } from "react-router-dom";
import UserAccessModal from "../../ModalPageSection/UserAccessModal";

function getCurrentUrl(id) {
  switch (id) {
    case 0:
      return "resume-professional";
    case 1:
      return "resume-classic";
    case 2:
      return "resume-modern";
    default:
      break;
  }
}

export default class ResumeModal extends Component {
  constructor(props) {
    super(props);
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.state = {
      activeSlide: null,
      url: ""
    };
  }

  next() {
    this.slider.slickNext();
  }
  previous() {
    this.slider.slickPrev();
  }

  componentDidMount() {
    const { currentImage } = this.props;
    this.setState({
      activeSlide: currentImage.slide,
      url: getCurrentUrl(currentImage.slide)
    });
  }

  render() {
    const { activeSlide } = this.state;
    console.log(activeSlide);
    const { images, currentImage, loggedOut } = this.props;
    const settings = {
      arrows: false,
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      afterChange: current =>
        this.setState({ activeSlide: current, url: getCurrentUrl(current) })
    };

    // let url =
    //   activeSlide == 0
    //     ? "resume-professional"
    //     : activeSlide == 1
    //     ? "resume-classic"
    //     : "resume-modern";
    return (
      <div>
        <Modal
          open={this.props.handleOpen}
          onClose={this.props.handleClose}
          basic
          closeIcon
          className="resume-action-button-modal"
          size="large">
          <Header content="Resume" className="resume-header-model" />
          <Modal.Content className="resume-modal">
            <Slider
              ref={c => (this.slider = c)}
              initialSlide={currentImage.slide}
              {...settings}>
              {images.map((img, index) => (
                <div key={index}>
                  <Image
                    src={img.src}
                    wrapped
                    size="medium"
                    className="modal-image"
                  />
                </div>
              ))}
            </Slider>
          </Modal.Content>
        </Modal>
        {this.props.handleOpen ? (
          <div className="modal-footer-resume">
            <Modal.Actions style={{ backgroundColor: "#fff" }}>
              <div className="resume-action-button-resume-modal">
                <div>
                  <a
                    href="javascript:;"
                    className="go-back"
                    onClick={this.previous}>
                    <Icon name="angle left" className="back-icon" /> Prev
                  </a>
                </div>
                <div>
                  {loggedOut ? (
                    <UserAccessModal
                      modalTrigger={
                        <Button className="resume-apply">Apply</Button>
                      }
                      isAction
                      actionText="create"
                      entity={""}
                    />
                  ) : (
                    <Button
                      className="pdf-download"
                      as={Link}
                      to={`/cv-template/${this.state.url}`}>
                      Apply this template
                    </Button>
                  )}
                </div>
                <div>
                  <a
                    href="javascript:;"
                    className="go-back"
                    onClick={this.next}>
                    Next <Icon name="angle right" className="back-icon" />
                  </a>
                </div>
              </div>
            </Modal.Actions>
          </div>
        ) : (
          <div />
        )}
      </div>
    );
  }
}
