import React, { Component } from "react";
import MainHeader from "./MainHeader";
export default class Header extends Component {
  render() {
    const { user, hideEditICon } = this.props;
    return (
      <div>
        <MainHeader user={user} hideEditICon={hideEditICon} />
      </div>
    );
  }
}
