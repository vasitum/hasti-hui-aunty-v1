import React, { Component } from "react";
import { Icon, Button, Responsive } from "semantic-ui-react";
import Header from "./Header";
import Main from "./Main";
import "./index.scss";
import BottomAction from "../CvActionButton";
import { USER } from "../../../constants/api";
import getUserData from "../../../api/user/getUserById";
import html2pdf from "html2pdf.js";
import MobileHeader from "../../MobileComponents/MobileHeader";
import IcFooterArrowIcon from "../../../assets/svg/IcFooterArrowIcon";

export default class CvFirst extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
      hideEditICon: true
    };
  }

  async componentDidMount() {
    const userId = window.localStorage.getItem(USER.UID);
    try {
      const res = await getUserData(userId);
      if (res.status === 200) {
        const data = await res.json();
        this.setState({
          user: data
        });
      }
    } catch (error) {
      console.log(error);
    }
  }

  onDownloadClick = e => {
    this.setState(
      {
        hideEditICon: true
      },
      () => {
        var opt = {
          filename: "MyResume.pdf",
          html2canvas: { windowWidth: "1336px" }
        };
        html2pdf()
          .set(opt)
          .from(document.getElementById("mypdfroot"))
          .save();
      }
    );
  };

  render() {
    const { user, hideEditICon } = this.state;
    if (!user) {
      return null;
    }
    return (
      <React.Fragment>
        <Responsive maxWidth={1024}>
          <div className="MyJobsDetailsMobile_header">
            <MobileHeader
              className="isMobileHeader_fixe"
              headerLeftIcon={
                <Button
                  onClick={e => this.props.history.go(-1)}
                  className="backArrowBtn">
                  <IcFooterArrowIcon pathcolor="#6e768a" />
                </Button>
              }
              headerTitle="Create Resume"
            />
          </div>
        </Responsive>
        <div
          className={`cv-template-first-block hide-icon ${
            hideEditICon ? "" : ""
          }`}>
          <div className="cv-template-first" id={"mypdfroot"}>
            <article className="resume-wrapper text-center position-relative">
              <div className="resume-wrapper-inner mx-auto">
                <div className="text-left bg-white shadow-lg">
                  <Header user={user} />
                  <Main user={user} />
                </div>
              </div>
            </article>
          </div>
          <BottomAction user={user} onDownloadClick={this.onDownloadClick} />
        </div>
      </React.Fragment>
    );
  }
}
