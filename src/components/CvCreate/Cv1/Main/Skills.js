import React, { Component } from "react";
import EditButton from "../../CvEditAction";
export default class Skills extends Component {
  render() {
    const { skills } = this.props;
    // console.log(skills);
    return (
      <React.Fragment>
        {skills["skills"].length > 0 ? (
          <div>
            <section className="resume-section skills-section mb-5 skills-edit-section">
              <h2 className="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">
                Skills &amp; Tools{" "}
                <span className="float-right">
                  <EditButton user={skills} />
                </span>
              </h2>
              <div className="resume-section-content">
                <div className="resume-skill-item">
                  <ul className="list-unstyled mb-4">
                    {skills["skills"].map((item, index) => {
                      let per;
                      if (item.exp) {
                        per = (item.exp * 100) / 10;
                      }
                      return (
                        <li className="mb-2" key={item._id}>
                          <div className="resume-skill-name">
                            {item.name ? item.name : ""}
                          </div>
                          {per ? (
                            <div className="progress resume-progress">
                              <div
                                className="progress-bar theme-progress-bar-dark"
                                role="progressbar"
                                style={{ width: per + "%" }}
                                aria-valuenow="25"
                                aria-valuemin="0"
                                aria-valuemax="100"
                              />
                            </div>
                          ) : (
                            <div />
                          )}
                        </li>
                      );
                    })}
                  </ul>
                </div>
              </div>
            </section>
          </div>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
