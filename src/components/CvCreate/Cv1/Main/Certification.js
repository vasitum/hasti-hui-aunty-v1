import React, { Component } from "react";
import EditButton from "../../CvEditAction";
export default class Language extends Component {
  render() {
    const { certification } = this.props;
    return (
      <React.Fragment>
        {certification["certs"] ? (
          certification["certs"][0].certificate_name ? (
            <div>
              <section className="resume-section education-section mb-5 certificate-edit-section">
                <h2 className="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">
                  Certifications{" "}
                  <span className="float-right">
                    <EditButton user={certification} />
                  </span>
                </h2>
                <div className="resume-section-content">
                  <ul className="list-unstyled">
                    {certification["certs"].map((item, index) => {
                      return (
                        <li className="mb-2" key={item._id}>
                          <div className="resume-degree font-weight-bold">
                            {item.certificate_name ? item.certificate_name : ""}
                          </div>
                          <div className="resume-degree-org">
                            {item.cert_Institute ? item.cert_Institute : ""}
                          </div>
                          <div className="resume-degree-time">
                            {item.receive_date ? item.receive_date : ""}
                          </div>
                        </li>
                      );
                    })}
                  </ul>
                </div>
              </section>
            </div>
          ) : (
            <div />
          )
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
