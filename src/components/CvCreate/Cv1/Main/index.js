import React, { Component } from "react";
import Summary from "./Summary";
import Experience from "./Experience";
import Skills from "./Skills";
import Educations from "./Education";
import Certification from "./Certification";
export default class Main extends Component {
  render() {
    const { user } = this.props;
    return (
      <div>
        <div className="resume-body p-5">
          <Summary summary={user} />
          <div className="row">
            <div className="col-lg-9">
              <Experience experience={user} />
            </div>
            <div className="col-lg-3">
              <Skills skills={user} />
              <Educations education={user} />
              <Certification certification={user} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
