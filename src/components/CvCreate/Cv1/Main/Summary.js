import React, { Component } from "react";
import EditButton from "../../CvEditAction";
import myFunction from "../../../../utils/env/fromHtmltoText";
export default class Summary extends Component {
  render() {
    const { summary } = this.props;
    return (
      <React.Fragment>
        {summary.desc ? (
          <div>
            <section className="resume-section summary-section mb-5 summary-edit-section">
              <h2 className="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">
                Career Summary{" "}
                <span className="float-right">
                  <EditButton user={summary} />
                </span>
              </h2>
              <div className="resume-section-content">
                {summary.desc ? (
                  <p
                    className="mb-0"
                    dangerouslySetInnerHTML={{ __html: summary.desc }}
                  />
                ) : null}
              </div>
            </section>
          </div>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
