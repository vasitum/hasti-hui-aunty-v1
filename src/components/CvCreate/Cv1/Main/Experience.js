import React, { Component } from "react";
import EditButton from "../../CvEditAction";
export default class Experience extends Component {
  render() {
    const { experience } = this.props;
    return (
      <React.Fragment>
        {experience["exps"] ? (
          experience["exps"][0].designation ? (
            <div>
              <section className="resume-section experience-section mb-5 experience-edit-section">
                <h2 className="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">
                  Work Experience{" "}
                  <span className="float-right">
                    <EditButton user={experience} />
                  </span>
                </h2>
                <div className="resume-section-content">
                  <div className="resume-timeline position-relative">
                    {experience["exps"].map((item, index) => {
                      return (
                        <article
                          className="resume-timeline-item position-relative pb-5"
                          key={item._id}>
                          <div className="resume-timeline-item-header mb-2">
                            <div className="d-flex flex-column flex-md-row">
                              <h3 className="resume-position-title font-weight-bold mb-1">
                                {item.designation ? item.designation : ""}
                              </h3>
                              <div className="resume-company-name ml-auto">
                                {item.name ? item.name : ""},{" "}
                                {item.loc ? item.loc : ""}
                              </div>
                            </div>
                            <div className="resume-position-time">
                              {item.doj.year ? item.doj.year : ""} -{" "}
                              {item.dor.year ? item.dor.year : "Present"}
                            </div>
                          </div>
                          <div className="resume-timeline-item-desc">
                            {/* <p>{item.desc ? dangerouslySetInnerHTML={{__html: thisIsMyCopy}} : null}</p> */}
                            {item.desc ? (
                              <p
                                dangerouslySetInnerHTML={{ __html: item.desc }}
                              />
                            ) : null}
                          </div>
                        </article>
                      );
                    })}
                  </div>
                </div>
              </section>
            </div>
          ) : (
            <div />
          )
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
