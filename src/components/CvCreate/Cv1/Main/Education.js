import React, { Component } from "react";
import EditButton from "../../CvEditAction";
export default class Education extends Component {
  render() {
    const { education } = this.props;
    return (
      <React.Fragment>
        {education["edus"] ? (
          education["edus"][0].course ? (
            <div>
              <section className="resume-section education-section mb-5 education-edit-section">
                <h2 className="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">
                  Education{" "}
                  <span className="float-right">
                    <EditButton user={education} />
                  </span>
                </h2>
                <div className="resume-section-content">
                  <ul className="list-unstyled">
                    {education["edus"].map((item, index) => {
                      return (
                        <li className="mb-2" key={item._id}>
                          <div className="resume-degree font-weight-bold">
                            {item.course ? item.course : ""}
                          </div>
                          <div className="resume-degree-org">
                            {item.institute ? item.institute : ""}
                          </div>
                          <div className="resume-degree-time">
                            {item.started ? item.started : ""} -{" "}
                            {item.completed ? item.completed : ""}
                          </div>
                        </li>
                      );
                    })}
                  </ul>
                </div>
              </section>
            </div>
          ) : (
            <div />
          )
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
