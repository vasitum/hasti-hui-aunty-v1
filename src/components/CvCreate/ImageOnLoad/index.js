import React, { Component } from "react";

export default class ImageOnLoad extends Component {
  state = {
    src: null
  };
  async componentDidMount() {
    const { src } = this.props;
    const $img = new Image();
    $img.crossOrigin = "anonymous";

    $img.onload = () => {
      const $ctx = document.createElement("canvas");
      $ctx.width = $img.width;
      $ctx.height = $img.height;
      const $ctxContext = $ctx.getContext("2d");
      $ctxContext.drawImage($img, 0, 0);
      this.setState({
        src: $ctx.toDataURL("image/png")
      });
    };
    $img.src = src + "?data";
  }
  render() {
    const { src: mSrc, ...otherProps } = this.props;
    const { src } = this.state;
    if (!src) {
      return null;
    }
    return <img src={src} {...otherProps} />;
  }
}
