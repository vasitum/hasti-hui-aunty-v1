import React, { Component } from "react";
import Summary from "./Summary";
import Experience from "./Experience";
import Skills from "./Skills";
import Educations from "./Educations";
import Certification from "./Certification";
export default class Main extends Component {
  render() {
    const { user } = this.props;
    return (
      <div>
        <div className="resume-body p-5">
          <Summary summary={user} />
          <Experience experience={user} />
          <Educations education={user} />
          <Skills skills={user} />
          <Certification certification={user} />
        </div>
      </div>
    );
  }
}
