import React, { Component } from "react";
import EditButton from "../../CvEditAction";
export default class Educations extends Component {
  render() {
    const { education } = this.props;
    return (
      <React.Fragment>
        {education["edus"] ? (
          education["edus"][0].course ? (
            <div>
              <div className="row education-edit-section">
                <div className="col-lg-3">
                  <section className="resume-section skills-section">
                    <h2 className="resume-section-title text-uppercase font-weight-bold pt-4 mt-4">
                      Education{" "}
                      <span className="float-right">
                        <EditButton user={education} />
                      </span>
                    </h2>
                  </section>
                </div>
                <div className="col-lg-9">
                  <section className="resume-section experience-section">
                    <h2 className="resume-section-title text-uppercase font-weight-bold pt-4 mt-4" />
                    <div className="resume-section-content">
                      {education["edus"].map((item, index) => {
                        return (
                          <article key={item._id}>
                            <div className="resume-timeline-item-header mb-2">
                              <div className="d-flex flex-column flex-md-row">
                                <h3 className="resume-position-title font-weight-bold mb-1">
                                  {item.course ? item.course : ""}
                                </h3>
                                <div className="resume-company-name ml-auto">
                                  {item.started ? item.started : ""} -{" "}
                                  {item.completed ? item.completed : ""}
                                </div>
                              </div>
                              <div className="resume-position-time">
                                {item.institute ? item.institute : ""}
                              </div>
                            </div>
                            <div className="resume-timeline-item-desc">
                              {/* <p>
                            Role description goes here ipsum dolor sit amet,
                            consectetuer adipiscing elit. Aenean commodo ligula
                            eget dolor. Aenean massa. Cum sociis natoque
                            penatibus et magnis dis parturient montes, nascetur
                            ridiculus mus. Donec quam felis, ultricies nec,
                            pellentesque eu, pretium quis, sem. Donec pede
                            justo, fringilla vel.
                          </p> */}
                            </div>
                          </article>
                        );
                      })}
                    </div>
                  </section>
                </div>
              </div>
            </div>
          ) : (
            <div />
          )
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
