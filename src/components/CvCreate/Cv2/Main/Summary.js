import React, { Component } from "react";
import EditButton from "../../CvEditAction";
import myFunction from "../../../../utils/env/fromHtmltoText";
export default class Summary extends Component {
  render() {
    const { summary } = this.props;
    return (
      <React.Fragment>
        {summary.desc ? (
          <div>
            <div className="row summary-edit-section">
              <div className="col-lg-3">
                <section className="resume-section skills-section ">
                  <h2 className="resume-section-title text-uppercase font-weight-bold pt-4 mt-4">
                    About{" "}
                    <span className="float-right">
                      <EditButton user={summary} />
                    </span>
                  </h2>
                </section>
              </div>
              <div className="col-lg-9">
                <section className="resume-section experience-section">
                  <h2 className="resume-section-title text-uppercase font-weight-bold pt-4 mt-4" />
                  <div className="resume-section-content">
                    <article>
                      <div className="resume-timeline-item-desc">
                        {summary.desc ? (
                          <p
                            dangerouslySetInnerHTML={{ __html: summary.desc }}
                          />
                        ) : null}
                      </div>
                    </article>
                  </div>
                </section>
              </div>
            </div>
          </div>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
