import React, { Component } from "react";
import EditButton from "../../CvEditAction";
export default class Skills extends Component {
  render() {
    const { skills } = this.props;
    return (
      <React.Fragment>
        {skills["skills"].length > 0 ? (
          <div>
            <div className="row skills-edit-section myskills" id="myskills">
              <div className="col-lg-3">
                <section className="resume-section skills-section">
                  <h2 className="resume-section-title text-uppercase font-weight-bold pt-4 mt-4">
                    Skills{" "}
                    <span className="float-right">
                      <EditButton user={skills} />
                    </span>
                  </h2>
                </section>
              </div>
              <div className="col-lg-9">
                <section className="resume-section experience-section">
                  <h2 className="resume-section-title text-uppercase font-weight-bold pt-4 mt-4" />
                  <div className="resume-section-content">
                    <article>
                      <ul className="list-unstyled mb-4">
                        <div className="row">
                          {skills["skills"].map((item, index) => {
                            let per;
                            if (item.exp) {
                              per = (item.exp * 100) / 10;
                            }
                            return (
                              <div className="col-lg-6">
                                <li className="mb-2">
                                  <div className="resume-skill-name">
                                    {item.name ? item.name : ""}
                                  </div>
                                  {per ? (
                                    <div className="progress resume-progress">
                                      <div
                                        className="progress-bar theme-progress-bar-dark"
                                        role="progressbar"
                                        style={{ width: per + "%" }}
                                        aria-valuenow="25"
                                        aria-valuemin="0"
                                        aria-valuemax="100"
                                      />
                                    </div>
                                  ) : (
                                    <div />
                                  )}
                                </li>
                              </div>
                            );
                          })}
                        </div>
                      </ul>
                    </article>
                  </div>
                </section>
              </div>
            </div>
          </div>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
