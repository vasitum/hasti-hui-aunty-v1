import React, { Component } from "react";
import EditButton from "../../CvEditAction";
export default class Online extends Component {
  render() {
    const { certification } = this.props;
    return (
      <React.Fragment>
        {certification["certs"] ? (
          certification["certs"][0].certificate_name ? (
            <div>
              <div className="row certificate-edit-section">
                <div className="col-lg-3">
                  <section className="resume-section skills-section">
                    <h2 className="resume-section-title text-uppercase font-weight-bold pt-4 mt-4">
                      Certification{" "}
                      <span className="float-right">
                        <EditButton user={certification} />
                      </span>
                    </h2>
                  </section>
                </div>
                <div className="col-lg-9">
                  <section className="resume-section experience-section">
                    <h2 className="resume-section-title text-uppercase font-weight-bold pt-4 mt-4" />
                    <div className="resume-section-content">
                      {certification["certs"].map((item, index) => {
                        return (
                          <article key={item._id}>
                            <div className="resume-timeline-item-header mb-2">
                              <div className="d-flex flex-column flex-md-row">
                                <h3 className="resume-position-title font-weight-bold mb-1">
                                  {item.certificate_name
                                    ? item.certificate_name
                                    : ""}
                                </h3>
                                <div className="resume-company-name ml-auto">
                                  {item.receive_date ? item.receive_date : ""}
                                </div>
                              </div>
                              {/* <div className="resume-position-time">Abc College</div> */}
                            </div>
                            <div className="resume-timeline-item-desc">
                              <p>
                                {item.cert_Institute ? item.cert_Institute : ""}
                              </p>
                            </div>
                          </article>
                        );
                      })}
                    </div>
                  </section>
                </div>
              </div>
            </div>
          ) : (
            <div />
          )
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
