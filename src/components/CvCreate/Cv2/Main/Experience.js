import React, { Component } from "react";
import EditButton from "../../CvEditAction";
import myFunction from "../../../../utils/env/fromHtmltoText";
export default class Experience extends Component {
  render() {
    const { experience } = this.props;
    return (
      <React.Fragment>
        {experience["exps"] ? (
          experience["exps"][0].designation ? (
            <div>
              <div className="row experience-edit-section">
                <div className="col-lg-3">
                  <section className="resume-section skills-section ">
                    <h2 className="resume-section-title text-uppercase font-weight-bold pt-4 mt-4">
                      Work{" "}
                      <span className="float-right">
                        <EditButton user={experience} />
                      </span>
                    </h2>
                  </section>
                </div>
                <div className="col-lg-9">
                  <section className="resume-section experience-section">
                    <h2 className="resume-section-title text-uppercase font-weight-bold pt-4 mt-4" />
                    <div className="resume-section-content">
                      {experience["exps"].map((item, index) => {
                        return (
                          <article className="resume-timeline-item position-relative pb-5">
                            <div className="resume-timeline-item-header mb-2">
                              <div className="d-flex flex-column flex-md-row">
                                <h3 className="resume-position-title font-weight-bold mb-1">
                                  {item.designation ? item.designation : ""}
                                </h3>
                                <div className="resume-company-name ml-auto">
                                  {item.name ? item.name : ""},{" "}
                                  {item.loc ? item.loc : ""}
                                </div>
                              </div>
                              <div className="resume-position-time">
                                {item.doj.year ? item.doj.year : ""} -{" "}
                                {item.dor.year ? item.dor.year : "Present"}
                              </div>
                            </div>
                            <div className="resume-timeline-item-desc">
                              {item.desc ? (
                                <p
                                  dangerouslySetInnerHTML={{
                                    __html: item.desc
                                  }}
                                />
                              ) : null}
                            </div>
                          </article>
                        );
                      })}
                    </div>
                  </section>
                </div>
              </div>
            </div>
          ) : (
            <div />
          )
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
