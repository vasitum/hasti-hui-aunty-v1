import React, { Component } from "react";
import { Icon } from "semantic-ui-react";
import EditButton from "../../CvEditAction";
import UserLogo from "../../../../assets/svg/IcUser";
import ImageOnLoad from "../../ImageOnLoad";
import { USER } from "../../../../constants/api";

export default class MainHeader extends Component {
  render() {
    const { user } = this.props;
    return (
      <div>
        <header className="resume-header pt-4 pt-md-0 p-5 edit-icon-show">
          <div className="media flex-column flex-md-row pt-4">
            {!user.ext || !user.ext.img ? (
              <React.Fragment>
                <div className="svg-middle">
                  <UserLogo
                    width="110"
                    height="110"
                    rectcolor="#f7f7fb"
                    pathcolor="#c8c8c8"
                  />
                </div>
              </React.Fragment>
            ) : (
              <ImageOnLoad
                src={`https://s3-us-west-2.amazonaws.com/img-mwf/${
                  user._id
                }/image.jpg`}
                className="mr-3 img-fluid picture mx-auto"
                alt="user-pic"
              />
            )}

            <div className="media-body p-4 d-flex flex-column flex-md-row mx-auto mx-lg-0">
              <div className="primary-info">
                <h1 className="name mt-0 mb-1 text-uppercase text-uppercase">
                  {user.fullName}
                </h1>
                <div className="title mb-3">{user.title}</div>
                <ul className="list-unstyled">
                  {user.email ? (
                    <li className="mb-3-new trucate">
                      <a href="#">
                        <Icon className="mr-2" name="envelope outline" />
                        {user.email}
                      </a>
                    </li>
                  ) : (
                    <li />
                  )}
                  {user.countryCode || user.mobile ? (
                    <li className="mb-3-new trucate">
                      <a href="#">
                        <Icon className="mr-2" name="mobile alternate" />
                        {user.countryCode + "-" + user.mobile}
                      </a>
                    </li>
                  ) : (
                    <li />
                  )}

                  {user.loc && user.loc.adrs ? (
                    <li className="mb-3-new trucate">
                      <a href="#">
                        <Icon className="mr-2" name="map marker alternate" />
                        {user.loc.adrs}
                      </a>
                    </li>
                  ) : (
                    <li />
                  )}

                  <li className="mb-3-new trucate user-profile-link">
                    <a
                      href={`/view/user/${window.localStorage.getItem(
                        USER.UID
                      )}`}>
                      <Icon className="mr-2" name="globe" />
                      {window.location.host +
                        "/view/user/" +
                        window.localStorage.getItem(USER.UID)}
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="p-4">
              <EditButton user={user} />
            </div>
          </div>
        </header>
      </div>
    );
  }
}
