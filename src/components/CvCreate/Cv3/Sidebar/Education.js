import React, { Component } from "react";
import EditButton from "../../CvEditAction";
export default class Education extends Component {
  render() {
    const { education } = this.props;
    return (
      <React.Fragment>
        {education["edus"] ? (
          education["edus"][0].course ? (
            <div>
              <div className="education-container container-block education-edit-section">
                <div className="title-header">
                  <div>
                    <h2 className="container-block-title">Education</h2>
                  </div>
                  <div>
                    <EditButton user={education} />
                  </div>
                </div>
                {education["edus"].map((item, index) => {
                  return (
                    <div className="item" key={item._id}>
                      <h4 className="degree">
                        {item.course ? item.course : ""}
                      </h4>
                      <h5 className="meta">
                        {item.institute ? item.institute : ""}
                      </h5>
                      <div className="time">
                        {item.started ? item.started : ""} -{" "}
                        {item.completed ? item.completed : ""}
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          ) : (
            <div />
          )
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
