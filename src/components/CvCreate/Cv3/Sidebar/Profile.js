import React, { Component } from "react";
import { Icon } from "semantic-ui-react";
import EditButton from "../../CvEditAction";
import UserLogo from "../../../../assets/svg/IcUser";
import ImageOnLoad from "../../ImageOnLoad";
import { USER } from "../../../../constants/api";
export default class Profile extends Component {
  render() {
    const { user } = this.props;
    return (
      <div className="profile-edit-show">
        <div className="profile-container">
          {!user.ext || !user.ext.img ? (
            <React.Fragment>
              <UserLogo
                width="110"
                height="110"
                rectcolor="#f7f7fb"
                pathcolor="#c8c8c8"
              />
            </React.Fragment>
          ) : (
            <ImageOnLoad
              src={`https://s3-us-west-2.amazonaws.com/img-mwf/${
                user._id
              }/image.jpg`}
              className="mr-3 img-fluid picture mx-auto cv-3-image"
              alt="user-pic"
            />
          )}
          <div className="edit-show-cv-3-image">
            <EditButton user={user} />
          </div>
          <h1 className="name">{user.fullName}</h1>
          <h3 className="tagline">{user.title}</h3>
        </div>

        <div className="contact-container container-block">
          <ul className="list-unstyled contact-list">
            {user.email ? (
              <li className="email trucate">
                <Icon name="envelope outline" />
                <a href="#" title={user.email}>
                  {user.email}
                </a>
              </li>
            ) : (
              <li />
            )}
            {user.countryCode || user.mobile ? (
              <li className="phone trucate">
                <Icon name="mobile alternate" />
                <a href="#" title={user.mobile}>
                  {user.countryCode + "-" + user.mobile}
                </a>
              </li>
            ) : (
              <li />
            )}
            {user.loc.adrs ? (
              <li className="website trucate">
                <Icon name="map marker alternate" />
                <a href="#" title={user.loc.adrs}>
                  {user.loc.adrs}
                </a>
              </li>
            ) : (
              <li />
            )}
            <li className="website trucate">
              <Icon name="globe" />
              <a
                href={`/view/user/${window.localStorage.getItem(USER.UID)}`}
                title={`${
                  window.location.host
                }/view/user/${window.localStorage.getItem(USER.UID)}`}>
                {" "}
                {window.location.host +
                  "/view/user/" +
                  window.localStorage.getItem(USER.UID)}
              </a>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}
