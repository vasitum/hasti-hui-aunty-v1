import React, { Component } from "react";
import EditButton from "../../CvEditAction";
export default class Language extends Component {
  render() {
    const { certification } = this.props;
    return (
      <React.Fragment>
        {certification["certs"] ? (
          certification["certs"][0].certificate_name ? (
            <div>
              <div className="education-container container-block certificate-edit-section">
                <div className="title-header">
                  <div>
                    <h2 className="container-block-title">Certification</h2>
                  </div>
                  <div>
                    <EditButton user={certification} />
                  </div>
                </div>
                {certification["certs"].map((item, index) => {
                  return (
                    <div className="item" key={item._id}>
                      <h4 className="degree">
                        {item.certificate_name ? item.certificate_name : ""}
                      </h4>
                      <h5 className="meta">
                        {item.cert_Institute ? item.cert_Institute : ""}
                      </h5>
                      <div className="time">
                        {item.receive_date ? item.receive_date : ""}
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          ) : (
            <div />
          )
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
