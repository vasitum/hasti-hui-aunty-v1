import React, { Component } from "react";
import Profile from "./Profile";
import Education from "./Education";
import Certication from "./Certication";
export default class Sidebar extends Component {
  render() {
    const { user } = this.props;
    return (
      <div>
        <div className="sidebar-wrapper">
          <Profile user={user} />
          <Education education={user} />
          <Certication certification={user} />
        </div>
      </div>
    );
  }
}
