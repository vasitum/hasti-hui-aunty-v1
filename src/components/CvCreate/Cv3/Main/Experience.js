import React, { Component } from "react";
import EditButton from "../../CvEditAction";
import myFunction from "../../../../utils/env/fromHtmltoText";
import { Icon } from "semantic-ui-react";

export default class Experience extends Component {
  render() {
    const { experience } = this.props;
    return (
      <React.Fragment>
        {experience["exps"] ? (
          experience["exps"][0].designation ? (
            <div>
              <section className="section experiences-section experience-edit-section">
                <div className="title-header">
                  <div>
                    <h2 className="section-title">
                      <span className="icon-holder">
                        <Icon name="rocket" className="icon-cv3" />
                      </span>
                      Experiences
                    </h2>
                  </div>
                  <div>
                    <EditButton user={experience} />
                  </div>
                </div>
                {experience["exps"].map((item, index) => {
                  return (
                    <div className="item" key={item._id}>
                      <div className="meta">
                        <div className="upper-row">
                          <h3 className="job-title">
                            {item.designation ? item.designation : ""}
                          </h3>
                          <div className="time">
                            {item.doj.year ? item.doj.year : ""} -{" "}
                            {item.dor.year ? item.dor.year : "Present"}
                          </div>
                        </div>
                        <div className="company">
                          {item.name ? item.name : ""},{" "}
                          {item.loc ? item.loc : ""}
                        </div>
                      </div>
                      <div className="details">
                        {item.desc ? (
                          <p dangerouslySetInnerHTML={{ __html: item.desc }} />
                        ) : null}
                      </div>
                    </div>
                  );
                })}
              </section>
            </div>
          ) : (
            <div />
          )
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
