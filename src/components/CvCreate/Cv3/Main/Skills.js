import React, { Component } from "react";
import EditButton from "../../CvEditAction";
import { Icon } from "semantic-ui-react";

export default class Skills extends Component {
  render() {
    const { skills } = this.props;
    return (
      <React.Fragment>
        {skills["skills"].length > 0 ? (
          <div>
            <section className="skills-section section skills-edit-section">
              <div className="title-header">
                <div>
                  <h2 className="section-title">
                    <span className="icon-holder">
                      <Icon name="briefcase" className="icon-cv3" />
                    </span>
                    Skills &amp; Proficiency
                  </h2>
                </div>
                <div>
                  <EditButton user={skills} />
                </div>
              </div>
              <div className="skillset">
                {skills["skills"].map((item, index) => {
                  let per;
                  if (item.exp) {
                    per = (item.exp * 100) / 10;
                  }
                  // console.log(per);
                  return (
                    <div className="item" key={item._id}>
                      <h3 className="level-title">
                        {item.name ? item.name : ""}
                      </h3>
                      {per ? (
                        <div className="progress level-bar">
                          <div
                            className="progress-bar theme-progress-bar"
                            role="progressbar"
                            style={{ width: per + "%" }}
                            aria-valuenow="99"
                            aria-valuemin="0"
                            aria-valuemax="100"
                          />
                        </div>
                      ) : (
                        <div />
                      )}
                    </div>
                  );
                })}
              </div>
            </section>
          </div>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
