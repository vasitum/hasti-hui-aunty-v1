import React, { Component } from "react";
import EditButton from "../../CvEditAction";
import myFunction from "../../../../utils/env/fromHtmltoText";
import { Icon } from "semantic-ui-react";

export default class Summary extends Component {
  render() {
    const { summary } = this.props;
    return (
      <React.Fragment>
        {summary.desc ? (
          <div>
            <section className="section summary-section summary-edit-section">
              <div className="title-header">
                <div>
                  <h2 className="section-title">
                    <span className="icon-holder">
                      <Icon name="user" className="icon-cv3" />
                    </span>
                    Summary
                  </h2>
                </div>
                <div>
                  <EditButton user={summary} />
                </div>
              </div>
              <div className="summary">
                {/* <p>{summary.desc ? myFunction(summary.desc) : <p />}</p> */}
                {summary.desc ? (
                  <p dangerouslySetInnerHTML={{ __html: summary.desc }} />
                ) : null}
              </div>
            </section>
          </div>
        ) : (
          <div />
        )}
      </React.Fragment>
    );
  }
}
