import React, { Component } from "react";
import Summary from "./Summary";
import Experience from "./Experience";
import Skills from "./Skills";
export default class Main extends Component {
  render() {
    const { user } = this.props;
    return (
      <div>
        <div className="main-wrapper">
          <Summary summary={user} />
          <Experience experience={user} />
          <Skills skills={user} />
        </div>
      </div>
    );
  }
}
