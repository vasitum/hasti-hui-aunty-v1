import React, { Component } from "react";
// import FirstCv from "./Cv1";
// import SecondCv from "./Cv2";
// import ThirdCv from "./Cv3";
// import FourthCv from "./Cv4";
import CvTemplate from "./CvTemplate";
import { USER } from "../../constants/api";
import getUserData from "../../api/user/getUserById";
import html2pdf from "html2pdf.js";
import { Helmet } from "react-helmet";
export default class CvCreate extends Component {
  constructor(props) {
    super(props);
    // this.state = {
    //   user: null
    // };
  }

  // async componentDidMount() {
  //   const userId = window.localStorage.getItem(USER.UID);
  //   try {
  //     const res = await getUserData(userId);
  //     if (res.status === 200) {
  //       const data = await res.json();
  //       this.setState({
  //         user: data
  //       });
  //     }
  //   } catch (error) {
  //     console.log(error);
  //   }
  // }

  // onDownloadClick = e => {
  //   var opt = {
  //     filename: "myfile.pdf"
  //   };
  //   html2pdf()
  //     .set(opt)
  //     .from(document.getElementById("mypdfroot"))
  //     .save();
  // };

  render() {
    // const { user } = this.state;
    // if (!user) {
    //   return null;
    // }
    // console.log(user);
    return (
      <React.Fragment>
        <Helmet>
          <meta charSet="utf-8" />
          <title>
            Free Resume Templates |Resume Creator | Resume Cover Letter| Vasitum
          </title>
          <meta
            name="title"
            content="Free Resume Templates |Resume Creator | Resume Cover Letter| Vasitum "
          />
          <meta
            name="twitter:title"
            content="Free Resume Templates |Resume Creator | Resume Cover Letter| Vasitum "
          />
          <meta
            property="og:title"
            content="Free Resume Templates |Resume Creator | Resume Cover Letter| Vasitum "
          />
          <meta
            property="og:description"
            content={`Online free resume creator, Resume Builder, Cover Letter Writing Services for Fresher’s and Mid-level professionals on Vasitum’s AI Recruiting Platform for Free.  Visit website now for more!.`}
          />
          <meta
            property="twitter:description"
            content={`Online free resume creator, Resume Builder, Cover Letter Writing Services for Fresher’s and Mid-level professionals on Vasitum’s AI Recruiting Platform for Free.  Visit website now for more!.`}
          />
          <link rel="canonical" href="https://vasitum.com/cv-template" />
        </Helmet>
        {/* <CvTemplate user={user} onDownloadClick={this.onDownloadClick} /> */}
        <CvTemplate />
      </React.Fragment>
    );
  }
}
