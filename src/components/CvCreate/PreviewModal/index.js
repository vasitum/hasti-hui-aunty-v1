import React, { Component } from "react";
import CV3 from "../../../assets/img/cv33.png";
import { Button, Header, Icon, Modal, Image } from "semantic-ui-react";
import "./index.scss";
import html2pdf from "html2pdf.js";

function Pdf() {
  var opt = {
    filename: "myfile.pdf"
  };
  return;
}

class PreviewImage extends React.Component {
  state = {
    src: ""
  };

  async componentDidMount() {
    html2pdf()
      .from(document.getElementById("mypdfroot"))
      .output("img")
      .outputImg("datauristring")
      .then(output => {
        this.setState({ src: output });
      })
      .error(err => console.error(err));
  }

  render() {
    const { src } = this.state;

    if (!src) {
      return null;
    }
    return <img src={src} />;
  }
}

export default class PreviewModal extends Component {
  // onDownloadClick = e => {
  //   var opt = {
  //     filename: "myfile.pdf"
  //   };

  // };

  render() {
    return (
      <Modal
        open={this.props.handleOpen}
        onClose={this.props.handleClose}
        basic
        closeIcon
        className="resume-action-button-modal-preview"
        size="fullscreen">
        <Modal.Content>
          {/* <Image src={CV3} wrapped size="medium" className="modal-image" /> */}
          <PreviewImage />
          {/* <Pdf /> */}
        </Modal.Content>
      </Modal>
    );
  }
}
