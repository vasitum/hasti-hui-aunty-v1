import React from "react";
import getJobByIdWithDetails from "../../api/jobs/getJobByIdWithDetails";
import getJobById from "../../api/jobs/getJobById";

import JobConstants from "../../constants/storage/jobs";

function withPostedJob(WrappedComponent, { connected }, isPublic) {
  return class extends React.Component {
    state = {
      data: {}
    };

    async componentDidMount() {
      let jobId;
      if (connected) {
        const { id } = this.props.match.params;
        jobId = id;
      } else {
        const { id } = this.props;
        jobId = id;
      }

      try {
        let res;

        if (isPublic) {
          res = await getJobById(jobId);
        } else {
          res = await getJobByIdWithDetails(jobId);
        }

        if (res.status === 200) {
          const data = await res.json();
          this.setState({
            data: data
          });

          /**
           * @Set
           * Set user data
           */
          window.localStorage.setItem(
            JobConstants.JOB_EDIT_DATA,
            JSON.stringify(data)
          );
        } else {
          console.error(res);
        }
      } catch (error) {
        console.log(error);
      }
    }

    render() {
      const { data } = this.state;

      if (!data) {
        return <div>loading ...</div>;
      }

      return <WrappedComponent {...this.props} data={data} jobData={data} />;
    }
  };
}

export default withPostedJob;
