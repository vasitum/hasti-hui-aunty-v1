import React from "react";
import getUserById from "../../api/user/getUserById";
function withProfile(WrappedComponent, { connected }) {
  return class extends React.Component {
    state = {
      data: null
    };

    async componentDidMount() {
      let id;
      if (connected) {
        id = this.props.match.params.id;
      } else {
        id = this.props.id;
      }

      try {
        const res = await getUserById(id);
        if (res.status === 200) {
          const data = await res.json();
          // console.log(data);
          this.setState({ data: data });
        }
      } catch (error) {
        console.error(error);
      }
    }

    render() {
      return <WrappedComponent {...this.props} data={this.state.data} />;
    }
  };
}

export default withProfile;
