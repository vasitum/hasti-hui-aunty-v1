import React from "react";
import getApplicationById from "../../api/jobs/getApplicationByIdAndChangeStaus";

function withApplications(WrappedComponent, { connected }) {
  return class extends React.Component {
    state = {
      data: null
    };

    async componentDidMount() {
      let jobId, applicationId;

      // console.log(this.props);

      if (connected) {
        jobId = this.props.match.params.jobId;
        applicationId = this.props.match.params.applicationId;
      } else {
        jobId = this.props.jobId;
        applicationId = this.props.applicationId;
      }

      try {
        const res = await getApplicationById(jobId, applicationId);
        if (res.status === 200) {
          const data = await res.json();
          // console.log(data);
          this.setState({ data: data });
        } else {
          console.error(res);
        }
      } catch (error) {
        console.error(error);
      }
    }

    render() {
      const { data } = this.state;

      if (!data) {
        return <div>loading ...</div>;
      } else {
        return (
          <WrappedComponent
            {...this.props}
            data={data}
            id={connected ? this.props.match.params.jobId : this.props.id}
            applData={data}
          />
        );
      }
    }
  };
}

export default withApplications;
