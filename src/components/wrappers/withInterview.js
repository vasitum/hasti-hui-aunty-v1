import React from "react";
import getAllUserInterviews from "../../api/user/getAllUserInterviews";

function withaAllInterviews(WrappedComponent) {
  return class extends React.Component {
    state = {
      data: null
    };

    async componentDidMount() {
      try {
        const res = await getAllUserInterviews();
        if (res.status === 200) {
          const data = await res.json();
          this.setState({ data: data });
          // console.log(data);
        } else {
          console.error(res);
        }
      } catch (error) {
        console.error(error);
      }
    }

    render() {
      const { data } = this.state;

      if (!data) {
        return <div>loading ...</div>;
      } else {
        return <WrappedComponent {...this.props} data={data} />;
      }
    }
  };
}

export default withaAllInterviews;
