import React from "react";

import { Grid, Header } from "semantic-ui-react";
import PropTypes from "prop-types";
import FileuploadBtn from "../../Buttons/FileuploadBtn";
import InputFieldLabel from "../../Forms/FormFields/InputFieldLabel";
import IcInfoIcon from "../../../assets/svg/IcInfo";

import Progressbar from "../../Progressbar";
import IcUploadIcon from "../../../assets/svg/IcUpload";

import InfoIconLabel from "../../utils/InfoIconLabel";
import RemoveBtn from "../../Buttons/RemoveBtn";
import IcRemove from "../../../assets/svg/IcRemove";

import { CREATE_PROFILE } from "../../../strings";

import "./index.scss";
const infoicon = <IcInfoIcon pathcolor="#c8c8c8" />;

const CreateProfileBannerHead = props => {
  return (
    <div className="CreateProfileBannerHead">
      <Header as="h1" className="CreateProfileBannerHead_header">
        {CREATE_PROFILE.PAGE_TITLE}
      </Header>
      <Grid>
        <Grid.Row>
          <Grid.Column mobile={16} computer={6}>
            <Progressbar counter={props.progressCounter} />
          </Grid.Column>
          <Grid.Column
            mobile={16}
            computer={10}
            textAlign="right"
            className="mobileGrid_bannerHeader">
            {props.bannerImageFile && (
              <RemoveBtn
                onClick={props.onFileRemove}
                btnText="Remove"
                className="fileuploadFlatBtn"
                style={{ marginRight: "10px" }}
                onFileUpload={props.onFileChange}
                accept=".jpg, .jpeg, .png"
                btnIcon={<IcRemove pathcolor="#c8c8c8" />}
              />
            )}
            <FileuploadBtn
              className="fileuploadFlatBtn"
              btnText={
                props.bannerImageFile
                  ? "Update"
                  : "Upload cover image"
              }
              onFileUpload={props.onFileChange}
              accept=".jpg, .jpeg, .png"
              btnIcon={<IcUploadIcon pathcolor="#c8c8c8" />}
            />
            <div className="file_specLabel">
              <InputFieldLabel
                className="Infoicon"
                label="File specifications"
                infoicon={<InfoIconLabel text="Accepts uploads of .jpg, .gif,.png file types (Maximum 2MB)"/>}
              />
            </div>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
};

CreateProfileBannerHead.propTypes = {};

export default CreateProfileBannerHead;
