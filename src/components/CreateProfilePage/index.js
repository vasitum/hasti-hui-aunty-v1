import React from "react";

import {
  Container,
  Grid,
  Button,
  Header,
  Popup,
  Icon
} from "semantic-ui-react";
import { Form } from "formsy-semantic-ui-react";
import PageBanner from "../Banners/PageBanner";
import aunty from "../../assets/banners/aunty.png";

import CreateProfileBannerHead from "./CreateProfileBannerHead";
import { geocodeByAddress, getLatLng } from "react-places-autocomplete";

import IcInfoIcon from "../../assets/svg/IcInfo";
import IcLinkedinIcon from "../../assets/svg/IcLinkedin";

import IcDownArrowIcon from "../../assets/svg/IcDownArrow";
import IcProfileViewIcon from "../../assets/svg/IcProfileView";

import FlatDefaultBtn from "../Buttons/FlatDefaultBtn";

import ActionBtn from "../Buttons/ActionBtn";
import PersonalDetails from "./PersonalDetails";
import ProfileDetails from "./ProfileDetails";
import Education from "./Education";
import WorkExperience from "./WorkExperience";
import Skills from "./Skills";
import DesireJobs from "./DesireJobs";
import Certificate from "./Certificate";

import InfoIconLabel from "../utils/InfoIconLabel";

// import LinkedIn from "react-linkedin-sdk";
import ReactLinkedIn from "react-linkedin-login-oauth2";

import "./index.scss";
import { toast } from "react-toastify";

import createProfileOnServer from "../../api/user/createProfile";
import isAuthenticated from "../../utils/env/isLoggedin";
import { Redirect, withRouter } from "react-router-dom";
import { USER } from "../../constants/api";
import UserConstants from "../../constants/storage/user";
import { uploadImage, uploadResume } from "../../utils/aws";

import PageLoader from "../PageLoader";
import uriToBlob from "../../utils/env/uriToBlob";

import locParser from "../../utils/env/locParser";

import ProfileImagePicker from "../Pickers/ProfileImagePicker";

import { withUserContextConsumer } from "../../contexts/UserContext";

import enableGA from "../../utils/env/enableGa";

import ReactGA from "react-ga";
import { CreateProfileString } from "../EventStrings/CreateProfile";

import { CREATE_PROFILE } from "../../strings";
import getLinkedInData from "../../api/jobs/getLinkedInData";

const infoicon = <IcInfoIcon pathcolor="#c8c8c8" />;

function getMonth(month) {
  const months = {
    1: "January",
    2: "February",
    3: "March",
    4: "April",
    5: "May",
    6: "June",
    7: "July",
    8: "August",
    9: "September",
    10: "October",
    11: "November",
    12: "December"
  };

  month = Number(month);

  if (!month || month > 12 || month < 1) {
    return "";
  }

  return months[month];
}

function getProcessedSkills(value, skills) {
  if (skills.length === value.length) {
    return skills;
  }

  // when user remvoes a value
  if (skills.length > value.length) {
    return skills.filter((val, idx) => {
      return value.indexOf(val.name) !== -1;
    });
  }

  if (skills.length < value.length) {
    return skills.concat([
      {
        exp: 0,
        name: value[value.length - 1]
      }
    ]);
  }
}

function parseUserExp(data) {
  if (!data.positions) {
    return false;
  }

  if (!data.positions.values || !Array.isArray(data.positions.values)) {
    return false;
  }

  return data.positions.values.map((val, idx) => {
    return {
      current: val.isCurrent,
      desc: "",
      designation: val.title,
      industry: "",
      doj: {
        month: getMonth(val.startDate.month),
        year: val.startDate.year
      },
      dor: {
        month: "",
        year: ""
      },
      loc: val.location.name,
      name: val.company.name,
      noticePeriod: 0,
      orgEmail: "",
      orgPhone: "",
      onInfo: idx === 0 ? true : false
    };
  });
}

class CreateProfilePage extends React.Component {
  state = {
    _id: window.localStorage.getItem(USER.UID),
    certs: [
      {
        cert_Institute: "",
        certificate_name: "",
        is_valid: true,
        receive_date: ""
      }
    ],
    countryCode: "+91",
    desc: "",
    desireJob: {
      avail: "",
      currentCtc: "",
      empType: "",
      expectedCtc: "",
      funArea: "",
      industry: "",
      role: "",
      spec: "",
      currency: "INR"
    },
    dob: 0,
    edus: [
      {
        completed: 0,
        course: "",
        institute: "",
        level: "",
        specs: [""],
        started: 0,
        type: "",
        onInfo: true
      }
    ],
    email: window.localStorage.getItem(USER.EMAIL),
    exps: [
      {
        current: true,
        desc: "",
        designation: "",
        industry: "",
        doj: {
          month: "",
          year: ""
        },
        dor: {
          month: "",
          year: ""
        },
        loc: "",
        name: "",
        noticePeriod: 0,
        orgEmail: "",
        orgPhone: "",
        onInfo: true
      }
    ],
    ext: {
      img: "",
      resume: "",
      imgBanner: ""
    },
    fName: "",
    gen: "",
    isVerified: false,
    lName: "",
    loc: {
      adrs: "",
      city: "",
      country: "",
      lat: 0,
      lon: 0,
      street: "",
      zipcode: ""
    },
    mobile: "",
    skills: [],
    skillFieldOptions: [],
    skillFieldCurrentValues: [],
    skillShowEditBtn: true,
    profileImage: null,
    profileImageFile: "",
    resumeDoc: null,
    resumeDocFile: "",
    resumeDocDataFile: "",
    title: "",
    bannerImage: aunty,
    bannerImageFile: "",
    counter: 5,
    canSubmit: false,
    isProfileLoading: false,
    loaderText: "Creating Profile ...",
    imagePickerOpen: false
  };

  constructor(props) {
    super(props);
    this.onProfileSubmit = this.onProfileSubmit.bind(this);
    this.onLinkedInSuccess = this.onLinkedInSuccess.bind(this);
  }

  componentDidMount() {
    ReactGA.pageview('/user/new');
    try {
      const profileData = window.localStorage.getItem(
        UserConstants.USER_PREVIEW_STATE
      );
      const profileDataJson = JSON.parse(profileData);

      this.setState(state => profileDataJson);
      window.localStorage.removeItem(UserConstants.USER_PREVIEW_STATE);
    } catch (error) {
      console.error(error);
    }
  }

  onInputChange = (e, { value, name }) => {
    this.setState({
      [name]: value
    });
  };

  async onLinkedInSuccess(data) {
    try {
      const res = await getLinkedInData({
        code: data.code,
        redirectUrl: "https://vasitum.com/create/profile/linkedin"
      });

      if (res.status === 200) {
        const data = await res.json();
        this.setState({
          fName: data.firstName,
          lName: data.lastName,
          title: data.headline,
          desc: data.summary
        });

        const parsedExps = parseUserExp(data);
        if (parsedExps) {
          this.setState({
            exps: parsedExps
          });
        }
      }
    } catch (error) {
      console.error(error);
    }
  }

  onLinkedInError(err) {
    alert("error");
  }

  onLinkedInCallback = res => {
    alert("Callback Called");
  };

  onLocSelect = address => {
    geocodeByAddress(address)
      .then(results => {
        const addrDetails = locParser(results[0].address_components, address);

        if (typeof addrDetails === "string") {
          this.setState({
            loc: {
              ...this.state.loc,
              city: addrDetails,
              adrs: address
            }
          });
        } else {
          this.setState({
            loc: {
              ...this.state.loc,
              city: addrDetails.primary,
              country: addrDetails.country,
              adrs: address
            }
          });
        }

        return getLatLng(results[0]);
      })
      .then(latLng => {
        this.setState({
          loc: {
            ...this.state.loc,
            lat: latLng.lat,
            lon: latLng.lng
          }
        });
      })
      .catch(error => console.error("Error", error));
  };

  onLocChange = address => {
    this.setState({
      loc: {
        ...this.state.loc,
        adrs: address
      }
    });
  };

  onProfileSummaryChange = html => {
    this.setState({
      desc: html
    });
  };

  onImageChange = e => {
    const file = e.target.files[0];

    // Reset Value
    e.target.value = "";

    if (!file) {
      return;
    }

    const EXTNS = ["png", "jpeg", "jpg", "gif", "tiff", "bmp"];
    const extn = file.name.split(".").pop();
    if (!extn || EXTNS.indexOf(String(extn).toLowerCase()) === -1) {
      return;
    }

    const fr = new FileReader();

    fr.onload = e => {
      this.setState({
        ext: {
          ...this.state.ext,
          img: extn
        },

        profileImageFile: file,
        profileImage: e.target.result,
        imagePickerOpen: !this.state.imagePickerOpen
      });
    };

    fr.readAsDataURL(file);
  };

  onImageRemove = e => {
    this.setState({
      ext: {
        ...this.state.ext,
        img: ""
      },

      profileImageFile: "",
      profileImage: ""
    });
  };

  onResumeChange = e => {
    const file = e.target.files[0];
    // Reset Value
    e.target.value = "";

    if (!file) {
      return;
    }

    const EXTNS = ["doc", "docx", "rtf", "pdf", "jpeg", "jpg"];
    const extn = file.name.split(".").pop();
    if (!extn || EXTNS.indexOf(String(extn).toLowerCase()) === -1) {
      return;
    }

    const fr = new FileReader();

    fr.onload = e => {
      this.setState({
        ext: {
          ...this.state.ext,
          resume: extn
        },
        resumeDocFile: file,
        resumeDoc: file.name,
        resumeDocDataFile: e.target.result
      });
    };

    fr.readAsDataURL(file);
  };

  onResumeRemove = e => {
    this.setState({
      ext: {
        ...this.state.ext,
        resume: ""
      },
      resumeDocFile: "",
      resumeDoc: "",
      resumeDocDataFile: ""
    });
  };

  onAddMoreEducation = ev => {
    this.setState({
      edus: [
        ...this.state.edus,
        {
          completed: 0,
          course: "",
          institute: "",
          level: "",
          specs: [""],
          started: 0,
          type: "",
          onInfo: false
        }
      ]
    });
  };

  onRemoveEducationClick = idx => {
    this.setState({
      edus: this.state.edus.filter((val, index) => {
        return index !== idx;
      })
    });
  };

  onAddMoreWorkExpClick = ev => {
    this.setState({
      exps: [
        ...this.state.exps,
        {
          current: false,
          desc: "",
          designation: "",
          industry: "",
          doj: {
            month: "",
            year: ""
          },
          dor: {
            month: "",
            year: ""
          },
          loc: "",
          name: "",
          noticePeriod: 0,
          orgEmail: "",
          orgPhone: "",
          onInfo: false
        }
      ]
    });
  };

  onRemoveWorkExpClick = idx => {
    this.setState({
      exps: this.state.exps.filter((val, index) => {
        return index !== idx;
      })
    });
  };

  onAddMoreCertificateClick = ev => {
    this.setState({
      certs: [
        ...this.state.certs,
        {
          cert_Institute: "",
          certificate_name: "",
          is_valid: true,
          receive_date: 0
        }
      ]
    });
  };

  onRemoveCertificateClick = idx => {
    this.setState({
      certs: this.state.certs.filter((val, index) => {
        return index !== idx;
      })
    });
  };

  onExpInputChange = (ev, { value, name, idx }) => {
    const newExp = [...this.state.exps];

    // current dataset
    if (name === "current" || name === "onInfo") {
      newExp[idx][name] = !newExp[idx][name];

      if (name === "current") {
        newExp[idx].dor = {
          ...newExp[idx].dor,
          month: "",
          year: ""
        };
      }
    } else if (name === "doj_year" || name === "doj_month") {
      newExp[idx].doj = {
        ...newExp[idx].doj,
        [`${name.split("_")[1]}`]: value
      };
    } else if (name === "dor_year" || name === "dor_month") {
      newExp[idx].dor = {
        ...newExp[idx].dor,
        [`${name.split("_")[1]}`]: value
      };
    } else {
      newExp[idx][name] = value;
    }

    this.setState({
      exps: newExp
    });
  };

  onExpQuillChange = (html, idx) => {
    const newExp = [...this.state.exps];
    newExp[idx].desc = html;

    this.setState({
      exps: newExp
    });
  };

  onEduInputChange = (ev, { value, name, idx }) => {
    const newExp = [...this.state.edus];

    if (name === "onInfo") {
      newExp[idx][name] = !newExp[idx][name];
    } else if (name === "specs") {
      newExp[idx][name] = [value];
    } else {
      newExp[idx][name] = value;
    }

    this.setState({
      edus: newExp
    });
  };

  onCertInputChange = (ev, { value, name, idx }) => {
    const newExp = [...this.state.certs];
    newExp[idx][name] = value;

    this.setState({
      certs: newExp
    });
  };

  onSKillAdd = (e, { value }) => {
    this.setState({
      skillFieldOptions: [
        { text: value, value },
        ...this.state.skillFieldOptions
      ]
    });
  };

  onSKillChange = (e, { value }) => {
    this.setState({
      skillFieldCurrentValues: value,
      skills: getProcessedSkills(value, this.state.skills)
    });
  };

  onDesiredJobChange = (ev, { value, name }) => {
    this.setState({
      desireJob: {
        ...this.state.desireJob,
        [name]: value
      }
    });
  };

  onDesiredJobAutoComplete = ({ value, name }) => {
    this.setState({
      desireJob: {
        ...this.state.desireJob,
        [name]: value
      }
    });
  };

  onFileChange = e => {
    const file = e.target.files[0];

    if (!file) {
      return;
    }

    const EXTNS = ["png", "jpeg", "jpg", "gif", "tiff", "bmp"];
    const extn = file.name.split(".").pop();
    if (!extn || EXTNS.indexOf(String(extn).toLowerCase()) === -1) {
      return;
    }
    const reader = new FileReader();

    reader.onloadend = e => {
      this.setState({
        ext: {
          ...this.state.ext,
          imgBanner: "jpg"
        },
        bannerImageFile: file,
        bannerImage: reader.result
      });
    };

    reader.readAsDataURL(file);
  };

  onFileRemove = e => {
    this.setState({
      ext: {
        ...this.state.ext,
        imgBanner: ""
      },
      bannerImageFile: "",
      bannerImage: aunty
    });
  };

  onImagePickerCancelClick = ev => {
    this.setState({
      imagePickerOpen: !this.state.imagePickerOpen,
      profileImageFile: "",
      profileImage: ""
    });
  };

  onImagePickerSaveClick = image => {
    this.setState({
      profileImage: image,
      imagePickerOpen: !this.state.imagePickerOpen
    });
  };

  /**
   *
   * @param {SyntheticEvent} ev
   */
  async onProfileSubmit(ev) {
    const {
      skillShowEditBtn,
      skillFieldCurrentValues,
      skillFieldOptions,
      bannerImage,
      bannerImageFile,
      profileImage,
      profileImageFile,
      resumeDocDataFile,
      resumeDocFile,
      resumeDoc,
      isProfileLoading,
      counter,
      canSubmit,
      loaderText,
      imagePickerOpen,
      ...userData
    } = this.state;
    const baseJson = JSON.parse(window.localStorage.getItem(USER.BASE_JSON));
    const finalJson = Object.assign({}, baseJson, userData, {
      isVerified: true
    });
    // console.log(finalJson);

    this.setState({
      isProfileLoading: true
    });

    try {
      const res = await createProfileOnServer(finalJson);
      const data = await res.json();

      if (bannerImageFile) {
        let imgBlob = uriToBlob(bannerImage);
        const imgBannerUpload = await uploadImage(
          `banner/${userData._id}/image.png`,
          imgBlob
        );
      }

      if (resumeDocDataFile) {
        let resBlob = uriToBlob(resumeDocDataFile);
        if (resBlob.size > 0) {
          const resUpload = await uploadResume(
            `${userData._id}/${userData.fName}_${userData.lName}.${
              userData.ext.resume
            }`,
            resBlob
          );
        }
      }

      if (profileImage) {
        let imgBlob = uriToBlob(profileImage);
        const profileImgUpload = await uploadImage(
          `${userData._id}/image.jpg`,
          imgBlob
        );
      }

      if (data) {
        // window.localStorage.setItem(USER.UID, data._id);
        // window.localStorage.setItem(USER.EMAIL, data.email);
        // window.localStorage.setItem(USER.NAME, `${data.fName} ${data.lName}`);
        // window.localStorage.setItem(
        //   USER.CTC,
        //   data.desireJob ? data.desireJob.currentCtc : ""
        // );
        // window.localStorage.setItem(USER.EXP, 0);
        // window.localStorage.setItem(USER.IMG_EXT, data.ext ? data.ext.img : "");
        // window.localStorage.setItem(USER.LOC, data.loc ? data.loc.city : "");
        // window.localStorage.setItem(USER.PHONE, data.mobile);
        // window.localStorage.setItem(USER.TITLE, data.title);

        this.props.onUserChange({
          id: data._id,
          email: data.email,
          fName: data.fName,
          lName: data.lName,
          title: data.title,
          loc: data.loc ? data.loc.city : "",
          userImg: data.ext ? data.ext.img : "",
          mobile: data.mobile,
          token: data.googleToken,
          isVerified: data.isVerified,
          ctc: 0
        });

        // toast("Profile Successfully Created");
        // window.location.href = "/user/view/profile";
        this.props.history.push("/user/view/profile");
      }
    } catch (error) {
      console.error(error);
    }
  }

  onSkillEditModalInputChange = (e, { value, idx, name }) => {
    const newSkills = [...this.state.skills];
    newSkills[idx][name] = value;

    this.setState({
      skills: newSkills
    });
  };

  onSkillEditModalRemove = (e, { idx }) => {
    this.setState((prevState, props) => {
      const newSkills = [...prevState.skills];
      const newSkillOptions = [...prevState.skillFieldOptions];
      const newSkillCurrentValues = [...prevState.skillFieldCurrentValues];

      newSkills.splice(idx, 1);
      newSkillCurrentValues.splice(idx, 1);

      return {
        ...prevState,
        skills: newSkills,
        skillFieldCurrentValues: newSkillCurrentValues
      };
    });
  };

  onSkillModalSave = e => {
    this.setState({
      skillShowEditBtn: false
    });
  };

  onFormValid = ev => {
    this.setState({
      canSubmit: true,
      counter: 20
    });
  };

  onFormInValid = ev => {
    this.setState({
      canSubmit: false,
      counter: 5
    });
  };

  onPreviewProfileClick = ev => {
    window.localStorage.setItem(
      UserConstants.USER_PREVIEW_STATE,
      JSON.stringify(this.state)
    );
    this.props.history.push("/view/preview/user");
  };

  onCheckInfoEdu = valId => {
    const edus = this.state.edus.map(val => {
      if (val.onInfo === true && valId === val._id) {
        return {
          ...val,
          onInfo: false
        };
      }

      if (valId === val._id) {
        return {
          ...val,
          onInfo: true
        };
      }

      return {
        ...val,
        onInfo: false
      };
    });

    this.setState({
      edus: edus
    });
  };

  onCheckInfoExp = valId => {
    const exps = this.state.exps.map(val => {
      if (val.onInfo === true && valId === val._id) {
        return {
          ...val,
          onInfo: false
        };
      }

      if (valId === val._id) {
        return {
          ...val,
          onInfo: true
        };
      }

      return {
        ...val,
        onInfo: false
      };
    });

    this.setState({
      exps: exps
    });
  };

  render() {
    const UserId = window.localStorage.getItem(USER.UID);
    return (
      <div className="CreateProfilePage">
        <div>
          <PageBanner size="medium" image={this.state.bannerImage}>
            <Container>
              <CreateProfileBannerHead
                bannerImageFile={this.state.bannerImageFile}
                progressCounter={this.state.counter}
                onFileChange={this.onFileChange}
                onFileRemove={this.onFileRemove}
              />
            </Container>
          </PageBanner>
        </div>

        <Container>
          <div className="card_panel">
            <div className="card_panelHeader">
              <div className="createProfile_personalDetailAutoFill">
                <div className="auto_fillLavel">
                  <Header as="h4" className="">
                    It’s easy!
                    <Popup
                      trigger={
                        <div className="InfoIconLabel">
                          <IcInfoIcon pathcolor="#ceeefe" />
                        </div>
                      }
                      content={CREATE_PROFILE.LINKEDIN_BANNER_BUBBLE}
                    />
                    {/* <InfoIconLabel/> */}
                  </Header>
                </div>
                <div className="auto_fillBtn">
                  {/* <LinkedIn
                    clientId="78hqxeizeuh9lu"
                    callBack={this.onLinkedInCallback}
                    fields=":(id,num-connections,picture-url,first-name,last-name,headline,location,industry,summary,positions,public-profile-url)"
                    textButton={
                      <React.Fragment>
                        <span>Autofill profile with LinkedIn</span>
                        <IcDownArrowIcon pathcolor="#c8c8c8" />
                      </React.Fragment>
                    }
                    buttonType={"button"}
                    icon={<IcLinkedinIcon className="no-rotate" />}
                    className="ui compact button autoFill_btn"
                  /> */}

                  <ReactLinkedIn
                    clientId={"78hqxeizeuh9lu"}
                    onFailure={this.onLinkedInError}
                    onSuccess={this.onLinkedInSuccess}
                    redirectUri={"https://vasitum.com/create/profile/linkedin"}>
                    {" "}
                    <React.Fragment>
                      <Icon name="linkedin" />
                      <span>Autofill profile with LinkedIn</span>
                      <IcDownArrowIcon pathcolor="#c8c8c8" />
                    </React.Fragment>
                  </ReactLinkedIn>
                </div>
              </div>

              {/* <Grid>
                <Grid.Row>
                  <Grid.Column
                    mobile={16}
                    computer={2}
                    className="panelHeader_text">
                    <Header as="h4" className="">
                      It’s easy!
                      <Popup
                        trigger={
                          <div className="InfoIconLabel">
                            <IcInfoIcon pathcolor="#ceeefe" />
                          </div>
                        }
                        content={CREATE_PROFILE.LINKEDIN_BANNER_BUBBLE}
                      />
                     
                    </Header>
                  </Grid.Column>
                  <Grid.Column mobile={14} computer={7}>
                    
                    <LinkedIn
                      clientId="78hqxeizeuh9lu"
                      callBack={this.onLinkedInCallback}
                      fields=":(id,num-connections,picture-url,first-name,last-name,headline,location,industry,summary,positions,public-profile-url)"
                      textButton={
                        <React.Fragment>
                          <span>Autofill profile with LinkedIn</span>
                          <IcDownArrowIcon pathcolor="#c8c8c8" />
                        </React.Fragment>
                      }
                      buttonType={"button"}
                      icon={<IcLinkedinIcon className="no-rotate" />}
                      className="ui compact button autoFill_btn"
                    />
                  </Grid.Column>
                </Grid.Row>
              </Grid> */}
            </div>

            <Form
              noValidate
              onValidSubmit={this.onProfileSubmit}
              onValid={this.onFormValid}
              onInvalid={this.onFormInValid}>
              <PersonalDetails
                data={this.state}
                onInputChange={this.onInputChange}
                onLocChange={this.onLocChange}
                onLocSelect={this.onLocSelect}
              />
              <ProfileDetails
                data={this.state}
                onInputChange={this.onInputChange}
                onProfileSummaryChange={this.onProfileSummaryChange}
                onImageChange={this.onImageChange}
                onImageRemove={this.onImageRemove}
                onResumeChange={this.onResumeChange}
                onResumeRemove={this.onResumeRemove}
                isActive={true}
              />
              <Skills
                data={this.state}
                onSKillAdd={this.onSKillAdd}
                onSKillChange={this.onSKillChange}
                onSkillModalChange={this.onSkillEditModalInputChange}
                onSkillModalRemove={this.onSkillEditModalRemove}
                onSkillModalSave={this.onSkillModalSave}
                isActive={true}
              />
              <Education
                data={this.state}
                onAddMoreClick={this.onAddMoreEducation}
                onRemoveClick={this.onRemoveEducationClick}
                onInputChange={this.onEduInputChange}
                onCheckInfoEdu={this.onCheckInfoEdu}
                isActive={true}
              />
              <WorkExperience
                data={this.state}
                onInputChange={this.onExpInputChange}
                onAddMoreClick={this.onAddMoreWorkExpClick}
                onRemoveClick={this.onRemoveWorkExpClick}
                onQuillChange={this.onExpQuillChange}
                onCheckInfoExp={this.onCheckInfoExp}
                isActive={true}
              />
              <DesireJobs
                data={this.state.desireJob}
                onAutoComplete={this.onDesiredJobAutoComplete}
                onInputChange={this.onDesiredJobChange}
                isActive={true}
              />
              <Certificate
                data={this.state}
                onAddMoreClick={this.onAddMoreCertificateClick}
                onRemoveClick={this.onRemoveCertificateClick}
                onInputChange={this.onCertInputChange}
                isActive={true}
              />
              <div className="createProfile_actionBtn">
                <FlatDefaultBtn
                  btnicon={
                    <IcProfileViewIcon height="12" pathcolor="#c8c8c8" />
                  }
                  btntext="Preview Profile"
                  onClick={this.onPreviewProfileClick}
                />
                <ActionBtn
                  className={enableGA() ? "js__enable-track" : ""}
                  disabled={!this.state.canSubmit}
                  actioaBtnText="Create"
                  onClick={() => {
                    ReactGA.event({
                      category: CreateProfileString.creatProfileBtn.category,
                      action: CreateProfileString.creatProfileBtn.action,
                      value: isAuthenticated() ? "" : UserId,
                    });
                  }}
                />
              </div>
            </Form>
          </div>
        </Container>
        <PageLoader
          active={this.state.isProfileLoading}
          loaderText={this.state.loaderText}
        />

        <ProfileImagePicker
          open={this.state.imagePickerOpen}
          src={this.state.profileImage}
          onCancel={this.onImagePickerCancelClick}
          onSave={this.onImagePickerSaveClick}
        />
      </div>
    );
  }
}

export default withUserContextConsumer(withRouter(CreateProfilePage));
