import React from "react";

import { Grid } from "semantic-ui-react";

import InfoSection from "../../Sections/InfoSection";

import InputContainer from "../../Forms/InputContainer";
import InputField from "../../Forms/FormFields/InputField";
import InputFieldLabel from "../../Forms/FormFields/InputFieldLabel";
import IcInfoIcon from "../../../assets/svg/IcInfo";
import LocationInputField from "../../Forms/FormFields/LocationInputField";
import InfoIconLabel from "../../utils/InfoIconLabel";
import getPlaceholder from "../../Forms/FormFields/Placeholder";

import { CREATE_PROFILE } from "../../../strings";
import "./index.scss";

const labelStar = str => {
  return (
    <div>
      <span>{str}</span>
      <span style={{ color: "#0b9ed0" }}>*</span>
    </div>
  );
};

const infoicon = <IcInfoIcon pathcolor="#c8c8c8" />;

class PersonalDetails extends React.Component {
  render() {
    const {
      data,
      onInputChange,
      onLocChange,
      onLocSelect,
      isEditProfile,
      isActive
    } = this.props;

    return (
      <div
        id="personal"
        className="panel-cardBody personal_details PersonalDetails"
        style={{ paddingBottom: "30px" }}>
        <InfoSection
          headerSize="large"
          color="blue"
          headerText="Personal details"
          collapsible={isEditProfile}
          rightHeader={
            <div className="personal_detailHeaderRight">
              <span>* </span>
              {CREATE_PROFILE.PERSONAL_DETAILS.CARD_HEADER_TITLE.RIGHT_TEXT}
            </div>
          }
          isActive={isActive}>
          <InputContainer
            label={labelStar(`First name`)}
            infoicon={<InfoIconLabel text={CREATE_PROFILE.BUBBLE.FNAME} />}>
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only="mobile">
                  <InputFieldLabel
                    label={labelStar(`First name`)}
                    infoicon={
                      <InfoIconLabel text={CREATE_PROFILE.BUBBLE.FNAME} />
                    }
                  />
                </Grid.Column>
                <Grid.Column computer={16}>
                  <InputField
                    name="fName"
                    value={data.fName}
                    onChange={onInputChange}
                    placeholder={getPlaceholder("For Eg: Jack", "For Eg: Jack")}
                    required
                    validationErrors={{
                      isDefaultRequiredValue: "Field is required"
                    }}
                    errorLabel={
                      <span style={{ color: "red", fontSize: "12px" }} />
                    }
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>

          <InputContainer
            label={labelStar(`Last name`)}
            infoicon={<InfoIconLabel text={CREATE_PROFILE.BUBBLE.LNAME} />}>
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only="mobile">
                  <InputFieldLabel
                    label={labelStar(`Last name`)}
                    infoicon={
                      <InfoIconLabel text={CREATE_PROFILE.BUBBLE.LNAME} />
                    }
                  />
                </Grid.Column>
                <Grid.Column computer={16}>
                  <InputField
                    name="lName"
                    value={data.lName}
                    onChange={onInputChange}
                    placeholder={getPlaceholder("For Eg: Ryan", "For Eg: Ryan")}
                    required
                    validationErrors={{
                      isDefaultRequiredValue: "Field is required"
                    }}
                    errorLabel={
                      <span style={{ color: "red", fontSize: "12px" }} />
                    }
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>

          <InputContainer
            label={labelStar(`Phone number`)}
            infoicon={<InfoIconLabel text={CREATE_PROFILE.BUBBLE.MOBILE} />}>
            <div className="phonNumber_container">
              <div className="phonNumber_label">
                <InputFieldLabel
                  label={labelStar(`Phone number`)}
                  infoicon={
                    <InfoIconLabel text={CREATE_PROFILE.BUBBLE.MOBILE} />
                  }
                />
              </div>
              <div className="phonNumber_innerContainer">
                <div className="contryCode_feild">
                  <InputField
                    name="countryCode"
                    pattern="[5-9]{1}[0-9]{9}"
                    title="Please enter a valid country code"
                    value={data.countryCode}
                    inputtype="text"
                    required
                    onChange={onInputChange}
                    placeholder={getPlaceholder("For Eg: +91", "For Eg: +91")}
                    errorLabel={
                      <span style={{ color: "red", fontSize: "12px" }} />
                    }
                  />
                </div>
                <div className="phoneNumber_feild">
                  <InputField
                    name="mobile"
                    pattern="[5-9]{1}[0-9]{9}"
                    title="Please enter a valid mobile number"
                    value={data.mobile}
                    inputtype="text"
                    validations={{
                      customMobileValidation: (values, value) => {
                        return /^(\+\d{1,3}[- ]?)?\d{10}$/.test(value);
                      }
                    }}
                    validationErrors={{
                      customMobileValidation:
                        "Please enter a valid mobile number"
                    }}
                    required
                    onChange={onInputChange}
                    placeholder={getPlaceholder(
                      "For Eg: 9875648534",
                      "For Eg: 7854965486"
                    )}
                    errorLabel={
                      <span style={{ color: "red", fontSize: "12px" }} />
                    }
                  />
                </div>
              </div>
            </div>
            {/* <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only="mobile">
                  <InputFieldLabel
                    label={labelStar(`Phone number`)}
                    infoicon={
                      <InfoIconLabel text={CREATE_PROFILE.BUBBLE.MOBILE} />
                    }
                  />
                </Grid.Column>
                <Grid.Column mobile={4} computer={3}>
                  <InputField
                    name="countryCode"
                    pattern="[5-9]{1}[0-9]{9}"
                    title="Please enter a valid country code"
                    value={data.countryCode}
                    inputtype="text"
                    required
                    onChange={onInputChange}
                    placeholder={getPlaceholder("For Eg: +91", "For Eg: +91")}
                    errorLabel={
                      <span style={{ color: "red", fontSize: "12px" }} />
                    }
                  />
                </Grid.Column>
                <Grid.Column mobile={12} computer={13}>
                  <InputField
                    name="mobile"
                    pattern="[5-9]{1}[0-9]{9}"
                    title="Please enter a valid mobile number"
                    value={data.mobile}
                    inputtype="text"
                    validations={{
                      customMobileValidation: (values, value) => {
                        return /^(\+\d{1,3}[- ]?)?\d{10}$/.test(value);
                      }
                    }}
                    validationErrors={{
                      customMobileValidation:
                        "Please enter a valid mobile number"
                    }}
                    required
                    onChange={onInputChange}
                    placeholder={getPlaceholder(
                      "For Eg: 9875648534",
                      "For Eg: 7854965486"
                    )}
                    errorLabel={
                      <span style={{ color: "red", fontSize: "12px" }} />
                    }
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid> */}
          </InputContainer>

          <InputContainer
            label={labelStar(`Location`)}
            infoicon={<InfoIconLabel text={CREATE_PROFILE.BUBBLE.LOCATION} />}>
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only="mobile">
                  <InputFieldLabel
                    label={labelStar(`Location`)}
                    infoicon={
                      <InfoIconLabel text={CREATE_PROFILE.BUBBLE.LOCATION} />
                    }
                  />
                </Grid.Column>
                <Grid.Column computer={16}>
                  <LocationInputField
                    value={data.loc ? data.loc.adrs : ""}
                    onSelect={onLocSelect}
                    onChange={onLocChange}
                    placeholder={"For Eg: Noida"}
                    required
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>
        </InfoSection>
      </div>
    );
  }
}

export default PersonalDetails;
