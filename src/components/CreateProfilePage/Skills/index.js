import React from "react";

import { Grid } from "semantic-ui-react";

import InfoSection from "../../Sections/InfoSection";
import InputContainer from "../../Forms/InputContainer";
import InputTagField from "../../Forms/FormFields/InputTagField";
import InputFieldLabel from "../../Forms/FormFields/InputFieldLabel";
import getPlaceholder from "../../Forms/FormFields/Placeholder";

import InfoIconLabel from "../../utils/InfoIconLabel";
import SkipNowCollaps from "../../utils/SkipNowCollaps";
import { CREATE_PROFILE } from "../../../strings";

import "./index.scss";

const labelStar = str => {
  return (
    <div>
      <span>{str}</span>
      <span style={{ color: "#0b9ed0" }}>*</span>
    </div>
  );
};

class Skills extends React.Component {
  render() {
    const {
      data,
      onSKillChange,
      onSKillAdd,
      onSkillModalChange,
      onSkillModalRemove,
      onSkillModalSave,
      isEditProfile,
      isActive
    } = this.props;

    return (
      <div className="panel-cardBody Profile_details" id="skill">
        <InfoSection
          headerSize="large"
          color="blue"
          headerText="Skills"
          rightIcon={!isEditProfile && <SkipNowCollaps />}
          collapsible
          isActive={isActive}>
          <InputContainer
            label={`Specify skills`}
            infoicon={<InfoIconLabel text={CREATE_PROFILE.BUBBLE.SKILLS} />}>
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only="mobile">
                  <InputFieldLabel
                    label={`Specify Skills`}
                    infoicon={
                      <InfoIconLabel text={CREATE_PROFILE.BUBBLE.SKILLS} />
                    }
                  />
                </Grid.Column>
                <Grid.Column computer={16}>
                  <InputTagField
                    profile
                    currentValues={data.skillFieldCurrentValues}
                    options={data.skillFieldOptions}
                    showEditBtn={data.skillShowEditBtn}
                    onAdd={onSKillAdd}
                    onChange={onSKillChange}
                    skillsModel={data.skills}
                    onModalItemChange={onSkillModalChange}
                    onModalItemRemove={onSkillModalRemove}
                    onModalSave={onSkillModalSave}
                    placeholder={getPlaceholder(
                      "For Eg: HTML,CSS",
                      "For Eg: HTML,CSS (Separate by enter)"
                    )}
                    name="skills"
                    required={true}
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>
        </InfoSection>
      </div>
    );
  }
}

export default Skills;
