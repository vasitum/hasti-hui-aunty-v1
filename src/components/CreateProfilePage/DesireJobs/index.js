import React from "react";

import { Grid, Header } from "semantic-ui-react";
import SelectOptionField from "../../Forms/FormFields/SelectOptionField";
import InfoSection from "../../Sections/InfoSection";
import InputContainer from "../../Forms/InputContainer";
import InputField from "../../Forms/FormFields/InputField";
import DropdownMenu from "../../DropdownMenu";
import InputFieldLabel from "../../Forms/FormFields/InputFieldLabel";
import IcInfoIcon from "../../../assets/svg/IcInfo";
import IcPrivacyLock from "../../../assets/svg/IcPrivacyLock";
import getPlaceholder from "../../Forms/FormFields/Placeholder";
import SearchFilterInput from "../../Forms/FormFields/SearchFilterInput";

import InfoIconLabel from "../../utils/InfoIconLabel";
import SkipNowCollaps from "../../utils/SkipNowCollaps";

import MobileGrid from "../../MobileComponents/MobileGrid";
import AutoCompleteSearch from "../../Forms/FormFields/AutoCompleteSearch";

import "./index.scss";
import { CREATE_PROFILE } from "../../../strings";

const infoicon = <IcInfoIcon pathcolor="#c8c8c8" />;

const autoSearchOptionsFeild = [
  { key: "afq2", value: "", flag: "af", text: "Select" },
  { key: "af", value: "af", flag: "af", text: "Afghanistan" },
  { key: "india", value: "india", flag: "india", text: "india" },
  { key: "us", value: "us", flag: "us", text: "US" }
  // { key: 'af', value: 'af', flag: 'af', text: 'Afghanistan' },
  // { key: 'af', value: 'af', flag: 'af', text: 'Afghanistan' },
  // { key: 'af', value: 'af', flag: 'af', text: 'Afghanistan' },
];

function getSalaryPlaceHolder(currency, type) {
  if (currency === "INR" || !currency) {
    return getPlaceholder(
      `${type}. Annual Salary (LPA)`,
      `${type} Annual Salary (LPA)`
    );
  }

  return getPlaceholder(
    `${type} Annual Salary (Thousands)`,
    `${type} Annual Salary (Thousands)`
  );
}

class DesireJobs extends React.Component {
  render() {
    const {
      data,
      onSalaryChange,
      onInputChange,
      onAutoComplete,
      isEditProfile,
      isActive
    } = this.props;

    return (
      <div className="panel-cardBody Profile_details DesireJobs" id="desired">
        <InfoSection
          headerSize="large"
          color="blue"
          headerText="Desired jobs"
          subHeaderText="only you can see this."
          subHeaderIcon={IcPrivacyLock}
          rightIconSubHeader={!isEditProfile && <SkipNowCollaps />}
          collapsible
          isActive={isActive}>
          <InputContainer
            label="Job role"
            infoicon={
              <InfoIconLabel text={CREATE_PROFILE.BUBBLE.DESIRED_ROLE} />
            }>
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only="mobile">
                  <InputFieldLabel
                    label="Job role"
                    infoicon={
                      <InfoIconLabel
                        text={CREATE_PROFILE.BUBBLE.DESIRED_ROLE}
                      />
                    }
                  />
                </Grid.Column>
                <Grid.Column computer={16}>
                  <AutoCompleteSearch
                    dataType={"ROLE"}
                    placeholder={getPlaceholder(
                      "For Eg: Cost Accountant",
                      "For Eg: Cost Accountant"
                    )}
                    // results={[]}
                    value={data.role}
                    name="role"
                    onInputChange={onInputChange}
                  />
                  {/* <SearchFilterInput
                    placeholder={getPlaceholder(
                      "Eg. Cost Accountant",
                      "Eg. Cost Accountant"
                    )}
                    results={[]}
                    value={data.role ? data.role : ""}
                    name="role"
                    onChange={onAutoComplete}
                  /> */}
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>

          <InputContainer
            label="Employment type"
            infoicon={
              <InfoIconLabel text={CREATE_PROFILE.BUBBLE.DESIRED_TYPE} />
            }>
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only="mobile">
                  <InputFieldLabel
                    label="Employment type"
                    infoicon={
                      <InfoIconLabel
                        text={CREATE_PROFILE.BUBBLE.DESIRED_TYPE}
                      />
                    }
                  />
                </Grid.Column>
                <Grid.Column computer={16}>
                  <SelectOptionField
                    placeholder={getPlaceholder(
                      "For Eg: Full Time, Contract",
                      "For Eg: Full Time, Contract"
                    )}
                    optionsItem={[
                      { value: "Full-Time", text: "Full-Time" },
                      { value: "Contract", text: "Contract" },
                      { value: "Freelancer", text: "Freelancer" },
                      { value: "Part-Time", text: "Part-Time" }
                    ]}
                    name="empType"
                    value={data.empType}
                    onChange={onInputChange}
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>

          <InputContainer
            label="Salary"
            infoicon={
              <InfoIconLabel text={CREATE_PROFILE.BUBBLE.DESIRED_SALARY} />
            }
            className="createProfile_SalaryText">
            <p>{CREATE_PROFILE.BUBBLE.DESIRED_TAG}</p>
          </InputContainer>

          <InputContainer
            infoicon={
              <DropdownMenu
                name="currency"
                value={data.currency}
                onChange={onInputChange}
              />
            }
            className="createProfile_salaryDropdown">
            <Grid>
              <div className="createProfileSalary_mobileContainer">
                <div className="">
                  <p>
                    Salary
                    <DropdownMenu
                      name="currency"
                      value={data.currency}
                      onChange={onInputChange}
                    />
                  </p>
                </div>
              </div>
              <Grid.Row className="mobile_Salary">
                {/* <Grid.Column mobile={8} only="mobile">
                  <InputFieldLabel label="Salary" />
                </Grid.Column> */}
                {/* <Grid.Column mobile={8} only="mobile" textAlign="right">
                  <div className="mobile_SalaryDropdown">
                    <DropdownMenu
                      name="currency"
                      value={data.currency}
                      onChange={onInputChange}
                    />
                  </div>
                </Grid.Column> */}

                <Grid.Column
                  computer={8}
                  // mobile={16}
                  className="curentSalary_input">
                  <InputField
                    inputtype="number"
                    placeholder={getSalaryPlaceHolder(data.currency, "Current")}
                    name="currentCtc"
                    value={data.currentCtc}
                    onChange={onInputChange}
                  />
                </Grid.Column>
                <Grid.Column
                  computer={8}
                  // mobile={16}
                >
                  <InputField
                    inputtype="number"
                    placeholder={getSalaryPlaceHolder(
                      data.currency,
                      "Expected"
                    )}
                    name="expectedCtc"
                    value={data.expectedCtc}
                    onChange={onInputChange}
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>

          {/* <div className="InputGridContainer">
            <Grid>
              <Grid.Row>
                <Grid.Column computer={4} className="mobile hidden">
                  <InputFieldLabel
                    label="Salary"
                    infoicon={
                      <DropdownMenu
                        onChange={onSalaryChange}
                        value={data.salary.currency}
                        name="currency"
                      />
                    }
                  />
                </Grid.Column>
                <Grid.Column
                  mobile={8}
                  computer={3}
                  className="mobile_inputField inputGrid_comMin">

                  <MobileGrid>
                    <div className="mobile_inputLabel">
                      <p>Salary</p>
                    </div>
                  </MobileGrid>

                  <InputField
                    placeholder={getSalaryPlaceHolder(
                      data.salary.currency,
                      "Min."
                    )}
                    value={data.salary.min}
                    onChange={onSalaryChange}
                    name="min"
                  />
                </Grid.Column>
                <Grid.Column
                  computer={1}
                  className="mobile hidden isFlexCenter divider_to"
                  textAlign="center">
                    To
                </Grid.Column>
                <Grid.Column
                  mobile={8}
                  computer={3}
                  className="mobile_inputField inputGrid_comMax">

                  <MobileGrid>
                    <div
                      className="mobile_inputLabel"
                      style={{
                        textAlign: "right"
                      }}>
                      <DropdownMenu />
                    </div>
                  </MobileGrid>

                  <InputField
                    placeholder={getSalaryPlaceHolder(
                      data.salary.currency,
                      "Max."
                    )}
                    inputtype="number"
                    value={data.salary.max}
                    onChange={onSalaryChange}
                    name="max"
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </div> */}
        </InfoSection>
      </div>
    );
  }
}

export default DesireJobs;
