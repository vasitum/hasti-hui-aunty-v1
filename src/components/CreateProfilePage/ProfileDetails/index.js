import React from "react";

import { Grid, Image } from "semantic-ui-react";

import InfoSection from "../../Sections/InfoSection";
import FlatDefaultBtn from "../../Buttons/FlatDefaultBtn";
import InputContainer from "../../Forms/InputContainer";
import InputField from "../../Forms/FormFields/InputField";
import ImgPickerField from "../../Forms/FormFields/ImgPickerField";
import QuillText from "../../CardElements/QuillText";
import InputFieldLabel from "../../Forms/FormFields/InputFieldLabel";
import SelectOptionField from "../../Forms/FormFields/SelectOptionField";
import IcUploadIcon from "../../../assets/svg/IcUpload";
import getPlaceholder from "../../Forms/FormFields/Placeholder";
import FileuploadBtn from "../../Buttons/FileuploadBtn";
import profile from "../../../assets/img/profile.png";
import InfoIconLabel from "../../utils/InfoIconLabel";
import SkipNowCollaps from "../../utils/SkipNowCollaps";
import ProfileImagePicker from "../../Pickers/ProfileImagePicker";

import { CREATE_PROFILE } from "../../../strings";

import "./index.scss";

function formatUserExp(data) {
  if (!data || !data.exps) {
    return {
      options: [
        {
          key: "a",
          text: "",
          value: ""
        }
      ],
      value: ""
    };
  }

  let mVal = "";
  const mOptions = data.exps.map((val, idx) => {
    if (val.onInfo) {
      mVal = val._id;
    }

    return {
      key: val._id,
      text: `${val.designation ? val.designation : ""} ${
        val.name ? " at " + val.name : ""
      }`,
      value: val._id
    };
  });

  const mFinalOptions = mOptions.filter(val => {
    if (val.text) {
      return true;
    }
  });

  return {
    options: mFinalOptions,
    value: mVal
  };
}

function formatUserEdus(data) {
  if (!data || !data.edus) {
    return {
      options: [
        {
          key: "a",
          text: "",
          value: ""
        }
      ],
      value: ""
    };
  }

  let mVal = "";
  const mOptions = data.edus.map((val, idx) => {
    if (val.onInfo) {
      mVal = val._id;
    }

    return {
      key: val._id,
      text: `${val.course ? val.course : ""} ${
        val.institute ? " from " + val.institute : ""
      }`,
      value: val._id
    };
  });

  const mFinalOptions = mOptions.filter(val => {
    if (val.text) {
      return true;
    }
  });

  return {
    options: mFinalOptions,
    value: mVal
  };
}

class ProfileDetails extends React.Component {
  render() {
    const {
      data,
      onInputChange,
      onProfileSummaryChange,
      onImageChange,
      onImageRemove,
      onResumeChange,
      onResumeRemove,
      onSelectInfoEdu,
      onSelectInfoExp,
      isEditProfile,
      imagePickerOpen,
      onImagePickerCancelClick,
      onImagePickerSaveClick,
      isActive
    } = this.props;

    let formattedUserEdus = {};
    let formattedUserExps = {};

    if (isEditProfile) {
      formattedUserEdus = formatUserEdus(data);
      formattedUserExps = formatUserExp(data);
    }

    return (
      <div className="panel-cardBody Profile_details" id="profile">
        <InfoSection
          headerSize="large"
          color="blue"
          headerText="Profile details"
          rightIcon={!isEditProfile && <SkipNowCollaps />}
          collapsible
          isActive={isActive}>
          <InputContainer
            label="Profile headline"
            // infoicon={<InfoIconLabel text={CREATE_PROFILE.BUBBLE.JOB_TITLE} />}
          >
            <Grid>
              <Grid.Row>
                {/* <Grid.Column mobile={16} only="mobile">
                  <InputFieldLabel
                    label="Profile headline"
                    infoicon={
                      <InfoIconLabel text={CREATE_PROFILE.BUBBLE.JOB_TITLE} />
                    }
                  />
                </Grid.Column> */}
                <Grid.Column computer={16}>
                  <InputField
                    value={data.title}
                    onChange={onInputChange}
                    name="title"
                    placeholder={getPlaceholder(
                      "For Eg: 6 yrs of exp. in development",
                      "For Eg: 6 yrs of experience in Creative Writing and Editing"
                    )}
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>

          {isEditProfile && (
            <React.Fragment>
              <InputContainer
                label="Current position"
                infoicon={
                  <InfoIconLabel text="Mention your current role with present employer" />
                }>
                <Grid>
                  <Grid.Row>
                    <Grid.Column mobile={16} only="mobile">
                      <InputFieldLabel
                        label="Current position"
                        infoicon={
                          <InfoIconLabel text="Mention your current role with present employer" />
                        }
                      />
                    </Grid.Column>
                    <Grid.Column computer={16}>
                      <SelectOptionField
                        placeholder={getPlaceholder(
                          "Select Current Position",
                          "Current Position"
                        )}
                        optionsItem={formattedUserExps.options}
                        onChange={onSelectInfoExp}
                        value={formattedUserExps.value}
                        name="currentPos"
                      />
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </InputContainer>

              <InputContainer
                label="Education"
                infoicon={
                  <InfoIconLabel text="Specify your highest level of educational qualification" />
                }>
                <Grid>
                  <Grid.Row>
                    <Grid.Column mobile={16} only="mobile">
                      <InputFieldLabel
                        label="Profile headline"
                        infoicon={
                          <InfoIconLabel text="Specify your highest level of educational qualification" />
                        }
                      />
                    </Grid.Column>
                    <Grid.Column computer={16}>
                      <SelectOptionField
                        placeholder={getPlaceholder(
                          "Select Education",
                          "Select Education"
                        )}
                        optionsItem={formattedUserEdus.options}
                        onChange={onSelectInfoEdu}
                        value={formattedUserEdus.value}
                        name="currentPos"
                      />
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </InputContainer>
            </React.Fragment>
          )}

          <InputContainer
            label="Summary"
            // infoicon={<InfoIconLabel text={CREATE_PROFILE.BUBBLE.SUMMARY} />}
          >
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only="mobile">
                  <InputFieldLabel
                    label="Summary"
                    // infoicon={
                    //   <InfoIconLabel text={CREATE_PROFILE.BUBBLE.SUMMARY} />
                    // }
                  />
                </Grid.Column>
                <Grid.Column computer={16}>
                  <QuillText
                    name="desc"
                    defaultValue={data.desc}
                    value={data.desc}
                    onChange={onProfileSummaryChange}
                    placeholder={CREATE_PROFILE.PLACEHOLDER.SUMMARY}
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>

          <InputContainer
            label="Upload resume"
            infoicon={
              <InfoIconLabel text={CREATE_PROFILE.BUBBLE.UPLOAD_RESUME} />
            }
            className="upload_resume">
            <FileuploadBtn
              btnIcon={<IcUploadIcon pathcolor="#c8c8c8" width="11" />}
              btnText={data.resumeDoc ? data.resumeDoc : "Upload resume"}
              accept=".doc, .docx, .rtf, .pdf, .jpeg, .jpg"
              onFileUpload={onResumeChange}
            />

            {data.resumeDoc && (
              <FlatDefaultBtn
                onClick={onResumeRemove}
                btntext="Cancel"
                className="bgTranceparent"
              />
            )}

            <p className="upload_resumeLabel">
              Supported formats: .DOC, .DOCX, .RTF, .PDF, .JPG, .JPEG
            </p>
          </InputContainer>

          <InputContainer
            label="Upload profile image"
            infoicon={
              <InfoIconLabel text={CREATE_PROFILE.BUBBLE.UPLOAD_IMAGE} />
            }
            className="">
            <ImgPickerField
              onImgChange={onImageChange}
              imgUrl={data.profileImage}
              onRemoveClick={onImageRemove}
              btnText={"Upload Image"}
            />
          </InputContainer>
        </InfoSection>
        <ProfileImagePicker
          open={imagePickerOpen}
          onCancel={onImagePickerCancelClick}
          onSave={onImagePickerSaveClick}
          src={data.profileImage}
        />
      </div>
    );
  }
}

export default ProfileDetails;
