import React from "react";

import { Grid } from "semantic-ui-react";

import InfoSection from "../../Sections/InfoSection";

import InputContainer from "../../Forms/InputContainer";
import InputField from "../../Forms/FormFields/InputField";
import InputFieldLabel from "../../Forms/FormFields/InputFieldLabel";
// import IcInfoIcon from "../../../assets/svg/IcInfo";
import CloseIcon from "../../utils/CloseIcon";

import IcPlusIcon from "../../../assets/svg/IcPlus";
import FlatDefaultBtn from "../../Buttons/FlatDefaultBtn";
import getPlaceholder from "../../Forms/FormFields/Placeholder";

import InfoIconLabel from "../../utils/InfoIconLabel";
import SkipNowCollaps from "../../utils/SkipNowCollaps";
import { CREATE_PROFILE } from "../../../strings";
import AutoCompleteSearch from "../../Forms/FormFields/AutoCompleteSearch";

import "./index.scss";

const autoSearchOptionsFeild = [
  { key: "afq2", value: "", flag: "af", text: "Select" },
  { key: "af", value: "af", flag: "af", text: "Afghanistan" },
  { key: "india", value: "india", flag: "india", text: "india" },
  { key: "us", value: "us", flag: "us", text: "US" }
  // { key: 'af', value: 'af', flag: 'af', text: 'Afghanistan' },
  // { key: 'af', value: 'af', flag: 'af', text: 'Afghanistan' },
  // { key: 'af', value: 'af', flag: 'af', text: 'Afghanistan' },
];

class Certificate extends React.Component {
  render() {
    const {
      data,
      onAddMoreClick,
      onRemoveClick,
      onInputChange,
      isEditProfile,
      isActive
    } = this.props;

    return (
      <div className="panel-cardBody Profile_details" id="certificate">
        <InfoSection
          headerSize="large"
          color="blue"
          headerText="Certificate"
          rightIcon={!isEditProfile && <SkipNowCollaps />}
          collapsible
          isActive={isActive}>
          {data.certs.map((val, idx) => (
            <div className="New_add" key={idx}>
              {/* {idx ? <CloseIcon onClick={() => onRemoveClick(idx)} /> : null} */}
              {data.certs.length > 1 && (
                <CloseIcon onClick={() => onRemoveClick(idx)} />
              )}
              <InputContainer
                label="Certificate name"
                infoicon={
                  <InfoIconLabel
                    text={CREATE_PROFILE.BUBBLE.CERTIFICATE_NAME}
                  />
                }>
                <Grid>
                  <Grid.Row>
                    <Grid.Column mobile={16} only="mobile">
                      <InputFieldLabel
                        label="Certificate name"
                        infoicon={
                          <InfoIconLabel
                            text={CREATE_PROFILE.BUBBLE.CERTIFICATE_NAME}
                          />
                        }
                      />
                    </Grid.Column>
                    <Grid.Column computer={16}>
                      <AutoCompleteSearch
                        dataType={"CERTIFICATE"}
                        placeholder={getPlaceholder(
                          "For Eg: Certificate in java development",
                          "For Eg: Certificate in java development"
                        )}
                        name="certificate_name"
                        value={val.certificate_name}
                        idx={idx}
                        onInputChange={onInputChange}
                      />
                      {/* <InputField
                        placeholder={getPlaceholder(
                          "For Eg: Certificate in java development",
                          "For Eg: Certificate in java development"
                        )}
                        name="certificate_name"
                        value={val.certificate_name}
                        idx={idx}
                        onChange={onInputChange}
                      /> */}
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </InputContainer>

              <InputContainer
                label="Certificate issuing authority"
                infoicon={
                  <InfoIconLabel
                    text={CREATE_PROFILE.BUBBLE.CERITIFCATE_ISSUE_AUTHORITY}
                  />
                }>
                <Grid>
                  <Grid.Row>
                    <Grid.Column mobile={16} only="mobile">
                      <InputFieldLabel
                        label="Certification authority"
                        infoicon={
                          <InfoIconLabel
                            text={
                              CREATE_PROFILE.BUBBLE.CERITIFCATE_ISSUE_AUTHORITY
                            }
                          />
                        }
                      />
                    </Grid.Column>
                    <Grid.Column computer={16}>
                      <InputField
                        placeholder={getPlaceholder(
                          "For Eg: Colad Infotech",
                          "For Eg: Colad Infotech"
                        )}
                        name="cert_Institute"
                        value={val.cert_Institute}
                        idx={idx}
                        onChange={onInputChange}
                      />
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </InputContainer>

              <InputContainer
                label="Receiving year"
                infoicon={
                  <InfoIconLabel
                    text={CREATE_PROFILE.BUBBLE.CERTIFICATE_YEAR}
                  />
                }>
                <Grid>
                  <Grid.Row>
                    <Grid.Column mobile={16} only="mobile">
                      <InputFieldLabel
                        label="Receiving year"
                        infoicon={
                          <InfoIconLabel
                            text={CREATE_PROFILE.BUBBLE.CERTIFICATE_YEAR}
                          />
                        }
                      />
                    </Grid.Column>
                    <Grid.Column computer={16}>
                      <InputField
                        inputtype="number"
                        max="2018"
                        min="1800"
                        placeholder={getPlaceholder("For Eg: ‎2018", "For Eg: ‎2018")}
                        name="receive_date"
                        value={val.receive_date}
                        validations={{
                          customYearValidation: (values, value) => {
                            if (value === "" || value === 0) {
                              return true;
                            }

                            if (!Number(value)) {
                              return false;
                            }

                            if (Number(value) > 2018 || Number(value) < 1800) {
                              return false;
                            }

                            return true;
                          }
                        }}
                        validationErrors={{
                          customYearValidation:
                            "Please select Year between 1800 - 2018"
                        }}
                        errorLabel={
                          <span style={{ color: "red", fontSize: "12px" }} />
                        }
                        idx={idx}
                        onChange={onInputChange}
                      />
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </InputContainer>
            </div>
          ))}

          <InputContainer className="addMore_btnForm">
            <FlatDefaultBtn
              btnicon={<IcPlusIcon pathcolor="#c8c8c8" />}
              btntext="Add more certificate"
              type="button"
              onClick={onAddMoreClick}
            />
          </InputContainer>
        </InfoSection>
      </div>
    );
  }
}

export default Certificate;
