import React from "react";

import { Grid, Icon } from "semantic-ui-react";

import InfoSection from "../../Sections/InfoSection";
import InputContainer from "../../Forms/InputContainer";
import InputField from "../../Forms/FormFields/InputField";
import InputFieldLabel from "../../Forms/FormFields/InputFieldLabel";
import IcInfoIcon from "../../../assets/svg/IcInfo";
import IcPlusIcon from "../../../assets/svg/IcPlus";
import FlatDefaultBtn from "../../Buttons/FlatDefaultBtn";
import SearchFilterInput from "../../Forms/FormFields/SearchFilterInput";

import AutoCompleteSearch from "../../Forms/FormFields/AutoCompleteSearch";

import CloseIcon from "../../utils/CloseIcon";

import CheckboxField from "../../Forms/FormFields/CheckboxField";
import InfoIconLabel from "../../utils/InfoIconLabel";
import SkipNowCollaps from "../../utils/SkipNowCollaps";
import Select from "../../Forms/FormFields/SelectOptionField";
import getPlaceholder from "../../Forms/FormFields/Placeholder";
import { CREATE_PROFILE } from "../../../strings";

import "./index.scss";

const infoicon = <IcInfoIcon pathcolor="#c8c8c8" />;

class Education extends React.Component {
  render() {
    const {
      data,
      onAddMoreClick,
      onRemoveClick,
      onInputChange,
      onCheckInfoEdu,
      isEditProfile,
      isActive
    } = this.props;

    return (
      <div className="panel-cardBody education" id="education">
        <InfoSection
          headerSize="large"
          color="blue"
          headerText="Education"
          rightIcon={!isEditProfile && <SkipNowCollaps />}
          collapsible
          isActive={isActive}>
          {data.edus.map((val, idx) => {
            return (
              <div className="New_add" key={idx}>
                {/* {idx ? <CloseIcon onClick={() => onRemoveClick(idx)} /> : null} */}

                {data.edus.length > 1 && (
                  <CloseIcon onClick={() => onRemoveClick(idx)} />
                )}
                <InputContainer
                  label="Institute/University"
                  // infoicon={
                  //   <InfoIconLabel text="Mention your college / university name" />
                  // }
                >
                  <Grid>
                    <Grid.Row>
                      <Grid.Column mobile={16} only="mobile">
                        <InputFieldLabel
                          label="Institute/University"
                          // infoicon={
                          //   <InfoIconLabel text="Mention your college / university name" />
                          // }
                        />
                      </Grid.Column>
                      <Grid.Column computer={16}>
                        {/* <AutoCompleteSearch
                          placeholder={getPlaceholder(
                            "For Eg: University of delhi",
                            "For Eg: University of delhi"
                          )}
                          autoSearchOptions={autoSearchOptionsFeild}
                          value={val.institute}
                          idx={idx}
                          name="institute"
                          onChange={onInputChange}
                          validations={{
                            customInstitueValidationFix: (values, value) => {
                              if (val.course && !value) {
                                return false;
                              }

                              return true;
                            }
                          }}
                          validationErrors={{
                            customInstitueValidationFix:
                              "Please Fill in Your institute"
                          }}
                          errorLabel={
                            <span style={{ color: "red", fontSize: "12px" }} />
                          }
                        /> */}
                        <InputField
                          placeholder={getPlaceholder(
                            "For Eg: University of delhi",
                            "For Eg: University of delhi"
                          )}
                          value={val.institute}
                          idx={idx}
                          name="institute"
                          onChange={onInputChange}
                          validations={{
                            customInstitueValidationFix: (values, value) => {
                              if (val.course && !value) {
                                return false;
                              }

                              return true;
                            }
                          }}
                          validationErrors={{
                            customInstitueValidationFix:
                              "Please Fill in Your institute"
                          }}
                          errorLabel={
                            <span style={{ color: "red", fontSize: "12px" }} />
                          }
                        />
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                </InputContainer>

                <InputContainer
                  label="Degree"
                  infoicon={
                    <InfoIconLabel text={CREATE_PROFILE.BUBBLE.DEGREE} />
                  }>
                  <Grid>
                    <Grid.Row>
                      <Grid.Column mobile={16} only="mobile">
                        <InputFieldLabel
                          label="Degree"
                          infoicon={
                            <InfoIconLabel
                              text={CREATE_PROFILE.BUBBLE.DEGREE}
                            />
                          }
                        />
                      </Grid.Column>
                      <Grid.Column computer={16}>
                        <AutoCompleteSearch
                          dataType={"DEGREE"}
                          placeholder={getPlaceholder(
                            "For Eg: B.E/B.Tech",
                            "For Eg: B.E/B.Tech"
                          )}
                          value={val.course}
                          idx={idx}
                          name="course"
                          onInputChange={onInputChange}
                          validations={{
                            customCourseValidationFix: (values, value) => {
                              if (val.institute && !value) {
                                return false;
                              }

                              return true;
                            }
                          }}
                          validationErrors={{
                            customCourseValidationFix:
                              "Please Fill in Your Course"
                          }}
                          errorLabel={
                            <span style={{ color: "red", fontSize: "12px" }} />
                          }
                        />
                        {/* <InputField
                          placeholder={getPlaceholder(
                            "For Eg: B.E/B.Tech",
                            "For Eg: B.E/B.Tech"
                          )}
                          value={val.course}
                          idx={idx}
                          name="course"
                          onChange={onInputChange}
                          validations={{
                            customCourseValidationFix: (values, value) => {
                              if (val.institute && !value) {
                                return false;
                              }

                              return true;
                            }
                          }}
                          validationErrors={{
                            customCourseValidationFix:
                              "Please Fill in Your Course"
                          }}
                          errorLabel={
                            <span style={{ color: "red", fontSize: "12px" }} />
                          }
                        /> */}
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                </InputContainer>

                <InputContainer
                  label="Stream"
                  infoicon={
                    <InfoIconLabel text={CREATE_PROFILE.BUBBLE.STREAM} />
                  }>
                  <Grid>
                    <Grid.Row>
                      <Grid.Column mobile={16} only="mobile">
                        <InputFieldLabel
                          label="Stream"
                          infoicon={
                            <InfoIconLabel
                              text={CREATE_PROFILE.BUBBLE.STREAM}
                            />
                          }
                        />
                      </Grid.Column>
                      <Grid.Column computer={16}>
                        <AutoCompleteSearch
                          dataType={"STREAM"}
                          placeholder={getPlaceholder(
                            "For Eg: Computer Science & Engineering",
                            "For Eg: Computer Science & Engineering"
                          )}
                          value={val.specs[0]}
                          idx={idx}
                          name="specs"
                          onInputChange={onInputChange}
                        />
                        {/* <InputField
                          placeholder={getPlaceholder(
                            "For Eg: Computer Science & Engineering",
                            "For Eg: Computer Science & Engineering"
                          )}
                          value={val.specs[0]}
                          idx={idx}
                          name="specs"
                          onChange={onInputChange}
                        /> */}
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                </InputContainer>

                <InputContainer
                  label="Education level"
                  // infoicon={
                  //   <InfoIconLabel text={CREATE_PROFILE.BUBBLE.LEVEL} />
                  // }
                >
                  <Grid>
                    <Grid.Row>
                      <Grid.Column mobile={16} only="mobile">
                        {/* <Select
                          placeholder={getPlaceholder(
                            "Pick the closet category",
                            "Pick the closet category"
                          )}
                          name="level"
                          onChange={onInputChange}
                          value={val.level}
                          idx={idx}
                          optionsItem={[
                            { value: "Less then 12", text: "Less than 12" },
                            { value: "Under Graduate", text: "Under Graduate" },
                            { value: "Graduate", text: "Graduate" },
                            { value: "Post Graduate", text: "Post Graduate" },
                            { value: "Doctorate", text: "Doctorate" }
                          ]}
                        /> */}
                        <InputFieldLabel
                          label="Education level"
                          // infoicon={
                          //   <InfoIconLabel text="Mention the field of study" />
                          // }
                        />
                      </Grid.Column>
                      <Grid.Column computer={16}>
                        {/* <InputField
                          placeholder={getPlaceholder(
                            "For Eg: Computer Science & Engineering",
                            "For Eg: Computer Science & Engineering"
                          )}
                          value={val.level}
                          idx={idx}
                          name="level"
                          onChange={onInputChange}
                        /> */}
                        <Select
                          placeholder={getPlaceholder(
                            "Pick the closet category",
                            "Pick the closet category"
                          )}
                          optionsItem={[
                            {
                              value:
                                "Class 10th / Secondary School Certificate (SSC) / Matriculation",
                              text:
                                "Class 10th / Secondary School Certificate (SSC) / Matriculation"
                            },
                            {
                              value:
                                "Class 12th / Higher Secondary Certificate (HSC) / Intermediate (+2)",
                              text:
                                "Class 12th / Higher Secondary Certificate (HSC) / Intermediate (+2)"
                            },
                            { value: "Diploma", text: "Diploma" },
                            { value: "Undergraduate", text: "Undergraduate" },
                            {
                              value: "Graduate (Bachelors)",
                              text: "Graduate (Bachelors)"
                            },
                            {
                              value: "Post Graduate (Masters)",
                              text: "Post Graduate (Masters)"
                            },
                            { value: "Doctorate", text: "Doctorate" }
                          ]}
                          idx={idx}
                          name="level"
                          onChange={onInputChange}
                          value={val.level}
                        />
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                </InputContainer>

                <div className="education_skillYear">
                  <Grid>
                    <Grid.Row>
                      <Grid.Column width={4} className="mobile hidden">
                        <InputFieldLabel
                          label="Starting year"
                          infoicon={
                            <InfoIconLabel
                              text={CREATE_PROFILE.BUBBLE.START_YEAR}
                            />
                          }
                        />
                      </Grid.Column>
                      <Grid.Column mobile={16} only="mobile">
                        <InputFieldLabel
                          label="Starting year"
                          infoicon={
                            <InfoIconLabel
                              text={CREATE_PROFILE.BUBBLE.START_YEAR}
                            />
                          }
                        />
                      </Grid.Column>
                      <Grid.Column
                        mobile={16}
                        computer={5}
                        className="starting_yearField">
                        <InputField
                          placeholder={getPlaceholder(
                            "For Eg: 2018",
                            "For Eg: 2018"
                          )}
                          inputtype="number"
                          min="1800"
                          max="2019"
                          value={val.started}
                          idx={idx}
                          name="started"
                          validations={{
                            customEduStartedValidation: (values, value) => {
                              if (Number(!val.completed)) {
                                return true;
                              }

                              return Number(value) <= Number(val.completed);
                            }
                          }}
                          validationErrors={{
                            customEduStartedValidation:
                              "Cannot be greater than completed year"
                          }}
                          errorLabel={
                            <span style={{ color: "red", fontSize: "12px" }} />
                          }
                          onChange={onInputChange}
                        />
                      </Grid.Column>
                      <Grid.Column
                        width={2}
                        // className="mobile hidden completed_yearColumn"
                        className="mobile hidden completed_yearColumn"
                        textAlign="center">
                        <InputFieldLabel label="Completing year " />
                      </Grid.Column>
                      <Grid.Column mobile={16} only="mobile">
                        <InputFieldLabel
                          label="Completing year"
                          infoicon={<InfoIconLabel />}
                        />
                      </Grid.Column>
                      <Grid.Column mobile={16} computer={5}>
                        <InputField
                          inputtype="number"
                          value={val.completed}
                          idx={idx}
                          name="completed"
                          onChange={onInputChange}
                          min="1800"
                          max="2018"
                          errorLabel={
                            <span style={{ color: "red", fontSize: "12px" }} />
                          }
                          validations={{
                            customEduCompletedValidation: (values, value) => {
                              if (Number(!val.started)) {
                                return true;
                              }
                              return Number(value) >= Number(val.started);
                            }
                          }}
                          validationErrors={{
                            customEduCompletedValidation:
                              "Cannot be smaller than started year"
                          }}
                          placeholder={getPlaceholder(
                            "For Eg: 2019",
                            "For Eg: 2019"
                          )}
                        />
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                </div>
                <InputContainer>
                  <CheckboxField
                    name="onInfo"
                    checked={val.onInfo}
                    idx={idx}
                    onChange={() => onCheckInfoEdu(val._id)}
                    checkboxLabel="Show above qualification in my profile header"
                  />
                </InputContainer>
              </div>
            );
          })}

          <InputContainer className="addMore_btnForm">
            <FlatDefaultBtn
              btnicon={<IcPlusIcon pathcolor="#c8c8c8" />}
              btntext="Add more education"
              type="button"
              onClick={onAddMoreClick}
              // className="addMore_btn"
            />
          </InputContainer>
        </InfoSection>
      </div>
    );
  }
}

export default Education;
