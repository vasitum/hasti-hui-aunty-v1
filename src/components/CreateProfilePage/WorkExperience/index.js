import React from "react";

import { Grid } from "semantic-ui-react";
import InfoSection from "../../Sections/InfoSection";
import InputContainer from "../../Forms/InputContainer";
import InputField from "../../Forms/FormFields/InputField";
import SelectOptionField from "../../Forms/FormFields/SelectOptionField";
import LocationInputField from "../../Forms/FormFields/LocationInputField";
import QuillText from "../../CardElements/QuillText";
import InputFieldLabel from "../../Forms/FormFields/InputFieldLabel";

import IcPlusIcon from "../../../assets/svg/IcPlus";

import FlatDefaultBtn from "../../Buttons/FlatDefaultBtn";
import CheckboxField from "../../Forms/FormFields/CheckboxField";

import getPlaceholder from "../../Forms/FormFields/Placeholder";
import SearchFilterInput from "../../Forms/FormFields/SearchFilterInput";

import InfoIconLabel from "../../utils/InfoIconLabel";
import SkipNowCollaps from "../../utils/SkipNowCollaps";

import CloseIcon from "../../utils/CloseIcon";
import AutoCompleteSearch from "../../Forms/FormFields/AutoCompleteSearch";
import { CREATE_PROFILE } from "../../../strings";
import "./index.scss";

const autoSearchOptionsFeild = [
  { key: "afq2", value: "", flag: "af", text: "Select" },
  { key: "af", value: "af", flag: "af", text: "Afghanistan" },
  { key: "india", value: "india", flag: "india", text: "india" },
  { key: "us", value: "us", flag: "us", text: "US" }
  // { key: 'af', value: 'af', flag: 'af', text: 'Afghanistan' },
  // { key: 'af', value: 'af', flag: 'af', text: 'Afghanistan' },
  // { key: 'af', value: 'af', flag: 'af', text: 'Afghanistan' },
];

function compareDorToDojMonth(dorMonth, dojMonth, dorYear, dojYear) {
  const MONTH_MAP = [
    { idx: 1, value: "Jan", text: "January" },
    { idx: 2, value: "Feb", text: "February" },
    { idx: 3, value: "Mar", text: "March " },
    { idx: 4, value: "Apr", text: "April" },
    { idx: 5, value: "May", text: "May" },
    { idx: 6, value: "June", text: "June" },
    { idx: 7, value: "July", text: "July" },
    { idx: 8, value: "Aug", text: "August" },
    { idx: 9, value: "Sep", text: "September" },
    { idx: 10, value: "Oct", text: "October" },
    { idx: 11, value: "Nov", text: "November" },
    { idx: 12, value: "Dec", text: "December" }
  ];

  const dojMonthNumber = MONTH_MAP.filter(val => {
    if (dojMonth === val.value) {
      return true;
    }

    return false;
  });

  if (
    !dojMonthNumber ||
    !Array.isArray(dojMonthNumber) ||
    !dojMonthNumber.length
  ) {
    return true;
  }

  if (Number(dorMonth) >= Number(dojMonthNumber[0].idx)) {
    return true;
  }

  if (Number(dorYear) >= Number(dojYear)) {
    return true;
  }

  return false;
}

class WorkExperience extends React.Component {
  render() {
    const {
      data,
      onAddMoreClick,
      onRemoveClick,
      onInputChange,
      onQuillChange,
      isEditProfile,
      onCheckInfoExp,
      isActive
    } = this.props;
    return (
      <div className="panel-cardBody work_experience" id="experience">
        <InfoSection
          headerSize="large"
          color="blue"
          headerText="Work experience"
          rightIcon={!isEditProfile && <SkipNowCollaps />}
          collapsible
          isActive={isActive}>
          {data.exps.map((val, idx) => (
            <div className="New_add" key={idx}>
              {/* {idx ? <CloseIcon onClick={() => onRemoveClick(idx)} /> : null} */}
              {data.exps.length > 1 && (
                <CloseIcon onClick={() => onRemoveClick(idx)} />
              )}

              <InputContainer
                label={idx === 0 ? "Current employer" : "Previous employer"}
                infoicon={
                  <InfoIconLabel
                    text={
                      idx === 0
                        ? CREATE_PROFILE.BUBBLE.EXPERIENCE_CURRENT_EMPLOYER
                        : CREATE_PROFILE.BUBBLE.EXPERIENCE_PREVIOUS_EMPLOYER
                    }
                  />
                }>
                <Grid>
                  <Grid.Row>
                    <Grid.Column mobile={16} only="mobile">
                      <InputFieldLabel
                        label={
                          idx === 0 ? "Current employer" : "Previous employer"
                        }
                        infoicon={
                          <InfoIconLabel
                            text={
                              idx === 0
                                ? CREATE_PROFILE.BUBBLE
                                    .EXPERIENCE_CURRENT_EMPLOYER
                                : CREATE_PROFILE.BUBBLE
                                    .EXPERIENCE_PREVIOUS_EMPLOYER
                            }
                          />
                        }
                      />
                    </Grid.Column>
                    <Grid.Column computer={16}>
                      <AutoCompleteSearch
                        dataType={"COMPNAME"}
                        placeholder={getPlaceholder(
                          " For Eg: Google, Microsoft, Wipro",
                          " For Eg: Google, Microsoft, Wipro"
                        )}
                        onInputChange={onInputChange}
                        idx={idx}
                        name="name"
                        value={val.name}
                        validations={{
                          customNameValidationFix: (values, value) => {
                            if (val.designation && !value) {
                              return false;
                            }

                            return true;
                          }
                        }}
                        validationErrors={{
                          customNameValidationFix: "Please Mention Company Name"
                        }}
                        errorLabel={
                          <span style={{ color: "red", fontSize: "12px" }} />
                        }
                      />
                      {/* <InputField
                        placeholder={getPlaceholder(
                          " For Eg: Google, Microsoft, Wipro",
                          " For Eg: Google, Microsoft, Wipro"
                        )}
                        onChange={onInputChange}
                        idx={idx}
                        name="name"
                        value={val.name}
                        validations={{
                          customNameValidationFix: (values, value) => {
                            if (val.designation && !value) {
                              return false;
                            }

                            return true;
                          }
                        }}
                        validationErrors={{
                          customNameValidationFix: "Please Mention Company Name"
                        }}
                        errorLabel={
                          <span style={{ color: "red", fontSize: "12px" }} />
                        }
                      /> */}
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </InputContainer>

              <InputContainer
                label="Designation"
                infoicon={
                  <InfoIconLabel
                    text={CREATE_PROFILE.BUBBLE.EXPERIENCE_DESIGNATION}
                  />
                }>
                <Grid>
                  <Grid.Row>
                    <Grid.Column mobile={16} only="mobile">
                      <InputFieldLabel
                        label="Designation"
                        infoicon={
                          <InfoIconLabel
                            text={CREATE_PROFILE.BUBBLE.EXPERIENCE_DESIGNATION}
                          />
                        }
                      />
                    </Grid.Column>
                    <Grid.Column computer={16}>
                      <AutoCompleteSearch
                        dataType={"ROLE"}
                        placeholder={getPlaceholder(
                          " For Eg: Software Developer",
                          " For Eg: Software Developer"
                        )}
                        onInputChange={onInputChange}
                        idx={idx}
                        name="designation"
                        value={val.designation}
                        validations={{
                          customDesignationValidationFix: (values, value) => {
                            if (val.name && !value) {
                              return false;
                            }

                            return true;
                          }
                        }}
                        validationErrors={{
                          customDesignationValidationFix:
                            "Please Mention Your Designation"
                        }}
                        errorLabel={
                          <span style={{ color: "red", fontSize: "12px" }} />
                        }
                      />
                      {/* <InputField
                        placeholder={getPlaceholder(
                          " For Eg: Software Developer",
                          " For Eg: Software Developer"
                        )}
                        onChange={onInputChange}
                        idx={idx}
                        name="designation"
                        value={val.designation}
                        validations={{
                          customDesignationValidationFix: (values, value) => {
                            if (val.name && !value) {
                              return false;
                            }

                            return true;
                          }
                        }}
                        validationErrors={{
                          customDesignationValidationFix:
                            "Please Mention Your Designation"
                        }}
                        errorLabel={
                          <span style={{ color: "red", fontSize: "12px" }} />
                        }
                      /> */}
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </InputContainer>

              <InputContainer
                label="Industry"
                // infoicon={
                //   <InfoIconLabel
                //     text={CREATE_PROFILE.BUBBLE.EXPERIENCE_INDUSTRY}
                //   />
                // }
              >
                <Grid>
                  <Grid.Row>
                    <Grid.Column mobile={16} only="mobile">
                      <InputFieldLabel
                        label="Industry"
                        // infoicon={
                        //   <InfoIconLabel
                        //     text={CREATE_PROFILE.BUBBLE.EXPERIENCE_INDUSTRY}
                        //   />
                        // }
                      />
                    </Grid.Column>
                    <Grid.Column computer={16}>
                      <AutoCompleteSearch
                        dataType={"INDUSTRY"}
                        placeholder={getPlaceholder(
                          " For Eg: IT, Marketing, Recruitment",
                          " For Eg: IT, Marketing, Recruitment"
                        )}
                        onInputChange={onInputChange}
                        idx={idx}
                        name="industry"
                        value={val.industry}
                      />
                      {/* <InputField
                        placeholder={getPlaceholder(
                          " For Eg: IT, Marketing, Recruitment",
                          " For Eg: IT, Marketing, Recruitment"
                        )}
                        onChange={onInputChange}
                        idx={idx}
                        name="industry"
                        value={val.industry}
                      /> */}
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </InputContainer>

              <InputContainer
                label="Company location"
                infoicon={
                  <InfoIconLabel
                    text={CREATE_PROFILE.BUBBLE.EXPERIENCE_LOCATION}
                  />
                }>
                <Grid>
                  <Grid.Row>
                    <Grid.Column mobile={16} only="mobile">
                      <InputFieldLabel
                        label="Company location"
                        infoicon={
                          <InfoIconLabel
                            text={CREATE_PROFILE.BUBBLE.EXPERIENCE_LOCATION}
                          />
                        }
                      />
                    </Grid.Column>
                    <Grid.Column computer={16}>
                      <InputField
                        placeholder={getPlaceholder(
                          " For Eg: London, Noida, Delhi.",
                          " For Eg: London, Noida, Delhi."
                        )}
                        onChange={onInputChange}
                        idx={idx}
                        name="loc"
                        value={val.loc}
                      />
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </InputContainer>

              <InputContainer
                label="Describe your job profile"
                // infoicon={
                //   <InfoIconLabel
                //     text={CREATE_PROFILE.BUBBLE.EXPERIENCE_DESCR}
                //   />
                // }
              >
                <Grid>
                  <Grid.Row>
                    <Grid.Column mobile={16} only="mobile">
                      <InputFieldLabel
                        label="Describe your job profile"
                        // infoicon={
                        //   <InfoIconLabel
                        //     text={CREATE_PROFILE.BUBBLE.EXPERIENCE_DESCR}
                        //   />
                        // }
                      />
                    </Grid.Column>
                    <Grid.Column computer={16}>
                      <QuillText
                        name="desc"
                        defaultValue={val.desc}
                        value={val.desc}
                        onChange={html => onQuillChange(html, idx)}
                        placeholder={
                          CREATE_PROFILE.BUBBLE.EXPERIENCE_JOB_PRO_DESC
                        }
                      />
                      <div
                        style={{
                          textAlign: "right",
                          color: "#ada1b9"
                        }}>
                        {val.desc.length && val.desc.length !== 11
                          ? val.desc.length
                          : 0}{" "}
                        / 2000
                      </div>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </InputContainer>

              <div className="modal_monthField">
                <Grid>
                  <Grid.Row>
                    <Grid.Column width={4} className="mobile hidden">
                      <InputFieldLabel label="From" />
                    </Grid.Column>
                    <Grid.Column mobile={16} only="mobile">
                      <InputFieldLabel
                        label="From"
                        infoicon={<InfoIconLabel />}
                      />
                    </Grid.Column>
                    <Grid.Column mobile={16} computer={6} className="month">
                      <SelectOptionField
                        placeholder={getPlaceholder("Month", "Month")}
                        optionsItem={[
                          { value: "Jan", text: "January" },
                          { value: "Feb", text: "February" },
                          { value: "Mar", text: "March " },
                          { value: "Apr", text: "April" },
                          { value: "May", text: "May" },
                          { value: "June", text: "June" },
                          { value: "July", text: "July" },
                          { value: "Aug", text: "August" },
                          { value: "Sep", text: "September" },
                          { value: "Oct", text: "October" },
                          { value: "Nov", text: "November" },
                          { value: "Dec", text: "December" }
                        ]}
                        validations={{
                          customDojExpValidation: (values, value) => {
                            if (!val.doj.month || !val.dor.month) {
                              return true;
                            }

                            return compareDorToDojMonth(
                              val.dor.month,
                              val.doj.month,
                              val.dor.year,
                              val.doj.year
                            );
                          }
                        }}
                        validationErrors={{
                          customDojExpValidation: "Invalid Month"
                        }}
                        errorLabel={
                          <span style={{ color: "red", fontSize: "12px" }} />
                        }
                        onChange={onInputChange}
                        value={val.doj.month}
                        name="doj_month"
                        idx={idx}
                      />
                    </Grid.Column>

                    <Grid.Column mobile={16} computer={6}>
                      <InputField
                        inputtype="number"
                        min="1800"
                        max="2018"
                        onChange={onInputChange}
                        value={val.doj.year}
                        name="doj_year"
                        validations={{
                          customDojExpValidation: (values, value) => {
                            if (!val.doj.year || !val.dor.year) {
                              return true;
                            }

                            return Number(value) <= Number(val.dor.year);
                          }
                        }}
                        validationErrors={{
                          customDojExpValidation:
                            "Cannot be greater than leave year"
                        }}
                        errorLabel={
                          <span style={{ color: "red", fontSize: "12px" }} />
                        }
                        idx={idx}
                        placeholder="Year"
                      />
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </div>

              <InputContainer className="Input_check">
                <CheckboxField
                  name="current"
                  checked={val.current}
                  idx={idx}
                  onChange={onInputChange}
                  checkboxLabel="I currently work here"
                />

                <CheckboxField
                  name="onInfo"
                  checked={val.onInfo}
                  idx={idx}
                  onChange={() => onCheckInfoExp(val._id)}
                  checkboxLabel="Show above experience in my profile header"
                />
              </InputContainer>
              {!val.current && (
                <div className="">
                  <Grid>
                    <Grid.Row>
                      <Grid.Column width={4} className="mobile hidden">
                        <InputFieldLabel label="To" />
                      </Grid.Column>
                      <Grid.Column mobile={16} only="mobile">
                        <InputFieldLabel
                          label="To"
                          infoicon={<InfoIconLabel />}
                        />
                      </Grid.Column>
                      <Grid.Column mobile={16} computer={6} className="month">
                        <SelectOptionField
                          placeholder={getPlaceholder("Month", "Month")}
                          optionsItem={[
                            { value: "1", text: "January" },
                            { value: "2", text: "February" },
                            { value: "3", text: "March " },
                            { value: "4", text: "April" },
                            { value: "5", text: "May" },
                            { value: "6", text: "June" },
                            { value: "7", text: "July" },
                            { value: "8", text: "August" },
                            { value: "9", text: "September" },
                            { value: "10", text: "October" },
                            { value: "11", text: "November" },
                            { value: "12", text: "December" }
                          ]}
                          validations={{
                            customDojExpValidation: (values, value) => {
                              if (!val.doj.month || !val.dor.month) {
                                return true;
                              }

                              return compareDorToDojMonth(
                                val.dor.month,
                                val.doj.month,
                                val.dor.year,
                                val.doj.year
                              );
                            }
                          }}
                          validationErrors={{
                            customDojExpValidation: "Invalid Month"
                          }}
                          errorLabel={
                            <span style={{ color: "red", fontSize: "12px" }} />
                          }
                          onChange={onInputChange}
                          value={val.dor.month}
                          name="dor_month"
                          idx={idx}
                        />
                      </Grid.Column>

                      <Grid.Column mobile={16} computer={6}>
                        <InputField
                          inputtype="number"
                          min="1800"
                          max="2018"
                          onChange={onInputChange}
                          value={val.dor.year}
                          name="dor_year"
                          idx={idx}
                          validations={{
                            customDojExpValidation: (values, value) => {
                              if (!val.dor.year) {
                                return true;
                              }

                              return Number(value) >= Number(val.doj.year);
                            }
                          }}
                          validationErrors={{
                            customDojExpValidation:
                              "Cannot be smaller than joining year"
                          }}
                          errorLabel={
                            <span style={{ color: "red", fontSize: "12px" }} />
                          }
                          placeholder="Year"
                        />
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                </div>
              )}
            </div>
          ))}

          <InputContainer className="addMore_btnForm">
            <FlatDefaultBtn
              btnicon={<IcPlusIcon pathcolor="#c8c8c8" />}
              btntext="Add more experience"
              type="button"
              onClick={onAddMoreClick}
            />
          </InputContainer>
        </InfoSection>
      </div>
    );
  }
}

export default WorkExperience;
