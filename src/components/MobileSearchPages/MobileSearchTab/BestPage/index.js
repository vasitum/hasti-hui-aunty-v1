import React from "react";

// import { Link } from 'react-router-dom';
import { Tab, Menu } from "semantic-ui-react";

import SearchBanner from "../../../Banners/SearchBanner";
import JobMediaCard from "../../../Cards/JobMediaCard";
import PeopleMediaCard from "../../../Cards/PeopleMediaCard";
import "./index.scss";

class BestPage extends React.Component {
  render() {
    return (
      <div className="BestPage">
        <div className="mobileSearch_jobCard">
          <SearchBanner />
          <JobMediaCard />
        </div>

        <div className="mobileSearch_People">
          <SearchBanner type="people" />
          <PeopleMediaCard />
        </div>
      </div>
    );
  }
}

export default BestPage;
