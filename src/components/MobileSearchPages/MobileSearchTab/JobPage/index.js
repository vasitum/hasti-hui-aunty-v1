import React from 'react';

// import { Link } from 'react-router-dom';
import { Tab, Menu } from "semantic-ui-react";
import SearchFilterBtn from "../SearchFilterBtn";

import SearchBanner from "../../../Banners/SearchBanner";
import JobMediaCard from "../../../Cards/JobMediaCard";
class JobPage extends React.Component {
  

  render() {
    return (
      <div className="BestPage">
        <SearchFilterBtn />

        <div className="mobileSearch_jobCard">
          <SearchBanner />
          <JobMediaCard />
        </div>
      </div>
    );
  }
}
  
  export default JobPage;