import React from 'react';

// import { Link } from 'react-router-dom';
import { Tab, Menu } from "semantic-ui-react";


import BestPage from "./BestPage";
import JobPage from "./JobPage";
import MobilePeoplePaneTab from "./PeoplePage";

import "./index.scss";



const MobileSearch = [
  {
    menuItem: (
      <Menu.Item key="Best">
        Best
      </Menu.Item>
    ),
    render: () => {
      return (
        <div className="BestPage">
          <Tab.Pane attached={false}>
            <BestPage />
          </Tab.Pane>
        </div>
      );
    }
  },

  {
    menuItem: (
      <Menu.Item key="Job">
        Job 
      </Menu.Item>
    ),
    render: (props) => {
      return (
        <div className="">
          <Tab.Pane attached={false}>
            <JobPage  />
          </Tab.Pane>
        </div>
      );
    }
  },

  {
    menuItem: (
      <Menu.Item  key="People">
        People
      </Menu.Item>
    ),
    render: () => {
      return (
        <div className="">
          <Tab.Pane attached={false}>
            <MobilePeoplePaneTab  />
          </Tab.Pane>
        </div>
      );
    }
  }
];

  class MobileSearchTab extends React.Component {
    constructor(props) {
      super(props);
      this.state = {};
    }
  
    render() {
      return (
        <div className="MobileSearchTab">
          <Tab
            menu={{ text: true }}
            panes={MobileSearch}
            // sidebarToggle={this.props.sidebarToggle}
          />
        </div>
      );
    }
  }
  
  export default MobileSearchTab;