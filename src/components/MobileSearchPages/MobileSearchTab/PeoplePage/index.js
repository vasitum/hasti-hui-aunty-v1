import React from 'react';

// import { Link } from 'react-router-dom';
import { Tab, Menu } from "semantic-ui-react";
import SearchFilterBtn from "../SearchFilterBtn";

import SearchBanner from "../../../Banners/SearchBanner";
import PeopleMediaCard from "../../../Cards/PeopleMediaCard";
class PeoplePage extends React.Component {
  

  render() {
    return (
      <div className="BestPage">
        <SearchFilterBtn />

        <div className="mobileSearch_People">
          <SearchBanner type="people"/>
          <PeopleMediaCard />
          <PeopleMediaCard />
        </div>
      </div>
    );
  }
}
  
  export default PeoplePage;