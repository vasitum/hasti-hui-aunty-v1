import React from 'react';

// import { Link } from 'react-router-dom';
import { Button, Sidebar, Menu, Icon, Segment, Header } from "semantic-ui-react";
import IcFilterIcon from "../../../../assets/svg/IcFilterIcon";
import IcSearchAlertIcon from "../../../../assets/svg/IcSearchAlertIcon";

import "./index.scss";

class SearchFilterBtn extends React.Component {


  render() {
    return (
      <div className="SearchFilterBtn">

        <div className="SearchFilterBtn_header">
          <Button className="jobFilter_Btn"><IcFilterIcon pathcolor="#c9c9c9" height="11"/> Filter Jobs</Button>
          <Button className="search__alertBtn"><IcSearchAlertIcon pathcolor="#c9c9c9" rectcolor="#c8c8c8" width="14.38" height="12.187" />Get search alert</Button>
        </div>
        <div>
          <div>
            {/* <Sidebar.Pushable as={Segment}>
              <Sidebar as={Menu} animation='overlay' width='thin' visible={visible} icon='labeled' vertical inverted>

              </Sidebar>
              <Sidebar.Pusher>
                <Segment basic>
                  <Header as='h3'>Application Content</Header>

                </Segment>
              </Sidebar.Pusher>
            </Sidebar.Pushable> */}
          </div>
        </div>
      </div>
    );
  }
}

export default SearchFilterBtn;