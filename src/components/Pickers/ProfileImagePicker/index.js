import React, { Component } from "react";

import ReactAvatarEditor from "react-avatar-editor";

import { Modal, Card } from "semantic-ui-react/dist/commonjs";

import ModalBanner from "../../ModalBanner";

import FlatDefaultBtn from "../../Buttons/FlatDefaultBtn";
import ActionBtn from "../../Buttons/ActionBtn";

import "./index.scss";

/**
 * ProfileImagePicker:
 * Image Picker for Mobile and data
 *
 *
 * @prop
 * @required
 * onSave: Function
 *
 * @prop
 * @required
 * onCancel: Function
 *
 * @prop
 * @required
 * open: Boolean
 *
 * @prop
 * @required
 * src: URL
 */
export default class extends Component {
  constructor(props) {
    super(props);
    this.editor = null;
  }

  state = {
    loading: false
  };

  componentWillUpdate() {
    this.fixBody();
  }

  componentDidUpdate() {
    this.fixBody();
  }

  fixBody() {
    const anotherModal = document.getElementsByClassName("ui page modals")
      .length;
    if (anotherModal > 0)
      document.body.classList.add("scrolling", "dimmable", "dimmed");
  }

  onSave = ev => {
    this.setState({ loading: true });
    if (this.editor) {
      const mCanvas = this.editor.getImageScaledToCanvas();
      const image = mCanvas.toDataURL("image/jpeg");

      // send it to state
      this.setState({ loading: false });
      this.props.onSave(image);
      return;
    }
    this.setState({ loading: false });
    return this.props.onSave(this.props.src);
  };

  setEditorRef = editor => (this.editor = editor);

  render() {
    return (
      <Modal
        onClose={this.props.onCancel}
        className="modal_form avatar__modal"
        open={this.props.open}
        closeIcon
        closeOnDimmerClick={false}>
        <div
          className="card_panel"
          style={{
            textAlign: "center",
            background: "#f7f7fb"
          }}>
          <ModalBanner headerText="Select Image" />
          <ReactAvatarEditor
            ref={this.setEditorRef}
            image={this.props.src}
            width={200}
            height={200}
            border={50}
            scale={1}
          />

          <div
            className="ModalFooterBtn"
            style={{
              paddingBottom: "15px",
              paddingRight: "10px"
            }}>
            <FlatDefaultBtn
              onClick={this.props.onCancel}
              type="button"
              disabled={this.state.loading}
              btntext="Cancel"
            />
            <ActionBtn
              onClick={this.onSave}
              type="button"
              loading={this.state.loading}
              actioaBtnText="Save"
            />
          </div>
        </div>
      </Modal>
    );
  }
}
