import React from "react";


import  { Grid } from "semantic-ui-react";
import SearchPageContainer from "../SearchPageContainer";

import BestPaneLeft from "./BestPaneLeft";
import BestPaneRight from "./BestPaneRight";



class BestPage extends React.Component{
  render(){
    return(
      <div className="BestPage">
        <SearchPageContainer leftSidePane={<BestPaneLeft />}>
          <BestPaneRight />
        </SearchPageContainer>
      </div>
    )
  }
}

export default BestPage;