import React from "react";

import  {List, Header} from "semantic-ui-react";
import UserSignUp from "../../../SearchPageComponent/UserSignUp";

import "./index.scss";


class BestPaneLeft extends React.Component{
  render(){
    return(
      <div className="BestPaneLeft">
        <List verticalAlign="middle">
          <List.Item>
            <List.Content>
              <List.Header>
                <div className="bestPane_header">
                  <Header as="h3">
                    {" "}
                    Sign Up
                  </Header>
                  <p>
                    Apply now &amp; increase your chances
                  </p>
                </div>
              </List.Header>
            </List.Content>
          </List.Item>
        </List>

        <UserSignUp className="displayNone" siginbtntext="Get started for free"/>
      </div>
    )
  }
}

export default BestPaneLeft;