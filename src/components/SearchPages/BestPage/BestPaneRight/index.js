import React from "react";


import  { Grid } from "semantic-ui-react";

import SearchBanner from "../../../Banners/SearchBanner";
import JobBestCard from "../../../Cards/JobBestCard";
import PeopleBestCard from "../../../Cards/PeopleBestCard";

import "./index.scss";


class BestPanelRight extends React.Component{
  render(){
    return(
      <div className="BestPanelRight">
        <div className="BestPanel_job">
          <SearchBanner btnText="Sign up and apply job"/>
          <JobBestCard />
        </div>

        <div className="BestPanel_people">
          <SearchBanner type="people" headerText="People" btnText="Sign up and post job"/>
          <PeopleBestCard />
        </div>

      </div>
    )
  }
}

export default BestPanelRight;