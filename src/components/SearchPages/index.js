import React from "react";

import { Tab, Menu, Label, Container } from "semantic-ui-react";

// import { } from "../Banners/"

import BestPage from "./BestPage";
import JobPage from "./JobPage";
import PeoplePage from "./PeoplePage";

// import BannerHeader from "../BannerHeader";
import PageBanner from "../Banners/PageBanner";
import aunty from "../../assets/banners/aunty.png";

import "./index.scss";


const job = [
  {
    menuItem: (
      <Menu.Item>
        Best
      </Menu.Item>
    ),
    render: () => {
      return (
        <div className="bestTab">
          <Tab.Pane attached={false}>
            <BestPage />
          </Tab.Pane>
        </div>
      );
    }
  },

  {
    menuItem: (
      <Menu.Item >
        Job <Label floating>105</Label>
      </Menu.Item>
    ),
    render: () => {
      return (
        <div className="">
          <Tab.Pane attached={false}>
            <JobPage />
          </Tab.Pane>
        </div>
      );
    }
  },

  {
    menuItem: (
      <Menu.Item>
        People<Label floating>4</Label>
      </Menu.Item>
    ),
    render: () => {
      return (
        <div className="">
          <Tab.Pane attached={false}>
            <PeoplePage />
          </Tab.Pane>
        </div>
      );
    }
  }
];

class SearchPages extends React.Component{
  
  constructor(props) {
    super(props);
    this.state = {};
  }

  // searchIndex = {
  //   BEST: 0,
  //   JOB: 1,
  //   PEOPLE: 2
  // };

  // getActiveTab = () => {
  //   const { match } = this.props;

  //   if (!match.params) {
  //     return 0;
  //   }

  //   switch (match.params.pageid) {
  //     case "job":
  //       return 1;

  //     case "people":
  //       return 2;

  //     default:
  //       return 0;
  //   }
  // };

  render(){
    return(
      <div className="SearchPages">
        <PageBanner size="small" image={aunty}/>

        <div className="SearchPages_tab">
          <Container>
            <Tab
              menu={{ text: true }}
              panes={job}
              // activeIndex={this.getActiveTab()}
            />
          </Container>
        </div>
      </div>
    )
  }
}

export default SearchPages;