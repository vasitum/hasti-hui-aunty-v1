import React from "react";


import  { Grid } from "semantic-ui-react";
import SearchPageContainer from "../SearchPageContainer";

import PeoplePaneLeft from "./PeoplePaneLeft";
import PeoplePaneRight from "./PeoplePaneRight";



class PeoplePage extends React.Component{
  render(){
    return(
      <div className="PeoplePage">
        <SearchPageContainer leftSidePane={<PeoplePaneLeft />}>
          <PeoplePaneRight />
        </SearchPageContainer>
      </div>
    )
  }
}

export default PeoplePage;