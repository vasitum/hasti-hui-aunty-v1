import React from "react";


import { Grid } from "semantic-ui-react";

class SearchPageContainer extends React.Component{
  render(){
    const { leftSidePane, children } = this.props;
    return(
      <div className="SearchPageContainer">
        <Grid>
          <Grid.Row>
            {/* <Grid.Column width={4}>
              { leftSidePane }
            </Grid.Column> */}
            <Grid.Column width={16}>
              { children }
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    )
  }
}

export default SearchPageContainer