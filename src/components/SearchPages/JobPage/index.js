import React from "react";


import  { Grid } from "semantic-ui-react";
import SearchPageContainer from "../SearchPageContainer";

import ElevateContainer from "../../Pages/ElevateContainer";
import JobPaneLeft from "./JobPaneLeft";
import JobPaneRight from "./JobPaneRight";




class JobPage extends React.Component{
  render(){
    return(
      <div className="JobPage">
        {/* <SearchPageContainer>
          
        </SearchPageContainer> */}
        <SearchPageContainer leftSidePane={<JobPaneLeft />}>
          <JobPaneRight />
        </SearchPageContainer>
      </div>
    )
  }
}

export default JobPage;