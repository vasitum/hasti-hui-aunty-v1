import React from "react";

import { Grid, Header, Button } from "semantic-ui-react";
import PropTypes from "prop-types";

import InputButton from "../../../InputButton";
import EmailAlertCard from "../../../Cards/EmailAlertCard";
import CardInputButton from "../../../Cards/CardInputButton";
import IcCloseIcon from "../../../../assets/svg/IcCloseIcon";

import "./index.scss";


const JobPaneLeftHeader = props => {
  
  const { filterFor, filterValue } = props;

  return (
    <div className="JobPaneLeftHeader">
      {/* <EmailAlertCard /> */}

      <CardInputButton />

      <Grid>

        {/* mobile header */}
        <Grid.Row only='mobile' className="mobile_Header">
          <Grid.Column width={8} className="topBottom_center" >
            <Header as="h3">Filter people</Header>
          </Grid.Column>
          <Grid.Column width={8} className="" textAlign="right">

            <Button className="apply_btn" basic>
              Apply
            </Button>

            <Button inverted className="close_btn" color="#1f2532">
              <IcCloseIcon pathcolor="#6e768a" height="14" width="14" />
            </Button>

          </Grid.Column>
        </Grid.Row>
        <Grid.Row only='mobile' className="mobileHeader_clearBtn">
          <Grid.Column width={16}>
            <Button compact inverted className="allClear_btn">
              <IcCloseIcon pathcolor="#0b9ed0" height="10" width="10" />
            </Button>
            <span>Clear all applied filters</span>
          </Grid.Column>
        </Grid.Row>
        {/* mobile header */}

        <Grid.Row only='computer'>
          <Grid.Column width={9}>
            <Header as="h2">
              Filter {filterFor} by
            </Header>
          </Grid.Column>
          <Grid.Column width={7} textAlign="right">
            <Button
              className="filterValue_Btn"
              // onClick={() => refine(items)}
              // disabled={!canRefine}
              size="mini"
              compact
              disabled
              inverted>
              Clear All ({filterValue})
            </Button>
          </Grid.Column>
        </Grid.Row>

        
      </Grid>
    </div>
  )

}

JobPaneLeftHeader.propTypes = {
  filterFor: PropTypes.string,
  filterValue: PropTypes.string
};

JobPaneLeftHeader.defaultProps = {
  filterFor: "Job",
  filterValue: "10"
};

export default JobPaneLeftHeader;