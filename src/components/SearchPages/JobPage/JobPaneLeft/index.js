import React from "react";

import  { Icon, Label, Grid } from "semantic-ui-react";

import JobPaneLeftHeader from "../JobPaneLeftHeader";
import FilterCollapsible from "../../../SearchPageComponent/FilterCollapsible";

import InputField from "../../../Forms/FormFields/InputField";
import CheckboxField from "../../../Forms/FormFields/CheckboxField";

import "./index.scss";


class JobPaneLeft extends React.Component{
  render(){
    
    const data = [
      {
        checkboxLabel: "Bangalore",
        numberLabel: 3,
      },

      {
        checkboxLabel: "Noida",
        numberLabel: 5,
      },

      {
        checkboxLabel: "Pune",
        numberLabel: 6,
      },

      {
        checkboxLabel: "Gurgaon",
        numberLabel: 8,
      },

      {
        checkboxLabel: "Bengaluru",
        numberLabel: 32,
      },

      {
        checkboxLabel: "Chennai",
        numberLabel: 5,
      },

      {
        checkboxLabel: "Ahmednagar",
        numberLabel: 7,
      },

      {
        checkboxLabel: "Faridabad",
        numberLabel: 31,
      },

      {
        checkboxLabel: "Mumbai",
        numberLabel: 42,
      },

      {
        checkboxLabel: "Hyderabad",
        numberLabel: 1,
      },
    ]

    return(
      <div className="JobPaneLeft">
        <JobPaneLeftHeader />

        <FilterCollapsible
          headerSize="large"
          // color="blue"
          headerText="Location"
          headerIcon={<Icon name="angle down" className="vas_accordionArrow" circular />}
          collapsible>

          <div className="">
            <div className="filter-inputField">
              <InputField />
            </div>

            {
              data.map((item, idx) =>{
                return(
                  <div className="filter-checkbox" idx={idx}>
                    <Grid>
                      <Grid.Row>
                        <Grid.Column width={13}>
                          <CheckboxField checkboxLabel={item.checkboxLabel}/>
                        </Grid.Column>
                        <Grid.Column width={3}>
                          <div className="filter_label">
                            <Label size="large">{item.numberLabel}</Label>
                          </div>
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                  </div>
                )
              })
            }
          </div>
        </FilterCollapsible>
      </div>
    )
  }
}

export default JobPaneLeft;