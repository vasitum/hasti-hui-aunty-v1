import React from "react";

import { Grid } from "semantic-ui-react";

import SearchBanner from "../../../Banners/SearchBanner";
import RecentDropdown from "../../../Dropdown/RecentDropdown";
import InfoCard from "../../../Cards/JobInfoCard";
import InfoSection from "../../../Sections/InfoSection";

import SkillList from "../../../CardElements/SkillList";
import FlatDefaultBtn from "../../../Buttons/FlatDefaultBtn";

import SearchCardFooter from "../../../Cards/SearchCard/SearcCardFooter";
import MediaCard from "../../../Cards/MediaCard"

import "./index.scss";


class JobPaneRight extends React.Component {



  render() {

    return (
      <div className="JobPaneRight">
        <SearchBanner
          recentDropdownMenu={
            <RecentDropdown defaultRefinement="job"
              items={[
                { value: "job", text: "Relevant" },
                { value: "job_by_date", text: "Recent" }
              ]}
            />
          }
        />

        <InfoCard>

          <div className="InfoCard_body">
            <div className="searchCard_divider"></div>
            <div className="InfoCard_bodyContainer">

              <MediaCard fluid shadowless>
                <div className="InfoCard_jobDescription">
                  <InfoSection headerSize="medium" color="blue" headerText="Job Description">
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad ipsum dolor
                      sit amet, consectetur adipisicing elit, sed do iusmod tempor incididunt ut
                      labore minim veniam, quis nostru exercitation aliquip ex ea commodo
                      consequat. <span>See more</span>
                    </p>
                  </InfoSection>
                </div>
                <div className="InfoCard_bodySkill">
                  <InfoSection headerSize="medium" color="blue" headerText="Skills">
                    <SkillList />
                  </InfoSection>
                </div>
                
                <div className="InfoCard_bodyBtn">
                  <FlatDefaultBtn btntext="View complete Job"/>
                </div>
              </MediaCard>
            </div>

          </div>
          <div className="InfoCard_footer">
            <SearchCardFooter />
          </div>
        </InfoCard>
      </div>
    )
  }
}

export default JobPaneRight;