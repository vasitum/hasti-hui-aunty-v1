import React from "react";
import RecommendedJobHeader from "./RecommendedJobHeader";
import RecommendedJobCard from "./RecommendedJobCard";

import "./index.scss";

class RecommendedJobs extends React.Component {
  render() {
    return (
      <div className="RecommendedJobs">
        <RecommendedJobHeader 
          placeholder="Search recommended jobs"
        />
        <RecommendedJobCard />
      </div>
    )
  }
}



export default RecommendedJobs;