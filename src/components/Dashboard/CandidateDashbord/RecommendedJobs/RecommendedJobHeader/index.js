import React from "react";
import { Grid, Button, Dropdown } from "semantic-ui-react";
import { Form } from "formsy-semantic-ui-react";
import SearchIcon from "../../../../../assets/svg/SearchIcon";
import InputField from "../../../../Forms/FormFields/InputField";
import StatusSortByDropdown from "../../../../Dropdown/StatusSortByDropdown";

import PropTypes from "prop-types";

import "./index.scss";

const Recentstatus = () => {
  const items = [
    {
      text: "Relevant",
      value: "Relevant"
    },
    {
      text: "Recent",
      value: "Recent"
    }
  ];
  return (
    <span className="recent_status">
      Tag: <Dropdown inline options={items} defaultValue={items[0].value} />
    </span>
  );
};

class RecommendedJobHeader extends React.Component {
  state = {
    searchText: ""
  };

  onChange = (e, { value }) => {
    this.setState({
      searchText: value
    });
  };

  onSubmit = () => {
    if (this.props.onFilter) {
      this.props.onFilter(this.state.searchText);
    }
  };

  render() {
    const {
      headerLeftColumn,
      headerRightColumn,
      isShowSortByDropdown,
      placeholder
    } = this.props;

    return (
      <div className="recommendedJobs_header">
        <div className="recommendedJobs_headerLeft">
          <Form className="" onSubmit={this.onSubmit}>
            <div className="recommended_jobSearch">
              <InputField
                name="recommended_job"
                placeholder={placeholder}
                onChange={this.onChange}
                value={this.state.searchText}
              />
              <Button compact className="SearchBtn">
                <SearchIcon
                  pathcolor={this.state.searchText ? "#1f2532" : "#c8c8c8"}
                />
              </Button>
            </div>
          </Form>
        </div>

        {!isShowSortByDropdown ? (
          <div className="recommendedJobs_headerRight">
            <div className="dropdown_right">
              {/* <StatusSortByDropdown
                items={options}
                defaultRefinement={options[0].value}
                /> */}
              <Recentstatus />
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}

RecommendedJobHeader.propTypes = {
  headerLeftColumn: PropTypes.string,
  headerRightColumn: PropTypes.string
};

RecommendedJobHeader.defaultProps = {
  headerLeftColumn: "5",
  headerRightColumn: "11"
};

export default RecommendedJobHeader;
