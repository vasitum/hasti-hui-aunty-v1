import React from "react";
import { Button, Modal, Container } from "semantic-ui-react";
import MobileHeader from "../../../MobileComponents/MobileHeader";
import MobileFooter from "../../../MobileComponents/MobileFooter";

import IcFooterArrowIcon from "../../../../assets/svg/IcFooterArrowIcon";

import CandidatDetailMoreOption from "../CandidatesDetailpage/CandidatDetailMoreOption";

import MyJobsDetailsHeader from "../MyJobsDetails/MyJobsDetailsHeader";

import MyJobsDetailsBodyContainer from "../MyJobsDetails/MyJobsDetailsBodyContainer";
import StatusMyJob from "../../Common/StatusMyJob";
import MyJobCardStatusMore from "../RecruiterDashbordMobile/RecruiterMyjobMobile/MyJobCardStatusMore";

import MobileMoreOptions from "./MobileMoreOptions";

import "./index.scss";

const FooterStatus = ({ data }) => {
  return (
    <div className="mobileFooter_status">
      <p>Status</p>
      <Button>{data.status}</Button>
      {/* <Modal
        className="rejectModal_container"
        trigger={<Button>{data.status}</Button>}
        closeIcon
        closeOnDimmerClick={false}>
        <StatusMyJob status={data.status} jobId={data._id} />
      </Modal> */}
    </div>
  );
};

class MyJobsDetailsMobile extends React.Component {
  render() {
    const { data } = this.props;

    return (
      <div className="MyJobsDetailsMobile">
        <div className="MyJobsDetailsMobile_header">
          <MobileHeader
            className="isMobileHeader_fixe"
            headerLeftIcon={
              <Button className="backArrowBtn">
                <IcFooterArrowIcon pathcolor="#6e768a" />
              </Button>
            }
            headerTitle={data.title}>
            <MobileMoreOptions />
          </MobileHeader>
        </div>
        <div className="MyJobsDetailsMobile_body">
          <Container>
            <MyJobsDetailsHeader
              HeaderDetailsLeftColummn="16"
              HeaderDetailsRightColummn="16"
              data={data}>
              {/* <MyJobsDetailsHeaderStatus 
                isShowHeaderStatus
                isShowCount
              /> */}
              <MyJobCardStatusMore hit={data} />
            </MyJobsDetailsHeader>

            <div>
              <MyJobsDetailsBodyContainer data={data} />
            </div>
          </Container>
        </div>
        <div className="MyJobsDetailsMobile_footer">
          <MobileFooter
            className="isMobileFooter_fixe"
            isShowHeaderTitle
            mobileFooterLeftColumn="16"
            headerLeftIcon={<FooterStatus data={data} />}
          />
        </div>
      </div>
    );
  }
}

export default MyJobsDetailsMobile;
