import React from "react";

import {Button, Dropdown, Modal } from "semantic-ui-react";

import EditJob from "../../../../ModalPageSection/EditJob";

import IcMoreOptionIcon from "../../../../../assets/svg/IcMoreOptionIcon";
import IcEdit from "../../../../../assets/svg/IcEdit";

import "./index.scss";

const MoreOptionsTrigger = (
  <Button compact>
    <IcMoreOptionIcon  pathcolor="#acaeb5"/>
  </Button>
);


const getItems = isMobile => {
  return [
    {
      text: "Share",
      value: "Share",
      // onClick: () => this.props.onChangeSearchField()
    },

    {
      text: <Modal
      className="modal_form"
      // open={this.state.editCompanyModal}
      // onClose={this.onEditCompanyModalClose}
      trigger={
        <Button className="jobEditBtn">
          Edit job
        </Button>
      }
      closeIcon
      closeOnDimmerClick={false}>
      <EditJob
      // data={data.selectedCompanyData}
      // onCompanyCreate={onCompanyCreate}
      // onModalClose={this.onEditCompanyModalClose}
      // onEnableFormLoader={onEnableFormLoader}
      // onDisableFormLoader={onDisableFormLoader}
      />
    </Modal>,
      value: "Share",
      // onClick: () => this.props.onChangeSearchField()
    }
  ];
};

class MobileMoreOptions extends React.Component {
  render() {
    return (
      <div className="MyJobDetailMobileMoreOption">
        <Dropdown 
          trigger={MoreOptionsTrigger}
          options={getItems()}
          icon={null}
          pointing="right"
        />
      </div>
    )
  }
}

export default MobileMoreOptions;