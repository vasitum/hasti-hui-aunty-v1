import React from "react";

import { Tab, Menu, Responsive } from "semantic-ui-react";

import IcDashboard from "../../../assets/svg/IcDashboard";
import IcMyJobs from "../../../assets/svg/IcMyJobs";
import IcMyCandidates from "../../../assets/svg/IcMyCandidates";
import IcLikedProfiles from "../../../assets/svg/IcLikedProfiles";
import IcRecommendedApplicants from "../../../assets/svg/IcRecommendedApplicants";
import IcMyCalendar from "../../../assets/svg/IcMyCalendar";

import RecommendedJobs from "./RecommendedJobs";

import LikeJobs from "./LikeJobs";

import RecommendedJobsMobile from "../../Dashboard/CandidateDashbord/CandidateDashbordMobile/RecommendedJobsMobile";

import CandidateDashbordMobile from "../../Dashboard/CandidateDashbord/CandidateDashbordMobile/LikeJobsCondidateMobile";
import SaveJobCandidateMobile from "../../Dashboard/CandidateDashbord/CandidateDashbordMobile/SaveJobCandidateMobile";

import ConddidateSaveJobs from "../CandidateDashbord/SaveJob";

import getActiveTab from "../../../utils/ui/getActiveTab";

import CandidateApplyedJobs from "./CandidateApplyedJobs";

import CandidateMyJobsMobile from "./CandidateMyJobsMobile";
import ListOfInterviews from "./ListOfInterviews";

import IcListofInterviews from "../../../assets/svg/IcListofInterviews";
import IcSaveAsDraft from "../../../assets/svg/IcSave";
import { Link } from "react-router-dom";

import "./index.scss";

const panes = [
  // {
  //   menuItem: (
  //     <Menu.Item
  //       as={Link}
  //       to="/job/candidate/dashboard"
  //       className="RecruiterTabMenu"
  //       key="dashboard">
  //       <p className="">
  //         <IcDashboard pathcolor="#acaeb5 " />
  //       </p>
  //       Dashboard
  //     </Menu.Item>
  //   ),
  //   render: () => {
  //     return (
  //       <Tab.Pane>
  //         <h1>Candidates</h1>
  //       </Tab.Pane>
  //     );
  //   }
  // },

  // {
  //   menuItem: (
  //     <Menu.Item
  //       as={Link}
  //       to="/job/candidate/recommendedJobs"
  //       className="RecruiterTabMenu"
  //       key="recommendedJob">
  //       <p>
  //         <IcRecommendedApplicants pathcolor="#acaeb5" />
  //       </p>
  //       Recommended Jobs
  //     </Menu.Item>
  //   ),
  //   render: () => {
  //     return (
  //       <Tab.Pane>
  //         <RecommendedJobs />
  //       </Tab.Pane>
  //     );
  //   }
  // },

  // {
  //   menuItem: (
  //     <Menu.Item
  //       as={Link}
  //       to="/job/candidate/calender"
  //       className="RecruiterTabMenu"
  //       key="myCelander">
  //       <p>
  //         <IcMyCalendar pathcolor="#acaeb5" />
  //       </p>
  //       My Calender
  //     </Menu.Item>
  //   ),
  //   render: () => <Tab.Pane>My Celander</Tab.Pane>
  // }
  {
    menuItem: (
      <Menu.Item
        as={Link}
        to="/job/candidate/myjobs"
        className="RecruiterTabMenu"
        key="myJobs">
        <p>
          <IcMyJobs pathcolor="#acaeb5" />
        </p>
        Applied jobs
      </Menu.Item>
    ),
    render: () => (
      <Tab.Pane>
        <CandidateApplyedJobs />
      </Tab.Pane>
    )
  },

  // {
  //   menuItem: (
  //     <Menu.Item
  //       as={Link}
  //       to="/job/candidate/liked"
  //       className="RecruiterTabMenu"
  //       key="likedJobs">
  //       <p>
  //         <IcLikedProfiles pathcolor="#acaeb5" />
  //       </p>
  //       Liked Jobs
  //     </Menu.Item>
  //   ),
  //   render: () => (
  //     <Tab.Pane>
  //       <LikeJobs />
  //     </Tab.Pane>
  //   )
  // },

  {
    menuItem: (
      <Menu.Item
        as={Link}
        to="/job/candidate/saveJob"
        className="RecruiterTabMenu"
        key="SavedJobs">
        <p>
          <IcSaveAsDraft pathcolor="#acaeb5" />
        </p>
        Saved Jobs
      </Menu.Item>
    ),
    render: () => (
      <Tab.Pane>
        <ConddidateSaveJobs />
      </Tab.Pane>
    )
  },
  {
    menuItem: (
      <Menu.Item
        as={Link}
        to="/job/candidate/interview"
        className="RecruiterTabMenu"
        key="dashboard">
        <p className="">
          <IcListofInterviews pathcolor="#acaeb5 " />
        </p>
        Interview List
      </Menu.Item>
    ),
    render: () => {
      return (
        <Tab.Pane>
          <ListOfInterviews />
        </Tab.Pane>
      );
    }
  }
];

class CandidateDashbord extends React.Component {
  render() {
    const { match } = this.props;

    return (
      <div className="CandidateDashbord">
        <Responsive maxWidth={1024}>
          <div>
            {/* <RecommendedJobsMobile /> */}
            {/* <CandidateDashbordMobile /> */}
            {/* <SaveJobCandidateMobile /> */}
            {/* switch based routing */
            (activeId => {
              switch (activeId) {
                case 0:
                  return <CandidateMyJobsMobile {...this.props} />;
                case 1:
                  return <CandidateDashbordMobile />;
                case 2:
                  return <SaveJobCandidateMobile />;
                case 3:
                  return <ListOfInterviews />;
                case 5:
                default:
                  return <div />;
              }
            })(
              getActiveTab(match.params.id, {
                // dashboard: 0,
                // recommendedJobs: 2, // 2
                // calender: 5
                // liked: 1,
                myjobs: 0,
                saveJob: 1,
                interview: 2
              })
            )}
          </div>
        </Responsive>
        <Responsive minWidth={1025}>
          <div className="RecruiterDashboard">
            <Tab
              menu={{ fluid: true, vertical: true, tabular: true }}
              panes={panes}
              grid={{ tabWidth: 2, paneWidth: 12 }}
              activeIndex={getActiveTab(match.params.id, {
                // dashboard: 0,
                // recommendedJobs: 2, // 2
                // calender: 5
                // liked: 1,
                myjobs: 0,
                saveJob: 1,
                interview: 2
              })}
            />
            {/* <MultipleChekPopup /> */}
          </div>
        </Responsive>
      </div>
    );
  }
}

export default CandidateDashbord;
