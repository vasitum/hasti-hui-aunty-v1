import React from "react";
import "./index.scss";
import PropTypes from "prop-types";
import { Header, Grid, Modal, Button } from "semantic-ui-react";
// import MobileFilter from "./MobileFilter";

import ICFilterIcon from "../../../../../assets/svg/IcFilterIcon";

// const footer = () =>{
//   return(
//     <span>
//       Filter
//       <ICFilterIcon pathcolor="#6e768a" />
//     </span>
//   )
// }

const DashboardFilterFooter = props => {
  const { footerIconLeft, footerTitle, onClick } = props;
  return (
    <div className="DashboardFilterFooter">
      <Grid>
        <Grid.Row>
          <Grid.Column width={8} className="filterFooter_LeftColumn">
            <div className="MobileHeader_Left">
              <Header as="h3">
                {/* {footerIconLeft} */}
                {/* footerIconLeft={<ICFilterIcon pathcolor="#6e768a" />} */}
                <div className="mobile_candidateFilter">
                  <Button compact className="filterBtn" onClick={onClick}>
                    <ICFilterIcon pathcolor="#6e768a" height="12"/>
                    Filter
                  </Button>

                </div>
              </Header>
            </div>
          </Grid.Column>
          <Grid.Column
            width={8}
            textAlign="right"
            className="filterFooter_rightColumn">
            {props.children}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
};

export default DashboardFilterFooter;
