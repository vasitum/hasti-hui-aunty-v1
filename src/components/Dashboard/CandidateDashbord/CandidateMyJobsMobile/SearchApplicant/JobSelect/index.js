import React from "react";
import { withSelect } from "../../../../../AlgoliaContainer";
import { Dropdown } from "semantic-ui-react";

import PropTypes from "prop-types";

class JobSelect extends React.Component {
  render() {
    const { items, currentRefinement, onSelect } = this.props;

    return (
      <Dropdown
        placeholder="All Jobs"
        selection
        options={[].concat(
          {
            text: "All Jobs",
            value: "",
            key: "All Jobs"
          },
          items.map(item => {
            return {
              text: item.label,
              value: item.label,
              key: item.label
            };
          })
        )}
        value={currentRefinement ? currentRefinement : ""}
        onChange={(e, { value }) => onSelect(value)}
        fluid
      />
    );
  }
}

JobSelect.PropTypes = {
  items: PropTypes.string
};

JobSelect.defaultProps = {
  items: {
    label: ""
  }
};

export default withSelect(JobSelect);
