import React from "react";
import { Dropdown } from "semantic-ui-react";
import { withSelect } from "../../../../AlgoliaContainer";
import StatisticOptionGrid from "../SearchApplicant/StatisticOptionGrid";

const STAGES = {
  NewApplicant: "NEW APPLICANT",
  Seen: "SEEN",
  ShortList: "SHORTLIST",
  Interview: "INTERVIEW",
  Offer: "OFFER",
  Joinend: "JOINED",
  OfferDropOut: "OFFER DROPOUT",
  Hold: "HOLD",
  Rejected: "REJECTED"
};

function processItems(stages, items, currentRefinement, onSelect) {
  const ret = [];
  let totalCount = 0;
  let isActiveFound = false;

  stages.map(stage => {
    // find the data
    const index = items.filter(val => val.label === stage);
    const active = currentRefinement === stage;
    if (active) {
      isActiveFound = true;
    }

    // check the data
    if (index.length > 0) {
      const data = index[0];

      // increase counter
      totalCount = totalCount + data.count;

      // push it!
      ret.push({
        title: STAGES[stage],
        number: data.count,
        stage: stage,
        active: active
      });

      return;
    }

    // unable to find it
    ret.push({
      title: STAGES[stage],
      number: 0,
      stage: stage,
      active: active
    });
  });

  ret.unshift({
    title: "All",
    active: !isActiveFound,
    stage: ""
  });

  // console.log(ret);
  return ret;
}

class CandidateFilterDropdown extends React.PureComponent {
  state = {};

  render() {
    const { items, currentRefinement, onSelect, label } = this.props;
    return (
      <Dropdown
        onChange={(e, { value }) => onSelect(value)}
        placeholder={label}
        selection
        options={CardOptions}
        fluid
      />
    );
  }
}

export default withSelect(CandidateFilterDropdown);
