import React from "react";
import { Container, Dropdown, Button } from "semantic-ui-react";
import MobileHeader from "../../../MobileComponents/MobileHeader";
import SearchApplicant from "./SearchApplicant";
import CandidateCardContainer from "../CandidateApplyedJobs/CandidateCardContainer";
import DashboardFilterFooter from "./DashboardFilterFooter";
import CandidateSortBy from "../CandidateApplyedJobs/CandidateSortBy";

import IcFilterIcon from "../../../../assets/svg/IcFilterIcon";

import IcFooterArrowIcon from "../../../../assets/svg/IcFooterArrowIcon";

import CandidateCVFilter from "../CandidateApplyedJobs/CandidateCVFilter";
import DashBoardNavigationDrawer from "../../../MobileComponents/DashBoardNavigationDrawer";
import MobileFilter from "./MobileFilter";
import { withSearch } from "../../../AlgoliaContainer";
import { USER } from "../../../../constants/api";
import "./index.scss";

const SortBy = [
  {
    text: "Latest",
    value: "Latest"
  },
  {
    text: "Relevent",
    value: "Relevent"
  }
];

class MobileCandidate extends React.Component {
  constructor(props) {
    super(props);
  }

  state = {
    isShowFilter: false
  };

  isShowMobileFilter = () => {
    this.setState({
      isShowFilter: true
    });
  };

  isShowCandidate = () => {
    this.setState({
      isShowFilter: false
    });
  };

  render() {
    return (
      <div className="CandidateMyJobsMobile">
        <CandidateCVFilter
          label={"Recruiter"}
          attribute="candidateId"
          defaultRefinement={true}
          value={window.localStorage.getItem(USER.UID)}
          style={{
            display: "none"
          }}
        />

        <MobileFilter
          style={{
            display: this.state.isShowFilter ? "block" : "none"
          }}
          onClick={this.isShowCandidate}
        />
        <div
          className="MobileCondidate"
          style={{
            display: !this.state.isShowFilter ? "block" : "none"
          }}>
          <div>
            <MobileHeader
              mobileHeaderLeftColumn="14"
              mobileHeaderRightColumn="2"
              headerLeftIcon={<DashBoardNavigationDrawer />}
              className="isMobileHeader_fixe"
              // onClick={() => this.props.history.go(-1)}
              headerTitle="My Applied Jobs"
            />
          </div>

          <div>
            <Container>
              <SearchApplicant />
              <CandidateCardContainer noClick />
            </Container>
          </div>
          <div>
            <DashboardFilterFooter
              onClick={e =>
                this.setState(state => {
                  return {
                    ...state,
                    isShowFilter: true
                  };
                })
              }
              footerIconLeft={<IcFilterIcon pathcolor="#6e768a" height="12" />}
              footerTitle="Filter">
              <div className="mobileFooter_left">
                <CandidateSortBy
                  defaultRefinement={"jobApplication_date"}
                  items={[
                    { value: "jobApplication", label: "Relevent" },
                    { value: "jobApplication_date", label: "Recent" }
                  ]}
                />
              </div>
            </DashboardFilterFooter>
          </div>
        </div>
      </div>
    );
  }
}

export default withSearch(MobileCandidate, {
  apiKey: process.env.REACT_APP_ALGOLIA_APPLICATION_API_KEY,
  appId: process.env.REACT_APP_ALGOLIA_APPLICATION_APP_ID,
  indexName: process.env.REACT_APP_ALGOLIA_APPLICATION_INDEX_NAME,
  noRouting: true
});
