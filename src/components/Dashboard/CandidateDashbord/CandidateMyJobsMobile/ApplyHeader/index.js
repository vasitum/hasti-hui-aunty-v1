import React from "react";
import { Header, Grid, Button } from "semantic-ui-react";

import MobileHeaderApplyBtn from "../../../../Buttons/MobileHeaderApplyBtn"

import IcCloseIcon from "../../../../../assets/svg/IcCloseIcon";
import "./index.scss";

const ApplyHeader = props => {
  const { ...resProps } = props;
  return (
    <div className="ApplyHeader">
      {/* mobile header */}
      <Grid>
        <Grid.Row className="mobile_Header">
          <Grid.Column
            width={8}
            className="topBottom_center applyHeader_leftColumn">
            <Header as="h3">Filter </Header>
          </Grid.Column>
          <Grid.Column
            width={8}
            className="applyHeader_rightColumn"
            textAlign="right">
            <MobileHeaderApplyBtn 
              btnText="Apply"
              {...resProps}
            />

            <Button
              {...resProps}
              inverted
              className="close_btn"
              color="#1f2532">
              <IcCloseIcon pathcolor="#6e768a" height="14" width="14" />
            </Button>
          </Grid.Column>
        </Grid.Row>
        {/* <Grid.Row className="mobileHeader_clearBtn">
          <Grid.Column width={16}>
            <Button compact inverted className="allClear_btn" {...resProps}>
              <IcCloseIcon pathcolor="#0b9ed0" height="10" width="10" />
            </Button>
            <span>Clear all applied filters</span>
          </Grid.Column>
        </Grid.Row> */}
      </Grid>
      {/* mobile header */}
    </div>
  );
};

export default ApplyHeader;
