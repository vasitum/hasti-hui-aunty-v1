import React from "react";
import { withDateFilter } from "../../../../AlgoliaContainer";
import {
  addDays,
  // subDays,
  startOfDay,
  endOfDay,
  startOfWeek,
  startOfYesterday,
  endOfYesterday,
  startOfTomorrow,
  endOfTomorrow,
  getTime
} from "date-fns";
import Flatpickr from "react-flatpickr";
import SelectOptionField from "../../../../Forms/FormFields/SelectOptionField";

function getFinalRange(min = 0, max = 0) {
  return {
    min,
    max
  };
}

const dateConstants = {
  ALL: "ALL",
  TODAY: "TODAY",
  TOMMOROW: "TOMMOROW",
  YESTERDAY: "YESTERDAY",
  THIS_WEEK: "THIS_WEEK",
  LAST_WEEK: "LAST_WEEK",
  CUSTOM_SELECTION: "CUSTOM_SELECTION"
};

class MyJobDateSelectFilter extends React.Component {
  // state, range
  state = {
    pickerDate: new Date(),
    from: 0,
    to: 0
  };

  onRefine = data => {
    this.props.refine(data.min, data.max);
  };

  onPickerDateChange = date => {
    // console.log(date[0]);
    this.setState(
      {
        pickerDate: date
      },
      () => {
        const selectedDate = date[0];
        this.props.refine(
          getTime(startOfDay(selectedDate)),
          getTime(endOfDay(selectedDate))
        );
      }
    );
  };

  onChange = (e, { value }) => {
    this.setState(
      {
        currentValue: value
      },
      () => {
        let finalConstants;

        switch (value) {
          case dateConstants.ALL:
            finalConstants = getFinalRange();
            break;
          case dateConstants.TODAY:
            finalConstants = getFinalRange(
              getTime(startOfDay(new Date())),
              getTime(endOfDay(new Date()))
            );
            break;
          case dateConstants.TOMMOROW:
            finalConstants = getFinalRange(startOfTomorrow(), endOfTomorrow());
            break;
          case dateConstants.YESTERDAY:
            finalConstants = getFinalRange(
              startOfYesterday(),
              endOfYesterday()
            );
            break;
          case dateConstants.THIS_WEEK:
            finalConstants = getFinalRange(startOfWeek(new Date()), new Date());
            break;
          case dateConstants.LAST_WEEK:
            finalConstants = getFinalRange(startOfWeek(new Date()), new Date());
            break;
          case dateConstants.CUSTOM_SELECTION:
            break;
          default:
            break;
        }

        if (value !== dateConstants.CUSTOM_SELECTION) {
          const refinedConstants = {
            min: getTime(finalConstants.min),
            max: getTime(finalConstants.max)
          };

          // console.log(refinedConstants);
          this.onRefine(refinedConstants);
        }
      }
    );
  };

  getOptions = () => {
    const { past } = this.props;
    return [
      {
        text: "All",
        value: dateConstants.ALL
      },
      {
        text: "Today",
        value: dateConstants.TODAY
      },
      {
        text: past ? "Yesterday" : "Tommorow",
        value: past ? dateConstants.YESTERDAY : dateConstants.TOMMOROW
      },
      {
        text: "This Week",
        value: dateConstants.THIS_WEEK
      },
      {
        text: "Custom Date",
        value: dateConstants.CUSTOM_SELECTION,
        label: (
          <div
            style={{
              position: "absolute",
              top: "0",
              bottom: "0",
              left: "0",
              right: "0",
              zIndex: "1000",
              opacity: "0"
            }}>
            <Flatpickr
              onChange={this.onPickerDateChange}
              value={this.state.pickerDate}
              placeholder={"Select Date"}
              options={{
                disableMobile: true
              }}
            />
          </div>
        )
      }
    ];
  };

  render() {
    const { label } = this.props;
    const options = this.getOptions();

    return (
      <div className="jobFilterSelectField">
        <p>{label}</p>
        <SelectOptionField
          placeholder="All"
          optionsItem={options}
          onChange={this.onChange}
          noSelect
          name="PostedDate"
        />
      </div>
    );
  }
}

export default withDateFilter(MyJobDateSelectFilter);
