import React from "react";

import { Button, Container, Dropdown } from "semantic-ui-react";
import MobileHeader from "../../../../MobileComponents/MobileHeader";

import MobileFooter from "../../../../MobileComponents/MobileFooter";

import IcFooterArrowIcon from "../../../../../assets/svg/IcFooterArrowIcon";

import RecommendedJobHeader from "../../RecommendedJobs/RecommendedJobHeader";
import MobileCardFooter from "../../../../Cards/MyJobCards/MobileCardFooter";
import SaveJobCards from "../../SaveJob/SaveJobCards";
import { Link } from "react-router-dom";
import NoFoundMessageDashboard from "../../../Common/NoFoundMessageDashboard";
import DashBoardNavigationDrawer from "../../../../MobileComponents/DashBoardNavigationDrawer";

import {
  getSavedJobs,
  getSavedJobsFilter
} from "../../../../../api/user/getJobByActions";

import getParsedtags from "../../../../../utils/ui/getParsedtags";

// import "./index.scss";

const Recentstatus = ({ tags, value, onChange }) => {
  return (
    <span className="recent_status">
      Tag: <Dropdown inline options={tags} value={value} onChange={onChange} />
    </span>
  );
};

class SaveJobCandidateMobile extends React.Component {
  constructor(props) {
    super(props);
    this.onFilter = this.onFilter.bind(this);
    this.selectedText = "";
  }

  state = {
    hits: [],
    tagItems: [],
    tags: [],
    selectedtag: ""
  };

  onSelectChange = (e, { value }) => {
    this.setState(
      {
        selectedtag: value
      },
      () => {
        this.onFilter(this.selectedText, value);
      }
    );
  };

  async onFilter(text, tag = this.state.selectedtag) {
    /* remove all applied filters */
    if (!text && !tag) {
      this.selectedText = "";
      this.componentDidMount();
      return;
    }

    try {
      this.selectedText = text;
      const res = await getSavedJobsFilter(text, tag);
      if (res.status === 200) {
        const data = await res.json();
        // console.log(data);
        this.setState({
          hits: data.job
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  async componentDidMount() {
    try {
      const res = await getSavedJobs();
      if (res.status === 200) {
        const data = await res.json();
        // console.log(data);
        this.setState({
          hits: data.job,
          tagItems: data.labelItems,
          tags: data.label
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const { isShowMobile } = this.props;
    return (
      <div className="RecommendedJobsMobile">
        <div className="Recommended_mobileHeader">
          <MobileHeader
            mobileHeaderLeftColumn="14"
            mobileHeaderRightColumn="2"
            headerLeftIcon={<DashBoardNavigationDrawer />}
            headerTitle="Saved Jobs"
          />
        </div>
        <div className="Recommended_mobileBody">
          <Container>
            <RecommendedJobHeader
              onFilter={this.onFilter}
              isShowSortByDropdown
              headerLeftColumn="16"
              placeholder="Search saved jobs"
            />

            {this.state.hits.map(hit => {
              return (
                <SaveJobCards
                  tohref={`/job/public/${hit._id}`}
                  noClick={true}
                  CardsWidthLeft="16"
                  isShowCardApplyContainer
                  isShowMobileFooterbottom={
                    <MobileCardFooter
                      as={Link}
                      tohref={`/job/public/${hit._id}`}
                      hit={hit}
                      job
                      btnText="View Job"
                    />
                  }
                  hit={hit}
                  tags={getParsedtags(this.state.tagItems, hit._id)}
                />
              );
            })}
            {this.state.hits.length === 0 ? (
              <NoFoundMessageDashboard
                noFountTitle="No data found"
                noFountSubTitle="Saving jobs to apply later"
              />
            ) : null}
          </Container>
        </div>
        {!isShowMobile ? (
          <div className="Recommended_mobileFooter">
            <MobileFooter
              mobileFooterLeftColumn="10"
              headerLeftIcon={
                <Recentstatus
                  tags={[].concat(
                    [
                      {
                        text: "All",
                        value: ""
                      }
                    ],
                    this.state.tags.map(val => {
                      return {
                        text: val,
                        value: val
                      };
                    })
                  )}
                  value={this.state.selectedtag}
                  onChange={this.onSelectChange}
                />
              }
            />
          </div>
        ) : null}
      </div>
    );
  }
}

export default SaveJobCandidateMobile;
