import React from "react";
import { Button, Container, Dropdown } from "semantic-ui-react";
import MobileHeader from "../../../../MobileComponents/MobileHeader";

import MobileFooter from "../../../../MobileComponents/MobileFooter";

import IcFooterArrowIcon from "../../../../../assets/svg/IcFooterArrowIcon";

import RecommendedJobHeader from "../../RecommendedJobs/RecommendedJobHeader";

import MobileCardFooter from "../../../../Cards/MyJobCards/MobileCardFooter";

import NoFoundMessageDashboard from "../../../Common/NoFoundMessageDashboard";

import RecommendedJobCard from "../../RecommendedJobs/RecommendedJobCard";
import DashBoardNavigationDrawer from "../../../../MobileComponents/DashBoardNavigationDrawer";
import { Link } from "react-router-dom";

import {
  getLikedJobs,
  getLikedJobsFilter
} from "../../../../../api/user/getJobByActions";

import "./index.scss";

class LikeJobsCondidateMobile extends React.Component {
  constructor(props) {
    super(props);
    this.onFilter = this.onFilter.bind(this);
  }

  state = {
    hits: [],
    tagItems: [],
    tags: []
  };

  async onFilter(text) {
    /* remove all applied filters */
    if (!text) {
      this.componentDidMount();
      return;
    }

    try {
      const res = await getLikedJobsFilter(text);
      if (res.status === 200) {
        const data = await res.json();
        // console.log(data);
        this.setState({
          hits: data.job
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  async componentDidMount() {
    try {
      const res = await getLikedJobs();
      if (res.status === 200) {
        const data = await res.json();
        // console.log(data);
        this.setState({
          hits: data.job,
          tagItems: data.labelItems,
          tags: data.label
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    return (
      <div className="LikeJobsCondidateMobile">
        <div className="RecommendedJobsMobile">
          <div className="Recommended_mobileHeader">
            <MobileHeader
              mobileHeaderLeftColumn="14"
              mobileHeaderRightColumn="2"
              headerLeftIcon={<DashBoardNavigationDrawer />}
              headerTitle="Liked Jobs"
            />
          </div>
          <div className="Recommended_mobileBody">
            <Container>
              <RecommendedJobHeader
                onFilter={this.onFilter}
                isShowSortByDropdown
                headerLeftColumn="16"
                placeholder="Search liked jobs"
              />
              {this.state.hits.map(hit => {
                return (
                  <RecommendedJobCard
                    tohref={`/job/public/${hit._id}`}
                    noClick={true}
                    key={hit._id}
                    hit={hit}
                    CardsWidthLeft="16"
                    isShowCardApplyContainer
                    isShowMobileFooterbottom={
                      <MobileCardFooter
                        as={Link}
                        tohref={`/job/public/${hit._id}`}
                        hit={hit}
                        job
                        btnText="View Job"
                      />
                    }
                  />
                );
              })}
              {this.state.hits.length === 0 ? (
                <NoFoundMessageDashboard
                  noFountTitle="No data found"
                  noFountSubTitle="Start liking your dream jobs to boost your chances of hire"
                />
              ) : null}
            </Container>
          </div>
          {/* {!isShowMobile ? (
            <div className="Recommended_mobileFooter">
              <MobileFooter
                mobileFooterLeftColumn="10"
                headerLeftIcon={<Recentstatus />}
              />
            </div>
          ) : null} */}
        </div>
      </div>
    );
  }
}

export default LikeJobsCondidateMobile;
