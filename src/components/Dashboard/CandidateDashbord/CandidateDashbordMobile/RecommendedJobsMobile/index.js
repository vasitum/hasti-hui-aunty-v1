import React from "react";

import { Button, Container, Dropdown } from "semantic-ui-react";
import MobileHeader from "../../../../MobileComponents/MobileHeader";

import MobileFooter from "../../../../MobileComponents/MobileFooter";

import IcFooterArrowIcon from "../../../../../assets/svg/IcFooterArrowIcon";

import RecommendedJobHeader from "../../RecommendedJobs/RecommendedJobHeader";

import MobileCardFooter from "../../../../Cards/MyJobCards/MobileCardFooter";

import RecommendedJobCard from "../../RecommendedJobs/RecommendedJobCard";

import StatusSortByDropdown from "../../../../Dropdown/StatusSortByDropdown";

import "./index.scss";


const Recentstatus = () => {
  const items = [
    {
      text: 'Recent',
      value: 'Recent',
      
    },
    {
      text: 'Recentnew',
      value: 'Recent',
      
    },

  ]
  return (
    <span className="recent_status">
      Sort by:{' '}
      <Dropdown inline options={items} defaultValue={items[0].value} />
    </span>
  )
}

class RecommendedJobsMobile extends React.Component {
  render() {

    const {isShowMobile} = this.props;
    return (
      <div className="RecommendedJobsMobile">
        <div className="Recommended_mobileHeader">
          <MobileHeader
            mobileHeaderLeftColumn="14"
            mobileHeaderRightColumn="2"
            headerLeftIcon={<Button compact className="backHeaderBtn"><IcFooterArrowIcon pathcolor="#6e768a" /></Button>}
            headerTitle="Recommended Jobs"
          />
        </div>
        <div className="Recommended_mobileBody">
          <Container>
            <RecommendedJobHeader
              isShowSortByDropdown
              headerLeftColumn="16"
              placeholder="Search recommended jobs"
            />



            <RecommendedJobCard
              isShowCardApplyContainer
              isShowMobileFooterbottom={<MobileCardFooter />}
            />
            <RecommendedJobCard
              isShowCardApplyContainer
              isShowMobileFooterbottom={<MobileCardFooter />}
            />

          </Container>
        </div>
        {!isShowMobile ? (<div className="Recommended_mobileFooter">
          <MobileFooter mobileFooterLeftColumn="10" headerLeftIcon={<Recentstatus />} />
        </div>): null}
        
      </div>
    )
  }
}

export default RecommendedJobsMobile;