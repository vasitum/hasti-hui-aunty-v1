import React from "react";

import RecommendedJobHeader from "../RecommendedJobs/RecommendedJobHeader";
import RecommendedJobCard from "../RecommendedJobs/RecommendedJobCard";
import NoFoundMessageDashboard from "../../Common/NoFoundMessageDashboard";

import "./index.scss";

import {
  getLikedJobs,
  getLikedJobsFilter
} from "../../../../api/user/getJobByActions";
import { Container } from "semantic-ui-react";

class LikeJobs extends React.Component {
  constructor(props) {
    super(props);
    this.onFilter = this.onFilter.bind(this);
  }

  state = {
    hits: [],
    tagItems: [],
    tags: []
  };

  async onFilter(text) {
    /* remove all applied filters */
    if (!text) {
      this.componentDidMount();
      return;
    }

    try {
      const res = await getLikedJobsFilter(text);
      if (res.status === 200) {
        const data = await res.json();
        // console.log(data);
        this.setState({
          hits: data
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  async componentDidMount() {
    try {
      const res = await getLikedJobs();
      if (res.status === 200) {
        const data = await res.json();
        // console.log(data);
        this.setState({
          hits: data.job,
          tagItems: data.labelItems,
          tags: data.label
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const { hits } = this.state;
    return (
      <div className="RecommendedJobs LikeJobs_condidate">
        <Container>
          <RecommendedJobHeader
            onFilter={this.onFilter}
            isShowSortByDropdown
            placeholder="Search liked jobs"
          />
          <div className="LikeJobs_container">
            {this.state.hits.map(hit => {
              return (
                <RecommendedJobCard
                  tohref={`/job/public/${hit._id}`}
                  key={hit._id}
                  hit={hit}
                />
              );
            })}
          </div>
          {hits.length === 0 ? (
            <NoFoundMessageDashboard
              noFountTitle="No data found"
              noFountSubTitle="Start liking your dream jobs to boost your visibility"
            />
          ) : null}
        </Container>
      </div>
    );
  }
}

export default LikeJobs;
