import React from "react";
import { Responsive } from "semantic-ui-react";
import ListOfinterviewContainer from "./ListOfInterviewContainer";
import IcDotIcon from "../../../../assets/svg/IcDotIcon";
import ReactGA from "react-ga";
import "./index.scss";

export default class ListOfInterviews extends React.Component {
  componentDidMount() {
    ReactGA.pageview('/job/candidate/interview');
  }
  
  render() {
    let pathColor = "rgba(11, 158, 208, 1)";

    return (
      <div>
        <Responsive minWidth="1024">
          <ListOfinterviewContainer />
        </Responsive>
      </div>
    );
  }
}
