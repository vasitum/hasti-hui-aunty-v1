import React from "react";
import { Grid, Button, Dropdown } from "semantic-ui-react";
import { Form } from "formsy-semantic-ui-react";
import SearchIcon from "../../../../../assets/svg/SearchIcon";
import InputField from "../../../../Forms/FormFields/InputField";
import StatusSortByDropdown from "../../../../Dropdown/StatusSortByDropdown";
import PropTypes from "prop-types";
// import "./index.scss";

const Recentstatus = ({ tags, value, name, onChange }) => {
  const items = [
    {
      text: "UX Designer",
      value: "UX Designer"
    },
    {
      text: "Recentnew",
      value: "Recent"
    }
  ];
  return (
    <span className="recent_status">
      Tag:{" "}
      <Dropdown
        inline
        options={tags}
        name={name}
        onChange={onChange}
        defaultValue={value}
      />
    </span>
  );
};

class SaveJobHeader extends React.Component {
  state = {
    searchText: "",
    selectedTag: "__default"
  };

  onChange = (e, { value, name }) => {
    this.setState(
      {
        [name]: value
      },
      () => {
        if (name === "selectedTag") {
          this.onSubmit();
        }
      }
    );
  };

  onSubmit = e => {
    const { onFilter } = this.props;
    const refinedTag =
      this.state.selectedTag === "__default" ? "" : this.state.selectedTag;

    onFilter(this.state.searchText, refinedTag);
  };

  render() {
    const {
      headerLeftColumn,
      headerRightColumn,
      isShowSortByDropdown,
      placeholder,
      tags
    } = this.props;

    return (
      <div className="recommendedJobs_header">
       
       <div className="recommendedJobs_headerLeft">
       <Form className="" onSubmit={this.onSubmit}>
                <div className="recommended_jobSearch">
                  <InputField
                    name="searchText"
                    placeholder={placeholder}
                    onChange={this.onChange}
                    value={this.state.searchText}
                  />
                  <Button compact className="SearchBtn">
                    <SearchIcon
                      pathcolor={this.state.searchText ? "#1f2532" : "#c8c8c8"}
                    />
                  </Button>
                </div>
              </Form>
        </div>
            
            {!isShowSortByDropdown ? (
              <div className="recommendedJobs_headerRight">
                <div className="dropdown_right">
                  {/* <StatusSortByDropdown
                  items={options}
                  __defaultRefinement={options[0].value}
                  /> */}
                  {
                    <Recentstatus
                      tags={[].concat(
                        [
                          {
                            text: "All",
                            value: "__default"
                          }
                        ],
                        tags.map(val => {
                          return {
                            text: val,
                            value: val
                          };
                        })
                      )}
                      value={this.state.selectedTag}
                      onChange={this.onChange}
                      name="selectedTag"
                    />
                  }
                </div>
              </div>
            ) : null}
          
      </div>
    );
  }
}

SaveJobHeader.propTypes = {
  headerLeftColumn: PropTypes.string,
  headerRightColumn: PropTypes.string
};

SaveJobHeader.defaultProps = {
  headerLeftColumn: "5",
  headerRightColumn: "11"
};

export default SaveJobHeader;
