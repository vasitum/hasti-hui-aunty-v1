import React from "react";
import { Grid, Image, Header, List, Button } from "semantic-ui-react";

import IcJobThumb from "../../../../../assets/img/publicProfile.png";

import Icjob from "../../../../../assets/svg/Icjob";

import IcLocation from "../../../../../assets/svg/IcLocation";

import IcCompany from "../../../../../assets/svg/IcCompany";

import PropTypes from "prop-types";

import { Link } from "react-router-dom";

import "./index.scss";

class SaveCard extends React.Component {
  render() {
    const {
      tohref,
      isExp,
      isJobType,
      isHeaderTag,
      isSkillShow,
      isBorder,
      myJobCardsWidthLeft,
      myJobCardsWidthRight,
      isShowMobileFooter,
      children,
      hit,
      tags,
      ...resProps,
    } = this.props;

    if (!hit) {
      return null;
    }

    return (
      <div className={`MyJob_cardContainer ${isBorder}`} {...resProps}>
        <div className="MyJobCards jobDefault_card">
          <Grid>
            <Grid.Row>
              <Grid.Column
                width={myJobCardsWidthLeft}
                className="MyJobCards_leftSide"
              >
                <div className="MyjobCard_leftDetails">
                  <div className="companyLogo">
                    {/* <Image src={IcJobThumb} /> */}
                    {!hit.com || !hit.com.imgExt ? (
                      <IcCompany height="50" width="50" rectcolor="#f7f7fb" pathcolor="#c8c8c8" />
                    ) : (
                      <Image
                        src={`https://s3-us-west-2.amazonaws.com/img-mwf/company/${
                          hit.com.id
                        }/image.jpg`}
                        style={{
                          height: "50px",
                          width: "50px",
                          borderRadius: "6px"
                        }}
                      />
                    )}
                  </div>
                </div>
                <div className="companydetails">
                  <div className="title">
                    <Header as="h3"><Link to={tohref}>{hit.title}</Link></Header>
                    <p>
                      {/* <span><Icjob pathcolor="#0b9ed0" height="12.5"/></span>  */}
                      {hit.com ? hit.com.name : ""}
                    </p>
                  </div>
                  <div className="subTitle">
                    <div className="loctionExp">
                      <p>
                        {/* <span><IcLocation pathcolor="#0b9ed0" width="10" height="11.938"/></span> */}
                        {hit.loc ? hit.loc.city : ""}
                        {" "}
                        {hit.loc ? hit.loc.country : ""}
                      </p>
                      
                      {/* isExp */}
                      {isExp ? (<p className="experience">
                        <span>{hit.exp ? hit.exp.min : 0} - {hit.exp ? hit.exp.max : 0}</span> yrs experience
                      </p>):null}
                      {/* isExp end */}

                    </div>
                    {/* isJobType */}
                    {isJobType ? (<div className="jobTypeRoll">
                      <p className="jobType">Job type: <span>{hit.jobType}</span></p>
                      <p className="jobRole">Job Role: <span>{hit.role}</span></p>
                    </div>): null}
                    {/* isJobType end*/}

                    

                    

                    {/* isSkillShow */}
                    {isSkillShow ? (
                      <p
                        className="skills"
                        style={{
                          display: !hit.skills ? "none" : "block"
                        }}
                      >
                        Skills:{" "}
                        <span>
                          {" "}
                          <List bulleted horizontal>
                            {hit.skills
                              ? hit.skills.map(skill => {
                                return <List.Item>{skill.name}</List.Item>;
                              })
                              : null}
                          </List>
                        </span>
                      </p>
                    ) : null}
                    {/* isSkillShow end*/}


                    {/* isHeaderTag */}
                    {isHeaderTag ? ( <div className="headerTag">
                      <p>
                        Tag: {
                          tags.map(val => {
                            return (
                              <Button key={val} className="" compact>{val}</Button>
                            )
                          })
                        }
                      </p>
                    </div>):null}
                   
                    {/* isHeaderTag end */}

                    
                  </div>
                </div>
              </Grid.Column>
              <Grid.Column
                width={myJobCardsWidthRight}
                className="MyJobCards_rightSide"
              >
                {children}
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>
        {/* {isShowMoreBtn ? (
          <div className="moreJob_btn">
            <ActionBtn
              btnText="VIEW MORE JOBS"
              btnIcon={<IcDownArrowIcon height="6" />}
            />
          </div>
        ) : null} */}

        {isShowMobileFooter}
      </div>
    );
  }
}

SaveCard.propTypes = {
  myJobCardsWidthLeft: PropTypes.string,
  myJobCardsWidthRight: PropTypes.string,
  hit: PropTypes.string
};

SaveCard.defaultProps = {
  myJobCardsWidthLeft: "11",
  myJobCardsWidthRight: "5",
  hit: {
    title: "UX UI Designer",
    comName: "Wipro",
    locCity: "Noida",
    locCountry: "India",
    skills: [
      "html",
      "css",
      "javascript"
    ]
  }
};

export default SaveCard;