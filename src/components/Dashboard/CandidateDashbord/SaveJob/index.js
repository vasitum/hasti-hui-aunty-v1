import React from "react";
import SaveJobHeader from "./SaveJobHeader";
import SaveJobCards from "./SaveJobCards";

import NoFoundMessageDashboard from "../../Common/NoFoundMessageDashboard";
import ReactGA from "react-ga";
import {
  getSavedJobs,
  getSavedJobsFilter
} from "../../../../api/user/getJobByActions";

import getParsedtags from "../../../../utils/ui/getParsedtags";

import "./index.scss";
import { Container } from "semantic-ui-react";

class ConddidateSaveJobs extends React.Component {
  constructor(props) {
    super(props);
    this.onFilter = this.onFilter.bind(this);
  }

  state = {
    hits: [],
    tagItems: [],
    tags: []
  };

  async onFilter(text, tag) {
    /* remove all applied filters */
    if (!text && !tag) {
      this.componentDidMount();
      return;
    }

    try {
      const res = await getSavedJobsFilter(text ? text : null, tag);
      if (res.status === 200) {
        const data = await res.json();
        // console.log(data);
        this.setState({
          hits: data.job
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  async componentDidMount() {
    ReactGA.pageview('/job/candidate/saveJob');
    try {
      const res = await getSavedJobs();
      if (res.status === 200) {
        const data = await res.json();
        // console.log(data);
        this.setState({
          hits: data.job,
          tagItems: data.labelItems,
          tags: data.label
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const { hits } = this.state;
    return (
      <div className="RecommendedJobs LikeJobs_condidate">
        <Container>
          <SaveJobHeader
            onFilter={this.onFilter}
            tags={this.state.tags}
            placeholder="Search saved jobs"
          />
          <div className="LikeJobs_container">
            {this.state.hits.map(hit => {
              return (
                <SaveJobCards
                  tohref={`/job/public/${hit._id}`}
                  hit={hit}
                  tags={getParsedtags(this.state.tagItems, hit._id)}
                />
              );
            })}
          </div>
          {hits.length === 0 ? (
            <NoFoundMessageDashboard
              noFountTitle="No data found"
              noFountSubTitle="Start saving jobs to apply later"
            />
          ) : null}
        </Container>
      </div>
    );
  }
}

export default ConddidateSaveJobs;
