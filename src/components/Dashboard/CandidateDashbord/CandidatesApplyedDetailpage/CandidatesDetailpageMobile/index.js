import React from "react";

import { Image, Button, Modal } from "semantic-ui-react";

import MobileHeader from "../../../../MobileComponents/MobileHeader";
import IcFooterArrowIcon from "../../../../../assets/svg/IcFooterArrowIcon";
import UserLogo from "../../../../../assets/img/people.png";
import CandidatDetailMoreOption from "../CandidatDetailMoreOption";

import CondidateDetailPageContainer from "../CondidateDetailPageContainer";

import MobileFooter from "../../../../MobileComponents/MobileFooter";
import RejectModal from "../../../RejectModal";

import IcCompany from "../../../../../assets/svg/IcCompany";

import { withRouter } from "react-router-dom";
import { CAND_STAGES } from "../../../../../constants/applicationStages";

import "./index.scss";

const HeaderLeft = ({ history, data }) => {
  return (
    <div className="mobileHeaderLogo">
      <Button onClick={e => history.go(-1)} className="backArrowBtn">
        <IcFooterArrowIcon pathcolor="#6e768a" />
      </Button>

      {!data || !data.com || !data.com.imgExt ? (
        <span className="svgLogo">
          <IcCompany
            rectcolor="#f7f7fb"
            height="27"
            width="27"
            pathcolor="#c8c8c8"
          />
        </span>
      ) : (
        <span className="imgLogo">
          <Image
            width={"27px"}
            height={"27px"}
            src={`https://s3-us-west-2.amazonaws.com/img-mwf/company/${
              data.com.id
            }/image.jpg`}
          />
        </span>
      )}
    </div>
  );
};

const HeaderLeftRouted = withRouter(HeaderLeft);

const STAGES = CAND_STAGES;

const FooterStatus = ({ data }) => {
  return (
    <div className="mobileFooter_status">
      <p>Status</p>
      <Button>{STAGES[data.status]}</Button>
      {/* <Modal
        className="rejectModal_container"
        trigger={<Button>Interview</Button>}
        closeIcon
        closeOnDimmerClick={false}>
        <RejectModal
          status=""
          jobId=""
          applicationId=""
        />
      </Modal> */}
    </div>
  );
};

class CandidatesDetailpageMobile extends React.Component {
  render() {
    const { applData, jobData } = this.props;
    return (
      <div className="CandidatesDetailpageMobile">
        <div className="CandidatesDetail_Header">
          <MobileHeader
            className="isMobileHeader_fixe"
            mobileHeaderLeftColumn="14"
            mobileHeaderRightColumn="2"
            headerLeftIcon={<HeaderLeftRouted data={jobData} />}
            // headerTitle={applData.jobName}
          >
            <CandidatDetailMoreOption />
          </MobileHeader>
        </div>

        <div className="CandidatesDetail_Body">
          <CondidateDetailPageContainer
            applData={applData}
            jobData={jobData}
            isCondidateApplyHeader
          />
        </div>

        <div className="CandidatesDetail_Footer">
          <MobileFooter
            isShowHeaderTitle
            mobileFooterLeftColumn="16"
            className="isMobileFooter_fixe"
            headerLeftIcon={<FooterStatus data={applData} />}
          />
        </div>
      </div>
    );
  }
}

export default CandidatesDetailpageMobile;
