import React from "react";

import "./index.scss";
import { Button, Modal } from "semantic-ui-react";
// import StatusMyJob from "../../../Common/StatusMyJob";
import RejectModal from "../../../RejectModal";
import fromNow from "../../../../../utils/env/fromNow";
import { CAND_STAGES } from "../../../../../constants/applicationStages";

const STAGES = CAND_STAGES;

const CandidateApplyHeader = props => {
  const { isCondidateApplyHeader, data, jobData } = props;
  return (
    <div className="CandidateApplyHeader">
      <div className="candidateApplyStatus">
        <p className="view">
          Views: <span>{jobData.viewCount}</span>
        </p>
        <p className="view">
          Likes: <span>{jobData.likeCount}</span>
        </p>
        <p className="submit">
          Applied: <span>{fromNow(data.appliedDate)}</span>
        </p>
      </div>
      <div className="candidateApplyStatusBtn">
        <p>Application current status</p>
        <Button style={{ cursor: "default" }} compact>
          {STAGES[data.status]}
        </Button>
        {/* <Modal
            className="rejectModal_container"
            trigger={<Button compact>Interview</Button>}
            closeIcon
            closeOnDimmerClick={false}>
            <RejectModal
              status=""
              jobId=""
              applicationId=""
            />
          </Modal> */}
      </div>
    </div>
  );
};

export default CandidateApplyHeader;
