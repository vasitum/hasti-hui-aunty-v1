import React from "react";
import { Responsive } from "semantic-ui-react";
import CandidatesDetailpageMobile from "./CandidatesDetailpageMobile";
import CondidateDetailPageContainer from "./CondidateDetailPageContainer";

import withApplication from "../../../wrappers/withApplication";
import withPostedJob from "../../../wrappers/withPostedJob";
import ReactGA from "react-ga";
import "./index.scss";

class CandidatesDetailpage extends React.Component {

  componentDidMount() {
    const { match } = this.props;
    // console.log("check url",  match.url);
    ReactGA.pageview(`${match.url}`);
  }

  render() {
    // console.log(`
    //   Application Data: ${this.props.data}
    //   JobData: ${this.props.jobData}
    // `);
    // console.log(this.props.applData, this.props.jobData);

    return (
      <div className="CandidatesApplyedDetailpage">
        <Responsive maxWidth={1024}>
          <CandidatesDetailpageMobile
            applData={this.props.applData}
            jobData={this.props.jobData}
          />
        </Responsive>

        <Responsive minWidth={1025}>
          <CondidateDetailPageContainer
            applData={this.props.applData}
            jobData={this.props.jobData}
          />
        </Responsive>
      </div>
    );
  }
}

export default withApplication(
  withPostedJob(
    CandidatesDetailpage,
    {
      connected: false
    },
    true
  ),
  {
    connected: true
  }
);
