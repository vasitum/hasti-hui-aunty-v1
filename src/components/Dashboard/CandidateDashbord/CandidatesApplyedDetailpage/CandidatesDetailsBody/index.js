import React from "react";

import { Tab, Menu } from "semantic-ui-react";

import UserProfilePageSection from "./UserProfilePageSection";

import InterViewPageSection from "../../../Common/InterViewPageSection";

import ActivitiesPageSection from "../../../Common/ActivitiesPageSection";

// import NoInterviewScheduled from "../../../Common/NoInterviewScheduled";

import DashboardMessaging from "../../../Common/DashboardMessaging";

import "./index.scss";

function getDetailsPage({
  applicationId,
  jobId,
  userId,
  jobData,
  applicationData
}) {
  return [
    {
      menuItem: <Menu.Item>Job details</Menu.Item>,
      render: () => {
        return (
          <div className="detailPageTab">
            <Tab.Pane attached={false}>
              <UserProfilePageSection data={jobData} />
            </Tab.Pane>
          </div>
        );
      }
    },

    {
      menuItem: <Menu.Item>Messages</Menu.Item>,
      render: () => {
        return (
          <div className="detailPageTab">
            <Tab.Pane attached={false}>
              <DashboardMessaging userId={userId} />
            </Tab.Pane>
          </div>
        );
      }
    },

    {
      menuItem: <Menu.Item>Interview</Menu.Item>,
      render: () => {
        return (
          <div className="detailPageTab">
            <Tab.Pane attached={false}>
              <InterViewPageSection
                jobId={jobId}
                applicationId={applicationId}
                applicationData={applicationData}
                candidate={true}
              />
              {/* <NoInterviewScheduled /> */}
            </Tab.Pane>
          </div>
        );
      }
    },
    {
      menuItem: <Menu.Item>Activities</Menu.Item>,
      render: () => {
        return (
          <div className="detailPageTab">
            <Tab.Pane attached={false}>
              <ActivitiesPageSection
                jobId={jobId}
                applicationId={applicationId}
                candidate={true}
              />
            </Tab.Pane>
          </div>
        );
      }
    }
  ];
}

class CandidatesDetailsBody extends React.Component {
  render() {
    const { data, jobData } = this.props;

    if (!data) {
      return null;
    }

    return (
      <div className="CandidatesDetailsBody">
        <div className="">
          <Tab
            menu={{ text: true }}
            panes={getDetailsPage({
              applicationId: data._id,
              jobId: data.pId,
              userId: data.reqId,
              jobData: jobData,
              applicationData: data
            })}
            className="detailsBody_container"
          />
        </div>
      </div>
    );
  }
}

export default CandidatesDetailsBody;
