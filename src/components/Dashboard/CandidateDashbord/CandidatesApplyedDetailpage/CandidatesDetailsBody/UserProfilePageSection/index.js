import React from "react";
import PropTypes from "prop-types";

import { List } from "semantic-ui-react";

import InfoSectionGridHeader from "../../../../../CardElements/InfoSectionGridHeader";
import QuillText from "../../../../../CardElements/QuillText";

import AboutCompany from "../../../../../CardElements/AboutCompany";

import SkillComponent from "../../../../../ModalPageSection/PostLoginJob/SkillComponent";
import Currency from "../../../../../CardElements/Currency";
import "./index.scss";

const processDisablityOptions = other => {
  if (!other || !Array.isArray(other.disFriendlyTags)) {
    return null;
  }

  const tags = other.disFriendlyTags;

  return tags.map(val => {
    return <List.Item key={val}>{val}</List.Item>;
  });
};

const processEducation = education => {
  if (!education) {
    return null;
  }

  const eduArry = education.map(val => {
    return {
      // course: val.course,
      // spec: val.specs[0],
      level: val.level,
      type: "Full Time"
    };
  });

  const eduArrayComponent = eduArry.map(val => {
    if (!val || !val.level) {
      return (
        <p
          style={{
            color: "#c8c8c8"
          }}>
          Describe the education qualification for your job
        </p>
      );
    }

    return <p>{val.level}</p>;
  });

  return eduArrayComponent;
};

const UserProfilePageSection = props => {
  const { data } = props;

  if (!data) {
    return null;
  }
  // console.log("check data", data);
  return (
    <div>
      <div className="UserProfilePageSection">
        <div className="description_container">
          <div className="description">
            <p className="descTitle">
              Designation: <span>{data.role}</span>
            </p>
            <div className="menu-list">
              <p className="">
                Experience:{" "}
                <span>
                  {data.exp ? data.exp.min : ""} -{" "}
                  {data.exp ? data.exp.max : ""} yrs
                </span>
              </p>
              <p className="">
                Job type: <span>{data.jobType}</span>
              </p>
              <p className="">
                Salary:{" "}
                <span>
                  <Currency data={data} />
                  {data.salary ? data.salary.min : ""} -{" "}
                  {data.salary ? data.salary.max : ""}{" "}
                  {data.salary
                    ? data.salary.currency && data.salary.currency !== "INR"
                      ? "k per annum"
                      : " LPA"
                    : ""}
                </span>
              </p>
            </div>
          </div>
          {/* <div className="downloadCVBtn">
          <Button>Download CV</Button>
        </div> */}
        </div>

        <div className="Userdedail_infoHeader">
          <InfoSectionGridHeader headerText="Job Description">
            <QuillText value={data.desc ? data.desc : ""} readOnly />
          </InfoSectionGridHeader>

          <InfoSectionGridHeader
            style={{
              display: data.respAndDuty ? "block" : "none"
            }}
            headerText="Job Duties">
            <QuillText
              value={data.respAndDuty ? data.respAndDuty : ""}
              readOnly
            />
          </InfoSectionGridHeader>

          <InfoSectionGridHeader
            style={{
              display: data.benefits ? "block" : "none"
            }}
            headerText="Benefits">
            <QuillText value={data.benefits ? data.benefits : ""} readOnly />
          </InfoSectionGridHeader>

          <SkillComponent data={data} />
          {/* <InfoSectionGridHeader headerText="Skills">
          </InfoSectionGridHeader> */}

          <InfoSectionGridHeader
            style={{
              display: !data.edus || !data.edus[0].level ? "none" : "block"
            }}
            headerText="Educational Requirement">
            <div className="">{processEducation(data.edus)}</div>
          </InfoSectionGridHeader>
          <InfoSectionGridHeader
            style={{
              display: !data.other || !data.other.dec ? "none" : "block"
            }}
            headerText="Other Requirement">
            <QuillText value={data.other ? data.other.dec : ""} readOnly />
          </InfoSectionGridHeader>

          <InfoSectionGridHeader
            style={{
              display:
                !data.other ||
                !data.other.disFriendlyTags ||
                !data.other.disFriendlyTags.length
                  ? "none"
                  : "block"
            }}
            headerText="Disabled Friendly Job">
            <List bulleted horizontal>
              {processDisablityOptions(data.other)}
            </List>
          </InfoSectionGridHeader>
        </div>
      </div>
      <AboutCompany compId={data.com ? data.com.id : ""} />
    </div>
  );
};

UserProfilePageSection.PropsType = {
  data: PropTypes.string
};

UserProfilePageSection.defaultProps = {
  data: {
    role: "Software developer",
    exp: {
      min: "3",
      max: "5"
    },
    jobType: "Full time",
    salary: {
      min: "3",
      max: "5"
    },
    desc:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad ipsum dolor sit amet, consectetur adipisicing elit, sed do iusmod tempor incididunt ut labore minim veniam, quis nostru exercitation aliquip ex ea commodo consequat."
  }
};

export default UserProfilePageSection;
