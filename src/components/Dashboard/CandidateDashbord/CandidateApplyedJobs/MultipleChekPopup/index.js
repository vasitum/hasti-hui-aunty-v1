import React from "react";
import "./index.scss";
import { Dropdown, Button, Grid, Modal } from "semantic-ui-react";
import RejectModal from "../../../RejectModal";

const BulkOption = [
  {
    text: "Basic Screening",
    value: "Basic Screening"
  },
  {
    text: "Send Message",
    value: "Send Message"
  },

  {
    text: "Share Profiles",
    value: "Share Profiles"
  },

  {
    text: (
      <Modal
        className="rejectModal_container"
        trigger={<span>Reject Profiles</span>}
        closeIcon
        closeOnDimmerClick={false}>
        <RejectModal />
      </Modal>
    ),
    value: "Reject Profiles"
  },

  {
    text: "Invite to Apply",
    value: "Invite to Apply"
  },

  {
    text: "Offer",
    value: "Offer"
  },
  {
    text: "Interview",
    value: "Interview"
  },

  {
    text: "Pending Aplication",
    value: "Pending Aplication"
  }
];

/**
 * TODO:
 *  - Enable Change Status Modal
 *
 */
class MultipleChekPopup extends React.Component {
  render() {
    return (
      <div className="MultipleChekPopup">
        <Grid>
          <Grid.Row>
            <Grid.Column width={3} />
            <Grid.Column width={10} textAlign="center">
              {/* <Dropdown
                className="BulkOptionMenu"
                placeholder="Bulk actions"
                selection
                pointing="top bottom"
                options={BulkOption}
              /> */}
              <Button className="deselect_allBtn">Select all</Button>
              <Button className="deselect_allBtn">Deselect all</Button>
            </Grid.Column>
            <Grid.Column width={3} />
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}

export default MultipleChekPopup;
