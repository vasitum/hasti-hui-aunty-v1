import React from "react";

import { Dropdown } from "semantic-ui-react";

import "./index.scss";

const dropdownHeader = totalData => {
  return (
    <div className="labelDropAll">
      <p className="lavelNumber">{totalData.number}</p>
      <p className="lavelText">{totalData.title}</p>
    </div>
  );
};

const formatItems = items => {
  const ret = [];
  const muted = ["Hold", "Rejected"];

  items.map(item => {
    // return if nothing is there is to show
    if (!item || !item.stage) {
      return;
    }

    if (muted.indexOf(item.stage) >= 0) {
      ret.push({
        muted: true,
        value: item.stage,
        text: item.title,
        description: item.number
      });
    } else {
      ret.push({
        muted: false,
        value: item.stage,
        text: item.title,
        description: item.number
      });
    }
  });

  return ret;
};

const CandidateStatisticDropMenu = ({
  items,
  currentRefinement,
  refine,
  totalData
}) => {
  const formattedItems = formatItems(items);

  return (
    <div className="CandidateStatisticDropMenu">
      <Dropdown text={dropdownHeader(totalData)}>
        <Dropdown.Menu>
          {formattedItems.map(item => {
            if (!item) {
              return null;
            }

            return (
              <Dropdown.Item
                onClick={e => refine(item.value)}
                className={item.muted ? "dropdownMenu_secondPart" : ""}
                text={item.text}
                description={item.description}
                value={item.value}
              />
            );
          })}
        </Dropdown.Menu>
      </Dropdown>
    </div>
  );
};

export default CandidateStatisticDropMenu;
