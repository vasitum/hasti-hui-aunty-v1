import React from "react";
import { Grid, Input } from "semantic-ui-react";
import { withSearchBox } from "../../../../AlgoliaContainer";

class CandidateSearchBox extends React.Component {
  render() {
    const { refine, currentRefinement } = this.props;
    return (
      <div className="aplicantSearch">
        <Input
          onChange={(e, { value }) => refine(value)}
          value={currentRefinement}
          fluid
          icon="search"
          fitted="right"
          placeholder="Search applied jobs"
        />
      </div>
    );
  }
}

export default withSearchBox(CandidateSearchBox);
