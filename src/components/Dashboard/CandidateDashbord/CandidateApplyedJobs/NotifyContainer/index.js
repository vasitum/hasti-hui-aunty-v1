import React from "react";
import { Dropdown, Grid } from "semantic-ui-react";
import InputContainer from "../../../../Forms/InputContainer";
import InfoIconLabel from "../../../../utils/InfoIconLabel";
import TextAreaField from "../../../../Forms/FormFields/TextAreaField";
import IcAttachment from "../../../../../assets/svg/IcAttachment";
import FileuploadBtn from "../../../../Buttons/FileuploadBtn";
import "./index.scss";

const ReportProfileOption = [
  {
    text: "Reject Profile",
    value: "Reject Profile"
  },

  {
    text: "Reject Profile 1",
    value: "Reject Profile 1"
  }
];

const NotifyContainer = () => (
  <div className="NotifyContainer">
    <div className="notify_field">
      <InputContainer label="Use template" infoicon={<InfoIconLabel />}>
        <Dropdown
          placeholder="Select template"
          selection
          fluid
          options={ReportProfileOption}
        />

        <div className="Textarea_field">
          <Grid>
            <Grid.Row>
              <Grid.Column computer={14}>
                <TextAreaField />
              </Grid.Column>
              <Grid.Column computer={2}>
                <div className="attachment">
                  <FileuploadBtn
                    className="bgTranceparent"
                    btnIcon={<IcAttachment pathcolor="#0b9ed0" />}
                  />
                  {/* <IcAttachment pathcolor="#0b9ed0" /> */}
                </div>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>
      </InputContainer>
    </div>
  </div>
);

export default NotifyContainer;
