import React from "react";
import { Checkbox } from "semantic-ui-react";
import { withToggleRefinement } from "../../../../AlgoliaContainer";
import isEqual from "lodash.isequal";

class CandidateCVFilter extends React.Component {
  state = {
    checked: false
  };

  onChange = e => {
    const { refine } = this.props;

    this.setState(
      {
        checked: !this.state.checked
      },
      () => {
        refine(this.state.checked);
      }
    );
  };

  shouldComponentUpdate(nextProps) {
    if (isEqual(this.props, nextProps)) {
      return false;
    }

    return true;
  }

  componentDidMount() {
    if (this.props.currentRefinement == true) {
      this.setState({ checked: true });
    }
  }

  render() {
    return (
      <Checkbox
        checked={this.state.checked}
        onChange={this.onChange}
        label={this.props.label}
        style={this.props.style}
      />
    );
  }
}

export default withToggleRefinement(CandidateCVFilter);
