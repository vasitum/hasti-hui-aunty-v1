import React from "react";
import {
  Grid,
  Checkbox,
  Dropdown,
  Button,
  List,
  Popup,
  Header
} from "semantic-ui-react";

import CandidateSortBy from "../CandidateSortBy";
import CandidateCVFilter from "../CandidateCVFilter";
import CandidateDateFilter from "../CandidateDateFilter";
import FilterTags from "../../../Common/FilterTags";

import ICFilter from "../../../../../assets/svg/IcFilterIcon";
import IcCloseIcon from "../../../../../assets/svg/IcCloseIcon";
import { USER } from "../../../../../constants/api";
import "./index.scss";
// import FlatDefaultBtn from "../../../../Buttons/FlatDefaultBtn";

const FilterBar = () => (
  <div className="CandidateFilter">
    <div className="Filter_Main">
      {/* <div className="Filter_MainLeft">
        
      </div>
      <div className="Filter_MainRight"></div> */}
      <Grid>
        <Grid.Row>
          <Grid.Column
            className="Filter_text"
            width="13"
            style={{
              display: "flex",
              alignItems: "center"
            }}>
            <List horizontal className="List_Main">
              <List.Item className="Filter_Sort">
                <CandidateCVFilter
                  label={"Recruiter"}
                  attribute="candidateId"
                  defaultRefinement={true}
                  value={window.localStorage.getItem(USER.UID)}
                  style={{
                    display: "none"
                  }}
                />
                <List.Content />
              </List.Item>
              <List.Item className="Filter_Sort">
                <CandidateDateFilter
                  attribute={"interviewScheduledDate"}
                  label={"Interview"}
                />
                <List.Content />
              </List.Item>
              <List.Item className="Filter_Sort">
                <CandidateDateFilter
                  attribute={"appliedDate"}
                  label={"Submitted"}
                  past
                />
                <List.Content />
              </List.Item>
            </List>
          </Grid.Column>

          <Grid.Column width="3" textAlign="right">
            <List className="List_Main">
              <List.Item className="Filter_Sort">
                <List.Content>
                  <CandidateSortBy
                    defaultRefinement={"jobApplication_date"}
                    items={[
                      // { value: "jobApplication", label: "Relevant" },
                      { value: "jobApplication_date", label: "Recent" }
                    ]}
                  />
                </List.Content>
              </List.Item>
            </List>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
    <div className="addTag_section">
      <FilterTags />
    </div>
  </div>
);

export default FilterBar;
