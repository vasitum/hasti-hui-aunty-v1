import React from "react";
import CandidateApplyedJobsCards from "../CandidateApplyedJobsCards";
import InfoSection from "../../../../Sections/InfoSection";
import SkillListExpBtn from "../../../../CardElements/SkillListExpBtn";
import FlatDefaultBtn from "../../../../Buttons/FlatDefaultBtn";
import MediaCard from "../../../../Cards/MediaCard";
import QuillText from "../../../../CardElements/QuillText";

/**
 * @item: Actually render elements
 */
export default ({ hit, onItemSelect, noClick }) => {
  return (
    <CandidateApplyedJobsCards
      noClick={noClick}
      hit={hit}
      onItemSelect={onItemSelect}>
      {/* <div className="CandidatesCardDetail_body">
        <div className="CandidatesCardDetail_divider" />
        <div className="InfoCard_bodyContainer">
          <MediaCard fluid shadowless>
            <div className="InfoCard_jobDescription">
              {hit.candidateSummary ? (
                <InfoSection
                  headerSize="medium"
                  color="blue"
                  headerText="Summary">
                  <QuillText readOnly value={hit.candidateSummary} />
                </InfoSection>
              ) : null}
            </div>
            <div className="InfoCard_bodySkill">
              {hit.skills && hit.skills.length > 0 ? (
                <InfoSection
                  headerSize="medium"
                  color="blue"
                  headerText="Skills">
                  <SkillListExpBtn skills={hit.skills} />
                </InfoSection>
              ) : null}
            </div>

            <div className="InfoCard_bodyBtn">
              <FlatDefaultBtn btntext="View complete Job" />
            </div>
          </MediaCard>
        </div>
      </div> */}
    </CandidateApplyedJobsCards>
  );
};
