import React from "react";
import PropTypes from "prop-types";
import {
  Grid,
  Button,
  Icon,
  Checkbox,
  Dropdown,
  Image
} from "semantic-ui-react";
import MoreOption from "../../../../../assets/svg/IcMoreOptionIcon";
import MediaCard from "../../../../Cards/MediaCard";
import CardSummary from "../../../../Cards/CardSummary";
import SearchCard from "../../../../Cards/SearchCard";
import IcDownArrowIcon from "../../../../../assets/svg/IcDownArrow";
import IcCompany from "../../../../../assets/svg/IcCompany";
import ActionBtn from "../../../../Buttons/ActionBtn";
import SkillList from "../../../../CardElements/SkillList";

import CandidateCardFooter from "../CandidateCardFooter";

import IcCalendarCard from "../../../../../assets/svg/IcCalendarCard";
import fromNow from "../../../../../utils/env/fromNow";
import { Link } from "react-router-dom";
import { format } from "date-fns";

import "./index.scss";

const profileOptions = [
  {
    text: "Download resume",
    value: "Download resume"
  },
  {
    text: "Send message",
    value: "Send message"
  },
  {
    text: "Share",
    value: "Share"
  },
  {
    text: "Likes",
    value: "Likes"
  }
];

class JobMediaCard extends React.PureComponent {
  state = {
    checked: false
  };

  onCheckboxChange = (e, { value }) => {
    // let's not open the card shall we?
    // brit flair!
    e.stopPropagation();

    this.setState(
      {
        checked: !this.state.checked
      },
      () => {
        const { hit, onItemSelect } = this.props;
        onItemSelect(hit.objectID);
      }
    );
  };

  render() {
    const { hit } = this.props;
    console.log("check location", hit);
    return (
      <div className="CandidateMediaCard CandidateApplyedJobsCards">
        <MediaCard fluid>
          <MediaCard.Avatar>
            {/* TODO: Get Vijay to fix comId and enable it */}
            {!hit.comExt ? (
              <IcCompany
                height="50"
                width="50"
                rectcolor="#f7f7fb"
                pathcolor="#c8c8c8"
                style={{
                  borderRadius: "6px"
                }}
              />
            ) : (
              <Image
                verticalAlign="top"
                style={{
                  // position: "absolute",
                  borderRadius: "6px"
                }}
                src={`https://s3-us-west-2.amazonaws.com/img-mwf/company/${
                  hit.comId
                }/image.jpg`}
              />
            )}
            {/* <IcCompany
              height="50"
              width="50"
              rectcolor="#f7f7fb"
              pathcolor="#c8c8c8"
            /> */}
            {/* <div className="CandidatesCardMatch">
              <p>{hit.matches}%</p>
              <span>matched</span>
            </div> */}
          </MediaCard.Avatar>
          <Grid>
            <Grid.Row>
              <Grid.Column computer={8} mobile={14}>
                <CardSummary>
                  <CardSummary.Item header>
                    {/* {hit.candidateName} */}
                    <Link
                      to={`/job/candidate/${hit.jobId}/application/${
                        hit.objectID
                      }`}>
                      {hit.jobName}
                    </Link>
                  </CardSummary.Item>
                  <div className="CardProfileHeaderDetail">
                    <CardSummary.Item>
                      {/* {hit.candidateLocation} */}
                      {hit.comName}
                    </CardSummary.Item>
                    <div className="LocationExp">
                      <p className="location">{hit.jobLocCity}</p>
                      {/* {hit.jobExpMax ? (<p className="exp">
                        <span>{hit.jobExpMin} - {hit.jobExpMax}</span> yrs experience
                      </p>) : null } */}
                    </div>
                    <div className="LocationExp">
                      {hit.jobExpMax ? (
                        <p className="location">
                          <span>
                            {hit.jobExpMin} - {hit.jobExpMax}
                          </span>{" "}
                          yrs experience
                        </p>
                      ) : null}
                    </div>

                    {/* <div className="skills">
                      <p>
                        Skills: <SkillList />
                      </p>
                    </div> */}
                    {/* <div className="applied">
                      <p>
                        {" "}
                        Applied: <span>{hit.jobName}</span>
                      </p>
                    </div> */}
                  </div>
                </CardSummary>
              </Grid.Column>

              <Grid.Column
                className="moblilMore_cardDetail"
                mobile={5}
                textAlign="right">
                <Dropdown
                  pointing="top left right"
                  className="mobileDropdown_cardDetail"
                  icon={<MoreOption pathcolor="#acaeb5" />}>
                  <Dropdown.Menu>
                    {/* <Dropdown.Item
                      as={"a"}
                      href={hit.objectID}
                      text="Download resume"
                    /> */}
                    <Dropdown.Item
                      as={Link}
                      to={"/messaging/" + hit.reqId}
                      text="Send message"
                    />
                    <Dropdown.Item
                      className="Desc_option"
                      description={fromNow(hit.appliedDate)}
                      text="Submitted"
                    />
                  </Dropdown.Menu>
                </Dropdown>
              </Grid.Column>

              <Grid.Column
                computer={8}
                textAlign="right"
                className="desktopViewBtn_cardDetail">
                <div className="SearchCardHeader">
                  <CardSummary centered>
                    <CardSummary.Extra
                      extras={[
                        {
                          key: 1,
                          head: "Submitted:",
                          body: fromNow(hit.appliedDate),
                          class: "todayTitle",
                          title: new Date(hit.appliedDate)
                        }
                      ]}
                    />
                  </CardSummary>

                  {/* <span className="accordionArrowCircle">
                    <Icon name="caret down" className="accordionArrow" />
                  </span> */}
                </div>

                <div className="SearchCardHeaderBtn">
                  {/* <Button ></Button> */}
                  {hit.interviewScheduledDate ? (
                    <Button className="interViewBtn">
                      <div className="interViewBtn_box">
                        <div className="svgIcon">
                          <IcCalendarCard pathcolor="#0bd0bb" />
                        </div>
                        <div className="interTitle">
                          <p>
                            Date:{" "}
                            <span>
                              {format(
                                hit.interviewScheduledDate,
                                "DD MMM, YYYY"
                              )}
                            </span>
                          </p>
                          <p>
                            Time:{" "}
                            <span>
                              {format(hit.interviewScheduledDate, "hh:mm A")}
                            </span>
                          </p>
                        </div>
                      </div>
                    </Button>
                  ) : null}

                  {/* TODO: Add Image Extension */}
                  {/* {hit.candidateImageExt ? (
                    <Button as={"a"} className="downloadCV_btn">
                      Download CV
                    </Button>
                  ) : null} */}
                  <ActionBtn
                    // as={Link}
                    as={Link}
                    to={`/job/candidate/${hit.jobId}/application/${
                      hit.objectID
                    }`}
                    // to={"/messaging/" + hit.reqId}
                    actioaBtnText="View detail"
                    // btnIcon={<IcDownArrowIcon pathcolor="#99d9f9" />}
                  />
                </div>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </MediaCard>
      </div>
    );
  }
}

const InfoCard = props => {
  const { hit, noClick, onItemSelect, children } = props;
  // console.log("Hit candidate", hit);
  return (
    <div className="CandidatesCardDetail">
      <SearchCard>
        <SearchCard.Accordion
          noClick={noClick}
          header={() => <JobMediaCard hit={hit} onItemSelect={onItemSelect} />}
          body={() => <div>{children}</div>}
        />
        <CandidateCardFooter hit={hit} />
      </SearchCard>
    </div>
  );
};

InfoCard.propTypes = {};

InfoCard.defaultProps = {};

export default InfoCard;
