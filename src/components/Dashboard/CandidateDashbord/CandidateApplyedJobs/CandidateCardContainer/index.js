import React, { Component } from "react";
import { withHits } from "../../../../AlgoliaContainer";
import CandidateDetailCard from "../CandidateDetailCard";
import isEqual from "lodash.isequal";

import NoFoundMessageDashboard from "../../../Common/NoFoundMessageDashboard";

class CandidateCardContainer extends Component {
  shouldComponentUpdate(nextProps) {
    if (isEqual(this.props, nextProps)) {
      return false;
    }

    return true;
  }

  render() {
    const { hits, onCardSelect, noClick } = this.props;

    if (!hits || hits.length === 0) {
      return (
        <NoFoundMessageDashboard
          noFountTitle="No data found"
          noFountSubTitle="Start applying for jobs to kick start your career"
        />
      );
    }
    return hits.map(hit => {
      return (
        <CandidateDetailCard
          noClick={noClick}
          onItemSelect={onCardSelect}
          hit={hit}
        />
      );
    });
  }
}

export default withHits(CandidateCardContainer);
