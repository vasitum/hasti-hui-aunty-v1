import React from "react";
import { Dropdown } from "semantic-ui-react";
import { withSortBy } from "../../../../AlgoliaContainer";
import isEqual from "lodash.isequal";

class CandidateSortBy extends React.Component {
  state = {};

  shouldComponentUpdate(nextProps) {
    if (isEqual(this.props, nextProps)) {
      return false;
    }

    return true;
  }

  render() {
    const SortBy = [
      {
        text: "Relevant",
        value: "jobApplication",
        key: "jobApplication"
      },
      {
        text: "Recent",
        value: "jobApplication_date",
        key: "jobApplication_date"
      }
    ];

    const { refine, currentRefinement } = this.props;

    return (
      <span>
        Sort by:{" "}
        <Dropdown
          onChange={(e, { value }) => refine(value)}
          inline
          pointing="top right"
          options={SortBy}
          value={currentRefinement}
        />
      </span>
    );
  }
}

export default withSortBy(CandidateSortBy);
