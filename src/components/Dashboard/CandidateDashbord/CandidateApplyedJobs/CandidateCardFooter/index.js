import React from "react";
import { Grid, Dropdown, Button, Modal } from "semantic-ui-react";

import IcMoreOptionIcon from "../../../../../assets/svg/IcMoreOptionIcon";

import getPlaceholder from "../../../../Forms/FormFields/Placeholder";

import MoreOption from "../../../Common/MoreOption";
import { CAND_STAGES } from "../../../../../constants/applicationStages";

import "./index.scss";

const MoreOptionsTrigger = (
  <span>
    <IcMoreOptionIcon pathcolor="#acaeb5" />
  </span>
);

const MoreOptions = [
  {
    text: "Share",
    value: "Share"
  }
];

const STAGES = CAND_STAGES;

const CandidateCardFooter = ({ hit }) => (
  <div className="CardStatusFooter CandidateCardFooter CandidateDhashboard_cardFooter">
    <div className="CardStatusFooter_container">
      <div className="CardStatusFooter_left">
        <div className="footerTItle">
          <p>{getPlaceholder("Status", "Application current status")}</p>
        </div>
        <div className="CardStatusFooter_Modal">
          <Button className="CardFooter_btn" style={{ cursor: "default" }}>
            {STAGES[hit.status]}
          </Button>
        </div>
      </div>
      <div className="CardStatusFooter_right">
        <MoreOption url={`/view/user/${hit.objectID}`} />
      </div>
    </div>
  </div>
  // <div className="Main_Card">
  //   <Grid className="Main_Card_Grid">
  //     <Grid.Row className="Main_Card_Grid_Row">
  //       <Grid.Column
  //         mobile={4}
  //         computer={5}
  //         className="mainCard_apllicationStatus"
  //         textAlign="right">
  //         <h4>{getPlaceholder("Status", "Application current status")}</h4>
  //       </Grid.Column>

  //       <Grid.Column
  //         mobile={12}
  //         computer={9}
  //         className="Basic_Section"
  //         textAlign="left">
  //         <Button>{STAGES[hit.status]}</Button>
  //         <Modal
  //           className="rejectModal_container"
  //           trigger={<Button>{hit.status}</Button>}
  //           closeIcon
  //           closeOnDimmerClick={false}>
  //           <RejectModal
  //             status={hit.status}
  //             jobId={hit.jobId}
  //             applicationId={hit.objectID}
  //           />
  //         </Modal>
  //       </Grid.Column>

  //       <Grid.Column
  //         computer={2}
  //         className="mobile hidden"
  //         textAlign="right"
  //         style={{
  //           display: "flex",
  //           alignItems: "center",
  //           justifyContent: "flex-end"
  //         }}>
  //         <Dropdown
  //           className="DropDownMenuNotShowingOnMObile"
  //           trigger={MoreOptionsTrigger}
  //           options={MoreOptions}
  //           pointing="right bottom"
  //           icon={null}
  //         />
  //       </Grid.Column>
  //     </Grid.Row>
  //   </Grid>
  // </div>
);

export default CandidateCardFooter;
