import React from "react";
import { Button, Form, Dropdown, Grid } from "semantic-ui-react";
import NotifyContainer from "../NotifyContainer";
import FlatDefaultBtn from "../../../../Buttons/FlatDefaultBtn";
import ActionBtn from "../../../../Buttons/ActionBtn";
import IcInfoIcon from "../../../../../assets/svg/IcInfo";
import CheboxButton from "../../../../Forms/FormFields/CheboxButton";

import "./index.scss";

const ReportProfileOption = [
  {
    text: "Reject Profile",
    value: "Reject Profile"
  },
  {
    text: "Reject Profile 1",
    value: "Reject Profile 1"
  }
];

const ReportProfile = () => (
  <div>
    <Form className="Tag_Form"> 
      <label>Report this profile</label>
      <div className="" style={{ marginTop: "20px" }}>
        <Grid>
          <Grid.Row>
            <Grid.Column computer={8}>
              <CheboxButton checkboxTitle="Unresponsive" />
            </Grid.Column>
            <Grid.Column computer={8}>
              <CheboxButton checkboxTitle="Don’t meet qualifications" />
            </Grid.Column>
          </Grid.Row>

          <Grid.Row>
            <Grid.Column computer={8}>
              <CheboxButton checkboxTitle="Failed interview" />
            </Grid.Column>
            <Grid.Column computer={8}>
              <CheboxButton checkboxTitle="Accepted another offer" />
            </Grid.Column>
          </Grid.Row>

          <Grid.Row>
            <Grid.Column computer={8}>
              <CheboxButton checkboxTitle="Bad timings" />
            </Grid.Column>
            <Grid.Column computer={8}>
              <CheboxButton checkboxTitle="Something else" />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
      <div className="btnclass">
        <FlatDefaultBtn classNames="bgTranceparent" btntext="Cancel" />
        {/* <FlatDefaultBtn classNames="bgTranceparent" btntext="Save template" /> */}
        <ActionBtn actioaBtnText="Submit" />
      </div>
    </Form>
  </div>
);

export default ReportProfile;
