import React from "react";

import { Container } from "semantic-ui-react";
import PropTypes from "prop-types";

import CandidateStatistic from "./CandidateStatistic";
import CandidateFilter from "./CandidateFilter";
import ShowApplicant from "./ShowApplicant";

// import ShowApplicantHeader from "../../Common/ShowApplicantHeader";

import CandidateCardContainer from "./CandidateCardContainer";
import MultiCheckPopup from "./MultipleChekPopup";
import ReactGA from "react-ga";
import { withSearch } from "../../../AlgoliaContainer";
import { withRouter } from "react-router-dom";
import "./index.scss";

class Candidates extends React.Component {
  constructor(props) {
    super(props);
    this.selectedItems = [];
  }

  componentDidMount() {
    ReactGA.pageview('/job/candidate/dashboard');
  }

  state = {
    selectedItems: []
  };

  onItemSelect = itemId => {
    if (!this.state.selectedItems.includes(itemId)) {
      this.setState({
        selectedItems: [...this.state.selectedItems, itemId]
      });
    } else {
      this.setState({
        selectedItems: this.state.selectedItems.filter(val => val !== itemId)
      });
    }

    // console.log(this.selectedItems);
  };

  render() {
    return (
      <Container>
        <div className="Candidates">
          <ShowApplicant attribute={"jobName"} />
          <CandidateStatistic attribute={"status"} />
          <CandidateFilter />
          <CandidateCardContainer noClick onCardSelect={this.onItemSelect} />

          {/* TODO: Bulk Action Design */}
          {/* {this.state.selectedItems.length > 0 ? <MultiCheckPopup /> : null} */}
        </div>
      </Container>
    );
  }
}

export default withRouter(
  withSearch(Candidates, {
    apiKey: process.env.REACT_APP_ALGOLIA_APPLICATION_API_KEY,
    appId: process.env.REACT_APP_ALGOLIA_APPLICATION_APP_ID,
    indexName: process.env.REACT_APP_ALGOLIA_APPLICATION_INDEX_NAME,
    noRouting: true
  })
);
