import React from "react";
import { Grid, Input } from "semantic-ui-react";

import { Form } from "formsy-semantic-ui-react";

import SelectOptionField from "../../../../Forms/FormFields/SelectOptionField";
import getPlaceholder from "../../../../Forms/FormFields/Placeholder";
// import SearchIcon from "../../assets/svg/SearchIcon";
import CandidateSearchBox from "../CandidateSearchBox";
import "./index.scss";
import { withSelect } from "../../../../AlgoliaContainer";
import isEqual from "lodash.isequal";

class ShowApplicant extends React.PureComponent {
  // shouldComponentUpdate(nextProps) {
  //   if (isEqual(this.props, nextProps)) {
  //     return false;
  //   }

  //   return true;
  // }

  render() {
    const { items, currentRefinement, onSelect } = this.props;
    return (

      <div className="recommendedJobs_header">
        <div className="recommendedJobs_headerLeft">
          <CandidateSearchBox />
        </div>
      </div>
    );
  }
}

export default withSelect(ShowApplicant);
// export default ShowApplicant;
