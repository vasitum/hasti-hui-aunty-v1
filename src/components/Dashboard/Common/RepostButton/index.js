import React, { Component } from "react";
import {
  Icon,
  Modal,
  Header,
  Button,
  Popup,
  Grid,
  Transition,
  TransitionablePortal
} from "semantic-ui-react";
import { withRouter } from "react-router-dom";
import "./index.scss";

class RepostButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalOpen: false,
      visible: true,
      isOpen: false,
      modal: false
    };
  }

  handleOpen = () => {
    const { shortListCount, jobId } = this.props;

    if (!shortListCount) {
      this.props.history.push(`/job/new/${jobId}`);
      return;
    }

    this.setState({ modalOpen: true, visible: true, isOpen: true });
  };

  onRepost = () => {
    const { shortListCount, jobId } = this.props;

    this.props.history.push(`/job/new/${jobId}`);
  };

  handleClose = () =>
    this.setState({ modalOpen: false, visible: false, isOpen: false });

  toggleVisibility = () => this.setState({ visible: !this.state.visible });

  handleModal = () => this.setState({ modal: !this.state.modal });

  render() {
    const { visible } = this.state;
    const { shortListCount, jobId } = this.props;
    return (
      <React.Fragment>
        <Button className="repost-button" onClick={this.handleOpen}>
          Repost
        </Button>
        <TransitionablePortal
          onClose={this.handleClose}
          open={this.state.modalOpen}
          transition={{ animation: "fade up", duration: 300 }}>
          <Modal
            open={this.state.modalOpen}
            onClose={this.handleClose}
            className="repost-open-modal-block"
            size="small">
            <Modal.Content>
              <div className="cross-button">
                <Icon
                  name="close"
                  className="cross-icon"
                  onClick={this.handleClose}
                />
              </div>
              <div className="text-style">
                <p className="text-style-font1">
                  <strong> {shortListCount} </strong> candidates are still{" "}
                  <strong> SHORTLISTED </strong>
                </p>
                <p className="text-style-font2">
                  Would you like to cancel this process and see those candidates
                  or want to continue?
                </p>
              </div>
              <div className="action-button">
                <Button
                  onClick={this.onRepost}
                  className="repost-button mb-10 repost-active">
                  Continue to repost
                </Button>
                <Button
                  onClick={this.handleClose}
                  className="repost-button mb-10">
                  Cancel this process
                </Button>
              </div>
            </Modal.Content>
          </Modal>
        </TransitionablePortal>

        {/* <Transition visible={visible} animation="scale" duration={500}>
          <Popup
            trigger={
              <Button className="repost-button" onClick={this.toggleVisibility}>
                Repost
              </Button>
            }
            flowing
            open={this.state.isOpen}
            onClose={this.handleClose}
            onOpen={this.handleOpen}
            on="click"
            className="repost-open-modal-block">
            <div className="text-style">
              <p className="text-style-font">
                <strong> 5 </strong> candidates are still{" "}
                <strong> SHORTLISTED </strong>
              </p>
              <p className="text-style-font">
                Would you like to cancel this process and see those candidates
                or want to continue?
              </p>
            </div>
            <div className="action-button">
              <Button className="repost-button mb-10">
                Continue to repost
              </Button>
              <Button className="repost-button mb-10">
                Cancel this process
              </Button>
            </div>
          </Popup>
        </Transition> */}
      </React.Fragment>
    );
  }
}

export default withRouter(RepostButton);
