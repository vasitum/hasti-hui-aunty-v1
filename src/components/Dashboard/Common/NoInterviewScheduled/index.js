import React from "react";

import { Button, Modal } from "semantic-ui-react";

import DashbordSechuleInterView from "../DashbordSechuleInterView";

import IcCalendarCard from "../../../../assets/svg/IcCalendarCard";
import IcDownArrow from "../../../../assets/svg/IcDownArrow";

import "./index.scss";

class NoInterviewScheduled extends React.Component {
  state = {
    scheduleModal: false
  };

  onScheduleModalOpen = e => {
    this.setState({ scheduleModal: true });
  };

  onScheduleModalClose = e => {
    this.setState({ scheduleModal: false });
  };

  onSchedule = e => {
    this.onScheduleModalClose();
  };

  render() {
    const { candidate, applicationData } = this.props;

    return (
      <div className="NoInterviewScheduled">
        <div className="Scheduled_innerSection">
          <div className="logo">
            <IcCalendarCard width="54" height="53" pathcolor="#acaeb5" />
          </div>
          <p className="NoInterviewTitle">No action yet!</p>
          <p className="NoInterview_titleSub">
            Have patience, great things might happen soon
          </p>
          {/* <p className="NoInterview_titleSub">Great things might happen.</p> */}
          {!candidate ? (
            <Modal
              open={this.state.scheduleModal}
              onClose={this.onScheduleModalClose}
              className="CardStatusFooterModal_container"
              trigger={
                <Button
                  onClick={this.onScheduleModalOpen}
                  compact
                  className="NoInterviewBtn">
                  Schedule interview{" "}
                  <IcDownArrow pathcolor="#0b9ed0" height="5.6" />
                </Button>
              }
              closeIcon
              closeOnDimmerClick={false}>
              <DashbordSechuleInterView
                onCancelClick={this.onScheduleModalClose}
                onInterviewScheduled={this.onSchedule}
                applicationData={applicationData}
                notConnected
              />
            </Modal>
          ) : null}
        </div>
      </div>
    );
  }
}

export default NoInterviewScheduled;
