import React from "react";
import { Dropdown } from "semantic-ui-react";
import { withSortBy } from "../../../AlgoliaContainer";

import isEqual from "lodash.isequal";

class SortBy extends React.Component {
  state = {
    currentVal: "job"
  };

  componentDidMount() {
    const { currentRefinement, refine } = this.props;

    if (!currentRefinement) {
      return;
    }

    this.setState({
      currentVal: currentRefinement
    });
  }

  onChange = (e, { value }) => {
    const { refine } = this.props;
    this.setState(
      {
        currentVal: value
      },
      () => {
        refine(value);
      }
    );
  };

  render() {
    const SortByMenu = [
      [
        { text: "Relevant", value: "job", key: "job" },
        { text: "Recent", value: "job_by_date", key: "job_by_date" }
      ]
    ];

    const { refine, items, currentRefinement } = this.props;

    return (
      <span>
        Sort by:{" "}
        <Dropdown
          onChange={this.onChange}
          inline
          options={items}
          value={this.state.currentVal}
          pointing="top right"
        />
      </span>
    );
  }
}

export default withSortBy(SortBy);
