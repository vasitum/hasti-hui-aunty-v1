import React from "react";
import { Grid, Input, Header } from "semantic-ui-react";

import { Form } from "formsy-semantic-ui-react";

import SelectOptionField from "../../../Forms/FormFields/SelectOptionField";
import getPlaceholder from "../../../Forms/FormFields/Placeholder";
// import SearchIcon from "../../assets/svg/SearchIcon";

import SearchBox from "../SearchBox";

import "./index.scss";
// import { withSelect } from "../../../AlgoliaContainer";
import isEqual from "lodash.isequal";

class ShowApplicantHeader extends React.PureComponent {
  shouldComponentUpdate(nextProps) {
    if (isEqual(this.props, nextProps)) {
      return false;
    }

    return true;
  }

  render() {
    
    return (
      <div className="ShowApplicantMain_Grid">
        <div className="ShowApplicant_left">
          <Header as="h2">My Jobs</Header>
        </div>
        <div className="ShowApplicant_right">
          <SearchBox />
        </div>
      </div>
    );
  }
}

export default ShowApplicantHeader;