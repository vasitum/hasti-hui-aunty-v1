import React from "react";
import IcThupUp from "../../../../assets/svg/IcThupUp";
import getScreeningByApplication from "../../../../api/jobs/getScreeningByapplication";
import "./index.scss";

const status = {
  location: "Job Location",
  education: "Education Eligiblity",
  experience: "Experience Eligibility",
  work_shift: "Work Shift Preference",
  join_timing: "Joining Availablity",
  skills: "Skills"
};

function renderText(text) {
  const mtext = document.createElement("p");
  mtext.innerHTML = text;
  return mtext.innerText;
}

function makeScreeningSense(screening) {
  // console.log("screening data", screening);
  if (!screening) {
    return null;
  }
  const screeningEnd = screening.screenEnd;
  const report = screeningEnd.report;
  let totalData = 0;
  const finalReport = Object.keys(report).map(val => {
    if (report[val]) {
      totalData = totalData + 1;
    }

    if (val === "skills") {
      return {
        stage: val,
        cleared: report[val]
          ? Object.keys(report[val]).map(value => {
              return {
                skill: value,
                cleared: report[val][value]
              };
            })
          : null
      };
    }

    // if (report[val]) {
    //   finalReport.total = finalReport.total + 1;
    // }
    return {
      stage: val,
      cleared: report[val]
    };
  });
  // let y = finalReport.map(item => {
  //   let p = item.cleared ? item.cleared : null;
  //   return { stage: item.stage, cleared: p };
  // });
  // console.log("y data", y);
  let finalDataReport = finalReport.filter(item => {
    if (item.cleared === null) {
      return false;
    }
    return true;
  });
  let totalDataFromServer = Object.keys(finalDataReport).length;

  // console.log("final report", finalReport);
  // console.log("final Datareport", finalDataReport);

  return {
    total: totalData,
    // data: finalReport,
    data: finalDataReport,
    ques: screeningEnd.questions,
    totalData: totalDataFromServer
  };
}

class ScreeningPageSection extends React.Component {
  state = {
    screening: null
  };

  async componentDidMount() {
    const { applicationData } = this.props;

    if (!applicationData) {
      return null;
    }

    const { screeningStatus } = applicationData;
    if (!screeningStatus || screeningStatus === "Screening Pending") {
      return null;
    }

    try {
      const res = await getScreeningByApplication(
        applicationData.pId,
        applicationData.candidateId
      );

      if (res.status === 200) {
        const data = await res.json();
        // console.log("___MY_DTAT__", data);
        this.setState({
          screening: data
        });
      }
    } catch (error) {}
  }

  render() {
    const { applicationData } = this.props;
    const { screening } = this.state;

    if (!applicationData) {
      return null;
    }

    const { screeningStatus } = applicationData;

    if (!screeningStatus || screeningStatus === "Screening Pending") {
      return (
        <div
          style={{
            padding: "10px 10px 20px 10px",
            textAlign: "center"
          }}>
          Screening is currently pending
        </div>
      );
    }

    if (screening) {
      const { total, data, ques, totalData } = makeScreeningSense(screening);
      return (
        <div className="ScreeningPageSection">
          <div className="ScreeningPageSection_container">
            <div className="ScreeningSection_header">
              <div className="overAll_matchCoutBox">
                <p className="title">0{total}</p>
                <div className="divider" />
                <p className="subTitle">0{totalData}</p>
              </div>
              <div className="leftTitle">
                <p>Overall Matched</p>
              </div>
            </div>
            <div className="ScreeningSection_body">
              <div className="matchHeaderTitle">
                <div className="headerTitle_left">
                  <p>Questions</p>
                </div>
                <div className="headerTitle_right">
                  <p>Match</p>
                </div>
              </div>
              {data.map((val, idx) => {
                if (val.stage === "skills") {
                  // console.log("Skills Found");
                  return (
                    <React.Fragment key={idx}>
                      <div className="match_listContainer">
                        <div className="list_counter">
                          <p>0{idx + 1}</p>
                        </div>
                        <div className="match_listBox">
                          <div className="match_listBoxInner">
                            <p className="listBoxInner_title">
                              {status[val.stage]}
                            </p>
                            {val.cleared ? (
                              <div className="match_parcent">
                                <span className="thumb">
                                  <IcThupUp pathcolor="#43a047" />
                                </span>
                              </div>
                            ) : (
                              <div className="match_parcent">
                                <span className="thumb reject_thumb">
                                  <IcThupUp pathcolor="#e2190f" />
                                </span>
                              </div>
                            )}
                          </div>
                        </div>
                      </div>
                      {val.cleared.map((value, index) => {
                        return (
                          <div
                            key={index}
                            className="match_listContainer skill"
                            style={{
                              width: "95%",
                              marginLeft: "auto"
                            }}>
                            <div className="list_counter">
                              <p>0{index + 1}</p>
                            </div>
                            <div className="match_listBox">
                              <div className="match_listBoxInner">
                                <p className="listBoxInner_title">
                                  {value.skill}
                                </p>
                                {value.cleared ? (
                                  <div className="match_parcent">
                                    <span className="thumb">
                                      <IcThupUp pathcolor="#43a047" />
                                    </span>
                                  </div>
                                ) : (
                                  <div className="match_parcent">
                                    <span className="thumb reject_thumb">
                                      <IcThupUp pathcolor="#e2190f" />
                                    </span>
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>
                        );
                      })}
                    </React.Fragment>
                  );
                }

                return (
                  <div className="match_listContainer">
                    <div className="list_counter">
                      <p>0{idx + 1}</p>
                    </div>
                    <div className="match_listBox">
                      <div className="match_listBoxInner">
                        <p className="listBoxInner_title">
                          {status[val.stage]}
                        </p>
                        {val.cleared ? (
                          <div className="match_parcent">
                            <span className="thumb">
                              <IcThupUp pathcolor="#43a047" />
                            </span>
                          </div>
                        ) : (
                          <div className="match_parcent">
                            <span className="thumb reject_thumb">
                              <IcThupUp pathcolor="#e2190f" />
                            </span>
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
          <div className="ScreeningSection_footer">
            {ques.map((val, idx) => {
              return (
                <React.Fragment key={idx}>
                  <div className="footerQuestion_title">
                    <div className="titleLeft">
                      <p>Q. {idx + 1}</p>
                    </div>
                    <div className="titleRight">
                      <p>{val.ques}</p>
                    </div>
                  </div>
                  <div className="footerQuestion_title">
                    <div className="titleLeft">
                      <p>A.</p>
                    </div>
                    <div className="titleRight rightBox">
                      <p>{renderText(val.ans)}</p>
                    </div>
                  </div>
                </React.Fragment>
              );
            })}
          </div>
        </div>
      );
    }
    return null;
  }
}

export default ScreeningPageSection;
