import React from "react";

import ModalBanner from "../../../ModalBanner";

import ScheduleInterview from "../../../Messaging/InterView/ScheduleInterview";

import "./index.scss";

class DashbordSechuleInterView extends React.Component {
  render() {
    return (
      <div className="DashbordSechuleInterView">
        <ModalBanner headerText="Schedule interview" />
        <ScheduleInterview
          {...this.props}
          DashBoardInterview="DashbordInterView"
        />
      </div>
    );
  }
}

export default DashbordSechuleInterView;
