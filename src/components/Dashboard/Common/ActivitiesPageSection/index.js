import React from "react";
import getApplicationActivies from "../../../../api/jobs/getApplicationActivies";
import fromNow from "../../../../utils/env/fromNow";
import NoFoundMessageDashboard from "../NoFoundMessageDashboard";
import {
  RECR_STAGES,
  CAND_STAGES
} from "../../../../constants/applicationStages";
import "./index.scss";

function getRejectReason(item) {
  if (!item.rejectReason) {
    return null;
  }

  return item.rejectReason.split("$");
}

class ActivitiesPageSection extends React.Component {
  state = {
    data: [],
    rejectreason: ""
  };

  async componentDidMount() {
    const { jobId, applicationId } = this.props;

    try {
      const res = await getApplicationActivies(jobId, applicationId);
      if (res.status) {
        const data = await res.json();
        // console.log(data);
        this.setState({ data: data.activity, rejectreason: data.rejectReason });
      } else {
        console.error(res);
      }
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const { candidate } = this.props;
    if (this.state.data.length === 0) {
      return (
        <NoFoundMessageDashboard
          noFountTitle="No data found"
          noFountSubTitle="Here you will see your companies"
        />
      );
    }

    if (candidate) {
      return this.state.data.map(item => {
        if (!item.status) {
          return (
            <div className="ActivitiesPageSection">
              <div className="PageSection_container">
                <p className="ActivitiesTitle">You applied to this job.</p>
                <p className="Activities_subTitle">{fromNow(item.time)}</p>
              </div>
              {/* {this.state.data.map({})} */}
            </div>
          );
        }

        return (
          <div className="ActivitiesPageSection">
            <div className="PageSection_container">
              <p className="ActivitiesTitle">
                Your application status moved from{" "}
                <span>{CAND_STAGES[item.previousStatus]}</span> to{" "}
                <span>{CAND_STAGES[item.status]}</span>.
              </p>
              <p className="Activities_subTitle">{fromNow(item.time)}</p>
            </div>
            {/* {this.state.data.map({})} */}
          </div>
        );
      });
    }

    return this.state.data.map(item => {
      if (!item.status) {
        return null;

        return (
          <div className="ActivitiesPageSection">
            <div className="PageSection_container">
              <p className="ActivitiesTitle">New application to this job.</p>
              <p className="Activities_subTitle">{fromNow(item.time)}</p>
            </div>
            {/* {this.state.data.map({})} */}
          </div>
        );
      }

      const reason = getRejectReason(item);

      return (
        <div className="ActivitiesPageSection">
          <div className="PageSection_container">
            <p className="ActivitiesTitle">
              You shifted application from{" "}
              <span>{RECR_STAGES[item.previousStatus]}</span> to{" "}
              <span>{RECR_STAGES[item.status]}</span>.
            </p>
            {reason && item.status === "Rejected" ? (
              <React.Fragment>
                <span>
                  <strong>Reject Reason:</strong> {reason[1]}
                </span>
                <br />
                {reason[0] ? (
                  <span>
                    <strong>Additional Reason:</strong> {reason[0]}
                  </span>
                ) : null}
              </React.Fragment>
            ) : null}
            <p className="Activities_subTitle">{fromNow(item.time)}</p>
          </div>
          {/* {this.state.data.map({})} */}
        </div>
      );
    });
  }
}

export default ActivitiesPageSection;
