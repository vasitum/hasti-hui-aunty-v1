import { Form } from "formsy-semantic-ui-react";
import React from "react";
import { Dropdown } from "semantic-ui-react";
import ReactGA from "react-ga";
import { USER } from "../../../../constants/api";
import { RejectModalString } from "../../../EventStrings/RejectModalString";


// import NotifyContainer from "../../RecruiterDashboard/Candidates/NotifyContainer";
import ActionBtn from "../../../Buttons/ActionBtn";
import FlatDefaultBtn from "../../../Buttons/FlatDefaultBtn";
import ModalBanner from "../../../ModalBanner";

import changeStatus from "../../../../api/jobs/changeStatus";

import { toast } from "react-toastify";

import "./index.scss";

const RejectModalOption = [
  {
    text: "Select",
    value: ""
  },
  {
    text: "Draft",
    value: "Draft"
  },
  {
    text: "Active",
    value: "Active"
  },
  {
    text: "Close",
    value: "Close"
  }
  // {
  //   text: "Renewed",
  //   value: "Renewed"
  // },
  // {
  //   text: "Paused",
  //   value: "Paused"
  // }
];

class RejectModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      IsShowNotify: false,
      currentVal: "Active"
    };

    // bind data
    this.onSubmit = this.onSubmit.bind(this);
  }

  IsShowNotifyBox = () => {
    this.setState({
      IsShowNotify: !this.state.IsShowNotify
    });
  };

  async onSubmit(e) {
    const { status, jobId, onChange } = this.props;

    if (!this.state.currentVal) {
      return;
    }

    try {
      const res = await changeStatus(jobId, this.state.currentVal);

      if (res.status === 406) {
        toast("To change the status, Please add screening to your job");
        return;
      }

      if (res.status === 200) {
        const data = await res.json();
        // console.log(data);
        if (onChange) {
          onChange(this.state.currentVal);
        }
      }
    } catch (error) {}
  }

  componentDidMount() {
    const { status } = this.props;
    if (status !== this.state.currentVal) {
      this.setState({
        currentVal: status
      });
    }
  }

  onChange = (e, { value }) => {
    this.setState({
      currentVal: value
    });
  };

  render() {
    const UserId = window.localStorage.getItem(USER.UID);
    return (
      <div className="RejectModal">
        <ModalBanner headerText="Change status" />
        <div className="userInterView_staus">
          <Form onSubmit={this.onSubmit}>
            <div className="userInterView_stausInner">
              <div>
                <Dropdown
                  placeholder="Select Status"
                  selection
                  fluid
                  onChange={this.onChange}
                  value={this.state.currentVal}
                  options={RejectModalOption}
                />
              </div>
            </div>
            {this.state.currentVal === "Close" ? (
              <div className="statusClose_message">
                <span className="warning_text"> Warning:</span> Once closed you
                cannot reopen this position. To temporary unpublish, select{" "}
                <span className="draft_text">Draft.</span>
              </div>
            ) : null}

            <div className="RejectModal_footer">
              <FlatDefaultBtn
                onClick={this.props.onClose}
                classNames="bgTranceparent"
                btntext="Cancel"
              />
              <ActionBtn
                disabled={
                  this.state.currentVal === this.props.status ||
                  !this.state.currentVal
                }
                actioaBtnText="Change"
                onClick={() => {
                  ReactGA.event({
                    category: RejectModalString.ChangeStatusEvent.category,
                    action: RejectModalString.ChangeStatusEvent.action,
                    value: UserId,
                  });
                }}
              />
            </div>
          </Form>
        </div>
      </div>
    );
  }
}

export default RejectModal;
