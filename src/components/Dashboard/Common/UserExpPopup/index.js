import React, { Component } from "react";
import BorderButton from "../../../Buttons/BorderButton";
import "./index.scss";
import { Icon, Modal } from "semantic-ui-react";
import userExpPopup from "../../../../api/user/userExpPopup";
import ThanksIcon from "../../../../assets/svg/ic_thank_you.svg";
// import ThanksIcon from "../../../../assets/svg/IcThankYou";
import VasiImage from "../../../../assets/img/vasi-bot.png";
export default class UserExpPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      delay1: false,
      delay2: false,
      delay3: false,
      showThanku: false
    };
    this.handleUserExpFeedback = this.handleUserExpFeedback.bind(this);
  }

  componentDidMount = async () => {
    setTimeout(() => this.setState({ delay1: true }), 1000);
    setTimeout(() => this.setState({ delay2: true }), 2000);
    setTimeout(() => this.setState({ delay3: true }), 3000);
  };

  handleUserExpFeedback = async text => {
    try {
      const res = await userExpPopup(text);
      if (res.status === 200) {
        const data = await res.json();
        this.setState({ showThanku: true });
        // console.log("preference save succefully");
        setTimeout(() => this.props.handleModal(), 3000);
      }
    } catch (error) {
      console.error(error);
    }
  };

  render() {
    const { delay1, delay2, delay3, showThanku } = this.state;
    const { open, handleModal } = this.props;
    return (
      <React.Fragment>
        <Modal
          open={open}
          onClose={handleModal}
          className="user-exp-modal-block"
          size="tiny">
          <Modal.Content className="modal-card-content">
            <div className="cross-button">
              <Icon name="close" className="cross-icon" onClick={handleModal} />
            </div>
            {!showThanku ? (
              <div>
                <div className="modal-card-popup">
                  <div className="user-icon">
                    <img className="img-responsive" src={VasiImage} />
                  </div>
                  {delay1 ? (
                    <div className="user-help-text-block">
                      <span className="user-help-text">
                        Help us to improve your experience, please let us know
                        what are you looking for?
                      </span>
                    </div>
                  ) : (
                    <div className="loading-dots">
                      <span>.</span>
                      <span>.</span>
                      <span>.</span>
                    </div>
                  )}
                </div>
                {delay2 ? (
                  <div className="modal-action-button">
                    <BorderButton
                      className="btn-border mb-8"
                      btnText="Job"
                      onClick={() => this.handleUserExpFeedback("job")}
                    />{" "}
                    <BorderButton
                      className="btn-border mb-8"
                      btnText="Candidates"
                      onClick={() => this.handleUserExpFeedback("candidates")}
                    />{" "}
                    <BorderButton
                      className="btn-border mb-8"
                      btnText="Both"
                      onClick={() => this.handleUserExpFeedback("both")}
                    />
                  </div>
                ) : delay1 ? (
                  <div className="loading-dots mt--25">
                    <span>.</span>
                    <span>.</span>
                    <span>.</span>
                  </div>
                ) : (
                  <div />
                )}
                {delay3 ? (
                  <div className="mt-20 mb-40 skip-button-block">
                    <a
                      href="javascript:;"
                      className="skip-button"
                      onClick={handleModal}>
                      Skip for now
                    </a>
                  </div>
                ) : (
                  <div />
                )}
              </div>
            ) : (
              <div className="text-center">
                <img src={ThanksIcon} />
                {/* <ThanksIcon /> */}
                <h3>Thank You!</h3>
              </div>
            )}
          </Modal.Content>
        </Modal>
      </React.Fragment>
    );
  }
}
