import React from "react";
import { Dropdown } from "semantic-ui-react";
import IcMoreOptionIcon from "../../../../../assets/svg/IcMoreOptionIcon";
import ShareModal from "../../../../ShareButton/ShareModal";

const MoreOptionsTrigger = (
  <span>
    <IcMoreOptionIcon pathcolor="#acaeb5" />
  </span>
);

class MoreOptionDropDown extends React.Component {
  state = { modalOpen: false };
  handleModal = () => this.setState({ modalOpen: !this.state.modalOpen });
  getItems = () => {
    return [
      {
        text: "Share",
        value: "Share",
        onClick: () => this.handleModal()
      }
    ];
  };

  render() {
    const { url, ...resProps } = this.props;
    return (
      <div className="Main_Dropdown" {...resProps}>
        <Dropdown
          trigger={MoreOptionsTrigger}
          options={this.getItems()}
          icon={null}
          pointing="right"
        />
        <ShareModal
          modalOpen={this.state.modalOpen}
          handleModal={this.handleModal}
          url={url}
        />
      </div>
    );
  }
}

export default MoreOptionDropDown;
