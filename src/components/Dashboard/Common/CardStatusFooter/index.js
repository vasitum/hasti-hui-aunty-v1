import React from "react";
import { Grid, Button, Modal } from "semantic-ui-react";
import getPlaceholder from "../../../Forms/FormFields/Placeholder";
import StatusMyJob from "../StatusMyJob";

import ActionBtn from "../../../Buttons/ActionBtn";
import IcDownArrow from "../../../../assets/svg/IcDownArrow";
import MoreOptionDropDown from "./MoreOption";

import "./index.scss";
import RepostButton from "../RepostButton";

class CardStatusFooter extends React.Component {
  state = {
    statusModal: false,
    statusChanged: false,
    newStatus: ""
  };

  onStatusModalOpen = e => {
    this.setState({ statusModal: true });
  };

  onStatusModalClose = e => {
    this.setState({ statusModal: false });
  };

  onStatusChange = status => {
    this.setState({
      statusChanged: true,
      newStatus: status,
      statusModal: false
    });
  };

  render() {
    const { status, jobId, shortListCount } = this.props;
    const { statusChanged, newStatus } = this.state;

    return (
      <div className="CardStatusFooter">
        <div className="CardStatusFooter_container">
          <div className="CardStatusFooter_left">
            <div className="footerTItle">
              <p>{getPlaceholder("Status", "Job status")}</p>
            </div>
            <div className="CardStatusFooter_Modal">
              <Modal
                open={this.state.statusModal}
                onClose={this.onStatusModalClose}
                className="CardStatusFooterModal_container"
                trigger={
                  <ActionBtn
                    onClick={
                      status !== "Close" ? this.onStatusModalOpen : () => {}
                    }
                    className="status_Btn "
                    btnIcon={<IcDownArrow pathcolor="#fff" height="6" />}
                    actioaBtnText={statusChanged ? newStatus : status}
                  />
                }
                closeIcon
                closeOnDimmerClick={false}>
                <StatusMyJob
                  onChange={this.onStatusChange}
                  onClose={this.onStatusModalClose}
                  status={statusChanged ? newStatus : status}
                  jobId={jobId}
                />
              </Modal>
            </div>
            {status === "Close" ? (
              <span>
                <RepostButton shortListCount={shortListCount} jobId={jobId} />
              </span>
            ) : null}
          </div>
          <div className="CardStatusFooter_right">
            <MoreOptionDropDown url={`/view/job/${jobId}`} />
          </div>
        </div>
      </div>
    );
  }
}

export default CardStatusFooter;
