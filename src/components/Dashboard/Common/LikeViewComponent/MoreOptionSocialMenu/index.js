// import React from "react";
// import { Link } from "react-router-dom";

// import {Button, Dropdown} from "semantic-ui-react";

// const MoreOptionSocialMenu = props => {
//   const { UserId } = props
//   return (
//     <div className="MoreOptionSocialMenu">

//       <Button className="has-hover-blue moreOptionBtn">
//         <Dropdown icon="ellipsis vertical" pointing="bottom right">
//           <Dropdown.Menu>
//             {UserId ? (
//               <Dropdown.Item as={Link} to={`/user/public/${UserId._id}`}>
//                 Public Profile
//              </Dropdown.Item>
//             ) : null }

//             <Dropdown.Item>
//               Share
//             </Dropdown.Item>
//             {/* <Dropdown.Item as={Link} to={`/cv-template`}>
//               Share
//             </Dropdown.Item> */}
//           </Dropdown.Menu>
//         </Dropdown>
//       </Button>
//     </div>
//   )
// }

// export default MoreOptionSocialMenu;

import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Button, Dropdown } from "semantic-ui-react";
import ShareModal from "../../../../ShareButton/ShareModal";

export default class MoreOptionSocialMenu extends Component {
  state = { modalOpen: false };
  handleModal = () => this.setState({ modalOpen: !this.state.modalOpen });
  render() {
    const { UserId } = this.props;
    return (
      <div className="MoreOptionSocialMenu">
        <Button className="has-hover-blue moreOptionBtn">
          <Dropdown icon="ellipsis vertical" pointing="bottom right">
            <Dropdown.Menu>
              {UserId ? (
                <Dropdown.Item as={Link} to={`/user/public/${UserId._id}`}>
                  Public Profile
                </Dropdown.Item>
              ) : null}

              <Dropdown.Item onClick={this.handleModal}>Share</Dropdown.Item>
              {/* <Dropdown.Item as={Link} to={`/cv-template`}>
              Share
            </Dropdown.Item> */}
            </Dropdown.Menu>
          </Dropdown>
        </Button>
        <ShareModal
          modalOpen={this.state.modalOpen}
          handleModal={this.handleModal}
          url={`/view/user/${UserId._id}`}
        />
      </div>
    );
  }
}
