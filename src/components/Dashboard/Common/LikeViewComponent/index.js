import React from "react";

import { Grid, List, Button, Dropdown, Responsive } from "semantic-ui-react";

import PropTypes from "prop-types";

import { Link } from "react-router-dom";

import ProfileView from "../../../../assets/svg/IcProfileView";

import LikeLogo from "../../../../assets/svg/IcLike";

import fromNow from "../../../../utils/env/fromNow";

import MoreOptionSocialMenu from "./MoreOptionSocialMenu";

import "./index.scss";

const LikeViewComponent = props => {
  const {
    user,
    data,
    LikeViewLeftWidth,
    LikeViewRightWidth,
    isShowMoreOption,
    isShowPostTime
  } = props;

  return (
    <div className="socialLink_profilpage">
      <Grid>
        <Grid.Row>
          <Grid.Column
            width={LikeViewLeftWidth}
            className="socialLink_viewColumn">
            <div className="socialLink_viewContainer">
              <div className="socialLink_viewInner">
                <p className="count">{!user.viewCount ? 0 : user.viewCount}</p>
                <p className="titleName">Views</p>
              </div>
              {/* <div className="socialLink_viewInner">
                <p className="count">{!user.likeCount ? 0 : user.likeCount}</p>
                <p className="titleName">Like</p>
              </div> */}
              <div className="socialLink_viewInner">
                <p className="count">
                  {!user.followerCount ? 0 : user.followerCount}
                </p>
                <p className="titleName">Followers</p>
              </div>
              <div className="socialLink_viewInner">
                <p className="count">
                  {!user.followingCount ? 0 : user.followingCount}
                </p>
                <p className="titleName">Following</p>
              </div>
            </div>

            {/* <List horizontal>
              <List.Item>
                <span className="padding-right-3">
                  <ProfileView width="18" height="11" pathcolor="#acaeb5" />
                </span>
                <span className="profile_social">
                  {isShowPostTime ? "" : "View: "}
                </span>
                <span className="count">
                  {!user.viewCount ? 0 : user.viewCount}
                </span>
              </List.Item>
              <List.Item>
                <span className="padding-right-3">
                  <LikeLogo width="17" height="12" pathcolor="#acaeb5" />
                </span>
                {isShowPostTime ? "" : "Like: "}
                <span className="count">
                  {!user.likeCount ? 0 : user.likeCount}
                </span>
              </List.Item>
              {isShowPostTime ? (
                <List.Item>
                  Posted:{" "}
                  <span className="count">
                    
                    {fromNow(user.cTime)}
                  </span>
                </List.Item>
              ) : null}
            </List> */}
          </Grid.Column>

          <Responsive minWidth={1025}>
            {!isShowMoreOption ? (
              <Grid.Column
                width={LikeViewRightWidth}
                textAlign="right"
                className="socialLink_Btn">
                <MoreOptionSocialMenu UserId={user} />
              </Grid.Column>
            ) : null}
          </Responsive>
        </Grid.Row>
      </Grid>
    </div>
  );
};

LikeViewComponent.propTypes = {
  // user: PropTypes.string,
  data: PropTypes.string,
  LikeViewLeftWidth: PropTypes.string,
  LikeViewRightWidth: PropTypes.string
};
LikeViewComponent.defaultProps = {
  user: {
    viewCount: "",
    likeCount: "",
    _id: ""
  },
  data: "",
  LikeViewLeftWidth: "15",
  LikeViewRightWidth: "1"
};

export default LikeViewComponent;
