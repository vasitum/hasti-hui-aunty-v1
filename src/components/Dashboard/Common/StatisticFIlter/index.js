import React from "react";
import { Grid, Button, List } from "semantic-ui-react";
import SortBy from "../SortBy";
import CandidateDateFilter from "../../RecruiterDashboard/Candidates/CandidateDateFilter";
import DropdownFilter from "../../Common/DropdownFilter";
import FilterTags from "../../Common/FilterTags";
import IcCloseIcon from "../../../../assets/svg/IcCloseIcon";
import "./index.scss";

const StatisticFIlter = () => (
  <div className="CandidateFilter">
    <div className="Filter_Main">
      <Grid>
        <Grid.Row>
          <Grid.Column
            className="Filter_text"
            width="12"
            style={{
              display: "flex",
              alignItems: "center"
            }}>
            <List horizontal className="List_Main">
              <List.Item className="Filter_Sort">
                <CandidateDateFilter
                  attribute={"datePosted"}
                  label={"Posted Date"}
                  past
                />
                <List.Content />
              </List.Item>
              <List.Item className="Filter_Sort">
                <DropdownFilter
                  defaultRefinement={""}
                  attribute={"locCity"}
                  label={"Job Location"}
                />
                <List.Content />
              </List.Item>
              <List.Item className="Filter_Sort">
                <DropdownFilter
                  defaultRefinement={""}
                  attribute={"comName"}
                  label={"Company Name"}
                />
                <List.Content />
              </List.Item>
              <List.Item className="Filter_Sort">
                <DropdownFilter
                  defaultRefinement={""}
                  attribute={"skills"}
                  label={"Skills"}
                />
                <List.Content />
              </List.Item>
              {/* <List.Item className="Filter_Button">
                <Button>Clear all (0)</Button>
                <List.Content />
              </List.Item> */}
            </List>
          </Grid.Column>

          <Grid.Column width="4" textAlign="right">
            <List className="List_Main">
              <List.Item className="Filter_Sort">
                <List.Content>
                  {/* <span>
                    Sort by:{" "}
                    <Dropdown
                      inline
                      options={SortBy}
                      defaultValue={SortBy[0].value}
                    />
                  </span> */}
                  <SortBy
                    defaultRefinement={"job_by_date"}
                    items={[
                      { text: "Apply Count", value: "job_by_apply" },
                      { text: "Recent", value: "job_by_date" }
                    ]}
                  />
                </List.Content>
              </List.Item>
            </List>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
    <div className="addTag_section">
      {/* <Button compact className="addTag_sectionBtn">
        Submitted: <span>Anytime</span>
        <IcCloseIcon pathcolor="#797979" width="10" height="10" />
      </Button> */}
      <FilterTags />
    </div>
  </div>
);

export default StatisticFIlter;
