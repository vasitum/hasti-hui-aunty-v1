import React, { Component } from 'react'

import PropTypes from "prop-types";

import "./index.scss";

class NoFoundMessageDashboard extends Component {
  render() {
    const {noFountTitle, noFountSubTitle} = this.props;
    return (
      <div className="NoFoundMessageDashboard">
        <h1>{noFountTitle}</h1>
        <p>{noFountSubTitle}</p>
      </div>
    )
  }
}

NoFoundMessageDashboard.propTypes = {
  noFountTitle: PropTypes.string,
  noFountSubTitle: PropTypes.string
}

NoFoundMessageDashboard.defaultProps = {
  noFountTitle: "noFountTitle",
  noFountSubTitle: "noFountSubTitle"
}

export default NoFoundMessageDashboard;
