import React, { Component } from "react";
import BorderButton from "../../../Buttons/BorderButton";
import "./index.scss";
import { Icon, Modal } from "semantic-ui-react";
import userFeedbackPopup from "../../../../api/user/userFeedbackPopup";
import fromNow from "../../../../utils/env/fromNow";
import IcUser from "../../../../assets/svg/IcUser";
import { Link } from "react-router-dom";
export default class UserProfileFeedbackPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      interviewData: null,
      seeAll: 2,
      totalCount: 0,
      Loader: false
    };
  }

  componentDidMount = async () => {
    try {
      const res = await userFeedbackPopup("Interview", 0, 10);
      if (res.status === 200) {
        const data = await res.json();
        this.setState({
          interviewData: data,
          totalCount: data.length,
          //totalCount: 3,
          Loader: true
        });
      }
    } catch (error) {
      console.error(error);
    }
  };

  showInterview() {
    const { seeAll, interviewData } = this.state;
    // console.log(interviewData);
    if (!interviewData) {
      return null;
    }
    return interviewData.slice(0, seeAll);
  }

  render() {
    const { open, handleModal } = this.props;
    const { totalCount, Loader } = this.state;
    return (
      <React.Fragment>
        <Modal
          open={open}
          onClose={handleModal}
          className="user-profile-feedback-modal-block"
          size="small">
          <Modal.Content className="modal-content">
            <div className="cross-button">
              <Icon name="close" className="cross-icon" onClick={handleModal} />
            </div>
            <div className="modal-title">
              Waiting for your feedback on these interviewed applicants
            </div>
            <div className="item-card-popup">
              {Loader ? (
                this.showInterview() ? (
                  this.showInterview().map((item, index) => {
                    return (
                      <div className="modal-card-popup" key={item._id}>
                        <div className="user-icon">
                          {!item.candidateImageExt ? (
                            <IcUser
                              rectcolor="#f7f7fb"
                              height="50"
                              width="50"
                              pathcolor="#c8c8c8"
                            />
                          ) : (
                            <img
                              className="img-responsive"
                              src={`https://s3-us-west-2.amazonaws.com/img-mwf/${
                                item.candidateId
                              }/image.jpg`}
                            />
                          )}
                        </div>
                        <div style={{ width: "100%" }}>
                          <div className="user-details-card">
                            <div>
                              <div className="user-name">
                                {item.candidateName}
                              </div>
                              <div className="user-location hidden-mobile">
                                {item.candidateProfileTitle}
                              </div>
                              <div className="mb-5">
                                <span className="user-applied-text">
                                  Applied :
                                </span>{" "}
                                <span className="user-designation">
                                  {item.jobName}
                                </span>
                              </div>
                            </div>
                            <div>
                              <div className="user-card-right">
                                <div>
                                  <span className="user-applied-text1">
                                    Submtitted :
                                  </span>{" "}
                                  <span className="user-status">
                                    {fromNow(item.appliedDate)}
                                  </span>
                                </div>
                                <div className="mt-15 hidden-mobile">
                                  <BorderButton
                                    className="btn btn-border"
                                    btnText="View detail"
                                    as={Link}
                                    to={`/job/${item.pId}/application/${
                                      item._id
                                    }`}
                                  />
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    );
                  })
                ) : (
                  <div />
                )
              ) : (
                <div className="loader-block">
                  <div className="loader" />
                </div>
              )}
            </div>
            {totalCount >= 3 ? (
              <div className="pt-15 text-center border-top">
                <BorderButton
                  className="btn"
                  btnText="See all"
                  icon="angle right"
                  as={Link}
                  to={`/job/recruiter/candidates?menu%5Bstatus%5D=Interview&page=1&vasitum_action=feedback_pending`}
                />
              </div>
            ) : (
              <div />
            )}
          </Modal.Content>
        </Modal>
      </React.Fragment>
    );
  }
}
