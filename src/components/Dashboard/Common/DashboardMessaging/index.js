import React from "react";
import ChatThreadMessagingContainer from "../../../Messaging/ChatThread/ChatThreadMessagingContainer";
import { Button, Modal } from "semantic-ui-react";

import DashbordSechuleInterView from "../DashbordSechuleInterView";

import ActionBtn from "../../../Buttons/ActionBtn";

import "./index.scss";

class DashboardMessaging extends React.Component {
  state = {
    scheduleModal: false,
    parsedTime: null
  };

  onScheduleModalOpen = (e, time) => {
    this.setState({ scheduleModal: true, parsedTime: time });
  };

  onScheduleModalClose = e => {
    this.setState({ scheduleModal: false });
  };

  onSchedule = e => {
    this.onScheduleModalClose();
  };

  render() {
    return (
      <div className="DashboardMessaging">
        <ChatThreadMessagingContainer
          {...this.props}
          noRouting={true}
          foundMessages={[]}
          // onChatIdFound={id => alert("Chat ID is", id)}
          onShowInterviewScreen={this.onScheduleModalOpen}
        />

        <div className="footerBtn">
          <Modal
            open={this.state.scheduleModal}
            onClose={this.onScheduleModalClose}
            className="CardStatusFooterModal_container"
            // trigger={<ActionBtn compact actioaBtnText="Res" className="" />}
            closeIcon
            closeOnDimmerClick={false}>
            <DashbordSechuleInterView
              parsedTime={this.state.parsedTime}
              onCancelClick={this.onScheduleModalClose}
              onInterviewScheduled={this.onSchedule}
              applicationData={this.props.applicationData}
              notConnected
            />
          </Modal>
          {/* <Modal
            className="CardStatusFooterModal_container"
            trigger={
              <Button compact className="NoInterviewBtn">
                Update{" "}
              </Button>
            }
            closeIcon
            closeOnDimmerClick={false}>
            <DashbordSechuleInterView />
          </Modal> */}
        </div>
      </div>
    );
  }
}

export default DashboardMessaging;
