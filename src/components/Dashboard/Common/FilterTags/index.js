import React from "react";
import { Button } from "semantic-ui-react";
import IcCloseIcon from "../../../../assets/svg/IcCloseIcon";
import { withCurrentFilters } from "../../../AlgoliaContainer";

const EXCLUDED = ["reqId", "userId", "candidateId"];

class FilterTags extends React.Component {
  render() {
    const { refine, items } = this.props;

    // console.log(this.props);
    return items.map(item => {
      if (EXCLUDED.indexOf(item.attribute) === -1) {
        return (
          <Button compact className="addTag_sectionBtn">
            {/* Submitted: <span>Anytime</span> */}
            {item.label}
            <IcCloseIcon
              onClick={e => refine(item.value)}
              pathcolor="#797979"
              width="10"
              height="10"
            />
          </Button>
        );
      } else {
        return null;
      }
    });
  }
}

export default withCurrentFilters(FilterTags);
