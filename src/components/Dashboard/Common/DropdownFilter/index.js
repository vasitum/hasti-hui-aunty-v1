import React from "react";
import { withSelect } from "../../../AlgoliaContainer";
import { Dropdown } from "semantic-ui-react";

class DropdownFilter extends React.Component {
  render() {
    const { items, currentRefinement, onSelect, label } = this.props;

    return (
      <div>
        {label}:{" "}
        <Dropdown
          inline
          placeholder={"All"}
          onChange={(e, { value }) => onSelect(value)}
          options={[].concat(
            [
              {
                text: "All",
                value: ""
              }
            ],
            items.map(item => {
              return {
                text: item.label,
                value: item.label
              };
            })
          )}
          defaultValue={currentRefinement}
        />
      </div>
    );
  }
}

export default withSelect(DropdownFilter);
