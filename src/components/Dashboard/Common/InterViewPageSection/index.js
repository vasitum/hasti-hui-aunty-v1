import React from "react";

import { Header, Button } from "semantic-ui-react";

import AccordionCardContainer from "../../../Cards/AccordionCardContainer";

import IcCalendarCard from "../../../../assets/svg/IcCalendarCard";

import InterViewPageSectionDedail from "./InterViewPageSectionDedail";

import IcRightCheck from "../../../../assets/svg/IcRightCheck";

import IcDownArrow from "../../../../assets/svg/IcDownArrow";

import getAllInterviews from "../../../../api/jobs/getAllInterviews";

import NoInterviews from "../NoInterviewScheduled";

import { format } from "date-fns";

import "./index.scss";

function getScheduled(params) {
  switch (params) {
    case "telephone":
      return "Telephonic";

    case "videoCall":
      return "Video Call";

    case "inPerson":
      return "In Person (F2F)";

    default:
      break;
  }
}

const PageSectionHeaderTitle = ({ data, candidate }) => {
  return (
    <div className="PageSectionHeaderTitle">
      <div className="HeaderTitle_left">
        <p className="headerTitle">
          <IcCalendarCard pathcolor="#acaeb5" height="15" />
          {!candidate ? data.candidateName : data.interviewerName}{" "}
          <span>|</span> {getScheduled(data.interviewMode)}
        </p>
        <p className="headerSubTitle">
          {/* Tue Jul 01, 2018, 11:00am – 11:30am IST */}
          {format(data.scheduledDate, "ddd MMM DD, YYYY")},{" "}
          {format(data.scheduledDate, "HH:mm a")} -{" "}
          {format(data.closedDate, "HH:mm a")}
        </p>
      </div>
      <div className="HeaderTitle_right">
        <Button className="interviewFinish">
          <IcRightCheck pathcolor="#48a24c" />
        </Button>
        <Button className="DownArrow">
          <IcDownArrow pathcolor="#cdcdcd" />
        </Button>
      </div>
    </div>
  );
};

class InterViewPageSection extends React.Component {
  state = {
    data: []
  };

  /**
   *
   */
  async componentDidMount() {
    const { jobId, applicationId } = this.props;
    try {
      const res = await getAllInterviews(jobId, applicationId);
      if (res.status === 200) {
        const data = await res.json();
        // console.log(data);
        this.setState({
          data: data
        });
      } else {
        console.error(res);
      }
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const { data } = this.state;
    const { candidate, applicationData } = this.props;

    return (
      <div className="InterViewPageSection">
        {data.map((val, idx) => {
          return (
            <AccordionCardContainer
              defaultActive={idx === 0}
              cardHeader={
                <PageSectionHeaderTitle candidate={candidate} data={val} />
              }>
              <InterViewPageSectionDedail
                applicationData={applicationData}
                data={val}
                candidate={candidate}
              />
            </AccordionCardContainer>
          );
        })}
        {data.length === 0 ? (
          <NoInterviews
            applicationData={applicationData}
            candidate={candidate}
          />
        ) : null}
      </div>
    );
  }
}

export default InterViewPageSection;
