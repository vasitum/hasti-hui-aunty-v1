import React from "react";

import { Button, Image, Modal } from "semantic-ui-react";

import SuggestLogo from "../../../../../../assets/img/people.png";

import ActionBtn from "../../../../../Buttons/ActionBtn";
import DashbordSechuleInterView from "../../../DashbordSechuleInterView";

import SechuleInterViewNew from "../../../SechuleInterViewNew";
import SechuleInterViewUpdate from "../../../SechuleInterViewUpdate";

import { format } from "date-fns";

// import IcDownArrow from "../../../../../../assets/svg/IcDownArrow";

import "./index.scss";

function getWhen(data) {
  if (!data) {
    return null;
  }

  return (
    <React.Fragment>
      <p>
        {format(
          data.suggestTime ? data.suggestTime.scheduledDate : 0,
          "ddd MMM DD, YYYY"
        )}
      </p>
      <p>
        {format(
          data.suggestTime ? data.suggestTime.scheduledDate : 0,
          "HH:mm a"
        )}{" "}
        - {format(data.closedDate, "HH:mm a")}
      </p>
    </React.Fragment>
  );
}

const InterviewSuggestedBox = props => {
  const {
    isSuggestedBox,
    isShowLogo,
    open,
    onOpen,
    onClose,
    onCancel,
    onDone,
    onAccept,
    interviewData,
    applicationData,
    notConnected,
    candidate
  } = props;

  return (
    <div className="Interview_suggestedBox">
      <div className="suggestedBox_header">
        {/* {!isShowLogo ? <Image src={SuggestLogo} /> : null} */}
        <p>New suggested time slot</p>
      </div>
      <div className="suggestedBox_Body">
        <div className="suggestedBox_BodyLeft">When</div>
        <div className="suggestedBox_BodyRight">{getWhen(interviewData)}</div>
      </div>
      <div className="suggestedBox_Footer">
        {!isSuggestedBox ? (
          candidate ? null : (
            <div>
              <ActionBtn
                onClick={e => onAccept(interviewData)}
                actioaBtnText="Accept"
              />
              <Modal
                open={open}
                onClose={onClose}
                className="CardStatusFooterModal_container"
                trigger={
                  <Button onClick={onOpen} className="alternativeBtn" compact>
                    Propose alternative
                  </Button>
                }
                closeIcon
                closeOnDimmerClick={false}>
                <DashbordSechuleInterView
                  onCancelClick={onCancel}
                  onInterviewScheduled={onDone}
                  applicationData={applicationData}
                  interviewData={interviewData}
                  candidate={candidate}
                  notConnected={notConnected}
                />
              </Modal>
            </div>
          )
        ) : (
          <div>
            <Modal
              className="CardStatusFooterModal_container"
              trigger={
                <Button className="alternativeBtn" compact>
                  Update
                </Button>
              }
              closeIcon
              closeOnDimmerClick={false}>
              <SechuleInterViewUpdate />
            </Modal>

            <Modal
              className="CardStatusFooterModal_container"
              trigger={
                <Button className="alternativeBtn" compact>
                  Create New
                </Button>
              }
              closeIcon
              closeOnDimmerClick={false}>
              <SechuleInterViewNew />
            </Modal>
          </div>
        )}
      </div>
    </div>
  );
};

export default InterviewSuggestedBox;
