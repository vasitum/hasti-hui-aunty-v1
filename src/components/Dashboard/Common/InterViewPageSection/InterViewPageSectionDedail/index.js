import React from "react";

import { Button, Modal, Image } from "semantic-ui-react";

import IcDownArrow from "../../../../../assets/svg/IcDownArrow";

import DashbordSechuleInterView from "../../DashbordSechuleInterView";
import InterviewSuggestedBox from "./InterviewSuggestedBox";

import { format } from "date-fns";

import "./index.scss";
import { USER } from "../../../../../constants/api";

import updateInterview from "../../../../../api/jobs/updateInterview";

const userData = window.localStorage.getItem(USER.UID);

function isRecruiter(data) {
  if (!data) {
    return false;
  }

  return data.recruiterId === userData;
}

function getWhereText(data) {
  // console.log(data);

  if (isRecruiter(data)) {
    // return (
    //   <span>
    //     You to call {data.candidateName} at{" "}
    //     {format(data.scheduledDate, "ddd MMM DD, YYYY")}
    //   </span>
    // );
    switch (data.interviewMode) {
      case "telephone":
        return (
          <span>
            You to call {data.candidateName} on {data.candidateNumber}
          </span>
        );

      case "videoCall":
        return (
          <span>
            You to Video call {data.candidateName} on {data.interviewSubMode} at{" "}
            {data.interviewContactInfo}
          </span>
        );
        break;

      case "inPerson":
        return (
          <span>
            You to meet {data.candidateName} at {data.interviewSubMode}
          </span>
        );
        break;

      default:
        break;
    }
  } else {
    // return (
    //   <span>
    //     You will get a call from {data.interviewerName} at{" "}
    //     {format(data.scheduledDate, "ddd MMM DD, YYYY")}
    //   </span>
    // );

    switch (data.interviewMode) {
      case "telephone":
        return (
          <span>
            You will get a call from {data.interviewerName} on{" "}
            {data.candidateNumber}
          </span>
        );

      case "videoCall":
        return (
          <span>
            You to join Video call {data.interviewerName} on{" "}
            {data.interviewSubMode} at {data.interviewContactInfo}
          </span>
        );
        break;

      case "inPerson":
        return (
          <span>
            You to meet {data.interviewerName} at {data.interviewSubMode}
          </span>
        );
        break;

      default:
        break;
    }
  }
}

function getWhoText(data) {
  if (isRecruiter(data)) {
    return (
      <React.Fragment>
        <p>
          Host <span>You</span>
        </p>
        <p>
          Guest <span>{data.candidateName}</span>
        </p>
      </React.Fragment>
    );
  } else {
    return (
      <React.Fragment>
        <p>
          Host <span>{data.interviewerName}</span>
        </p>
        <p>
          Guest <span>You</span>
        </p>
      </React.Fragment>
    );
  }
}

function getStatus(data) {
  const currentTime = +new Date();

  if (data.suggestTime) {
    return <span>Time Proposed</span>;
  }

  if (data.scheduledDate < currentTime) {
    return <span>End</span>;
  } else if (data.scheduledDate > currentTime) {
    return <span>Upcoming</span>;
  }
}

class InterViewPageSectionDedail extends React.Component {
  state = {
    scheduleModal: false
  };

  onScheduleModalOpen = e => {
    this.setState({ scheduleModal: true });
  };

  onScheduleModalClose = e => {
    this.setState({ scheduleModal: false });
  };

  onSchedule = e => {
    this.onScheduleModalClose();
  };

  async onAccept(interviewData) {
    try {
      const res = await updateInterview(
        {
          ...interviewData,
          scheduledDate: interviewData.suggestTime.scheduledDate,
          suggestTime: null
        },
        interviewData.jobId,
        interviewData.jobApplicationId,
        interviewData._id
      );

      if (res.status === 200) {
        window.location.reload();
        return;
      } else {
        console.error("Unable to accept interview intv");
      }
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const { data, applicationData, candidate } = this.props;

    return (
      <div className="InterViewPageSectionDedail">
        <div className="Dedail_innerSection">
          <div className="InterViewPageSectionDedail_container">
            <div className="DedailInner">
              <p className="DedailInner_title">When</p>
              <div className="DedailInner_subTitle">
                <p>{format(data.scheduledDate, "ddd MMM DD, YYYY")}</p>
                <p>
                  {format(data.scheduledDate, "HH:mm a")} -{" "}
                  {format(data.closedDate, "HH:mm a")}
                </p>
              </div>
            </div>
            <div className="DedailInner">
              <p className="DedailInner_title">Where</p>
              <div className="DedailInner_subTitle">
                <p>{getWhereText(data)}</p>
              </div>
            </div>
            <div className="DedailInner">
              <p className="DedailInner_title">Who</p>
              <div className="DedailInner_subTitle">{getWhoText(data)}</div>
            </div>
            <div className="DedailInner">
              <p className="DedailInner_title">Status</p>
              <div className="DedailInner_subTitle">
                <p> {getStatus(data)}</p>
              </div>
            </div>
          </div>
          {data.suggestTime ? (
            <InterviewSuggestedBox
              open={this.state.scheduleModal}
              onOpen={this.onScheduleModalOpen}
              onClose={this.onScheduleModalClose}
              onCancel={this.onScheduleModalClose}
              onDone={this.onSchedule}
              onAccept={this.onAccept}
              interviewData={data}
              applicationData={applicationData}
              notConnected
              candidate={candidate}
            />
          ) : null}
        </div>
        <div className="PageSectionDedail_footer">
          {!candidate ? (
            <p className="footer_title">
              <span>Notified: </span>
              <span>
                {data.notifyEmail
                  ? data.notifyEmail.map(val => {
                      return val;
                    })
                  : `No one`}
              </span>
            </p>
          ) : null}
          {!data.suggestTime ? (
            <div className="interviewBtn">
              <Modal
                open={this.state.scheduleModal}
                onClose={this.onScheduleModalClose}
                className="CardStatusFooterModal_container"
                trigger={
                  <Button onClick={this.onScheduleModalOpen} compact>
                    Reschedule interview
                    <IcDownArrow pathcolor="#0b9ed0" height="6" />
                  </Button>
                }
                closeIcon
                closeOnDimmerClick={false}>
                <DashbordSechuleInterView
                  onCancelClick={this.onScheduleModalClose}
                  onInterviewScheduled={this.onSchedule}
                  applicationData={applicationData}
                  interviewData={data}
                  candidate={candidate}
                  notConnected
                />
              </Modal>
            </div>
          ) : null}
        </div>
      </div>
    );
  }
}

export default InterViewPageSectionDedail;
