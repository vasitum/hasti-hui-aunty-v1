import React from "react";
import "./index.scss";
import { Dropdown,Button } from "semantic-ui-react";

const BulkOption = [
    {
      text: "Bulk actions",
      value: "Bulk actions"
    },
    {
      text: "Other actions",
      value: "Other actions"
    }
  ];

const Multiplefooter = props => {
    return (
        <div className="Multiplefooter_main">
         <Dropdown
            placeholder="Basic screening"
            selection
            options={BulkOption}
          />
          <Button>Deselect all</Button>
      </div>
    )}


    export default Multiplefooter;