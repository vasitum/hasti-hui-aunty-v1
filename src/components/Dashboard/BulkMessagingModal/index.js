import { Form } from "formsy-semantic-ui-react";
import React from "react";
import ActionBtn from "../../Buttons/ActionBtn";
import FlatDefaultBtn from "../../Buttons/FlatDefaultBtn";
import ModalBanner from "../../ModalBanner";
import IsShowNotifyAplication from "../RejectModal/IsShowNotifyAplication";
import sendBulkMessage from "../../../api/jobs/sendBulkMessage";
import PageLoader from "../../PageLoader";
import "./index.scss";

class BulkMessagingModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: null,
      loaderText: "Send message ...",
      loaderActive: false
    };

    // autobind
    this.onSubmit = this.onSubmit.bind(this);
  }

  onLoaderActive = () => {
    this.setState({
      loaderActive: true
    });
  };

  onLoaderDisable = () => {
    this.setState({
      loaderActive: false
    });
  };

  onQuillChange = html => {
    this.setState({
      text: html
    });
  };

  async onSubmit(e) {
    const { onChange, selectedItems } = this.props;
    try {
      this.onLoaderActive();
      const res = await sendBulkMessage(selectedItems, this.state.text);

      if (res.status === 200) {
        if (onChange) {
          onChange(this.state.currentVal);
        }
      }
      this.onLoaderDisable();
    } catch (error) {
      console.log(error);
      this.onLoaderDisable();
    }
  }

  render() {
    return (
      <div className="RejectModal">
        <ModalBanner headerText="Send Message" />
        <div className="userInterView_staus">
          <Form onSubmit={this.onSubmit}>
            <div className="userInterView_stausInner">
              <IsShowNotifyAplication
                label={"Use Template to message the Applicant."}
                onChange={this.onQuillChange}
                value={this.state.text}
              />
            </div>

            <div className="RejectModal_footer">
              <FlatDefaultBtn
                type="button"
                onClick={this.props.onClose}
                classNames="bgTranceparent"
                btntext="Cancel"
              />
              <ActionBtn
                disabled={!this.state.text}
                actioaBtnText={"Send Message"}
              />
            </div>
          </Form>
        </div>
        <PageLoader
          loaderText={this.state.loaderText}
          active={this.state.loaderActive}
        />
      </div>
    );
  }
}

export default BulkMessagingModal;
