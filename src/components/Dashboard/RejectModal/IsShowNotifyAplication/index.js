import React from "react";

import { Grid, Modal, Header } from "semantic-ui-react";
import { Form } from "formsy-semantic-ui-react";

import FlatDefaultBtn from "../../../Buttons/FlatDefaultBtn";
import QuillPlaceholder from "../../../CardElements/QuillPlaceholderExtra";
import SelectOptionField from "../../../Forms/FormFields/SelectOptionField";
import NewTemplates from "../../../Messaging/Templates/NewTemplate";
import ICPlus from "../../../../assets/svg/IcPlus";
import getAllUserTemplate from "../../../../api/messaging/getAllUserTemplate";

import { Link } from "react-router-dom";
import { USER } from "../../../../constants/api";

import "./index.scss";

const getAllTemp = [
  {
    text: "Reject Profile",
    value: "Reject Profile"
  },
  {
    text: "Reject Profile 1",
    value: "Reject Profile 1"
  }
];
const saveTamplateTrigger = (
  <FlatDefaultBtn classNames="bgTranceparent" btntext="Save template" />
);

class IsShowNotifyAplication extends React.Component {
  constructor(props) {
    super(props);
    this.templates = [];
  }

  state = {
    templateJson: [],
    currentTemplate: "",
    displayText: "",
    attachment: null
  };

  async componentDidMount() {
    const uId = window.localStorage.getItem(USER.UID);
    try {
      const res = await getAllUserTemplate(uId, "applicationTemp");
      if (res.status === 404) {
        // console.log("No, Templates Found");
        return;
      }

      const dataJson = await res.json();

      // set the data
      const filteredData = Array.from(dataJson).filter(val => {
        if (val.tempName) {
          return true;
        }

        return false;
      });

      this.templates = filteredData;

      this.setState({
        templateJson: filteredData.map(val => {
          return {
            text: val.tempName,
            value: val._id,
            key: val._id
          };
        })
      });
    } catch (error) {
      console.error(error);
    }
  }

  onSelectChange = (ev, { value }) => {
    // No templates found
    if (!this.templates) {
      return;
    }

    const filteredTemplate = this.templates.filter(val => val._id === value);

    if (!filteredTemplate || filteredTemplate.length === 0) {
      return;
    }
    const txt = filteredTemplate[0].text || "";
    const userMedia = filteredTemplate[0].userMedia || null;

    if (!userMedia) {
      this.setState({
        currentTemplate: value,
        displayText: txt
      });

      return;
    }

    this.setState({
      currentTemplate: value,
      displayText: txt,
      attachment: {
        name: userMedia.fileName,
        size: userMedia.size,
        ext: userMedia.ext,
        type: userMedia.mediaType
      }
    });
  };

  onQuillChange = html => {
    this.setState(
      {
        displayText: html
      },
      () => {
        if (this.props.onChange) {
          this.props.onChange(html);
        }
      }
    );
  };

  render() {
    const { props } = this;

    const { ...resProps } = props;
    return (
      <div className="IsShowNotifyAplication" {...resProps}>
        <Header>Use Template to notify the Applicant.</Header>
        <div className="">
          <Grid>
            <Grid.Row className="selectOption">
              <Grid.Column width="12" className="selectOption_left">
                <Form>
                  <SelectOptionField
                    placeholder="Please Select the template"
                    value={""}
                    optionsItem={this.state.templateJson}
                    name="options"
                    onChange={this.onSelectChange}
                  />
                </Form>
              </Grid.Column>
              <Grid.Column width="4" className="selectOption_right">
                <FlatDefaultBtn
                  as={Link}
                  to={"/job/managetemplate"}
                  className="bgTranceparent"
                  btntext="Create new"
                  btnicon={<ICPlus pathcolor="#c8c8c8" height="10" />}
                />
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>
        <div>
          <QuillPlaceholder
            isSmall
            isDoubleDown
            // readOnly
            // defaultValue=
            onChange={this.onQuillChange}
            value={this.state.displayText}
            placeholder="Your selected template will show here"
          />
        </div>
      </div>
    );
  }
}

export default IsShowNotifyAplication;
