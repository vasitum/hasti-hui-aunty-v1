import React from "react";

import { TextArea } from "semantic-ui-react";
import { Form } from "formsy-semantic-ui-react";

import NotifyContainer from "../NotifyContainer";
import InfoIconLabel from "../../../utils/InfoIconLabel";
import CheboxButton from "../../../Forms/FormFields/CheboxButton";
// import QuillPlaceholder from "../../../CardElements/QuillPlaceholder";

import "./index.scss";

class RejectAplication extends React.Component {
  render() {
    const { reason, text, onReasonChange, onTextChange } = this.props;
    
    

    return (
      <div className="RejectAplication">
        <div className="">
          <Form>
            <div className="">
              <div>
                <NotifyContainer
                  className="notify_Label"
                  label="Choose reason (Internally use only)"
                  infoicon={<InfoIconLabel text="Check" />}
                />
              </div>
              <div className="RejectAplication_btnGroup">
                <div className="btnWidth">
                  <CheboxButton
                    value={reason}
                    onChange={onReasonChange}
                    boxVal={"Unresponsive"}
                    name={"__karan"}
                    checkboxTitle="Unresponsive"
                  />
                </div>
                <div className="btnWidth">
                  <CheboxButton
                    name={"__karan"}
                    value={reason}
                    onChange={onReasonChange}
                    boxVal={"Don’t meet qualifications"}
                    checkboxTitle="Don’t meet qualifications"
                  />
                </div>
                <div className="btnWidth">
                  <CheboxButton
                    value={reason}
                    onChange={onReasonChange}
                    boxVal={"Failed interview"}
                    name={"__karan"}
                    checkboxTitle="Failed interview"
                  />
                </div>
              </div>
              <div className="RejectAplication_btnGroup">
                <div className="btnWidth">
                  <CheboxButton
                    value={reason}
                    onChange={onReasonChange}
                    boxVal={"Accepted another offer"}
                    name={"__karan"}
                    checkboxTitle="Accepted another offer"
                  />
                </div>
                <div className="btnWidth">
                  <CheboxButton
                    value={reason}
                    onChange={onReasonChange}
                    boxVal={"Bad timings"}
                    name={"__karan"}
                    checkboxTitle="Bad timings"
                  />
                </div>
                <div className="btnWidth">
                  <CheboxButton
                    value={reason}
                    onChange={onReasonChange}
                    boxVal={"Something else"}
                    name={"__karan"}
                    checkboxTitle="Something else"
                  />
                </div>
              </div>
              <div className="">
                <TextArea
                  onChange={onTextChange}
                  value={text}
                  placeholder="Describe here reason for this disqualification, this will not notify/email the candidate."
                />
              </div>
            </div>
          </Form>
        </div>
      </div>
    );
  }
}

export default RejectAplication;
