import React from "react";

import CheckboxField from "../../../Forms/FormFields/CheckboxField";
import "./index.scss";

const NotifyAplicationLabel = props => {
  const { ...resProps } = props;

  return (
    <div className="NotifyAplicationLabel">
      <CheckboxField checkboxLabel="Notify applicant" {...resProps} />
    </div>
  );
};

export default NotifyAplicationLabel;
