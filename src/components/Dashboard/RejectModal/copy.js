import React from "react";

import { Grid, Dropdown, Modal } from "semantic-ui-react";
import { Form } from "formsy-semantic-ui-react";

import InfoSection from "../../Sections/InfoSection";
import InputContainer from "../../Forms/InputContainer";
import InfoIconLabel from "../../utils/InfoIconLabel";
import CheboxButton from "../../Forms/FormFields/CheboxButton";
import NotifyAplicationLabel from "./NotifyAplicationLabel";
import ActionBtn from "../../Buttons/ActionBtn";
import FlatDefaultBtn from "../../Buttons/FlatDefaultBtn";
import NotifyContainer from "../RecruiterDashboard/Candidates/NotifyContainer";
import InputField from "../../Forms/FormFields/InputField";

import ModalBanner from "../../ModalBanner";

import "./index.scss";

const RejectModalOption = [
  {
    text: "Reject Profile",
    value: "Reject Profile"
  },
  {
    text: "Reject Profile 1",
    value: "Reject Profile 1"
  }
];

const saveTamplateTrigger = (
  <FlatDefaultBtn classNames="bgTranceparent" btntext="Save template" />
);

const saveTamplate = [
  {
    text: "New",
    value: "new"
  },

  {
    text: (
      <Modal
        className="addTagModal_container"
        trigger={<span>Existing</span>}
        size="small"
        closeIcon
        closeOnDimmerClick={false}>
        Existing template
      </Modal>
    ),
    value: "Existing"
  }
];

const RejectModal = props => {
  return (
    <div className="RejectModal">
      <ModalBanner headerText="Change status" />
      <Form>
        <InfoSection
          headerText="Reason for disqualification"
          // rightHeader="hello"
          headerSize="medium"
          color="blue">
          <div>
            <InputContainer
              label="Choose reason (Internally use only)"
              infoicon={<InfoIconLabel />}>
              <Grid>
                <Grid.Row>
                  <Grid.Column computer={8}>
                    <CheboxButton checkboxTitle="Unresponsive" />
                  </Grid.Column>
                  <Grid.Column computer={8}>
                    <CheboxButton checkboxTitle="Don’t meet qualifications" />
                  </Grid.Column>
                </Grid.Row>

                <Grid.Row>
                  <Grid.Column computer={8}>
                    <CheboxButton checkboxTitle="Failed interview" />
                  </Grid.Column>
                  <Grid.Column computer={8}>
                    <CheboxButton checkboxTitle="Accepted another offer" />
                  </Grid.Column>
                </Grid.Row>

                <Grid.Row className="someThink_checkbox">
                  <Grid.Column computer={8}>
                    <CheboxButton checkboxTitle="Bad timings" />
                  </Grid.Column>
                  <Grid.Column computer={8}>
                    <CheboxButton checkboxTitle="Something else" />
                  </Grid.Column>
                </Grid.Row>

                <Grid.Row className="someThink_checkboxInput">
                  <Grid.Column computer={16}>
                    <Form>
                      <div>
                        <InputField
                          placeholder="Describe here reason for this disqualification, this will not notify/email the candidate."
                          name="condition"
                        />
                      </div>
                    </Form>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </InputContainer>
          </div>

          <div>
            <InputContainer
              className="notify_Label"
              label={<NotifyAplicationLabel />}
              infoicon={<InfoIconLabel />}
            />
          </div>

          <NotifyContainer />
        </InfoSection>

        <div className="RejectModal_footer">
          <FlatDefaultBtn classNames="bgTranceparent" btntext="Cancel" />
          <Dropdown
            trigger={saveTamplateTrigger}
            options={saveTamplate}
            icon={null}
            pointing="right bottom"
          />

          <ActionBtn actioaBtnText="Send" />
        </div>
      </Form>
    </div>
  );
};

export default RejectModal;
