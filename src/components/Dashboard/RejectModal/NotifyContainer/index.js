import React from "react";
import PropTypes from "prop-types";

import { Grid, Header } from "semantic-ui-react";
import InputFieldLabel from "../../../Forms/FormFields/InputFieldLabel";

import "./index.scss";

const NotifyContainer = props => {
  const { label, children, infoicon, className, ...restProps } = props;
  return (
    <div className={`NotifyContainer ${className}`} {...restProps}>
      <Grid>
        <Grid.Column width={16} textAlign="right">
          <InputFieldLabel label={label} infoicon={infoicon} />
        </Grid.Column>

        {/* <Grid.Column mobile={6} computer={12}>
          {children}
        </Grid.Column> */}
      </Grid>
    </div>
  );
};

NotifyContainer.propTypes = {
  label: PropTypes.string
};

export default NotifyContainer;