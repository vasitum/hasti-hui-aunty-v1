import { Form } from "formsy-semantic-ui-react";
import React from "react";
import { Dropdown, Modal } from "semantic-ui-react";
import ActionBtn from "../../Buttons/ActionBtn";
import FlatDefaultBtn from "../../Buttons/FlatDefaultBtn";
import ModalBanner from "../../ModalBanner";
import InfoIconLabel from "../../utils/InfoIconLabel";
import "./index.scss";
import IsShowNotifyAplication from "./IsShowNotifyAplication";
import NotifyAplicationLabel from "./NotifyAplicationLabel";
import NotifyContainer from "./NotifyContainer";
import RejectAplication from "./RejectAplication";
import changeApplicationStatus from "../../../api/jobs/changeApplicationStatus";

import ReactGA from "react-ga";
import { USER } from "../../../constants/api";
import { RejectModalString } from "../../EventStrings/RejectModalString";

import { RECR_STAGES } from "../../../constants/applicationStages";

const RejectModalOption = Object.keys(RECR_STAGES)
  .map(val => {
    return {
      text: RECR_STAGES[val],
      value: val
    };
  })
  .filter(val => val.value !== "Yettoscreen");

const saveTamplateTrigger = (
  <FlatDefaultBtn classNames="bgTranceparent" btntext="Save template" />
);

const saveTamplate = [
  {
    text: "New",
    value: "new"
  },

  {
    text: (
      <Modal
        className="addTagModal_container"
        trigger={<span>Existing</span>}
        size="small"
        closeIcon
        closeOnDimmerClick={false}>
        Existing template
      </Modal>
    ),
    value: "Existing"
  }
];

class RejectModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      IsShowNotify: false,
      isReject: false,
      currentVal: "NewApplicant",
      rejectReason: null,
      rejectText: "",
      text: null
    };

    // autobind
    this.onSubmit = this.onSubmit.bind(this);
  }

  onReasonChange = val => {
    this.setState({
      rejectReason: val
    });
  };

  onTextChange = (e, { value }) => {
    this.setState({
      rejectText: value
    });
  };

  onQuillChange = html => {
    this.setState({
      text: html
    });
  };

  IsShowNotifyBox = () => {
    this.setState({
      IsShowNotify: !this.state.IsShowNotify
    });
  };

  componentDidMount() {
    const { status } = this.props;
    if (status !== this.state.currentVal) {
      this.setState({
        currentVal: status
      });
    }
  }

  onChange = (e, { value }) => {
    if (value === "Rejected") {
      this.setState({
        currentVal: value,
        isReject: true
      });
    } else {
      this.setState({
        currentVal: value,
        isReject: false
      });
    }
  };

  async onSubmit(e) {
    const { jobId, applicationId, onChange, selectedItems } = this.props;
    try {
      const res = await changeApplicationStatus(
        selectedItems
          ? selectedItems
          : [
              {
                applicationId: applicationId,
                jobId: jobId
              }
            ],
        this.state.currentVal,
        this.state.rejectReason,
        this.state.text,
        this.state.rejectText
      );

      if (res.status === 200) {
        // console.log("Status Changed");

        if (onChange) {
          onChange(this.state.currentVal);
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    const UserId = window.localStorage.getItem(USER.UID);
    return (
      <div className="RejectModal">
        <ModalBanner headerText="Change status" />
        <div className="userInterView_staus">
          <Form onSubmit={this.onSubmit}>
            <div className="userInterView_stausInner">
              <div>
                <Dropdown
                  placeholder="Select template"
                  selection
                  fluid
                  options={RejectModalOption}
                  value={this.state.currentVal}
                  onChange={this.onChange}
                />
              </div>
              <div>
                {this.state.isReject ? (
                  <RejectAplication
                    reason={this.state.rejectReason}
                    text={this.state.rejectText}
                    onReasonChange={this.onReasonChange}
                    onTextChange={this.onTextChange}
                  />
                ) : null}
              </div>
              <div onClick={this.IsShowNotifyBox}>
                <NotifyContainer
                  className="notify_Label"
                  label={
                    <NotifyAplicationLabel checked={this.state.IsShowNotify} />
                  }
                  infoicon={<InfoIconLabel text="Check" />}
                />
              </div>
              {this.state.IsShowNotify && (
                <IsShowNotifyAplication
                  onChange={this.onQuillChange}
                  value={this.state.text}
                />
              )}
            </div>

            <div className="RejectModal_footer">
              <FlatDefaultBtn
                type="button"
                onClick={this.props.onClose}
                classNames="bgTranceparent"
                btntext="Cancel"
              />
              <ActionBtn
                disabled={
                  this.state.currentVal === this.props.status ||
                  !this.state.currentVal
                }
                onClick={() => {
                  ReactGA.event({
                    category: RejectModalString.Change_SC_RD.category,
                    action: RejectModalString.Change_SC_RD.action,
                    value: UserId,
                  });
                }}
                actioaBtnText={
                  this.state.IsShowNotify ? "Change & Notify" : "Change Status"
                }
              />
            </div>
          </Form>
        </div>
      </div>
    );
  }
}

export default RejectModal;
