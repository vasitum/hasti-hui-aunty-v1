import React from "react";

import { Tab, Menu, Responsive } from "semantic-ui-react";
import Candidates from "./Candidates";

import IcMyJobs from "../../../assets/svg/IcMyJobs";
import IcMyCandidates from "../../../assets/svg/IcMyCandidates";
import RecruiterCandidateMobile from "./RecruiterCandidateMobile";
import ListOfInterviews from "../RecruiterDashboard/ListOfInterviews";

import LikeJobs from "./LikeJobs";
import RecommendedJobs from "./RecommendedJobs";
import SaveJob from "./SaveJob";
import LikeJobsCondidateMobile from "../RecruiterDashboard/RecruiterDashbordMobile/LikeJobsCondidateMobile";
import SavedProfileMobile from "../RecruiterDashboard/RecruiterDashbordMobile/SaveJobCandidateMobile";
import IcSaveAsDraft from "../../../assets/svg/IcSave";
import IcLikedProfiles from "../../../assets/svg/IcLikedProfiles";
import IcRecommendedApplicants from "../../../assets/svg/IcRecommendedApplicants";
import IcMyCalendar from "../../../assets/svg/IcMyCalendar";
import Multiplefooter from "../MultipleFooter";
import MultipleChekPopup from "./Candidates/MultipleChekPopup/";
import IcDashboard from "../../../assets/svg/IcDashboard";
import IcjobIcon from "../../../assets/svg/Icjob";

import MyJobs from "./MyJobs";
import MultiCheckPopup from "./Candidates/MultipleChekPopup";
import RecruiterMyjobMobile from "../RecruiterDashboard/RecruiterDashbordMobile/RecruiterMyjobMobile";
import ListOfinterviewContainerMobile from "../RecruiterDashboard/ListOfInterviews/ListOfInterviewContainerMobile";
import IcListofInterviews from "../../../assets/svg/IcListofInterviews";
import IcCompanyNocircle from "../../../assets/svg/IcCompanyNocircle";
import ManageComapnies from "../../ManageComapnies";
import ManageComapniesMobile from "../../ManageComapnies/ManageComapniesMobile";
import getActiveTab from "../../../utils/ui/getActiveTab";
import { Link } from "react-router-dom";

import "./index.scss";

const panes = [
  {
    menuItem: (
      <Menu.Item
        as={Link}
        to="/job/recruiter/myjobs"
        className="RecruiterTabMenu"
        key="myJobs">
        <p>
          <IcMyJobs pathcolor="#acaeb5" />
        </p>
        Posted jobs
      </Menu.Item>
    ),
    render: () => (
      <Tab.Pane>
        <MyJobs />
      </Tab.Pane>
    )
  },

  {
    menuItem: (
      <Menu.Item
        as={Link}
        to="/job/recruiter/candidates"
        className="RecruiterTabMenu"
        key="candidates">
        <p>
          <IcMyCandidates pathcolor="#acaeb5" />
        </p>
        Candidates
      </Menu.Item>
    ),
    render: () => (
      <Tab.Pane>
        <Candidates />
      </Tab.Pane>
    )
  },

  // {
  //   menuItem: (
  //     <Menu.Item
  //       as={Link}
  //       to="/job/recruiter/liked"
  //       className="RecruiterTabMenu"
  //       key="likedProfiles">
  //       <p>
  //         <IcLikedProfiles pathcolor="#acaeb5" />
  //       </p>
  //       Liked Profiles
  //     </Menu.Item>
  //   ),
  //   render: () => (
  //     <Tab.Pane>
  //       <LikeJobs />
  //     </Tab.Pane>
  //   )
  // },

  // {
  //   menuItem: (
  //     <Menu.Item
  //       as={Link}
  //       to="/job/recruiter/saved"
  //       className="RecruiterTabMenu"
  //       key="saved_profiles">
  //       <p className="">
  //         <IcSaveAsDraft pathcolor="#acaeb5 " />
  //       </p>
  //       Saved Profiles
  //     </Menu.Item>
  //   ),
  //   render: () => {
  //     return (
  //       <Tab.Pane>
  //         <SaveJob />
  //       </Tab.Pane>
  //     );
  //   }
  // },

  {
    menuItem: (
      <Menu.Item
        as={Link}
        to="/job/recruiter/compaines"
        className="RecruiterTabMenu"
        key="companies">
        <p className="">
          <IcCompanyNocircle pathcolor="#acaeb5 " />
        </p>
        Companies
      </Menu.Item>
    ),
    render: () => {
      return (
        <Tab.Pane>
          <ManageComapnies />
        </Tab.Pane>
      );
    }
  },

  {
    menuItem: (
      <Menu.Item
        as={Link}
        to="/job/recruiter/interview"
        className="RecruiterTabMenu"
        key="interview-list">
        <p className="">
          <IcListofInterviews pathcolor="#acaeb5 " />
        </p>
        Interview List
      </Menu.Item>
    ),
    render: () => {
      return (
        <Tab.Pane>
          <ListOfInterviews />
        </Tab.Pane>
      );
    }
  }

  // {
  //   menuItem: (
  //     <Menu.Item
  //       as={Link}
  //       to="/job/recruiter/recommended"
  //       className="RecruiterTabMenu"
  //       key="recommendedProfiles">
  //       <p>
  //         <IcRecommendedApplicants pathcolor="#acaeb5" />
  //       </p>
  //       Recommended Profiles
  //     </Menu.Item>
  //   ),
  //   render: () => (
  //     <Tab.Pane>
  //       <RecommendedJobs />
  //     </Tab.Pane>
  //   )
  // },

  // {
  //   menuItem: (
  //     <Menu.Item
  //       as={Link}
  //       to="/job/recruiter/calender"
  //       className="RecruiterTabMenu"
  //       key="myCelander">
  //       <p>
  //         <IcMyCalendar pathcolor="#acaeb5" />
  //       </p>
  //       My Calender
  //     </Menu.Item>
  //   ),
  //   render: () => <Tab.Pane>My Celander</Tab.Pane>
  // }
];

class RecruiterDashboard extends React.Component {
  constructor(props) {
    super(props);
    this.selectedItems = [];
  }

  state = {
    selectedItems: []
  };

  hasId = (applId, jobId) => {
    const apps = this.state.selectedItems.filter(val => {
      if (val.applicationId === applId && val.jobId === jobId) {
        return true;
      }

      return false;
    });

    if (apps && apps.length > 0) {
      return true;
    }

    return false;
  };

  onItemSelect = (applId, jobId) => {
    if (!this.hasId(applId, jobId)) {
      this.setState({
        selectedItems: [
          ...this.state.selectedItems,
          {
            applicationId: applId,
            jobId: jobId
          }
        ]
      });
    } else {
      this.setState({
        selectedItems: this.state.selectedItems.filter(val => {
          if (val.applicationId === applId && val.jobId === jobId) {
            return false;
          }

          return true;
        })
      });
    }

    // console.log(this.selectedItems);
  };

  render() {
    const { match } = this.props;

    return (
      <div>
        <Responsive maxWidth={1024}>
          {(key => {
            switch (key) {
              case 0:
                return <RecruiterMyjobMobile {...this.props} />;
              case 1:
                return <RecruiterCandidateMobile {...this.props} />;
              // case 2:
              //   return <LikeJobsCondidateMobile {...this.props} />;
              // case 3:
              //   return <SavedProfileMobile {...this.props} />;
              case 2:
                return <ManageComapniesMobile />;
              case 3:
                return <ListOfinterviewContainerMobile {...this.props} />;
              default:
                return <div />;
            }
          })(
            getActiveTab(match.params.id, {
              myjobs: 0,
              candidates: 1, // 2
              // liked: 2,
              // saved: 3,
              compaines: 2,
              interview: 3

              // recommended: 3,
              // calender: 5
            })
          )}
        </Responsive>
        <Responsive minWidth={1025}>
          <div className="RecruiterDashboard">
            <Tab
              menu={{ fluid: true, vertical: true, tabular: true }}
              panes={panes}
              grid={{ tabWidth: 2, paneWidth: 12 }}
              activeIndex={getActiveTab(match.params.id, {
                myjobs: 0,
                candidates: 1, // 2
                // liked: 2,
                // saved: 3,
                compaines: 2,
                interview: 3
                // recommended: 3,
                // calender: 5
              })}
            />
            {/* TODO: Bulk Action Design */}
            {this.state.selectedItems.length > 0 ? (
              <MultiCheckPopup selectedItems={this.state.selectedItems} />
            ) : null}
          </div>
        </Responsive>
      </div>
    );
  }
}

export default RecruiterDashboard;
