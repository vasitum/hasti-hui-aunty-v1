import React from "react";
import { Container } from "semantic-ui-react";
import MobileHeader from "../../../../MobileComponents/MobileHeader";
import DashBoardNavigationDrawer from "../../../../MobileComponents/DashBoardNavigationDrawer";
import getInterviews from "../../../../../api/jobs/getInterviews";

import IcDotIcon from "../../../../../assets/svg/IcDotIcon";

import { startOfDay, startOfMonth, getTime, format } from "date-fns";

import "./index.scss";

function getInterviewSubData(interview) {
  switch (interview.interviewMode) {
    case "telephone":
      return (
        <React.Fragment>
          <span>Telephonic</span>
          <span />
        </React.Fragment>
      );

    case "videoCall":
      return (
        <React.Fragment>
          <span>Video Call | {interview.interviewSubMode}</span>
          <span>{interview.interviewContactInfo}</span>
        </React.Fragment>
      );

    case "inPerson":
      return (
        <React.Fragment>
          <span>In Person (F2F)</span>
          <span>{interview.interviewSubMode}</span>
        </React.Fragment>
      );

    default:
      break;
  }
}

function getGroupedData(data) {
  if (!data || !Array.isArray(data) || !data.length) {
    return [];
  }

  let groupedData = {};

  data.map(interview => {
    if (!interview) {
      return;
    }

    const { scheduledDate } = interview;

    if (!scheduledDate) {
      return;
    }

    const month = getTime(startOfMonth(scheduledDate));
    const day = getTime(startOfDay(scheduledDate));

    // groupedData[month][day] = [...groupedDate, interview];

    // check grouped data
    if (Object.prototype.hasOwnProperty.call(groupedData, month)) {
      // type object
      const groupedMonth = groupedData[month];
      if (Object.prototype.hasOwnProperty.call(groupedMonth, day)) {
        // type Array
        const groupedDate = groupedMonth[day];
        groupedData[month][day] = [...groupedDate, interview];
      } else {
        groupedData[month][day] = [interview];
      }
    } else {
      groupedData[month] = {
        [day]: [interview]
      };
    }
  });

  return groupedData;
}

export default class ListOfInterviews extends React.Component {
  state = {
    interviews: []
  };

  async componentDidMount() {
    try {
      const res = await getInterviews();
      const data = await res.json();
      // console.log("this is data", getGroupedData(data));

      this.setState({
        interviews: getGroupedData(data)
      });
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    let pathColor = "rgba(11, 158, 208, 1)";
    let pathColor2 = "red";

    const interviewKeys = Object.keys(this.state.interviews);
    return (
      <React.Fragment>
        <MobileHeader
          headerLeftIcon={<DashBoardNavigationDrawer />}
          headerTitle="Interviews List"
        />
        <Container>
          {interviewKeys.map(month => {
            const interviewsOfMonth = this.state.interviews[month];
            const interviewDayKeys = Object.keys(interviewsOfMonth);
            return interviewDayKeys.map(interviewDay => {
              // console.log(interviewDay);
              const interviewsOfDay = interviewsOfMonth[interviewDay];
              return (
                <div>
                  <div className="MobListOfInterviews">
                    <div className="CardMobListOfInterviews">
                      <span className="date">
                        {format(Number(interviewDay), "D")}
                      </span>
                      <span className="day">
                        {" "}
                        {format(Number(interviewDay), "dddd")}{" "}
                      </span>
                    </div>
                  </div>
                  {interviewsOfDay.map(interview => {
                    return (
                      <div className="MobListOfInterviews">
                        <div className="CardMobListOfInterviews">
                          <div className="content">
                            <span className="time">
                              {format(interview.scheduledDate, "hh:mm a")}
                            </span>
                          </div>
                        </div>
                        <div className="CardMobListOfInterviews">
                          <div className="content">
                            <span>
                              <IcDotIcon
                                width="8"
                                height="8"
                                pathcolor={pathColor}
                              />{" "}
                              {interview.candidateName}
                            </span>
                            <span>
                              <IcDotIcon
                                width="8"
                                height="8"
                                pathcolor={pathColor2}
                              />{" "}
                              {interview.interviewerName}
                            </span>
                          </div>
                        </div>
                        <div className="CardMobListOfInterviews">
                          <div className="content">
                            <span>
                              {interview.comName}{" "}
                              {interview.jobName
                                ? `| ${interview.jobName}`
                                : ""}
                            </span>
                          </div>
                        </div>
                        <div className="CardMobListOfInterviews">
                          <div className="content">
                            {getInterviewSubData(interview)}
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </div>
              );
            });
          })}
        </Container>
      </React.Fragment>
    );

    return (
      <React.Fragment>
        <MobileHeader
          headerLeftIcon={<DashBoardNavigationDrawer />}
          headerTitle="Interviews List"
        />

        {this.state.interviews.map(interview => {
          let dateObj = new Date(interview.scheduledDate);
          let date = dateObj.getDate().toString();
          let interviewTime = dateObj.toLocaleTimeString();
          let day;
          switch (dateObj.getDay()) {
            case 0:
              day = "Sunday";
              break;
            case 1:
              day = "Monday";
              break;
            case 2:
              day = "Tuesday";
              break;
            case 3:
              day = "Wednesday";
              break;
            case 4:
              day = "Thursday";
              break;
            case 5:
              day = "Friday";
              break;
            case 6:
              day = "Saturday";
          }

          let interviewMode;
          switch (interview.interviewMode) {
            case "inPerson":
              interviewMode = interview.interviewMode;
              break;
            case "telephone":
              interviewMode = interview.interviewContactNumber;
              break;
            case "videoCall":
              interviewMode = interview.interviewContactInfo;
              break;
          }

          return (
            <div key={interview._id}>
              <div className="MobListOfInterviews">
                <div className="CardMobListOfInterviews">
                  <span className="date">{date}</span>
                  <span className="day"> {day} </span>
                </div>
              </div>

              <div className="MobListOfInterviews">
                <div className="CardMobListOfInterviews">
                  <div className="content">
                    <span className="time">{interviewTime}</span>
                  </div>
                </div>
                <div className="CardMobListOfInterviews">
                  <div className="content">
                    <span>
                      <IcDotIcon width="8" height="8" pathcolor={pathColor} />{" "}
                      {interview.candidateName}
                    </span>
                    <span>
                      <IcDotIcon width="8" height="8" pathcolor={pathColor2} />{" "}
                      {interview.interviewerName}
                    </span>
                  </div>
                </div>
                <div className="CardMobListOfInterviews">
                  <div className="content">
                    <span>{interview.comName}</span>
                  </div>
                </div>
                <div className="CardMobListOfInterviews">
                  <div className="content">
                    <span>{interview.interviewMode}</span>
                    <span>{interviewMode}</span>
                  </div>
                </div>
              </div>
            </div>
          );
        })}
        {/* <div className="MobListOfInterviews">
          <div className="CardMobListOfInterviews">
            <span className="date">27</span>
            <span className="day"> Tuesday </span>
          </div>
        </div>

        <div className="MobListOfInterviews">
          <div className="CardMobListOfInterviews">
            <div className="content">
              <span className="time">9 AM</span>
            </div>
          </div>
          <div className="CardMobListOfInterviews">
            <div className="content">
              <span>
                <IcDotIcon width="8" height="8" pathcolor={pathColor} /> tuesday
              </span>
              <span>
                <IcDotIcon width="8" height="8" pathcolor={pathColor} /> tuesday
              </span>
            </div>
          </div>
        </div> */}
      </React.Fragment>
    );
  }
}
