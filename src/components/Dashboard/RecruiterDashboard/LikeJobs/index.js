import React from "react";

import RecommendedJobHeader from "../RecommendedJobs/RecommendedJobHeader";
import RecommendedJobCard from "../RecommendedJobs/RecommendedJobCard";

import NoFoundMessageDashboard from "../../Common/NoFoundMessageDashboard";

import {
  getLikedUser,
  getLikedUserFilter
} from "../../../../api/user/getJobByActions";

import "./index.scss";
import { Container } from "semantic-ui-react";

class LikeJobs extends React.Component {
  constructor(props) {
    super(props);
    this.onFilter = this.onFilter.bind(this);
  }

  state = {
    hits: [],
    tagItems: [],
    tags: []
  };

  async onFilter(text, tag) {
    /* remove all applied filters */
    if (!text) {
      this.componentDidMount();
      return;
    }

    try {
      const res = await getLikedUserFilter(text);
      if (res.status === 200) {
        const data = await res.json();
        // console.log(data);
        this.setState({
          hits: data.user
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  async componentDidMount() {
    try {
      const res = await getLikedUser();
      if (res.status === 200) {
        const data = await res.json();
        // console.log(data);
        this.setState({
          hits: data.user,
          tagItems: data.labelItems,
          tags: data.label
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const { hits } = this.state;

    return (
      <div className="RecommendedJobs LikeJobs_condidate">
        <Container>
          <RecommendedJobHeader
            onFilter={this.onFilter}
            isShowSortByDropdown
            placeholder="Search liked profiles"
          />
          <div className="LikeJobs_container">
            {hits.map(hit => {
              return (
                <RecommendedJobCard
                  tohref={`/user/public/${hit._id}`}
                  hit={hit}
                />
              );
            })}{" "}
          </div>
          {hits.length === 0 ? (
            <NoFoundMessageDashboard
              noFountTitle="No data found"
              noFountSubTitle="People you've liked will show up here"
            />
          ) : null}
        </Container>
      </div>
    );
  }
}

export default LikeJobs;
