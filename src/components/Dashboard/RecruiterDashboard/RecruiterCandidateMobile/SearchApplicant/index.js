import React from "react";
import "./index.scss";
import PropTypes from "prop-types";
import { Dropdown } from "semantic-ui-react";
import StatisticOptionGrid from "./StatisticOptionGrid";
import JobSelect from "./JobSelect";
import StageSelect from "./StageSelect";
import "./index.scss";

const CardOptions = [
  {
    text: (
      <div className="CardOptions">
        <p className="title">UI/UI Design</p>
        <p className="subTitle">TCS</p>
      </div>
    ),
    value: "Basic screening"
  },
  {
    text: (
      <div className="CardOptions">
        <p className="title">Software Developer</p>
        <p className="subTitle">WIPRO</p>
      </div>
    ),
    value: "Interview"
  },
  {
    text: (
      <div className="CardOptions">
        <p className="title">Web Developer</p>
        <p className="subTitle">INFOSYS</p>
      </div>
    ),
    value: "New applicants"
  }
  // {
  //   text: "Pending applicants",
  //   value: "Pending applicants"
  // }
];

const StatisticOption = [
  {
    text: <StatisticOptionGrid title="New applicants" count="12" />,
    value: "New applicants"
  },
  {
    text: <StatisticOptionGrid title="Pending" count="08" />,
    value: "Pending applicants"
  },
  {
    text: <StatisticOptionGrid title="Basic Screening" count="06" />,
    value: "Basic Screening"
  },
  {
    text: <StatisticOptionGrid title="Interview" count="14" />,
    value: "Interview"
  },
  {
    text: <StatisticOptionGrid title="Offere" count="12" />,
    value: "Offere"
  }
];

const SearchApplicant = props => {
  const { footerIconLeft, footerTitle, children } = props;
  return (
    <div className="SearchApplicant">
      <div className="mobile_allJobs">
        <JobSelect attribute={"jobName"} />
      </div>
      <div className="mobile_allCandidates">
        {/* <Dropdown placeholder="All" selection options={StatisticOption} fluid /> */}
        <StageSelect attribute={"status"} />
      </div>
    </div>
  );
};

export default SearchApplicant;
