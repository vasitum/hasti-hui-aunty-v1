import React from "react";

import { Grid } from "semantic-ui-react";
import PropTypes from "prop-types";

import "./index.scss";

const StatisticOptionGrid = props => {
  const { title, count } = props;
  return (
    <Grid>
      <Grid.Row >
        <Grid.Column width="13">
          <span>{title}</span>
        </Grid.Column>
        <Grid.Column width="3" textAlign="right">
          <span>{count}</span>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

export default StatisticOptionGrid;
