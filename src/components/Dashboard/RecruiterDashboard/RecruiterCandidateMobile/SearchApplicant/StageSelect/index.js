import React from "react";
import { withSelect } from "../../../../../AlgoliaContainer";
import { Dropdown } from "semantic-ui-react";
import StatisticOptionGrid from "../StatisticOptionGrid";
import { RECR_STAGES } from "../../../../../../constants/applicationStages";

const STAGES = RECR_STAGES;

function processItems(stages, items, currentRefinement, onSelect) {
  const ret = [];
  let totalCount = 0;
  // let isActiveFound = false;

  stages.map(stage => {
    // find the data
    const index = items.filter(val => val.label === stage);
    // const active = currentRefinement === stage;
    // if (active) {
    //   isActiveFound = true;
    // }

    // check the data
    if (index.length > 0) {
      const data = index[0];

      // increase counter
      totalCount = totalCount + data.count;

      // push it!
      ret.push({
        text: <StatisticOptionGrid title={STAGES[stage]} count={data.count} />,
        value: stage
      });

      return;
    }

    // unable to find it
    ret.push({
      text: <StatisticOptionGrid title={STAGES[stage]} count={0} />,
      value: stage
    });
  });

  ret.unshift({
    text: <StatisticOptionGrid title={"All"} count={totalCount} />,
    value: ""
  });

  // console.log(ret);
  return ret;
}

class StageSelect extends React.Component {
  render() {
    const { items, currentRefinement, onSelect } = this.props;
    const finalItems = processItems(
      Object.keys(STAGES),
      items,
      currentRefinement,
      onSelect
    );

    return (
      <Dropdown
        placeholder="All"
        selection
        value={currentRefinement}
        onChange={(e, { value }) => onSelect(value)}
        options={finalItems}
        fluid
      />
    );
  }
}

export default withSelect(StageSelect);
