import React from "react";
import { withSelect } from "../../../../../AlgoliaContainer";
import { Dropdown } from "semantic-ui-react";

class JobSelect extends React.Component {
  render() {
    const { items, currentRefinement, onSelect } = this.props;

    return (
      <Dropdown
        placeholder="All jobs"
        selection
        options={[].concat(
          {
            text: "All jobs",
            value: "",
            key: "All Jobs"
          },
          items.map(item => {
            return {
              text: item.label,
              value: item.label,
              key: item.label
            };
          })
        )}
        value={currentRefinement ? currentRefinement : ""}
        onChange={(e, { value }) => onSelect(value)}
        fluid
      />
    );
  }
}

export default withSelect(JobSelect);
