import React from "react";
import { withSearchBox } from "../../../../../AlgoliaContainer";
import { Input } from "semantic-ui-react";

class SearchBox extends React.Component {
  render() {
    const { currentRefinement, refine } = this.props;
    return (
      <React.Fragment>
        <label />
        <Input
          icon="search"
          value={currentRefinement}
          onChange={(e, { value }) => refine(value)}
          placeholder="Search applicants"
        />
      </React.Fragment>
    );
  }
}

export default withSearchBox(SearchBox);
