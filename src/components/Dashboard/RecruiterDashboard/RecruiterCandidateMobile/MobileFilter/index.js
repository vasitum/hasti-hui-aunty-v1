import React, { Component } from "react";
import { withCurrentFilters } from "../../../../AlgoliaContainer";
import { Dropdown, Input, Checkbox, Grid } from "semantic-ui-react";
import { Form } from "formsy-semantic-ui-react";
import ApplyHeader from "../ApplyHeader";
import MyJobDateSelectFilter from "../../RecruiterDashbordMobile/RecruiterMyjobMobile/MyJobFilterMobile/MyJobDateSelectFilter";
import MyJobCustomFilter from "../../RecruiterDashbordMobile/RecruiterMyjobMobile/MyJobFilterMobile/MyJobCustomFilter";
import SearchBox from "./SearchBox";

import "./index.scss";
// import StatisticOptionGrid from "./SearchApplicant/StatisticOptionGrid";

function getCurrentStatus(items) {
  for (let i = 0; i < items.length; i++) {
    const element = items[i];
    if (element.attribute === "status") {
      return element;
    }
  }

  return {
    attribute: "status",
    currentRefinement: "All",
    id: "status",
    index: "jobApplication",
    label: "status: All"
  };
}

class MobileFilter extends Component {
  state = {};
  handleChange = (e, { value }) => this.setState({ value });

  render() {
    const { style, items, ...resProps } = this.props;
    const { currentRefinement } = getCurrentStatus(items);

    return (
      <div className="Main_Box_Filter" style={style}>
        <ApplyHeader {...resProps} />
        <div className="candidate_filterContainer">
          <Grid>
            <Grid.Row className="Main_Form">
              <Grid.Column width={16}>
                <Form>
                  {/* <Form.Field>
                  <Checkbox
                    radio
                    label="CV available"
                    name="checkboxRadioGroup"
                    value="CV"
                    checked={this.state.value === "CV"}
                    onChange={this.handleChange}
                  />
                </Form.Field>
                <Form.Field>
                  <Checkbox
                    radio
                    label="Rejected"
                    name="checkboxRadioGroup"
                    value="Rejected"
                    checked={this.state.value === "Rejected"}
                    onChange={this.handleChange}
                  />
                </Form.Field> */}
                  {/* <Form.Field>
                  <label>Interview</label>
                  <Dropdown
                    selection
                    options={InterviewOptions}
                    defaultValue={InterviewOptions[0].value}
                  />
                </Form.Field>
                <Form.Field>
                  <label>Submitted</label>
                  <Dropdown
                    selection
                    options={SubmittedOptions}
                    defaultValue={SubmittedOptions[3].value}
                  />
                </Form.Field> */}

                  {currentRefinement === "Rejected" ||
                  currentRefinement === "Hold" ||
                  currentRefinement === "Yettoscreen" ? null : (
                    <MyJobDateSelectFilter
                      label={"Interview"}
                      attribute={"interviewScheduledDate"}
                    />
                  )}

                  <MyJobDateSelectFilter
                    label={"Submitted"}
                    attribute={"appliedDate"}
                    past
                  />

                  {currentRefinement === "Interview" ? (
                    <MyJobCustomFilter
                      attribute={"interviewScheduledDate"}
                      label={"Interview Status"}
                      currentDate={new Date().getTime()}
                      options={[
                        {
                          text: "Select",
                          value: "",
                          refineType: ""
                        },
                        {
                          text: "Scheduled",
                          value: "sched",
                          refineType: "lt"
                        },
                        {
                          text: "Feedback Pending",
                          value: "feedback",
                          refineType: "gt"
                        }
                      ]}
                    />
                  ) : null}

                  {currentRefinement === "Rejected" ? (
                    <MyJobCustomFilter
                      attribute={"rejectBy"}
                      label={"Rejected By"}
                      options={[
                        {
                          text: "Select",
                          value: "",
                          refineType: "string"
                        },
                        {
                          text: "AI",
                          value: "ai",
                          refineType: "string"
                        },
                        {
                          text: "User",
                          value: "user",
                          refineType: "string"
                        }
                      ]}
                    />
                  ) : null}

                  {/* <Form.Field>
                  <label />
                  <Input icon="search" placeholder="Search applicants" />
                </Form.Field> */}
                  <Form.Field>
                    <SearchBox />
                  </Form.Field>
                  {/* <MyJobSearchBox /> */}
                </Form>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>
      </div>
    );
  }
}
export default withCurrentFilters(MobileFilter);
