import React from "react";
import { Container, Dropdown, Button } from "semantic-ui-react";
import MobileHeader from "../../../MobileComponents/MobileHeader";
import SearchApplicant from "./SearchApplicant";
import CandidateCardContainer from "../Candidates/CandidateCardContainer";
import DashboardFilterFooter from "./DashboardFilterFooter";
import CandidateSortBy from "../Candidates/CandidateSortBy";

import IcFilterIcon from "../../../../assets/svg/IcFilterIcon";

import IcFooterArrowIcon from "../../../../assets/svg/IcFooterArrowIcon";

import CandidateCVFilter from "../Candidates/CandidateCVFilter";

import DashBoardNavigationDrawer from "../../../MobileComponents/DashBoardNavigationDrawer";

import MultipleChekPopup from "../Candidates/MultipleChekPopup";

import MobileFilter from "./MobileFilter";
import { withSearch } from "../../../AlgoliaContainer";
import { USER } from "../../../../constants/api";
import "./index.scss";

const SortBy = [
  {
    text: "Latest",
    value: "Latest"
  },
  {
    text: "Relevent",
    value: "Relevent"
  }
];

class MobileCandidate extends React.Component {
  constructor(props) {
    super(props);
  }

  state = {
    isShowFilter: false,
    selectedItems: []
  };

  isShowMobileFilter = () => {
    this.setState({
      isShowFilter: true
    });
  };

  isShowCandidate = () => {
    this.setState({
      isShowFilter: false
    });
  };

  hasId = (applId, jobId) => {
    const apps = this.state.selectedItems.filter(val => {
      if (val.applicationId === applId && val.jobId === jobId) {
        return true;
      }

      return false;
    });

    if (apps && apps.length > 0) {
      return true;
    }

    return false;
  };

  onDeselectClick = e => {
    this.setState({
      selectedItems: []
    });
  };

  onItemSelect = (applId, jobId, candidateId) => {
    if (!this.hasId(applId, jobId)) {
      this.setState({
        selectedItems: [
          ...this.state.selectedItems,
          {
            applicationId: applId,
            jobId: jobId,
            receiverId: candidateId
          }
        ]
      });
    } else {
      this.setState({
        selectedItems: this.state.selectedItems.filter(val => {
          if (val.applicationId === applId && val.jobId === jobId) {
            return false;
          }

          return true;
        })
      });
    }

    // console.log(this.selectedItems);
  };

  render() {
    return (
      <div>
        <CandidateCVFilter
          label={"Recruiter"}
          attribute="reqId"
          defaultRefinement={true}
          value={window.localStorage.getItem(USER.UID)}
          style={{
            display: "none"
          }}
        />

        <MobileFilter
          style={{
            display: this.state.isShowFilter ? "block" : "none"
          }}
          onClick={this.isShowCandidate}
        />
        <div
          className="MobileCondidate"
          style={{
            display: !this.state.isShowFilter ? "block" : "none"
          }}>
          <div>
            <MobileHeader
              headerLeftIcon={<DashBoardNavigationDrawer />}
              className="isMobileHeader_fixe"
              // onClick={() => this.props.history.go(-1)}
              headerTitle="Candidates"
            />
          </div>
          <div>
            <SearchApplicant />
          </div>
          <div>
            <Container>
              <CandidateCardContainer
                selectedItems={this.state.selectedItems}
                onCardSelect={this.onItemSelect}
                noClick
              />
            </Container>
          </div>
          <div>
            <DashboardFilterFooter
              onClick={e =>
                this.setState(state => {
                  return {
                    ...state,
                    isShowFilter: true
                  };
                })
              }
              footerIconLeft={<IcFilterIcon pathcolor="#6e768a" height="12" />}
              footerTitle="Filter">
              <div className="mobileFooter_left">
                <CandidateSortBy
                  defaultRefinement={"jobApplication_date"}
                  items={[
                    { value: "jobApplication", label: "Relevent" },
                    { value: "jobApplication_date", label: "Recent" }
                  ]}
                />
              </div>
              {this.state.selectedItems.length > 0 ? (
                <MultipleChekPopup
                  isMobileBulkAction
                  selectedItems={this.state.selectedItems}
                  onDeselectClick={this.onDeselectClick}
                />
              ) : null}
            </DashboardFilterFooter>
          </div>
        </div>
      </div>
    );
  }
}

export default withSearch(MobileCandidate, {
  apiKey: process.env.REACT_APP_ALGOLIA_APPLICATION_API_KEY,
  appId: process.env.REACT_APP_ALGOLIA_APPLICATION_APP_ID,
  indexName: process.env.REACT_APP_ALGOLIA_APPLICATION_INDEX_NAME,
  noRouting: true
});
