import React from "react";
import { Header, Segment, Grid, List, Dropdown, Menu } from "semantic-ui-react";
import ICDots from "../../../../assets/svg/IcMoreOptionIcon";
import "./index.scss";

const friendOptions = [
  {
    text: "Pending",
    value: "Pending"
  },
  {
    text: "Reject",
    value: "Reject"
  },
  {
    text: "Basic screening",
    value: "Basic screening"
  },
  {
    text: "Interviews",
    value: "Interviews"
  },
  {
    text: "Offer extended",
    value: "Offer extended"
  }
];

const Bulkoption = () => (
  <div className="Main_top">
    <Header attached="top" className="Main_header">
      <Grid>
        <Grid.Column width={12} className="headersection">
          <h4>Bulk actions</h4>
        </Grid.Column>
        <Grid.Column width={4} className="dropdownsections">
          <h5>02</h5>
          <Dropdown
            pointing="top left right"
            icon={<ICDots pathcolor="#ceeefe" />}>
            <Dropdown.Menu>
              <Dropdown.Item text="Select all" />
              <Dropdown.Item text="Deselect all" />
            </Dropdown.Menu>
          </Dropdown>
        </Grid.Column>
      </Grid>
    </Header>
    <Segment attached className="Main_segment">
      <List>
        <List.Item>Send Message</List.Item>
        <Dropdown text="Change status" options={friendOptions} />
      </List>
    </Segment>
  </div>
);

export default Bulkoption;
