import React from "react";
import RecommendedJobHeader from "./RecommendedJobHeader";
import RecommendedJobCard from "./RecommendedJobCard";

import "./index.scss";
import { Container } from "semantic-ui-react";

class RecommendedJobs extends React.Component {
  render() {
    return (
      <div className="RecommendedJobs">
        <Container>
          <RecommendedJobHeader
            placeholder="Search recommended jobs"
          />
          <div className="safadb">
            <RecommendedJobCard />
          </div>
        </Container>

      </div>
    )
  }
}



export default RecommendedJobs;