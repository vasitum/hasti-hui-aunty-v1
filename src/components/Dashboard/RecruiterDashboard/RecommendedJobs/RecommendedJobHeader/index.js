import React from "react";
import { Grid, Button, Dropdown } from "semantic-ui-react";
import { Form } from "formsy-semantic-ui-react";
import SearchIcon from "../../../../../assets/svg/SearchIcon";
import InputField from "../../../../Forms/FormFields/InputField";
import StatusSortByDropdown from "../../../../Dropdown/StatusSortByDropdown";

import PropTypes from "prop-types";

import "./index.scss";

// const options = [
//   { value: "jobApplication", label: "Relevant" },
//   { value: "jobApplication_date", label: "Recent" }

// ]

const Recentstatus = () => {
  const items = [
    {
      text: "Relevant",
      value: "Relevant"
    },
    {
      text: "Recent",
      value: "Recent"
    }
  ];
  return (
    <span className="recent_status">
      Tag: <Dropdown inline options={items} defaultValue={items[0].value} />
    </span>
  );
};

class RecommendedJobHeader extends React.Component {
  state = {
    searchtext: ""
  };

  onChange = (e, { value, name }) => {
    this.setState({
      [name]: value
    });
  };

  onSubmit = e => {
    const { onFilter } = this.props;
    onFilter(this.state.searchtext);
  };

  render() {
    const {
      headerLeftColumn,
      headerRightColumn,
      isShowSortByDropdown,
      placeholder
    } = this.props;

    return (
      <div className="recommendedJobs_header">
        <div className="recommendedJobs_headerLeft">
          <Form className="" onSubmit={this.onSubmit}>
            <div className="recommended_jobSearch">
              <InputField
                name="searchtext"
                placeholder={placeholder}
                onChange={this.onChange}
                value={this.state.searchtext}
              />
              <Button compact className="SearchBtn">
                <SearchIcon
                  pathcolor={this.state.searchtext ? "#1f2532" : "#c8c8c8"}
                />
              </Button>
            </div>
          </Form>
        </div>

        {!isShowSortByDropdown ? (
          <div className="recommendedJobs_headerRight">
            <div className="dropdown_right">
              {<Recentstatus />}
              {/* <StatusSortByDropdown
                  items={options}
                  defaultRefinement={options[0].value}
                  /> */}
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}

RecommendedJobHeader.propTypes = {
  headerLeftColumn: PropTypes.string,
  headerRightColumn: PropTypes.string
};

RecommendedJobHeader.defaultProps = {
  headerLeftColumn: "5",
  headerRightColumn: "11"
};

export default RecommendedJobHeader;
