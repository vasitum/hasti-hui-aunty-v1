import React from "react";

import AccordionCardContainer from "../../../../Cards/AccordionCardContainer";

import MyJobCard from "../../../../Cards/MyJobCards";
import CardApplyContainer from "../../../../Cards/MyJobCards/CardApplyContainer";

import InfoSection from "../../../../Sections/InfoSection";
import QuillText from "../../../../CardElements/QuillText";

import FlatDefaultBtn from "../../../../Buttons/FlatDefaultBtn";

import MobileCardFooter from "../../../../Cards/MyJobCards/MobileCardFooter";

import MyJobsCardRecruiter from "../../../../Cards/MyJobCards/MyJobsCardRecruiter";

import SkillListExpBtn from "../../../../CardElements/SkillListExpBtn";

import SkillList from "../../../../CardElements/SkillList";

import PropTypes from "prop-types";

import "./index.scss";
import { Link } from "react-router-dom";

class RecommendedJobCard extends React.Component {
  render() {
    const {
      hit,
      noClick,
      isShowCardApplyContainer,
      isShowMobileFooterbottom,
      CardsWidthLeft,
      tohref,
      ...resProps
    } = this.props;

    return (
      <div className="RecommendedJobs_cardContainer">
        {/* <AccordionCardContainer
          noClick={noClick}
          cardHeader={
            <MyJobsCardRecruiter
              hit={hit}
              isProfleIncrease
              isSkillShow
              isRecommendation
              isShowMobileFooter={isShowMobileFooterbottom}
              {...resProps}>
              {!isShowCardApplyContainer ? <CardApplyContainer /> : null}
            </MyJobsCardRecruiter>
          }>
          <div className="Recommended_jobBody">
            <div className="Recommended_jobBodyContainer">
              <div className="Recommended_jobBodyLeft" />
              <div className="Recommended_jobBodyright">
                <div>
                  {hit.desc ? (
                    <InfoSection
                      headerSize="medium"
                      color="blue"
                      headerText="Job Description">
                      <QuillText readOnly value={hit.desc} />
                    </InfoSection>
                  ) : null}
                </div>
                <div>
                  {hit.skills && hit.skills.length > 0 ? (
                    <InfoSection
                      headerSize="medium"
                      color="blue"
                      headerText="Skills">
                      <SkillListExpBtn skills={hit.skills} />
                    </InfoSection>
                  ) : null}
                </div>

                <div className="Recommended_jobBodyFooter">
                  <FlatDefaultBtn btntext="View complete Job" />
                </div>
              </div>
            </div>
          </div>
        </AccordionCardContainer> */}
        <MyJobsCardRecruiter
          tohref={tohref}
          hit={hit}
          isProfleIncrease
          isSkillShow
          isRecommendation
          myJobCardsWidthLeft={CardsWidthLeft}
          isShowMobileFooter={isShowMobileFooterbottom}
          {...resProps}>
          {!isShowCardApplyContainer ? (
            <CardApplyContainer
              hit={hit}
              as={Link}
              tohref={tohref}
              btnText="View Profile"
            />
          ) : null}
        </MyJobsCardRecruiter>
      </div>
    );
  }
}

RecommendedJobCard.propTypes = {
  hit: PropTypes.string
};

RecommendedJobCard.defaultProps = {
  hit: {
    candidateSummary:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad ipsum dolor sit amet, consectetur adipisicing elit, sed do iusmod tempor incididunt ut labore minim veniam, quis nostru exercitation aliquip ex ea commodo consequat. See more",
    skills: ["html", "css", "javascript"]
  }
};

export default RecommendedJobCard;
