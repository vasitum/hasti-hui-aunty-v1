import React from "react";

import { Button, Container, Dropdown } from "semantic-ui-react";
import MobileHeader from "../../../../MobileComponents/MobileHeader";

import MobileFooter from "../../../../MobileComponents/MobileFooter";

import IcFooterArrowIcon from "../../../../../assets/svg/IcFooterArrowIcon";

import RecommendedJobHeader from "../../RecommendedJobs/RecommendedJobHeader";

import MobileCardFooter from "../../../../Cards/MyJobCards/MobileCardFooter";

import RecommendedJobCard from "../../RecommendedJobs/RecommendedJobCard";

import { Link } from "react-router-dom";

import {
  getLikedUser,
  getLikedUserFilter
} from "../../../../../api/user/getJobByActions";

import DashBoardNavigationDrawer from "../../../../MobileComponents/DashBoardNavigationDrawer";
import NoFoundMessageDashboard from "../../../Common/NoFoundMessageDashboard";
import "./index.scss";

class LikeJobsCondidateMobile extends React.Component {
  constructor(props) {
    super(props);
    this.onFilter = this.onFilter.bind(this);
  }

  state = {
    hits: [],
    tagItems: [],
    tags: []
  };

  async onFilter(text, tag) {
    /* remove all applied filters */
    if (!text) {
      this.componentDidMount();
      return;
    }

    try {
      const res = await getLikedUserFilter(text);
      if (res.status === 200) {
        const data = await res.json();
        // console.log(data);
        this.setState({
          hits: data.user
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  async componentDidMount() {
    try {
      const res = await getLikedUser();
      if (res.status === 200) {
        const data = await res.json();
        // console.log(data);
        this.setState({
          hits: data.user,
          tagItems: data.labelItems,
          tags: data.label
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    return (
      <div className="RecommendedJobsMobile LikeJobsCondidateMobile">
        <div className="RecommendedJobsMobile">
          <div className="Recommended_mobileHeader">
            <MobileHeader
              mobileHeaderLeftColumn="14"
              mobileHeaderRightColumn="2"
              headerLeftIcon={<DashBoardNavigationDrawer />}
              headerTitle="Liked Profiles"
            />
          </div>
          <div className="Recommended_mobileBody">
            <Container>
              <RecommendedJobHeader
                onFilter={this.onFilter}
                isShowSortByDropdown
                headerLeftColumn="16"
                placeholder="Search Liked Profile"
              />
              {this.state.hits.map(hit => {
                return (
                  <RecommendedJobCard
                    tohref={`/user/public/${hit._id}`}
                    noClick
                    hit={hit}
                    CardsWidthLeft="16"
                    isShowCardApplyContainer
                    isShowMobileFooterbottom={
                      <MobileCardFooter
                        as={Link}
                        tohref={`/user/public/${hit._id}`}
                        btnText="View Profile"
                      />
                    }
                  />
                );
              })}{" "}
              {this.state.hits.length === 0 ? (
                <NoFoundMessageDashboard
                  noFountTitle="No data found"
                  noFountSubTitle="People you've liked will show up here"
                />
              ) : null}
            </Container>
          </div>
          {/* {!isShowMobile ? (
            <div className="Recommended_mobileFooter">
              <MobileFooter
                mobileFooterLeftColumn="10"
                headerLeftIcon={<Recentstatus />}
              />
            </div>
          ) : null} */}
        </div>
      </div>
    );
  }
}

export default LikeJobsCondidateMobile;
