import React from "react";

import { Container, Button } from "semantic-ui-react";

import { Form } from "formsy-semantic-ui-react";

import MobileHeader from "../../../../../MobileComponents/MobileHeader";
import IcCloseIcon from "../../../../../../assets/svg/IcCloseIcon";
import MyJobSearchBox from "./MyJobSearchBox";

import MyJobSelectFilter from "./MyJobSelectFilter";
import MyJobDateSelect from "./MyJobDateSelectFilter";

import MobileHeaderApplyBtn from "../../../../../Buttons/MobileHeaderApplyBtn";

import "./index.scss";
class MyJobFilterMobile extends React.Component {
  render() {
    const { onCloseClick, style } = this.props;

    return (
      <div className="MyJobFilterMobile" style={style}>
        <div className="">
          <MobileHeader isHeaderTitle headerTitle="Filter jobs">
            <div className="fillterApplyBtn">
              <MobileHeaderApplyBtn 
                onClick={onCloseClick}
                btnText="Apply" 
              />
              <Button
                onClick={onCloseClick}
                compact
                className="filterHeader_closeBtn">
                <IcCloseIcon pathcolor="#ffffff" />
              </Button>
            </div>
          </MobileHeader>
        </div>
        <div>
          <Container>
            {/* <div className="clearAll_filter">
              <Button size="tiny" className="filterBtn">
                <IcCloseIcon pathcolor="#0b9ed0" width="10" height="10" />
                Clear all applied filters
              </Button>
            </div> */}

            <div className="">
              <Form>
                <MyJobSearchBox />

                <MyJobDateSelect
                  label={"Posted date"}
                  attribute={"datePosted"}
                />

                <MyJobSelectFilter
                  label={"Job Location"}
                  name="loc"
                  attribute={"locCity"}
                />

                <MyJobSelectFilter
                  label={"Company Name"}
                  name="com"
                  attribute={"comName"}
                />

                <MyJobSelectFilter
                  label={"Skills"}
                  name="skills"
                  attribute={"skills"}
                />
              </Form>
            </div>
          </Container>
        </div>
      </div>
    );
  }
}

export default MyJobFilterMobile;
