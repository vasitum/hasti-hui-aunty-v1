import React from "react";
import SelectOptionField from "../../../../../../Forms/FormFields/SelectOptionField";
import { withSelect } from "../../../../../../AlgoliaContainer";

class MyJobSelectFilter extends React.Component {
  render() {
    const { label, items, currentRefinement, onSelect, name } = this.props;

    return (
      <div className="jobFilterSelectField">
        <p>{label}</p>
        <SelectOptionField
          placeholder="All"
          optionsItem={[].concat(
            items.map(item => {
              return {
                text: `${item.label} (${item.count})`,
                value: item.label
              };
            })
          )}
          name={name}
          value={currentRefinement}
          onChange={(e, { value }) => onSelect(value)}
        />
      </div>
    );
  }
}

export default withSelect(MyJobSelectFilter);
