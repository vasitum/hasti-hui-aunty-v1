import React from "react";
import InputField from "../../../../../../Forms/FormFields/InputField";
import SearchIcon from "../../../../../../../assets/svg/SearchIcon";
import { withSearchBox } from "../../../../../../AlgoliaContainer";
import { Button } from "semantic-ui-react";

class MyJobSearchBox extends React.Component {
  state = {
    text: ""
  };

  onChange = (e, { value }) => {
    this.setState({
      text: value
    });
  };

  onClick = e => {
    const { refine } = this.props;
    refine(this.state.text);
  };

  render() {
    return (
      <div className="jobTitleField">
        <Button onClick={this.onClick} compact>
          <SearchIcon pathcolor="#c8c8c8" />
        </Button>
        <InputField
          name="jobTitle"
          onChange={this.onChange}
          value={this.state.text}
          placeholder="Enter Job Title or Location"
        />
      </div>
    );
  }
}

export default withSearchBox(MyJobSearchBox);
