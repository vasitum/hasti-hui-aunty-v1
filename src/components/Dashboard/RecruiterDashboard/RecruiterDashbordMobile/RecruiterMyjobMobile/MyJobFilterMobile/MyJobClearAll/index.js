import React from "react";
import { withCurrentFilters } from "../../../../../../AlgoliaContainer";
import { Button } from "semantic-ui-react";
import IcCloseIcon from "../../../../../../../assets/svg/IcCloseIcon";

class MyJobClearAll extends React.Component {
  render() {
    return (
      <div className="clearAll_filter">
        <Button
          onClick={this.props.refine(this.props.items)}
          size="tiny"
          className="filterBtn">
          <IcCloseIcon pathcolor="#0b9ed0" width="10" height="10" />
          Clear all applied filters
        </Button>
      </div>
    );
  }
}

export default withCurrentFilters(MyJobClearAll);
