import React from "react";
import { withCustomFilter } from "../../../../../../AlgoliaContainer";
import { CUSTOM_TYPES as type } from "../../../../../../../constants/algolia";
import SelectOptionField from "../../../../../../Forms/FormFields/SelectOptionField";

class MyJobDateSelectFilter extends React.Component {
  state = {
    filter: ""
  };

  onRefine = filter => {
    const { options, attribute, currentDate } = this.props;

    if (!filter) {
      this.props.refine(``);
      return;
    }

    const refineType = (function(options) {
      for (let i = 0; i < options.length; i++) {
        const element = options[i];
        if (element.value === filter) {
          return element.refineType;
        }
      }

      return "";
    })(options);

    switch (refineType) {
      case type.NUMBER.LESS_THAN:
        this.props.refine(`${attribute}<${currentDate}`);
        break;

      case type.NUMBER.GREATER_THAN:
        this.props.refine(`${attribute}>${currentDate}`);
        break;

      case type.NUMBER.EQUALS:
        this.props.refine(`${attribute}=${currentDate}`);
        break;

      case type.STRING:
      default:
        this.props.refine(`${attribute}:${filter}`);
        break;
    }
  };

  onChange = (e, { value }) => {
    this.setState(
      {
        currentValue: value
      },
      () => {
        this.onRefine(value);
      }
    );
  };

  render() {
    const { label, options } = this.props;

    return (
      <div className="jobFilterSelectField">
        <p>{label}</p>
        <SelectOptionField
          placeholder="All"
          optionsItem={options}
          onChange={this.onChange}
          noSelect
          name="PostedDate"
        />
      </div>
    );
  }
}

export default withCustomFilter(MyJobDateSelectFilter);
