import React from "react";
import { Button } from "semantic-ui-react";
import { Link } from "react-router-dom";
import Proptypes from "prop-types";

import "./index.scss";

function getSectionName(status) {
  switch (status) {
    case "NewApplicant":
      return "New applicants";

    case "Seen":
      return "Seen";

    case "Hold":
      return "Hold";

    case "Screening":
      return "Screening";

    case "ShortList":
      return "Shortlist";

    case "Interview":
      return "Interview";

    case "Joinend":
      return "Joined";

    case "OfferDropOut":
      return "Offer Dropout";

    case "Rejected":
      return "Rejected";

    case "Offer":
      return "Offer";

    default:
      return "NewApplicant";
  }
}

class MyJobCardStatusMore extends React.Component {
  state = {
    hidden: true
  };

  onHideChange = e => {
    e.stopPropagation();
    this.setState(state => {
      return {
        ...state,
        hidden: !state.hidden
      };
    });
  };

  render() {
    const { hit } = this.props;
    const { hidden } = this.state;
    return (
      <div className="MyJobCardStatusMore">
        <div>
          <div className="status">
            <div className="status_list" />
            <Link
              to={`/job/recruiter/candidates?menu%5BjobName%5D=${encodeURIComponent(
                hit.title
              )}&page=1`}
               
              className="total_countMobile"
              >

              <p className="total">Total application</p>
              <p className="count">

                {hit.appliCount
                  ? hit.appliCount
                  : hit.applicationCount
                    ? hit.applicationCount
                    : 0}
              </p>

            </Link>

          </div>
        </div>
        <div className="mobile_MyJobsCardListStatus">
          {!hidden ? (
            <div className="mobile_statusMenu">
              {hit.applicationStatus.map(val => {
                return (
                  <Link
                    to={`/job/recruiter/candidates?menu%5Bstatus%5D=${
                      val.status
                      }&menu%5BjobName%5D=${encodeURIComponent(
                        hit.title
                      )}&page=1`}
                    style={{
                      pointerEvents: val.count > 0 ? "all" : "none"
                    }}>
                    <div className="statusList" key={val.status}>
                      <p>{val.status}</p>
                      <p>{val.count}</p>
                    </div>
                  </Link>
                );
              })}
            </div>
          ) : null}
          <div className="status_menuBtn">
            <div className="status_menuSpace" />
            <Button onClick={this.onHideChange} size="tiny" fluid>
              {!hidden ? "Less" : "More"}
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

MyJobCardStatusMore.Proptypes = {
  hit: Proptypes.string
};

MyJobCardStatusMore.defaultProps = {
  hit: {
    appliCount: "2"
  }
};

export default MyJobCardStatusMore;
