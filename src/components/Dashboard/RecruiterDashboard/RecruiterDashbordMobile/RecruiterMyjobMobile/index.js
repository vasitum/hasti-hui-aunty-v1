import React from "react";
import RecruiterMyjobMobileContainer from "./RecruiterMyjobMobileContainer";

import "./index.scss";

class RecruiterMyjobMobile extends React.Component {
  
  render() {
    return (
      <div className="RecruiterMyjobMobile">
        <RecruiterMyjobMobileContainer />
      </div>
    )
  }
}

export default RecruiterMyjobMobile;