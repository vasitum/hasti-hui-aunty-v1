import React from "react";
import { Button, Dropdown, Container } from "semantic-ui-react";
import MobileHeader from "../../../../../MobileComponents/MobileHeader";
import MobileFooter from "../../../../../MobileComponents/MobileFooter";

import MyJobCardHits from "../../../../RecruiterDashboard/MyJobs/MyJobCardHits";

import MyJobFilterMobile from "../MyJobFilterMobile";

import IcFooterArrowIcon from "../../../../../../assets/svg/IcFooterArrowIcon";
import IcFilterIcon from "../../../../../../assets/svg/IcFilterIcon";
import { withRouter } from "react-router-dom";
import { withSearch } from "../../../../../AlgoliaContainer";

import SortBy from "../../../../Common/SortBy";
import StageSelect from "../StageSelect";

import CandidateCVFilter from "../../../Candidates/CandidateCVFilter";
import { USER } from "../../../../../../constants/api";

import DashBoardNavigationDrawer from "../../../../../MobileComponents/DashBoardNavigationDrawer";

import "./index.scss";

class RecruiterMyjobMobileContainer extends React.Component {
  constructor(props) {
    super(props);
  }

  state = {
    isShowMobileFilter: "none",
    bodyClasses: "block"
  };

  isMobileFilter = e => {
    e.preventDefault();
    e.stopPropagation();
    this.setState({
      isShowMobileFilter: "block",
      bodyClasses: "none"
    });
  };

  isMobileFilterClose = e => {
    e.preventDefault();
    e.stopPropagation();
    this.setState({
      isShowMobileFilter: "none",
      bodyClasses: "block"
    });
  };
  render() {
    return (
      <div>
        <MyJobFilterMobile
          style={{
            display: this.state.isShowMobileFilter
          }}
          onCloseClick={this.isMobileFilterClose}
        />
        <div
          className="RecruiterMyjobMobileContainer"
          style={{
            display: this.state.bodyClasses
          }}>
          <div className="MyjobMobile_header">
            <MobileHeader
              className="isMobileHeader_fixe"
              headerLeftIcon={<DashBoardNavigationDrawer />}
              headerTitle="My Jobs"
              // <Button compact className="backArrowBtn">
              //     <IcFooterArrowIcon pathcolor="#6e768a" />
              //   </Button>
            />
          </div>
          <CandidateCVFilter
            label={"Recruiter"}
            attribute="userId"
            defaultRefinement={true}
            value={window.localStorage.getItem(USER.UID)}
            style={{
              display: "none"
            }}
          />
          <div className="MyjobMobile_body">
            <Container>
              <div className="mobile_allMyJobStatistic">
                <StageSelect attribute={"status"} />
              </div>

              <div>
                <MyJobCardHits
                  noClick={true}
                  isShowCardApplyContainer
                  isShowStausList
                />
              </div>
            </Container>
          </div>

          <div className="MyjobMobile_footer">
            <MobileFooter
              className="isMobileFooter_fixe"
              mobileFooterLeftColumn="6"
              mobileFooterRightColumn="10"
              headerLeftIcon={
                <Button
                  compact
                  onClick={this.isMobileFilter}
                  className="filterBtn">
                  <IcFilterIcon pathcolor="#6e768a" height="11.375" /> Filter
                </Button>
              }>
              <span className="recent_status">
                <SortBy
                  defaultRefinement={"job_by_date"}
                  items={[
                    { text: "Relevent", value: "job" },
                    { text: "Apply Count", value: "job_by_apply" },
                    { text: "Recent", value: "job_by_date" }
                  ]}
                />
              </span>
            </MobileFooter>
          </div>
          <div />
        </div>
      </div>
    );
  }
}

export default withRouter(
  withSearch(RecruiterMyjobMobileContainer, {
    apiKey: process.env.REACT_APP_ALGOLIA_API_KEY,
    appId: process.env.REACT_APP_ALGOLIA_APP_ID,
    indexName: "job",
    noRouting: true
  })
);
