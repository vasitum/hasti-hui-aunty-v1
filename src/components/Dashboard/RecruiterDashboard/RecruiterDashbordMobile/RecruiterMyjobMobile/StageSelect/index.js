import React from "react";
import { withSelect } from "../../../../../AlgoliaContainer";
import { Dropdown } from "semantic-ui-react";

const STAGES = {
  Draft: "DRAFT",
  Active: "ACTIVE",
  Close: "CLOSED"
};

function processItems(stages, items, currentRefinement, onSelect) {
  const ret = [];
  let totalCount = 0;
  // let isActiveFound = false;

  stages.map(stage => {
    // find the data
    const index = items.filter(val => val.label === stage);
    // const active = currentRefinement === stage;
    // if (active) {
    //   isActiveFound = true;
    // }

    // check the data
    if (index.length > 0) {
      const data = index[0];

      // increase counter
      totalCount = totalCount + data.count;

      // push it!
      ret.push({
        text: `${STAGES[stage]} (${data.count})`,
        value: stage
      });

      return;
    }

    // unable to find it
    ret.push({
      text: `${STAGES[stage]} (0)`,
      value: stage
    });
  });

  ret.unshift({
    text: `All (${totalCount})`,
    value: ""
  });

  // console.log(ret);
  return ret;
}

class StageSelect extends React.Component {
  render() {
    const { items, currentRefinement, onSelect } = this.props;
    const finalItems = processItems(
      Object.keys(STAGES),
      items,
      currentRefinement,
      onSelect
    );
    return (
      <Dropdown
        placeholder={"ALL"}
        selection
        options={finalItems}
        value={currentRefinement}
        onChange={(e, { value }) => onSelect(value)}
        fluid
      />
    );
  }
}

export default withSelect(StageSelect);
