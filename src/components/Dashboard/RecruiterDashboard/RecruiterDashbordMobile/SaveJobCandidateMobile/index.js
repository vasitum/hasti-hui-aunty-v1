import React from "react";

import { Button, Container, Dropdown } from "semantic-ui-react";
import MobileHeader from "../../../../MobileComponents/MobileHeader";

import MobileFooter from "../../../../MobileComponents/MobileFooter";

import IcFooterArrowIcon from "../../../../../assets/svg/IcFooterArrowIcon";

import RecommendedJobHeader from "../../RecommendedJobs/RecommendedJobHeader";

import MobileCardFooter from "../../../../Cards/MyJobCards/MobileCardFooter";

import SaveJobCards from "../../SaveJob/SaveJobCards";

import { Link } from "react-router-dom";

import DashBoardNavigationDrawer from "../../../../MobileComponents/DashBoardNavigationDrawer";

import NoFoundMessageDashboard from "../../../Common/NoFoundMessageDashboard";

import {
  getSavedUser,
  getSavedUserFilter
} from "../../../../../api/user/getJobByActions";

import getParsedtags from "../../../../../utils/ui/getParsedtags";

// import "./index.scss";

const Recentstatus = ({ tags, value, onChange }) => {
  const items = [
    {
      text: "UX Designer",
      value: "UX Designer"
    },
    {
      text: "Recentnew",
      value: "Recent"
    }
  ];
  return (
    <span className="recent_status">
      Tag: <Dropdown inline options={tags} value={value} onChange={onChange} />
    </span>
  );
};

class SaveJobCandidateMobile extends React.Component {
  constructor(props) {
    super(props);
    this.onFilter = this.onFilter.bind(this);

    this.text = "";
  }

  state = {
    hits: [],
    tagItems: [],
    tags: [],
    selectedTag: ""
  };

  onSelectChange = (e, { value }) => {
    this.setState(
      {
        selectedTag: value
      },
      () => {
        this.onFilter(this.text, this.state.selectedTag);
      }
    );
  };

  async onFilter(text, tag = this.state.selectedTag) {
    /* remove all applied filters */
    if (!text && !tag) {
      this.text = "";
      this.componentDidMount();
      return;
    }

    try {
      this.text = text;
      const res = await getSavedUserFilter(text ? text : null, tag);
      if (res.status === 200) {
        const data = await res.json();
        // console.log(data);
        this.setState({
          hits: data.user
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  async componentDidMount() {
    try {
      const res = await getSavedUser();
      if (res.status === 200) {
        const data = await res.json();
        // console.log(data);
        this.setState({
          hits: data.user,
          tagItems: data.labelItems,
          tags: data.label
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const { isShowMobile } = this.props;
    return (
      <div className="RecommendedJobsMobile">
        <div className="Recommended_mobileHeader">
          <MobileHeader
            mobileHeaderLeftColumn="14"
            mobileHeaderRightColumn="2"
            headerLeftIcon={<DashBoardNavigationDrawer />}
            headerTitle="Saved Jobs"
          />
        </div>
        <div className="Recommended_mobileBody">
          <Container>
            <RecommendedJobHeader
              onFilter={this.onFilter}
              isShowSortByDropdown
              headerLeftColumn="16"
              placeholder="Search Saved jobs"
            />
            {this.state.hits.map(hit => {
              return (
                <SaveJobCards
                  tohref={`/user/public/${hit._id}`}
                  hit={hit}
                  tags={getParsedtags(this.state.tagItems, hit._id)}
                  noClick
                  CardsWidthLeft="16"
                  isShowCardApplyContainer
                  isShowMobileFooterbottom={
                    <MobileCardFooter
                      as={Link}
                      tohref={`/user/public/${hit._id}`}
                      btnText="View Profile"
                    />
                  }
                />
              );
            })}
            {this.state.hits.length === 0 ? (
              <NoFoundMessageDashboard
                noFountTitle="No data found"
                noFountSubTitle="You can reach out to these saved profiles later"
              />
            ) : null}
          </Container>
        </div>
        {!isShowMobile ? (
          <div className="Recommended_mobileFooter">
            <MobileFooter
              mobileFooterLeftColumn="10"
              headerLeftIcon={
                <Recentstatus
                  tags={[].concat(
                    [
                      {
                        text: "All",
                        value: ""
                      }
                    ],
                    this.state.tags.map(val => {
                      return {
                        text: val,
                        value: val
                      };
                    })
                  )}
                  onChange={this.onSelectChange}
                  value={this.state.selectedTag}
                />
              }
            />
          </div>
        ) : null}
      </div>
    );
  }
}

export default SaveJobCandidateMobile;
