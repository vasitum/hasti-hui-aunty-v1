import React from "react";
import { Button } from "semantic-ui-react";
import { Form } from "formsy-semantic-ui-react";
import IcInfoIcon from "../../../../../assets/svg/IcInfo";
import ActionBtn from "../../../../Buttons/ActionBtn";
import FlatDefaultBtn from "../../../../Buttons/FlatDefaultBtn";
import InputTagField from "../../../../Forms/FormFields/InputTagField";
import "./index.scss";

const AddTag = () => (
  <Form className="Tag_Form">
    <Form.Field>
      <label>
        Add tag <IcInfoIcon pathcolor="#c8c8c8" />
      </label>
      <InputTagField placeholder="Eg. Manager, Developer etc." name="addTag" />
    </Form.Field>
    <Form.Field className="Tag_Button">
      <FlatDefaultBtn className="bgTranceparent" btntext="Cancel" />
      <ActionBtn actioaBtnText="Save" />
    </Form.Field>
  </Form>
);

export default AddTag;
