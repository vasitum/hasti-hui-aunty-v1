import React from "react";
import {
  Grid,
  Checkbox,
  Dropdown,
  Button,
  List,
  Popup,
  Header
} from "semantic-ui-react";

import { withCurrentFilters } from "../../../../AlgoliaContainer";

import CandidateSortBy from "../CandidateSortBy";
import CandidateCVFilter from "../CandidateCVFilter";
import CandidateDateFilter from "../CandidateDateFilter";
import CandidateCustomFilter from "../CandidateCustomFilter";

import CandidateScreeningStatusFilter from "../CandidateScreeningStatusFilter";
import FilterTags from "../../../Common/FilterTags";

import ICFilter from "../../../../../assets/svg/IcFilterIcon";
import IcCloseIcon from "../../../../../assets/svg/IcCloseIcon";
import { USER } from "../../../../../constants/api";
import qs from "qs";
import "./index.scss";
// import FlatDefaultBtn from "../../../../Buttons/FlatDefaultBtn";

function getCurrentStatus(items) {
  for (let i = 0; i < items.length; i++) {
    const element = items[i];
    if (element.attribute === "status") {
      return element;
    }
  }

  return {
    attribute: "status",
    currentRefinement: "All",
    id: "status",
    index: "jobApplication",
    label: "status: All"
  };
}

function getCurrentValue() {
  const parsedQs = qs.parse(window.location.search);
  if (Object.prototype.hasOwnProperty.call(parsedQs, "vasitum_action")) {
    switch (parsedQs["vasitum_action"]) {
      case "feedback_pending":
        return "feedback";

      default:
        return null;
    }
  }
  return null;
}

const FilterBar = ({ refine, items }) => {
  // console.log("REFINED", getCurrentStatus(items));
  const { currentRefinement } = getCurrentStatus(items);
  return (
    <div
      className="CandidateFilter"
      style={{
        display:
          currentRefinement === "Joinend" || currentRefinement === "Offered"
            ? "none"
            : "block"
      }}>
      <div className="Filter_Main">
        <Grid>
          <Grid.Row>
            <Grid.Column
              className="Filter_text"
              width="12"
              style={{
                display: "flex",
                alignItems: "center"
              }}>
              <List horizontal className="List_Main">
                <List.Item className="Filter_Sort">
                  <CandidateCVFilter
                    label={"Recruiter"}
                    attribute="reqId"
                    defaultRefinement={true}
                    value={window.localStorage.getItem(USER.UID)}
                    style={{
                      display: "none"
                    }}
                  />
                  <List.Content />
                </List.Item>
                <List.Item className="Filter_Sort">
                  <CandidateDateFilter
                    attribute={"appliedDate"}
                    label={"Submitted"}
                    past
                  />
                  <List.Content />
                </List.Item>
                {currentRefinement === "Rejected" ||
                currentRefinement === "Hold" ||
                currentRefinement === "Yettoscreen" ? null : (
                  <List.Item className="Filter_Sort">
                    <CandidateDateFilter
                      attribute={"interviewScheduledDate"}
                      label={"Interview"}
                      past
                    />
                    <List.Content />
                  </List.Item>
                )}
                {currentRefinement === "Rejected" ? (
                  <List.Item className="Filter_Sort">
                    <CandidateCustomFilter
                      attribute={"rejectBy"}
                      label={"Rejected By"}
                      options={[
                        {
                          text: "Select",
                          value: "",
                          refineType: "string"
                        },
                        {
                          text: "AI",
                          value: "ai",
                          refineType: "string"
                        },
                        {
                          text: "User",
                          value: "user",
                          refineType: "string"
                        }
                      ]}
                    />
                  </List.Item>
                ) : null}
                {currentRefinement === "Interview" ? (
                  <List.Item className="Filter_Sort">
                    <CandidateCustomFilter
                      attribute={"interviewScheduledDate"}
                      label={"Interview Status"}
                      currentDate={new Date().getTime()}
                      currentValue={getCurrentValue()}
                      options={[
                        {
                          text: "Select",
                          value: "",
                          refineType: ""
                        },
                        {
                          text: "Scheduled",
                          value: "sched",
                          refineType: "lt"
                        },
                        {
                          text: "Feedback Pending",
                          value: "feedback",
                          refineType: "gt"
                        }
                      ]}
                    />
                  </List.Item>
                ) : null}
                {/* <List.Item className="Filter_Sort">
                  <CandidateScreeningStatusFilter
                    attribute={"screeningStatus"}
                    label={"Interview Status"}
                    past
                  />
                  <List.Content />
                </List.Item> */}
              </List>
            </Grid.Column>

            <Grid.Column width="4" textAlign="right">
              <List className="List_Main">
                <List.Item className="Filter_Sort">
                  <List.Content>
                    <CandidateSortBy
                      defaultRefinement={"jobApplication_date"}
                      items={[
                        { value: "jobApplication", label: "Profile Score" },
                        { value: "jobApplication_date", label: "Latest" }
                      ]}
                    />
                  </List.Content>
                </List.Item>
              </List>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
      <div className="addTag_section">
        {/* <Button compact className="addTag_sectionBtn">
        Submitted: <span>Anytime</span>
        <IcCloseIcon pathcolor="#797979" width="10" height="10" />
      </Button> */}
        <FilterTags />
      </div>
    </div>
  );
};

export default withCurrentFilters(FilterBar);
