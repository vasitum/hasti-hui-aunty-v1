import React from "react";

import { List } from "semantic-ui-react";
import Slider from "react-slick";
import PropTypes from "prop-types";

import IcSliderRightArrow from "../../../../../assets/svg/IcSliderRightArrow";
import CandidateStatisticDropMenu from "../CandidateStatisticDropMenu";
import isEqual from "lodash.isequal";

import { withSelect } from "../../../../AlgoliaContainer";
import { RECR_STAGES } from "../../../../../constants/applicationStages";
import "./index.scss";

// const STAGES = {
//   NewApplicant: "NEW APPLICANT",
//   Seen: "SEEN",
//   Hold: "HOLD",
//   ShortList: "SHORTLIST",
//   Interview: "INTERVIEW",
//   Offer: "OFFER",
//   OfferDropOut: "OFFER DROPOUT",
//   Joinend: "JOINED",
//   Rejected: "REJECTED"
// };

function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      // style={{ ...style, display: "block", background: "red" }}
      onClick={onClick}>
      <IcSliderRightArrow pathcolor="#0b9ed0" />
    </div>
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      // style={{ ...style, display: "block", background: "green" }}
      onClick={onClick}>
      <IcSliderRightArrow pathcolor="#0b9ed0" />.
    </div>
  );
}

const StatisticSliderSettings = {
  className: "slider variable-width",
  dots: false,
  infinite: true,
  speed: 500,
  slidesToShow: 9,
  slidesToScroll: 1,
  variableWidth: true,
  arrows: true,
  nextArrow: <SampleNextArrow />,
  prevArrow: <SamplePrevArrow />
};

function processItems(stages, items, currentRefinement, onSelect) {
  const ret = [];
  let totalCount = 0;
  let isActiveFound = false;

  stages.map(stage => {
    // find the data
    const index = items.filter(val => val.label === stage);
    const active = currentRefinement === stage;
    if (active) {
      isActiveFound = true;
    }

    // check the data
    if (index.length > 0) {
      const data = index[0];

      // increase counter
      totalCount = totalCount + data.count;

      // push it!
      ret.push({
        title: RECR_STAGES[stage],
        number: data.count,
        stage: stage,
        active: active
      });

      return;
    }

    // unable to find it
    ret.push({
      title: RECR_STAGES[stage],
      number: 0,
      stage: stage,
      active: active
    });
  });

  ret.unshift({
    // title: (
    //   <CandidateStatisticDropMenu
    //     totalData={{
    //       title: "All",
    //       number: totalCount
    //     }}
    //     items={ret}
    //     currentRefinement={currentRefinement}
    //     refine={onSelect}
    //   />
    // ),
    title: "All",
    number: totalCount,
    active: !isActiveFound,
    stage: ""
  });

  // console.log(ret);
  return ret;
}

class CandidateStatistic extends React.Component {
  shouldComponentUpdate(nextProps) {
    if (isEqual(this.props, nextProps)) {
      return false;
    }

    return true;
  }

  render() {
    const { items, currentRefinement, onSelect } = this.props;
    const finalItems = processItems(
      Object.keys(RECR_STAGES),
      items,
      currentRefinement,
      onSelect
    );

    return (
      <div className="CandidateStatistic">
        {finalItems.map((item, idx) => {
          return (
            <div
              onClick={e => onSelect(item.stage)}
              key={idx}
              className="slider_list">
              <div
                key={idx}
                className={`slider_listItem ${item.active ? "active" : ""}`}>
                <div style={{ display: item.number < 0 ? "none" : "block" }}>
                  <p style={{ color: `${item.color}` }}>{item.number}</p>
                </div>

                {item.title}
              </div>
            </div>
          );
        })}
        {/* <Slider {...StatisticSliderSettings} className="padding-top-20">
        {statisticData.map((item, idx) => {
          return (
            <div key={item.title} className="slider_list">
              <div key={idx} className={`slider_listItem ${item.active}`}>
                <div style={{ display: !item.number ? "none" : "block" }}>
                  <p style={{ color: `${item.color}` }}>{item.number}</p>
                </div>

                {item.title}
              </div>
            </div>
          );
        })}
      </Slider> */}
      </div>
    );
  }
}

CandidateStatistic.propTypes = {
  // placeholder: PropTypes.string,
  // inputtype: PropTypes.string
};

CandidateStatistic.defaultProps = {
  // inputtype: "text"
};

export default withSelect(CandidateStatistic);
