import React from "react";
import { Grid, Header } from "semantic-ui-react";

import { Form } from "formsy-semantic-ui-react";

import SelectOptionField from "../../../../Forms/FormFields/SelectOptionField";
import getPlaceholder from "../../../../Forms/FormFields/Placeholder";
// import SearchIcon from "../../assets/svg/SearchIcon";
import CandidateSearchBox from "../CandidateSearchBox";
import "./index.scss";
import { withSelect } from "../../../../AlgoliaContainer";
import isEqual from "lodash.isequal";

class ShowApplicant extends React.PureComponent {
  // shouldComponentUpdate(nextProps) {
  //   if (isEqual(this.props, nextProps)) {
  //     return false;
  //   }

  //   return true;
  // }

  render() {
    const { items, currentRefinement, onSelect } = this.props;
    return (
      <div className="ShowApplicantMain_Grid">
        <div className="ShowApplicant_left">
          <Header as="h2">Showing applicants for</Header>
          <Form className="ShowApplicant_form">
            <SelectOptionField
              placeholder={getPlaceholder("All Jobs", "All Jobs")}
              name="showApplication"
              options={[].concat(
                {
                  text: "All jobs",
                  value: "",
                  key: "All Jobs"
                },
                items.map(item => {
                  return {
                    text: item.label,
                    value: item.label,
                    key: item.label
                  };
                })
              )}
              value={currentRefinement ? currentRefinement : ""}
              onChange={(e, { value }) => onSelect(value)}
            />
          </Form>
        </div>
        <div className="ShowApplicant_right">
          <CandidateSearchBox />
        </div>
        {/* <Grid className="Main_Grid_Show">
          <Grid.Row className="Main_Grid_Row">
            <Grid.Column width="5" className="applicant_column">
              <h2>Showing applicants for</h2>
            </Grid.Column>

            <Grid.Column
              width="4"
              textAlign="left"
              className="selectOption_column">
              <Form>
                <SelectOptionField
                  placeholder={getPlaceholder("All Jobs", "All Jobs")}
                  name="showApplication"
                  options={[].concat(
                    {
                      text: "All jobs",
                      value: "",
                      key: "All Jobs"
                    },
                    items.map(item => {
                      return {
                        text: item.label,
                        value: item.label,
                        key: item.label
                      };
                    })
                  )}
                  value={currentRefinement ? currentRefinement : ""}
                  onChange={(e, { value }) => onSelect(value)}
                />
              </Form>
            </Grid.Column>

            <Grid.Column width="3" />

            <Grid.Column width="4" textAlign="right">
              <CandidateSearchBox />
            </Grid.Column>
          </Grid.Row>
        </Grid> */}
      </div>
    );
  }
}

export default withSelect(ShowApplicant);
// export default ShowApplicant;
