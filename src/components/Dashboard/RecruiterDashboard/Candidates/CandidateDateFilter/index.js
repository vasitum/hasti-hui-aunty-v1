import React from "react";
import { Dropdown } from "semantic-ui-react";
import { withDateFilter } from "../../../../AlgoliaContainer";
import Flatpickr from "react-flatpickr";
import isEqual from "lodash.isequal";

import {
  addDays,
  // subDays,
  startOfDay,
  endOfDay,
  startOfWeek,
  startOfYesterday,
  endOfYesterday,
  startOfTomorrow,
  endOfTomorrow,
  getTime
} from "date-fns";

const dateConstants = {
  ALL: "ALL",
  TODAY: "TODAY",
  TOMMOROW: "TOMMOROW",
  YESTERDAY: "YESTERDAY",
  THIS_WEEK: "THIS_WEEK",
  LAST_WEEK: "LAST_WEEK",
  CUSTOM_SELECTION: "CUSTOM_SELECTION"
};

function getFinalRange(min = 0, max = 0) {
  return {
    min,
    max
  };
}

class CandidateDateFilter extends React.Component {
  // state, range
  state = {
    pickerDate: new Date(),
    from: 0,
    to: 0
  };

  shouldComponentUpdate(nextProps) {
    if (isEqual(this.props, nextProps)) {
      return false;
    }

    return true;
  }

  onRefine = data => {
    this.props.refine(data.min, data.max);
  };

  onPickerDateChange = date => {
    // console.log(date[0]);
    this.setState(
      {
        pickerDate: date
      },
      () => {
        const selectedDate = date[0];
        this.props.refine(
          getTime(startOfDay(selectedDate)),
          getTime(endOfDay(selectedDate))
        );
      }
    );
  };

  onChange = (e, { value }) => {
    this.setState(
      {
        currentValue: value
      },
      () => {
        let finalConstants;

        switch (value) {
          case dateConstants.ALL:
            finalConstants = getFinalRange();
            break;
          case dateConstants.TODAY:
            finalConstants = getFinalRange(
              getTime(startOfDay(new Date())),
              getTime(endOfDay(new Date()))
            );
            break;
          case dateConstants.TOMMOROW:
            finalConstants = getFinalRange(startOfTomorrow(), endOfTomorrow());
            break;
          case dateConstants.YESTERDAY:
            finalConstants = getFinalRange(
              startOfYesterday(),
              endOfYesterday()
            );
            break;
          case dateConstants.THIS_WEEK:
            finalConstants = getFinalRange(startOfWeek(new Date()), new Date());
            break;
          case dateConstants.LAST_WEEK:
            finalConstants = getFinalRange(startOfWeek(new Date()), new Date());
            break;
          case dateConstants.CUSTOM_SELECTION:
            break;
          default:
            break;
        }

        if (value !== dateConstants.CUSTOM_SELECTION) {
          const refinedConstants = {
            min: getTime(finalConstants.min),
            max: getTime(finalConstants.max)
          };

          // console.log(refinedConstants);
          this.onRefine(refinedConstants);
        }
      }
    );
  };

  getOptions = () => {
    const { past } = this.props;
    return [
      {
        text: "All",
        value: dateConstants.ALL
      },
      {
        text: "Today",
        value: dateConstants.TODAY
      },
      {
        text: past ? "Yesterday" : "Tommorow",
        value: past ? dateConstants.YESTERDAY : dateConstants.TOMMOROW
      },
      {
        text: "This Week",
        value: dateConstants.THIS_WEEK
      },
      {
        text: "Custom Date",
        value: dateConstants.CUSTOM_SELECTION,
        label: (
          <div
            style={{
              position: "absolute",
              top: "0",
              bottom: "0",
              left: "0",
              right: "0",
              zIndex: "1000",
              opacity: "0"
            }}>
            <Flatpickr
              onChange={this.onPickerDateChange}
              value={this.state.pickerDate}
              placeholder={"Select Date"}
            />
          </div>
        )
      }
    ];
  };

  render() {
    const { label } = this.props;
    const options = this.getOptions();
    return (
      <div>
        {label}:{" "}
        <Dropdown
          inline
          onChange={this.onChange}
          options={options}
          defaultValue={options[0].value}
        />
      </div>
    );
  }
}

export default withDateFilter(CandidateDateFilter);
