import React from "react";
import { Dropdown } from "semantic-ui-react";
import { withCustomFilter } from "../../../../AlgoliaContainer";
import isEqual from "lodash.isequal";
import { CUSTOM_TYPES as type } from "../../../../../constants/algolia";

class CandidateCustomFilter extends React.Component {
  state = {
    filter: "",
    currentValue: ""
  };

  shouldComponentUpdate(nextProps) {
    if (isEqual(this.props, nextProps)) {
      return false;
    }

    return true;
  }

  componentDidMount() {
    // console.log("curentValue");
    const { currentValue, currentDate, attribute } = this.props;
    // console.log("curentValue", currentValue);
    if (currentValue) {
      this.props.refine(`${attribute}>${currentDate}`);
    }
  }

  onRefine = filter => {
    const { options, attribute, currentDate } = this.props;

    if (!filter) {
      this.props.refine(``);
      return;
    }

    const refineType = (function(options) {
      for (let i = 0; i < options.length; i++) {
        const element = options[i];
        if (element.value === filter) {
          return element.refineType;
        }
      }

      return "";
    })(options);

    switch (refineType) {
      case type.NUMBER.LESS_THAN:
        this.props.refine(`${attribute}<${currentDate}`);
        break;

      case type.NUMBER.GREATER_THAN:
        this.props.refine(`${attribute}>${currentDate}`);
        break;

      case type.NUMBER.EQUALS:
        this.props.refine(`${attribute}=${currentDate}`);
        break;

      case type.STRING:
      default:
        this.props.refine(`${attribute}:${filter}`);
        break;
    }
  };

  onChange = (e, { value }) => {
    this.setState(
      {
        currentValue: value
      },
      () => {
        this.onRefine(value);
      }
    );
  };

  render() {
    const { label, options } = this.props;
    return (
      <div>
        {label}:{" "}
        <Dropdown
          inline
          onChange={this.onChange}
          options={options}
          placeholder={"Select"}
        />
      </div>
    );
  }
}

export default withCustomFilter(CandidateCustomFilter);
