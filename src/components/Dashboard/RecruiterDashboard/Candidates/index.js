import React from "react";

import { Container } from "semantic-ui-react";
import PropTypes from "prop-types";
import Loadable from "react-loadable";
import CandidateStatistic from "./CandidateStatistic";
import CandidateFilter from "./CandidateFilter";
import ShowApplicant from "./ShowApplicant";
import ReactGA from "react-ga";
// import ShowApplicantHeader from "../../Common/ShowApplicantHeader";

import CandidateCardContainer from "./CandidateCardContainer";
import MultiCheckPopup from "./MultipleChekPopup";

import { withSearch } from "../../../AlgoliaContainer";
import { withRouter } from "react-router-dom";

import "./index.scss";

class Candidates extends React.Component {
  constructor(props) {
    super(props);
    this.selectedItems = [];
  }

  state = {
    selectedItems: []
  };

  hasId = (applId, jobId) => {
    const apps = this.state.selectedItems.filter(val => {
      if (val.applicationId === applId && val.jobId === jobId) {
        return true;
      }

      return false;
    });

    if (apps && apps.length > 0) {
      return true;
    }

    return false;
  };

  onDeselectClick = e => {
    this.setState({
      selectedItems: []
    });
  };

  onItemSelect = (applId, jobId, candidateId) => {
    if (!this.hasId(applId, jobId)) {
      this.setState({
        selectedItems: [
          ...this.state.selectedItems,
          {
            applicationId: applId,
            jobId: jobId,
            receiverId: candidateId
          }
        ]
      });
    } else {
      this.setState({
        selectedItems: this.state.selectedItems.filter(val => {
          if (val.applicationId === applId && val.jobId === jobId) {
            return false;
          }

          return true;
        })
      });
    }

    // console.log(this.selectedItems);
  };

  componentDidMount() {
    ReactGA.pageview('/job/recruiter/candidates');
  }

  render() {
    const { selectedItemsChek } = this.props;
    // const { hits } = this.state;
    return (
      <Container>
        <div className="Candidates">
          <ShowApplicant attribute={"jobName"} />
          <CandidateStatistic attribute={"status"} />
          <CandidateFilter />
          <div>
            <CandidateCardContainer
              selectedItems={this.state.selectedItems}
              onCardSelect={this.onItemSelect}
            />
          </div>

          {/* TODO: Bulk Action Design */}
          {this.state.selectedItems.length > 0 ? (
            <MultiCheckPopup
              selectedItems={this.state.selectedItems}
              onDeselectClick={this.onDeselectClick}
            />
          ) : null}
        </div>
      </Container>
    );
  }
}

export default withRouter(
  withSearch(Candidates, {
    apiKey: process.env.REACT_APP_ALGOLIA_APPLICATION_API_KEY,
    appId: process.env.REACT_APP_ALGOLIA_APPLICATION_APP_ID,
    indexName: process.env.REACT_APP_ALGOLIA_APPLICATION_INDEX_NAME,
    noRouting: true
  })
);
