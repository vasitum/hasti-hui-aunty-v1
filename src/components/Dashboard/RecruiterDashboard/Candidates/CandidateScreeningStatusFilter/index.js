import React from "react";
import { Dropdown } from "semantic-ui-react";
import { withSelect } from "../../../../AlgoliaContainer";
import { SCREENING_STATUS } from "../../../../../constants/applicationStages";

const DataUpdate = ({ items, currentRefinement, onSelect, label }) => {
  return (
    <Dropdown
      onChange={(e, { value }) => onSelect(value)}
      placeholder={label}
      selection
      options={Object.keys(SCREENING_STATUS).map(val => {
        return {
          text: val,
          value: val,
          key: val
        };
      })}
      fluid
    />
  );
};

export default withSelect(DataUpdate);
