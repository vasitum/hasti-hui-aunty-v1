import React from "react";
import { Grid, Dropdown, Button, Modal } from "semantic-ui-react";

import IcMoreOptionIcon from "../../../../../assets/svg/IcMoreOptionIcon";

import getPlaceholder from "../../../../Forms/FormFields/Placeholder";
import IcDownArrow from "../../../../../assets/svg/IcDownArrow";
import RejectModal from "../../../RejectModal";

import ReportProfile from "../ReportProfile";

import AddTag from "../AddTag";

import MoreOption from "../../../Common/MoreOption";

import FlatDefaultBtn from "../../../../Buttons/FlatDefaultBtn";
import { RECR_STAGES } from "../../../../../constants/applicationStages";

import "./index.scss";

const CardOptions = [
  {
    text: getPlaceholder("Pending", "Pending applicants"),
    value: "Pending applicants"
  },

  {
    text: "Reject",
    value: "Reject"
  },
  {
    text: "Basic screening",
    value: "Basic screening"
  },
  {
    text: "Interview",
    value: "Interview"
  },
  {
    text: "Offer extended",
    value: "Offer extended"
  }
];

const MoreOptionsTrigger = (
  <span>
    <IcMoreOptionIcon pathcolor="#acaeb5" />
  </span>
);

const MoreOptions = [
  {
    text: "Share",
    value: "Share"
  },
  {
    text: (
      <Modal
        className="addTagModal_container"
        trigger={<span>Add tag</span>}
        size="small"
        closeIcon
        closeOnDimmerClick={false}>
        <AddTag />
      </Modal>
    ),
    value: "Add tag"
  },
  {
    text: (
      <Modal
        className="addTagModal_container"
        trigger={<span>Report this profile</span>}
        size="small"
        closeIcon
        closeOnDimmerClick={false}>
        <ReportProfile />
      </Modal>
    ),
    value: "Report this profile"
  }
];

class CandidateCardFooter extends React.Component {
  state = {
    statusModal: false,
    statusChanged: false,
    newStatus: ""
  };

  // STAGES = {
  //   NewApplicant: "NEW APPLICANT",
  //   Seen: "SEEN",
  //   Hold: "HOLD",
  //   ShortList: "SHORTLIST",
  //   Interview: "INTERVIEW",
  //   Offer: "OFFER",
  //   OfferDropOut: "OFFER DROPOUT",
  //   Joinend: "JOINED",
  //   Rejected: "REJECTED"
  // };

  onStatusModalOpen = e => {
    const { hit } = this.props;
    if (hit.status === "Yettoscreen") {
      return;
    }
    this.setState({ statusModal: true });
  };

  onStatusModalClose = e => {
    this.setState({ statusModal: false });
  };

  onStatusChange = status => {
    this.setState({
      statusChanged: true,
      newStatus: status,
      statusModal: false
    });
  };

  render() {
    const { hit } = this.props;
    return (
      <div className="CardStatusFooter CandidateCardFooter">
        <div className="CardStatusFooter_container">
          <div className="CardStatusFooter_left">
            <div className="footerTItle">
              <p>{getPlaceholder("Status", "Application current status")}</p>
            </div>
            <div className="CardStatusFooter_Modal">
              <Modal
                open={this.state.statusModal}
                onClose={this.onStatusModalClose}
                className="rejectModal_container"
                trigger={
                  <Button
                    className="CardFooter_btn"
                    onClick={this.onStatusModalOpen}>
                    {this.state.statusChanged
                      ? RECR_STAGES[this.state.newStatus]
                      : RECR_STAGES[hit.status]}
                    {hit.status === "Yettoscreen" ? null : (
                      <IcDownArrow pathcolor="#0b9ed0" height="6" />
                    )}
                  </Button>
                }
                closeIcon
                closeOnDimmerClick={false}>
                <RejectModal
                  onClose={this.onStatusModalClose}
                  onChange={this.onStatusChange}
                  status={
                    this.state.statusChanged ? this.state.newStatus : hit.status
                  }
                  jobId={hit.jobId}
                  applicationId={hit.objectID}
                />
              </Modal>
            </div>
          </div>
          <div className="CardStatusFooter_right">
            <MoreOption url={`/view/user/${hit.objectID}`} />
          </div>
        </div>
      </div>
      // <div className="Main_Card">

      //   <Grid className="Main_Card_Grid">
      //     <Grid.Row className="Main_Card_Grid_Row">
      //       <Grid.Column
      //         mobile={4}
      //         computer={5}
      //         className="mainCard_apllicationStatus"
      //         textAlign="right">
      //         <h4>{getPlaceholder("Status", "Application current status")}</h4>
      //       </Grid.Column>

      //       <Grid.Column
      //         mobile={12}
      //         computer={8}
      //         className="Basic_Section"
      //         textAlign="left">
      //         <Modal
      //           open={this.state.statusModal}
      //           onClose={this.onStatusModalClose}
      //           className="rejectModal_container"
      //           trigger={
      //             <Button onClick={this.onStatusModalOpen}>
      //               {this.state.statusChanged
      //                 ? RECR_STAGES[this.state.newStatus]
      //                 : RECR_STAGES[hit.status]}
      //             </Button>
      //           }
      //           closeIcon
      //           closeOnDimmerClick={false}>
      //           <RejectModal
      //             onClose={this.onStatusModalClose}
      //             onChange={this.onStatusChange}
      //             status={
      //               this.state.statusChanged ? this.state.newStatus : hit.status
      //             }
      //             jobId={hit.jobId}
      //             applicationId={hit.objectID}
      //           />
      //         </Modal>
      //       </Grid.Column>

      //       <Grid.Column
      //         computer={3}
      //         className="mobile hidden"
      //         textAlign="right"
      //         style={{
      //           display: "flex",
      //           alignItems: "center",
      //           justifyContent: "flex-end"
      //         }}>
      //         <Dropdown
      //           trigger={MoreOptionsTrigger}
      //           options={MoreOptions}
      //           pointing="right bottom"
      //           icon={null}
      //         />
      //       </Grid.Column>
      //     </Grid.Row>
      //   </Grid>
      // </div>
    );
  }
}

export default CandidateCardFooter;
