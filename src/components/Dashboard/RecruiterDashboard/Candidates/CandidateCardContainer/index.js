import React, { Component } from "react";
import { withHits } from "../../../../AlgoliaContainer";
import CandidateDetailCard from "../CandidateDetailCard";
import isEqual from "lodash.isequal";

import NoFoundMessageDashboard from "../../../Common/NoFoundMessageDashboard";

class CandidateCardContainer extends Component {
  shouldComponentUpdate(nextProps) {
    if (isEqual(this.props, nextProps)) {
      return false;
    }

    return true;
  }

  render() {
    const { hits, onCardSelect, noClick, selectedItems } = this.props;
    if (!hits || hits.length === 0) {
      return (
        <NoFoundMessageDashboard
          noFountTitle="No data found"
          noFountSubTitle="Candidates who've applied for jobs will show up here"
        />
      );
    }

    return hits.map(hit => {
      return (
        <CandidateDetailCard
          selectedItems={selectedItems}
          noClick={noClick}
          onItemSelect={onCardSelect}
          hit={hit}
        />
      );
    });
  }
}

export default withHits(CandidateCardContainer);
