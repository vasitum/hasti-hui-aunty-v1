import React from "react";
import "./index.scss";
import { Dropdown, Button, Grid, Modal } from "semantic-ui-react";
import RejectModal from "../../../RejectModal";
import BulkMessagingModal from "../../../BulkMessagingModal";
import ActionBtn from "../../../../Buttons/ActionBtn";

// const BulkOption = [
//   {
//     text: "Basic Screening",
//     value: "Basic Screening"
//   },
//   {
//     text: "Send Message",
//     value: "Send Message"
//   },

//   {
//     text: "Share Profiles",
//     value: "Share Profiles"
//   },

//   {
//     text: (
//       <Modal
//         className="rejectModal_container"
//         trigger={<span>Reject Profiles</span>}
//         closeIcon
//         closeOnDimmerClick={false}>
//         <RejectModal />
//       </Modal>
//     ),
//     value: "Reject Profiles"
//   },

//   {
//     text: "Invite to Apply",
//     value: "Invite to Apply"
//   },

//   {
//     text: "Offer",
//     value: "Offer"
//   },
//   {
//     text: "Interview",
//     value: "Interview"
//   },

//   {
//     text: "Pending Aplication",
//     value: "Pending Aplication"
//   }
// ];

/**
 * TODO:
 *  - Enable Change Status Modal
 *
 */
class MultipleChekPopup extends React.Component {
  state = {
    statusModal: false,
    bulkMessagingModal: false,
    statusChanged: false,
    newStatus: ""
  };

  STAGES = {
    NewApplicant: "NEW APPLICANT",
    Seen: "SEEN",
    ShortList: "SHORTLIST",
    Interview: "INTERVIEW",
    Offer: "OFFER",
    Joinend: "JOINED",
    OfferDropOut: "OFFER DROPOUT",
    Hold: "HOLD",
    Rejected: "REJECTED"
  };

  onStatusModalOpen = e => {
    this.setState({ statusModal: true });
  };

  onStatusModalClose = e => {
    this.setState({ statusModal: false });
  };

  onMessageModalOpen = e => {
    this.setState({ bulkMessagingModal: true });
  };

  onMessageModalClose = e => {
    this.setState({ bulkMessagingModal: false });
  };

  onStatusChange = status => {
    this.setState(
      {
        statusChanged: true,
        newStatus: status,
        statusModal: false
      },
      () => {
        window.location.reload();
      }
    );
  };

  render() {
    const { selectedItems, onDeselectClick, isMobileBulkAction } = this.props;
    // console.log(selectedItems);

    const BulkOption = [
      {
        text: (
          <Modal
            open={this.state.statusModal}
            onClose={this.onStatusModalClose}
            className="rejectModal_container"
            trigger={
              <Button onClick={this.onStatusModalOpen}>
                Change Aplication Status
              </Button>
            }
            closeIcon
            closeOnDimmerClick={false}>
            <RejectModal
              selectedItems={selectedItems}
              onClose={this.onStatusModalClose}
              onChange={this.onStatusChange}
            />
          </Modal>
        ),
        value: "Reject Profiles"
      },

      {
        text: (
          <Modal
            open={this.state.bulkMessagingModal}
            onClose={this.onMessageModalClose}
            className="rejectModal_container"
            trigger={
              <Button onClick={this.onMessageModalOpen}>
                Send Bulk Message
              </Button>
            }
            closeIcon
            closeOnDimmerClick={false}>
            <BulkMessagingModal
              selectedItems={selectedItems}
              onClose={this.onMessageModalClose}
              onChange={() => alert("Change Called")}
            />
          </Modal>
        ),
        value: "Invite to Apply"
      }
    ];
    return (
      <div className="MultipleChekPopup">
        {!isMobileBulkAction ? (
          <Grid>
            <Grid.Row>
              <Grid.Column width={16} textAlign="center">
                {/* <Dropdown
                className="BulkOptionMenu"
                placeholder="Bulk actions"
                selection
                pointing="top bottom"
                options={BulkOption}
              /> */}
                {/* <Button className="deselect_allBtn">Select all</Button>
              <Button className="deselect_allBtn">Deselect all</Button> */}
                <Modal
                  open={this.state.statusModal}
                  onClose={this.onStatusModalClose}
                  className="rejectModal_container"
                  trigger={
                    // <Button
                    //   onClick={this.onStatusModalOpen}
                    //   className="multiCheckBtn">
                    //   Change Aplication Status
                    // </Button>
                    <ActionBtn
                      actioaBtnText="Change Aplication Status"
                      className="multiCheckBtn"
                      onClick={this.onStatusModalOpen}
                    />
                  }
                  closeIcon
                  closeOnDimmerClick={false}>
                  <RejectModal
                    selectedItems={selectedItems}
                    onClose={this.onStatusModalClose}
                    onChange={this.onStatusChange}
                  />
                </Modal>

                <Modal
                  open={this.state.bulkMessagingModal}
                  onClose={this.onMessageModalClose}
                  className="rejectModal_container"
                  trigger={
                    // <Button
                    //   onClick={this.onStatusModalOpen}
                    //   className="multiCheckBtn">
                    //   Change Aplication Status
                    // </Button>
                    <ActionBtn
                      actioaBtnText="Send Bulk Message"
                      className="multiCheckBtn"
                      onClick={this.onMessageModalOpen}
                    />
                  }
                  closeIcon
                  closeOnDimmerClick={false}>
                  <BulkMessagingModal
                    selectedItems={selectedItems}
                    onClose={this.onMessageModalClose}
                    onChange={this.onMessageModalClose}
                  />
                </Modal>

                <Button onClick={onDeselectClick} className="deselect_allBtn">
                  Deselect All
                </Button>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        ) : (
          <div className="">
            <Dropdown
              className="BulkOptionMenu"
              // placeholder="Bulk actions"
              // selection
              trigger={<Button>Bulk actions</Button>}
              pointing="top"
              options={BulkOption}
            />
            <Button onClick={onDeselectClick} className="deselect_allBtn">
              Deselect All
            </Button>
          </div>
        )}
      </div>
    );
  }
}

export default MultipleChekPopup;
