import React from "react";
import { withHits } from "../../../../AlgoliaContainer";
import MyJobsCardsContainer from "../MyJobsCardsContainer";
import NoFoundMessageDashboard from "../../../Common/NoFoundMessageDashboard";

class MyJobCardHits extends React.Component {
  render() {
    const { hits } = this.props;
    if(!hits || hits.length === 0 ){
      return(
        <NoFoundMessageDashboard 
          noFountTitle="No data found"
          noFountSubTitle="Posting jobs will boost your credibility as a recruiter"
        />
      )
    }
    return hits.map(hit => {
      return (
        <MyJobsCardsContainer {...this.props} hit={hit} key={hit.objectID} />
      );
    });
  }
}

export default withHits(MyJobCardHits);
