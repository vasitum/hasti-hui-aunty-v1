import PropTypes from "prop-types";
import React from "react";
import FlatDefaultBtn from "../../../../Buttons/FlatDefaultBtn";
import QuillText from "../../../../CardElements/QuillText";
import AccordionCardContainer from "../../../../Cards/AccordionCardContainer";
// import MyJobsCardRecruiter from "../../../../Cards/MyJobCards/MyJobsCardRecruiter";
// import SkillListExpBtn from "../../../../CardElements/SkillListExpBtn";

import SkillComponent from "../../../../ModalPageSection/commonComponent/skills";

// import SkillList from "../../../../CardElements/SkillList";
import MoreOptionDropDown from "../../../../Dropdown/MoreOptionDropDown";
import InfoSection from "../../../../Sections/InfoSection";
import CardStatusFooter from "../../../Common/CardStatusFooter";
import MyJobCardStatusMore from "../../RecruiterDashbordMobile/RecruiterMyjobMobile/MyJobCardStatusMore";
import MyJobCardStatusContainer from "../MyJobCardStatusContainer";
import MyJobsCardListStatus from "../MyJobsCardListStatus";
import MyJobsCards from "../MyJobsCards";
import getShortListCount from "../../../../../utils/env/getShortlistCount";
import "./index.scss";

class RecommendedJobCard extends React.Component {
  render() {
    const {
      hit,
      isShowCardApplyContainer,
      isShowStausList,
      isShowMobileFooterbottom,
      myJobCardsWidthLeft,
      noClick,
      ...resProps
    } = this.props;

    return (
      <div className="RecommendedJobs RecruiterMyjobCard_container">
        <div className="RecommendedJobs_cardContainer ">
          {/* <AccordionCardContainer
            noClick={noClick}
            cardHeader={
              
            }>
            <div className="Recommended_jobBody">
              <div className="Recommended_jobBodyContainer">
                <div className="Recommended_jobBodyLeft" />
                <div className="Recommended_jobBodyright">
                  <div>
                    {hit.desc ? (
                      <InfoSection
                        headerSize="medium"
                        color="blue"
                        headerText="Job Description">
                        <QuillText readOnly value={hit.desc} />
                      </InfoSection>
                    ) : null}
                  </div>
                  <div>
                    {hit.skills && hit.skills.length > 0 ? (
                      <SkillComponent val={hit} />
                    ) : null}
                  </div>

                  <div className="Recommended_jobBodyFooter">
                    <FlatDefaultBtn btntext="View complete Job" />
                  </div>
                </div>
              </div>
            </div>
          </AccordionCardContainer> */}
          <MyJobsCards
            hit={hit}
            isShowMobileFooter={isShowMobileFooterbottom}
            {...resProps}
            isStausList={
              !isShowStausList ? (
                <MyJobsCardListStatus hit={hit} />
              ) : (
                <MyJobCardStatusMore hit={hit} />
              )
            }>
            {!isShowCardApplyContainer ? (
              <MyJobCardStatusContainer hit={hit} />
            ) : (
              <MoreOptionDropDown hit={hit} />
            )}
          </MyJobsCards>

          <div className="cardFooter">
            <CardStatusFooter
              jobId={hit.objectID}
              shortListCount={getShortListCount(hit.applicationStatus)}
              status={hit.status}
            />
          </div>
        </div>
      </div>
    );
  }
}

RecommendedJobCard.propTypes = {
  hit: PropTypes.string
};

RecommendedJobCard.defaultProps = {
  hit: {
    candidateSummary:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad ipsum dolor sit amet, consectetur adipisicing elit, sed do iusmod tempor incididunt ut labore minim veniam, quis nostru exercitation aliquip ex ea commodo consequat. See more",
    skills: ["html", "css", "javascript"]
  }
};

export default RecommendedJobCard;
