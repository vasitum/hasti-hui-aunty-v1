import React from "react";
import ShowApplicantHeader from "../../Common/ShowApplicantHeader";
import StatisticFIlter from "../../Common/StatisticFIlter";
import MyJobStatistic from "../../RecruiterDashboard/MyJobs/MyJobStatistic";
import MyJobCardHits from "./MyJobCardHits";
import { withSearch } from "../../../AlgoliaContainer";
import { withRouter } from "react-router-dom";
import CandidateCVFilter from "../Candidates/CandidateCVFilter";
import { USER } from "../../../../constants/api";
import ReactGA from "react-ga";
import { Container } from "semantic-ui-react";

class MyJobs extends React.Component {
  componentDidMount() {
    ReactGA.pageview('/job/recruiter/myjobs');
  }
  render() {
    return (
      <div>
        <Container>
          <ShowApplicantHeader />
          <MyJobStatistic attribute={"status"} />
          <StatisticFIlter />
          <CandidateCVFilter
            label={"Recruiter"}
            attribute="userId"
            defaultRefinement={true}
            value={window.localStorage.getItem(USER.UID)}
            style={{
              display: "none"
            }}
          />
          <MyJobCardHits />
        </Container>
      </div>
    );
  }
}

export default withRouter(
  withSearch(MyJobs, {
    apiKey: process.env.REACT_APP_ALGOLIA_API_KEY,
    appId: process.env.REACT_APP_ALGOLIA_APP_ID,
    indexName: "job",
    noRouting: true
  })
);
