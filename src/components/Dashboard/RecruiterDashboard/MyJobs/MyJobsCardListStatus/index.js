import React from "react";
import { RECR_STAGES } from "../../../../../constants/applicationStages";
function getSectionName(status) {
  switch (status) {
    case "NewApplicant":
      return "New applicants";

    case "Seen":
      return "Seen";

    case "Hold":
      return "Hold";

    case "Screening":
      return "Screening";

    case "ShortList":
      return "Shortlist";

    case "Interview":
      return "Interview";

    case "Joinend":
      return "Joined";

    case "OfferDropOut":
      return "Offer Dropout";

    case "Rejected":
      return "Rejected";

    case "Offer":
      return "Offer";

    default:
      return "NewApplicant";
  }
}

const MyJobsCardListStatus = ({ hit }) => {
  const ListStatusData = [
    {
      count: "12",
      title: "New applicants"
    },
    {
      count: "08",
      title: "Seen"
    },
    {
      count: "05",
      title: "Shortlist"
    },
    {
      count: "03",
      title: "Interview"
    },
    {
      count: "02",
      title: "Offer"
    },
    {
      count: "08",
      title: "Joined"
    },
    {
      count: "02",
      title: "Offer Dropout"
    },
    {
      count: "06",
      title: "Hold"
    },
    {
      count: "11",
      title: "Rejected"
    }
  ];
  return (
    <div className="cardHeader_listStatus">
      {hit.applicationStatus.map(val => {
        return (
          <div key={val.status} className="listMenu">
            <p className="count">{val.count}</p>
            <p className="title">{RECR_STAGES[val.status]}</p>
          </div>
        );
      })}
      {/* {ListStatusData.map((showData, idx) => {
        return (
          <div key={idx} className="listMenu">
            <p className="count">{showData.count}</p>
            <p className="title">{showData.title}</p>
          </div>
        );
      })} */}
    </div>
  );
};

export default MyJobsCardListStatus;
