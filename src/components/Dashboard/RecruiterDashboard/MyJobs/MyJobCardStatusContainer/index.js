import React from "react";
import fromNow from "../../../../../utils/env/fromNow";
import { Link } from "react-router-dom";

const MyJobCardStatusContainer = ({ hit }) => {
  return (
    <div className="CardApplyContainer">
      <div className="">
        <div className="MyJobCardStatus_posted">
          <p>
            {" "}
            Posted: <span>{fromNow(hit.datePosted)}</span>
          </p>
        </div>
        <div className="applicantTitle">
          {/* <Link
            to={`/job/recruiter/candidates&menu%5BjobName%5D=${encodeURIComponent(
              hit.title
            )}`}> */}

            <Link
              to={`/job/recruiter/candidates?menu%5BjobName%5D=${encodeURIComponent(
                hit.title
              )}&page=1`}>

              
            <p>
                Total applicants <span>{hit.appliCount}</span>
              </p>
            </Link>
        </div>
        </div>
      </div>
      );
    };
    
    export default MyJobCardStatusContainer;
