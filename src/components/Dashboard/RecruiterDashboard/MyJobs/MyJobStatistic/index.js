import React from "react";

import { List } from "semantic-ui-react";
import Slider from "react-slick";
import PropTypes from "prop-types";

import IcSliderRightArrow from "../../../../../assets/svg/IcSliderRightArrow";
// import CandidateStatisticDropMenu from "../CandidateStatisticDropMenu";
import isEqual from "lodash.isequal";

import { withSelect } from "../../../../AlgoliaContainer";
import "./index.scss";

const STAGES = {
  Draft: "DRAFT",
  Active: "ACTIVE",
  Close: "CLOSED"
  // Renewed: "RENEWED",
  // Paused: "PAUSED"
};

function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      // style={{ ...style, display: "block", background: "red" }}
      onClick={onClick}>
      <IcSliderRightArrow pathcolor="#0b9ed0" />
    </div>
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      // style={{ ...style, display: "block", background: "green" }}
      onClick={onClick}>
      <IcSliderRightArrow pathcolor="#0b9ed0" />.
    </div>
  );
}

const StatisticSliderSettings = {
  className: "slider variable-width",
  dots: false,
  infinite: true,
  speed: 500,
  slidesToShow: 9,
  slidesToScroll: 1,
  variableWidth: true,
  arrows: true,
  nextArrow: <SampleNextArrow />,
  prevArrow: <SamplePrevArrow />
};

function processItems(stages, items, currentRefinement, onSelect) {
  const ret = [];
  let totalCount = 0;
  let isActiveFound = false;

  stages.map(stage => {
    // find the data
    const index = items.filter(val => val.label === stage);
    const active = currentRefinement === stage;
    if (active) {
      isActiveFound = true;
    }

    // check the data
    if (index.length > 0) {
      const data = index[0];

      // increase counter
      totalCount = totalCount + data.count;

      // push it!
      ret.push({
        title: STAGES[stage],
        number: data.count,
        stage: stage,
        active: active
      });

      return;
    }

    // unable to find it
    ret.push({
      title: STAGES[stage],
      number: 0,
      stage: stage,
      active: active
    });
  });

  ret.unshift({
    // title: (
    //   <CandidateStatisticDropMenu
    //     totalData={{
    //       title: "All",
    //       number: totalCount
    //     }}
    //     items={ret}
    //     currentRefinement={currentRefinement}
    //     refine={onSelect}
    //   />
    // ),
    title: "All",
    active: !isActiveFound,
    number: totalCount,
    stage: ""
  });

  // console.log(ret);
  return ret.filter(item => item.stage !== "Rejected");
}

class MyJobStatistic extends React.PureComponent {
  shouldComponentUpdate(nextProps) {
    if (isEqual(this.props, nextProps)) {
      return false;
    }

    return true;
  }

  render() {
    const { items, currentRefinement, onSelect } = this.props;
    const finalItems = processItems(
      Object.keys(STAGES),
      items,
      currentRefinement,
      onSelect
    );

    // const finalItems = [
    //   {
    //     title: "Total",
    //     number: 52,
    //     stage: "total",
    //     active: true
    //   },
    //   {
    //     title: "Active",
    //     number: 12,
    //     stage: "active",
    //     active: false
    //   },
    //   {
    //     title: "Paused",
    //     number: "08",
    //     stage: "paused",
    //     active: false
    //   },
    //   {
    //     title: "Draft",
    //     number: "06",
    //     stage: "draft",
    //     active: false
    //   },
    //   {
    //     title: "Closed",
    //     number: "14",
    //     stage: "closed",
    //     active: false
    //   }
    // ];
    return (
      <div className="CandidateStatistic MyJobStatistic">
        {finalItems.map((item, idx) => {
          return (
            <div
              onClick={e => onSelect(item.stage)}
              key={idx}
              className="slider_list">
              <div
                key={idx}
                className={`slider_listItem ${item.active ? "active" : ""}`}>
                <div style={{ display: item.number < 0 ? "none" : "block" }}>
                  <p style={{ color: `${item.color}` }}>{item.number}</p>
                </div>

                {item.title}
              </div>
            </div>
          );
        })}
      </div>
    );
  }
}

MyJobStatistic.propTypes = {
  // placeholder: PropTypes.string,
  // inputtype: PropTypes.string
};

MyJobStatistic.defaultProps = {
  // inputtype: "text"
};

export default withSelect(MyJobStatistic);
