import React from "react";

import { Button, Dropdown, Modal } from "semantic-ui-react";

import EditJob from "../../../../ModalPageSection/EditJob";
import ShareModal from "../../../../ShareButton/ShareModal";
import IcMoreOptionIcon from "../../../../../assets/svg/IcMoreOptionIcon";
import IcEdit from "../../../../../assets/svg/IcEdit";

import { Link } from "react-router-dom";

import "./index.scss";

const MoreOptionsTrigger = (
  <Button compact>
    <IcMoreOptionIcon pathcolor="#acaeb5" />
  </Button>
);

class MobileMoreOptions extends React.Component {
  state = {
    editModal: false,
    modalOpen: false
  };

  getItems = (state, onOpen, onClose, id, data) => {
    return [
      {
        text: "Share",
        value: "Share",
        // onClick: () => this.props.onChangeSearchField()
        onClick: () => this.handleModal()
      },
      {
        text: <Link to={`/job/public/${id}`}>Public View</Link>,
        value: "Public View"
        // onClick: () => this.props.onChangeSearchField()
      },
      {
        text: <Link to={`/job/edit/${id}`}>Edit Job</Link>,
        value: "Edit Job"
        // onClick: () => this.props.onChangeSearchField()
      },
      {
        text: <Link to={`/job/edit/screening/${id}`}>Edit Screening</Link>,
        value: "Edit Screening"
        // onClick: () => this.props.onChangeSearchField()
      }
      // {
      //   text: (
      //     <Modal
      //       className="modal_form"
      //       open={state}
      //       onClose={onClose}
      //       trigger={
      //         <Button onClick={onOpen} className="jobEditBtn">
      //           Edit job
      //         </Button>
      //       }
      //       closeIcon
      //       closeOnDimmerClick={false}>
      //       <EditJob onCloseClick={onClose} />
      //     </Modal>
      //   ),
      //   value: "Share"
      //   // onClick: () => this.props.onChangeSearchField()
      // }
    ];
  };
  handleModal = () => this.setState({ modalOpen: !this.state.modalOpen });
  onEditModalOpen = e => {
    this.setState({ editModal: true });
  };

  onEditModalClose = e => {
    this.setState({ editModal: false });
  };

  render() {
    const { id } = this.props;

    return (
      <div className="MyJobDetailMobileMoreOption">
        <Dropdown
          trigger={MoreOptionsTrigger}
          options={this.getItems(
            this.state.editModal,
            this.onEditModalOpen,
            this.onEditModalClose,
            id
          )}
          icon={null}
          pointing="right"
        />
        <ShareModal
          modalOpen={this.state.modalOpen}
          handleModal={this.handleModal}
          url={`view/job/${id}`}
        />
      </div>
    );
  }
}

export default MobileMoreOptions;
