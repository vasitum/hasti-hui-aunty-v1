import React from "react";
import { Button, Modal, Container } from "semantic-ui-react";
import MobileHeader from "../../../MobileComponents/MobileHeader";
import MobileFooter from "../../../MobileComponents/MobileFooter";

import IcFooterArrowIcon from "../../../../assets/svg/IcFooterArrowIcon";

import CandidatDetailMoreOption from "../CandidatesDetailpage/CandidatDetailMoreOption";

import MyJobsDetailsHeader from "../MyJobsDetails/MyJobsDetailsHeader";

import MyJobsDetailsBodyContainer from "../MyJobsDetails/MyJobsDetailsBodyContainer";
import StatusMyJob from "../../Common/StatusMyJob";
import MyJobCardStatusMore from "../RecruiterDashbordMobile/RecruiterMyjobMobile/MyJobCardStatusMore";

import MobileMoreOptions from "./MobileMoreOptions";

import IcDownArrow from "../../../../assets/svg/IcDownArrow";

import { withRouter } from "react-router-dom";

import "./index.scss";

class FooterStatus extends React.Component {
  state = {
    statusModal: false,
    statusChanged: false,
    newStatus: ""
  };

  onStatusModalOpen = e => {
    this.setState({ statusModal: true });
  };

  onStatusModalClose = e => {
    this.setState({ statusModal: false });
  };

  onStatusChange = status => {
    this.setState({
      statusChanged: true,
      newStatus: status,
      statusModal: false
    });
  };

  render() {
    const { data } = this.props;
    return (
      <div className="mobileFooter_status">
        <p>Status</p>
        <Modal
          open={this.state.statusModal}
          onClose={this.onStatusModalClose}
          className="rejectModal_container"
          trigger={
            <Button onClick={this.onStatusModalOpen}>
              {this.state.statusChanged ? this.state.newStatus : data.status}
              <IcDownArrow pathcolor="#6e768a"/>
            </Button>
          }
          closeIcon
          closeOnDimmerClick={false}>
          <StatusMyJob
            onChange={this.onStatusChange}
            onClose={this.onStatusModalClose}
            status={
              this.state.statusChanged ? this.state.newStatus : data.status
            }
            jobId={data._id}
          />
        </Modal>
      </div>
    );
  }
}

class MyJobsDetailsMobile extends React.Component {
  render() {
    const { data, user } = this.props;

    return (
      <div className="MyJobsDetailsMobile">
        <div className="MyJobsDetailsMobile_header">
          <MobileHeader
            mobileHeaderLeftColumn="14"
            mobileHeaderRightColumn="2"
            className="isMobileHeader_fixe"
            headerLeftIcon={
              <Button
                onClick={e => this.props.history.go(-1)}
                className="backArrowBtn">
                <IcFooterArrowIcon pathcolor="#6e768a" />
              </Button>
            }
            headerTitle={data.title}>
            <MobileMoreOptions id={data._id} />
          </MobileHeader>
        </div>
        <div className="MyJobsDetailsMobile_body">
          <Container>
            <MyJobsDetailsHeader
              HeaderDetailsLeftColummn="16"
              HeaderDetailsRightColummn="16"
              data={data}>
              {/* <MyJobsDetailsHeaderStatus 
                isShowHeaderStatus
                isShowCount
              /> */}

              <MyJobCardStatusMore hit={data} />
            </MyJobsDetailsHeader>

            <div>
              <MyJobsDetailsBodyContainer data={data} />
            </div>
          </Container>
        </div>
        <div className="MyJobsDetailsMobile_footer">
          <MobileFooter
            className="isMobileFooter_fixe"
            isShowHeaderTitle
            mobileFooterLeftColumn="16"
            headerLeftIcon={<FooterStatus data={data} />}
          />
        </div>
      </div>
    );
  }
}

export default withRouter(MyJobsDetailsMobile);
