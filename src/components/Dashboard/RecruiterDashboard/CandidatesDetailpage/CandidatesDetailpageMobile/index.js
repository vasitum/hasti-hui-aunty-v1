import React from "react";

import { Image, Button, Modal } from "semantic-ui-react";

import MobileHeader from "../../../../MobileComponents/MobileHeader";
import IcFooterArrowIcon from "../../../../../assets/svg/IcFooterArrowIcon";
import UserLogo from "../../../../../assets/img/people.png";
import CandidatDetailMoreOption from "../CandidatDetailMoreOption";

import CondidateDetailPageContainer from "../CondidateDetailPageContainer";

import MobileFooter from "../../../../MobileComponents/MobileFooter";
import RejectModal from "../../../RejectModal";

import IcDownArrow from "../../../../../assets/svg/IcDownArrow";

import IcUser from "../../../../../assets/svg/IcUser";

import { withRouter } from "react-router-dom";
import { RECR_STAGES } from "../../../../../constants/applicationStages";
import "./index.scss";

class FooterStatus extends React.Component {
  state = {
    statusModal: false,
    statusChanged: false,
    newStatus: ""
  };

  STAGES = RECR_STAGES;

  onStatusModalOpen = e => {
    const { data } = this.props;
    if (data.status === "Yettoscreen") {
      return;
    }
    this.setState({ statusModal: true });
  };

  onStatusModalClose = e => {
    this.setState({ statusModal: false });
  };

  onStatusChange = status => {
    this.setState({
      statusChanged: true,
      newStatus: status,
      statusModal: false
    });
  };

  render() {
    const { data } = this.props;

    return (
      <div className="mobileFooter_status">
        <p>Status</p>
        <Modal
          open={this.state.statusModal}
          onClose={this.onStatusModalClose}
          className="rejectModal_container"
          trigger={
            <Button onClick={this.onStatusModalOpen}>
              {this.state.statusChanged
                ? this.STAGES[this.state.newStatus]
                : this.STAGES[data.status]}

              {data.status === "Yettoscreen" ? null : (
                <IcDownArrow pathcolor="#6e768a" />
              )}
            </Button>
          }
          closeIcon
          closeOnDimmerClick={false}>
          <RejectModal
            onClose={this.onStatusModalClose}
            onChange={this.onStatusChange}
            status={
              this.state.statusChanged ? this.state.newStatus : data.status
            }
            jobId={data.pId}
            applicationId={data._id}
          />
        </Modal>
      </div>
    );
  }
}

const HeaderLeft = ({ history, data }) => {
  return (
    <div className="mobileHeaderLogo">
      <Button onClick={e => history.go(-1)} className="backArrowBtn">
        <IcFooterArrowIcon pathcolor="#6e768a" />
      </Button>
      {!data.candidateImageExt ? (
        <IcUser
          rectcolor="#ffffff"
          pathcolor="#c8c8c8"
          height="27"
          width="27"
        />
      ) : (
        <Image
          src={`https://s3-us-west-2.amazonaws.com/img-mwf/${
            data.candidateId
          }/image.jpg`}
          style={{
            borderRadius: "50%"
          }}
        />
      )}
    </div>
  );
};

const HeaderLeftRouted = withRouter(HeaderLeft);

class CandidatesDetailpageMobile extends React.Component {
  render() {
    const { data } = this.props;

    return (
      <div className="CandidatesDetailpageMobile">
        <div className="CandidatesDetail_Header">
          <MobileHeader
            mobileHeaderLeftColumn="13"
            mobileHeaderRightColumn="3"
            className="isMobileHeader_fixe"
            headerLeftIcon={<HeaderLeftRouted data={data} />}
            // headerTitle={data.candidateName}
          >
            <CandidatDetailMoreOption />
          </MobileHeader>
        </div>

        <div className="CandidatesDetail_Body">
          <CondidateDetailPageContainer data={data} isCondidateApplyHeader />
        </div>

        <div className="CandidatesDetail_Footer">
          <MobileFooter
            isShowHeaderTitle
            mobileFooterLeftColumn="16"
            className="isMobileFooter_fixe"
            headerLeftIcon={<FooterStatus data={data} />}
          />
        </div>
      </div>
    );
  }
}

export default CandidatesDetailpageMobile;
