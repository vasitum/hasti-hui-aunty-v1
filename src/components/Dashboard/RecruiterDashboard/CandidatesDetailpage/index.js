import React from "react";
import { Responsive } from "semantic-ui-react";
import CandidatesDetailpageMobile from "./CandidatesDetailpageMobile";
import CondidateDetailPageContainer from "./CondidateDetailPageContainer";
import withApplications from "../../../wrappers/withApplication";
import ReactGA from "react-ga";
import "./index.scss";

class CandidatesDetailpage extends React.Component {

componentDidMount() {
  const { match } = this.props;
  // console.log("check url",  match.url);
  ReactGA.pageview(`${match.url}`);
}
  render() {
    const { data } = this.props;

    return (
      <div>
        <Responsive maxWidth={1024}>
          <CandidatesDetailpageMobile data={data} />
        </Responsive>

        <Responsive minWidth={1025}>
          <CondidateDetailPageContainer data={data} />
        </Responsive>
      </div>
    );
  }
}

export default withApplications(CandidatesDetailpage, { connected: true });
