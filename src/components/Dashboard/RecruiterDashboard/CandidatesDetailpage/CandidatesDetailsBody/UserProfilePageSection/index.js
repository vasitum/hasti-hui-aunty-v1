import PropTypes from "prop-types";
import React from "react";
import { Button } from "semantic-ui-react";
import InfoSectionGridHeader from "../../../../../CardElements/InfoSectionGridHeader";
import QuillText from "../../../../../CardElements/QuillText";
import UserEducation from "../../../../../CardElements/UserEducation";
import SkillListExpBtn from "../../../../../CardElements/SkillListExpBtn";
import UserExperience from "../../../../../CardElements/UserExperience";
import withProfile from "../../../../../wrappers/withProfile";
import { Link } from "react-router-dom";
import Currency from "../../../../../CardElements/Currency";

import "./index.scss";

const UserProfilePageSection = props => {
  const { data } = props;
  if (!data) {
    return null;
  }

  // console.log("check designation", data.exps.designation);

  return (
    <div className="UserProfilePageSection">
      <div className="description_container">
        <div className="description">
          {data.title ? (
            <p className="descTitle">
              Title: <span>{data.title}</span>
            </p>
          ) : null}

          <div
            className="menu-list"
            style={{
              display:
                data.desireJob && data.desireJob.currentCtc ? "block" : "none"
            }}>
            {/* <p className="">
              Experience:{" "}
              <span>
                {data.exp ? data.exp.min : ""} - {data.exp ? data.exp.max : ""}{" "}
                yrs
              </span>
            </p>
            <p className="">
              Job type: <span>{data.jobType}</span>
            </p> */}
            <p className="">
              Salary:{" "}
              <span>
                <Currency data={data} />
                {data.desireJob ? data.desireJob.currentCtc : ""}{" "}
                {data.salary
                  ? data.salary.currency && data.salary.currency !== "INR"
                    ? "k per annum"
                    : " LPA"
                  : ""}
              </span>
            </p>
          </div>
        </div>
        <div className="downloadCVBtn">
          <Button
            className="publicProfileBtn"
            as={Link}
            to={`/user/public/${data._id}`}>
            Public Profile
          </Button>
          {data.ext && data.ext.resume ? (
            <Button
              className="cvBtn"
              as={"a"}
              href={`https://s3-us-west-2.amazonaws.com/res-mwf/${data._id}/${
                data.fName
              }_${data.lName}.${data.ext.resume}`}>
              Download CV
            </Button>
          ) : null}
        </div>
      </div>

      <div className="Userdedail_infoHeader">
        <InfoSectionGridHeader headerText="Summary">
          <QuillText value={data.desc ? data.desc : ""} readOnly />
        </InfoSectionGridHeader>

        {/* {!data ? ():null} */}
        {/* style={{
                  display:
                    !user.exps || !user.exps.length || !user.exps[0].name
                      ? "none"
                      : "block"
                }} */}

        {data.skills.length === 0 ? null : (
          <InfoSectionGridHeader headerText="Skills">
            <SkillListExpBtn skills={data.skills} />
          </InfoSectionGridHeader>
        )}

        {/* !education || education.length === 0 */}

        {data.exps.length === 1 &&
        !data.exps[0].doj.month &&
        !data.exps[0].dor.month ? null : (
          <InfoSectionGridHeader headerText="Experience">
            <UserExperience experience={data.exps} />
          </InfoSectionGridHeader>
        )}

        {data.edus.length === 1 &&
        !data.edus[0].course &&
        !data.edus[0].institute ? null : (
          <InfoSectionGridHeader headerText="Education">
            <UserEducation education={data.edus} />
          </InfoSectionGridHeader>
        )}
      </div>
    </div>
  );
};

UserProfilePageSection.PropsType = {
  data: PropTypes.string
};

UserProfilePageSection.defaultProps = {
  data: {
    role: "Software developer",
    exp: {
      min: "3",
      max: "5"
    },
    jobType: "Full time",
    salary: {
      min: "3",
      max: "5"
    },
    desc:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad ipsum dolor sit amet, consectetur adipisicing elit, sed do iusmod tempor incididunt ut labore minim veniam, quis nostru exercitation aliquip ex ea commodo consequat."
  }
};

export default withProfile(UserProfilePageSection, { connected: false });
