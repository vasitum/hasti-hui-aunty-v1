import React from "react";

import { Tab, Menu } from "semantic-ui-react";

import UserProfilePageSection from "./UserProfilePageSection";
import DashboardMessaging from "../../../Common/DashboardMessaging";
import InterViewPageSection from "../../../Common/InterViewPageSection";
import ScreeningPageSection from "../../../Common/ScreeningPageSection";
import NoInterviewScheduled from "../../../Common/NoInterviewScheduled";
import ActivitiesPageSection from "../../../Common/ActivitiesPageSection";

import getActiveTab from "../../../../../utils/ui/getActiveTab";
import qs from "qs";

import "./index.scss";

function getDetailPage({ userId, jobId, applicationId, applicationData }) {
  return [
    {
      menuItem: <Menu.Item>Profile</Menu.Item>,
      render: () => {
        return (
          <div className="detailPageTab">
            <Tab.Pane attached={false}>
              <UserProfilePageSection id={userId} />
            </Tab.Pane>
          </div>
        );
      }
    },

    {
      menuItem: <Menu.Item>Screening</Menu.Item>,
      render: () => {
        return (
          <div className="detailPageTab">
            <Tab.Pane attached={false}>
              <ScreeningPageSection
                userId={userId}
                applicationData={applicationData}
              />
            </Tab.Pane>
          </div>
        );
      }
    },

    {
      menuItem: <Menu.Item>Messages</Menu.Item>,
      render: () => {
        return (
          <div className="detailPageTab">
            <Tab.Pane attached={false}>
              <DashboardMessaging
                userId={userId}
                applicationData={applicationData}
              />
            </Tab.Pane>
          </div>
        );
      }
    },

    {
      menuItem: <Menu.Item>Interview</Menu.Item>,
      render: () => {
        return (
          <div className="detailPageTab">
            <Tab.Pane attached={false}>
              <InterViewPageSection
                jobId={jobId}
                applicationId={applicationId}
                applicationData={applicationData}
              />
              {/* <InterViewPageSection /> */}
              {/* <NoInterviewScheduled /> */}
            </Tab.Pane>
          </div>
        );
      }
    },
    {
      menuItem: <Menu.Item>Activities</Menu.Item>,
      render: () => {
        return (
          <div className="detailPageTab">
            <Tab.Pane attached={false}>
              <ActivitiesPageSection
                jobId={jobId}
                applicationId={applicationId}
              />
            </Tab.Pane>
          </div>
        );
      }
    }
  ];
}

class CandidatesDetailsBody extends React.Component {
  state = {
    currentIndex: 0
  };

  componentDidMount() {
    const parsedQs = qs.parse(window.location.search);
    if (parsedQs && parsedQs["?screen"]) {
      const screen = parsedQs["?screen"];
      const activeIndex = getActiveTab(screen, {
        profile: 0,
        screening: 1,
        messaging: 2,
        interview: 3,
        activities: 4
      });
      this.setState({ currentIndex: activeIndex });
    }

    return "";
  }

  render() {
    const { data } = this.props;
    return (
      <div className="CandidatesDetailsBody">
        <div className="detailsBody_container">
          <Tab
            activeIndex={this.state.currentIndex}
            menu={{ text: true }}
            onTabChange={(e, { activeIndex }) =>
              this.setState({ currentIndex: activeIndex })
            }
            panes={getDetailPage({
              applicationId: data._id,
              jobId: data.pId,
              userId: data.candidateId,
              applicationData: data
            })}
          />
        </div>
      </div>
    );
  }
}

export default CandidatesDetailsBody;
