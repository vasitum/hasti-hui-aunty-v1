import React from "react";

import { Container } from "semantic-ui-react";

import CandidatesDetailsHeader from "../CandidatesDetailsHeader";
import CandidateApplyHeader from "../CandidateApplyHeader";

import CandidatesDetailsBody from "../CandidatesDetailsBody";

const MobileProfileIncrese = ({ data }) => {
  return (
    <div className="MobileProfileIncrese">
      <p className="Increse">{data.matchProfile}%</p>
      <p className="match">Matched</p>
    </div>
  );
};

class CondidateDetailPageContainer extends React.Component {
  render() {
    const { isCondidateApplyHeader, data } = this.props;

    return (
      <div className="CondidateDetailPageContainer">
        <div className="CandidatesDetailpage">
          <Container className="CandidatesDetailpage_Header">
            <CandidatesDetailsHeader data={data}>
              {!isCondidateApplyHeader ? (
                <CandidateApplyHeader data={data} />
              ) : (
                  <MobileProfileIncrese data={data} />
                )}
            </CandidatesDetailsHeader>
          </Container>
          <Container className="CandidatesDetailpage_Body">
            <CandidatesDetailsBody data={data} />
          </Container>
        </div>
      </div>
    );
  }
}

export default CondidateDetailPageContainer;
