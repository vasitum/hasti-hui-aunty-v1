import React from "react";

import { Header, Grid, Image } from "semantic-ui-react";

import PropTypes from "prop-types";

import IcJobThumb from "../../../../../assets/img/people.png";
import IcFooterArrowIcon from "../../../../../assets/svg/IcFooterArrowIcon";

import IcUser from "../../../../../assets/svg/IcUser";

import { withRouter } from "react-router-dom";

import "./index.scss";

class CandidatesDetailsHeader extends React.Component {
  render() {
    const { children, data, onBackClick } = this.props;

    if (!data) {
      return null;
    }

    return (
      <div className="CandidatesDetailsHeader statusSubmit">
        <div className="CandidatesDetail_headerBody">
          <div className="jobDefault_card">
            <Grid>
              <Grid.Row>
                <Grid.Column
                  width={11}
                  className="CandidatesDetailsHeader_leftSide">
                  <div className="back_arrow">
                    <IcFooterArrowIcon
                      onClick={e => this.props.history.go(-1)}
                      pathcolor="#acaeb5"
                    />
                  </div>
                  <div className="companyLeftDetail">
                    <div className="companyLogo">
                      {/* <IcUser rectcolor="#f7f7fb" pathcolor="#c8c8c8"/> */}
                      {// TODO: Use Default Candidate Icon
                      !data.candidateImageExt ? (
                        <IcUser
                          rectcolor="#ffffff"
                          pathcolor="#c8c8c8"
                          height="50"
                          width="50"
                        />
                      ) : (
                        <Image
                          src={`https://s3-us-west-2.amazonaws.com/img-mwf/${
                            data.candidateId
                          }/image.jpg`}
                          style={{
                            borderRadius: "50%"
                          }}
                        />
                      )}
                    </div>
                    <div className="profileIncrese">
                      <p className="increse">{data.matchProfile}%</p>
                      <p className="match">matched</p>
                    </div>
                  </div>
                  <div className="companyRightdetails">
                    <div className="headTitle">
                      <Header as="h2">{data.candidateName}</Header>
                    </div>
                    <div className="subTitle">
                      <p>
                        {/* <span>{data.com.name}</span> at{" "} */}
                        {data.candidateLocation}
                      </p>

                      <p>
                        <span>{data.candidatePhone}</span> |{" "}
                        <span>{data.candidateEmail}</span>
                      </p>
                      <p>
                        Applied: <span>{data.jobName}</span>
                      </p>
                    </div>
                  </div>
                </Grid.Column>
                <Grid.Column
                  width={5}
                  className="CandidatesDetailsHeader_rightSide">
                  {children}
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </div>
        </div>
      </div>
    );
  }
}

CandidatesDetailsHeader.propTypes = {
  data: PropTypes.string
};

CandidatesDetailsHeader.defaultProps = {
  data: {
    title: "Ruby",
    com: {
      name: "Maven"
    },
    loc: {
      adrs: "Noida, India"
    },
    num: "+1 732 000 4444",
    email: "ruby20@gmail.com",
    des: "Software developer",
    increse: "80%",
    match: "matched"
  }
};

export default withRouter(CandidatesDetailsHeader);
