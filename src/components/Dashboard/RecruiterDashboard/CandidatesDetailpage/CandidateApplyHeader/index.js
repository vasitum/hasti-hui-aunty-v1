import React from "react";

import "./index.scss";
import { Button, Modal } from "semantic-ui-react";
// import StatusMyJob from "../../../Common/StatusMyJob";
import RejectModal from "../../../RejectModal";
import fromNow from "../../../../../utils/env/fromNow";
import { RECR_STAGES } from "../../../../../constants/applicationStages";

class CandidateApplyHeader extends React.Component {
  state = {
    statusModal: false,
    statusChanged: false,
    newStatus: ""
  };

  STAGES = {
    NewApplicant: "NEW APPLICANT",
    Seen: "SEEN",
    ShortList: "SHORTLIST",
    Interview: "INTERVIEW",
    Offer: "OFFER",
    Joinend: "JOINED",
    OfferDropOut: "OFFER DROPOUT",
    Hold: "HOLD",
    Rejected: "REJECTED"
  };

  onStatusModalOpen = e => {
    const { data } = this.props;
    if (data.status === "Yettoscreen") {
      return;
    }
    this.setState({ statusModal: true });
  };

  onStatusModalClose = e => {
    this.setState({ statusModal: false });
  };

  onStatusChange = status => {
    this.setState({
      statusChanged: true,
      newStatus: status,
      statusModal: false
    });
  };

  render() {
    const { isCondidateApplyHeader, data } = this.props;
    return (
      <div className="CandidateApplyHeader">
        <div className="candidateApplyStatus">
          <p className="submit">
            Submitted: <span>{fromNow(data.appliedDate)}</span>
          </p>
        </div>
        <div className="candidateApplyStatusBtn">
          <p>Application current status</p>
          <Modal
            open={this.state.statusModal}
            onClose={this.onStatusModalClose}
            className="rejectModal_container"
            trigger={
              <Button onClick={this.onStatusModalOpen} compact>
                {this.state.statusChanged
                  ? RECR_STAGES[this.state.newStatus]
                  : RECR_STAGES[data.status]}
              </Button>
            }
            closeIcon
            closeOnDimmerClick={false}>
            <RejectModal
              onClose={this.onStatusModalClose}
              onChange={this.onStatusChange}
              status={
                this.state.statusChanged ? this.state.newStatus : data.status
              }
              jobId={data.pId}
              applicationId={data._id}
            />
          </Modal>
        </div>
      </div>
    );
  }
}

export default CandidateApplyHeader;
