import React from "react";

import { List } from "semantic-ui-react";
import Slider from "react-slick";
import PropTypes from "prop-types";

import { Link } from "react-router-dom";

import IcSliderRightArrow from "../../../../../assets/svg/IcSliderRightArrow";
import CandidateStatisticDropMenu from "../../Candidates/CandidateStatisticDropMenu";
import isEqual from "lodash.isequal";
import { RECR_STAGES } from "../../../../../constants/applicationStages";

// import { withSelect } from "../../../../AlgoliaContainer";
import "./index.scss";

const STAGES = RECR_STAGES;

function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      // style={{ ...style, display: "block", background: "red" }}
      onClick={onClick}>
      <IcSliderRightArrow pathcolor="#0b9ed0" />
    </div>
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      // style={{ ...style, display: "block", background: "green" }}
      onClick={onClick}>
      <IcSliderRightArrow pathcolor="#0b9ed0" />.
    </div>
  );
}

const StatisticSliderSettings = {
  className: "slider variable-width",
  dots: false,
  infinite: true,
  speed: 500,
  slidesToShow: 9,
  slidesToScroll: 1,
  variableWidth: true,
  arrows: true,
  nextArrow: <SampleNextArrow />,
  prevArrow: <SamplePrevArrow />
};

function processItems(stages, items, currentRefinement, onSelect) {
  const ret = [];
  let totalCount = 0;
  let isActiveFound = false;

  stages.map(stage => {
    // find the data
    const index = items.filter(val => val.label === stage);
    const active = currentRefinement === stage;
    if (active) {
      isActiveFound = true;
    }

    // check the data
    if (index.length > 0) {
      const data = index[0];

      // increase counter
      totalCount = totalCount + data.count;

      // push it!
      ret.push({
        title: STAGES[stage],
        number: data.count,
        stage: stage,
        active: active
      });

      return;
    }

    // unable to find it
    ret.push({
      title: STAGES[stage],
      number: 0,
      stage: stage,
      active: active
    });
  });

  ret.unshift({
    title: (
      <CandidateStatisticDropMenu
        totalData={{
          title: "All",
          number: totalCount
        }}
        items={ret}
        currentRefinement={currentRefinement}
        refine={onSelect}
      />
    ),
    // title: "All",
    active: !isActiveFound,
    stage: ""
  });

  // console.log(ret);
  return ret.filter(item => item.stage !== "Rejected");
}

class MyJobsDetailsStatistic extends React.Component {
  shouldComponentUpdate(nextProps) {
    if (isEqual(this.props, nextProps)) {
      return false;
    }

    return true;
  }

  render() {
    const { data } = this.props;
    // const finalItems = processItems(
    //   Object.keys(STAGES),
    //   items,
    //   currentRefinement,
    //   onSelect
    // );

    if (!data) {
      return null;
    }

    return (
      <div className="CandidateStatistic MyJobsDetailsStatistic">
        {data.applicationStatus
          ? data.applicationStatus.map((item, idx) => {
              return (
                <Link
                  title={item.count > 0 ? "" : "No Applications Found"}
                  to={`/job/recruiter/candidates?menu%5Bstatus%5D=${
                    item.status
                  }&menu%5BjobName%5D=${encodeURIComponent(data.title)}&page=1`}
                  style={{
                    pointerEvents: item.count > 0 ? "all" : "none"
                  }}>
                  <div
                    // onClick={e => onSelect(item.stage)}
                    key={idx}
                    className="slider_list">
                    <div
                      key={idx}
                      className={`slider_listItem ${
                        item.active ? "active" : ""
                      }`}>
                      <div
                        className="count"
                        style={{ display: item.count < 0 ? "none" : "block" }}>
                        <p>{item.count}</p>
                      </div>

                      {STAGES[item.status] ? STAGES[item.status] : item.status}
                    </div>
                  </div>
                </Link>
              );
            })
          : ""}
        {/* <Slider {...StatisticSliderSettings} className="padding-top-20">
        {statisticData.map((item, idx) => {
          return (
            <div key={item.title} className="slider_list">
              <div key={idx} className={`slider_listItem ${item.active}`}>
                <div style={{ display: !item.number ? "none" : "block" }}>
                  <p style={{ color: `${item.color}` }}>{item.number}</p>
                </div>

                {item.title}
              </div>
            </div>
          );
        })}
      </Slider> */}
      </div>
    );
  }
}

MyJobsDetailsStatistic.propTypes = {
  // placeholder: PropTypes.string,
  items: PropTypes.string
};

MyJobsDetailsStatistic.defaultProps = {
  items: []
};

export default MyJobsDetailsStatistic;
