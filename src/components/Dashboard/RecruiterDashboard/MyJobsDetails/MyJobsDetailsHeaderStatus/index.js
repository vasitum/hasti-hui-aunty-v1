import React from "react";
import fromNow from "../../../../../utils/env/fromNow";
import { Link } from "react-router-dom";
import "./index.scss";

class MyJobsDetailsHeaderStatus extends React.Component {
  render() {
    const { isShowHeaderStatus, isShowCount, data } = this.props;

    return (
      <div className="MyJobsDetailsHeaderStatus">
        <div className="CandidateApplyHeader">
          {!isShowHeaderStatus ? (
            <div className="candidateApplyStatus">
              <p className="view">
                Views: <span>{data.viewCount}</span>
              </p>
              <p className="view">
                Likes: <span>{data.likeCount}</span>
              </p>
              <p className="submit">
                Posted: <span>{fromNow(data.cTime)}</span>
              </p>
            </div>
          ) : null}

          <div className="MyJobsApplyStatusBtn">
            {isShowCount ? (
              <Link
                to={`/job/recruiter/candidates&menu%5BjobName%5D=${encodeURIComponent(
                  data.title
                )}`}>
                <p>
                  <span className="count">{data.applicationCount}</span>
                  {""}
                  <span className="totalNum">Total applicants</span>
                </p>
              </Link>
            ) : (
              <Link
                to={`/job/recruiter/candidates&menu%5BjobName%5D=${encodeURIComponent(
                  data.title
                )}`}>
                <p>
                  <span className="totalNum">Total applicants</span>
                  <span className="count">{data.applicationCount}</span>
                </p>
              </Link>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default MyJobsDetailsHeaderStatus;
