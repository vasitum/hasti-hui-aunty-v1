import React from "react";

import MyJobsDetailsStatistic from "../MyJobsDetailsStatistic";
import DetailsBodyHeaderStatus from "../DetailsBodyHeaderStatus";

import BodyHeaderDetail from "../BodyHeaderDetail";

import Proptypes from "prop-types";

import MyJobsDetailsBodyContainer from "../MyJobsDetailsBodyContainer";

// import SkillComponent from "../../../../CardElements/SkillListBtn";

import "./index.scss";

class MyJobsDetailsBody extends React.Component {
  state = {
    active: true
  };

  onHandleClick = () => {
    this.setState({
      active: !this.state.active
    });
    // console.log("Click hua hua hua");
  };

  render() {
    const { data } = this.props;
    return (
      <div className="MyJobsDetails_bodyContainer">
        <div className="MyJobsDetailsBody">
          <div className="MyJobsDetailsBody_header">
            <div className="MyJobsDetailsBody_headerStatic">
              <MyJobsDetailsStatistic data={data} />
            </div>
            {/* <BorderComponent /> */}
            <div className="MyJobsDetailsBody_headerStatus">
              <DetailsBodyHeaderStatus data={data} />
            </div>
          </div>
          <div>
            <BodyHeaderDetail data={data} />
          </div>
          <MyJobsDetailsBodyContainer
            onHandleClick={this.onHandleClick}
            active={this.state.active}
            data={data}
          />
        </div>
      </div>
    );
  }
}

MyJobsDetailsBody.Proptypes = {
  data: Proptypes.string
};

MyJobsDetailsBody.defaultProps = {
  data: {
    desc:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad ipsum dolor sit amet, consectetur adipisicing elit, sed do iusmod tempor incididunt ut labore minim veniam, quis nostru exercitation aliquip ex ea commodo consequat."
  }
};

export default MyJobsDetailsBody;
