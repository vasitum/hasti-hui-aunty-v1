import React from "react";
import { Button, Modal, Dropdown } from "semantic-ui-react";
import { Link } from "react-router-dom";
import IcMoreOptionIcon from "../../../../../assets/svg/IcMoreOptionIcon";
import IcEdit from "../../../../../assets/svg/IcEdit";
import ShareModal from "../../../../ShareButton/ShareModal";
import StatusMyJob from "../../../Common/StatusMyJob";

// import EditJob from "../../../../ModalPageSection/EditJob";

// import CandidatDetailMoreOption from "../../CandidatesDetailpage/CandidatDetailMoreOption";

import "./index.scss";

const MoreOptionsTrigger = (
  <Button compact className="jobMoreOption">
    <IcMoreOptionIcon pathcolor="#ceeefe" />
  </Button>
);

// const getItems = data => {
//   return [
//     {
//       text: "Share",
//       value: "Share",
//       // onClick: () => this.props.onChangeSearchField()
//       onClick: () => this.handleModal()
//     },
//     {
//       text: <Link to={`/job/public/${data._id}`}>Public View</Link>,
//       value: "Public View"
//       // onClick: () => this.props.onChangeSearchField()
//     }
//   ];
// };

const getEditItems = data => {
  return [
    {
      text: <Link to={`/job/edit/${data._id}`}>Edit Job details</Link>,
      value: "Edit Job"
      // onClick: () => this.props.onChangeSearchField()
    },
    {
      text: (
        <Link to={`/job/edit/screening/${data._id}/edit`}>Edit Screening</Link>
      ),
      value: "Edit Screening"
      // onClick: () => this.props.onChangeSearchField()
    }
  ];
};

class DetailsBodyHeaderStatus extends React.Component {
  state = {
    statusModal: false,
    editModal: false,
    statusChanged: false,
    newStatus: "",
    modalOpen: false
  };
  getItems = data => {
    return [
      {
        text: "Share",
        value: "Share",
        // onClick: () => this.props.onChangeSearchField()
        onClick: () => this.handleModal()
      },
      {
        text: <Link to={`/job/public/${data._id}`}>Public View</Link>,
        value: "Public View"
        // onClick: () => this.props.onChangeSearchField()
      }
    ];
  };
  handleModal = () => this.setState({ modalOpen: !this.state.modalOpen });
  onStatusModalOpen = e => {
    this.setState({ statusModal: true });
  };

  onStatusModalClose = e => {
    this.setState({ statusModal: false });
  };

  onEditModalOpen = e => {
    this.setState({ editModal: true });
  };

  onEditModalClose = e => {
    this.setState({ editModal: false });
  };

  onStatusChange = status => {
    this.setState({
      statusChanged: true,
      newStatus: status,
      statusModal: false
    });
  };

  render() {
    const { data } = this.props;
    const { statusChanged, newStatus } = this.state;
    // console.log(data);
    return (
      <div className="DetailsBodyHeaderStatus">
        <div className="BodyHeaderStatus_container">
          <div className="BodyHeaderStatusLeft">
            <p>Job status</p>
            <Modal
              open={this.state.statusModal}
              onClose={this.onStatusModalClose}
              className="rejectModal_container"
              trigger={
                <Button
                  onClick={
                    data.status === "Close" ? () => {} : this.onStatusModalOpen
                  }
                  className="">
                  {statusChanged ? newStatus : data.status}
                </Button>
              }
              closeIcon
              closeOnDimmerClick={false}>
              <StatusMyJob
                onChange={this.onStatusChange}
                onClose={this.onStatusModalClose}
                status={statusChanged ? newStatus : data.status}
                jobId={data._id}
              />
            </Modal>
          </div>
          <div className="BodyHeaderStatusRight">
            {/* <Modal
              className="modal_form"
              open={this.state.editModal}
              onClose={this.onEditModalClose}
              trigger={
                <Button className="jobEditBtn" onClick={this.onEditModalOpen}>
                  <IcEdit pathcolor="#ceeefe" /> Edit job
                </Button>
              }
              closeIcon
              closeOnDimmerClick={false}>
              <EditJob onCloseClick={this.onEditModalClose} />
            </Modal> */}

            <Dropdown
              trigger={
                <Button className="jobEditBtn">
                  <IcEdit pathcolor="#ceeefe" /> Edit
                </Button>
              }
              options={getEditItems(data)}
              icon={null}
              pointing="right"
            />

            {/* <CandidatDetailMoreOption /> */}
            <Dropdown
              trigger={MoreOptionsTrigger}
              options={this.getItems(data)}
              icon={null}
              pointing="right"
            />
            <ShareModal
              modalOpen={this.state.modalOpen}
              handleModal={this.handleModal}
              url={`/view/job/${data._id}`}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default DetailsBodyHeaderStatus;
