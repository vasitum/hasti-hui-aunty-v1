import React from "react";
import { Responsive, Button } from "semantic-ui-react";
import MyJobsDetailsContainer from "./MyJobsDetailsContainer";
import MyJobsDetailsMobile from "../MyJobsDetailsMobile";
import withPostedJob from "../../../wrappers/withPostedJob";
// import jobConstants from "../../../../constants/storage/jobs";
import ReactGA from "react-ga";
import "./index.scss";

class MyJobsDetails extends React.Component {
  // componentDidMount() {
  //   window.localStorage.setItem(
  //     jobConstants.JOB_EDIT_DATA,
  //     JSON.stringify(this.props.data)
  //   );
  // }

  // componentWillUnmount() {
  //   window.localStorage.removeItem(jobConstants.JOB_EDIT_DATA);
  // }

  componentDidMount() {
    const { match } = this.props;
    console.log("check url",  match.url);
    ReactGA.pageview(`${match.url}`);
  }

  render() {
    const { data } = this.props;
    return (
      <div className="MyJobsDetails">
        <Responsive maxWidth={1024}>
          <MyJobsDetailsMobile data={data} />
        </Responsive>
        <Responsive minWidth={1025}>
          <MyJobsDetailsContainer data={data} />
        </Responsive>
      </div>
    );
  }
}

export default withPostedJob(MyJobsDetails, { connected: true });
