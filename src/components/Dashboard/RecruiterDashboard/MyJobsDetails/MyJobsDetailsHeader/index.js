import PropTypes from "prop-types";
import React from "react";
import { Grid, Header, Image } from "semantic-ui-react";
import CompanySvg from "../../../../../assets/svg/IcCompany";
import IcFooterArrowIcon from "../../../../../assets/svg/IcFooterArrowIcon";

import { withRouter } from "react-router-dom";

import "./index.scss";

class MyJobsDetailsHeader extends React.Component {
  render() {
    const {
      children,
      data,
      onBackClick,
      HeaderDetailsLeftColummn,
      HeaderDetailsRightColummn
    } = this.props;

    if (!data) {
      return null;
    }

    return (
      <div className="CandidatesDetailsHeader">
        <div className="CandidatesDetail_headerBody">
          <div className="jobDefault_card">
            <Grid>
              <Grid.Row>
                <Grid.Column
                  width={HeaderDetailsLeftColummn}
                  className="CandidatesDetailsHeader_leftSide">
                  <div className="back_arrow">
                    <IcFooterArrowIcon
                      onClick={e => this.props.history.go(-1)}
                      pathcolor="#acaeb5"
                    />
                  </div>
                  <div className="companyLeftDetail">
                    <div className="companyLogo">
                      {/* <IcUser rectcolor="#f7f7fb" pathcolor="#c8c8c8"/> */}
                      {/* <Image src={IcJobThumb} /> */}
                      {/* <CompanySvg
                        width="40"
                        height="40"
                        rectcolor="#f7f7fb"
                        pathcolor="#c8c8c8"
                      /> */}
                      {!data.com || !data.com.imgExt ? (
                        <CompanySvg
                          width="50"
                          height="50"
                          rectcolor="#ffffff"
                          pathcolor="#c8c8c8"
                        />
                      ) : (
                        <Image
                          src={`https://s3-us-west-2.amazonaws.com/img-mwf/company/${
                            data.com.id
                          }/image.jpg`}
                          style={{
                            borderRadius: "6px"
                          }}
                        />
                      )}
                    </div>
                  </div>
                  <div className="companyRightdetails">
                    <div className="headTitle">
                      <Header as="h2">{data.title}</Header>
                    </div>
                    <div className="subTitle">
                      <p className="comName">
                        <span>{data.com ? data.com.name : ""}</span>
                        {/* {data.loc.adrs} */}
                      </p>

                      <p>
                        {/* <span>{data.num}</span> | <span>{data.email}</span> */}
                        {!data.remote
                          ? data.loc
                            ? data.loc.adrs
                            : ""
                          : "Remote"}
                      </p>
                      {/* <p>
                        Applied: <span>{data.des}</span>
                      </p> */}
                    </div>
                  </div>
                </Grid.Column>
                <Grid.Column
                  width={HeaderDetailsRightColummn}
                  className="CandidatesDetailsHeader_rightSide">
                  {children}
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </div>
        </div>
      </div>
    );
  }
}

MyJobsDetailsHeader.propTypes = {
  data: PropTypes.string,
  HeaderDetailsLeftColummn: PropTypes.string,
  HeaderDetailsRightColummn: PropTypes.string
};

MyJobsDetailsHeader.defaultProps = {
  HeaderDetailsLeftColummn: "11",
  HeaderDetailsRightColummn: "5",
  data: {
    title: "UX UI Designer",
    com: {
      name: "Maven"
    },
    loc: {
      adrs: "Noida, India"
    },
    num: "+1 732 000 4444",
    email: "ruby20@gmail.com",
    des: "Software developer",
    increse: "80%",
    match: "matched"
  }
};

export default withRouter(MyJobsDetailsHeader);
