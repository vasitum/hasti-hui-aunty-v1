import React from "react";

import { Button, Icon } from "semantic-ui-react";

import PropTypes from "prop-types";

import Currency from "../../../../CardElements/Currency";

import "./index.scss";

const BodyHeaderDetail = props => {
  const { data } = props;
  // console.log("check daTA", data);

  // const option = [
  //   { key: "INR", text: "INR", value: "INR" },
  //   { key: "EUR", text: "EUR", value: "EUR" },
  //   { key: "USD", text: "USD", value: "USD" },
  //   { key: "JPY", text: "JPY", value: "JPY" },
  //   { key: "CNY", text: "CNY", value: "CNY" }
  // ];

  // state = {
  //   data: data.salary.currency
  // }

  return (
    <div className="BodyHeaderDetail UserProfilePageSection">
      <div className="description_container">
        <div className="description">
          <p className="descTitle">
            Role: <span>{data.role}</span>
          </p>
          <div className="menu-list">
            <p className="">
              Experience:{" "}
              <span>
                {data.exp ? data.exp.min : ""} - {data.exp ? data.exp.max : ""}{" "}
                yrs
              </span>
            </p>
            <p className="">
              Job type: <span>{data.jobType}</span>
            </p>
            <p className="">
              Salary:{" "}
              <span>
                <Currency data={data} />
                {data.salary ? data.salary.min : ""} -{" "}
                {data.salary ? data.salary.max : ""}{" "}
                {/* {data.salary
                  ? data.salary.currency === "INR"
                    ? " LPA"
                    : "k per annum"
                  : ""}{" "} */}
                {data.salary
                  ? data.salary.currency && data.salary.currency !== "INR"
                    ? "k per annum"
                    : " LPA"
                  : ""}{" "}
              </span>
            </p>
          </div>
        </div>
        {/* <div className="downloadCVBtn">
          <Button>Download CV</Button>
        </div> */}
      </div>
    </div>
  );
};

BodyHeaderDetail.PropsType = {
  data: PropTypes.string
};

BodyHeaderDetail.defaultProps = {
  data: {
    role: "Software developer",
    exp: {
      min: "3",
      max: "5"
    },
    jobType: "Full time",
    salary: {
      min: "3",
      max: "5"
    },
    desc:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad ipsum dolor sit amet, consectetur adipisicing elit, sed do iusmod tempor incididunt ut labore minim veniam, quis nostru exercitation aliquip ex ea commodo consequat."
  }
};

export default BodyHeaderDetail;
