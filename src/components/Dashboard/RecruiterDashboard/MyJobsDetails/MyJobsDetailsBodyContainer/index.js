import React from "react";
import { Button, List } from "semantic-ui-react";
import IcDownArrowd from "../../../../../assets/svg/IcDownArrow";
import InfoSectionGridHeader from "../../../../CardElements/InfoSectionGridHeader";
import QuillText from "../../../../CardElements/QuillText";
import Proptypes from "prop-types";
import AboutCompany from "../../../../CardElements/AboutCompany";
import BorderComponent from "../../../../CardElements/BorderComponent";
import AccordionCardContainer from "../../../../Cards/AccordionCardContainer";
import SkillComponent from "../../../../ModalPageSection/PostLoginJob/SkillComponent";

import LikeViewComponent from "../../../Common/LikeViewComponent";

import "./index.scss";

const processDisablityOptions = other => {
  if (!other || !Array.isArray(other.disFriendlyTags)) {
    return null;
  }

  const tags = other.disFriendlyTags;

  return tags.map(val => {
    return <List.Item key={val}>{val}</List.Item>;
  });
};

const processEducation = education => {
  if (!education) {
    return (
      <p
        style={{
          color: "#a1a1a1"
        }}>
        Describe the education qualification for your job
      </p>
    );
  }

  const eduArry = education.map(val => {
    return {
      // course: val.course,
      // spec: val.specs[0],
      level: val.level,
      type: "Full Time"
    };
  });

  const eduArrayComponent = eduArry.map(val => {
    if (!val || !val.level) {
      return (
        <p
          style={{
            color: "#c8c8c8"
          }}>
          Describe the education qualification for your job
        </p>
      );
    }

    return <p>{val.level}</p>;
  });

  return eduArrayComponent;
};

const HideJob = ({ active, onHandleClick, ...resProps }) => {
  return (
    <div className="HideJob" onClick={onHandleClick}>
      <p>
        <span className="ShowJob_detail">Show job details</span>
        <span className="hideJob_detail">hide job details</span>
        <Button className="hideBtn">
          <IcDownArrowd pathcolor="#c8c8c8" />
        </Button>
      </p>
    </div>
  );
};

class MyJobsDetailsBodyContainer extends React.Component {
  render() {
    const { data, user, active, onHandleClick } = this.props;

    return (
      <div className="MyJobsDetailsBodyContainer">
        <div className="MyJobsDetailsBody_bodyDetail">
          <BorderComponent />
          <AccordionCardContainer
            active={active}
            cardHeader={<HideJob onHandleClick={onHandleClick} />}>
            <div className="MyJobsDetailsBody_bodyDetailContainer">
              {/* <LikeViewComponent
                user={data}
                isShowPostTime
                isShowMoreOption
                LikeViewLeftWidth="16"
              /> */}

              <InfoSectionGridHeader headerText="Job Description">
                <QuillText value={data.desc ? data.desc : ""} readOnly />
              </InfoSectionGridHeader>

              {/* <InfoSectionGridHeader headerText="Job Duties">
                <QuillText
                  value={data.respAndDuty ? data.respAndDuty : ""}
                  readOnly
                />
              </InfoSectionGridHeader> */}

              {/* <InfoSectionGridHeader headerText="Benefits">
                <QuillText
                  value={data.benefits ? data.benefits : ""}
                  readOnly
                />
              </InfoSectionGridHeader> */}

              {/* <InfoSectionGridHeader headerText="Skills">
                
              </InfoSectionGridHeader> */}
              <SkillComponent data={data} />

              <InfoSectionGridHeader
                style={{
                  display: !data.edus ? "none" : "block"
                }}
                headerText="Educational qualification">
                {processEducation(data.edus)}
              </InfoSectionGridHeader>

              <InfoSectionGridHeader
                style={{
                  display: !data.other || !data.other.dec ? "none" : "block"
                }}
                headerText="Other Requirement">
                <QuillText
                  value={data.other ? data.other.dec : ""}
                  placeholder={""}
                  readOnly
                />
              </InfoSectionGridHeader>

              <InfoSectionGridHeader
                style={{
                  display:
                    !data.other ||
                    !data.other.disFriendlyTags ||
                    !data.other.disFriendlyTags.length
                      ? "none"
                      : "block"
                }}
                headerText="Disabled Friendly Job">
                <List bulleted horizontal>
                  {processDisablityOptions(data.other)}
                </List>
              </InfoSectionGridHeader>
            </div>
            <AboutCompany compId={data.com ? data.com.id : ""} />
          </AccordionCardContainer>
        </div>
      </div>
    );
  }
}

MyJobsDetailsBodyContainer.Proptypes = {
  data: Proptypes.string
};

MyJobsDetailsBodyContainer.defaultProps = {
  data: {
    desc:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad ipsum dolor sit amet, consectetur adipisicing elit, sed do iusmod tempor incididunt ut labore minim veniam, quis nostru exercitation aliquip ex ea commodo consequat."
  }
};

export default MyJobsDetailsBodyContainer;
