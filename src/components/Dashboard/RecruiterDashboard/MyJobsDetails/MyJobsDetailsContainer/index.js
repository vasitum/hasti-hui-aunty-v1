import React from "react";

import MyJobsDetailsHeader from "../MyJobsDetailsHeader";
import { Container } from "semantic-ui-react";

import MyJobsDetailsHeaderStatus from "../MyJobsDetailsHeaderStatus";

import MyJobsDetailsBody from "../MyJobsDetailsBody";

class MyJobsDetailsContainer extends React.Component {
  render() {
    const { data } = this.props;
    return (
      <div className="MyJobsDetailsContainer">
        <Container>
          <MyJobsDetailsHeader data={data}>
            <MyJobsDetailsHeaderStatus data={data} />
          </MyJobsDetailsHeader>

          <MyJobsDetailsBody data={data} />
        </Container>
      </div>
    );
  }
}

export default MyJobsDetailsContainer;
