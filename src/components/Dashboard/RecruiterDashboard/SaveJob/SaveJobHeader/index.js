import React from "react";
import { Grid, Button, Dropdown } from "semantic-ui-react";
import { Form } from "formsy-semantic-ui-react";
import SearchIcon from "../../../../../assets/svg/SearchIcon";
import InputField from "../../../../Forms/FormFields/InputField";

import PropTypes from "prop-types";

// import "./index.scss";

const Recentstatus = ({ tags, name, onChange, value }) => {
  return (
    <span className="recent_status">
      Tag:{" "}
      <Dropdown
        inline
        options={tags}
        name={name}
        value={value}
        onChange={onChange}
      />
    </span>
  );
};

class SaveJobHeader extends React.Component {
  state = {
    searchtext: "",
    selectedTag: "__default"
  };

  onChange = (e, { value, name }) => {
    this.setState(
      {
        [name]: value
      },
      () => {
        if (name === "selectedTag") {
          this.onSubmit();
        }
      }
    );
  };

  onSubmit = e => {
    const { onFilter } = this.props;
    const refinedTag =
      this.state.selectedTag === "__default" ? "" : this.state.selectedTag;

    onFilter(this.state.searchtext, refinedTag);
  };

  render() {
    const {
      tags,
      headerLeftColumn,
      headerRightColumn,
      isShowSortByDropdown,
      placeholder
    } = this.props;

    return (
      <div className="recommendedJobs_header">
        <div className="recommendedJobs_headerLeft">
          <Form className="" onSubmit={this.onSubmit}>
            <div className="recommended_jobSearch">
              <InputField
                name="searchtext"
                placeholder={placeholder}
                value={this.state.searchtext}
                onChange={this.onChange}
              />
              <Button compact className="SearchBtn">
                <SearchIcon
                  pathcolor={this.state.searchtext ? "#1f2532" : "#c8c8c8"}
                />
              </Button>
            </div>
          </Form>
        </div>

        {!isShowSortByDropdown ? (
          <div className="recommendedJobs_headerRight">
            <div className="dropdown_right">
              {/* <StatusSortByDropdown
                  items={options}
                  defaultRefinement={options[0].value}
                  /> */}
              {
                <Recentstatus
                  tags={[].concat(
                    [
                      {
                        text: "All",
                        value: "__default"
                      }
                    ],
                    tags.map(val => {
                      return {
                        text: val,
                        value: val
                      };
                    })
                  )}
                  onChange={this.onChange}
                  name={"selectedTag"}
                  value={this.state.selectedTag}
                />
              }
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}

SaveJobHeader.propTypes = {
  headerLeftColumn: PropTypes.string,
  headerRightColumn: PropTypes.string
};

SaveJobHeader.defaultProps = {
  headerLeftColumn: "5",
  headerRightColumn: "11"
};

export default SaveJobHeader;
