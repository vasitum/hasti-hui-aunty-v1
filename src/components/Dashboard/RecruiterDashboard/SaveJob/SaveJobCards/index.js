import React from "react";

import AccordionCardContainer from "../../../../Cards/AccordionCardContainer";

import MyJobCard from "../../../../Cards/MyJobCards";
import CardApplyContainer from "../../../../Cards/MyJobCards/CardApplyContainer";

import InfoSection from "../../../../Sections/InfoSection";
import QuillText from "../../../../CardElements/QuillText";

import FlatDefaultBtn from "../../../../Buttons/FlatDefaultBtn";

import MobileCardFooter from "../../../../Cards/MyJobCards/MobileCardFooter";

// import SkillComponent from "../../../../CardElements/SkillListBtn";

import SkillListExpBtn from "../../../../CardElements/SkillListExpBtn";

import SkillList from "../../../../CardElements/SkillList";

import SaveCard from "../SaveCard";

import { Link } from "react-router-dom";

import PropTypes from "prop-types";

import "./index.scss";

class SaveJobCards extends React.Component {
  render() {
    const {
      noClick,
      hit,
      tags,
      isShowCardApplyContainer,
      isShowMobileFooterbottom,
      CardsWidthLeft,
      tohref,
      ...resProps
    } = this.props;

    return (
      <div className="RecommendedJobs_cardContainer">
        {/* <AccordionCardContainer
          noClick={noClick}
          cardHeader={
            <SaveCard
              hit={hit}
              tags={tags}
              isSkillShow
              isHeaderTag
              isShowMobileFooter={isShowMobileFooterbottom}
              {...resProps}>
              {!isShowCardApplyContainer ? <CardApplyContainer /> : null}
            </SaveCard>
          }>
          <div className="Recommended_jobBody">
            <div className="Recommended_jobBodyContainer">
              <div className="Recommended_jobBodyLeft" />
              <div className="Recommended_jobBodyright">
                <div>
                  {hit.desc ? (
                    <InfoSection
                      headerSize="medium"
                      color="blue"
                      headerText="Summary">
                      <QuillText readOnly value={hit.desc} />
                    </InfoSection>
                  ) : null}
                </div>
                <div>
                  {hit.skills && hit.skills.length > 0 ? (
                    <InfoSection
                      headerSize="medium"
                      color="blue"
                      headerText="Skills">
                      <SkillListExpBtn skills={hit.skills} />
                    </InfoSection>
                  ) : null}
                </div>

                <div className="Recommended_jobBodyFooter">
                  <FlatDefaultBtn btntext="View complete profile" />
                </div>
              </div>
            </div>
          </div>
        </AccordionCardContainer> */}
        <SaveCard
          tohref={tohref}
          hit={hit}
          tags={tags}
          isSkillShow
          isHeaderTag
          myJobCardsWidthLeft={CardsWidthLeft}
          isShowMobileFooter={isShowMobileFooterbottom}
          {...resProps}>
          {!isShowCardApplyContainer ? (
            <CardApplyContainer
              hit={hit}
              as={Link}
              tohref={tohref}
              btnText="View Profile"
            />
          ) : null}
        </SaveCard>
      </div>
    );
  }
}

SaveJobCards.propTypes = {
  hit: PropTypes.string
};

SaveJobCards.defaultProps = {
  hit: {
    candidateSummary:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad ipsum dolor sit amet, consectetur adipisicing elit, sed do iusmod tempor incididunt ut labore minim veniam, quis nostru exercitation aliquip ex ea commodo consequat. See more",
    skills: ["html", "css", "javascript"]
  }
};

export default SaveJobCards;
