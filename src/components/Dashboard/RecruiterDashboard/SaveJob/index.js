import React from "react";

import SaveJobHeader from "./SaveJobHeader";
import SaveJobCards from "./SaveJobCards";

import {
  getSavedUser,
  getSavedUserFilter
} from "../../../../api/user/getJobByActions";

import NoFoundMessageDashboard from "../../Common/NoFoundMessageDashboard";

import getParsedtags from "../../../../utils/ui/getParsedtags";
import "./index.scss";
import { Container } from "semantic-ui-react";

class ConddidateSaveJobs extends React.Component {
  constructor(props) {
    super(props);
    this.onFilter = this.onFilter.bind(this);
  }

  state = {
    hits: [],
    tagItems: [],
    tags: []
  };

  async onFilter(text, tag) {
    /* remove all applied filters */
    if (!text && !tag) {
      this.componentDidMount();
      return;
    }

    try {
      const res = await getSavedUserFilter(text ? text : null, tag);
      if (res.status === 200) {
        const data = await res.json();
        // console.log(data);
        this.setState({
          hits: data.user
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  async componentDidMount() {
    try {
      const res = await getSavedUser();
      if (res.status === 200) {
        const data = await res.json();
        // console.log(data);
        this.setState({
          hits: data.user,
          tagItems: data.labelItems,
          tags: data.label
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    return (
      <div className="RecommendedJobs LikeJobs_condidate">
        <Container>
          <SaveJobHeader
            onFilter={this.onFilter}
            tags={this.state.tags}
            placeholder="Search saved profiles"
          />
          <div className="LikeJobs_container">
            {this.state.hits.map(hit => {
              return (
                <SaveJobCards
                  tohref={`/user/public/${hit._id}`}
                  hit={hit}
                  tags={getParsedtags(this.state.tagItems, hit._id)}
                />
              );
            })}{" "}
          </div>
          {this.state.hits.length === 0 ? (
            // <div className="noFound">
            //   <h1>No, Saved Profiles Found!</h1>
            //   <p>You will see your saved profiles here</p>
            // </div>
            <NoFoundMessageDashboard
              noFountTitle="No data found"
              noFountSubTitle="You can reach out to these saved profiles any time "
            />
          ) : null}
        </Container>
      </div>
    );
  }
}

export default ConddidateSaveJobs;
