import React from "react";

import { Container, Modal } from "semantic-ui-react";
import { Form } from "formsy-semantic-ui-react";

import { geocodeByAddress, getLatLng } from "react-places-autocomplete";

import BannerHeader from "./BannerHeader";
import PageBanner from "../Banners/PageBanner";
import aunty from "../../assets/banners/job_cover_img.jpg";
import PostJobHeader from "./PostJobHeader";

import ActionBtn from "../Buttons/ActionBtn";
import FlatDefaultBtn from "../Buttons/FlatDefaultBtn";
import IcSaveAsDraftIcon from "../../assets/svg/IcSaveAsDraft";
import SaveLogo from "../../assets/svg/IcSave";
import IcProfileViewIcon from "../../assets/svg/IcProfileView";
import InfoSection from "../Sections/InfoSection";
import JobDetails from "./JobDetails";
import OtherRequirement from "./OtherRequirement";
import EligibilityDetails from "./EligibilityDetails";

import ReactGA from "react-ga";
import { PostJobPages } from "../EventStrings/PostJobs";
import isLoggedIn from "../../utils/env/isLoggedin";

import fetchJobFromUrl from "../../api/jobs/fetchJobFromUrl";
// import createJobServer from "../../api/jobs/createJob";
import draftJobServer from "../../api/jobs/draftJob";

import getAllPostedJobs from "../../api/jobs/getAllPostedJobs";
import getAllCompanies from "../../api/company/getCompByUserId";
import getJobById from "../../api/jobs/getJobByIdWithDetails";

import IcDownArrow from "../../assets/svg/IcDownArrow";

import DownArrowCollaps from "../utils/DownArrowCollaps";

import isUrl from "is-url";
// import debounce from "../../utils/env/debounce";

import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import CompanyOption from "./JobDetails/CompanyOption";

import "./index.scss";
import { USER } from "../../constants/api";
import jobConstants from "../../constants/storage/jobs";
import CreateCompanyPage from "../CreateCompanyPage";
import EditCompanyPage from "../ModalPageSection/EditCompany";

import PageLoader from "../PageLoader";

// import isAuthenticated from "../../utils/env/isLoggedin";
import { uploadImage } from "../../utils/aws";
// import { Redirect } from "react-router-dom";

import { withRouter } from "react-router-dom";
import uriToBlob from "../../utils/env/uriToBlob";

// import InfoIconLabel from "../utils/InfoIconLabel";
import locParser from "../../utils/env/locParser";
import { parse } from "date-fns";

function parseFinalSalary(salary) {
  const { minSal, maxSal } = salary;
  const { lakh: minLakh, thousand: minThousand } = minSal;
  const { lakh: maxLakh, thousand: maxThousand } = maxSal;

  return {
    min: Number(`${minLakh ? minLakh : 0}.${minThousand ? minThousand : 0}`),
    max: Number(`${maxLakh ? maxLakh : 0}.${maxThousand ? maxThousand : 0}`)
  };
}

function getFinalSalary(salary) {
  let minSal = {
    lakh: 0,
    thousand: 0
  };

  let maxSal = {
    lakh: 0,
    thousand: 0
  };

  const { min, max } = salary;
  if (min) {
    const [minlakh, minthousand] = String(min).split(".");
    minSal = {
      lakh: Number(minlakh) ? Number(minlakh) : 0,
      thousand: minthousand
    };
  }

  if (max) {
    const [maxlakh, maxthousand] = String(max).split(".");
    maxSal = {
      lakh: Number(maxlakh) ? Number(maxlakh) : 0,
      thousand: maxthousand
    };
  }

  return {
    min: min,
    max: max,
    minSal: minSal,
    maxSal: maxSal,
    currency: salary.currency
  };
}

function getProcessedSkills(values, skills, type, typeValues) {
  const oldSkills = skills;

  // if there is a removal of skills
  // process and filter them
  if (values.length < typeValues.length) {
    const [removedVal, ...restVals] = typeValues.filter(val => {
      if (values.indexOf(val) === -1) {
        return true;
      } else {
        return false;
      }
    });

    const finalSkills = oldSkills.filter(val => {
      if (removedVal !== val.name) {
        return true;
      }
    });

    return finalSkills;
  }

  // process oldSkills
  const processedOldSkills = oldSkills.map(val => {
    return val.name;
  });

  const finalValues = values.filter(val => {
    if (processedOldSkills.indexOf(val) === -1) {
      return true;
    } else {
      return false;
    }
  });

  // process new values
  const processedValues = finalValues.map(val => {
    return {
      exp: 0,
      name: val,
      reqType: type
    };
  });

  return oldSkills.concat(processedValues);
}

function getValuesFromSkills(skills, mandOptions, optionalOptions) {
  const skillOptions = {
    mandatory: {
      values: [],
      options: []
    },
    optional: {
      values: [],
      options: []
    }
  };

  skills.map(val => {
    if (val.reqType === "optional") {
      skillOptions.optional.values.push(val.name);
      skillOptions.optional.options.push({
        text: val.name,
        value: val.name
      });
    } else {
      skillOptions.mandatory.values.push(val.name);
      skillOptions.mandatory.options.push({
        text: val.name,
        value: val.name
      });
    }
  });

  // mandatory options
  skillOptions.mandatory.options = skillOptions.mandatory.options.concat(
    mandOptions
  );

  // optional options
  skillOptions.optional.options = skillOptions.optional.options.concat(
    optionalOptions
  );

  return skillOptions;
}

function getProcessedValues(values, otherVal) {
  const finalValues = values.filter(val => {
    if (otherVal.indexOf(val) === -1) {
      return true;
    } else {
      return false;
    }
  });

  return finalValues;
}

function getParsedSkills({ skills }) {
  if (!skills || skills.length === 0) {
    return {
      skills: [],
      skillOptions: [],
      currentValues: []
    };
  }

  let uniqSkills = [...new Set(skills)];

  const pSkills = uniqSkills.map(val => {
    return {
      exp: 0,
      name: val,
      reqType: "mandatory"
    };
  });

  const options = uniqSkills.map(val => {
    return {
      text: val,
      value: val
    };
  });

  return {
    skills: pSkills,
    skillOptions: options,
    currentValues: uniqSkills
  };
}

function postedJobParsedSkills(skills) {
  const skillOptions = {
    skills: [],
    mandatory: {
      options: [],
      value: []
    },
    optional: {
      options: [],
      value: []
    }
  };

  if (!skills || skills.length === 0) {
    return skillOptions;
  }

  skillOptions.skills = skills;

  skills.map(val => {
    if (val.reqType === "optional") {
      skillOptions.optional.options.push({
        text: val.name,
        value: val.name
      });

      skillOptions.optional.value.push(val.name);
    } else {
      skillOptions.mandatory.options.push({
        text: val.name,
        value: val.name
      });

      skillOptions.mandatory.value.push(val.name);
    }
  });

  return skillOptions;
}

function checkPostedJobVal(key) {
  return key ? key : "";
}

class PostJobPage extends React.Component {
  state = {
    title: "",
    com: {
      id: "",
      name: "",
      imgExt: "",
      optionsItem: [],
      showModal: true
    },
    desc: "",
    benefits: "",
    respAndDuty: "",
    imgBanner: "",
    edus: {
      level: ""
    },
    exp: {
      min: "",
      max: ""
    },
    jobType: "",
    remote: false,
    loc: {
      adrs: "",
      city: "",
      country: "",
      lat: "",
      lon: "",
      street: "",
      zipcode: ""
    },
    role: "",
    salary: {
      minSal: {
        lakh: "",
        thousand: ""
      },
      maxSal: {
        lakh: "",
        thousand: ""
      },
      currency: "",
      max: "",
      min: ""
    },
    other: {
      dec: "",
      disFriendlyTags: [],
      disfriendly: false
    },

    workDays: {
      days: "",
      mandatory: false,
      shift: "",
      time: ""
    },

    joiningProb: {
      joiningProbability: 0,
      mandatory: false
    },

    hasAdditionalDetails: "",
    skills: [],
    skillFieldOptionsMandatory: [],
    skillFieldCurrentValuesMandatory: [],
    skillFieldOptionsOptional: [],
    skillFieldCurrentValuesOptional: [],
    skillShowEditBtn: true,
    isLinkLoading: false,
    canSubmit: false,
    parseUrl: "",
    myJobs: {
      optionsItem: [],
      preJobs: [],
      value: ""
    },
    selectedCompanyData: null,
    bannerImage: aunty,
    bannerImageFile: "",
    isFormLoading: false,
    formLoadingText: "Loading Job",
    create_company: false,
    edit_company: false
  };

  constructor(props) {
    super(props);
    this.onLinkChange = this.onLinkChange.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.onDraftSubmit = this.onDraftSubmit.bind(this);
    this.setCompanyDropdown = this.setCompanyDropdown.bind(this);
    this.setJobDropdown = this.setJobDropdown.bind(this);
  }

  onCreateCompanyOpen = e => {
    this.setState({
      create_company: true
    });
  };

  onEditCompanyOpen = e => {
    this.setState({
      edit_company: true
    });
  };

  onCreateCompanyClose = e => {
    this.setState({
      create_company: false
    });
  };

  onEditCompanyClose = e => {
    this.setState({
      edit_company: false
    });
  };

  onShiftChange = (e, { value, name }) => {
    this.setState({
      workDays: {
        ...this.state.workDays,
        [name]: parse(value).getTime()
      }
    });
  };

  onjoiningProbChange = (e, { value, name }) => {
    this.setState({
      joiningProb: {
        ...this.state.joiningProb,
        [name]: value
      }
    });
  };

  onMinSalChange = (e, { value, name }) => {
    this.setState({
      salary: {
        ...this.state.salary,
        minSal: {
          ...this.state.salary.minSal,
          [name]: value
        }
      }
    });
  };

  onMaxSalChange = (e, { value, name }) => {
    this.setState({
      salary: {
        ...this.state.salary,
        maxSal: {
          ...this.state.salary.maxSal,
          [name]: value
        }
      }
    });
  };

  async setCompanyDropdown() {
    try {
      const res = await getAllCompanies();
      const data = await res.json();

      // data from server
      const hasCompanies = data.content.length;

      if (hasCompanies) {
        const comdata = data.content.map(val => {
          console.log("check val", val);
          return {
            key: val._id,
            text: (
              <CompanyOption
                compName={val.name}
                location={val.headquarter ? val.headquarter.location : ""}
                imgUrl={val.imgExt ? `${val._id}/image.${val.imgExt}` : null}
                tag={val.clientName ? val.clientName : ""}
              />
            ),
            // text: val.clientName ? val.name + ", " + val.clientName : val.name,
            imgExt: val.imgExt,
            value: val._id,
            compName: val.name
          };
        });

        this.setState({
          com: {
            ...this.state.com,
            optionsItem: comdata
          }
        });

        // store the companies data;
        window.localStorage.setItem(
          jobConstants.JOB_COMPANY_INFO,
          JSON.stringify(data.content)
        );
      }
    } catch (error) {
      console.error("Unable to Fetch Companies");
    }
  }

  async setJobDropdown() {
    try {
      const res = await getAllPostedJobs();
      const data = await res.json();

      if (data.length === 0) {
        return;
      }

      const jobOptionsItem = data.map(val => {
        return {
          key: val._id,
          value: val._id,
          text:
            val.title +
            " | " +
            val.com.name +
            (val.com.tags ? " | " + val.com.tags : "")
        };
      });

      this.setState({
        myJobs: {
          preJobs: data,
          optionsItem: jobOptionsItem
        }
      });
    } catch (error) {
      console.error("Unable to fetch Jobs", error);
    }
  }

  async componentDidMount() {
    ReactGA.pageview('/job/new');

    if (window.location.pathname.split("/").pop() !== "new") {
      try {
        const res = await getJobById(this.props.match.params.id);
        if (res.status === 200) {
          const data = await res.json();
          this.onJobChangeState(data);
          this.setCompanyDropdown();
          this.setJobDropdown();
          return;
        }
      } catch (error) {
        console.error(error);
      }
      this.setCompanyDropdown();
      this.setJobDropdown();
      return;
    }

    const jobJsonData = window.localStorage.getItem(
      jobConstants.JOB_PREVIEW_STATE
    );

    // if state exists use it and remove everything
    if (jobJsonData && jobJsonData !== "undefined" && jobJsonData !== "null") {
      console.log("JOB JSON DATA CALLED");

      const jsonJob = JSON.parse(jobJsonData);

      this.setState(state => ({
        ...jsonJob,
        com: {
          ...jsonJob.com,
          optionsItem: []
        }
      }));

      this.setCompanyDropdown();
      window.localStorage.removeItem(jobConstants.JOB_PREVIEW_STATE);
    } else {
      this.setCompanyDropdown();
      this.setJobDropdown();
    }
  }

  parseJobdataToState = data => {
    // MORPH IT!
    data = data.data;

    const { skills, skillOptions, currentValues } = getParsedSkills(data);

    this.setState(state => ({
      ...state,
      com: {
        ...state.com,
        id: "",
        name: "",
        imgExt: "",
        tags: "",
        showModal: true
      },
      selectedCompanyData: "",
      benefits: "",
      respAndDuty: "",
      imgBanner: "",
      edus: {
        level: ""
      },
      jobType: "",
      loc: {
        adrs: "",
        city: "",
        country: "",
        lat: "",
        lon: "",
        street: "",
        zipcode: ""
      },
      role: "",
      other: {
        dec: "",
        disFriendlyTags: [],
        disfriendly: false
      },
      hasAdditionalDetails: "",
      desc: data.desc || "",
      exp: {
        min: data.exp ? data.exp.min : 0,
        max: data.exp ? data.exp.max : 0
      },
      salary: {
        currency: "INR",
        max: 0,
        min: 0
      },
      title: data.title || "",
      role: data.role || "",
      skills: skills || [],
      skillFieldOptionsMandatory: skillOptions || [],
      skillFieldCurrentValuesMandatory: currentValues || [],
      skillShowEditBtn: currentValues.length ? true : false,
      isLinkLoading: false,
      isFormLoading: false
    }));
  };

  async sendRequestUri(url) {
    return await fetchJobFromUrl(url);
  }

  onLinkInputChange = (e, { value }) => {
    this.setState({
      parseUrl: value
    });
  };

  async onLinkChange(ev) {
    if (!isUrl(this.state.parseUrl) || !this.state.parseUrl) {
      toast("Sorry, Entered Text is not a URL", {
        autoClose: 1500,
        pauseOnHover: true
      });

      return;
    }

    // toast("Parsing URL", {
    //   autoClose: 5000,
    //   pauseOnHover: true
    // });

    this.setState({
      isLinkLoading: true,
      isFormLoading: true,
      formLoadingText: "Parsing Job ..."
    });

    try {
      const jobData = await this.sendRequestUri(this.state.parseUrl);
      console.log(jobData);
      this.parseJobdataToState(jobData);
    } catch (error) {
      console.log(error);
      toast("Unable to parse url, please manually fill the post");
      this.setState({
        isLinkLoading: false,
        isFormLoading: false
      });
    }
  }

  onLocSelect = address => {
    geocodeByAddress(address)
      .then(results => {
        const addrDetails = locParser(results[0].address_components, address);

        if (typeof addrDetails === "string") {
          this.setState({
            loc: {
              ...this.state.loc,
              city: addrDetails,
              adrs: address
            }
          });
        } else {
          this.setState({
            loc: {
              ...this.state.loc,
              city: addrDetails.primary,
              country: addrDetails.country,
              adrs: address
            }
          });
        }

        return getLatLng(results[0]);
      })
      .then(latLng => {
        this.setState({
          loc: {
            ...this.state.loc,
            lat: latLng.lat,
            lon: latLng.lng
          }
        });
      })
      .catch(error => console.error("Error", error));
  };

  onLocChange = address => {
    this.setState({
      loc: {
        ...this.state.loc,
        adrs: address
      }
    });
  };

  onInputChange = (ev, { value, name }) => {
    if (name === "hasAdditionalDetails") {
      this.setState({
        [name]: value,
        other: {
          ...this.state.other,
          dec: ""
        }
      });

      return;
    }

    this.setState({
      [name]: value
    });
  };

  onQuillChange = html => {
    this.setState({
      desc: html
    });
  };

  onDescChange = html => {
    this.setState({
      desc: html
    });
  };

  onResChange = html => {
    this.setState({
      respAndDuty: html
    });
  };

  onBenefitsChange = html => {
    this.setState({
      benefits: html
    });
  };

  onSKillAdd = (e, { value, type }) => {
    switch (type) {
      case "mandatory":
        this.setState({
          skillFieldOptionsMandatory: [
            { text: value, value },
            ...this.state.skillFieldOptionsMandatory
          ]
        });
        break;

      case "optional":
        this.setState({
          skillFieldOptionsOptional: [
            { text: value, value },
            ...this.state.skillFieldOptionsOptional
          ]
        });
        break;

      default:
        break;
    }
  };

  onSkillChange = (e, { value, type }) => {
    switch (type) {
      case "mandatory":
        this.setState({
          skillFieldCurrentValuesMandatory: getProcessedValues(
            value,
            this.state.skillFieldCurrentValuesOptional
          ),
          skills: getProcessedSkills(
            value,
            this.state.skills,
            type,
            this.state.skillFieldCurrentValuesMandatory
          )
        });
        break;

      case "optional":
        this.setState({
          skillFieldCurrentValuesOptional: getProcessedValues(
            value,
            this.state.skillFieldCurrentValuesMandatory
          ),
          skills: getProcessedSkills(
            value,
            this.state.skills,
            type,
            this.state.skillFieldCurrentValuesOptional
          )
        });
        break;

      default:
        break;
    }

    console.log(type);
  };

  /**
   * @TODO: Change for the new System
   */
  onSkillEditModalInputChange = (e, { value, idx, name }) => {
    const newSkills = [...this.state.skills];
    newSkills[idx][name] = value;

    if (name === "reqType") {
      const skillsFormatted = getValuesFromSkills(
        newSkills,
        this.state.skillFieldOptionsMandatory,
        this.state.skillFieldOptionsOptional
      );
      this.setState({
        skillFieldCurrentValuesMandatory: skillsFormatted.mandatory.values,
        skillFieldCurrentValuesOptional: skillsFormatted.optional.values,
        skillFieldOptionsMandatory: skillsFormatted.mandatory.options,
        skillFieldOptionsOptional: skillsFormatted.optional.options,
        skills: newSkills
      });
    } else {
      this.setState({
        skills: newSkills
      });
    }
  };

  onSkillModalRemoveCurrentValuesUpdate(skill) {
    if (!skill) {
      return;
    }

    const reqType = skill.reqType;
    let currentValuesOptional = this.state.skillFieldCurrentValuesOptional,
      currentValuesMandatory = this.state.skillFieldCurrentValuesMandatory;

    console.log("Current Val Mandatory Pre Change", currentValuesMandatory);
    console.log("Current Val Option Pre Change", currentValuesOptional);

    if (reqType === "optional") {
      currentValuesOptional = currentValuesOptional.filter(val => {
        return val !== skill.name;
      });
    } else {
      currentValuesMandatory = currentValuesMandatory.filter(
        val => val != skill.name
      );
    }

    return {
      currentValuesOptional: currentValuesOptional,
      currentValuesMandatory: currentValuesMandatory
    };
  }

  onSkillEditModalRemove = (e, { idx }) => {
    this.setState((prevState, props) => {
      const newSkills = [...prevState.skills];
      const removedSkill = newSkills.splice(idx, 1);

      console.log("Removed Skill is", removedSkill);

      const {
        currentValuesMandatory,
        currentValuesOptional
      } = this.onSkillModalRemoveCurrentValuesUpdate(removedSkill[0]);

      console.log("Current Val Mandatory", currentValuesMandatory);
      console.log("Current Val Option", currentValuesOptional);

      return {
        ...prevState,
        skills: newSkills,
        skillFieldCurrentValuesMandatory: currentValuesMandatory,
        skillFieldCurrentValuesOptional: currentValuesOptional
      };
    });
  };

  onSkillModalSave = e => {
    this.forceUpdate();
  };

  onExpChange = (e, { value, name }) => {
    this.setState({
      exp: {
        ...this.state.exp,
        [name]: value
      }
    });
  };

  onSalaryChange = (e, { value, name }) => {
    this.setState({
      salary: {
        ...this.state.salary,
        [name]: value
      }
    });
  };

  onEduChange = (e, { value, name }) => {
    this.setState({
      edus: {
        ...this.state.edus,
        [name]: value
      }
    });
  };

  onOtherChange = (e, { value, name }) => {
    this.setState({
      other: {
        ...this.state.other,
        [name]: value
      }
    });
  };

  onCompanyCreate = (data, isEditModal) => {
    if (isEditModal) {
      const newOptions = this.state.com.optionsItem.filter(
        val => val.key !== data._id
      );
      this.setState({
        com: {
          ...this.state.com,
          optionsItem: [
            ...newOptions,
            {
              key: data._id,
              text: (
                <CompanyOption
                  compName={data.name}
                  location={data.headquarter ? data.headquarter.location : ""}
                  imgUrl={
                    data.imgExt ? `${data._id}/image.${data.imgExt}` : null
                  }
                  tag={data.clientName ? data.clientName : ""}
                />
              ),
              // text: data.clientName
              //   ? data.name + ", " + data.clientName
              //   : data.name,
              value: data._id
            }
          ],
          id: data._id,
          name: data.name,
          imgExt: data.imgExt,
          tags: data.clientName,
          showModal: true
        },
        selectedCompanyData: data
      });

      return;
    }

    this.setState({
      com: {
        ...this.state.com,
        optionsItem: [
          ...this.state.com.optionsItem,
          {
            key: data._id,
            // text: data.clientName
            //   ? data.name + ", " + data.clientName
            //   : data.name,
            text: (
              <CompanyOption
                compName={data.name}
                location={data.headquarter ? data.headquarter.location : ""}
                imgUrl={data.imgExt ? `${data._id}/image.${data.imgExt}` : null}
                tag={data.clientName ? data.clientName : ""}
              />
            ),
            value: data._id
          }
        ],
        id: data._id,
        name: data.name,
        imgExt: data.imgExt,
        tags: data.clientName
      },
      selectedCompanyData: data
    });

    const compdata = window.localStorage.getItem(jobConstants.JOB_COMPANY_INFO);
    if (!compdata || compdata === "undefined" || compdata === "null") {
      window.localStorage.setItem(
        jobConstants.JOB_COMPANY_INFO,
        JSON.stringify([...data])
      );
    } else {
      let compjson = JSON.parse(compdata);
      compjson = [...compjson, data];
      window.localStorage.setItem(
        jobConstants.JOB_COMPANY_INFO,
        JSON.stringify([...compjson, data])
      );
    }
  };

  getSelectedCompanyName = key => {
    let compdata = null;
    this.state.com.optionsItem.map(val => {
      if (val.key === key) {
        // name = val.text;
        compdata = {
          name: val.compName,
          imgExt: val.imgExt
        };
      }
    });

    return compdata;
  };

  getSelectedPostedJob = key => {
    let selectedJob = null;

    this.state.myJobs.preJobs.map(val => {
      if (val._id === key) {
        selectedJob = val;
      }
    });

    return selectedJob;
  };

  /* Enable Form Loader */
  enableFormLoader = loaderText => {
    this.setState({
      isFormLoading: true,
      formLoadingText: loaderText
    });
  };

  /* Disable Form Loader */
  disableFormLoader = () => {
    this.setState({
      isFormLoading: false
    });
  };

  getSelectedCompanyJson = id => {
    const compdata = window.localStorage.getItem(jobConstants.JOB_COMPANY_INFO);
    const compjson = JSON.parse(compdata);

    // if (!compjson) {
    //   return;
    // }

    let data;
    compjson.map(val => {
      if (val._id === id) {
        data = val;
      }
    });

    if (data) {
      return data;
    }

    return null;
  };

  onSelectCompanyChange = (ev, { value }) => {
    if (!value) {
      return;
    }

    const compdata = this.getSelectedCompanyName(value);
    const compJson = this.getSelectedCompanyJson(value);
    this.setState({
      com: {
        ...this.state.com,
        id: value,
        name: compdata.name,
        imgExt: compdata.imgExt ? compdata.imgExt : "",
        tags: compdata.clientName
      },
      selectedCompanyData: compJson
    });
  };

  onJobChangeState = selectedJob => {
    const parsedSkills = postedJobParsedSkills(selectedJob.skills);
    const selectedCompJson = this.getSelectedCompanyJson(selectedJob.com.id);

    this.setState({
      ...this.state,
      title: checkPostedJobVal(selectedJob.title),
      com: {
        ...this.state.com,
        id: checkPostedJobVal(selectedJob.com.id),
        name: checkPostedJobVal(selectedJob.com.name),
        imgExt: checkPostedJobVal(selectedJob.com.imgExt),
        tags: checkPostedJobVal(selectedJob.com.tags),
        showModal: true
      },
      selectedCompanyData: selectedCompJson ? selectedCompJson : "",
      desc: checkPostedJobVal(selectedJob.desc),
      benefits: checkPostedJobVal(selectedJob.benefits),
      respAndDuty: checkPostedJobVal(selectedJob.respAndDuty),
      edus: {
        ...this.state.edus,
        level: checkPostedJobVal(selectedJob.edus[0].level)
      },
      exp: {
        min: checkPostedJobVal(selectedJob.exp.min),
        max: checkPostedJobVal(selectedJob.exp.max)
      },
      jobType: checkPostedJobVal(selectedJob.jobType),
      remote: checkPostedJobVal(selectedJob.remote) ? true : false,
      loc: {
        ...this.state.loc,
        adrs: checkPostedJobVal(selectedJob.loc.adrs),
        city: checkPostedJobVal(selectedJob.loc.city),
        lat: checkPostedJobVal(selectedJob.loc.lat),
        lon: checkPostedJobVal(selectedJob.loc.lon)
      },
      role: checkPostedJobVal(selectedJob.role),
      salary: getFinalSalary(selectedJob.salary),
      other: {
        ...this.state.other,
        dec: checkPostedJobVal(selectedJob.other.dec),
        disFriendlyTags: checkPostedJobVal(selectedJob.other.disFriendlyTags)
      },
      hasAdditionalDetails: checkPostedJobVal(selectedJob.other.dec)
        ? true
        : "",
      skills: parsedSkills.skills,
      skillFieldCurrentValuesMandatory: parsedSkills.mandatory.value,
      skillFieldCurrentValuesOptional: parsedSkills.optional.value,
      skillFieldOptionsMandatory: parsedSkills.mandatory.options,
      skillFieldOptionsOptional: parsedSkills.optional.options,
      skillShowEditBtn: true,
      myJobs: {
        ...this.state.myJobs,
        value: selectedJob._id
      },
      parseUrl: ""
    });
  };

  onSelectJobChange = (ev, { value }) => {
    if (!value) {
      return;
    }

    const selectedJob = this.getSelectedPostedJob(value);
    this.onJobChangeState(selectedJob);
  };

  onFileChange = e => {
    const file = e.target.files[0];

    // flush values
    // Fix Multi Select Values
    e.target.value = "";

    if (!file) return;

    const extn = file.name.split(".").pop();
    const reader = new FileReader();

    reader.onloadend = () => {
      this.setState({
        imgBanner: "jpg",
        bannerImage: reader.result,
        bannerImageFile: file
      });
    };

    reader.readAsDataURL(file);
  };

  onFileRemove = e => {
    this.setState({
      imgBanner: "",
      bannerImage: aunty,
      bannerImageFile: ""
    });
  };

  onRemoteChange = e => {
    this.setState({
      remote: !this.state.remote
    });
  };

  async onFormSubmit(event) {
    // event.preventDefault();
    const { history } = this.props;

    if (!this.state.desc || this.state.desc.length === 11) {
      toast("Job Description not found");
      return;
    }

    if (this.state.desc.length > 3000 && this.state.desc) {
      toast("Job Description Should be less than 3000 words");
      return;
    }

    if (this.state.respAndDuty.length > 2000 && this.state.respAndDuty) {
      toast("Responsibilities and Duties Should be less than 2000 words");
      return;
    }

    if (this.state.benefits.length > 2000 && this.state.benefits) {
      toast("Benefits Should be less than 2000 words");
      return;
    }

    if (!this.state.skills || !this.state.skills.length) {
      toast("Please add skills, you want to hire for");
      return;
    }

    this.setState({
      isFormLoading: true,
      formLoadingText: "Publishing Job"
    });

    try {
      const res = await draftJobServer({
        ...this.state,
        edus: [
          {
            level: this.state.edus.level
          }
        ],
        salary: parseFinalSalary(this.state.salary),
        userId: window.localStorage.getItem(USER.UID)
      });

      const data = await res.json();
      console.log(data);
      if (this.state.bannerImageFile) {
        const imgFile = uriToBlob(this.state.bannerImage);
        const imgBannerUpload = await uploadImage(
          `banner/${data._id}/image.png`,
          imgFile
        );
      }

      // toast("Job Successfully Posted!");

      this.setState({
        isFormLoading: false
      });

      window.localStorage.removeItem(jobConstants.JOB_PREVIEW_STATE);
      history.push(`/job/edit/${data._id}`);
      history.push(`/job/edit/screening/${data._id}`);
    } catch (error) {
      console.error(error);
    }
  }

  async onDraftSubmit(event) {
    event.preventDefault();
    const { history } = this.props;

    if (!this.state.desc || this.state.desc.length === 11) {
      toast("Job Description not found");
      return;
    }

    if (this.state.desc.length > 3000 && this.state.desc) {
      toast("Job Description Should be less than 3000 words");
      return;
    }

    if (this.state.respAndDuty.length > 2000 && this.state.respAndDuty) {
      toast("Responsibilities and Duties Should be less than 2000 words");
      return;
    }

    if (this.state.benefits.length > 2000 && this.state.benefits) {
      toast("Benefits Should be less than 2000 words");
      return;
    }

    if (!this.state.skills || !this.state.skills.length) {
      toast("Please add skills, you want to hire for");
      return;
    }

    this.enableFormLoader("Loading Job ...");

    try {
      const res = await draftJobServer({
        ...this.state,
        edus: [
          {
            level: this.state.edus.level
          }
        ],
        salary: parseFinalSalary(this.state.salary),
        userId: window.localStorage.getItem(USER.UID)
      });

      const data = await res.json();
      console.log(data);
      if (this.state.bannerImageFile) {
        const imgBannerUpload = await uploadImage(
          `banner/${data._id}/image.png`,
          this.state.bannerImageFile
        );
      }

      // toast("Job Successfully Posted!");

      this.setState({
        isFormLoading: false
      });

      window.localStorage.removeItem(jobConstants.JOB_PREVIEW_STATE);

      history.push(`/job/view/${data._id}`);
    } catch (error) {
      console.error(error);
    }
  }

  onDisabilityOptionschange = value => {
    this.setState(state => {
      if (state.other.disFriendlyTags.indexOf(value) === -1) {
        return {
          ...state,
          other: {
            ...state.other,
            disFriendlyTags: [...state.other.disFriendlyTags, value]
          }
        };
      }

      return {
        ...state,
        other: {
          ...state.other,
          disFriendlyTags: state.other.disFriendlyTags.filter(
            val => val !== value
          )
        }
      };
    });
  };

  onFormValid = ev => {
    this.setState({
      canSubmit: true
    });
  };

  onFormInValid = ev => {
    this.setState({
      canSubmit: false
    });
  };

  onPreviewJobClick = ev => {
    if (!this.state.desc || this.state.desc.length === 11) {
      toast("Job Description not found");
      return;
    }

    if (this.state.desc.length > 3000 && this.state.desc) {
      toast("Job Description Should be less than 3000 words");
      return;
    }

    if (this.state.respAndDuty.length > 2000 && this.state.respAndDuty) {
      toast("Responsibilities and Duties Should be less than 2000 words");
      return;
    }

    if (this.state.benefits.length > 2000 && this.state.benefits) {
      toast("Benefits Should be less than 2000 words");
      return;
    }

    if (!this.state.skills || !this.state.skills.length) {
      toast("Please add skills, you want to hire for");
      return;
    }

    const stateStr = JSON.stringify(this.state);
    window.localStorage.setItem(jobConstants.JOB_PREVIEW_STATE, stateStr);
    this.props.history.push("/job/preview");
  };

  componentWillUnmount() {
    // window.localStorage.removeItem(jobConstants.JOB_COMPANY_INFO);
  }

  onActionBtnClick = ev => {
    if (!this.state.canSubmit) {
      // ev.preventDefault();
      toast("Oops! Please Fill your Form Correctly");
    }
  };

  render() {
    const UserId = window.localStorage.getItem(USER.UID);
    return (
      <React.Fragment>
        <div className="PostJobPage">
          <div>
            <PageBanner size="medium" image={this.state.bannerImage}>
              <Container>
                <BannerHeader
                  bannerImageFile={this.state.bannerImageFile}
                  onFileRemove={this.onFileRemove}
                  onFileChange={this.onFileChange}
                />
              </Container>
            </PageBanner>
          </div>

          <Form
            noValidate
            onValidSubmit={this.onFormSubmit}
            onValid={this.onFormValid}
            onInvalid={this.onFormInValid}>
            <Container>
              <PostJobHeader
                data={this.state.myJobs}
                onPostedJobSelect={this.onSelectJobChange}
                onLinkChange={this.onLinkChange}
                onLinkInputChange={this.onLinkInputChange}
                isLoading={this.state.isLinkLoading}
              />
              <div className="postJob_jobDetails panel-card">
                <InfoSection
                  headerSize="large"
                  color="blue"
                  headerText="Job Details"
                  rightIcon={<DownArrowCollaps />}
                  collapsible
                  isActive={true}>
                  <JobDetails
                    data={this.state}
                    onSalaryMinChange={this.onMinSalChange}
                    onSalaryMaxChange={this.onMaxSalChange}
                    onInputChange={this.onInputChange}
                    onLocChange={this.onLocChange}
                    onLocSelect={this.onLocSelect}
                    onQuillChange={this.onQuillChange}
                    onResChange={this.onResChange}
                    onBenefitsChange={this.onBenefitsChange}
                    onDescChange={this.onDescChange}
                    onSalaryChange={this.onSalaryChange}
                    onCompanyCreate={this.onCompanyCreate}
                    onCompanyChange={this.onSelectCompanyChange}
                    onEnableFormLoader={this.enableFormLoader}
                    onDisableFormLoader={this.disableFormLoader}
                    onRemoteChange={this.onRemoteChange}
                    onModalCreateCompany={this.onCreateCompanyOpen}
                    onModalEditCompany={this.onEditCompanyOpen}
                  />

                  <EligibilityDetails
                    data={this.state}
                    onSkillChange={this.onSkillChange}
                    onSKillAdd={this.onSKillAdd}
                    onSkillModalChange={this.onSkillEditModalInputChange}
                    onSkillModalRemove={this.onSkillEditModalRemove}
                    onSkillModalSave={this.onSkillModalSave}
                    onExpChange={this.onExpChange}
                    onInputChange={this.onInputChange}
                    onEduChange={this.onEduChange}
                    onOtherChange={this.onOtherChange}
                  />
                </InfoSection>
              </div>
            </Container>
            <OtherRequirement
              data={this.state.other}
              dataOther={this.state}
              onjoiningProbChange={this.onjoiningProbChange}
              onShiftChange={this.onShiftChange}
              onInputChange={this.onInputChange}
              onOtherChange={this.onOtherChange}
              onDisabilityOptionschange={this.onDisabilityOptionschange}
            />
            <Container>
              <div className="postJob_actionBtn">
                <FlatDefaultBtn
                  btnicon={
                    <IcProfileViewIcon height="12" pathcolor="#c8c8c8" />
                  }
                  btntext="Preview Job"
                  type="button"
                  disabled={!this.state.canSubmit}
                  onClick={this.onPreviewJobClick}
                  // className="profileView_Icon"
                  style={{
                    paddingLeft: "2px",
                    paddingRight: "15px"
                  }}
                />
                <span className="mobile hidden">
                  <FlatDefaultBtn
                    btnicon={<SaveLogo height="12" pathcolor="#c8c8c8" />}
                    onClick={this.onDraftSubmit}
                    disabled={!this.state.canSubmit}
                    btntext="Save as draft"
                    type="button"
                  />
                </span>
                <div className="actionBtnFooter">
                  <ActionBtn
                    style={{
                      opacity: !this.state.canSubmit ? "0.6" : "1"
                    }}
                    onClick={() => {
                      ReactGA.event({
                        category: PostJobPages.PostJobsEvent.category,
                        action: PostJobPages.PostJobsEvent.action,
                        value: isLoggedIn() ? "" : UserId,
                      });
                      this.onActionBtnClick();
                    }}
                    type="submit"
                    actioaBtnText="Next"
                    btnIcon={<IcDownArrow pathcolor="#99d9f9" />}
                  />
                  <p>Review screening</p>
                </div>
              </div>
            </Container>
          </Form>
        </div>
        <PageLoader
          active={this.state.isFormLoading}
          loaderText={this.state.formLoadingText}
        />

        {/* Edit Company Modal */}
        <Modal
          className="modal_form"
          open={this.state.edit_company}
          onClose={this.onEditCompanyClose}
          closeIcon
          closeOnDimmerClick={false}>
          <EditCompanyPage
            data={this.state.selectedCompanyData}
            onCompanyCreate={this.onCompanyCreate}
            onModalClose={this.onEditCompanyClose}
            onEnableFormLoader={this.enableFormLoader}
            onDisableFormLoader={this.disableFormLoader}
          />
        </Modal>

        {/* Create Company Modal */}
        <Modal
          className="modal_form"
          open={this.state.create_company}
          onClose={this.onCreateCompanyClose}
          closeIcon
          closeOnDimmerClick={false}>
          <CreateCompanyPage
            onCompanyCreate={this.onCompanyCreate}
            onModalClose={this.onCreateCompanyClose}
            onEnableFormLoader={this.enableFormLoader}
            onDisableFormLoader={this.disableFormLoader}
          />
        </Modal>
      </React.Fragment>
    );
  }
}

export default withRouter(PostJobPage);
