import React from "react";

import { Grid, Header, List } from "semantic-ui-react";
import PropTypes from "prop-types";

import SelectOptionField from "../../Forms/FormFields/SelectOptionField";
import InputField from "../../Forms/FormFields/InputField";
import IcInfoIcon from "../../../assets/svg/IcInfo";
import IcDownArrowIcon from "../../../assets/svg/IcDownArrow";
import getPlaceholder from "../../Forms/FormFields/Placeholder";
import ActionBtn from "../../Buttons/ActionBtn";

import debounce from "../../../utils/env/debounce";
import InfoIconLabel from "../../utils/InfoIconLabel";

import { POST_JOB } from "../../../strings";

import "./index.scss";

function getDisabledPlaceHolder(length) {
  if (!length) {
    return getPlaceholder("No Previous Job Found", " No Previous Job Found");
  }

  return getPlaceholder("Select", " Select posted job to autofill");
}

const PostJobHeader = props => {
  return (
    <div className="PostJobHeader">
      <div className="header_text">
        <Header as="h2">{POST_JOB.SECTION_TOP_FEATURE_TITLE}:</Header>
      </div>
      <Grid>
        <Grid.Row className="header_inputField">
          <Grid.Column mobile={16} computer={7} className="selectOption_column">
            <div className="selectOption">
              <Header as="h4" className="selectOption_label">
                {POST_JOB.SECTION_TOP_FEATURE_FIRST}
                {/* <IcInfoIcon pathcolor="#c8c8c8" /> */}
                <InfoIconLabel
                  text={POST_JOB.SECTION_TOP_FEATURE_FIRST_BUBBLE}
                />
              </Header>
              <SelectOptionField
                placeholder={getDisabledPlaceHolder(
                  props.data ? props.data.optionsItem.length : 0
                )}
                optionsItem={props.data.optionsItem}
                onChange={props.onPostedJobSelect}
                value={props.data.value}
                name="selectJobPreSelect"
              />
            </div>
          </Grid.Column>

          <Grid.Column
            width={1}
            only="large screen"
            className="inputField_divider">
            <Header as="h4">'Or'</Header>
          </Grid.Column>

          <Grid.Column
            mobile={16}
            computer={8}
            className="selectOption_jobLinkColumn">
            <div className="selectOption">
              <Grid>
                <Grid.Row className="inputField_lavel">
                  <Grid.Column width={16}>
                    <Header as="h4">
                      {POST_JOB.SECTION_TOP_FEATURE_SECND}
                      <span className="list_item">
                        <List bulleted horizontal>
                          <List.Item>Times</List.Item>
                          {/* <List.Item>Naukri</List.Item> */}
                          <List.Item>iimjobs</List.Item>
                          <List.Item>Monster</List.Item>
                        </List>
                      </span>
                    </Header>
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row className="header_inputField">
                  <Grid.Column mobile={11} computer={13}>
                    <div>
                      <InputField
                        name="parseUrl"
                        onChange={props.onLinkInputChange}
                        placeholder="Paste your job link here."
                      />
                    </div>
                  </Grid.Column>
                  <Grid.Column
                    mobile={5}
                    computer={3}
                    className="header_btnColumn">
                    <div>
                      <ActionBtn
                        type="button"
                        onClick={props.onLinkChange}
                        loading={props.isLoading}
                        actioaBtnText="Parse"
                      />
                    </div>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </div>
          </Grid.Column>
        </Grid.Row>
      </Grid>

      <div className="header_footerText" />
    </div>
  );
};

// PostJobHeader.propTypes = {

// }

export default PostJobHeader;
