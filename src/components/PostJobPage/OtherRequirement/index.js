import React from "react";
import { Container, Grid, Button } from "semantic-ui-react";
import InfoSection from "../../Sections/InfoSection";
import InputContainer from "../../Forms/InputContainer";
import SelectOptionField from "../../Forms/FormFields/SelectOptionField";
import TextAreaField from "../../Forms/FormFields/TextAreaField";
import getPlaceholder from "../../Forms/FormFields/Placeholder";
import InputFieldLabel from "../../Forms/FormFields/InputFieldLabel";
import PrimaryCard from "../../Cards/PrimaryCard";
import InputField from "../../Forms/FormFields/InputField";
import InfoIconLabel from "../../utils/InfoIconLabel";
import SearchTagField from "../../Forms/FormFields/SearchTagField";
import DownArrowCollaps from "../../utils/DownArrowCollaps";
import { POST_JOB } from "../../../strings";
import SelectOption from "../../Forms/FormFields/SelectOption";
import CheckBoxRightCheck from "../../Forms/FormFields/CheckBoxRightCheck";
import FlatPickr from "react-flatpickr";
import IcCloseIcon from "../../../assets/svg/IcCloseIcon";

import "./index.scss";

function isChecked(value, match) {
  if (value.indexOf(match) === -1) {
    return false;
  }

  return true;
}

const options = [
  {
    key: "Wheelchair Accessible",
    text: "Wheelchair Accessible",
    value: "Wheelchair Accessible"
  },
  {
    key: "Visually Impaired",
    text: "Visually Impaired",
    value: "Visually Impaired"
  },
  {
    key: "Hearing Impaired",
    text: "Hearing Impaired",
    value: "Hearing Impaired"
  }
];

const labelStar = str => {
  return (
    <div>
      <span>{str}</span>
      <span style={{ color: "#0b9ed0" }}>*</span>
    </div>
  );
};

class OtherRequirement extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowPreferenceField: true
    };
  }

  PreferenceField = () => {
    this.setState({
      isShowPreferenceField: false
    });
  };

  render() {
    const {
      data,
      onDisabilityOptionschange,
      onInputChange,
      dataOther,
      onjoiningProbChange,
      onShiftChange,
      onOtherChange
    } = this.props;

    console.log("check time", dataOther);

    return (
      <Container>
        <PrimaryCard>
          <div className="OtherRequirement">
            <InfoSection
              headerSize="large"
              color="blue"
              headerText="Other details"
              rightIcon={<DownArrowCollaps />}
              // collapsible
              isActive={true}>
              {/* <InputContainer label="Differently-abled Friendly">
                <Grid>
                  <Grid.Row>
                    <Grid.Column mobile={16} only="mobile">
                      <InputFieldLabel label="Disability options" />
                    </Grid.Column>
                    <Grid.Column computer={16}>
                      <SearchTagField
                        name="disFriendly"
                        icon={false}
                        placeholder="Please Select Disablity Options"
                        options={options}
                        values={data.disFriendlyTags}
                        onTagChange={onDisabilityOptionschange}
                        search={false}
                      />
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </InputContainer> */}
              <div className="InputGridContainer">
                <Grid>
                  <Grid.Row>
                    <Grid.Column computer={4} className="mobile hidden">
                      <InputFieldLabel
                        label="Working shift preference"
                        infoicon={
                          <InfoIconLabel text="Specify work timings for this job" />
                        }
                      />
                    </Grid.Column>

                    <Grid.Column mobile={16} only="mobile">
                      <InputFieldLabel
                        label="Working shift preference"
                        infoicon={
                          <InfoIconLabel text="Specify work timings for this job" />
                        }
                      />
                    </Grid.Column>
                    <Grid.Column
                      mobile={8}
                      // computer={3}
                      className="mobile_inputField inputGrid_comMin col-width-34">
                      {/* {this.state.isShowPreferenceField ? () : null} */}
                      <div className="working_preferenceField">
                        <FlatPickr
                          onChange={val =>
                            onShiftChange(null, { value: val, name: "time" })
                          }
                          value={dataOther.workDays.time}
                          options={{
                            dateFormat: "H:i",
                            enableTime: true,
                            noCalendar: true,
                            time_24hr: true,
                            defaultHour: "00",
                            defaultMinute: "00",
                            wrap: true
                          }}
                          // data-enable-time
                        >
                          <input
                            placeholder={"Enter Start time"}
                            type="text"
                            data-input
                          />
                          {/* <button type='button' data-toggle>Toggle</button> */}
                          <Button
                            type="button"
                            data-clear
                            disabled={!dataOther.workDays.time}
                            className="preferenceBtn_startTime">
                            Clear
                          </Button>
                        </FlatPickr>

                        {/* <Flatpickr
                          value={dataOther.workDays.time} 
                          options={{ 
                          wrap: true,
                          dateFormat: "H:i",
                          enableTime: true,
                          noCalendar: true,
                          time_24hr: true,
                          defaultHour: "00",
                          defaultMinute: "00"
                        }}
                          onChange={val =>
                            onShiftChange(null, { value: val, name: "time" })
                          }
                        >
                          <input type='text' data-input />
                          <button type='button' data-toggle>Toggle</button>
                          <button type='button' data-clear>Clear</button>
                        </Flatpickr> */}

                        {/* <Button
                          disabled={!dataOther.workDays.time}
                          onClick={val =>
                            onShiftChange(null, { value: "", name: "time" })
                          }
                          type="button"
                          className="preferenceBtn_startTime">
                          
                          Clear
                        </Button> */}
                      </div>
                    </Grid.Column>
                    <Grid.Column
                      computer={1}
                      className="mobile hidden divider_to display-flex-other-addital"
                      textAlign="center">
                      To
                    </Grid.Column>
                    <Grid.Column
                      mobile={8}
                      // computer={3}
                      className="mobile_inputField inputGrid_comMax col-width-34">
                      <div className="working_preferenceField">
                        <FlatPickr
                          onChange={val =>
                            onShiftChange(null, { value: val, name: "days" })
                          }
                          value={dataOther.workDays.days}
                          options={{
                            dateFormat: "H:i",
                            enableTime: true,
                            noCalendar: true,
                            time_24hr: true,
                            defaultHour: "00",
                            defaultMinute: "00",
                            wrap: true
                          }}
                          placeholder={"Enter End time"}>
                          <input
                            placeholder={"Enter End time"}
                            type="text"
                            data-input
                          />
                          {/* <button type='button' data-toggle>Toggle</button> */}
                          <Button
                            type="button"
                            data-clear
                            disabled={!dataOther.workDays.days}
                            className="preferenceBtn_endTime">
                            Clear
                          </Button>
                        </FlatPickr>
                        {/* {
                          dataOther.workDays.days ? (
                            <Button 
                              disabled={!dataOther.workDays.days}
                              onClick={val =>
                                onShiftChange(null, { value: "", name: "days" })
                              }
                              type="button"
                              className="preferenceBtn_endTime">
                              Clear
                            </Button>
                          ) : null
                        } */}
                        {/* <Button
                          disabled={!dataOther.workDays.days}
                          onClick={val =>
                            onShiftChange(null, { value: "", name: "days" })
                          }
                          type="button"
                          className="preferenceBtn_endTime">
                          Clear
                        </Button> */}
                      </div>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>

                <InputContainer
                  label="Joining time"
                  infoicon={
                    <InfoIconLabel text="How early must the candidate join?" />
                  }>
                  <Grid>
                    <Grid.Row>
                      <Grid.Column mobile={16} only="mobile">
                        <InputFieldLabel label="Joining time" />
                      </Grid.Column>
                      <Grid.Column computer={16}>
                        <SelectOption
                          placeholder="Select"
                          value={dataOther.joiningProb.joiningProbability}
                          optionsItem={[
                            {
                              text: "Immediate",
                              value: 0,
                              key: 0
                            },
                            {
                              text: "15 days",
                              value: 15,
                              key: 15
                            },
                            {
                              text: "1 Month",
                              value: 30,
                              key: 30
                            },
                            {
                              text: "2 Months",
                              value: 60,
                              key: 60
                            },
                            {
                              text: "3 Months",
                              value: 90,
                              key: 90
                            }
                          ]}
                          onChange={(e, { value }) =>
                            onjoiningProbChange(e, {
                              value: value,
                              name: "joiningProbability"
                            })
                          }
                        />
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                </InputContainer>

                <InputContainer label="Additional requirments">
                  <Grid>
                    <Grid.Row>
                      <Grid.Column mobile={16} only="mobile">
                        <InputFieldLabel label="Additional requirments" />
                      </Grid.Column>
                      <Grid.Column computer={16}>
                        <SelectOptionField
                          placeholder={getPlaceholder(
                            "Select",
                            "Additional requirments about the job"
                          )}
                          optionsItem={[
                            { value: true, text: "Yes" },
                            { value: "no", text: "No" }
                          ]}
                          name="hasAdditionalDetails"
                          value={dataOther.hasAdditionalDetails}
                          onChange={onInputChange}
                        />
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                </InputContainer>
                {dataOther.hasAdditionalDetails &&
                  dataOther.hasAdditionalDetails !== "no" && (
                    <InputContainer>
                      <TextAreaField
                        onChange={onOtherChange}
                        value={data.dec}
                        name="dec"
                        placeholder="Please specify here.."
                      />
                    </InputContainer>
                  )}

                <InputContainer label="Disability friendly">
                  <Grid>
                    <Grid.Row>
                      <Grid.Column mobile={16} only="mobile">
                        <InputFieldLabel label="Disability friendly" />
                      </Grid.Column>
                      <Grid.Column computer={5} mobile={16}>
                        <div className="other-additional-checkbox">
                          <CheckBoxRightCheck
                            labelTitle=""
                            checked={isChecked(
                              data.disFriendlyTags,
                              "Wheelchair Accessible"
                            )}
                            onChange={() => {
                              onDisabilityOptionschange(
                                "Wheelchair Accessible"
                              );
                            }}
                          />{" "}
                          <p>Wheelchair Accessible</p>
                        </div>
                      </Grid.Column>
                      <Grid.Column computer={5} mobile={16}>
                        <div className="other-additional-checkbox">
                          <CheckBoxRightCheck
                            labelTitle=""
                            checked={isChecked(
                              data.disFriendlyTags,
                              "Visually Impaired"
                            )}
                            onChange={() => {
                              onDisabilityOptionschange("Visually Impaired");
                            }}
                          />{" "}
                          <p>Visually Impaired</p>
                        </div>
                      </Grid.Column>
                      <Grid.Column computer={5} mobile={16}>
                        <div className="other-additional-checkbox">
                          <CheckBoxRightCheck
                            labelTitle=""
                            checked={isChecked(
                              data.disFriendlyTags,
                              "Hearing Impaired"
                            )}
                            onChange={() => {
                              onDisabilityOptionschange("Hearing Impaired");
                            }}
                          />{" "}
                          <p>Hearing Impaired</p>
                        </div>
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                </InputContainer>
              </div>
            </InfoSection>
          </div>
        </PrimaryCard>
      </Container>
    );
  }
}

export default OtherRequirement;
