import React, { Component } from 'react'

import { Image } from "semantic-ui-react";

import PropTypes from "prop-types";

import IcCompany from "../../../../assets/svg/IcCompany";

import "./index.scss";

const CompanyOption = ({ compName, imgUrl, location, tag }) => {
  return (
    <div className="companyOption_dropdown">
      <div className="company_logo">
        {
          imgUrl ? (
            <Image src={`https://s3-us-west-2.amazonaws.com/img-mwf/company/${imgUrl}`} />
          ) : (<IcCompany width="27" height="27" pathcolor="#c8c8c8" rectcolor="#f7f7fb" />)

        }
      </div>
      <div className="companyoption_right">
        <p className="title">{compName} {location ? <span>
          <span className="spanTitle"> | </span> <span className="spanSub">{location}</span>
        </span> : null}</p>
        <p className="subTitle">{tag}</p>
      </div>
    </div>
  )
}



CompanyOption.propTypes = {
  compName: PropTypes.string,
}

CompanyOption.defaultProps = {
  compName: "dell"
}


export default CompanyOption;
