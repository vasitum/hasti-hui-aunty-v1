import React from "react";

import { Grid, Popup, Modal, Image, Responsive } from "semantic-ui-react";
import InputContainer from "../../Forms/InputContainer";
import InputField from "../../Forms/FormFields/InputField";
import SelectOptionField from "../../Forms/FormFields/SelectOptionField";
import QuillText from "../../CardElements/QuillText";
import DropdownMenu from "../../DropdownMenu";
import FlatDefaultBtn from "../../Buttons/FlatDefaultBtn";
import CreateCompanyPage from "../../CreateCompanyPage";
import EditCompanyPage from "../../ModalPageSection/EditCompany";
import InputFieldLabel from "../../Forms/FormFields/InputFieldLabel";
import IcEditIcon from "../../../assets/svg/IcEdit";
import MobileGrid from "../../MobileComponents/MobileGrid";
import getPlaceholder from "../../Forms/FormFields/Placeholder";
import SearchFilterInput from "../../Forms/FormFields/SearchFilterInput";
import InfoIconLabel from "../../utils/InfoIconLabel";
import CheckBoxRightCheck from "../../Forms/FormFields/CheckBoxRightCheck";
import LocationInputField from "../../Forms/FormFields/LocationInputField";
import { Dropdown } from "formsy-semantic-ui-react";
import { POST_JOB } from "../../../strings";

import AutoCompleteSearch from "../../Forms/FormFields/AutoCompleteSearch";

import CompanyOption from "./CompanyOption";

import IcCompany from "../../../assets/svg/IcCompany";

import "./index.scss";

const autoSearchOptionsFeild = [
  { key: "afq2", value: "", flag: "af", text: "Select" },
  { key: "af", value: "af", flag: "af", text: "Afghanistan" },
  { key: "india", value: "india", flag: "india", text: "india" },
  { key: "us", value: "us", flag: "us", text: "US" }
  // { key: 'af', value: 'af', flag: 'af', text: 'Afghanistan' },
  // { key: 'af', value: 'af', flag: 'af', text: 'Afghanistan' },
  // { key: 'af', value: 'af', flag: 'af', text: 'Afghanistan' },
];

const labelStar = str => {
  return (
    <div>
      <span>{str}</span>
      <span style={{ color: "#0b9ed0" }}>*</span>
    </div>
  );
};

const salaryOptions = [
  { key: "INR", text: "INR", value: "INR" },
  { key: "EUR", text: "EUR", value: "EUR" },
  { key: "USD", text: "USD", value: "USD" },
  { key: "JPY", text: "JPY", value: "JPY" },
  { key: "CNY", text: "CNY", value: "CNY" }
];

function getDataOptions(start) {
  let mStart = 1;
  if (start) {
    mStart = start + 1;
  }

  let ret = [];
  for (let idx = mStart; idx < 100; idx++) {
    ret.push({
      text: idx,
      value: idx
    });
  }

  return ret;
}

function getThousandOptions() {
  return [
    {
      text: "00",
      value: "0"
    },
    {
      text: "10",
      value: "1"
    },
    {
      text: "20",
      value: "2"
    },
    {
      text: "30",
      value: "3"
    },
    {
      text: "40",
      value: "4"
    },
    {
      text: "50",
      value: "5"
    },
    {
      text: "60",
      value: "6"
    },
    {
      text: "70",
      value: "7"
    },
    {
      text: "80",
      value: "8"
    },
    {
      text: "90",
      value: "9"
    }
  ];
}

// const InfoIcon = () => {
//     return<span
//               style={{
//                 height: "16px",
//                 width: "16px"
//               }}>
//               <IcInfoIcon pathcolor="#c8c8c8" />
//           </span>
//   // <Popup
//   //   trigger={<IcInfoIcon pathcolor="#c8c8c8" />}
//   //   content="As expected this popup is way off to the bottom"
//   // />
// };

function getSalaryPlaceHolder(currency, type) {
  if (currency === "INR" || !currency) {
    return getPlaceholder(`${type}. lpa`, `${type} (LPA)`);
  }

  return getPlaceholder(
    `${type}. Thousands.`,
    `${type} in thousands per annum`
  );
}

function getDisabledPlaceHolder(length) {
  if (!length) {
    return "No Company Found";
  }

  return "Select company";
}

class JobDetails extends React.Component {
  state = {
    companyModal: false,
    editCompanyModal: false
  };

  onCompanyModalTrigger = ev => {
    ev.preventDefault();
    this.setState({ companyModal: true });
  };

  onEditCompanyModalTrigger = ev => this.setState({ editCompanyModal: true });

  onCompanyModalClose = ev => this.setState({ companyModal: false });

  onEditCompanyModalClose = ev => this.setState({ editCompanyModal: false });

  render() {
    const {
      data,
      onInputChange,
      onLocSelect,
      onLocChange,
      onResChange,
      onBenefitsChange,
      onSalaryChange,
      onDescChange,
      onCompanyCreate,
      onCompanyChange,
      onEnableFormLoader,
      onDisableFormLoader,
      onRemoteChange,
      onModalCreateCompany,
      onModalEditCompany,
      onSalaryMinChange,
      onSalaryMaxChange
    } = this.props;

    // console.log("check company name", data)

    // const CompanyOption = () => {
    //   return (
    //     <div className="companyOption">
    //       {data.com.optionsItem.map((title, idx) => {
    //         return (
    //           <div key={idx}>
    //             <div className="company_logo">
    //               <IcCompany width="27" height="27" pathcolor="#c8c8c8" rectcolor="#f7f7fb" />
    //               {/* <Image src={data.com.optionsItem.imgExt} /> */}
    //             </div>
    //             <div className="companyoption_right">
    //               <p className="title">{title.compName} <span className="spanTitle"> | Gurugram</span> <span className="spanSub"></span></p>
    //               <p className="subTitle">Private note</p>
    //             </div>
    //           </div>
    //         )
    //       })}

    //     </div>
    //   )
    // }

    return (
      <div className="JobDetails">
        {/* <InputContainer
          label={labelStar(`AutoCompleteSearch`)}
        >
          <AutoCompleteSearch />
        </InputContainer> */}
        <InputContainer
          label={labelStar(`Position title`)}
          infoicon={<InfoIconLabel text={POST_JOB.JOB_TITLE_BUBBLE} />}>
          <Grid>
            <Grid.Row>
              {/* only Mobile */}
              <Grid.Column mobile={16} only="mobile">
                <InputFieldLabel
                  label={labelStar(`Job title`)}
                  infoicon={<InfoIconLabel text={POST_JOB.JOB_TITLE_BUBBLE} />}
                />
              </Grid.Column>
              {/* only Mobile */}

              <Grid.Column computer={16}>
                <InputField
                  placeholder={getPlaceholder(
                    "Eg: Urgently looking for UI developer",
                    "Eg: Urgently looking for UI developer"
                  )}
                  value={data.title}
                  inputtype="text"
                  onChange={onInputChange}
                  validationErrors={{
                    isDefaultRequiredValue: "Field is required"
                  }}
                  errorLabel={
                    <span style={{ color: "red", fontSize: "12px" }} />
                  }
                  name="title"
                  required
                />
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </InputContainer>

        <div className="company_field">
          <Grid>
            <Grid.Row>
              <Grid.Column computer={4} className="mobile hidden">
                <InputFieldLabel
                  label={labelStar(`Company’s name`)}
                  infoicon={
                    <InfoIconLabel text={POST_JOB.COMPANY_NAME_BUBBLE} />
                  }
                />
              </Grid.Column>

              <Grid.Column mobile={16} only="mobile">
                <InputFieldLabel
                  label={labelStar(`Company’s name`)}
                  infoicon={
                    <InfoIconLabel text={POST_JOB.COMPANY_NAME_BUBBLE} />
                  }
                />
              </Grid.Column>
              <Grid.Column mobile={16} computer={7}>
                <SelectOptionField
                  // optionsItem={data.com.optionsItem.map((title, idx) => {
                  //   <div key={idx}>
                  //     <CompanyOption
                  //       compName={title.compName}
                  //     />compName
                  //   </div>
                  // })}
                  optionsItem={data.com.optionsItem}
                  placeholder={getDisabledPlaceHolder(
                    data.com
                      ? data.com.optionsItem
                        ? data.com.optionsItem.length
                        : 0
                      : 0
                  )}
                  value={data.com.id}
                  onChange={onCompanyChange}
                  required
                  name="selectCompany"
                  validationErrors={{
                    isDefaultRequiredValueValue: "You need to select a company"
                  }}
                  errorLabel={<span />}
                />
              </Grid.Column>

              <Grid.Column
                mobile={16}
                computer={5}
                className="createNew_companyGrid">
                {data.selectedCompanyData && (
                  // <Modal
                  //   className="modal_form"
                  //   open={this.state.editCompanyModal}
                  //   onClose={this.onEditCompanyModalClose}
                  //   trigger={
                  //     <FlatDefaultBtn
                  //       type="button"
                  //       btntext="Edit"
                  //       onClick={this.onEditCompanyModalTrigger}
                  //       style={{ marginRight: "10px" }}
                  //     />
                  //   }
                  //   closeIcon
                  //   closeOnDimmerClick={false}>
                  //   <EditCompanyPage
                  //     data={data.selectedCompanyData}
                  //     onCompanyCreate={onCompanyCreate}
                  //     onModalClose={this.onEditCompanyModalClose}
                  //     onEnableFormLoader={onEnableFormLoader}
                  //     onDisableFormLoader={onDisableFormLoader}
                  //   />
                  // </Modal>
                  <FlatDefaultBtn
                    type="button"
                    btntext="Edit"
                    onClick={onModalEditCompany}
                    style={{ marginRight: "10px" }}
                  />
                )}
                {/* <Modal
                  className="modal_form"
                  open={this.state.companyModal}
                  onClose={this.onCompanyModalClose}
                  trigger={
                    <FlatDefaultBtn
                      onClick={this.onCompanyModalTrigger}
                      type="button"
                      btnicon={<IcEditIcon pathcolor="#c8c8c8" />}
                      btntext="Create new company"
                    />
                  }
                  closeIcon
                  closeOnDimmerClick={false}>
                  <CreateCompanyPage
                    onCompanyCreate={onCompanyCreate}
                    onModalClose={this.onCompanyModalClose}
                    onEnableFormLoader={onEnableFormLoader}
                    onDisableFormLoader={onDisableFormLoader}
                  />
                </Modal> */}
                <FlatDefaultBtn
                  onClick={onModalCreateCompany}
                  type="button"
                  btnicon={<IcEditIcon pathcolor="#c8c8c8" />}
                  btntext="Create new company"
                />
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>

        <div className="company_field">
          <Grid>
            <Grid.Row>
              <Grid.Column computer={4} className="mobile hidden">
                <InputFieldLabel
                  label={labelStar(`Work location`)}
                  infoicon={<InfoIconLabel text={POST_JOB.LOCATION_BUBBLE} />}
                />
              </Grid.Column>

              <Grid.Column mobile={16} only="mobile">
                <InputFieldLabel
                  label={labelStar(`Work location`)}
                  infoicon={<InfoIconLabel text={POST_JOB.LOCATION_BUBBLE} />}
                />
              </Grid.Column>
              <Grid.Column mobile={16} computer={7}>
                <LocationInputField
                  disabled={data.remote}
                  placeholder={getPlaceholder("Eg: Noida", "Eg: Noida")}
                  required
                  value={data.loc.adrs}
                  onSelect={onLocSelect}
                  onChange={onLocChange}
                  validations={{
                    customLocValidation: (values, value) => {
                      if (data.remote) {
                        return true;
                      }

                      if (!value) {
                        return false;
                      }

                      return true;
                    }
                  }}
                  validationErrors={{
                    customLocValidation: "Location is Required"
                  }}
                  errorLabel={
                    <span style={{ color: "red", fontSize: "12px" }} />
                  }
                />
              </Grid.Column>

              <Grid.Column
                mobile={16}
                computer={5}
                className="createNew_companyGrid">
                <div className="remoteLocation_pastJob">
                  <CheckBoxRightCheck
                    labelTitle=""
                    checked={data.remote}
                    onChange={onRemoteChange}
                  />
                  <p>
                    Remote working{" "}
                    <InfoIconLabel text="Mention the geographical" />
                  </p>
                </div>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>

        {/* <InputContainer
          label={labelStar(`Job location`)}
          infoicon={
            <InfoIconLabel text="Mention the geographical location of the working space" />
          }>
          <Grid>
            <Grid.Row>
              <Grid.Column mobile={16} only="mobile">
                <InputFieldLabel
                  label={labelStar(`Job location`)}
                  infoicon={
                    <InfoIconLabel text="Mention the geographical location of the working space" />
                  }
                />
              </Grid.Column>
              <Grid.Column computer={10}>
                <LocationInputField
                  placeholder={getPlaceholder("Eg: Noida", "Eg: Noida")}
                  value={data.loc.adrs}
                  onSelect={onLocSelect}
                  onChange={onLocChange}
                  required
                  validationErrors={{
                    isDefaultRequiredValue: "Field is required"
                  }}
                  errorLabel={
                    <span style={{ color: "red", fontSize: "12px" }} />
                  }
                />
              </Grid.Column>
              <Grid.Column computer={5}>
                <CheckBoxRightCheck />
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </InputContainer> */}

        <InputContainer
          label={labelStar(`Job role`)}
          infoicon={
            <InfoIconLabel text="Mention the role of prospective employee" />
          }>
          <Grid>
            <Grid.Row>
              <Grid.Column mobile={16} only="mobile">
                <InputFieldLabel
                  label={labelStar(`Job role`)}
                  infoicon={
                    <InfoIconLabel text="Mention the role of prospective employee" />
                  }
                />
              </Grid.Column>
              <Grid.Column computer={16}>
                <AutoCompleteSearch
                  dataType={"ROLE"}
                  placeholder={getPlaceholder(
                    "Eg: Software Developer",
                    "Eg: Software Developer"
                  )}
                  value={data.role}
                  onInputChange={onInputChange}
                  name="role"
                  required
                  validationErrors={{
                    isDefaultRequiredValue: "Field is required"
                  }}
                  errorLabel={
                    <span style={{ color: "red", fontSize: "12px" }} />
                  }
                />
                {/* <InputField
                  placeholder={getPlaceholder(
                    "Eg: Software Developer",
                    "Eg: Software Developer"
                  )}
                  value={data.role}
                  onChange={onInputChange}
                  name="role"
                  required
                  validationErrors={{
                    isDefaultRequiredValue: "Field is required"
                  }}
                  errorLabel={
                    <span style={{ color: "red", fontSize: "12px" }} />
                  }
                /> */}
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </InputContainer>

        <InputContainer
          label={labelStar(`Job type`)}
          infoicon={
            <InfoIconLabel text="Select the type of role you are hiring for" />
          }>
          <Grid>
            <Grid.Row>
              <Grid.Column mobile={16} only="mobile">
                <InputFieldLabel
                  label={labelStar(`Job type`)}
                  infoicon={
                    <InfoIconLabel text="Select the type of role you are hiring for" />
                  }
                />
              </Grid.Column>
              <Grid.Column computer={16}>
                <SelectOptionField
                  placeholder={getPlaceholder("Eg: Full-Time", "Eg: Full-Time")}
                  optionsItem={[
                    { value: "Full-Time", text: "Full-Time" },
                    { value: "Contract", text: "Contract" },
                    { value: "Freelancer", text: "Freelancer" },
                    { value: "Part-Time", text: "Part-Time" }
                  ]}
                  value={data.jobType}
                  name="jobType"
                  onChange={onInputChange}
                  required
                  validationErrors={{
                    isDefaultRequiredValue: "Field is required"
                  }}
                  errorLabel={
                    <span style={{ color: "red", fontSize: "12px" }} />
                  }
                />
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </InputContainer>

        <InputContainer
          label={labelStar(`Job description`)}
          // infoicon={
          //   <InfoIconLabel text="Describe the employee's role in the company" />
          // }
        >
          <Grid>
            <Grid.Row>
              <Grid.Column mobile={16} only="mobile">
                <InputFieldLabel
                  label={labelStar(`Job description`)}
                  // infoicon={
                  //   <InfoIconLabel text="Describe the employee's role in the company" />
                  // }
                />
              </Grid.Column>
              <Grid.Column computer={16}>
                <QuillText
                  name="desc"
                  defaultValue={data.desc}
                  value={data.desc}
                  onChange={onDescChange}
                  placeholder={POST_JOB.DESCRIPTION_PLACEHOLDER}
                  required
                  isDoubleDown
                />
                <div
                  style={{
                    textAlign: "right"
                  }}>
                  <p
                    style={{
                      color: "#a1a1a1"
                    }}>
                    {data.desc.length && data.desc.length !== 11
                      ? data.desc.length
                      : 0}{" "}
                    / 3000
                  </p>
                </div>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </InputContainer>

        {/* <InputContainer
          label="Responsibilities and duties"
          infoicon={
            <InfoIconLabel text="Mention the duties your employee will be conducting on daily basis" />
          }>
          <Grid>
            <Grid.Row>
              <Grid.Column mobile={16} only="mobile">
                <InputFieldLabel
                  label="Responsibilities and duties"
                  infoicon={
                    <InfoIconLabel text="Mention the duties your employee will be conducting on daily basis" />
                  }
                />
              </Grid.Column>
              <Grid.Column computer={16}>
                <QuillText
                  isSmall
                  isDoubleDown
                  name="respAndDuty"
                  defaultValue={data.respAndDuty}
                  value={data.respAndDuty}
                  onChange={onResChange}
                  placeholder="For eg: Software developer who designs, constructs, tests and maintains computer application software."
                />
                <div
                  style={{
                    textAlign: "right"
                  }}>
                  <p
                    style={{
                      color: "#a1a1a1"
                    }}>
                    {data.respAndDuty.length && data.respAndDuty.length !== 11
                      ? data.respAndDuty.length
                      : 0}{" "}
                    / 2000
                  </p>
                </div>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </InputContainer> */}

        {/* <InputContainer
          label="Benefits"
          infoicon={
            <InfoIconLabel text="Mention special benefits you'll offer your employee / candidate" />
          }>
          <Grid>
            <Grid.Row>
              <Grid.Column mobile={16} only="mobile">
                <InputFieldLabel
                  label="Benefits"
                  infoicon={
                    <InfoIconLabel text="Mention special benefits you'll offer your employee / candidate" />
                  }
                />
              </Grid.Column>
              <Grid.Column computer={16}>
                <QuillText
                  isSmall
                  isDoubleDown
                  name="benefits"
                  defaultValue={data.benefits}
                  value={data.benefits}
                  onChange={onBenefitsChange}
                  placeholder="It may include training, mentoring, health insurance, lunch service etc."
                />
                <div
                  style={{
                    textAlign: "right"
                  }}>
                  <p
                    style={{
                      color: "#a1a1a1"
                    }}>
                    {data.benefits.length && data.benefits.length !== 11
                      ? data.benefits.length
                      : 0}{" "}
                    / 2000
                  </p>
                </div>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </InputContainer> */}

        {/* <div className="InputGridContainer">
              <InputGridContainer label="Salary" infoicon={<DropdownMenu />} />
            </div> */}

        <div className="InputGridContainer">
          <Grid>
            <div className="PostJobSalary_mobileContainer">
              <div className="">
                <p>
                  <span>
                    Salary
                    <DropdownMenu
                      onChange={onSalaryChange}
                      value={data.salary.currency}
                      name="currency"
                    />
                    {/* <Dropdown
                      placeholder="INR"
                      fluid
                      selection
                      options={salaryOptions}
                    /> */}
                  </span>
                </p>
              </div>
            </div>
            <Grid.Row className="PostJobSalary_mobileRow">
              <Grid.Column computer={4} className="mobile hidden">
                <InputFieldLabel
                  label="Salary"
                  // infoicon={
                  //   <DropdownMenu
                  //     onChange={onSalaryChange}
                  //     value={data.salary.currency}
                  //     name="currency"
                  //   />
                  // }
                />
              </Grid.Column>
              <Grid.Column computer={2} className="mobile hidden">
                <Dropdown
                  name="currency"
                  className="mobile hidden"
                  placeholder="INR"
                  onChange={onSalaryChange}
                  value={data.salary.currency}
                  fluid
                  selection
                  options={salaryOptions}
                />
              </Grid.Column>
              <Grid.Column computer={2} mobile={16} className="salary-heading">
                <strong>Minimum</strong>
              </Grid.Column>
              <Grid.Column
                mobile={8}
                computer={2}
                className="mobile_inputField inputGrid_comMin">
                {/* <MobileGrid>
                  <div className="mobile_inputLabel">
                    <p>Salary</p>
                  </div>
                </MobileGrid> */}

                {/* <InputField
                  // placeholder={getSalaryPlaceHolder(
                  //   data.salary.currency,
                  //   "Min."
                  // )}
                  placeholder="Eg. 10 Lakh"
                  inputtype="number"
                  min={0}
                  value={data.salary.min}
                  onChange={onSalaryChange}
                  name="min"
                  validations={{
                    customSalMinValidation: (values, value) => {
                      if (Number(!data.salary.max)) {
                        return true;
                      }

                      return Number(value) <= Number(data.salary.max);
                    }
                  }}
                  validationErrors={{
                    customSalMinValidation:
                      "Min salary cannot be greater than max salary"
                  }}
                  errorLabel={
                    <span style={{ color: "red", fontSize: "12px" }} />
                  }
                /> */}
                <div style={{ display: "flex", alignItems: "center" }}>
                  <Dropdown
                    disabled={
                      data.salary.currency === "EUR" ||
                      data.salary.currency === "USD" ||
                      data.salary.currency === "JPY" ||
                      data.salary.currency === "CNY"
                    }
                    placeholder="Eg. 10"
                    fluid
                    selection
                    options={getDataOptions()}
                    search
                    value={
                      data.salary
                        ? data.salary.minSal
                          ? data.salary.minSal.lakh
                          : ""
                        : ""
                    }
                    onChange={onSalaryMinChange}
                    // validations={{
                    //   customSalMinValidation: (values, value) => {
                    //     if (Number(!data.salary.max)) {
                    //       return true;
                    //     }

                    //     return Number(value) <= Number(data.salary.max);
                    //   }
                    // }}
                    // validationErrors={{
                    //   customSalMinValidation:
                    //     "Min salary cannot be greater than max salary"
                    // }}
                    // errorLabel={
                    //   <span style={{ color: "red", fontSize: "12px" }} />
                    // }
                    name="lakh"
                  />
                  <span className="desktop-hidden">Lakh</span>
                </div>
              </Grid.Column>
              <Grid.Column
                computer={1}
                mobile={1}
                className="mobile hidden"
                style={{ display: "flex", alignItems: "center" }}>
                Lakh
              </Grid.Column>
              <Grid.Column
                mobile={8}
                computer={2}
                className="mobile_inputField inputGrid_comMin check-space-mobile">
                {/* <MobileGrid>
                  <div
                    className="mobile_inputLabel"
                    style={{
                      textAlign: "right"
                    }}>
                    <DropdownMenu />
                  </div>
                </MobileGrid> */}

                {/* <InputField
                  // placeholder={getSalaryPlaceHolder(
                  //   data.salary.currency,
                  //   "Max."
                  // )}
                  placeholder="Eg. 10 Thousand"
                  inputtype="number"
                  min={0}
                  value={data.salary.max}
                  onChange={onSalaryChange}
                  validations={{
                    customSalMinValidation: (values, value) => {
                      if (Number(!data.salary.min)) {
                        return true;
                      }

                      return Number(value) >= Number(data.salary.min);
                    }
                  }}
                  validationErrors={{
                    customSalMinValidation:
                      "Max salary cannot be smaller than min salary"
                  }}
                  errorLabel={
                    <span style={{ color: "red", fontSize: "12px" }} />
                  }
                  name="max"
                /> */}
                <div style={{ display: "flex", alignItems: "center" }}>
                  <Dropdown
                    placeholder="Eg. 10"
                    fluid
                    selection
                    options={getThousandOptions()}
                    search
                    value={
                      data.salary
                        ? data.salary.minSal
                          ? data.salary.minSal.thousand
                          : ""
                        : ""
                    }
                    onChange={onSalaryMinChange}
                    name="thousand"
                  />
                  <span className="desktop-hidden">Thousand</span>
                </div>
              </Grid.Column>
              <Grid.Column
                computer={2}
                mobile={2}
                className="mobile hidden"
                style={{
                  display: "flex",
                  alignItems: "center"
                }}>
                Thousand
              </Grid.Column>
            </Grid.Row>
          </Grid>

          <Grid>
            <Grid.Row className="PostJobSalary_mobileRow">
              <Grid.Column computer={4} className="mobile hidden" />
              <Grid.Column computer={2} className="mobile hidden" />
              <Grid.Column
                computer={2}
                mobile={16}
                textAlign="left"
                className="salary-heading">
                <strong>Maximum</strong>
              </Grid.Column>
              <Grid.Column
                mobile={8}
                computer={2}
                className="mobile_inputField inputGrid_comMin">
                {/* <MobileGrid>
                  <div className="mobile_inputLabel">
                    <p>Salary</p>
                  </div>
                </MobileGrid> */}

                {/* <InputField
                  // placeholder={getSalaryPlaceHolder(
                  //   data.salary.currency,
                  //   "Min."
                  // )}
                  placeholder="Eg. 10 Lakh"
                  inputtype="number"
                  min={0}
                  value={data.salary.min}
                  onChange={onSalaryChange}
                  name="min"
                  validations={{
                    customSalMinValidation: (values, value) => {
                      if (Number(!data.salary.max)) {
                        return true;
                      }

                      return Number(value) <= Number(data.salary.max);
                    }
                  }}
                  validationErrors={{
                    customSalMinValidation:
                      "Min salary cannot be greater than max salary"
                  }}
                  errorLabel={
                    <span style={{ color: "red", fontSize: "12px" }} />
                  }
                /> */}

                <div style={{ display: "flex", alignItems: "center" }}>
                  <Dropdown
                    disabled={
                      data.salary.currency === "EUR" ||
                      data.salary.currency === "USD" ||
                      data.salary.currency === "JPY" ||
                      data.salary.currency === "CNY"
                    }
                    placeholder="Eg. 10"
                    fluid
                    selection
                    options={getDataOptions(
                      data.salary
                        ? data.salary.minSal
                          ? data.salary.minSal.lakh
                          : 0
                        : 0
                    ).concat([
                      {
                        text: "99+",
                        value: "99+"
                      }
                    ])}
                    search
                    value={
                      data.salary
                        ? data.salary.maxSal
                          ? data.salary.maxSal.lakh
                          : ""
                        : ""
                    }
                    onChange={onSalaryMaxChange}
                    name="lakh"
                  />
                  <span className="desktop-hidden">Lakh</span>
                </div>
              </Grid.Column>
              <Grid.Column
                computer={1}
                className="mobile hidden"
                style={{ display: "flex", alignItems: "center" }}>
                Lakh
              </Grid.Column>
              {/* <Grid.Column computer={1} className="mobile hidden" /> */}
              <Grid.Column
                mobile={8}
                computer={2}
                className="mobile_inputField inputGrid_comMin check-space-mobile">
                {/* <MobileGrid>
                  <div
                    className="mobile_inputLabel"
                    style={{
                      textAlign: "right"
                    }}>
                    <DropdownMenu />
                  </div>
                </MobileGrid> */}

                {/* <InputField */}
                {/* <SelectOptionField
                  placeholder={getSalaryPlaceHolder(
                    data.salary.currency,
                    "Max."
                  )}
                  optionsItem={[
                    { value: "0", text: "0" },
                    { value: "1", text: "1" },
                    { value: "2", text: "2" },
                    
                  ]}
                  value={data.salary.max}
                  name="Max"
                  min={0}
                  type="number"
                  onChange={onInputChange}
                  // required
                  validations={{
                    customSalMinValidation: (values, value) => {
                      if (Number(!data.salary.min)) {
                        return true;
                      } */}

                {/* <InputField
                  // placeholder={getSalaryPlaceHolder(
                  //   data.salary.currency,
                  //   "Max."
                  // )}
                  placeholder="Eg. 10 Thousand"
                  inputtype="number"
                  min={0}
                  value={data.salary.max}
                  onChange={onSalaryChange}
                  validations={{
                    customSalMinValidation: (values, value) => {
                      if (Number(!data.salary.min)) {
                        return true;
                      }

                      return Number(value) >= Number(data.salary.min);
                    }
                  }}
                  validationErrors={{
                    customSalMinValidation:
                      "Max salary cannot be smaller than min salary"
                  }}
                  errorLabel={
                    <span style={{ color: "red", fontSize: "12px" }} />
                  }
                  name="max"
                /> */}

                <div style={{ display: "flex", alignItems: "center" }}>
                  <Dropdown
                    placeholder="Eg. 10"
                    fluid
                    selection
                    options={getThousandOptions()}
                    search
                    value={
                      data.salary
                        ? data.salary.maxSal
                          ? data.salary.maxSal.thousand
                          : ""
                        : ""
                    }
                    onChange={onSalaryMaxChange}
                    name="thousand"
                  />
                  <span className="desktop-hidden">Thousand</span>
                </div>
              </Grid.Column>
              <Grid.Column
                computer={2}
                className="mobile hidden"
                style={{
                  display: "flex",
                  alignItems: "center"
                }}>
                Thousand
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>
      </div>
    );
  }
}

export default JobDetails;
