import React from "react";

import { Grid } from "semantic-ui-react";

import InfoSection from "../../Sections/InfoSection";
import InputContainer from "../../Forms/InputContainer";
import SelectOptionField from "../../Forms/FormFields/SelectOptionField";
import InputTagField from "../../Forms/FormFields/InputTagField";
import InputField from "../../Forms/FormFields/InputField";
import InputFieldLabel from "../../Forms/FormFields/InputFieldLabel";
import getPlaceholder from "../../Forms/FormFields/Placeholder";
import TextAreaField from "../../Forms/FormFields/TextAreaField";
import IcInfoIcon from "../../../assets/svg/IcInfo";

import InfoIconLabel from "../../utils/InfoIconLabel";
import { POST_JOB } from "../../../strings";

import "./index.scss";

const ExpCountMin = [
  { value: "0", text: "Fresher" },
  { value: 1, text: "1" },
  { value: 2, text: "2" },
  { value: 3, text: "3" },
  { value: 4, text: "4" },
  { value: 5, text: "5" },
  { value: 6, text: "6" },
  { value: 7, text: "7" },
  { value: 8, text: "8" },
  { value: 9, text: "9" },
  { value: 10, text: "10" },
  { value: 11, text: "11" },
  { value: 12, text: "12" },
  { value: 13, text: "13" },
  { value: 14, text: "14" },
  { value: 15, text: "15" },
  { value: 16, text: "16" },
  { value: 17, text: "17" },
  { value: 18, text: "18" },
  { value: 19, text: "19" },
  { value: 20, text: "20" },
  { value: 21, text: "21" },
  { value: 22, text: "22" },
  { value: 23, text: "23" },
  { value: 24, text: "24" },
  { value: 25, text: "25" },
  { value: 26, text: "26" },
  { value: 27, text: "27" },
  { value: 28, text: "28" },
  { value: 29, text: "29" },
  { value: 30, text: "30" },
  { value: 31, text: "31" },
  { value: 32, text: "32" },
  { value: 33, text: "33" },
  { value: 34, text: "34" },
  { value: 35, text: "35" },
  { value: 36, text: "36" },
  { value: 37, text: "37" },
  { value: 38, text: "38" },
  { value: 39, text: "39" },
  { value: 40, text: "40" }
];

const ExpCountMax = [
  { value: "0", text: "Fresher" },
  { value: 1, text: "1" },
  { value: 2, text: "2" },
  { value: 3, text: "3" },
  { value: 4, text: "4" },
  { value: 5, text: "5" },
  { value: 6, text: "6" },
  { value: 7, text: "7" },
  { value: 8, text: "8" },
  { value: 9, text: "9" },
  { value: 10, text: "10" },
  { value: 11, text: "11" },
  { value: 12, text: "12" },
  { value: 13, text: "13" },
  { value: 14, text: "14" },
  { value: 15, text: "15" },
  { value: 16, text: "16" },
  { value: 17, text: "17" },
  { value: 18, text: "18" },
  { value: 19, text: "19" },
  { value: 20, text: "20" },
  { value: 21, text: "21" },
  { value: 22, text: "22" },
  { value: 23, text: "23" },
  { value: 24, text: "24" },
  { value: 25, text: "25" },
  { value: 26, text: "26" },
  { value: 27, text: "27" },
  { value: 28, text: "28" },
  { value: 29, text: "29" },
  { value: 30, text: "30" },
  { value: 31, text: "31" },
  { value: 32, text: "32" },
  { value: 33, text: "33" },
  { value: 34, text: "34" },
  { value: 35, text: "35" },
  { value: 36, text: "36" },
  { value: 37, text: "37" },
  { value: 38, text: "38" },
  { value: 39, text: "39" },
  { value: `40+`, text: "40+" }
];

const labelStar = str => {
  return (
    <div>
      <span>{str}</span>
      <span style={{ color: "#0b9ed0" }}>*</span>
    </div>
  );
};

const infoicon = <IcInfoIcon pathcolor="#c8c8c8" />;

class EligibilityDetails extends React.Component {
  render() {
    const {
      onSkillChange,
      onSKillAdd,
      data,
      onExpChange,
      onInputChange,
      onEduChange,
      onSkillModalChange,
      onSkillModalRemove,
      onSkillModalSave,
      onOtherChange
    } = this.props;

    return (
      <div className="EligibilityDetails">
        <InfoSection
          headerSize="large"
          color="blue"
          InfoSecLeftColumn="14"
          InfoSecRightColumn="2"
          headerText="Eligibility criteria">
          {/* <div className="experience">
            <InputGridContainer label="Experience" infoicon={<InfoIconLabel />} mobileInputFieldLabel="Experience" />
          </div> */}

          <div className="InputGridContainer">
            {/* <MobileGrid>
              <div className="mobile_inputLabel">
                <p>Experience</p>
              </div>
            </MobileGrid> */}
            <Grid>
              <Grid.Row>
                <Grid.Column computer={4} className="mobile hidden">
                  <InputFieldLabel
                    label="Work experience"
                    infoicon={
                      <InfoIconLabel text="Specify the eligibility criteria for the position" />
                    }
                  />
                </Grid.Column>

                <Grid.Column mobile={16} only="mobile">
                  <InputFieldLabel
                    label="Work experience"
                    infoicon={
                      <InfoIconLabel text="Specify the eligibility criteria for the position" />
                    }
                  />
                </Grid.Column>

                <Grid.Column
                  mobile={8}
                  // computer={3}
                  className="mobile_inputField inputGrid_comMin col-width-34">
                  <SelectOptionField
                    placeholder={getPlaceholder(
                      "Min. years",
                      "Minimum in years"
                    )}
                    optionsItem={ExpCountMin}
                    value={data.exp.min}
                    name="min"
                    onChange={onExpChange}
                    // required
                    validations={{
                      customExpValidation: (values, value) => {
                        if (Number(!data.exp.max)) {
                          return true;
                        }

                        return Number(value) <= Number(data.exp.max);
                      }
                    }}
                    validationErrors={{
                      customExpValidation:
                        "Min experience cannot be greater than max experience"
                    }}
                    errorLabel={
                      <span style={{ color: "red", fontSize: "12px" }} />
                    }
                  />

                  {/* <InputField
                    inputtype="number"
                    placeholder={getPlaceholder(
                      "Min. years",
                      "Minimum in years"
                    )}
                    value={data.exp.min}
                    onChange={onExpChange}
                    name="min"
                    min={0}
                    validations={{
                      customExpValidation: (values, value) => {
                        if (Number(!data.exp.max)) {
                          return true;
                        }

                        return Number(value) <= Number(data.exp.max);
                      }
                    }}
                    validationErrors={{
                      customExpValidation:
                        "Min experience cannot be greater than max experience"
                    }}
                    errorLabel={
                      <span style={{ color: "red", fontSize: "12px" }} />
                    }
                  /> */}
                </Grid.Column>
                <Grid.Column
                  computer={1}
                  className="mobile hidden divider_to"
                  textAlign="center">
                  To
                </Grid.Column>
                <Grid.Column
                  mobile={8}
                  // computer={3}
                  className="mobile_inputField inputGrid_comMax col-width-34">
                  {/* <MobileGrid>
                    <div className="mobile_inputLabel" 
                      style={{
                        textAlign: "right"
                      }}
                    >
                      <DropdownMenu />
                    </div>
                  </MobileGrid> */}

                  <SelectOptionField
                    placeholder={getPlaceholder(
                      "Max. years",
                      "Maximum in years"
                    )}
                    optionsItem={ExpCountMax}
                    value={data.exp.max}
                    name="max"
                    onChange={onExpChange}
                    // required
                    validations={{
                      customExpMaxValidation: (values, value) => {
                        if (Number(!data.exp.min)) {
                          return true;
                        }

                        return Number(value) >= Number(data.exp.min);
                      }
                    }}
                    validationErrors={{
                      customExpMaxValidation:
                        "Max experience cannot be smaller than min experience"
                    }}
                    errorLabel={
                      <span style={{ color: "red", fontSize: "12px" }} />
                    }
                  />

                  {/* <InputField
                    inputtype="number"
                    placeholder={getPlaceholder(
                      "Max. years",
                      "Maximum in years"
                    )}
                    value={data.exp.max}
                    onChange={onExpChange}
                    name="max"
                    min={0}
                    validations={{
                      customExpMaxValidation: (values, value) => {
                        if (Number(!data.exp.min)) {
                          return true;
                        }

                        return Number(value) >= Number(data.exp.min);
                      }
                    }}
                    validationErrors={{
                      customExpMaxValidation:
                        "Max experience cannot be smaller than min experience"
                    }}
                    errorLabel={
                      <span style={{ color: "red", fontSize: "12px" }} />
                    }
                  /> */}
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </div>

          <InputContainer
            label={labelStar(`Mandatory skills`)}

            // infoicon={
            //   <InfoIconLabel text="Mention mandatory skills you're seeking in your employee" />
            // }
          >
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only="mobile">
                  <InputFieldLabel label={labelStar(`Mandatory Skills`)} />
                </Grid.Column>
                <Grid.Column computer={16}>
                  <InputTagField
                    currentValues={data.skillFieldCurrentValuesMandatory}
                    options={data.skillFieldOptionsMandatory}
                    showEditBtn={data.skillShowEditBtn}
                    onAdd={onSKillAdd}
                    onChange={onSkillChange}
                    skillsModel={data.skills}
                    onModalItemChange={onSkillModalChange}
                    onModalItemRemove={onSkillModalRemove}
                    onModalSave={onSkillModalSave}
                    placeholder="Eg: HTML CSS JavaScript (separate by Enter)"
                    name="jobSkills"
                    required={true}
                    type="mandatory"
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>

          <InputContainer
            label="Skills (optional)"
            // infoicon={
            //   <InfoIconLabel text="Mention additional skills you will prefer" />
            // }
          >
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only="mobile">
                  <InputFieldLabel label="Skills (optional)" />
                </Grid.Column>
                <Grid.Column computer={16}>
                  <InputTagField
                    currentValues={data.skillFieldCurrentValuesOptional}
                    options={data.skillFieldOptionsOptional}
                    showEditBtn={data.skillShowEditBtn}
                    onAdd={onSKillAdd}
                    onChange={onSkillChange}
                    skillsModel={data.skills}
                    onModalItemChange={onSkillModalChange}
                    onModalItemRemove={onSkillModalRemove}
                    onModalSave={onSkillModalSave}
                    placeholder="Eg: HTML CSS JavaScript (separate  by Enter)"
                    name="jobSkills"
                    type="optional"
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>

          <InputContainer
            label={labelStar(`Educational qualification`)}

            // infoicon={<InfoIconLabel text={POST_JOB.EDUCATION_BUBBLE} />}
          >
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only="mobile">
                  <InputFieldLabel label="Educational qualification" />
                </Grid.Column>
                <Grid.Column computer={16}>
                  <SelectOptionField
                    placeholder={getPlaceholder(
                      "Minimum educational qualification",
                      "Minimum educational qualification"
                    )}
                    optionsItem={[
                      {
                        value:
                          "Class 10th / Secondary School Certificate (SSC) / Matriculation",
                        text:
                          "Class 10th / Secondary School Certificate (SSC) / Matriculation"
                      },
                      {
                        value:
                          "Class 12th / Higher Secondary Certificate (HSC) / Intermediate (+2)",
                        text:
                          "Class 12th / Higher Secondary Certificate (HSC) / Intermediate (+2)"
                      },
                      { value: "Diploma", text: "Diploma" },
                      { value: "Undergraduate", text: "Undergraduate" },
                      {
                        value: "Graduate (Bachelors)",
                        text: "Graduate (Bachelors)"
                      },
                      {
                        value: "Post Graduate (Masters)",
                        text: "Post Graduate (Masters)"
                      },
                      { value: "Doctorate", text: "Doctorate" }
                    ]}
                    value={data.edus.level}
                    onChange={onEduChange}
                    name="level"
                    required
                    errorLabel={
                      <span style={{ color: "red", fontSize: "12px" }} />
                    }
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>
          {/* <InputContainer label="Additional requirments">
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only="mobile">
                  <InputFieldLabel label="Additional requirments" />
                </Grid.Column>
                <Grid.Column computer={16}>
                  <SelectOptionField
                    placeholder={getPlaceholder(
                      "Select",
                      "Additional requirments about the job"
                    )}
                    optionsItem={[
                      { value: true, text: "Yes" },
                      { value: "no", text: "No" }
                    ]}
                    name="hasAdditionalDetails"
                    value={data.hasAdditionalDetails}
                    onChange={onInputChange}
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer> */}
          {/* {data.hasAdditionalDetails && data.hasAdditionalDetails !== "no" && (
            <InputContainer>
              <TextAreaField
                onChange={onOtherChange}
                value={data.other.dec}
                name="dec"
                placeholder="Please specify here.."
              />
            </InputContainer>
          )} */}
        </InfoSection>
      </div>
    );
  }
}

export default EligibilityDetails;
