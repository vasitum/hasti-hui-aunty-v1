import React from "react";

import { Container, Grid, Button, Header, Form } from "semantic-ui-react";
import PageBanner from "../Banners/PageBanner";
import aunty from "../../assets/banners/aunty.png";

import BannerHeader from "./BannerHeader";
import PostJobHeader from "./PostJobHeader";

import InfoSection from "../Sections/InfoSection";
import InputContainer from "../Forms/InputContainer";
import InputField from "../Forms/FormFields/InputField";
import SelectOptionField from "../Forms/FormFields/SelectOptionField";
import InputTagField from "../Forms/FormFields/InputTagField";
import TextAreaField from "../Forms/FormFields/TextAreaField";
import QuillText from "../CardElements/QuillText";

import DropdownMenu from "../DropdownMenu";
import ActionBtn from "../Buttons/ActionBtn";
import FlatDefaultBtn from "../Buttons/FlatDefaultBtn";

import InputGridContainer from "../Forms/InputGridContainer";

import InputFieldLabel from "../Forms/FormFields/InputFieldLabel";
import IcInfoIcon from "../../assets/svg/IcInfo";
import IcEditIcon from "../../assets/svg/IcEdit";
import IcDownArrow from "../../assets/svg/IcDownArrow";
import IcSaveAsDraftIcon from "../../assets/svg/IcSaveAsDraft";
import IcProfileViewIcon from "../../assets/svg/IcProfileView";

import MobileGrid from "../MobileComponents/MobileGrid";

import "./index.scss";

const infoicon = <IcInfoIcon pathcolor="#c8c8c8" />;

class PostJobPage extends React.Component {
  createInput({ type, placeholder }) {
    if (type === "select") {
      return <SelectOptionField placeholder={placeholder} />;
    } else if (type === "text") {
      return <InputField placeholder={placeholder} />;
    } else {
      return <QuillText placeholder={placeholder} />;
    }
  }

  render() {
    const data = [
      {
        label: "Job title*",
        info_icon: infoicon,
        placeholder: "Eg. Required ui designer on urgent basis",
        type: "text"
      },
      {
        label: "Job location*",
        info_icon: infoicon,
        placeholder: "Enter the job location you are hiring for",
        type: "select"
      },
      {
        label: "Functional area*",
        info_icon: infoicon,
        placeholder: "Select or start typing",
        type: "select"
      },
      {
        label: "Job type*",
        info_icon: infoicon,
        placeholder: "Select job type",
        type: "select"
      },
      {
        label: "Job description*",
        info_icon: infoicon,
        placeholder: "Describe the position and its role within your company.",
        type: "quill"
      },
      {
        label: "Job description*",
        info_icon: infoicon,
        placeholder: "Describe the position and its role within your company.",
        type: "quill"
      }
    ];
    return (
      <div className="PostJobPage">
        <div>
          <PageBanner size="medium" image={aunty}>
            <Container>
              <BannerHeader />
            </Container>
          </PageBanner>
        </div>

        <Container>
          <PostJobHeader />
          <Form>
            <div className="postJob_jobDetails panel-card">
              <InfoSection
                headerSize="large"
                color="blue"
                headerText="Job Description"
                collapsible>
                {/* <InputContainer label="Job title*" infoicon={infoicon}>
                  <InputField placeholder="Eg. Required ui designer on urgent basis" />
                </InputContainer> */}

                <div className="company_field">
                  <Grid>
                    <Grid.Row>
                      <Grid.Column
                        computer={4}
                        only="computer large widescreem">
                        <InputFieldLabel
                          label="Company name*"
                          infoicon={infoicon}
                        />
                      </Grid.Column>
                      <Grid.Column mobile={16} computer={8}>
                        <SelectOptionField placeholder="Company name" />
                      </Grid.Column>
                      <Grid.Column mobile={16} computer={4}>
                        <FlatDefaultBtn
                          btnicon={<IcEditIcon pathcolor="#c8c8c8" />}
                          btntext="Create new company"
                        />
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                </div>

                {data.map(({ label, info_icon, placeholder, type }, i) => {
                  return (
                    <InputContainer label={label} infoicon={info_icon}>
                      {this.createInput({ type, placeholder })}
                    </InputContainer>
                  );
                })}

                {/* <InputContainer label="Benefits" infoicon={infoicon}>
                  <QuillText placeholder="This may include training, mentoring, health insurance, commuting support, lunch service, etc." />
                </InputContainer> */}

                <div className="InputGridContainer">
                  <InputGridContainer
                    label="Salary"
                    infoicon={<DropdownMenu />}
                  />
                </div>

                <div className="eligibility_details">
                  <InfoSection
                    headerSize="large"
                    color="blue"
                    headerText="Eligibility details">
                    <div className="experience">
                      <InputGridContainer
                        label="Experience"
                        infoicon={infoicon}
                      />
                    </div>

                    <InputContainer label="Skills*" infoicon={infoicon}>
                      <InputTagField placeholder="Tag" />
                    </InputContainer>

                    <InputContainer
                      label="Minimum educational"
                      infoicon={infoicon}>
                      <SelectOptionField placeholder="Select" />
                    </InputContainer>
                  </InfoSection>
                </div>
              </InfoSection>
            </div>

            <div className="panel-card other_requirement">
              <InfoSection
                headerSize="large"
                color="blue"
                headerText="Other requirement"
                collapsible>
                <InputContainer label="Disability friendly">
                  <SelectOptionField placeholder="Yes" />
                </InputContainer>

                <InputContainer label="Want to specify">
                  <SelectOptionField placeholder="Yes" />
                </InputContainer>

                <InputContainer>
                  <TextAreaField placeholder="Please specify here.." />
                </InputContainer>
              </InfoSection>
            </div>

            <div className="postJob_actionBtn">
              <FlatDefaultBtn
                className="bgTranceparent mobile hidden"
                btnicon={<IcSaveAsDraftIcon height="12" pathcolor="#c8c8c8" />}
                btntext="Save as draft"
              />
              <FlatDefaultBtn
                className="bgTranceparent"
                btnicon={<IcProfileViewIcon height="12" pathcolor="#c8c8c8" />}
                btntext="Preview Job"
              />
              <ActionBtn actioaBtnText="Publish Job" />
            </div>
          </Form>
        </Container>
      </div>
    );
  }
}

export default PostJobPage;
