import React, { Children } from "react";

import { Modal, Button, Grid, Input } from "semantic-ui-react";

import { Form } from "formsy-semantic-ui-react";

import ActionBtn from "../../Buttons/ActionBtn";
import FlatDefaultBtn from "../../Buttons/FlatDefaultBtn";
import InputFieldLabel from "../../Forms/FormFields/InputFieldLabel";
import SearchTagField from "../../Forms/FormFields/SearchTagField";

import SaveLogo from "../../../assets/svg/IcSave";
import IcInfo from "../../../assets/svg/IcInfo";

class DefaultModal extends React.Component {
  state = {
    modalOpen: false
  };

  handleOpen = () => this.setState({ modalOpen: true });

  handleClose = () => this.setState({ modalOpen: false });

  render() {
    const {
      trigger,
      size,
      // open,
      closeIcon,
      children,
      handleClose,
      handleOpen,
      ...restProps
    } = this.props;

    // const { modalOpen } = this.state;
    return (
      <Modal
        trigger={
          <Button
            size="mini"
            onClick={this.handleOpen}
            className="bg-transparent margin-right-10 socialBtn fontSize-12 text-Matterhorn">
            <SaveLogo
              width="15"
              height="15"
              pathcolor={this.state.isSaved ? "#0b9ed0" : "#c8c8c8"}
            />
          </Button>
        }
        size={size}
        open={this.state.modalOpen}
        onClose={this.handleClose}
        {...restProps}
        closeOnDimmerClick={false}>
        {/* {children} */}
        <div className="addLabels_Save">
          <Grid>
            <Grid.Row>
              <Grid.Column width={16}>
                <InputFieldLabel
                  label="Add labels"
                  infoicon={<IcInfo pathcolor="#c8c8c8" />}
                />
              </Grid.Column>
              <Grid.Column width={16}>
                <Form>
                  <SearchTagField name="hello" icon={false} />
                </Form>
              </Grid.Column>
            </Grid.Row>
          </Grid>
          <div className="addLabels_SaveFooter">
            <ActionBtn
              actioaBtnText="Save"
              onClick={e => {
                e.stopPropagation();
                alert("Share");
              }}
            />
            <FlatDefaultBtn
              btntext="Cancel"
              classNames="bgTranceparent"
              onClick={this.handleClose}
            />
          </div>
        </div>
      </Modal>
    );
  }
}

export default DefaultModal;




import React, { Children } from "react";

import { Modal } from "semantic-ui-react";

class DefaultModal extends React.Component {
  state = {
    modalOpen: false
  };

  handleOpen = () => this.setState({ modalOpen: true });

  handleClose = () => this.setState({ modalOpen: false });

  render() {
    const {
      TriggerModal,
      size,
      closeIcon,
      children,
      handleClose,
      handleOpen,
      ...restProps
    } = this.props;

    // const { modalOpen } = this.state;
    return (
      <Modal
        trigger={<TriggerModal onClick={this.handleOpen} />}
        size={size}
        open={this.state.modalOpen}
        onClose={this.handleClose}
        {...restProps}
        closeOnDimmerClick={false}>
        {children}
      </Modal>
    );
  }
}

export default DefaultModal;

