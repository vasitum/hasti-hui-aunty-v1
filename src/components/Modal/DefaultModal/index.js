import React, { Children } from "react";

import { Modal } from "semantic-ui-react";

class DefaultModal extends React.Component {
  state = {
    modalOpen: false
  };

  // handleOpen = () => this.setState({ modalOpen: true });

  handleClose() {
    this.setState({ modalOpen: false });
  }

  render() {
    const {
      trigger,
      size,
      // open,
      closeIcon,
      children,
      handleOpen,
      ...restProps
    } = this.props;

    // const { modalOpen } = this.state;
    return (
      <Modal
        content={children}
        trigger={trigger}
        size={size}
        // open={this.state.modalOpen}
        // onClose={this.handleClose}
        {...restProps}
        closeOnDimmerClick={false}
      />
    );
  }
}

export default DefaultModal;
