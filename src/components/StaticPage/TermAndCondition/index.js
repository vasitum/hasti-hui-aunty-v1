import React from "react";

import { Container, Grid, Button, Header, Form, List } from "semantic-ui-react";

import InfoSection from "../../Sections/InfoSection";

import PageBanner from "../../Banners/PageBanner";
import aunty from "../../../assets/banners/aunty.png";
import InputContainer from "../../Forms/InputContainer";

import InputField from '../../Forms/FormFields/InputField';
import TextAreaField from '../../Forms/FormFields/TextAreaField';

import IcRightCheckIcon from "../../../assets/svg/IcRightCheck";

import IcDownArrow from "../../../assets/svg/IcDownArrow";
import IcInfoIcon from "../../../assets/svg/IcInfo";

// import "./index.scss";

class TermAndCondition extends React.Component {
  render() {
    return (
      <div className="AboutUs StaticPage">
        <PageBanner size="medium" image={aunty}>
          <Container>
            <div className="banner_header">
              <Header as="h1">
                Term & Condition
              </Header>
            </div>
          </Container>
        </PageBanner>

        <Container>
          <div className="card_panel">
            <Header as="h2" className="header_text">
              Questions you may have
            </Header>

            <div className="">
              <List relaxed>

                <List.Item>
                  <List.Content>
                    <List.Header as='h4'>The standard Lorem Ipsum passage, used since the 1500s</List.Header>
                    <List.Description>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                      eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                      veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                      commodo consequat.
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as='h4'>The standard Lorem Ipsum passage, used since the 1500s</List.Header>
                    <List.Description>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                      eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                      veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                      commodo consequat.
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as='h4'>The standard Lorem Ipsum passage, used since the 1500s</List.Header>
                    <List.Description>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                      eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                      veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                      commodo consequat.
                    </List.Description>
                  </List.Content>
                </List.Item>
              </List>
            </div>


          </div>




        </Container>
      </div>
    )
  }
}

export default TermAndCondition;

