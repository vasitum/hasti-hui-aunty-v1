import React from "react";

import { Responsive } from "semantic-ui-react";



import AboutusContainer from "./AboutusContainer";

import AboutusMobile from "./AboutusMobile";

import "./index.scss";

class AboutUs extends React.Component {
  render() {
    return (
      <div className="AboutUs">
        <Responsive maxWidth={1024}>
          <AboutusMobile />
        </Responsive>
        <Responsive minWidth={1025}>
          <AboutusContainer />
        </Responsive>
      </div>
    )
  }
}

export default AboutUs;

