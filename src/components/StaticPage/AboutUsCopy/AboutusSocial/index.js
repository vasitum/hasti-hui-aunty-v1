import React from "react";

import IcLinkedin from "../../../../assets/svg/IcLinkedin";

const AboutusSocial = () => {
  return(
    <div className="AboutusSocial">
      <IcLinkedin />
    </div>
  )
}

export default AboutusSocial;