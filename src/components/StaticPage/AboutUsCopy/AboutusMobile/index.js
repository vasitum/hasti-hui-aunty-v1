import React, { Component } from 'react'

import AboutusContainer from "../AboutusContainer";

import "./index.scss";

class AboutusMobile extends Component {
  render() {
    return (
      <div className="AboutusMobile">
        <AboutusContainer />
      </div>
    )
  }
}

export default AboutusMobile;
