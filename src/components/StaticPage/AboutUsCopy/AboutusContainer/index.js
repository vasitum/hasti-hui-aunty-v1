import React, { Component } from 'react'

import { Container, Header, Responsive } from "semantic-ui-react";
import PageBanner from "../../../Banners/PageBanner";
import aunty from "../../../../assets/banners/aunty.png";
import AboutusHeader from "../AboutusHeader";
import AboutusBody from "../AboutusBody";



import HomePageFooter from "../../../HomePage/HomePageFooter";
import HomePageSubFooter from "../../../HomePage/HomePageFooter/HomePageSubFooter";
// import ComingSoon from "../../../HomePage/HomePageFooter/ComingSoon";
import AboutCompanyContainer from "../../../HomePage/HomePageFooter/AboutCompanyContainer";

import "./index.scss";

class AboutusContainer extends Component {
  render() {
    const { isShowSubFooter } = this.props;
    return (
      <div className="AboutusContainer">
        <PageBanner size="medium" image={aunty}>
          <Container>
            <div className="banner_header">
              <div className="banner_headerInner">
                <Header as="h1">
                  About Us
                </Header>
                <p>To become the most efficient and transparent recruitment platform</p>
              </div>
            </div>
          </Container>
        </PageBanner>
        <Container>
          <AboutusHeader />
          <AboutusBody />
        </Container>

        <Responsive minWidth={1025}>
          <HomePageFooter
            isShowSubFooterTop={<HomePageSubFooter 
              // FootComingSoon={<ComingSoon />} 
            />}
            isShowSubFooter={
              !isShowSubFooter ? (
                <AboutCompanyContainer />
              ) : null
            }
          />
        </Responsive>
      </div>
    )
  }
}

export default AboutusContainer;
