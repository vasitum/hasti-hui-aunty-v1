import React from "react";

import "./index.scss";
import { Header } from "semantic-ui-react";

const AboutusHeader = () => {
  return (
    <div className="AboutusHeader">
      <Header as="h2">Inception</Header>
      <p className="Headtitle">
        While the world was toying with the idea of merging AI and
        Traditional recruitment, Vasitum was patiently being
        sculptured to create a platform that aimed at curtailing
        manual processing. In 2016, Vikram Wadhwan
        (- Founder, Vasitum) brought to life his dream of easing
        out recruitment functions - digitizing screening and
        interview process; by enabling AI to boost quality of hire.
      </p>
      <p className="HeadSubTitle">
        “Changing the way employers hire and people get hired” <span>
          - Vikram Wadhawan
      </span>
      </p>
    </div>
  )
}

export default AboutusHeader;