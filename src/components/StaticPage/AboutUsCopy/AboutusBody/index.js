import React from "react";

import { Header, Image, Grid, Responsive } from "semantic-ui-react";

import AboutusCard from "./AboutusCard";
import Logouser from "../../../../assets/img/7824.png";
import BreaKutArrow from "../../../../assets/img/8338.png";
import AboutLogoUser1 from "../../../../assets/img/7873.png";

import AboutusSocial from "../AboutusSocial";
import "./index.scss";


const CardData = [
  {
    title: "Robin Gupta",
  },
  {
    title: "Akash Jain",
  },
  {
    title: "Ishita Verma",
  },
  {
    title: "John Martin",
  },
  {
    title: "Akash",
  },
  {
    title: "Robin",
  },
  {
    title: "Ishita",
  },
  {
    title: "John Martin",
  },
  
]


const AboutusBody = props => {
  const { isShowUserLogo } = props;
  return (
    <div className="AboutusBody">
      <div className="AboutusBody_header">
        <Header as="h2">Our Team</Header>
      </div>
      <AboutusCard className="AboutusCard_container">
        <div className="AboutusCard_left">
          <div className="header_left">
            <Image src={Logouser} alt={"Vikram Wadhawan"}/>
          </div>
          <div className="body_left">
            <p className="title">Vikram Wadhawan</p>
            <p className="subTitle">Founder</p>
            <AboutusSocial />
          </div>
        </div>
        <div className="AboutusCard_right">
          <Image src={BreaKutArrow} />
          <p>While the world was toying with the idea of merging AI and
             Traditional recruitment, Vasitum was patiently being
             sculptured to create a platform that aimed at curtailing
             manual processing. In 2016
          </p>
        </div>
      </AboutusCard>

      <div className="aboutPeople_CardContainer">
        <Grid>
          <Grid.Row>
            {CardData.map((dataTitle, idx) => {
              return (
                <Grid.Column
                  className="aboutPeople_CardGrid"
                  width={4}
                  key={idx}
                >
                  <div className="aboutPeople_Card">
                    <div className="aboutPeople_CardHover">
                      <div className="aboutPeople_CardHoverHeader">
                        <Image src={AboutLogoUser1} />
                      </div>
                      <div className="aboutPeople_CardHoverBody">
                        <p className="title">{dataTitle.title}</p>
                        <p className="subTitle">UI Lead</p>
                      </div>
                    </div>
                    <div className="aboutPeople_CardShow">
                      <div className="aboutPeople_CardShowHeader">
                        {/* {isShowUserLogo ? (): null} */}
                        <Responsive maxWidth={1024}>
                          <Image src={AboutLogoUser1} verticalAlign={"middle"} />
                        </Responsive>
                        <p className="title">{dataTitle.title}</p>
                        <p className="subTitle">UI Lead</p>
                        <AboutusSocial />
                      </div>
                      <div className="aboutPeople_CardShowBody">
                        <Image src={BreaKutArrow} verticalAlign={"middle"} />
                        <p className="title">
                          While the world was toying with the idea of
                          merging AI and Traditional recruitment, Vasitum
                          was patiently being sculptured to create In 2016
                    </p>
                      </div>
                    </div>
                  </div>
                </Grid.Column>
              )
            })}

          </Grid.Row>
        </Grid>
      </div>

    </div>
  )
}

export default AboutusBody;