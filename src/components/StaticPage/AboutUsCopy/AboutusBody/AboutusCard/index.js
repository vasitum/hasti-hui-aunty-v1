import React from "react";

import "./index.scss";

const AboutusCard = props => {
  const { children, ...resProps } = props;
  return (
    <div className="AboutusCard" {...resProps}>
      {children}
    </div>
  )
}

export default AboutusCard;