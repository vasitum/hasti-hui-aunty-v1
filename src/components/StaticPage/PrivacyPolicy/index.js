import React from "react";
import PrivacyPolicyContainer from "./PrivacyPolicyContainer";
import PrivacyPolicyMobile from "./PrivacyPolicyMobile";
import "./index.scss";
import { Responsive } from "semantic-ui-react";
import ReactGA from "react-ga";
import { Helmet } from "react-helmet";
class PrivacyPolicy extends React.Component {

  componentDidMount() {
    ReactGA.pageview('/privacy-policy');
  }

  render() {
    return (
      <div className="AboutUs StaticPage">
        <Helmet >
          <meta charSet="utf-8" />
          <title>
            AI Recruitment Terms and Conditions |Privacy Policy – Vasitum 
          </title>
          <meta
            name="title"
            content="AI Recruitment Terms and Conditions |Privacy Policy – Vasitum "
          />
          <meta
            name="twitter:title"
            content="AI Recruitment Terms and Conditions |Privacy Policy – Vasitum "
          />
          <meta
            property="og:title"
            content="AI Recruitment Terms and Conditions |Privacy Policy – Vasitum "
          />
          <meta
            property="og:description"
            content={`Vasitum helps with job search, recruitment process and resume creator services. It ensures user-data is protected by strong terms and conditions, and legal privacy policy.`}
          />
          <meta
            property="twitter:description"
            content={`Vasitum helps with job search, recruitment process and resume creator services. It ensures user-data is protected by strong terms and conditions, and legal privacy policy.`}
          />
          <link rel="canonical" href="https://vasitum.com/privacy-policy" />
        </Helmet>

        <Responsive maxWidth={1024}>
          <PrivacyPolicyMobile />
        </Responsive>
        <Responsive minWidth={1025}>
          <PrivacyPolicyContainer />
        </Responsive>


      </div>
    );
  }
}

export default PrivacyPolicy;
