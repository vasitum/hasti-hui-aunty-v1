import React, { Component } from "react";
import { Container, Grid, Button, Header, List } from "semantic-ui-react";
import PageBanner from "../../../Banners/PageBanner";
import aunty from "../../../../assets/banners/aunty.png";
import { PrivacyPolicy } from "../../../../strings";

export default class PrivacyPolicyContainer extends Component {
  render() {
    return (
      <div>
        <PageBanner size="medium" image={aunty}>
          <Container>
            <div className="banner_header">
              <Header as="h1">{PrivacyPolicy.HEADER_TITLE}</Header>
            </div>
          </Container>
        </PageBanner>
        <Container>
          <div className="card_panel">
            <div>
              <List relaxed>
                <List.Item>
                  <List.Content>
                    <List.Description className="text">
                      {PrivacyPolicy.MAIN_TITLE1}
                    </List.Description>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Content>
                    <List.Description className="text">
                      {PrivacyPolicy.MAIN_TITLE2}
                    </List.Description>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Content>
                    <List.Description className="text">
                      {PrivacyPolicy.MAIN_TITLE3}
                    </List.Description>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="header_text">
                      {PrivacyPolicy.DEFINITION.HEADER_TITLE}
                    </List.Header>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      Service
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.DEFINITION.SERVICE}
                    </List.Description>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      Personal Data
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.DEFINITION.PERSONAL_DATA}
                    </List.Description>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      Usage Data
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.DEFINITION.USAGE_DATA}
                    </List.Description>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      Cookies
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.DEFINITION.COOKIES}
                    </List.Description>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      Data Controller
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.DEFINITION.DATA_CONTROLLER}
                    </List.Description>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      Data Processors (or Service Providers)
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.DEFINITION.DATA_PROCESSOR}
                    </List.Description>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      Data Subject (or User)
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.DEFINITION.DATA_SUBJECT}
                    </List.Description>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="header_text">
                      {PrivacyPolicy.INFO_COLLECTION.HEADING}
                    </List.Header>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Description className="text">
                      {PrivacyPolicy.INFO_COLLECTION.MAIN}
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="header_text">
                      {PrivacyPolicy.INFO_COLLECTION.SUB_HEADING}
                    </List.Header>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {PrivacyPolicy.INFO_COLLECTION.PERSONAL_DATA.HEADING}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.INFO_COLLECTION.PERSONAL_DATA.MAIN}
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Description className="text">
                      <List bulleted>
                        <List.Item>
                          {PrivacyPolicy.INFO_COLLECTION.PERSONAL_DATA.A}
                        </List.Item>
                        <List.Item>
                          {PrivacyPolicy.INFO_COLLECTION.PERSONAL_DATA.B}
                        </List.Item>
                        <List.Item>
                          {PrivacyPolicy.INFO_COLLECTION.PERSONAL_DATA.C}
                        </List.Item>
                        <List.Item>
                          {PrivacyPolicy.INFO_COLLECTION.PERSONAL_DATA.D}
                        </List.Item>
                        <List.Item>
                          {PrivacyPolicy.INFO_COLLECTION.PERSONAL_DATA.E}
                        </List.Item>
                        <List.Item>
                          {PrivacyPolicy.INFO_COLLECTION.PERSONAL_DATA.F}
                        </List.Item>
                        <List.Item>
                          {PrivacyPolicy.INFO_COLLECTION.PERSONAL_DATA.G}
                        </List.Item>
                        <List.Item>
                          {PrivacyPolicy.INFO_COLLECTION.PERSONAL_DATA.H}
                        </List.Item>
                        <List.Item>
                          {PrivacyPolicy.INFO_COLLECTION.PERSONAL_DATA.I}
                        </List.Item>
                      </List>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Description className="text">
                      {PrivacyPolicy.INFO_COLLECTION.PERSONAL_DATA.MAIN1}
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {PrivacyPolicy.INFO_COLLECTION.USAGE_DATA.HEADING}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.INFO_COLLECTION.USAGE_DATA.MAIN_1}
                    </List.Description>
                    <List.Description className="text p-tag">
                      {PrivacyPolicy.INFO_COLLECTION.USAGE_DATA.MAIN_2}
                    </List.Description>
                    <List.Description className="text p-tag">
                      {PrivacyPolicy.INFO_COLLECTION.USAGE_DATA.MAIN_3}
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {PrivacyPolicy.INFO_COLLECTION.LOCATION_DATA.HEADING}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.INFO_COLLECTION.LOCATION_DATA.MAIN_1}
                    </List.Description>
                    <List.Description className="text p-tag">
                      {PrivacyPolicy.INFO_COLLECTION.LOCATION_DATA.MAIN_2}
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {PrivacyPolicy.INFO_COLLECTION.TRACKING_DATA.HEADING}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.INFO_COLLECTION.TRACKING_DATA.MAIN_1}
                    </List.Description>
                    <List.Description className="text p-tag">
                      {PrivacyPolicy.INFO_COLLECTION.TRACKING_DATA.MAIN_2}
                    </List.Description>
                    <List.Description className="text p-tag">
                      {PrivacyPolicy.INFO_COLLECTION.TRACKING_DATA.MAIN_3}
                    </List.Description>
                    <List.Description className="text p-tag">
                      {PrivacyPolicy.INFO_COLLECTION.TRACKING_DATA.MAIN_4}
                    </List.Description>
                    <List.Description className="text p-tag">
                      <List bulleted>
                        <List.Item>
                          {PrivacyPolicy.INFO_COLLECTION.TRACKING_DATA.A}
                        </List.Item>
                        <List.Item>
                          {PrivacyPolicy.INFO_COLLECTION.TRACKING_DATA.B}
                        </List.Item>
                        <List.Item>
                          {PrivacyPolicy.INFO_COLLECTION.TRACKING_DATA.C}
                        </List.Item>
                      </List>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="header_text mb-10">
                      {PrivacyPolicy.INFO_COLLECTION.USE_OF_DATA.HEADING}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.INFO_COLLECTION.USE_OF_DATA.MAIN_1}
                    </List.Description>
                    <List.Description className="text p-tag">
                      <List bulleted>
                        <List.Item>
                          {PrivacyPolicy.INFO_COLLECTION.USE_OF_DATA.A}
                        </List.Item>
                        <List.Item>
                          {PrivacyPolicy.INFO_COLLECTION.USE_OF_DATA.B}
                        </List.Item>
                        <List.Item>
                          {PrivacyPolicy.INFO_COLLECTION.USE_OF_DATA.C}
                        </List.Item>
                        <List.Item>
                          {PrivacyPolicy.INFO_COLLECTION.USE_OF_DATA.D}
                        </List.Item>
                        <List.Item>
                          {PrivacyPolicy.INFO_COLLECTION.USE_OF_DATA.E}
                        </List.Item>
                        <List.Item>
                          {PrivacyPolicy.INFO_COLLECTION.USE_OF_DATA.F}
                        </List.Item>
                        <List.Item>
                          {PrivacyPolicy.INFO_COLLECTION.USE_OF_DATA.G}
                        </List.Item>
                        <List.Item>
                          {PrivacyPolicy.INFO_COLLECTION.USE_OF_DATA.H}
                        </List.Item>
                      </List>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="header_text mb-10">
                      {PrivacyPolicy.INFO_COLLECTION.LEGAL_DATA.HEADING}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.INFO_COLLECTION.LEGAL_DATA.MAIN_1}
                    </List.Description>
                    <List.Description className="text p-tag">
                      {PrivacyPolicy.INFO_COLLECTION.LEGAL_DATA.MAIN_2}
                    </List.Description>
                    <List.Description className="text p-tag">
                      <List bulleted>
                        <List.Item>
                          {PrivacyPolicy.INFO_COLLECTION.LEGAL_DATA.A}
                        </List.Item>
                        <List.Item>
                          {PrivacyPolicy.INFO_COLLECTION.LEGAL_DATA.B}
                        </List.Item>
                        <List.Item>
                          {PrivacyPolicy.INFO_COLLECTION.LEGAL_DATA.C}
                        </List.Item>
                        <List.Item>
                          {PrivacyPolicy.INFO_COLLECTION.LEGAL_DATA.D}
                        </List.Item>
                        <List.Item>
                          {PrivacyPolicy.INFO_COLLECTION.LEGAL_DATA.E}
                        </List.Item>
                      </List>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="header_text mb-10">
                      {PrivacyPolicy.INFO_COLLECTION.RETAIN_DATA.HEADING}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.INFO_COLLECTION.RETAIN_DATA.MAIN_1}
                    </List.Description>
                    <List.Description className="text p-tag">
                      {PrivacyPolicy.INFO_COLLECTION.RETAIN_DATA.MAIN_2}
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="header_text mb-10">
                      {PrivacyPolicy.INFO_COLLECTION.TRANSFER_DATA.HEADING}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.INFO_COLLECTION.TRANSFER_DATA.MAIN_1}
                    </List.Description>
                    <List.Description className="text p-tag">
                      {PrivacyPolicy.INFO_COLLECTION.TRANSFER_DATA.MAIN_2}
                    </List.Description>
                    <List.Description className="text p-tag">
                      {PrivacyPolicy.INFO_COLLECTION.TRANSFER_DATA.MAIN_3}
                    </List.Description>
                    <List.Description className="text p-tag">
                      {PrivacyPolicy.INFO_COLLECTION.TRANSFER_DATA.MAIN_4}
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="header_text">
                      {PrivacyPolicy.INFO_COLLECTION.DISCLOSURE_DATA.HEADING}
                    </List.Header>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {
                        PrivacyPolicy.INFO_COLLECTION.DISCLOSURE_DATA
                          .BUSINESS_TRANS.HEADING
                      }
                    </List.Header>
                    <List.Description className="text">
                      {
                        PrivacyPolicy.INFO_COLLECTION.DISCLOSURE_DATA
                          .BUSINESS_TRANS.MAIN
                      }
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {
                        PrivacyPolicy.INFO_COLLECTION.DISCLOSURE_DATA.LAW_ENFOR
                          .HEADING
                      }
                    </List.Header>
                    <List.Description className="text">
                      {
                        PrivacyPolicy.INFO_COLLECTION.DISCLOSURE_DATA.LAW_ENFOR
                          .MAIN
                      }
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {
                        PrivacyPolicy.INFO_COLLECTION.DISCLOSURE_DATA
                          .LEGAL_ENFOR.HEADING
                      }
                    </List.Header>
                    <List.Description className="text">
                      {
                        PrivacyPolicy.INFO_COLLECTION.DISCLOSURE_DATA
                          .LEGAL_ENFOR.MAIN
                      }
                    </List.Description>
                    <List.Description className="text p-tag">
                      <List bulleted>
                        <List.Item>
                          {
                            PrivacyPolicy.INFO_COLLECTION.DISCLOSURE_DATA
                              .LEGAL_ENFOR.A
                          }
                        </List.Item>
                        <List.Item>
                          {
                            PrivacyPolicy.INFO_COLLECTION.DISCLOSURE_DATA
                              .LEGAL_ENFOR.B
                          }
                        </List.Item>
                        <List.Item>
                          {
                            PrivacyPolicy.INFO_COLLECTION.DISCLOSURE_DATA
                              .LEGAL_ENFOR.C
                          }
                        </List.Item>
                        <List.Item>
                          {
                            PrivacyPolicy.INFO_COLLECTION.DISCLOSURE_DATA
                              .LEGAL_ENFOR.D
                          }
                        </List.Item>
                        <List.Item>
                          {
                            PrivacyPolicy.INFO_COLLECTION.DISCLOSURE_DATA
                              .LEGAL_ENFOR.E
                          }
                        </List.Item>
                      </List>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="header_text mb-10">
                      {PrivacyPolicy.INFO_COLLECTION.SECURE_DATA.HEADING}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.INFO_COLLECTION.SECURE_DATA.MAIN}
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="header_text mb-10">
                      {PrivacyPolicy.CALOPPA.HEADING}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.CALOPPA.MAIN_1}
                    </List.Description>
                    <List.Description className="text p-tag">
                      {PrivacyPolicy.CALOPPA.MAIN_2}
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="header_text mb-10">
                      {PrivacyPolicy.GDPR.HEADING}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.GDPR.MAIN_1}
                    </List.Description>
                    <List.Description className="text p-tag">
                      {PrivacyPolicy.GDPR.MAIN_2}
                    </List.Description>
                    <List.Description className="text p-tag">
                      {PrivacyPolicy.GDPR.MAIN_3}
                    </List.Description>
                    <List.Description className="text p-tag">
                      <strong>{PrivacyPolicy.GDPR.SUB_MAIN_3.HEADING1}</strong>
                      {PrivacyPolicy.GDPR.SUB_MAIN_3.CONTENT1}
                    </List.Description>
                    <List.Description className="text p-tag">
                      <strong>{PrivacyPolicy.GDPR.SUB_MAIN_3.HEADING2}</strong>
                      {PrivacyPolicy.GDPR.SUB_MAIN_3.CONTENT2}
                    </List.Description>
                    <List.Description className="text p-tag">
                      <strong>{PrivacyPolicy.GDPR.SUB_MAIN_3.HEADING3}</strong>
                      {PrivacyPolicy.GDPR.SUB_MAIN_3.CONTENT3}
                    </List.Description>
                    <List.Description className="text p-tag">
                      <strong>{PrivacyPolicy.GDPR.SUB_MAIN_3.HEADING4}</strong>
                      {PrivacyPolicy.GDPR.SUB_MAIN_3.CONTENT4}
                    </List.Description>
                    <List.Description className="text p-tag">
                      <strong>{PrivacyPolicy.GDPR.SUB_MAIN_3.HEADING5}</strong>
                      {PrivacyPolicy.GDPR.SUB_MAIN_3.CONTENT5}
                    </List.Description>
                    <List.Description className="text p-tag">
                      <strong>{PrivacyPolicy.GDPR.SUB_MAIN_3.HEADING6}</strong>
                      {PrivacyPolicy.GDPR.SUB_MAIN_3.CONTENT6}
                    </List.Description>
                    <List.Description className="text p-tag">
                      {PrivacyPolicy.GDPR.MAIN_4}
                    </List.Description>
                    <List.Description className="text p-tag">
                      {PrivacyPolicy.GDPR.MAIN_5}
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="header_text mb-10">
                      {PrivacyPolicy.SERVICE_PROVIDER.HEADING}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.GDPR.MAIN_1}
                    </List.Description>
                    <List.Description className="text p-tag">
                      {PrivacyPolicy.GDPR.MAIN_2}
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="header_text mb-10">
                      {PrivacyPolicy.SERVICE_PROVIDER.ANALYTIC.HEADING}
                    </List.Header>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Content>
                    <List.Description className="text">
                      {PrivacyPolicy.SERVICE_PROVIDER.ANALYTIC.MAIN_1}
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {PrivacyPolicy.SERVICE_PROVIDER.ANALYTIC.SUB_HEADING_1}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.SERVICE_PROVIDER.ANALYTIC.SUB_HE_CONTENT_1}{" "}
                      <a
                        href={
                          PrivacyPolicy.SERVICE_PROVIDER.ANALYTIC
                            .SUB_HE_CO_LINK1
                        }>
                        here
                      </a>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {PrivacyPolicy.SERVICE_PROVIDER.ANALYTIC.SUB_HEADING_2}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.SERVICE_PROVIDER.ANALYTIC.SUB_HE_CONTENT_2}
                      <a
                        href={
                          PrivacyPolicy.SERVICE_PROVIDER.ANALYTIC
                            .SUB_HE_CO2_LINK1
                        }>
                        here
                      </a>{" "}
                      {
                        PrivacyPolicy.SERVICE_PROVIDER.ANALYTIC
                          .SUB_HE_CONTENT_21
                      }{" "}
                      <a
                        href={
                          PrivacyPolicy.SERVICE_PROVIDER.ANALYTIC
                            .SUB_HE_CO2_LINK2
                        }>
                        here
                      </a>{" "}
                      {
                        PrivacyPolicy.SERVICE_PROVIDER.ANALYTIC
                          .SUB_HE_CONTENT_23
                      }{" "}
                      <a
                        href={
                          PrivacyPolicy.SERVICE_PROVIDER.ANALYTIC
                            .SUB_HE_CO2_LINK3
                        }>
                        here
                      </a>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {PrivacyPolicy.SERVICE_PROVIDER.ANALYTIC.SUB_HEADING_3}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.SERVICE_PROVIDER.ANALYTIC.SUB_HE_CONTENT_3}{" "}
                      <a
                        href={
                          PrivacyPolicy.SERVICE_PROVIDER.ANALYTIC
                            .SUB_HE_CO3_LINK1
                        }>
                        here
                      </a>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {PrivacyPolicy.SERVICE_PROVIDER.ANALYTIC.SUB_HEADING_4}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.SERVICE_PROVIDER.ANALYTIC.SUB_HE_CONTENT_4}{" "}
                      <a
                        href={
                          PrivacyPolicy.SERVICE_PROVIDER.ANALYTIC
                            .SUB_HE_CO4_LINK1
                        }>
                        here
                      </a>{" "}
                      {
                        PrivacyPolicy.SERVICE_PROVIDER.ANALYTIC
                          .SUB_HE_CONTENT_41
                      }{" "}
                      <a
                        href={
                          PrivacyPolicy.SERVICE_PROVIDER.ANALYTIC
                            .SUB_HE_CO4_LINK2
                        }>
                        here
                      </a>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {PrivacyPolicy.SERVICE_PROVIDER.ANALYTIC.SUB_HEADING_5}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.SERVICE_PROVIDER.ANALYTIC.SUB_HE_CONTENT_5}{" "}
                      <a
                        href={
                          PrivacyPolicy.SERVICE_PROVIDER.ANALYTIC
                            .SUB_HE_CO5_LINK1
                        }>
                        here
                      </a>{" "}
                      {
                        PrivacyPolicy.SERVICE_PROVIDER.ANALYTIC
                          .SUB_HE_CONTENT_51
                      }{" "}
                      <a
                        href={
                          PrivacyPolicy.SERVICE_PROVIDER.ANALYTIC
                            .SUB_HE_CO5_LINK2
                        }>
                        here
                      </a>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {PrivacyPolicy.SERVICE_PROVIDER.ANALYTIC.SUB_HEADING_6}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.SERVICE_PROVIDER.ANALYTIC.SUB_HE_CONTENT_6}{" "}
                      <a
                        href={
                          PrivacyPolicy.SERVICE_PROVIDER.ANALYTIC
                            .SUB_HE_CO6_LINK1
                        }>
                        here
                      </a>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="header_text mb-10">
                      {PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING.HEADING}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING.MAIN_1}
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {
                        PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                          .SUB_HEADING_1
                      }
                    </List.Header>
                    <List.Description className="text">
                      {
                        PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                          .SUB_HE_CONTENT_1
                      }{" "}
                      <a
                        href={
                          PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                            .SUB_HE_CO1_LINK1
                        }>
                        here
                      </a>{" "}
                      {
                        PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                          .SUB_HE_CONTENT_11
                      }{" "}
                      <a
                        href={
                          PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                            .SUB_HE_CO1_LINK2
                        }>
                        here
                      </a>{" "}
                      {
                        PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                          .SUB_HE_CONTENT_12
                      }{" "}
                      <a
                        href={
                          PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                            .SUB_HE_CO1_LINK3
                        }>
                        here
                      </a>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {
                        PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                          .SUB_HEADING_2
                      }
                    </List.Header>
                    <List.Description className="text">
                      {
                        PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                          .SUB_HE_CONTENT_2
                      }{" "}
                      <a
                        href={
                          PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                            .SUB_HE_CO2_LINK1
                        }>
                        here
                      </a>{" "}
                      {
                        PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                          .SUB_HE_CONTENT_21
                      }{" "}
                      <a
                        href={
                          PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                            .SUB_HE_CO2_LINK2
                        }>
                        here
                      </a>{" "}
                      {
                        PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                          .SUB_HE_CONTENT_22
                      }{" "}
                      <a
                        href={
                          PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                            .SUB_HE_CO2_LINK3
                        }>
                        here
                      </a>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Description className="text">
                      {
                        PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                          .SUB_HE_CONTENT_23
                      }{" "}
                      <a
                        href={
                          PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                            .SUB_HE_CO2_LINK4
                        }>
                        here
                      </a>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {
                        PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                          .SUB_HEADING_3
                      }
                    </List.Header>
                    <List.Description className="text">
                      {
                        PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                          .SUB_HE_CONTENT_3
                      }{" "}
                      <a
                        href={
                          PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                            .SUB_HE_CO3_LINK1
                        }>
                        here
                      </a>{" "}
                      and{" "}
                      <a
                        href={
                          PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                            .SUB_HE_CO3_LINK2
                        }>
                        here
                      </a>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Description className="text">
                      {
                        PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                          .SUB_HE_CONTENT_31
                      }{" "}
                      <a
                        href={
                          PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                            .SUB_HE_CO3_LINK3
                        }>
                        here
                      </a>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {
                        PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                          .SUB_HEADING_4
                      }
                    </List.Header>
                    <List.Description className="text">
                      {
                        PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                          .SUB_HE_CONTENT_4
                      }{" "}
                      <a
                        href={
                          PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                            .SUB_HE_CO4_LINK1
                        }>
                        here
                      </a>{" "}
                      {
                        PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                          .SUB_HE_CONTENT_41
                      }{" "}
                      <a
                        href={
                          PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                            .SUB_HE_CO4_LINK2
                        }>
                        here
                      </a>{" "}
                      {
                        PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                          .SUB_HE_CONTENT_42
                      }{" "}
                      <a
                        href={
                          PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                            .SUB_HE_CO4_LINK3
                        }>
                        here
                      </a>{" "}
                      {
                        PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                          .SUB_HE_CONTENT_43
                      }{" "}
                      <a
                        href={
                          PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                            .SUB_HE_CO4_LINK4
                        }>
                        here
                      </a>{" "}
                      {
                        PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                          .SUB_HE_CONTENT_44
                      }{" "}
                      <a
                        href={
                          PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                            .SUB_HE_CO4_LINK5
                        }>
                        here
                      </a>{" "}
                      {
                        PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                          .SUB_HE_CONTENT_45
                      }{" "}
                      <a
                        href={
                          PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                            .SUB_HE_CO4_LINK6
                        }>
                        here
                      </a>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {
                        PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                          .SUB_HEADING_5
                      }
                    </List.Header>
                    <List.Description className="text">
                      {
                        PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                          .SUB_HE_CONTENT_5
                      }{" "}
                      <a
                        href={
                          PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                            .SUB_HE_CO5_LINK1
                        }>
                        here
                      </a>{" "}
                      {
                        PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                          .SUB_HE_CONTENT_51
                      }{" "}
                      <a
                        href={
                          PrivacyPolicy.SERVICE_PROVIDER.BREMARKETING
                            .SUB_HE_CO5_LINK2
                        }>
                        here
                      </a>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="header_text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.HEADING}
                    </List.Header>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Content>
                    <List.Description className="text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.MAIN_1}
                    </List.Description>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Content>
                    <List.Description className="text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.MAIN_2}
                    </List.Description>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Content>
                    <List.Description className="text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.MAIN_3}
                    </List.Description>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HEADING_1}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HE_CONTENT_1}{" "}
                      <a href={PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.link1}>
                        here
                      </a>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HEADING_2}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HE_CONTENT_2}{" "}
                      <a href={PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.link2}>
                        here
                      </a>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HEADING_3}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HE_CONTENT_3}{" "}
                      <a href={PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.link3}>
                        here
                      </a>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HEADING_4}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HE_CONTENT_4}{" "}
                      <a href={PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.link4}>
                        here
                      </a>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HEADING_5}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HE_CONTENT_5}{" "}
                      <a href={PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.link5}>
                        here
                      </a>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HEADING_6}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HE_CONTENT_6}{" "}
                      <a href={PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.link6}>
                        here
                      </a>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HEADING_7}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HE_CONTENT_7}{" "}
                      <a href={PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.link7}>
                        here
                      </a>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HEADING_8}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HE_CONTENT_8}{" "}
                      <a href={PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.link8}>
                        here
                      </a>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HEADING_9}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HE_CONTENT_9}{" "}
                      <a href={PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.link9}>
                        here
                      </a>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HEADING_10}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HE_CONTENT_10}{" "}
                      <a href={PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.link10}>
                        here
                      </a>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HEADING_11}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HE_CONTENT_11}{" "}
                      <a href={PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.link11}>
                        here
                      </a>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HEADING_12}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HE_CONTENT_12}{" "}
                      <a href={PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.link12}>
                        here
                      </a>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HEADING_13}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HE_CONTENT_13}{" "}
                      <a href={PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.link13}>
                        here
                      </a>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HEADING_14}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HE_CONTENT_14}{" "}
                      <a href={PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.link14}>
                        here
                      </a>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HEADING_15}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HE_CONTENT_15}{" "}
                      <a href={PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.link15}>
                        here
                      </a>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HEADING_16}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HE_CONTENT_16}{" "}
                      <a href={PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.link16}>
                        here
                      </a>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="sub_header_text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HEADING_17}
                    </List.Header>
                    <List.Description className="text">
                      {PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.SUB_HE_CONTENT_17}{" "}
                      <a href={PrivacyPolicy.SERVICE_PROVIDER.PAYMENT.link17}>
                        here
                      </a>
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="header_text">
                      {PrivacyPolicy.LINK_OTHER_SITE.HEADING}
                    </List.Header>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Content>
                    <List.Description className="text">
                      {PrivacyPolicy.LINK_OTHER_SITE.MAIN_1}
                    </List.Description>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Content>
                    <List.Description className="text">
                      {PrivacyPolicy.LINK_OTHER_SITE.MAIN_2}
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="header_text">
                      {PrivacyPolicy.CHILDREN_PRIVACY.HEADING}
                    </List.Header>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Content>
                    <List.Description className="text">
                      {PrivacyPolicy.CHILDREN_PRIVACY.MAIN_1}
                    </List.Description>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Content>
                    <List.Description className="text">
                      {PrivacyPolicy.CHILDREN_PRIVACY.MAIN_2}
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="header_text">
                      {PrivacyPolicy.CHANGE_PRIVACY.HEADING}
                    </List.Header>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Content>
                    <List.Description className="text">
                      {PrivacyPolicy.CHANGE_PRIVACY.MAIN_1}
                    </List.Description>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Content>
                    <List.Description className="text">
                      {PrivacyPolicy.CHANGE_PRIVACY.MAIN_2}
                    </List.Description>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Content>
                    <List.Header as="h4" className="header_text">
                      {PrivacyPolicy.CONTACT_US.HEADING}
                    </List.Header>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Content>
                    <List.Description className="text">
                      {PrivacyPolicy.CONTACT_US.MAIN_1}
                    </List.Description>
                  </List.Content>
                </List.Item>
              </List>
            </div>
          </div>
        </Container>
      </div>
    );
  }
}
