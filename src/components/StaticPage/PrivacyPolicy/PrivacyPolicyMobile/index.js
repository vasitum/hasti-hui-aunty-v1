import React, { Component } from "react";
import { Button } from "semantic-ui-react";
import MobileFooter from "../../../MobileComponents/MobileFooter";
import MobileAboutUsFooter from "./MobileAboutUsFooter";
import MainContainer from "../PrivacyPolicyContainer";
import IcFooterArrowIcon from "../../../../assets/svg/IcFooterArrowIcon";

export default class PrivacyPolicyMobile extends Component {
  render() {
    return (
      <div>
        <MainContainer />
        <MobileFooter
          className="mobileFooterForPrivacyPolicy"
          mobileFooterRightColumn={16}
          mobileLeftColumn>
          <MobileAboutUsFooter />
        </MobileFooter>
      </div>
    );
  }
}
