import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./index.scss";
export default class MobileAboutUsFooter extends Component {
  render() {
    let path = window.location.pathname;
    let active = path == "/privacy-policy" ? "active" : "";
    let active1 = path == "/about-us" ? "active" : "";
    return (
      <div className="aboutus-footer">
        <Link to="/about-us" className={`footer-text ${active1}`}>
          About
        </Link>
        <a href="javascript:;" className="footer-text">
          |
        </a>
        {/* <a href="javascript:;" className="footer-text">
          Careers
        </a> */}
        {/* <a href="javascript:;" className="footer-text">
          |
        </a> */}
        <a
          href="http://blog.vasitum.com/"
          target="_blank"
          className="footer-text">
          Blogs
        </a>
        <a href="javascript:;" className="footer-text">
          |
        </a>
        <Link to="/privacy-policy" className={`footer-text ${active}`}>
          Privacy Policy
        </Link>
      </div>
    );
  }
}
