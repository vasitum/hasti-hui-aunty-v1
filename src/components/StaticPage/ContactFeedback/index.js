import React from "react";

import { Container, Grid, Button, Header} from "semantic-ui-react";

import {Form} from "formsy-semantic-ui-react";

import InfoSection from "../../Sections/InfoSection";

import PageBanner from "../../Banners/PageBanner";
import aunty from "../../../assets/banners/aunty.png";
import InputContainer from "../../Forms/InputContainer";

import InputField from '../../Forms/FormFields/InputField';
import TextAreaField from '../../Forms/FormFields/TextAreaField';

import IcRightCheckIcon from "../../../assets/svg/IcRightCheck";

import IcDownArrow from "../../../assets/svg/IcDownArrow";
import IcInfoIcon  from "../../../assets/svg/IcInfo";

import "./index.scss";

const labelStar = (str) => {
  return <div>
           <span>{str}</span>
           <span style={{color: "#0b9ed0"}}>*</span>
        </div>;
  }



class ContactFeedback extends React.Component {
  render() {
    return (
      <div className="ContactFeedback">
        <PageBanner size="medium" image={aunty}>
          <Container>
            <div className="banner_header">
              <Header as="h1">
                We're here to help
              </Header>
            </div>
          </Container>
        </PageBanner>

        <Container>
          <div className="card_panel">
            <Header as="h2" className="header_text">
              Questions you may have
            </Header>

            <div className="">
              <InfoSection

                headerSize="small"
                color="mediumJungleGreen"
                headerIcon={<IcRightCheckIcon pathcolor="#43a047" height="15" width="15" />}
                headerText="How do I choose what I get notifications about?"
                collapsible>
                <span>

                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit
                  delectus voluptate tempora? Nam dolorem vitae, molestias fugiat
                  veniam quo repellendus animi ipsam eveniet, necessitatibus nesciunt
                  illum laboriosam dolorum doloribus sunt.
                </span>
              </InfoSection>

              <InfoSection

                headerSize="small"
                color="mediumJungleGreen"
                headerIcon={<IcRightCheckIcon pathcolor="#43a047" height="15" width="15" />}
                headerText="Where can I find my settings?"
                collapsible>
                <span>

                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit
                  delectus voluptate tempora? Nam dolorem vitae, molestias fugiat
                  veniam quo repellendus animi ipsam eveniet, necessitatibus nesciunt
                  illum laboriosam dolorum doloribus sunt.
                </span>
              </InfoSection>

              <InfoSection

                headerSize="small"
                color="mediumJungleGreen"
                headerIcon={<IcRightCheckIcon pathcolor="#43a047" height="15" width="15" />}
                headerText="How do I change or reset my password?"
                collapsible>
                <span>

                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit
                  delectus voluptate tempora? Nam dolorem vitae, molestias fugiat
                  veniam quo repellendus animi ipsam eveniet, necessitatibus nesciunt
                  illum laboriosam dolorum doloribus sunt.
                </span>
              </InfoSection>

              <InfoSection

                headerSize="small"
                color="mediumJungleGreen"
                headerIcon={<IcRightCheckIcon pathcolor="#43a047" height="15" width="15" />}
                headerText="Why am I seeing an error message saying I can't reply to a conversation on Vasitum?"
                collapsible>
                <span>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit
                  delectus voluptate tempora? Nam dolorem vitae, molestias fugiat
                  veniam quo repellendus animi ipsam eveniet, necessitatibus nesciunt
                  illum laboriosam dolorum doloribus sunt.
                </span>
              </InfoSection>
            </div>

            <div className="Feedback_panelBody">
              <p>Want to let us know about something broken, or a bug on Vasitum?</p>
              <Button >Tell us here</Button>
            </div>


          </div>

          <div className="ContactFeedback_form">
            <div className="ContactFeedback_formHeader">
              <p>
                Please let us know your issue by filling this form.
              </p>
            </div>

            <div className="ContactFeedback_formField">
              <Form>
                <InputContainer label={labelStar(`First name`)}>
                  <InputField placeholder="First name" name="first_name"/>
                </InputContainer>

                <InputContainer label={labelStar(`Last name`)}>
                  <InputField placeholder="Last name" name="last_name" />
                </InputContainer>

                <InputContainer label={labelStar("Email")}>
                  <InputField placeholder="Email" name="email"/>
                </InputContainer>

                <InputContainer label="Phone number">
                  <InputField placeholder="Phone number" name="phone_number"/>
                </InputContainer>

                <InputContainer label="Describe your issue" infoicon={<IcInfoIcon pathcolor="#c8c8c8" />}>
                  <TextAreaField placeholder="Describe here.." name="describe"/>
                </InputContainer>
              </Form>
            </div>

          </div>

          <div className="ContactFeedback__actionBtn">
            <Button primary>
              Send
              <IcDownArrow pathcolor="#99d9f9" />
            </Button>
          </div>


        </Container>
      </div>
    )
  }
}

export default ContactFeedback;

