import React, { Component } from "react";
import { Responsive, Container } from "semantic-ui-react";
import AboutUsPageContainer from "./AboutUsPageContainer";
import AboutUsPageMobile from "./AboutUsPageMobile";
import { Helmet } from "react-helmet";
// import Seo from "../../../containers/Seo";
import ReactGA from "react-ga";
import "./index.scss";

class AboutUsPage extends Component {
  componentDidMount() {
    ReactGA.pageview('/about-us');
  }

  render() {
    return (
      <div className="AboutUsPage">
        {/* <Seo
          schema="AboutPage"
          title="Base"
          description="A starting point for Meteor applications."
          path="/"
          contentType="product"
        /> */}
        <Helmet>
          <meta charSet="utf-8" />
          <title>
            Artificial Intelligence - Recruitment – free job | About Us -
            Vasitum
          </title>
          <meta
            name="title"
            content="Artificial Intelligence - Recruitment – free job | About Us - Vasitum "
          />
          <meta
            name="twitter:title"
            content="Artificial Intelligence - Recruitment – free job | About Us - Vasitum "
          />
          <meta
            property="og:title"
            content="Artificial Intelligence - Recruitment – free job | About Us - Vasitum "
          />
          <meta
            property="og:description"
            content={`Vasitum is an AI-powered recruitment platform, focusing on solving the challenges of recruiters and job seekers; Free job posting platform with resume creator services. Visit us for more details.`}
          />
          <meta
            property="twitter:description"
            content={`Vasitum is an AI-powered recruitment platform, focusing on solving the challenges of recruiters and job seekers; Free job posting platform with resume creator services. Visit us for more details.`}
          />
          <link rel="canonical" href="http://vasitum.com/about-us" />
        </Helmet>
        <Responsive maxWidth={1024}>
          <AboutUsPageMobile />
        </Responsive>
        <Responsive minWidth={1025}>
          <AboutUsPageContainer />
        </Responsive>
      </div>
    );
  }
}

export default AboutUsPage;
