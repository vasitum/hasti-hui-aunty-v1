import React from "react";

import { Header } from "semantic-ui-react";

import "./index.scss";


const AboutUsPageHeader = () => {
  return(
    <div className="AboutUsPageHeader">
      <Header as="h1">
        About Us
      </Header>
      <p>You don’t hire characters. You hire people. Real people. However, sometimes it’s hard to find them and other times it’s hard to find the job that sparks a dream.</p>
    </div>
  ) 
}

export default AboutUsPageHeader;