import React, { Component } from 'react';

import { Image, Header } from "semantic-ui-react";

import Mission from "../../../../assets/img/8698.png";

import { ABOUT_US_PAGE } from "../../../../strings";

import IcMissionIcon from "../../../../assets/svg/IcMissionIcon";
import IcValuesIcon from "../../../../assets/svg/IcValuesIcon";
import IcVisionIcon from "../../../../assets/svg/IcVisionIcon";

import "./index.scss";

const data = [
  {
    logo: <IcMissionIcon pathcolor="#183b7b"/>,
    headTitle: "Mission",
    title: "To be the most efficient career platform"
  },

  {
    logo: <IcValuesIcon pathcolor="#183b7b"/>,
    headTitle: "Vision",
    title: "Provide transparency and increase efficiency within the hiring process via minimal effort"
  },

  {
    logo: <IcVisionIcon pathcolor="#183b7b"/>,
    headTitle: "Values",
    title: "Confident, Inclusive, Transparent, Efficient"
  }
]

class AboutUsPageBodyContainer extends Component {
  render() {
    return (
      <div className="AboutUsPageBodyContainer">
        <div className="BodyContainer_header">
        
          <p>
            Vasitum, an AI-based Platform, is focused on solving the challenges of 
            recruiters and job seekers alike. Founded from almost two decades’ worth of 
            experience in Recruitment, Vasitum was launched by Vikram Wadhawan 
            (Founder & CEO), in January 2019.
          </p>

          <p>
            Vikram and his team’s background in recruiting and software development 
            served as the backbone. Together, they built a single solution as the most 
            efficient career platform, Vasitum, providing transparency and efficiency with 
            minimal effort from both candidates and employers.
          </p>

          <p>
            Meaning ‘infinite’ in Sanskrit, Vasitum fills all the gaps throughout the 
            entire recruitment process. The platform was created to foster everyone’s 
            ability to reach their full potential.
          </p>

          <p>
            At Vasitum, we come to work every day because we know there’s a better 
            way to find people and find career for better tomorrow. We’re innovators, 
            encouragers, and go-getters on a mission to bring efficiency and 
            transparency in Recruitment while enabling technology.
          </p>

        </div>

        <div className="BodyContainer_bodySection">
          <div className="BodyContainer_bodySectionContainer">
            {data.map((dataTitle, idx) => {
              return (
                <div className="aboutCard_container" key={idx}>
                  <div className="card_logo">
                    {dataTitle.logo}
                    {/* <Image verticalAlign={"middle"} src={dataTitle.logo} /> */}
                  </div>
                  <div className="card_title">
                    <Header as="h2">{dataTitle.headTitle}</Header>
                    <p>
                      {dataTitle.title}
                    </p>
                  </div>
                </div>
              )
            })}

          </div>

        </div>
      </div>
    )
  }
}

export default AboutUsPageBodyContainer;
