import React, { Component } from 'react'
import { Container } from 'semantic-ui-react';
import AboutUsPageHeader from "../AboutUsPageHeader";
import AboutUsPageBodyContainer from "../AboutUsPageBodyContainer";

import HomePageFooter from "../../../HomePage/HomePageFooter";
import HomePageSubFooter from "../../../HomePage/HomePageFooter/HomePageSubFooter";
import AboutCompanyContainer from "../../../HomePage/HomePageFooter/AboutCompanyContainer";


class AboutUsPageContainer extends Component {
  render() {
    const { isShowSubFooter } = this.props;
    return (
      <div className="AboutUsPageContainer">
        <Container>
          <AboutUsPageHeader />
          <AboutUsPageBodyContainer />
        </Container>
        <HomePageFooter
          isShowSubFooterTop={<HomePageSubFooter
          // FootComingSoon={<ComingSoon />} 
          />}
          isShowSubFooter={
            !isShowSubFooter ? (
              <AboutCompanyContainer />
            ) : null
          }
        />
      </div>
    )
  }
}

export default AboutUsPageContainer;
