import React, { Component } from "react";

import AboutUsPageContainer from "../AboutUsPageContainer";
import MobileHomePageFooterUpper from "../../../HomePage/MobileHomePageFooterUpper";
import HomePageSubFooter from "../../../HomePage/HomePageFooter/HomePageSubFooter";
import AboutCompanyContainer from "../../../HomePage/HomePageFooter/AboutCompanyContainer";
import MobileFooter from "../../../MobileComponents/MobileFooter";
import MobileAboutUsFooter from "../../PrivacyPolicy/PrivacyPolicyMobile/MobileAboutUsFooter";

class AboutUsPageMobile extends Component {
  render() {
    return (
      <div>
        <div className="AboutUsPageMobile">
          <AboutUsPageContainer />

          {/* <MobileHomePageFooterUpper />
          <HomePageSubFooter />
          <AboutCompanyContainer /> */}
        </div>
        <MobileFooter
          className="mobileFooterForPrivacyPolicy"
          mobileFooterRightColumn={16}
          mobileLeftColumn>
          <MobileAboutUsFooter />
        </MobileFooter>
      </div>
    );
  }
}

export default AboutUsPageMobile;
