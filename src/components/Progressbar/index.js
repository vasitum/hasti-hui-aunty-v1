import React from "react";

import { Progress, Header } from "semantic-ui-react";
import PropTypes from "prop-types";

import "./index.scss";

const Progressbar = props => {
  return (
    <div className="Progressbar">
      <div className="Progressbar_Label">
        <p className="">
          {props.counter ? props.counter : 0}% profile completed
        </p>
      </div>
      <div className="Progressbar_Body">
        <Progress percent={props.counter ? props.counter : 0} size="small" />
      </div>
    </div>
  );
};

Progressbar.propTypes = {
  placeholder: PropTypes.string
};

export default Progressbar;
