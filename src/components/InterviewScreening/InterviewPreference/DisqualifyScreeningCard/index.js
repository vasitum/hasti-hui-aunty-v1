import React from "react";

import FullwidthDefault from "../../../Buttons/FullwidthDefault";

import InputFieldLabel from "../../../Forms/FormFields/InputFieldLabel";

import InfoIconLabel from "../../../utils/InfoIconLabel";

import IcRightCheck from "../../../../assets/svg/IcRightCheck";

import IcCloseIcon from "../../../../assets/svg/IcCloseIcon";

import { INTERVIEW_PREFERENCE } from "../../../../strings";

const DisqualifyScreeningCard = () => {
  return (
    <div className="ScreeningCard">
      <div className="header_container">
        <div className="ScreeningCard_header">
          <p>
            {INTERVIEW_PREFERENCE.APPLICATION_WORKFLOW.DISQUALIFIED_CARD.HEADER_TITLE} <span>: {INTERVIEW_PREFERENCE.APPLICATION_WORKFLOW.DISQUALIFIED_CARD.SPAN_TITLE}</span>
          </p>
        </div>
        <div className="ScreeningCard_body">
          <div className="screeningCheck">
            <FullwidthDefault btnText="Screening" />
            <span>
              <IcCloseIcon height="11" width="11" pathcolor="#d92525" />
            </span>
          </div>
        </div>
      </div>
      <div className="screeningCard_footer">
        <InputFieldLabel
          label={INTERVIEW_PREFERENCE.APPLICATION_WORKFLOW.DISQUALIFIED_CARD.INPUT_LABEL}
          // infoicon={<InfoIconLabel text="Mention in a few words" />}
        />

        <FullwidthDefault btnText="Rejected" />

        <p>
        {INTERVIEW_PREFERENCE.APPLICATION_WORKFLOW.DISQUALIFIED_CARD.FOOTER_SUB_TITLE}
        </p>
      </div>
    </div>
  );
};

export default DisqualifyScreeningCard;
