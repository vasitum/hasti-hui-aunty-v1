import React from "react";

import FullwidthDefault from "../../../Buttons/FullwidthDefault";

import InputFieldLabel from "../../../Forms/FormFields/InputFieldLabel";

import InfoIconLabel from "../../../utils/InfoIconLabel";

import IcRightCheck from "../../../../assets/svg/IcRightCheck";

import { INTERVIEW_PREFERENCE } from "../../../../strings";

const ScreeningCard = () => {
  return (
    <div className="ScreeningCard">
      <div className="header_container">
        <div className="ScreeningCard_header">
          <p>{INTERVIEW_PREFERENCE.APPLICATION_WORKFLOW.QUALIFIED_CARD.HEADER_TITLE} <span>{INTERVIEW_PREFERENCE.APPLICATION_WORKFLOW.QUALIFIED_CARD.SPAN_TITLE}</span></p>
        </div>
        <div className="ScreeningCard_body">
          

          <div className="screeningCheck">
            <FullwidthDefault btnText="Screening" />
            <span><IcRightCheck pathcolor="#43a047" /></span>
          </div>
        </div>
      </div>
      <div className="screeningCard_footer">
        <InputFieldLabel
          label={INTERVIEW_PREFERENCE.APPLICATION_WORKFLOW.QUALIFIED_CARD.INPUT_LABEL}
          // infoicon={<InfoIconLabel text="Mention in a few words" />}
        />

        <FullwidthDefault btnText="Shortlist" />

        <p>
          {INTERVIEW_PREFERENCE.APPLICATION_WORKFLOW.QUALIFIED_CARD.FOOTER_SUB_TITLE}
        </p>
      </div>
    </div>
  )
}

export default ScreeningCard;