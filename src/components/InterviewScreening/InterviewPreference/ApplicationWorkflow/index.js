import React from "react";

import { Header, Grid } from "semantic-ui-react";

import ScreeningCard from "../ScreeningCard";
import DisqualifyScreeningCard from "../DisqualifyScreeningCard";

import { INTERVIEW_PREFERENCE } from "../../../../strings";

import ApplicationWorkflowList from "../ApplicationWorkflowList";

import AccordionCardContainer from "../../../Cards/AccordionCardContainer";

import DownArrowCollaps from "../../../utils/DownArrowCollaps";

import "./index.scss";

const ApplicationWorkflow = () => {
  return (
    <div className="ApplicationWorkflow">
      <AccordionCardContainer
        cardHeader={<div className="Workflow_headerTitle">
          <Header as="h2">{INTERVIEW_PREFERENCE.APPLICATION_WORKFLOW.TITLE}</Header>
          <p className="DownArrowCollaps_Icon">
            <DownArrowCollaps />
          </p>
        </div>}
      >

        <div className="Workflow_body">
          <p className="Workflow_bodyTitle">{INTERVIEW_PREFERENCE.APPLICATION_WORKFLOW.SUBTITLE}</p>
          <ApplicationWorkflowList type={"ShortList"} />
          {/* <Grid>
          <Grid.Row>
            <Grid.Column width={8}>
              <ScreeningCard />
            </Grid.Column>
            <Grid.Column width={8}>
              <DisqualifyScreeningCard />
            </Grid.Column>
          </Grid.Row>
        </Grid> */}
        </div>
      </AccordionCardContainer>





    </div>
  )
}

export default ApplicationWorkflow;