import React from "react";

import { Image, Button, Modal, Responsive } from "semantic-ui-react";

import IcUser from "../../../../assets/svg/IcUser";

import { Link } from "react-router-dom";

import ApplicationBucketImg from "../../../../assets/img/2.jpg";

import ImgInterviewBucketD from "../../../../assets/img/applicationWorkFlow/img_interview_bucket.jpg";
import ImgInterviewBucketM from "../../../../assets/img/applicationWorkFlow/img_interview_bucket(m).png";
import ImgInterviewBucketThumbD from "../../../../assets/img/applicationWorkFlow/img_interview_bucket_thumb.png";
import ImgInterviewBucketThumbM from "../../../../assets/img/applicationWorkFlow/img_interview_bucket_thumb(m).png";

import ImgRejectedBucketD from "../../../../assets/img/applicationWorkFlow/img_rejected_bucket.jpg";
import ImgRejectedBucketM from "../../../../assets/img/applicationWorkFlow/img_rejected_bucket(m).png";
import ImgRejectedBucketThumbD from "../../../../assets/img/applicationWorkFlow/img_rejected_bucket_thumb.png";
import ImgRejectedBucketThumbM from "../../../../assets/img/applicationWorkFlow/img_rejected_bucket_thumb(m).png";

import ImgShortlistBucketD from "../../../../assets/img/applicationWorkFlow/img_shortlist_bucket.jpg";
import ImgShortlistBucketM from "../../../../assets/img/applicationWorkFlow/img_shortlist_bucket(m).png";
import ImgShortlistBucketThumbD from "../../../../assets/img/applicationWorkFlow/img_shortlist_bucket_thumb.png";
import ImgShortlistBucketThumbM from "../../../../assets/img/applicationWorkFlow/img_shortlist_bucket_thumb(m).png";

import "./index.scss";

function getImage(type) {
  switch (type) {
    case "ShortList":
      return (
        <div className="ApplicationWorkflowList">
          <div className="ApplicationWorkflowList_left userImgThumb">
            <Modal
              className="ApplicationBucket_modal"
              trigger={
                <Button as="a">
                  <Responsive maxWidth={1024}>
                    <Image src={ImgShortlistBucketThumbM} />
                  </Responsive>
                  <Responsive minWidth={1025}>
                    <Image src={ImgShortlistBucketThumbD} />
                  </Responsive>
                </Button>
              }
              closeIcon
              closeOnDimmerClick={false}>
              <div className="">
                <Responsive maxWidth={1024}>
                  <Image src={ImgShortlistBucketM} />
                </Responsive>
                <Responsive minWidth={1025}>
                  <Image src={ImgShortlistBucketD} />
                </Responsive>
              </div>
            </Modal>
          </div>
          <div className="ApplicationWorkflowList_right">
            <p className="candidate-whoTitle">
              Candidates who - <span>Qualified screening </span>
            </p>
            <div className="application-statusTitle">
              <p className="statusTitle"> Applications will feature under </p>
              <p className="statusSubTitle">
                {" "}
                "
                <Modal
                  className="ApplicationBucket_modal"
                  trigger={<Button as="a">Shortlist</Button>}
                  closeIcon
                  closeOnDimmerClick={false}>
                  <div className="">
                    <Responsive maxWidth={1024}>
                      <Image src={ImgShortlistBucketM} />
                    </Responsive>
                    <Responsive minWidth={1025}>
                      <Image src={ImgShortlistBucketD} />
                    </Responsive>
                  </div>
                </Modal>
                {/* <Button as={Link} to={`/job/recruiter/candidates?menu%5Bstatus%5D=ShortList`}>Shortlist</Button> */}
                " bucket
              </p>
            </div>
          </div>
        </div>
      );

      break;

    case "Interview":
      return (
        <div className="ApplicationWorkflowList">
          <div className="ApplicationWorkflowList_left userImgThumb">
            <Modal
              className="ApplicationBucket_modal"
              trigger={
                <Button as="a">
                  <Responsive maxWidth={1024}>
                    <Image src={ImgInterviewBucketThumbM} />
                  </Responsive>
                  <Responsive minWidth={1025}>
                    <Image src={ImgInterviewBucketThumbD} />
                  </Responsive>
                </Button>
              }
              closeIcon
              closeOnDimmerClick={false}>
              <div className="">
                <Responsive maxWidth={1024}>
                  <Image src={ImgInterviewBucketM} />
                </Responsive>
                <Responsive minWidth={1025}>
                  <Image src={ImgInterviewBucketD} />
                </Responsive>
              </div>
            </Modal>
          </div>
          <div className="ApplicationWorkflowList_right">
            <p className="candidate-whoTitle">
              Candidates who - <span>Get scheduled for interview </span>
            </p>
            <div className="application-statusTitle">
              <p className="statusTitle"> Applications will feature under </p>
              <p className="statusSubTitle">
                {" "}
                "
                <Modal
                  className="ApplicationBucket_modal"
                  trigger={<Button as="a">Interview</Button>}
                  closeIcon
                  closeOnDimmerClick={false}>
                  <div className="">
                    <Responsive maxWidth={1024}>
                      <Image src={ImgInterviewBucketM} />
                    </Responsive>
                    <Responsive minWidth={1025}>
                      <Image src={ImgInterviewBucketD} />
                    </Responsive>
                  </div>
                </Modal>
                " bucket
              </p>
            </div>
          </div>
        </div>
      );
      break;

    case "Rejected":
      return (
        <div className="ApplicationWorkflowList">
          <div className="ApplicationWorkflowList_left userImgThumb">
            <Modal
              className="ApplicationBucket_modal"
              trigger={
                <Button as="a">
                  <Responsive maxWidth={1024}>
                    <Image src={ImgRejectedBucketThumbM} />
                  </Responsive>
                  <Responsive minWidth={1025}>
                    <Image src={ImgRejectedBucketThumbD} />
                  </Responsive>
                </Button>
              }
              closeIcon
              closeOnDimmerClick={false}>
              <div className="">
                <Responsive maxWidth={1024}>
                  <Image src={ImgRejectedBucketM} />
                </Responsive>
                <Responsive minWidth={1025}>
                  <Image src={ImgRejectedBucketD} />
                </Responsive>
              </div>
            </Modal>
          </div>
          <div className="ApplicationWorkflowList_right">
            <p className="candidate-whoTitle">
              Candidates who - <span>Failed screening </span>
            </p>
            <div className="application-statusTitle">
              <p className="statusTitle"> Applications will feature under </p>
              <p className="statusSubTitle">
                {" "}
                "
                <Modal
                  className="ApplicationBucket_modal"
                  trigger={<Button as="a">Rejected</Button>}
                  closeIcon
                  closeOnDimmerClick={false}>
                  <div className="">
                    <Responsive maxWidth={1024}>
                      <Image src={ImgRejectedBucketM} />
                    </Responsive>
                    <Responsive minWidth={1025}>
                      <Image src={ImgRejectedBucketD} />
                    </Responsive>
                  </div>
                </Modal>
                {/* <Button as={Link} to={`/job/recruiter/candidates?menu%5Bstatus%5D=Rejected`}>Rejected</Button> */}
                " bucket
              </p>
            </div>
          </div>
        </div>
      );
      break;

    default:
      break;
  }
}

const ApplicationWorkflowList = ({ type }) => {
  // img  = type === "Shortli"

  return (
    <React.Fragment>
      {getImage("ShortList")}
      {getImage("Interview")}
      {getImage("Rejected")}
    </React.Fragment>
  );
};

export default ApplicationWorkflowList;
