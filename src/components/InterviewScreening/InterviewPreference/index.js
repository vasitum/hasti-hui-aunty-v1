import React from "react";

import { Header, Container, Responsive } from "semantic-ui-react";

import InterviewPreferenceContainer from "./InterviewPreferenceContainer";

import InterviewPreferenceMobile from "./InterviewPreferenceMobile";
import { withRouter } from "react-router-dom";
import ReactGA from "react-ga";
import "./index.scss";

class InterviewPreference extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    const { match } = this.props;
    console.log("check url",  match.url);
    ReactGA.pageview(`${match.url}`);
  }
  render() {
    const {
      data,
      onPrev,
      onPreviewClick,
      onDraftJobClick,
      isEdit,
      googleAuthCode,
      defaultState
    } = this.props;

    return (
      <div className="InterviewPreference">
        <Responsive maxWidth={1024}>
          <InterviewPreferenceMobile
            isEdit={isEdit}
            data={data}
            onPreviewClick={onPreviewClick}
            onDraftJobClick={onDraftJobClick}
            onPrev={onPrev}
            googleAuthCode={googleAuthCode}
            defaultState={defaultState}
          />
        </Responsive>
        <Responsive minWidth={1025}>
          <InterviewPreferenceContainer
            isEdit={isEdit}
            data={data}
            onPreviewClick={onPreviewClick}
            onDraftJobClick={onDraftJobClick}
            onPrev={onPrev}
            googleAuthCode={googleAuthCode}
            defaultState={defaultState}
          />
          {/* <Container>
            
          </Container> */}
        </Responsive>
      </div>
    );
  }
}

export default withRouter(InterviewPreference);
