import React from "react";
import { Button } from "semantic-ui-react";
import { Link } from "react-router-dom";
import FlatDefaultBtn from "../../../Buttons/FlatDefaultBtn";
import ActionBtn from "../../../Buttons/ActionBtn";

import IcDownArrow from "../../../../assets/svg/IcDownArrow";
import SaveLogo from "../../../../assets/svg/IcSave";
import IcProfileViewIcon from "../../../../assets/svg/IcProfileView";
import IcFooterArrowIcon from "../../../../assets/svg/IcFooterArrowIcon";
import "./index.scss";

const InterviewPreferenceFooter = props => {
  const {
    containerRight,
    onPublish,
    onPreviewClick,
    onDraftClick,
    onPrev,
    isEdit
  } = props;

  if (isEdit) {
    return (
      <div className="InterviewPreferenceFooter">
        <div className="footer_containerLeft">
          <p onClick={onPrev}>
            {" "}
            <Button>
              <IcFooterArrowIcon pathcolor="#0b9ed0" /> Go Back to “Screening”
            </Button>{" "}
          </p>
        </div>
        <div className="footer_containerRight">
          <div className="saveDraftBtn">
            <div className="previewBtn">
              <FlatDefaultBtn
                btnicon={<IcProfileViewIcon height="12" pathcolor="#c8c8c8" />}
                btntext="Preview Job"
                type="button"
                // disabled={!this.state.canSubmit}
                onClick={onPreviewClick}
                // className="profileView_Icon"
                style={{
                  paddingLeft: "2px",
                  paddingRight: "15px"
                }}
              />
            </div>

            {/* <FlatDefaultBtn
            btnicon={<SaveLogo height="12" pathcolor="#c8c8c8" />}
            onClick={onDraftClick}
            // disabled={!this.state.canSubmit}
            btntext="Save as draft"
            type="button"
          /> */}
          </div>
          <div className="footerActionBtn">
            <ActionBtn
              // style={{
              //   opacity: !this.state.canSubmit ? "0.6" : "1"
              // }}
              onClick={onPublish}
              type="submit"
              actioaBtnText="Save and Exit"
              // btnIcon={<IcDownArrow pathcolor="#99d9f9" />}
            />
            {/* <div className="actionBtn_title">
            <p>You will go to your posted</p>
            <p className="subTitle">job detail page</p>
          </div> */}
          </div>
          {/* <div className="cancelBtn">
          <Link to="/job/recruiter/dashboard">
            <FlatDefaultBtn
              btntext="Cancel"
              type="button"
              className="cancelBtn"
            />
          </Link>
        </div> */}
        </div>
      </div>
    );
  }

  return (
    <div className="InterviewPreferenceFooter">
      <div className="footer_containerLeft">
        <p onClick={onPrev}>
          {" "}
          <Button>
            <IcFooterArrowIcon pathcolor="#0b9ed0" /> Go Back to “Screening”
          </Button>{" "}
        </p>
      </div>
      <div className="footer_containerRight">
        <div className="saveDraftBtn">
          <div className="previewBtn">
            <FlatDefaultBtn
              btnicon={<IcProfileViewIcon height="12" pathcolor="#c8c8c8" />}
              btntext="Preview Job"
              type="button"
              // disabled={!this.state.canSubmit}
              onClick={onPreviewClick}
              // className="profileView_Icon"
              style={{
                paddingLeft: "2px",
                paddingRight: "15px"
              }}
            />
          </div>
          <FlatDefaultBtn
            btnicon={<SaveLogo height="12" pathcolor="#c8c8c8" />}
            onClick={onDraftClick}
            // disabled={!this.state.canSubmit}
            btntext="Save as draft"
            type="button"
          />
        </div>
        <div className="footerActionBtn">
          <ActionBtn
            // style={{
            //   opacity: !this.state.canSubmit ? "0.6" : "1"
            // }}
            onClick={onPublish}
            type="submit"
            actioaBtnText="Publish Job"
            btnIcon={<IcDownArrow pathcolor="#99d9f9" />}
          />
          <div className="actionBtn_title">
            <p>You will go to your posted</p>
            <p className="subTitle">job detail page</p>
          </div>
        </div>
        {/* <div className="cancelBtn">
          <Link to="/job/recruiter/dashboard">
            <FlatDefaultBtn
              btntext="Cancel"
              type="button"
              className="cancelBtn"
            />
          </Link>
        </div> */}
      </div>
    </div>
  );
};

export default InterviewPreferenceFooter;
