import React from "react";

import { Image, Header, Grid, Button } from "semantic-ui-react";

import RadioButton from "../../../Forms/FormFields/RadioButton";

import FlatDefaultBtn from "../../../Buttons/FlatDefaultBtn";

import InputFieldLabel from "../../../Forms/FormFields/InputFieldLabel";

import InfoIconLabel from "../../../utils/InfoIconLabel";

import SelectOptionField from "../../../Forms/FormFields/SelectOptionField";

import GoogleCalendarIcon from "../../../../assets/img/google_calendar.png";
import OutlookCalendarIcon from "../../../../assets/img/outlook_calendar.png";
import IcRightCheck from "../../../../assets/svg/IcRightCheck";

import InterviewType from "../../../Messaging/InterView/ScheduleInterview/InterviewType";

import { INTERVIEW_PREFERENCE } from "../../../../strings";

import getUserById from "../../../../api/user/getUserById";
import setAuthToken from "../../../../api/user/setAuthToken";
import { getAuthUrl, getAuthToken } from "../../../../api/auth/google";

import "./index.scss";
import { USER } from "../../../../constants/api";

import { withRouter } from "react-router-dom";

const ManageInterviewsMessage = () => {
  return (
    <div className="ManageInterviewsMessage">
      <p>
        {INTERVIEW_PREFERENCE.INTERVIEW_SCHEDUL_CARD.RADIO_BTN_2.CARD_TITLE}
      </p>
    </div>
  );
};

class SchedulingInterviewsCard extends React.Component {
  constructor(props) {
    super(props);
  }

  state = {
    hasToken: false,
    authUri: false,
    authTokenGoogle: ""
  };

  async componentDidMount() {
    const { data } = this.props;
    const { interViewType } = data;

    if (interViewType.provider === "GOOGLE") {
      this.setState({
        hasToken: true,
        authUri: false
      });

      return;
    }

    if (interViewType.provider === "ERROR") {
      return;
    }

    try {
      const res = await getUserById(window.localStorage.getItem(USER.UID));
      if (res.status === 200) {
        const data = await res.json();
        const hasToken = data.googleToken ? true : false;
        let authUri = false;

        if (!hasToken) {
          const resAuthUri = await getAuthUrl(window.location.href);
          if (resAuthUri.status === 200) {
            const { authUrl } = await resAuthUri.json();
            authUri = authUrl;
          }
        }

        this.setState({
          hasToken: hasToken,
          authUri: authUri,
          authTokenGoogle: hasToken ? data.googleToken : ""
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  componentDidUpdate() {
    const { data } = this.props;
    const { interViewType } = data;

    if (!interViewType) {
      return;
    }

    if (this.state.hasToken) {
      return;
    }

    if (interViewType.provider === "GOOGLE") {
      this.setState({
        hasToken: true,
        authUri: false
      });

      return;
    }

    if (interViewType.provider === "ERROR") {
      return;
    }
  }

  onConnectClick = e => {
    e.preventDefault();
    const { authUri, hasToken } = this.state;
    if (!hasToken) {
      window.location.href = authUri;
    }
  };

  render() {
    const { data, onInputChange, onInterViewTypeChange } = this.props;

    return (
      <div className="SchedulingInterviewsCard">
        <div className="interviewsCard_Header">
          <div className="schedule_type">
            <RadioButton
              checked={
                data.interViewType
                  ? data.interViewType.scheduleType === "AUTO"
                  : false
              }
              onChange={e =>
                onInterViewTypeChange(e, {
                  value: "AUTO",
                  name: "scheduleType"
                })
              }
              label={
                INTERVIEW_PREFERENCE.INTERVIEW_SCHEDUL_CARD.RADIO_BTN_1.TITLE
              }
            />
            <div className="manual">
              <RadioButton
                checked={
                  data.interViewType
                    ? data.interViewType.scheduleType === "MANUAL"
                    : false
                }
                onChange={e =>
                  onInterViewTypeChange(e, {
                    value: "MANUAL",
                    name: "scheduleType"
                  })
                }
                label={
                  INTERVIEW_PREFERENCE.INTERVIEW_SCHEDUL_CARD.RADIO_BTN_2.TITLE
                }
              />
            </div>
          </div>
        </div>
        <div className="interviewsCard_body">
          {data.interViewType.scheduleType === "AUTO" ? (
            !this.state.hasToken ? (
              <div className="interview_connectBox">
                <div className="interviewsCard_bodyTitle">
                  {/* <Header as="h3">Sync your calendar with Vasitum</Header> */}
                  <p>
                    {
                      INTERVIEW_PREFERENCE.INTERVIEW_SCHEDUL_CARD.RADIO_BTN_1
                        .CARD_TITLE
                    }
                  </p>
                </div>

                <div className="connect_interviewBox">
                  <div className="googleCalendar calendar_box">
                    <Image src={GoogleCalendarIcon} />
                    <FlatDefaultBtn
                      onClick={this.onConnectClick}
                      btntext="Connect"
                    />
                  </div>
                  {/* <div className="outlookCalendar calendar_box">
                    <Image src={OutlookCalendarIcon} />
                    <FlatDefaultBtn btntext="Connect" />
                  </div> */}
                </div>
              </div>
            ) : (
              <div className="interviewScheduling_calendar">
                <div className="scheduling_calendarHeader">
                  <p className="calendarTitle">
                    <span>
                      <IcRightCheck
                        pathcolor="#43a047"
                        width="15"
                        height="12"
                      />
                    </span>
                    {
                      INTERVIEW_PREFERENCE.INTERVIEW_SCHEDUL_CARD.RADIO_BTN_1
                        .SCHEDUL_CALENDAR.TITLE
                    }
                    {/* Your Google calendar synced. */}
                  </p>
                  <p className="subTitle">
                    {
                      INTERVIEW_PREFERENCE.INTERVIEW_SCHEDUL_CARD.RADIO_BTN_1
                        .SCHEDUL_CALENDAR.SUBTITLE
                    }
                  </p>
                </div>
                <div className="scheduling_calendarBody">
                  <InterviewType
                    defaultInterview
                    data={{
                      interviewContactInfo: data.interviewContactInfo,
                      interviewContactNumber: data.interviewContactNumber,
                      interviewMode: data.interviewMode,
                      interviewSubMode: data.interviewSubMode,
                      notifyEmail: data.notifyEmail
                    }}
                    onInputChange={onInputChange}
                  />
                  <InputFieldLabel
                    label="Select interview avg. time duration"
                    infoicon={<InfoIconLabel text="Mention in a few words" />}
                  />
                  <Grid>
                    <Grid.Row>
                      <Grid.Column width={6}>
                        <SelectOptionField
                          placeholder="Select"
                          optionsItem={[
                            { value: "15", text: "15 mins" },
                            { value: "30", text: "30 mins" },
                            { value: "60", text: "1 Hr" }
                          ]}
                          name={"avrgTime"}
                          value={data.avrgTime}
                          onChange={onInputChange}
                        />
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                </div>
                <div className="scheduling_calendarFooter">
                  {/* <Button className="calenderDisconnectBtn">
                    {
                      INTERVIEW_PREFERENCE.INTERVIEW_SCHEDUL_CARD.RADIO_BTN_1
                        .SCHEDUL_CALENDAR.BTN_TEXT
                    }
                  </Button> */}
                </div>
              </div>
            )
          ) : (
            <React.Fragment>
              <ManageInterviewsMessage />
              <div className="scheduling_calendarBody">
                <InterviewType
                  defaultInterview
                  data={{
                    interviewContactInfo: data.interviewContactInfo,
                    interviewContactNumber: data.interviewContactNumber,
                    interviewMode: data.interviewMode,
                    interviewSubMode: data.interviewSubMode,
                    notifyEmail: data.notifyEmail
                  }}
                  onInputChange={onInputChange}
                />
                <div className="SelectInterview_notyTitme">
                  <InputFieldLabel
                    label={
                      INTERVIEW_PREFERENCE.INTERVIEW_SCHEDUL_CARD
                        .DROPDOWN_HEADER.TITLE
                    }
                    // infoicon={
                    //   <InfoIconLabel
                    //     text={
                    //       INTERVIEW_PREFERENCE.INTERVIEW_SCHEDUL_CARD
                    //         .DROPDOWN_HEADER.CARD_TITLE
                    //     }
                    //   />
                    // }
                  />
                </div>
                <Grid>
                  <Grid.Row>
                    <Grid.Column width={6}>
                      <SelectOptionField
                        placeholder="Select"
                        optionsItem={[
                          { value: "15", text: "15 mins" },
                          { value: "30", text: "30 mins" },
                          { value: "60", text: "1 hour" }
                        ]}
                        name={"avrgTime"}
                        value={data.avrgTime}
                        onChange={onInputChange}
                      />
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </div>
            </React.Fragment>
          )}

          {/* interviewScheduling_calendar */}
        </div>
      </div>
    );
  }
}

export default withRouter(SchedulingInterviewsCard);
