import React from "react";

import { Header, Container } from "semantic-ui-react";

import { Form } from "formsy-semantic-ui-react";

import RadioButton from "../../../Forms/FormFields/RadioButton";

import SchedulingInterviewsCard from "../SchedulingInterviewsCard";

import ApplicationWorkflow from "..//ApplicationWorkflow";

import InterviewPreferenceFooter from "../InterviewPreferenceFooter";
import push2Publish from "../../../../api/jobs/pushToPublish";
import pushToSave from "../../../../api/jobs/pushToSave";
import { getAuthToken } from "../../../../api/auth/google";
import setAuthToken from "../../../../api/user/setAuthToken";
import { USER } from "../../../../constants/api";
import JobConstants from "../../../../constants/storage/jobs";

import ReactGA from "react-ga";
import { InterviewScreeningString } from "../../../EventStrings/InterviewScreening";

import { withRouter } from "react-router-dom";
import { INTERVIEW_PREFERENCE } from "../../../../strings";

class InterviewPreferenceContainer extends React.Component {
  state = {
    avrgTime: 0,
    interViewType: {
      provider: "",
      scheduleType: "MANUAL"
    },
    interviewManage: true,
    calendar: false,
    interviewContactInfo: "",
    interviewContactNumber: "",
    interviewMode: "",
    interviewSubMode: "",
    avrgTime: "",
    notifyEmail: [],
    authTokenGoogle: ""
  };

  constructor(props) {
    super(props);
    // autobind
    this.onJobPublish = this.onJobPublish.bind(this);
    this.onDraftJobClick = this.onDraftJobClick.bind(this);
    this.sendTokenToServer = this.sendTokenToServer.bind(this);
  }

  onInputChange = (e, { value, name }) => {
    this.setState({
      [name]: value
    });
  };

  onInterviewManageChange = (e, { value }) => {
    this.setState(state => {
      return {
        ...state,
        interviewManage: !state.interviewManage
      };
    });
  };

  onInterViewTypeChange = (e, { value, name }) => {
    this.setState({
      interViewType: {
        ...this.state.interViewType,
        [name]: value
      }
    });
  };

  async onJobPublish(e) {
    const { data, isEdit } = this.props;

    const sendData = {
      _id: data.hasScreening ? data.screeningId : null,
      avrgTime: this.state.avrgTime,
      interViewType: this.state.interViewType,
      interviewContactInfo: this.state.interviewContactInfo,
      interviewContactNumber: this.state.interviewContactNumber,
      interviewManage: this.state.interviewManage,
      interviewMode: this.state.interviewMode,
      interviewSubMode: this.state.interviewSubMode,
      notifyEmail: this.state.notifyEmail,
      education: data.checked.education ? data.education : null,
      exp: data.checked.experience ? data.exp : null,
      jobId: data.jobId,
      joiningProb: data.checked.notice ? data.joiningProb : null,
      locMandatory: data.checked.location ? data.locMandatory : false,
      skills: data.checked.skills ? data.skills : null,
      userId: window.localStorage.getItem(USER.UID),
      workDays: data.checked.shift ? data.workDays : null
    };

    try {
      let res;
      if (isEdit) {
        res = await pushToSave(sendData);
      } else {
        res = await push2Publish(sendData);
      }

      if (res.status === 200) {
        const data = await res.json();
        // console.log(data);
        window.localStorage.removeItem(JobConstants.JOB_PREVIEW_STATE);
        this.props.history.push(`/job/view/${data._id}`);
      }
    } catch (error) {
      console.error(error);
    }
    
    const UserId = window.localStorage.getItem(USER.UID);

    ReactGA.event({
      category: InterviewScreeningString.InterviewPreference.category,
      action: InterviewScreeningString.InterviewPreference.action,
      value: UserId,
    });

  }

  async sendTokenToServer(data) {
    if (!data.access_token) {
      return;
    }

    try {
      const res = await setAuthToken(data);
      if (res.status === 200) {
        // console.log("Auth Token Saved!");
        return;
      }

      // console.log("Unable to save auth token");
    } catch (error) {
      console.log("Unable to save auth token", error);
    }
  }

  async componentDidMount() {
    const { googleAuthCode, defaultState } = this.props;

    if (defaultState) {
      this.setState({
        interViewType: {
          ...defaultState.interViewType,
          scheduleType: defaultState.interViewType.scheduleType
        },
        interviewMode: defaultState.interviewMode,
        interviewSubMode: defaultState.interviewSubMode,
        interviewManage: defaultState.interviewManage,
        interviewContactNumber: defaultState.interviewContactNumber,
        interviewContactInfo: defaultState.interviewContactInfo
      });
    }

    if (!googleAuthCode) {
      return;
    }

    if (googleAuthCode === "ERROR") {
      // handle error
      // alert("Handle Cancel Bro");
      return;
    }

    try {
      const res = await getAuthToken(
        googleAuthCode,
        window.location.href.split("?")[0]
      );
      if (res.status === 200) {
        const data = await res.json();
        // console.log(data);
        this.sendTokenToServer(data);
        this.setState({
          interviewManage: true,
          interViewType: {
            ...this.state.interViewType,
            scheduleType: "AUTO",
            provider: "GOOGLE"
          }
        });

        return;
      }

      // console.log(res);
    } catch (error) {
      console.error(error);
    }
  }

  async onDraftJobClick(e) {
    const { onDraftJobClick } = this.props;
    onDraftJobClick(this.state);
  }

  render() {
    const {
      headerTitle,
      isShowPrefenceFooter,
      onPrev,
      onPreviewClick,
      isEdit
    } = this.props;
    const { interviewManage } = this.state;
    return (
      <div className="InterviewPreferenceContainer">
        <Container>
          <div className="preference_container">
            {!headerTitle ? (
              <div className="preference_headerTitle">
                <Header as="h2">
                  {INTERVIEW_PREFERENCE.HEADER.HEADER_TITLE}
                </Header>
                <p>{INTERVIEW_PREFERENCE.HEADER.SUB_TITLE}</p>
              </div>
            ) : null}

            <Form>
              <div className="select_interviewType">
                <div className="scheduling_interviews">
                  <Form.Field>
                    <RadioButton
                      checked={this.state.interviewManage}
                      label={INTERVIEW_PREFERENCE.HEADER.CHECKBOX_1}
                      onChange={this.onInterviewManageChange}
                    />
                  </Form.Field>
                </div>
                <div className="manage_interviews">
                  <Form.Field>
                    <RadioButton
                      checked={!this.state.interviewManage}
                      label={INTERVIEW_PREFERENCE.HEADER.CHECKBOX_2}
                      onChange={this.onInterviewManageChange}
                    />
                  </Form.Field>
                </div>
              </div>
              {interviewManage ? (
                <SchedulingInterviewsCard
                  data={{
                    authTokenGoogle: this.state.authTokenGoogle,
                    calendar: this.state.calendar,
                    interViewType: this.state.interViewType,
                    interviewContactInfo: this.state.interviewContactInfo,
                    interviewContactNumber: this.state.interviewContactNumber,
                    interviewMode: this.state.interviewMode,
                    interviewSubMode: this.state.interviewSubMode,
                    avrgTime: this.state.avrgTime,
                    notifyEmail: this.state.notifyEmail
                  }}
                  onInputChange={this.onInputChange}
                  onInterViewTypeChange={this.onInterViewTypeChange}
                />
              ) : null}
              <div className="interview_divider" />

              <ApplicationWorkflow />

              {!isShowPrefenceFooter ? (
                <InterviewPreferenceFooter
                  isEdit={isEdit}
                  onPrev={onPrev}
                  onDraftClick={this.onDraftJobClick}
                  onPublish={this.onJobPublish}
                  onPreviewClick={onPreviewClick}
                />
              ) : null}
            </Form>
          </div>
        </Container>
      </div>
    );
  }
}

export default withRouter(InterviewPreferenceContainer);
