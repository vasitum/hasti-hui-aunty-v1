import React from "react";

import MobileHeader from "../../../MobileComponents/MobileHeader";

import InterviewPreferenceContainer from "../InterviewPreferenceContainer";
import BackArrowBtnLeft from "../../../Buttons/BackArrowBtnLeft";

import MoreOption from "./MoreOption";

import "./index.scss";

class InterviewPreferenceMobile extends React.Component {
  render() {
    const {
      data,
      onPrev,
      onPreviewClick,
      isEdit,
      onDraftJobClick,
      googleAuthCode,
      defaultState
    } = this.props;

    return (
      <div className="InterviewPreferenceMobile">
        <div className="PreferenceMobile_header">
          <MobileHeader
            mobileHeaderLeftColumn="14"
            mobileHeaderRightColumn="2"
            headerLeftIcon={<BackArrowBtnLeft onClick={onPrev} />}
            headerTitle="Choose interview preferences"
            className="isMobileHeader_fixe">
            <MoreOption onPreviewClick={onPreviewClick} />
          </MobileHeader>
        </div>
        <div className="PreferenceMobile_body">
          <InterviewPreferenceContainer
            data={data}
            onPrev={onPrev}
            isEdit={isEdit}
            onPreviewClick={onPreviewClick}
            onDraftJobClick={onDraftJobClick}
            googleAuthCode={googleAuthCode}
            defaultState={defaultState}
            headerTitle
          />
        </div>
        <div className="PreferenceMobile_header" />
      </div>
    );
  }
}

export default InterviewPreferenceMobile;
