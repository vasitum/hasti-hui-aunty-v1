import React from "react";
import { Button } from "semantic-ui-react";
import AccordionCardContainer from "../../../Cards/AccordionCardContainer";

import CheckBoxCircleRIghtCheck from "../../../Forms/FormFields/CheckBoxCircleRIghtCheck";

import CheckBoxRightCheck from "../../../Forms/FormFields/CheckBoxRightCheck";
// import SelectOptionField from "../../../Forms/FormFields/SelectOptionField";

import InputFieldSementic from "../../../Forms/FormFields/InputFieldSementic";
// import InfoIconLabel from "../../../utils/InfoIconLabel";
import SelectOption from "../../../Forms/FormFields/SelectOption";

import FlatDefaultBtn from "../../../Buttons/FlatDefaultBtn";

import IcClose from "../../../../assets/svg/IcCloseIcon";
import IcPlus from "../../../../assets/svg/IcPlus";

// import InputField from "../../../Forms/FormFields/InputField";

import FlatPickr from "react-flatpickr";

import { Grid } from "semantic-ui-react";

import { REVIEW_SCREENING } from "../../../../strings";

import { format } from "date-fns";

import "./index.scss";

const CardHeaderTitle = ({ checked, onChange, disableKnockout }) => {
  return (
    <div className="cardHeader QuestionCard_header">
      <div className="leftHeader">
        <CheckBoxCircleRIghtCheck
          checked={checked}
          onChange={onChange}
          name={"shift"}
        />
      </div>
      <div className="QuestionCard_headerTitle rightHeader_width">
        <p className="titleHead">
          {REVIEW_SCREENING.WORKING_SHIFT_PREFERENCE.HEADER_TITLE}
        </p>
        {/* <p className="subTitle">
          {REVIEW_SCREENING.WORKING_SHIFT_PREFERENCE.SUB_TITLE}
        </p> */}
      </div>
    </div>
  );
};

class WorkingShiftPreference extends React.Component {
  constructor(props) {
    super(props);
  }

  state = {
    shift: ""
  };

  componentDidMount() {
    const { data } = this.props;

    this.setState({
      shift: data.workDays.shift
    });
  }

  onInputChange = (e, { value, name }) => {
    const { onShiftChange } = this.props;

    this.setState(
      {
        [name]: value
      },
      () => {
        onShiftChange(e, { value: value, name: name });
      }
    );
  };

  render() {
    const {
      data,
      onQuestionChange,
      onKnockoutChange,
      onShiftChange,
      disableKnockout
    } = this.props;

    const { shift } = this.state;

    return (
      <div className="MatchSkills questionDefault_container">
        <AccordionCardContainer
          active
          cardHeader={
            <CardHeaderTitle
              checked={data.checked.shift}
              onChange={onQuestionChange}
            />
          }>
          <div className="MatchSkills_body workExperience questionDefault_body">
            <div className="QuestionCard_header">
              <div className="leftHeader" />
              <Grid>
                <Grid.Row className="MatchSkills_headerBody">
                  <Grid.Column width={5}>
                    <div className="Skill_contianer pb-15">
                      {/* <p>
                        {
                          REVIEW_SCREENING.WORKING_SHIFT_PREFERENCE
                            .START_TIME_INPUT_LABEL
                        }
                      </p> */}
                      {/* <InputFieldSementic
                        placeholder={"Enter Shift Name"}
                        name="shift"
                        value={shift}
                        key={"inputShift"}
                        onChange={this.onInputChange}
                      /> */}
                      {/* <FlatPickr
                        onChange={val =>
                          onShiftChange(null, { value: val, name: "time" })
                        }
                        value={data.workDays.time}
                        options={{
                          dateFormat: "H:i",
                          enableTime: true,
                          noCalendar: true,
                          time_24hr: true
                        }}
                        placeholder={"Enter Start time"}
                      /> */}
                      <div>
                        {/* {REVIEW_SCREENING.WORK_EXP.MIN_EXP_INPUT_LABEL} */}
                        Start Time:{" "}
                        <span className="match_skill_content">
                          {data.workDays
                            ? data.workDays.time
                              ? format(Number(data.workDays.time), "HH:mm")
                              : "NA"
                            : "NA"}
                        </span>
                      </div>
                    </div>
                  </Grid.Column>
                  <Grid.Column width={5}>
                    <div className="experience_container">
                      {/* <p>
                        {
                          REVIEW_SCREENING.WORKING_SHIFT_PREFERENCE
                            .END_TIME_INPUT_LABEL
                        }
                      </p> */}
                      <div className="dateTiming_container">
                        {/* <div className="startTime">
                          <FlatPickr
                            options={{
                              dateFormat: "H:i",
                              enableTime: true,
                              noCalendar: true
                            }}
                            placeholder={"Enter Start time"}
                          />
                        </div> */}
                        <div className="endTime">
                          {/* <FlatPickr
                            onChange={val =>
                              onShiftChange(null, { value: val, name: "days" })
                            }
                            value={data.workDays.days}
                            options={{
                              dateFormat: "H:i",
                              enableTime: true,
                              noCalendar: true,
                              time_24hr: true
                            }}
                            placeholder={"Enter End time"}
                          /> */}
                          <div>
                            {/* {REVIEW_SCREENING.WORK_EXP.MIN_EXP_INPUT_LABEL} */}
                            End Time:{" "}
                            <span className="match_skill_content">
                              {data.workDays
                                ? data.workDays.days
                                  ? format(Number(data.workDays.days), "HH:mm")
                                  : "NA"
                                : "NA"}{" "}
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Grid.Column>
                  <Grid.Column width={3}>
                    <div className="question_container">
                      <CheckBoxRightCheck
                        disabled={disableKnockout}
                        checked={
                          data.workDays
                            ? data.workDays.mandatory
                              ? data.workDays.mandatory
                              : false
                            : false
                        }
                        onChange={e => {
                          onKnockoutChange(e, { name: "workDays" });
                        }}
                      />
                      <p title="‘Check’ if you wish to be strict with mentioned specifications.">
                        Knockout question
                      </p>
                    </div>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </div>
          </div>
        </AccordionCardContainer>
      </div>
    );
  }
}

export default WorkingShiftPreference;
