import React from "react";

import { Header, Form } from "semantic-ui-react";
// import { Form } from "formsy-semantic-ui-react";

import DividerHorizontal from "../../../CardElements/DividerHorizontal";

// import InfoIconLabel from "../../../utils/InfoIconLabel";
import IcRightCheck from "../../../../assets/svg/IcRightCheck";

import MatchSkills from "../MatchSkills";

import WorkExperience from "../WorkExperience";
import EducationEligibility from "../EducationEligibility";
import JoiningTime from "../JoiningTime";
import WorkingShiftPreference from "../WorkingShiftPreference";
import JobLocation from "../JobLocation";
import { REVIEW_SCREENING } from "../../../../strings";

import "./index.scss";

class ScreeningQuestionCard extends React.Component {
  constructor(props) {
    super(props);
  }

  state = {};

  render() {
    const { props } = this;

    const {
      ColumnWidthLeftSide,
      ColumnWidthRightSide,
      data,
      stateData,
      onSkillChange,
      onSkillInputChange,
      onSkillMarkAll,
      onSkillRemove,
      onQuestionChange,
      onKnockoutChange,
      onSkillAddMore,
      onShiftChange,
      onjoiningProbChange,
      onNextClick,
      disableKnockout,
      currentLoc
    } = props;
    return (
      <div className="ScreeningQuestionCard">
        {/* {data.checked.location ? (
          <div className="QuestionCard_header">
            <div className="leftHeader headerTitle">
              <span>
                <IcRightCheck height="10" width="12" pathcolor="#43a047" />
              </span>
            </div>
            <div className="rightHeader">
              <div>
                <Header as="h3">
                  {REVIEW_SCREENING.REMOTE_LOCATION.HEADER_TITLE}
                </Header>
                <p>{REVIEW_SCREENING.REMOTE_LOCATION.SUB_TITLE}</p>
              </div>

              <DividerHorizontal />
            </div>
            <JobLocation />

            <div className="QuestionCard_header divider_container">
              <div className="leftHeader" />
              <div className="rightHeader">
                <DividerHorizontal />
              </div>
            </div>
          </div>
        ) : null} */}

        <div className="QuestionCard_body">
          <Form key={"x9k"}>
            <JobLocation
              disableKnockout={disableKnockout}
              onKnockoutChange={onKnockoutChange}
              data={data}
              currentLoc={currentLoc}
              onQuestionChange={onQuestionChange}
            />

            <div className="QuestionCard_header divider_container">
              <div className="leftHeader" />
              <div className="rightHeader">
                <DividerHorizontal />
              </div>
            </div>

            <MatchSkills
              key={"x_3"}
              onSkillChange={onSkillChange}
              onSkillInputChange={onSkillInputChange}
              onSkillMarkAll={onSkillMarkAll}
              onSkillRemove={onSkillRemove}
              onSkillAddMore={onSkillAddMore}
              onQuestionChange={onQuestionChange}
              data={data}
              stateData={stateData}
              ColumnWidthLeft={ColumnWidthLeftSide}
              ColumnWidthRight={ColumnWidthRightSide}
              disableKnockout={disableKnockout}
            />

            <div className="QuestionCard_header divider_container">
              <div className="leftHeader" />
              <div className="rightHeader">
                <DividerHorizontal />
              </div>
            </div>

            <WorkExperience
              key={"x_4"}
              onKnockoutChange={onKnockoutChange}
              onQuestionChange={onQuestionChange}
              data={data}
              disableKnockout={disableKnockout}
            />

            <div className="QuestionCard_header divider_container">
              <div className="leftHeader" />
              <div className="rightHeader">
                <DividerHorizontal />
              </div>
            </div>

            <EducationEligibility
              key={"x_5"}
              onKnockoutChange={onKnockoutChange}
              onQuestionChange={onQuestionChange}
              data={data}
              disableKnockout={disableKnockout}
            />

            <div className="QuestionCard_header divider_container">
              <div className="leftHeader" />
              <div className="rightHeader">
                <DividerHorizontal />
              </div>
            </div>

            <WorkingShiftPreference
              key={"x_6"}
              onShiftChange={onShiftChange}
              onKnockoutChange={onKnockoutChange}
              onQuestionChange={onQuestionChange}
              data={data}
              disableKnockout={disableKnockout}
            />

            <div className="QuestionCard_header divider_container">
              <div className="leftHeader" />
              <div className="rightHeader">
                <DividerHorizontal />
              </div>
            </div>

            <JoiningTime
              key={"x_7"}
              onjoiningProbChange={onjoiningProbChange}
              onKnockoutChange={onKnockoutChange}
              onQuestionChange={onQuestionChange}
              data={data}
              disableKnockout={disableKnockout}
            />
          </Form>
        </div>
      </div>
    );
  }
}

export default ScreeningQuestionCard;
