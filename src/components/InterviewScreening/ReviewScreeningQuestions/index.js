import React from "react";
import { Responsive } from "semantic-ui-react";
import ReactGA from "react-ga";
import ScreeningQuestionsContainer from "./ScreeningQuestionsContainer";
import ReviewScreeningQuestionsMobile from "./ReviewScreeningQuestionsMobile";
import { withRouter } from "react-router-dom";
// import {REVIEW_SCREENING} from "../../../strings";

import "./index.scss";

class ReviewScreeningQuestions extends React.Component {

  componentDidMount() {
    const { match } = this.props;
    // console.log("check url",  match.url);
    ReactGA.pageview(`${match.url}`);
  }
  render() {
    const {
      data,
      onQuestionChange,
      onSkillChange,
      onSkillInputChange,
      onSkillMarkAll,
      onSkillRemove,
      onKnockoutChange,
      onSkillAddMore,
      onShiftChange,
      onjoiningProbChange,
      onNextClick,
      onPreviewClick,
      onDraftJobClick,
      isEdit,
      disableKnockout,
      currentLoc
    } = this.props;

    return (
      <div className="ReviewScreeningQuestions">
        <Responsive maxWidth={1024}>
          <ReviewScreeningQuestionsMobile
            key={"x_3"}
            isEdit={isEdit}
            onSkillChange={onSkillChange}
            onQuestionChange={onQuestionChange}
            onSkillInputChange={onSkillInputChange}
            onSkillMarkAll={onSkillMarkAll}
            onSkillRemove={onSkillRemove}
            onKnockoutChange={onKnockoutChange}
            onSkillAddMore={onSkillAddMore}
            onShiftChange={onShiftChange}
            onjoiningProbChange={onjoiningProbChange}
            onNextClick={onNextClick}
            onPreviewClick={onPreviewClick}
            onDraftJobClick={onDraftJobClick}
            data={data}
            disableKnockout={disableKnockout}
            currentLoc={currentLoc}
          />
        </Responsive>
        <Responsive minWidth={1025}>
          <ScreeningQuestionsContainer
            key={"x_4"}
            isEdit={isEdit}
            onSkillChange={onSkillChange}
            onQuestionChange={onQuestionChange}
            onSkillInputChange={onSkillInputChange}
            onSkillMarkAll={onSkillMarkAll}
            onSkillRemove={onSkillRemove}
            onKnockoutChange={onKnockoutChange}
            onSkillAddMore={onSkillAddMore}
            onShiftChange={onShiftChange}
            onjoiningProbChange={onjoiningProbChange}
            onNextClick={onNextClick}
            onPreviewClick={onPreviewClick}
            onDraftJobClick={onDraftJobClick}
            data={data}
            disableKnockout={disableKnockout}
            currentLoc={currentLoc}
          />
        </Responsive>
      </div>
    );
  }
}

export default withRouter(ReviewScreeningQuestions);
