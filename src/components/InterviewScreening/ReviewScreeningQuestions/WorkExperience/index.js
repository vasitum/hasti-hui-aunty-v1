import React from "react";
import { Button } from "semantic-ui-react";
import AccordionCardContainer from "../../../Cards/AccordionCardContainer";

import CheckBoxCircleRIghtCheck from "../../../Forms/FormFields/CheckBoxCircleRIghtCheck";

import CheckBoxRightCheck from "../../../Forms/FormFields/CheckBoxRightCheck";
// import SelectOptionField from "../../../Forms/FormFields/SelectOptionField";

import InputFieldSementic from "../../../Forms/FormFields/InputFieldSementic";
// import InfoIconLabel from "../../../utils/InfoIconLabel";
import SelectOption from "../../../Forms/FormFields/SelectOption";

import FlatDefaultBtn from "../../../Buttons/FlatDefaultBtn";

import IcClose from "../../../../assets/svg/IcCloseIcon";
import IcPlus from "../../../../assets/svg/IcPlus";

import { REVIEW_SCREENING } from "../../../../strings";

// import InputField from "../../../Forms/FormFields/InputField";

import { Grid } from "semantic-ui-react";

const CardHeaderTitle = ({ checked, onChange }) => {
  return (
    <div className="cardHeader QuestionCard_header">
      <div className="leftHeader">
        <CheckBoxCircleRIghtCheck
          checked={checked}
          onChange={onChange}
          name={"experience"}
        />
      </div>
      <div className="QuestionCard_headerTitle rightHeader_width">
        <p className="titleHead">{REVIEW_SCREENING.WORK_EXP.HEADER_TITLE}</p>
        {/* <p className="subTitle">{REVIEW_SCREENING.WORK_EXP.SUB_TITLE}</p> */}
      </div>
    </div>
  );
};

const WorkExperience = ({
  data,
  disableKnockout,
  onQuestionChange,
  onKnockoutChange
}) => {
  return (
    <div className="MatchSkills questionDefault_container">
      <AccordionCardContainer
        active
        cardHeader={
          <CardHeaderTitle
            checked={data.checked.experience}
            onChange={onQuestionChange}
          />
        }>
        <div className="MatchSkills_body workExperience questionDefault_body">
          <div className="QuestionCard_header">
            <div className="leftHeader" />
            <Grid>
              <Grid.Row className="MatchSkills_headerBody">
                <Grid.Column width={5}>
                  <div className="Skill_contianer">
                    {/* <InputFieldSementic
                      style={{
                        pointerEvents: "none"
                      }}
                      name="year"
                      value={data.exp.min}
                      placeholder="Eg. 2"
                    /> */}
                    <div>
                      {/* {REVIEW_SCREENING.WORK_EXP.MIN_EXP_INPUT_LABEL} */}
                      Min:{" "}
                      <span className="match_skill_content">
                        {data.exp.min}
                      </span>
                    </div>
                  </div>
                </Grid.Column>
                <Grid.Column width={5}>
                  <div className="experience_container">
                    {/* <InputFieldSementic
                      style={{
                        pointerEvents: "none"
                      }}
                      name="year"
                      value={data.exp.max}
                      placeholder="Eg. 06"
                    /> */}
                    <div>
                      {/* {REVIEW_SCREENING.WORK_EXP.MAX_EXP_INPUT_LABEL} */}
                      Max:{" "}
                      <span className="match_skill_content">
                        {data.exp.max}
                      </span>
                    </div>
                  </div>
                </Grid.Column>
                <Grid.Column width={3}>
                  <div className="question_container checkBox_container">
                    <CheckBoxRightCheck
                      disabled={disableKnockout}
                      checked={data.exp.mandatory}
                      onChange={e => {
                        onKnockoutChange(e, { name: "exp" });
                      }}
                    />
                    <p title="‘Check’ if you wish to be strict with mentioned specifications.">
                      Knockout question
                    </p>
                  </div>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </div>
        </div>
      </AccordionCardContainer>
    </div>
  );
};

export default WorkExperience;
