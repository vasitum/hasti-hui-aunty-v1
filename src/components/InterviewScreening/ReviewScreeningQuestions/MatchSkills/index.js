import React from "react";
import { Button } from "semantic-ui-react";
import AccordionCardContainer from "../../../Cards/AccordionCardContainer";

import CheckBoxCircleRIghtCheck from "../../../Forms/FormFields/CheckBoxCircleRIghtCheck";

import CheckBoxRightCheck from "../../../Forms/FormFields/CheckBoxRightCheck";
// import SelectOptionField from "../../../Forms/FormFields/SelectOptionField";

import InputFieldSementic from "../../../Forms/FormFields/InputFieldSementic";
// import InfoIconLabel from "../../../utils/InfoIconLabel";
import SelectOption from "../../../Forms/FormFields/SelectOption";

import FlatDefaultBtn from "../../../Buttons/FlatDefaultBtn";

import IcClose from "../../../../assets/svg/IcCloseIcon";
import IcPlus from "../../../../assets/svg/IcPlus";

import PropTypes from "prop-types";

import getPlaceholder from "../../../Forms/FormFields/Placeholder";

// import InputField from "../../../Forms/FormFields/InputField";

import { REVIEW_SCREENING } from "../../../../strings";

import "./index.scss";
import { Grid } from "semantic-ui-react";

function canShowAddButton(data) {
  if (data.skills.length < data.skillOptions.length && data.skills.length < 5) {
    return true;
  }

  return false;
}

function getSkillOptionItem(data) {
  let items = [];
  for (let i = 1; i < 30; i++) {
    if (i === 1) {
      items.push({
        text: `${i} year`,
        value: i,
        key: i
      });
      return;
    }

    items.push({
      text: `${i} years`,
      value: i,
      key: i
    });
  }

  return items;
}

function isChecked(id, skills) {
  for (let i = skills.length - 1; i >= 0; i--) {
    const element = skills[i];
    if (element["_id"] === id) {
      return true;
    }
  }

  return false;
}

function isCheckedMandatory(id, skills) {
  for (let i = skills.length - 1; i >= 0; i--) {
    const element = skills[i];
    if (element["_id"] === id) {
      return element.mandatory;
    }
  }

  return false;
}

const CardHeaderTitle = ({ checked, onChange }) => {
  return (
    <div className="cardHeader QuestionCard_header">
      <div className="leftHeader">
        <CheckBoxCircleRIghtCheck
          checked={checked}
          onChange={onChange}
          name="skills"
        />
      </div>
      <div className="rightHeader_width">
        <p>
          {REVIEW_SCREENING.MATCH_SKILLS.HEADER_TITLE}{" "}
          <span>{REVIEW_SCREENING.MATCH_SKILLS.SPAN_TITLE}</span>
        </p>
      </div>
    </div>
  );
};

class MatchSkills extends React.Component {
  constructor(props) {
    super(props);
  }

  state = {};

  render() {
    const { props } = this;

    const {
      ColumnWidthLeft,
      ColumnWidthRight,
      data,
      stateData,
      onQuestionChange,
      onSkillChange,
      onSkillInputChange,
      onSkillMarkAll,
      onSkillRemove,
      onSkillAddMore,
      disableKnockout
    } = props;

    console.log(data);
    return (
      <div key={"xy"} className="MatchSkills questionDefault_container">
        <AccordionCardContainer
          key={"ksmdk"}
          active
          cardHeader={
            <CardHeaderTitle
              checked={data.checked.skills}
              onChange={onQuestionChange}
            />
          }>
          <div
            key={"sndj"}
            className="MatchSkills_body machskill_container questionDefault_body">
            <div key={"asknd"} className="QuestionCard_header">
              <div key={"dsakmn"} className="leftHeader" />
              <Grid key={"salmd"}>
                <Grid.Row key={"asdmkj"} className="MatchSkills_header">
                  <Grid.Column width={ColumnWidthLeft}>
                    <p>{REVIEW_SCREENING.MATCH_SKILLS.SKILL_INPUT_LABEL}</p>
                  </Grid.Column>
                  <Grid.Column width={ColumnWidthLeft}>
                    {/* {getPlaceholder(
                        `Min. years`,
                        `Years of experience `
                      )} */}
                    <p>{REVIEW_SCREENING.MATCH_SKILLS.EXP_INPUT_LABEL}</p>
                  </Grid.Column>
                  <Grid.Column
                    width={ColumnWidthRight}
                    className="checkBoxMarkAll">
                    {/* <div className="checkBoxTitle">
                      <CheckBoxRightCheck
                        disabled={disableKnockout}
                        checked={data.skillMarkAll}
                        onChange={onSkillMarkAll}
                        labelTitle="Mark All"
                      />
                    </div> */}
                  </Grid.Column>
                  <Grid.Column width={ColumnWidthRight}>
                    {/* <p>Skill name</p> */}
                  </Grid.Column>
                </Grid.Row>

                {data.skillsData
                  ? data.skillsData.map((val, idx) => {
                      if (!val) {
                        return null;
                      }
                      return (
                        <Grid.Row
                          key={val._id}
                          className="MatchSkills_headerBody">
                          <Grid.Column key={"askmdkas"} width={ColumnWidthLeft}>
                            <div className="Skill_contianer">
                              {/* <SelectOption
                                placeholder="Select"
                                optionsItem={data.skillOptions}
                                name="interview_time"
                                value={val._id}
                                onChange={(e, { value }) =>
                                  onSkillChange(e, {
                                    value: value,
                                    idx: idx
                                  })
                                }
                              /> */}
                              <div className="match_skill_content skill-align">
                                {
                                  <CheckBoxRightCheck
                                    key={val._id}
                                    checked={isChecked(val._id, data.skills)}
                                    value={val._id}
                                    onChange={(e, { value }) =>
                                      onSkillAddMore(e, {
                                        value: value,
                                        idx: idx
                                      })
                                    }
                                  />
                                }
                                <p>{val.name}</p>
                              </div>
                            </div>
                          </Grid.Column>
                          <Grid.Column key={val._id} width={ColumnWidthLeft}>
                            <div className="experience_container">
                              <div className="match_skill_content">
                                {val.exp ? val.exp : "Fresher"}
                              </div>
                            </div>
                          </Grid.Column>
                          <Grid.Column
                            width={ColumnWidthRight}
                            className="checkBox_Container">
                            <div className="question_container">
                              <CheckBoxRightCheck
                                disabled={
                                  disableKnockout ||
                                  !isChecked(val._id, data.skills)
                                }
                                checked={isCheckedMandatory(
                                  val._id,
                                  data.skills
                                )}
                                onChange={e =>
                                  onSkillInputChange(e, {
                                    value: "",
                                    idx: idx,
                                    skId: val._id
                                  })
                                }
                              />
                              <p title="‘Check’ if you wish to be strict with mentioned specifications.">
                                Knockout question
                              </p>
                            </div>
                          </Grid.Column>
                          <Grid.Column
                            width={ColumnWidthRight}
                            className="removeBox_container">
                            <div className="remove_container">
                              {/*data.skills.length > 1 && (
                                <Button
                                  onClick={e =>
                                    onSkillRemove(e, { value: val._id })
                                  }>
                                  <IcClose
                                    width="10"
                                    height="10"
                                    pathcolor="#c8c8c8"
                                  />
                                  <span>Remove</span>
                                </Button>
                              )*/}
                            </div>
                          </Grid.Column>
                        </Grid.Row>
                      );
                    })
                  : null}
              </Grid>
            </div>
            {/* canShowAddButton(data) ? (
              <div className="QuestionCard_header addmoreBtn">
                <div className="leftHeader" />
                <div className="">
                  <FlatDefaultBtn
                    onClick={e => onSkillAddMore(data.skills.length)}
                    btnicon={<IcPlus pathcolor="#c8c8c8" />}
                    btntext="Add skill"
                  />
                </div>
              </div>
            ) : null */}
          </div>
        </AccordionCardContainer>
      </div>
    );
  }
}

MatchSkills.propTypes = {
  ColumnWidthLeft: PropTypes.string,
  ColumnWidthRight: PropTypes.string
};

MatchSkills.defaultProps = {
  ColumnWidthLeft: "5",
  ColumnWidthRight: "3"
};

export default MatchSkills;
