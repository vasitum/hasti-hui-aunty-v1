import React from "react";
import { Container } from "semantic-ui-react";
import ScreeningQuestionHeader from "../ScreeningQuestionHeader";
import ScreeningQuestionCard from "../ScreeningQuestionCard";
import { parse } from "date-fns";

import ScreeningQuestionsFooter from "../ScreeningQuestionsFooter";

import { REVIEW_SCREENING } from "../../../../strings";

class ScreeningQuestionsContainer extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      isShowQuestionsFooter,
      isShowHeader,
      data,
      onQuestionChange,
      onSkillInputChange,
      onSkillMarkAll,
      onSkillChange,
      onSkillRemove,
      onKnockoutChange,
      onSkillAddMore,
      onShiftChange,
      onjoiningProbChange,
      onNextClick,
      onPreviewClick,
      onDraftJobClick,
      isEdit,
      disableKnockout,
      currentLoc
    } = this.props;

    return (
      <div className="ScreeningQuestionsContainer">
        <Container>
          {!isShowHeader ? <ScreeningQuestionHeader /> : null}

          <div className="ScreeningQuestions_body">
            <ScreeningQuestionCard
              key={"x5"}
              data={data}
              onQuestionChange={onQuestionChange}
              onSkillChange={onSkillChange}
              onSkillInputChange={onSkillInputChange}
              onSkillMarkAll={onSkillMarkAll}
              onSkillRemove={onSkillRemove}
              onKnockoutChange={onKnockoutChange}
              onSkillAddMore={onSkillAddMore}
              onShiftChange={onShiftChange}
              onjoiningProbChange={onjoiningProbChange}
              onNextClick={onNextClick}
              onPreviewClick={onPreviewClick}
              disableKnockout={disableKnockout}
              currentLoc={currentLoc}
            />
          </div>
          <div className="screening_note">
            <p>
              {" "}
              <span>{REVIEW_SCREENING.FOOTER_NOTE.HEADER_TITLE}</span>{" "}
              <i>{REVIEW_SCREENING.FOOTER_NOTE.SPAN_TITLE}</i>{" "}
            </p>
          </div>
          <div className="ScreeningQuestions_footer">
            {!isShowQuestionsFooter ? (
              <ScreeningQuestionsFooter
                key={"x6"}
                isEdit={isEdit}
                onNextClick={onNextClick}
                onPreviewClick={onPreviewClick}
                onDraftJobClick={onDraftJobClick}
                data={data}
              />
            ) : null}
          </div>
        </Container>
      </div>
    );
  }
}

export default ScreeningQuestionsContainer;
