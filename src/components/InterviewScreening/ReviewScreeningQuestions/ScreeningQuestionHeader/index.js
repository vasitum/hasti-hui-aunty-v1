import React from "react";

import { Header } from "semantic-ui-react";

import { REVIEW_SCREENING } from "../../../../strings";

import "./index.scss";

const ScreeningQuestionHeader = () => {
  return (
    <div className="ScreeningQuestionHeader">
      <Header as="h2">{REVIEW_SCREENING.HEADER.HEADER_TITLE}</Header>
      <p>{REVIEW_SCREENING.HEADER.SUB_TITLE}</p>
      <div className="subHeader_body">
        <p>
          {" "}
          <span>‘Check’</span> if you wish to be strict with above mentioned
          specifications.{" "}
        </p>
        <p>
          {" "}
          <span> ‘Uncheck’</span> If you wish to go flexible.
        </p>
        <p>
          {" "}
          <span>'Knockout'</span> is disabled for Fresher jobs.
        </p>
      </div>
    </div>
  );
};

export default ScreeningQuestionHeader;
