import React from "react";
import { Button, Grid } from "semantic-ui-react";
import AccordionCardContainer from "../../../Cards/AccordionCardContainer";
import CheckBoxRightCheck from "../../../Forms/FormFields/CheckBoxRightCheck";
import CheckBoxCircleRIghtCheck from "../../../Forms/FormFields/CheckBoxCircleRIghtCheck";
import { REVIEW_SCREENING } from "../../../../strings";

const CardHeaderTitle = ({ checked, data, onChange, disableKnockout }) => {
  return (
    <div className="cardHeader QuestionCard_header">
      <div className="leftHeader">
        <CheckBoxCircleRIghtCheck
          checked={checked}
          onChange={onChange}
          name={"location"}
        />
      </div>
      <div className="QuestionCard_headerTitle rightHeader_width">
        <p className="titleHead">
          {REVIEW_SCREENING.REMOTE_LOCATION.HEADER_TITLE}
        </p>
        {/* <p className="subTitle">{REVIEW_SCREENING.REMOTE_LOCATION.SUB_TITLE}</p> */}
      </div>
    </div>
  );
};

class JobLocation extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { props } = this;
    const { onQuestionChange, data, disableKnockout, onKnockoutChange } = props;
    return (
      <div className="MatchSkills questionDefault_container">
        <AccordionCardContainer
          active
          cardHeader={
            <CardHeaderTitle
              checked={data.checked.location}
              onChange={onQuestionChange}
            />
          }>
          <div className="MatchSkills_body workExperience questionDefault_body">
            <div className="QuestionCard_header">
              <div className="leftHeader" />
              <Grid>
                <Grid.Row className="MatchSkills_headerBody">
                  <Grid.Column width={5}>
                    <div className="Skill_contianer">
                      <div>
                        <span className="match_skill_content">
                          {data.currentLoc}
                        </span>
                      </div>
                    </div>
                  </Grid.Column>
                  <Grid.Column width={5} />
                  <Grid.Column width={3}>
                    <div className="question_container">
                      <CheckBoxRightCheck
                        disabled={disableKnockout}
                        checked={data.locMandatory}
                        onChange={e => {
                          onKnockoutChange(e, { name: "locMandatory" });
                        }}
                      />
                      <p title="‘Check’ if you wish to be strict with mentioned specifications.">
                        Knockout question
                      </p>
                    </div>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </div>
          </div>
        </AccordionCardContainer>
      </div>
    );
  }
}

export default JobLocation;
