import React from "react";
import { Button } from "semantic-ui-react";
import { Link } from "react-router-dom";
import FlatDefaultBtn from "../../../Buttons/FlatDefaultBtn";
import ActionBtn from "../../../Buttons/ActionBtn";

import IcDownArrow from "../../../../assets/svg/IcDownArrow";
import SaveLogo from "../../../../assets/svg/IcSave";
import IcFooterArrowIcon from "../../../../assets/svg/IcFooterArrowIcon";

import IcProfileViewIcon from "../../../../assets/svg/IcProfileView";
// import "./index.scss";

const ScreeningQuestionsFooter = ({
  onNextClick,
  onPreviewClick,
  onDraftJobClick,
  isEdit,
  data
}) => {
  if (isEdit) {
    return (
      <div className="InterviewPreferenceFooter">
        <div className="footer_containerLeft">
          <p>
            {" "}
            <Link to={`/job/edit/${data.jobId}/`}>
              <Button>
                <IcFooterArrowIcon pathcolor="#0b9ed0" />
              </Button>{" "}
              Go Back to “Job details”
            </Link>
          </p>
        </div>
        <div className="footer_containerRight">
          <div className="saveAsDraft">
            <div className="previewBtn">
              <FlatDefaultBtn
                btnicon={<IcProfileViewIcon height="12" pathcolor="#c8c8c8" />}
                btntext="Preview Job"
                type="button"

                // disabled={!this.state.canSubmit}
                onClick={onPreviewClick}
                // className="profileView_Icon"
                style={{
                  paddingLeft: "2px",
                  paddingRight: "15px"
                }}
              />
            </div>


            <FlatDefaultBtn
              // btnicon={<SaveLogo height="12" pathcolor="#c8c8c8" />}
              onClick={onDraftJobClick}
              // disabled={!this.state.canSubmit}
              btntext="Save and Exit"
              type="button"
            />
          </div>
          <div className="footerActionBtn">
            <ActionBtn
              // style={{
              //   opacity: !this.state.canSubmit ? "0.6" : "1"
              // }}
              // onClick={this.onActionBtnClick}
              type="submit"
              actioaBtnText="Save and Next"
              onClick={onNextClick}
              btnIcon={<IcDownArrow pathcolor="#99d9f9" />}
            />
            <p>Interview preference</p>
          </div>
          {/* <div className="cancelBtn">
          <Link to="/job/recruiter/dashboard">
            <FlatDefaultBtn
              btntext="Cancel"
              type="button"
              className="cancelBtn"
            />
          </Link>
        </div> */}
        </div>
      </div>
    );
  }

  return (
    <div className="InterviewPreferenceFooter">
      <div className="footer_containerLeft">
        <p>
          {" "}
          <Link to={`/job/edit/${data.jobId}/`}>
            <Button>
              <IcFooterArrowIcon pathcolor="#0b9ed0" />
            </Button>{" "}
            Go Back to “Job details”
          </Link>
        </p>
      </div>
      <div className="footer_containerRight">
        <div className="saveAsDraft">
          <div className="previewBtn">
            <FlatDefaultBtn
              btnicon={<IcProfileViewIcon height="12" pathcolor="#c8c8c8" />}
              btntext="Preview Job"
              type="button"
              // disabled={!this.state.canSubmit}
              onClick={onPreviewClick}
              // className="profileView_Icon"
              style={{
                paddingLeft: "2px",
                paddingRight: "15px"
              }}
            />
          </div>


          <FlatDefaultBtn
            btnicon={<SaveLogo height="12" pathcolor="#c8c8c8" />}
            onClick={onDraftJobClick}
            // disabled={!this.state.canSubmit}
            btntext="Save as draft"
            type="button"
          />
        </div>
        <div className="footerActionBtn">
          <ActionBtn
            // style={{
            //   opacity: !this.state.canSubmit ? "0.6" : "1"
            // }}
            // onClick={this.onActionBtnClick}
            type="submit"
            actioaBtnText="Next"
            onClick={onNextClick}
            btnIcon={<IcDownArrow pathcolor="#99d9f9" />}
          />
          <p>Interview preference</p>
        </div>
        {/* <div className="cancelBtn">
          <Link to="/job/recruiter/dashboard">
            <FlatDefaultBtn
              btntext="Cancel"
              type="button"
              className="cancelBtn"
            />
          </Link>
        </div> */}
      </div>
    </div>
  );
};

export default ScreeningQuestionsFooter;
