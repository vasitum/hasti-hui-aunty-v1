import React from "react";
import { Button, Input } from "semantic-ui-react";
import AccordionCardContainer from "../../../Cards/AccordionCardContainer";

import CheckBoxCircleRIghtCheck from "../../../Forms/FormFields/CheckBoxCircleRIghtCheck";

import CheckBoxRightCheck from "../../../Forms/FormFields/CheckBoxRightCheck";
// import SelectOptionField from "../../../Forms/FormFields/SelectOptionField";

import InputFieldSementic from "../../../Forms/FormFields/InputFieldSementic";
// import InfoIconLabel from "../../../utils/InfoIconLabel";
import SelectOption from "../../../Forms/FormFields/SelectOption";

import FlatDefaultBtn from "../../../Buttons/FlatDefaultBtn";

import IcClose from "../../../../assets/svg/IcCloseIcon";
import IcPlus from "../../../../assets/svg/IcPlus";

import { REVIEW_SCREENING } from "../../../../strings";

// import InputField from "../../../Forms/FormFields/InputField";

import { Grid } from "semantic-ui-react";

const CardHeaderTitle = ({ checked, onChange }) => {
  return (
    <div className="cardHeader QuestionCard_header">
      <div className="leftHeader">
        <CheckBoxCircleRIghtCheck
          checked={checked}
          onChange={onChange}
          name={"education"}
        />
      </div>
      <div className="QuestionCard_headerTitle rightHeader_width">
        <p className="titleHead">
          {REVIEW_SCREENING.EDUCATION_ELIGIBILITY.HEADER_TITLE}
        </p>
        {/* <p className="subTitle">
          {REVIEW_SCREENING.EDUCATION_ELIGIBILITY.SUB_TITLE}
        </p> */}
      </div>
    </div>
  );
};

const EducationEligibility = ({
  data,
  onQuestionChange,
  onKnockoutChange,
  disableKnockout
}) => {
  return (
    <div className="MatchSkills questionDefault_container">
      <AccordionCardContainer
        active
        cardHeader={
          <CardHeaderTitle
            onChange={onQuestionChange}
            checked={data.checked.education}
          />
        }>
        <div className="MatchSkills_body workExperience questionDefault_body">
          <div className="QuestionCard_header">
            <div className="leftHeader" />
            <Grid>
              <Grid.Row className="MatchSkills_headerBody">
                <Grid.Column width={5}>
                  <div className="Skill_contianer">
                    {/* <InputFieldSementic
                      style={{
                        pointerEvents: "none"
                      }}
                      value={data.education.level}
                    /> */}
                    <div className="match_skill_content">
                      {data.education.level}
                    </div>
                  </div>
                </Grid.Column>
                <Grid.Column width={5} />
                <Grid.Column width={3}>
                  <div className="question_container">
                    <CheckBoxRightCheck
                      disabled={disableKnockout}
                      checked={data.education.mandatory}
                      onChange={e => {
                        onKnockoutChange(e, { name: "education" });
                      }}
                    />
                    <p title="‘Check’ if you wish to be strict with mentioned specifications.">
                      Knockout question
                    </p>
                  </div>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </div>
        </div>
      </AccordionCardContainer>
    </div>
  );
};

export default EducationEligibility;
