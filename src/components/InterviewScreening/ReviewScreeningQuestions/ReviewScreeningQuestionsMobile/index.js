import React from "react";

import MobileHeader from "../../../MobileComponents/MobileHeader";

import BackArrowBtnLeft from "../../../Buttons/BackArrowBtnLeft";
import ScreeningQuestionsContainer from "../ScreeningQuestionsContainer";

import MoreOption from "./MoreOption";

import { withRouter } from "react-router-dom";

import "./index.scss";

class ReviewScreeningQuestionsMobile extends React.Component {
  render() {

    const {onPreviewClick} = this.props;
    return (
      <div className="ReviewScreeningQuestionsMobile">
        <div className="ScreeningQuestions_mobileHeader">
          <MobileHeader
            mobileHeaderLeftColumn="14"
            mobileHeaderRightColumn="2"
            headerLeftIcon={
              <BackArrowBtnLeft onClick={e => this.props.history.go(-1)} />
            }
            headerTitle="Review screeing questions"
            className="isMobileHeader_fixe"
          >
            <MoreOption onPreviewClick={onPreviewClick}/>
          </MobileHeader>
        </div>
        <div className="ScreeningQuestions_mobileBody">
          <ScreeningQuestionsContainer {...this.props} />
        </div>
      </div>
    );
  }
}

export default withRouter(ReviewScreeningQuestionsMobile);
