import React from "react";
import { Button } from "semantic-ui-react";
import AccordionCardContainer from "../../../Cards/AccordionCardContainer";

import CheckBoxCircleRIghtCheck from "../../../Forms/FormFields/CheckBoxCircleRIghtCheck";

import CheckBoxRightCheck from "../../../Forms/FormFields/CheckBoxRightCheck";
// import SelectOptionField from "../../../Forms/FormFields/SelectOptionField";

import InputFieldSementic from "../../../Forms/FormFields/InputFieldSementic";
// import InfoIconLabel from "../../../utils/InfoIconLabel";
import SelectOption from "../../../Forms/FormFields/SelectOption";

import FlatDefaultBtn from "../../../Buttons/FlatDefaultBtn";

import IcClose from "../../../../assets/svg/IcCloseIcon";
import IcPlus from "../../../../assets/svg/IcPlus";

import { REVIEW_SCREENING } from "../../../../strings";

// import InputField from "../../../Forms/FormFields/InputField";

import { Grid } from "semantic-ui-react";

const CardHeaderTitle = ({ checked, onChange, disableKnockout }) => {
  return (
    <div className="cardHeader QuestionCard_header">
      <div className="leftHeader">
        <CheckBoxCircleRIghtCheck
          checked={checked}
          onChange={onChange}
          name={"notice"}
        />
      </div>
      <div className="QuestionCard_headerTitle rightHeader_width">
        <p className="titleHead">
          {REVIEW_SCREENING.JOINING_TIME.HEADER_TITLE}
        </p>
        {/* <p className="subTitle">{REVIEW_SCREENING.JOINING_TIME.SUB_TITLE}</p> */}
      </div>
    </div>
  );
};

const JoiningTime = ({
  data,
  onQuestionChange,
  onKnockoutChange,
  onjoiningProbChange,
  disableKnockout
}) => {
  return (
    <div className="MatchSkills questionDefault_container">
      <AccordionCardContainer
        active
        cardHeader={
          <CardHeaderTitle
            checked={data.checked.notice}
            onChange={onQuestionChange}
          />
        }>
        <div className="MatchSkills_body workExperience questionDefault_body">
          <div className="QuestionCard_header">
            <div className="leftHeader" />
            <Grid>
              <Grid.Row
                className="MatchSkills_headerBody"
                style={{ paddingBottom: "15px" }}>
                <Grid.Column width={5}>
                  <div className="Skill_contianer">
                    {/* <SelectOption
                      placeholder="Select"
                      value={data.joiningProb.joiningProbability}
                      optionsItem={[
                        {
                          text: "Immediate",
                          value: 0,
                          key: 0
                        },
                        {
                          text: "15 days",
                          value: 15,
                          key: 15
                        },
                        {
                          text: "1 Month",
                          value: 30,
                          key: 30
                        },
                        {
                          text: "2 Months",
                          value: 60,
                          key: 60
                        },
                        {
                          text: "3 Months",
                          value: 90,
                          key: 90
                        }
                      ]}
                      onChange={(e, { value }) =>
                        onjoiningProbChange(e, {
                          value: value,
                          name: "joiningProbability"
                        })
                      }
                    /> */}
                    <div className="match_skill_content">
                      {data.joiningProb
                        ? data.joiningProb.joiningProbability
                          ? data.joiningProb.joiningProbability + " days"
                          : "Immediately"
                        : "NA"}
                    </div>
                  </div>
                </Grid.Column>
                <Grid.Column width={5} />
                <Grid.Column width={3}>
                  <div className="question_container">
                    <CheckBoxRightCheck
                      disabled={disableKnockout}
                      checked={
                        data.joiningProb
                          ? data.joiningProb.mandatory
                            ? data.joiningProb.mandatory
                            : false
                          : false
                      }
                      onChange={e => {
                        onKnockoutChange(e, { name: "joiningProb" });
                      }}
                    />
                    <p title="‘Check’ if you wish to be strict with mentioned specifications.">
                      Knockout question
                    </p>
                  </div>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </div>
        </div>
      </AccordionCardContainer>
    </div>
  );
};

export default JoiningTime;
