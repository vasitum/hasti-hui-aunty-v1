import React from "react";
import ReviewScreeningQuestions from "./ReviewScreeningQuestions";
import InterviewPreference from "./InterviewPreference";
import getJobById from "../../api/jobs/getJobById";

import ReactGA from "react-ga";
import { InterviewScreeningString } from "../EventStrings/InterviewScreening";

import getScreeningByJobId from "../../api/jobs/getScreeningByJobId";
import JobConstants from "../../constants/storage/jobs";
import pushToDraft from "../../api/jobs/pushToDraft";
import pushToSave from "../../api/jobs/pushToSave";
import { USER } from "../../constants/api";
import { toast } from "react-toastify";
import { parse } from "date-fns";
import qs from "qs";

function getExperience(exp) {
  const ret = {
    min: 0,
    max: 0
  };

  if (!exp) {
    return ret;
  }

  if (exp.min) {
    ret["min"] = exp.min;
  }

  if (exp.max) {
    ret["max"] = exp.max;
  }

  return ret;
}

function getEducation(edus) {
  if (!edus || !Array.isArray(edus) || !edus.length) {
    return "";
  }

  return edus[0].level;
}

function getSkills(skills) {
  if (!skills || !Array.isArray(skills) || !skills.length) {
    return [];
  }

  // Filter out mandatory skills
  const mandSkills = skills.filter(skill => {
    if (skill.reqType === "mandatory") {
      return true;
    }

    return false;
  });

  if (!mandSkills.length) {
    return [];
  }

  return mandSkills;
}

function parseJobToScreening(job) {
  const skills = getSkills(job.skills);
  const { min, max } = getExperience(job.exp);
  const eduLevel = getEducation(job.edus);

  // console.log(`Min: ${min} || Max: ${max}`);
  return {
    skills: skills,
    exp: {
      min: min,
      max: max
    },
    level: eduLevel,
    workDays: job.workDays,
    joiningProb: job.joiningProb,
    location: job.loc.adrs
  };
}

function getSkillOptions(skills) {
  return skills.map(val => {
    // console.log(`Skills are ${val}`);
    return {
      text: val.name,
      value: val._id
    };
  });
}

class InterviewScreening extends React.Component {
  state = {
    defaultState: null,
    screeningId: "",
    hasScreening: false,
    skillOptions: [],
    skillsData: [],
    skillMarkAll: false,
    currentLoc: "",
    checked: {
      location: false,
      skills: false,
      experience: false,
      education: false,
      shift: false,
      notice: false
    },

    // Interview
    showInterview: false,
    googleAuthCode: "",

    // state for data
    jobId: "",

    education: {
      level: "",
      mandatory: false
    },
    exp: {
      mandatory: false,
      max: 0,
      min: 0
    },
    joiningProb: {
      joiningProbability: 0,
      mandatory: false
    },
    locMandatory: false,
    skills: [],
    userId: "",
    workDays: {
      days: "",
      mandatory: false,
      shift: "",
      time: ""
    },
    disableKnockout: false,
    loading: true
  };

  constructor(props) {
    super(props);

    // async binding
    this.onDraftJobClick = this.onDraftJobClick.bind(this);
    this.isEdit = window.location.pathname.split("/").pop() === "edit";
  }

  onQuestionChange = (e, { name }) => {
    this.setState({
      checked: {
        ...this.state.checked,
        [name]: !this.state.checked[name]
      }
    });
  };

  onShiftChange = (e, { value, name }) => {
    this.setState({
      workDays: {
        ...this.state.workDays,
        [name]: parse(value).getTime()
      }
    });
  };

  onjoiningProbChange = (e, { value, name }) => {
    this.setState({
      joiningProb: {
        ...this.state.joiningProb,
        [name]: value
      }
    });
  };

  onSkillChange = (e, { value, idx, exp }) => {
    let newSkills = [...this.state.skills];

    if (exp) {
      newSkills[idx] = {
        ...newSkills[idx],
        exp: value
      };
      this.setState({
        skills: newSkills
      });

      return;
    }

    const selectedSkills = this.state.skillsData.filter(
      skill => skill._id === value
    );

    newSkills[idx] = selectedSkills[0];

    this.setState({
      skills: newSkills
    });
  };

  onSkillRemove = (e, { value }) => {
    let newSkills = [...this.state.skills];

    this.setState({
      skills: this.state.skills.filter(skill => skill._id !== value)
    });
  };

  onSkillInputChange = (e, { value, idx, name, skId }) => {
    let newSkills = [...this.state.skills];

    if (!skId) {
      console.error("Error in Fixing data");
      return;
    }

    let skillArray = newSkills.filter(skill => skill._id === skId);

    if (!skillArray.length) {
      console.error("Skill Not found");
      return;
    }

    let skill = skillArray[0];
    let finalSkills = newSkills.map(mSkill => {
      if (mSkill._id === skill._id) {
        return {
          ...mSkill,
          mandatory: !mSkill.mandatory
        };
      }

      return mSkill;
    });

    this.setState({
      skills: finalSkills
    });
  };

  onSkillMarkAll = e => {
    const { skillMarkAll, skills } = this.state;
    this.setState({
      skillMarkAll: !skillMarkAll,
      skills: skills.map(skill => {
        return {
          ...skill,
          mandatory: !skillMarkAll
        };
      })
    });
  };

  onSkillAddMore = (e, { value, idx }) => {
    const { skills } = this.state;

    let hasSkill = false;

    console.log("SKILL_ADDMORE_REQUEST", idx);

    for (let i = 0; i < skills.length; i++) {
      const element = skills[i];
      if (element["_id"] === value) {
        console.log("Element is", element);

        hasSkill = true;
        break;
      }
    }

    if (hasSkill) {
      this.setState({
        skills: skills.filter(skill => {
          if (skill["_id"] === value) {
            return false;
          }

          return true;
        })
      });
    } else {
      if (skills.length >= 5) {
        toast("You can only select upto 5 skills");
        return;
      }

      this.setState({
        skills: [...this.state.skills, this.state.skillsData[idx]]
      });
    }
  };

  onKnockoutChange = (e, { name }) => {
    if (name === "locMandatory") {
      this.setState({
        [name]: !this.state[name]
      });
      return;
    }

    this.setState({
      [name]: {
        ...this.state[name],
        mandatory: !this.state[name].mandatory
      }
    });
  };

  async componentDidMount() {
    // console.log("Screening Props", this.props);
    const { id } = this.props.match.params;

    // logging things out
    const parsedSearch = qs.parse(this.props.location.search);
    const keys = Object.keys(parsedSearch);
    if (keys.length > 0) {
      if (keys.indexOf("?code") >= 0) {
        this.setState({
          showInterview: true,
          googleAuthCode: parsedSearch["?code"]
        });
      } else {
        this.setState({
          showInterview: true,
          googleAuthCode: "ERROR"
        });
      }
    }

    // TODO call async requests
    try {
      const resScreening = await getScreeningByJobId(id);
      let hasScreening = false;
      let data;

      if (resScreening.status === 200) {
        data = await resScreening.json();
        hasScreening = true;
        this.setState({
          defaultState: data,
          screeningId: data._id,
          jobId: id,
          checked: {
            ...this.state.checked,
            location: data.locMandatory,
            skills: data.skills ? true : false,
            experience: data.exp ? true : false,
            education: data.education ? true : false,
            shift: data.workDays ? true : false,
            notice: data.joiningProb ? true : false
          },
          locMandatory: data.locMandatory ? data.locMandatory : false,
          exp: data.exp
            ? data.exp
            : {
                mandatory: false,
                max: 0,
                min: 0
              },
          education: data.education
            ? data.education
            : {
                level: "",
                mandatory: false
              },
          workDays: data.workDays
            ? data.workDays
            : {
                days: "",
                mandatory: false,
                shift: "",
                time: ""
              },
          joiningProb: data.joiningProb
            ? data.joiningProb
            : {
                joiningProbability: 0,
                mandatory: false
              }
        });
        // console.log(data);
      }

      const res = await getJobById(id);
      if (res.status === 200) {
        const dataJob = await res.json();

        // save it for preview button
        window.localStorage.setItem(
          JobConstants.JOB_PREVIEW_STATE,
          JSON.stringify(dataJob)
        );

        const jobState = parseJobToScreening(dataJob);
        if (!hasScreening) {
          this.setState(
            {
              loading: false,
              jobId: id,
              checked: {
                ...this.state.checked,
                location: !dataJob.remote
              },
              currentLoc: jobState.location,
              locMandatory: !dataJob.remote,
              exp: {
                ...this.state.exp,
                min: jobState.exp.min,
                max: jobState.exp.max
              },
              education: {
                ...this.state.education,
                level: jobState.level
              },
              workDays: {
                ...jobState.workDays
              },
              joiningProb: {
                ...jobState.joiningProb
              },
              skillOptions: getSkillOptions(jobState.skills),
              skillsData: jobState.skills,
              skills: []
            },
            () => {
              this.setState({
                disableKnockout: !jobState.exp.min ? true : false
              });
            }
          );
        } else {
          this.setState(
            {
              loading: false,
              hasScreening: true,
              currentLoc: jobState.location,
              skillOptions: getSkillOptions(jobState.skills),
              skillsData: jobState.skills,
              workDays: {
                ...jobState.workDays
              },
              joiningProb: {
                ...jobState.joiningProb
              },
              education: {
                ...this.state.education,
                level: jobState.level
              },
              exp: {
                ...this.state.exp,
                min: jobState.exp.min,
                max: jobState.exp.max
              },
              skills: data.skills ? data.skills : []
            },
            () => {
              this.setState({
                disableKnockout: data.exp
                  ? !data.exp.min
                    ? true
                    : false
                  : false
              });
            }
          );
        }
        // console.log(data);
      }
    } catch (error) {
      console.log(error);
    }
  }

  validateScreening = () => {
    const { checked, skills } = this.state;

    if (checked.skills && !skills.length) {
      toast("Please, select atleast 1 skill to ask question");
      return false;
    }

    return true;
  };

  onNextClick = e => {
    if (!this.validateScreening()) {
      return;
    }

    this.setState({
      showInterview: true
    });

    const UserId = window.localStorage.getItem(USER.UID);

    ReactGA.event({
      category: InterviewScreeningString.ReviewScreening.category,
      action: InterviewScreeningString.ReviewScreening.action,
      value: UserId,
    });
  };

  // componentDidUpdate() {
  //   // window.onpopstate = e => {
  //   //   //your code...
  //   //   // this.setState({
  //   //   //   showInterview: false
  //   //   // });
  //   //   // alert("hello");
  //   // };
  //   // history.pushState(null, null, location.href);
  //   // window.onpopstate = function() {
  //   //   history.go(1);
  //   // };
  //   history.listen(location => {
  //     if (location.action === "POP") {
  //       // Do your stuff
  //       alert("dfgfd");
  //     }
  //   });
  // }

  onPrevClick = e => {
    this.setState({
      showInterview: false
    });
  };

  onPreviewJobClick = e => {
    this.props.history.push("/job/preview");
  };

  async onDraftJobClick(data) {
    let sendData;

    if (!this.validateScreening()) {
      return;
    }

    if (!data) {
      sendData = {
        _id: this.state.hasScreening ? this.state.screeningId : null,
        avrgTime: 0,
        interViewType: {
          provider: "",
          scheduleType: "MANUAL"
        },
        interviewContactInfo: "",
        interviewContactNumber: "",
        interviewManage: true,
        interviewMode: "",
        interviewSubMode: "",
        notifyEmail: [],
        education: this.state.checked.education ? this.state.education : null,
        exp: this.state.checked.experience ? this.state.exp : null,
        jobId: this.state.jobId,
        joiningProb: this.state.checked.notice ? this.state.joiningProb : null,
        locMandatory: this.state.checked.location
          ? this.state.locMandatory
          : false,
        skills: this.state.checked.skills ? this.state.skills : null,
        userId: window.localStorage.getItem(USER.UID),
        workDays: this.state.checked.shift ? this.state.workDays : null
      };
    } else {
      sendData = {
        _id: this.state.hasScreening ? this.state.screeningId : null,
        avrgTime: data.avrgTime,
        interViewType: data.interViewType,
        interviewContactInfo: data.interviewContactInfo,
        interviewContactNumber: data.interviewContactNumber,
        interviewManage: data.interviewManage,
        interviewMode: data.interviewMode,
        interviewSubMode: data.interviewSubMode,
        notifyEmail: data.notifyEmail,
        education: this.state.checked.education ? this.state.education : null,
        exp: this.state.checked.experience ? this.state.exp : null,
        jobId: this.state.jobId,
        joiningProb: this.state.checked.notice ? this.state.joiningProb : null,
        locMandatory: this.state.checked.location
          ? this.state.locMandatory
          : false,
        skills: this.state.checked.skills ? this.state.skills : null,
        userId: window.localStorage.getItem(USER.UID),
        workDays: this.state.checked.shift ? this.state.workDays : null
      };
    }

    try {
      let res;
      if (this.isEdit) {
        res = await pushToSave(sendData);
      } else {
        res = await pushToDraft(sendData);
      }

      if (res.status === 200) {
        window.localStorage.removeItem(JobConstants.JOB_PREVIEW_STATE);
        this.props.history.push(`/job/view/${this.state.jobId}`);
      }
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    
    return (
      <div>
        {!this.state.showInterview ? (
          <ReviewScreeningQuestions
            key={"1x"}
            isEdit={this.isEdit}
            currentLoc={this.state.currentLoc}
            disableKnockout={this.state.disableKnockout}
            onSkillChange={this.onSkillChange}
            onSkillInputChange={this.onSkillInputChange}
            onSkillRemove={this.onSkillRemove}
            onSkillMarkAll={this.onSkillMarkAll}
            onSkillAddMore={this.onSkillAddMore}
            onShiftChange={this.onShiftChange}
            onjoiningProbChange={this.onjoiningProbChange}
            onKnockoutChange={this.onKnockoutChange}
            onQuestionChange={this.onQuestionChange}
            onNextClick={this.onNextClick}
            onPreviewClick={this.onPreviewJobClick}
            onDraftJobClick={this.onDraftJobClick}
            data={this.state}
          />
        ) : (
          <InterviewPreference
            key={"2x"}
            defaultState={this.state.defaultState}
            googleAuthCode={this.state.googleAuthCode}
            isEdit={this.isEdit}
            data={this.state}
            onPrev={this.onPrevClick}
            onPreviewClick={this.onPreviewJobClick}
            onDraftJobClick={this.onDraftJobClick}
          />
        )}
      </div>
    );
  }
}

export default InterviewScreening;
