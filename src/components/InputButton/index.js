import React from "react";
import PropTypes from "prop-types";
import { Button, Input, Popup, Icon, Form } from "semantic-ui-react";
import IcEmailIcon from "../../assets/svg/IcDrawerEmail";
import emailMeContent from "../../api/utils/emailMeContent";
import { toast } from "react-toastify";

import "./index.scss";

class InputButton extends React.Component {
  state = {
    email: "",
    emailSent: false
  };

  constructor(props) {
    super(props);
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  async onFormSubmit(ev) {
    try {
      const res = await emailMeContent(
        this.state.email,
        window.location.search
      );

      if (res.status === 200) {
        this.props.onEmailSent();
      }
    } catch (error) {
      console.error(error);
      toast("Unable to set alert");
    }
  }

  onValChange = (ev, { value }) => {
    this.setState({
      email: value
    });
  };

  render() {
    const {
      btnIcon,
      popOverContent,
      placeholder,
      inputSize,
      btnSize,
      subTitle,
      ...props
    } = this.props;

    return (
      <div className="InputButton" {...props}>
        <Form onSubmit={this.onFormSubmit}>
          <Input
            type="email"
            size={inputSize}
            icon
            iconPosition="right"
            fluid
            placeholder={placeholder}
            required
            value={this.state.email}
            onChange={this.onValChange}
          />
          <Button
            size={btnSize}
            onClick={this.onSubmitClick}
            icon={btnIcon}
            primary
          />
          {/* <Popup
            trigger={
              <Button
                size={btnSize}
                onClick={this.onSubmitClick}
                icon={btnIcon}
                primary
              />
            }
            content={`If you click this button, we will send emails featuring similar ${subTitle}.`}
          /> */}
        </Form>
      </div>
    );
  }
}

InputButton.propTypes = {
  onSendClick: PropTypes.func,
  btnIcon: PropTypes.string,
  popOverContent: PropTypes.string,
  placeholder: PropTypes.string,
  btnSize: PropTypes.string,
  inputSize: PropTypes.string
};

InputButton.defaultProps = {
  onSendClick: () => alert("Clicked"),
  btnIcon: "mail",
  popOverContent: `
  If you click this button, we will send emails featuring similar jobs.`,
  placeholder: "eg: john@doe.com",
  inputSize: "large",
  btnSize: "medium"
};

export default InputButton;
