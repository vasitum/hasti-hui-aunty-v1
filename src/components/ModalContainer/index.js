import React from "react";

import { Button, Modal, Header } from "semantic-ui-react";
import PropTypes from "prop-types";

import CreateCompanyPage from "../CreateCompanyPage";

import SearchFilterInput from "../Forms/FormFields/SearchFilterInput";

import MobileSearchTab from "../MobileSearchPages/MobileSearchTab";

import "./index.scss";

const ModalContainer = props => {
  // const { actioaBtnText } = props;
  return (
    <div>
      <Modal
        trigger={<Button>Show Modal</Button>}
        closeIconcloseOnDimmerClick={false}>
        <CreateCompanyPage />
      </Modal>

      <SearchFilterInput />

      <MobileSearchTab />
    </div>
  );
};

ModalContainer.propTypes = {
  placeholder: PropTypes.string
};

export default ModalContainer;
