import React, { Component } from 'react'

export default class ScreenMainContainer extends Component {
  render() {
    const {children} = this.props;
    return (
      <div className="ScreenMainContainer">
        {children}
      </div>
    )
  }
}
