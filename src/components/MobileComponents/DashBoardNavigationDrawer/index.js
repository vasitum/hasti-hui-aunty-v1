import React from "react";

import { Tab, Menu, Button, Form, Input } from "semantic-ui-react";

import IcHamburgerMenu from "../../../assets/svg/IcHamburgerMenu";
import IcChatbot from "../../../assets/svg/IcChatbot";
import SearchIcon from "../../../assets/svg/SearchIcon";
import IcFooterArrowIcon from "../../../assets/svg/IcFooterArrowIcon";

import { onInputChange } from "../../../actions/search";

// import IcDrawerMobile from "../../../assets/svg/IcDrawerMobile";

// import IcHome from "../../../assets/svg/IcHome";
// import IcMessage from "../../../assets/svg/IcMessage";

import NavigationDrawer from "../../NavigationDrawer";

import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

import isLoggedOut from "../../../utils/env/isLoggedin";

import "./index.scss";

const SidebarUI = ({ isOpen, ...rest }) => {
  const classes = ["Sidebar", isOpen ? "is-open" : ""];

  return <div aria-hidden={!isOpen} className={classes.join(" ")} {...rest} />;
};

SidebarUI.Overlay = props => <div className="SidebarOverlay" {...props} />;

SidebarUI.Content = ({ width = "20rem", isRight = false, ...rest }) => {
  const classes = ["SidebarContent", isRight ? "is-right" : ""];
  const style = {
    width,
    height: "100%",
    top: 0,
    right: isRight ? `-${width}` : "auto",
    left: !isRight ? `-${width}` : "auto"
  };

  return <div className={classes.join(" ")} style={style} {...rest} />;
};

class Sidebar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: props.isOpen
    };

    this.openSidebar = this.openSidebar.bind(this);
  }

  openSidebar(isOpen = true) {
    this.setState({ isOpen });
  }

  render() {
    const { isOpen } = this.state;
    const { hasOverlay, isRight } = this.props;
    return (
      <SidebarUI isOpen={isOpen}>
        <Button
          className="mobileHeaderBtn"
          type="button"
          onClick={this.openSidebar}>
          <IcHamburgerMenu pathcolor="#6e768a" />
        </Button>

        <SidebarUI.Content isRight={isRight}>
          <NavigationDrawer />
        </SidebarUI.Content>

        {hasOverlay ? (
          <SidebarUI.Overlay onClick={() => this.openSidebar(false)} />
        ) : (
            false
          )}
      </SidebarUI>
    );
  }
}

class DashBoardNavigationDrawer extends React.Component {
  state = {
    isSearchEnabled: false,
    inputVal: ""
  };

  onInputChange = (e, { value }) => {
    this.setState({ inputVal: value });
  };

  onSearchBtnClick = e => {
    this.setState({ isSearchEnabled: true });
  };

  onSearchBackClick = e => {
    this.setState({ isSearchEnabled: false });
  };

  onSearchSubmit = e => {
    if (e.preventDefault) {
      e.preventDefault();
    }
    this.props.onSearchInputChange(this.state.inputVal);
    // redirect if we are not search page
    if (!this.props.location.pathname.startsWith("/search")) {
      this.props.history.push({
        pathname: "/search",
        search: `?query=${this.state.inputVal}`
      });
    } else {
      this.props.history.push({
        search: `?query=${this.state.inputVal}`
      });
    }
  };

  render() {
    const { isSearchEnabled, inputVal } = this.state;
    const loggedOut = isLoggedOut();

    return isSearchEnabled ? (
      <div className="">
        <Form onSubmit={this.onSearchSubmit}>
          <div className="postLoginHeaderLogin_search">
            <div className="backArrow">
              <IcFooterArrowIcon onClick={this.onSearchBackClick} pathcolor="#6e768a" />
            </div>
            <div className="searchField">
              <Input
                onChange={this.onInputChange}
                value={inputVal}
                placeholder={"Search Input"}

              />
              <Button compact className="searchBtn">
                <SearchIcon pathcolor="#6e768a" />
              </Button>
            </div>
          </div>
        </Form>
      </div>
    ) : (
        <div className="DashBoardNavigationDrawer Notification_Container">
          {!loggedOut ? (
            <Sidebar hasOverlay />
          ) : null}
        </div>
      );
  }
}

const mapStateToProps = state => {
  return {
    searchValue: state.search.currentValue
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onSearchInputChange: data => {
      dispatch(onInputChange(data));
    }
  };
};

// export default withRouter(PostLoginMobileHeader);
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(DashBoardNavigationDrawer)
);
