import React, { Children } from "react";
import "./index.scss";
import PropTypes from "prop-types";
import { Header, Grid } from "semantic-ui-react";
import { Link } from "react-router-dom";
const MobileHeader = props => {
  const {
    headerLeftIcon,
    headerTitle,
    userId,
    mobileHeaderLeftColumn,
    mobileHeaderRightColumn,
    children,
    classNames,
    ...resProps
  } = props;
  return (
    <div className={`MobileHeader ${classNames}`} {...resProps}>
      <Grid>
        <Grid.Row>
          <Grid.Column computer={mobileHeaderLeftColumn}>
            <div className="MobileHeader_Left">
              {headerLeftIcon}
              {userId ? (
                <Header
                  as="h3"
                  className="clickTitle"
                  as={Link}
                  to={`/user/public/${userId}`}>
                  {headerTitle}
                </Header>
              ) : (
                <Header as="h3">{headerTitle}</Header>
              )}
            </div>
          </Grid.Column>
          <Grid.Column computer={mobileHeaderRightColumn} textAlign="right">
            {children}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
};

MobileHeader.propTypes = {
  classNames: PropTypes.string,
  mobileHeaderLeftColumn: PropTypes.string,
  mobileHeaderRightColumn: PropTypes.string
};

MobileHeader.defaultProps = {
  // classNames: "MobileHeader",
  mobileHeaderLeftColumn: "9",
  mobileHeaderRightColumn: "7"
};

export default MobileHeader;
