import React from "react";

import { Button, Container, Dropdown } from "semantic-ui-react";

import BannerHeader from "../../../components/PostJobPage/BannerHeader";
import PageBanner from "../../Banners/PageBanner";
import aunty from "../../../assets/banners/aunty.png";
import PostJobHeader from "../../../components/PostJobPage/PostJobHeader";
import ICFilterIcon from "../../../assets/svg/IcFilterIcon";
// import JobDetails from "../../PostJobPage/JobDetails";
import FlatDefaultBtn from "../../Buttons/FlatDefaultBtn";

import CandidatesCardDetail from "../../Cards/CandidatesCardDetail";

import MobileHeader from "../MobileHeader";
import DashboardFilterFooter from "../DashbordMobile/DashboardFilterFooter";
import SearchApplicant from "../DashbordMobile/SearchApplicant";

import "./index.scss";

const SortBy = [
  {
    text: "Latest",
    value: "Latest"
  },
  {
    text: "Relevent",
    value: "Relevent"
  }
];
class MobileContainer extends React.Component {
  render() {
    return (
      <div className="MobileContainer">
        <div className="">
          <MobileHeader headerTitle="Create profile" />
        </div>

        <div className="mobile_body">
          <SearchApplicant />

          <div>
            <Container>
              <CandidatesCardDetail />
            </Container>
          </div>
          {/* <PageBanner size="medium" image={aunty}>
            <Container>
              <BannerHeader />
            </Container>
          </PageBanner> */}
          {/* <Container>
            <PostJobHeader />
            <div className="panel-card">
              <InfoSection
                headerSize="large"
                color="blue"
                headerText="Job details"
                subHeaderText="required"
                subHeaderIcon={IcPrivacyLock}>
               
              </InfoSection>
            </div>
          </Container> */}

          {/* <div className="mobile_footer">
            <Button primary fluid className="nextBtn">
              Next
            </Button>

            <div className="skipBtn">
              <FlatDefaultBtn btntext="Skip for now" />
            </div>
          </div> */}

          <DashboardFilterFooter
            footerIconLeft={<ICFilterIcon pathcolor="#6e768a" />}
            footerTitle="Filter">
            <div className="mobileFooter_left">
              <span>
                Sort by:{" "}
                <Dropdown
                  inline
                  pointing="left right bottom"
                  options={SortBy}
                  defaultValue={SortBy[0].value}
                />
              </span>
            </div>
          </DashboardFilterFooter>
        </div>
      </div>
    );
  }
}

export default MobileContainer;
