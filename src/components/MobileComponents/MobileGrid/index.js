import React from 'react';

import { Grid } from "semantic-ui-react";
import PropTypes from 'prop-types';


import "./index.scss";


const MobileGrid = props => {
  const { children } = props;
  return(
    <Grid>
      <Grid.Row columns={1}>
        <Grid.Column only='mobile'>
          { children }
        </Grid.Column>
      </Grid.Row>
    </Grid>
  )
}



export default MobileGrid;