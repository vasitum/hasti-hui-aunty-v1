import React from "react";

import { Tab, Menu } from "semantic-ui-react";

import Icjob from "../../../assets/svg/IcMyJobs";

import IcHome from "../../../assets/svg/IcHome";
import IcMessage from "../../../assets/svg/IcMessage";
import { withRouter } from "react-router-dom";
import isLoggedOut from "../../../utils/env/isLoggedin";
import UserAccessModal from "../../ModalPageSection/UserAccessModal";
import "./index.scss";

function getFooterPanes(history) {
  return [
    {
      menuItem: (
        <Menu.Item onClick={() => history.push("/")} key="Home">
          <div className="tab_homeMenu">
            <IcHome pathcolor="#6e768a" width="20" height="21" />
          </div>
          <p>Home</p>
        </Menu.Item>
      ),

      render: () => {
        return (
          <div className="mediaGallery">
            <Tab.Pane attached={false}>
              <p>Welcome Home</p>
            </Tab.Pane>
          </div>
        );
      }
    },

    {
      menuItem: (
        <Menu.Item onClick={() => history.push("/job")} key="Jobs">
          <div className="tab_homeMenu">
            <Icjob pathcolor="#6e768a" width="20" height="21" />
          </div>
          <p>Jobs</p>
        </Menu.Item>
      ),

      render: () => {
        return (
          <div className="tab_homeMenu">
            <Tab.Pane attached={false}>
              <p>Welcome Jobe</p>
            </Tab.Pane>
          </div>
        );
      }
    },
    {
      menuItem: (
        <Menu.Item onClick={() => history.push("/messaging")} key="Message">
          <div>
            <IcMessage pathcolor="#6e768a" width="20" height="21" />
          </div>
          <p>Message</p>
        </Menu.Item>
      ),

      render: () => {
        return (
          <div className="tab_homeMenu">
            <Tab.Pane attached={false}>
              <p>Welcome Message</p>
            </Tab.Pane>
          </div>
        );
      }
    }
  ];
}

// const PostLoginMobileFooter = props => {
class PostLoginMobileFooter extends React.Component {
  componentDidMount() {
    let divId = document.getElementById("login_footer_bottom_bar");
    divId
      ? (document.body.style.marginBottom = "60px")
      : (document.body.style.marginBottom = null);
  }

  render() {
    if (isLoggedOut()) {
      return (
        <div className="PostLoginMobileFooter" id="login_footer_bottom_bar">
          <div
            className="Main_Tabs"
            style={{
              display: "flex",
              justifyContent: "space-around",
              alignItems: "center",
              height: "100%"
            }}>
            <UserAccessModal />
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
}

export default withRouter(PostLoginMobileFooter);
