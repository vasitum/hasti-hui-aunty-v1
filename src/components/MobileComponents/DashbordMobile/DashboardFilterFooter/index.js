import React from "react";
import "./index.scss";
import PropTypes from "prop-types";
import { Header, Grid, Modal, Button } from "semantic-ui-react";
import MobileFilter from "../MobileFilter";

import ICFilterIcon from "../../../../assets/svg/IcFilterIcon";

// const footer = () =>{
//   return(
//     <span>
//       Filter
//       <ICFilterIcon pathcolor="#6e768a" />
//     </span>
//   )
// }

const DashboardFilterFooter = props => {
  const { footerIconLeft, footerTitle, children } = props;
  return (
    <div className="DashboardFilterFooter">
      <Grid>
        <Grid.Row>
          <Grid.Column width={8} className="filterFooter_LeftColumn">
            <div className="MobileHeader_Left">
              <Header as="h3">
                {/* {footerIconLeft} */}
                {/* footerIconLeft={<ICFilterIcon pathcolor="#6e768a" />} */}
                <div>
                  <Modal
                    className="mobile_candidateFilter"
                    // size="fullscreen"
                    trigger={
                      <span>
                        <ICFilterIcon pathcolor="#6e768a" />
                        Filter
                      </span>
                    }
                    closeIcon
                    closeOnDimmerClick={false}>
                    <MobileFilter />
                  </Modal>
                </div>

                {/* {footerTitle} */}
              </Header>
            </div>
          </Grid.Column>
          <Grid.Column width={8} textAlign="right">
            {children}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
};

export default DashboardFilterFooter;
