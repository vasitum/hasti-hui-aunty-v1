import React from "react";
import { Header, Grid, Button } from "semantic-ui-react";
import IcCloseIcon from "../../../../assets/svg/IcCloseIcon";
import "./index.scss";

const ApplyHeader = () => (
  <div className="ApplyHeader">
    {/* mobile header */}
    <Grid>
      <Grid.Row className="mobile_Header">
        <Grid.Column width={8} className="topBottom_center">
          <Header as="h3">Filter </Header>
        </Grid.Column>
        <Grid.Column width={8} className="" textAlign="right">
          <Button className="apply_btn" basic>
            Apply
          </Button>

          {/* <Button inverted className="close_btn" color="#1f2532">
            <IcCloseIcon pathcolor="#6e768a" height="14" width="14" />
          </Button> */}
        </Grid.Column>
      </Grid.Row>
      <Grid.Row className="mobileHeader_clearBtn">
        <Grid.Column width={16}>
          <Button compact inverted className="allClear_btn">
            <IcCloseIcon pathcolor="#0b9ed0" height="10" width="10" />
          </Button>
          <span>Clear all applied filters</span>
        </Grid.Column>
      </Grid.Row>
    </Grid>
    {/* mobile header */}
  </div>
);

export default ApplyHeader;
