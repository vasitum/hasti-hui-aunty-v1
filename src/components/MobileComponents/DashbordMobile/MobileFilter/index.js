import React, { Component } from "react";

import { Dropdown, Input, Checkbox, Form, Grid } from "semantic-ui-react";
import ApplyHeader from "../ApplyHeader";

import "./index.scss";
import StatisticOptionGrid from "../SearchApplicant/StatisticOptionGrid";

const InterviewOptions = [
  {
    text: "All",
    value: "All"
  },
  {
    text: "Today",
    value: "Today"
  },
  {
    text: "Tomorrow",
    value: "Tomorrow"
  },
  {
    text: "This week",
    value: "This week"
  },
  {
    text: "Custom date",
    value: "Custom date"
  }
];

const SubmittedOptions = [
  {
    text: "Today",
    value: "Today"
  },
  {
    text: "Last 7 days",
    value: "Last 7 days"
  },
  {
    text: "Custom date",
    value: "Custom date"
  },
  {
    text: "Anytime",
    value: "Anytime"
  }
];

export default class MobileFilter extends Component {
  state = {};
  handleChange = (e, { value }) => this.setState({ value });

  render() {
    return (
      <div className="Main_Box_Filter">
        <ApplyHeader />
        <Grid>
          <Grid.Row className="Main_Form">
            <Grid.Column width={16}>
              <Form>
                <Form.Field>
                  <Checkbox
                    radio
                    label="CV available"
                    name="checkboxRadioGroup"
                    value="CV"
                    checked={this.state.value === "CV"}
                    onChange={this.handleChange}
                  />
                </Form.Field>
                <Form.Field>
                  <Checkbox
                    radio
                    label="Rejected"
                    name="checkboxRadioGroup"
                    value="Rejected"
                    checked={this.state.value === "Rejected"}
                    onChange={this.handleChange}
                  />
                </Form.Field>
                <Form.Field>
                  <label>Interview</label>
                  <Dropdown
                    selection
                    options={InterviewOptions}
                    defaultValue={InterviewOptions[0].value}
                  />
                </Form.Field>
                <Form.Field>
                  <label>Submitted</label>
                  <Dropdown
                    selection
                    options={SubmittedOptions}
                    defaultValue={SubmittedOptions[3].value}
                  />
                </Form.Field>
                <Form.Field>
                  <label />
                  <Input icon="search" placeholder="Search applicants" />
                </Form.Field>
              </Form>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}
