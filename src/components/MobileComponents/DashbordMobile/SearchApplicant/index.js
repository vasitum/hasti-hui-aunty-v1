import React from "react";
import "./index.scss";
import PropTypes from "prop-types";
import { Dropdown } from "semantic-ui-react";
import StatisticOptionGrid from "./StatisticOptionGrid";

import "./index.scss";

const CardOptions = [
  {
    text: "Basic screening",
    value: "Basic screening"
  },
  {
    text: "Interview",
    value: "Interview"
  },
  {
    text: "New applicants",
    value: "New applicants"
  },
  {
    text: "Pending applicants",
    value: "Pending applicants"
  }
];

const StatisticOption = [
  {
    text: <StatisticOptionGrid title="New applicants" count="12" />,
    value: "New applicants"
  },
  {
    text: <StatisticOptionGrid title="Pending" count="08" />,
    value: "Pending applicants"
  },
  {
    text: <StatisticOptionGrid title="Basic Screening" count="06" />,
    value: "Basic Screening"
  },
  {
    text: <StatisticOptionGrid title="Interview" count="14" />,
    value: "Interview"
  },
  {
    text: <StatisticOptionGrid title="Offere" count="12" />,
    value: "Offere"
  }
];

const SearchApplicant = props => {
  const { footerIconLeft, footerTitle, children } = props;
  return (
    <div className="SearchApplicant">
      <div className="mobile_allJobs">
        <Dropdown
          placeholder="All jobs"
          selection
          options={CardOptions}
          fluid
        />
      </div>
      <div className="mobile_allCandidates">
        <Dropdown placeholder="All" selection options={StatisticOption} fluid />
      </div>
    </div>
  );
};

export default SearchApplicant;
