import React from "react";

import { Grid } from "semantic-ui-react";
import PropTypes from "prop-types";

import "./index.scss";

const StatisticOptionGrid = props => {
  const { title, count } = props;
  return (
    <Grid>
      <Grid.Row columns={2} only="mobile">
        <Grid.Column>
          <span>{title}</span>
        </Grid.Column>
        <Grid.Column textAlign="right">
          <span>{count}</span>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

export default StatisticOptionGrid;
