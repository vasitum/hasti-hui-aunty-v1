import React, { Children } from "react";
import PropTypes from "prop-types";
import { Header, Grid } from "semantic-ui-react";

import "./index.scss";

const MobileFooter = props => {
  const {
    headerLeftIcon,
    headerTitle,
    isShowHeaderTitle,
    headerLeftAttachment,
    mobileFooterLeftColumn,
    mobileFooterRightColumn,
    mobileLeftColumn,
    children,
    ...resProps
  } = props;
  return (
    <div className="MobileHeader" {...resProps}>
      <Grid>
        <Grid.Row>
          {!mobileLeftColumn ? (
            <Grid.Column width={mobileFooterLeftColumn}>
              <div className="MobileHeader_Left">
                {headerLeftIcon}
                {headerLeftAttachment}
                {!isShowHeaderTitle ? (
                  <Header as="h3">{headerTitle}</Header>
                ) : null}
              </div>
            </Grid.Column>
          ) : null}

          <Grid.Column width={mobileFooterRightColumn} textAlign="right">
            {children}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
};

MobileFooter.propTypes = {
  mobileFooterLeftColumn: PropTypes.string,
  mobileFooterRightColumn: PropTypes.string
};

MobileFooter.defaultProps = {
  classNames: "MobileHeader",
  mobileFooterLeftColumn: "8",
  mobileFooterRightColumn: "8"
};

export default MobileFooter;
