import React from "react";

import { Tab, Menu, Button, Form, Input, Image } from "semantic-ui-react";

import IcHamburgerMenu from "../../../assets/svg/IcHamburgerMenu";
import IcChatbot from "../../../assets/svg/IcChatbot";
import SearchIcon from "../../../assets/svg/SearchIcon";
import IcFooterArrowIcon from "../../../assets/svg/IcFooterArrowIcon";

import { onInputChange } from "../../../actions/search";

// import IcDrawerMobile from "../../../assets/svg/IcDrawerMobile";

// import IcHome from "../../../assets/svg/IcHome";
// import IcMessage from "../../../assets/svg/IcMessage";

import NavigationDrawer from "../../NavigationDrawer";

import { withRouter, Link, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { withChatbotContext } from "../../../contexts/ChatbotContext";

import isLoggedOut from "../../../utils/env/isLoggedin";

import NotificationNavBar from "../../Navbar/NotificationNavBar";

import VasitumLogoMobile from "../../../assets/logo/vasitum_logoMobile.png";

import "./index.scss";

const SidebarUI = ({ isOpen, ...rest }) => {
  const classes = ["Sidebar", isOpen ? "is-open" : ""];

  return <div aria-hidden={!isOpen} className={classes.join(" ")} {...rest} />;
};

SidebarUI.Overlay = props => <div className="SidebarOverlay" {...props} />;

SidebarUI.Content = ({ width = "20rem", isRight = false, ...rest }) => {
  const classes = ["SidebarContent", isRight ? "is-right" : ""];
  const style = {
    width,
    height: "100%",
    top: 0,
    right: isRight ? `-${width}` : "auto",
    left: !isRight ? `-${width}` : "auto"
  };

  return <div className={classes.join(" ")} style={style} {...rest} />;
};

class Sidebar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: props.isOpen,
      searchText: this.props.searchValue || ""
    };

    this.openSidebar = this.openSidebar.bind(this);
  }

  openSidebar(isOpen = true) {
    this.setState({ isOpen });
  }

  render() {
    const { isOpen } = this.state;
    const { hasOverlay, isRight } = this.props;
    return (
      <SidebarUI isOpen={isOpen}>
        <Button
          className="mobileHeaderBtn"
          type="button"
          onClick={this.openSidebar}>
          <IcHamburgerMenu pathcolor="#6e768a" />
        </Button>

        <SidebarUI.Content isRight={isRight}>
          <NavigationDrawer />
        </SidebarUI.Content>

        {hasOverlay ? (
          <SidebarUI.Overlay onClick={() => this.openSidebar(false)} />
        ) : (
          false
        )}
      </SidebarUI>
    );
  }
}

class PostLoginMobileHeader extends React.Component {
  state = {
    isSearchEnabled: false,
    inputVal: ""
  };

  onInputChange = (e, { value }) => {
    this.setState({ inputVal: value });
  };

  onSearchBtnClick = e => {
    this.setState({ isSearchEnabled: true });
  };

  onSearchBackClick = e => {
    this.setState({ isSearchEnabled: false });
  };

  onSearchSubmit = e => {
    if (e.preventDefault) {
      e.preventDefault();
    }
    this.props.onSearchInputChange(this.state.inputVal);
    // redirect if we are not search page
    if (!this.props.location.pathname.startsWith("/search")) {
      this.props.history.push({
        pathname: "/search",
        search: `?query=${encodeURIComponent(this.state.inputVal)}`
      });
    } else {
      this.props.history.push({
        search: `?query=${encodeURIComponent(this.state.inputVal)}`
      });
    }
  };

  render() {
    const { isSearchEnabled, inputVal } = this.state;
    const { actions } = this.props;
    const loggedOut = isLoggedOut();

    return isSearchEnabled ? (
      <div className="PostLoginMobileHeader">
        <Form onSubmit={this.onSearchSubmit}>
          <div className="postLoginHeaderLogin_search">
            <div className="backArrow">
              <IcFooterArrowIcon
                onClick={this.onSearchBackClick}
                pathcolor="#6e768a"
              />
            </div>
            <div className="searchField">
              <Input
                onChange={this.onInputChange}
                value={decodeURIComponent(inputVal)}
                placeholder={"Search Jobs/ People/ Companies"}
              />
              <Button compact className="searchBtn">
                <SearchIcon pathcolor={inputVal ? "#1f2532" : "#eaeaee"} />
              </Button>
            </div>
          </div>
        </Form>
      </div>
    ) : (
      <div className="PostLoginMobileHeader">
        <div className="PostLoginMobileHeader_container">
          <div className="PostLoginMobileHeader_left headerBtn">
            <Button className="mobileHeaderBtn" as={Link} to={"/"}>
              {/* <IcChatbot pathcolor="#0b9ed0" pathcolorSecond="#0b9ed0" /> */}
              {/* <VasitumLogo /> */}
              <Image
                src={VasitumLogoMobile}
                verticalAlign="middle"
                width="40px"
              />
            </Button>
          </div>

          <div className="PostLoginMobileHeader_right headerBtn">
            <div className="searchBtn">
              <Button
                onClick={this.onSearchBtnClick}
                className="mobileHeaderBtn">
                <SearchIcon pathcolor="#6e768a" />
              </Button>
            </div>
            {!loggedOut ? (
              <div className="Notification_Container">
                <NotificationNavBar as={NavLink} to={"/notification"} />
                <Button className="mobileHeaderBtn">
                  <Sidebar hasOverlay />
                </Button>
              </div>
            ) : null}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    searchValue: state.search.currentValue
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onSearchInputChange: data => {
      dispatch(onInputChange(data));
    }
  };
};

// export default withRouter(PostLoginMobileHeader);
export default withChatbotContext(
  withRouter(
    connect(
      mapStateToProps,
      mapDispatchToProps
    )(PostLoginMobileHeader)
  )
);
