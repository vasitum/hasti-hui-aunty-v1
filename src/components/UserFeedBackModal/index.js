import React, { Component } from "react";
import { Modal, Icon, Form, Button, Image, Header } from "semantic-ui-react";
import "./index.scss";
import Good from "../../assets/svg/IcFeedbackGood";
import Avgrage from "../../assets/svg/IcFeedbackAvg";
import Great from "../../assets/svg/IcFeedbackGreat";
import Poor from "../../assets/svg/IcFeedbackPoor";
import BorderButton from "../Buttons/BorderButton";
import IcThankYou from "../../assets/svg/IcThankYou";
import userExpApi from "../../api/user/userExpFeedbackApi";

import AiVasiLogo from "../../assets/img/UserFeedBack/VasiImage.png";
import LaughingLogo from "../../assets/img/UserFeedBack/laughing.png";
import SmileLogo from "../../assets/img/UserFeedBack/smile.png";
import ConfusedLogo from "../../assets/img/UserFeedBack/confused.png";
import SadLogo from "../../assets/img/UserFeedBack/sad.png";


export default class UserFeedBackModal extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      iconActive: false,
      iconText: "",
      disabled: true,
      showThanku: false,
      value: "",
      modalFeedback: false
    };
  }

  handleFeedback = text => {
    this.setState({
      iconActive: true,
      disabled: false,
      iconText: text,
      value: ""
    });
  };

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  handleModal = () => {
    this.setState({
      modalFeedback: !this.state.modalFeedback,
      showThanku: false
    });
  };

  handleUserExpFeedback = async event => {
    event.preventDefault();
    const { value, iconText } = this.state;
    try {
      const res = await userExpApi(value, iconText);
      if (res.status === 200) {
        const data = await res.json();
        this.setState({ showThanku: true });
        console.log("thank you for feedback");
        this.props.handleGetFeedback();
        setTimeout(() => this.handleModal(), 2000);
      }
    } catch (error) {
      console.error(error);
    }
  };

  render() {
    const { iconActive, iconText, disabled, showThanku } = this.state;
    const { icon } = this.props;
    return (
      <React.Fragment>
        <Button
          circular
          icon={
            icon === "great" ? (
              // <Image src={LaughingLogo} />
              <Great pathcolor="#fff" width="25" height="25" />
            ) : icon === "good" ? (
              <Good pathcolor="#fff" width="25" height="25" />
            ) : icon === "average" ? (
              <Avgrage pathcolor="#fff" width="25" height="25" />
            ) : icon === "poor" ? (
              <Poor pathcolor="#fff" width="25" height="25" />
            ) : (
              "thumbs up outline"
            )
          }
          className="feedback-floating-button"
          onClick={this.handleModal}
        />
        <Modal
          open={this.state.modalFeedback}
          onClose={this.handleModal}
          className="user-feedback-modal-block"
          size="mini">
          <Modal.Content>
            <div className="cross-button">
              <Icon
                name="close"
                className="cross-icon"
                onClick={this.handleModal}
              />
            </div>
            {!showThanku ? (
              <div>
                <div className="modal-title">
                  <div className="modal-titleLogo">
                    <Image src={AiVasiLogo} verticalAlign={"middle"}/>
                  </div>
                  <div className="modal-bold-title">
                    We value your feedback!{" "}
                  </div>
                  Please rate your experience with us.
                </div>
                <div className="item-card-popup">
                  <div
                    className={`flex-button ${
                      iconActive && iconText === "great" ? "active" : ""
                    }`}
                    onClick={() => this.handleFeedback("great")}>
                    {/* <Great pathcolor="#acaeb5" width="50" height="50" /> */}
                    <Image src={LaughingLogo} />
                    <div className="share-font">Great</div>
                  </div>
                  <div
                    className="flex-button"
                    className={`flex-button ${
                      iconActive && iconText === "good" ? "active" : ""
                    }`}
                    onClick={() => this.handleFeedback("good")}>
                    <Image src={SmileLogo} />
                    {/* <Good pathcolor="#acaeb5" width="50" height="50" /> */}
                    <div className="share-font">Good</div>
                  </div>
                  <div
                    className="flex-button"
                    className={`flex-button ${
                      iconActive && iconText === "average" ? "active" : ""
                    }`}
                    onClick={() => this.handleFeedback("average")}>
                    <Image src={ConfusedLogo} />
                    {/* <Avgrage pathcolor="#acaeb5" width="50" height="50" /> */}
                    <div className="share-font">Average</div>
                  </div>
                  <div
                    className="flex-button"
                    className={`flex-button ${
                      iconActive && iconText === "poor" ? "active" : ""
                    }`}
                    onClick={() => this.handleFeedback("poor")}>
                    <Image src={SadLogo} />
                    {/* <Poor pathcolor="#acaeb5" width="50" height="50" /> */}
                    <div className="share-font">Poor</div>
                  </div>
                </div>
                <div className="remark-area">
                  <div className="remark-area-border">
                    {/* <label className="remark">Remarks:</label> */}
                    <Form.Input
                      fluid
                      value={this.state.value}
                      placeholder="Wish to leave suggestions"
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className="remark-button">
                    <BorderButton
                      btnText="Send"
                      className="btn-border"
                      disabled={disabled}
                      onClick={this.handleUserExpFeedback}
                    />
                  </div>
                </div>
              </div>
            ) : (
              <div className="text-center thank-img">
                <div>
                  {/* <img src={ThanksIcon} /> */}
                  <IcThankYou 
                    pathcolor="#ffffff"
                    pathcolor1="#ffffff"
                  />
                  <Header as="h3">Thank You!</Header>
                </div>
              </div>
            )}
          </Modal.Content>
        </Modal>
      </React.Fragment>
    );
  }
}
