import React from "react";
import { Popup } from "semantic-ui-react";
// import { Form, Input } from "semantic-ui-react";
// import PropTypes from "prop-types";

import IcInfoIcon from "../../../assets/svg/IcInfo";

import "./index.scss";

const InfoIconLabel = props => {
  // const {  } = props;
  return (
    <Popup
      trigger={
        <div className="InfoIconLabel">
          <IcInfoIcon pathcolor="#c8c8c8" />
        </div>
      }
      content={props.text}
    />
  );
};

// InfoIconLabel.propTypes = {
//   // inputtype: PropTypes.string
// };

// InfoIconLabel.defaultProps = {
//   // inputtype: "text"
// };

export default InfoIconLabel;
