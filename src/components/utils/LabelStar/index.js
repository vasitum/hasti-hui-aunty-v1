import React from "react";
import { Popup } from "semantic-ui-react";

import "./index.scss";

const LabelStar = str => {
  return (
    <div className="LabelStar">
      <span>{str}</span>
      <span style={{ color: "#0b9ed0" }}>*</span>
    </div>
  );
};

export default LabelStar;
