import React from "react";
import { Popup } from "semantic-ui-react";

import IcInfoIcon from "../../../assets/svg/IcInfo";
import IcCloseIcon from "../../../assets/svg/IcCloseIcon";

import "./index.scss";

const CloseIcon = props => {
  // const {  } = props;
  return (
    <div className="CloseIcon" onClick={props.onClick}>
      <span>
        <IcCloseIcon height="10" width="10" pathcolor="#c8c8c8" />
      </span>
    </div>
  );
};

// InfoIconLabel.propTypes = {
//   // inputtype: PropTypes.string
// };

// InfoIconLabel.defaultProps = {
//   // inputtype: "text"
// };

export default CloseIcon;
