import React from "react";

// import { Form, Input } from "semantic-ui-react";
// import PropTypes from "prop-types";

import "./index.scss";

import IcDownArrow from "../../../assets/svg/IcDownArrow";
import getPlaceholder from "../../Forms/FormFields/Placeholder";

const SkipNowCollaps = props => {
  const {} = props;

  return (
    <div className="SkipNowCollaps">
      <div className="skipNowTitle">
      
        {getPlaceholder("Details","Add Details")}
      </div>
      <div className="moreDetailTitle">{getPlaceholder("Skip","Skip For Now")}</div>
    </div>
  );
};

export default SkipNowCollaps;
