import React from "react";

// import { Form, Input } from "semantic-ui-react";
import PropTypes from "prop-types";

import "./index.scss";

import IcDownArrow from "../../../assets/svg/IcDownArrow";

const DownArrowCollaps = props => {
  const { ...resProps } = props;

  return (
    <div className="DownArrowCollaps"  { ...resProps}>
      <IcDownArrow pathcolor="#c8c8c8"/>
    </div>
  );
};

DownArrowCollaps.propTypes = {
  // inputtype: PropTypes.string
};

DownArrowCollaps.defaultProps = {
  // inputtype: "text"
};

export default DownArrowCollaps;
