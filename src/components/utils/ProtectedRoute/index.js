import React, { Component, Suspense } from "react";
import { Route, Redirect } from "react-router-dom";
import { withUserContextConsumer } from "../../../contexts/UserContext";
import { USER } from "../../../constants/api";

const RedirectHome = props => {
  return <Redirect to={"/"} />;
};

const RedirectCreateProfile = props => {
  return <Redirect to={"/create/profile"} />;
};

const RedirectEmailVerification = props => {
  return <Redirect to={"/emailVerification"} />;
};

export default function withProtectedRoute(WrappedComponent) {
  class E extends React.Component {
    state = {
      signOut: false,
      isVerified: false,
      hasProfile: true
    };

    static getDerivedStateFromProps(props) {
      const { user } = props;

      // console.log("User is", user);

      const ID = user.id;
      let signOut = false,
        hasProfile = user.isProfileCreated,
        isVerified = user.isVerified;

      if (!ID || ID == "undefined") {
        signOut = true;
      }

      return {
        signOut,
        hasProfile,
        isVerified
      };
    }

    render() {
      const { user, onChange, ...mProps } = this.props;
      const { signOut, hasProfile, isVerified } = this.state;

      if (signOut) {
        return <RedirectHome />;
      }

      if (!isVerified) {
        return <RedirectEmailVerification />;
      }

      if (!hasProfile) {
        return <RedirectCreateProfile />;
      }

      return <WrappedComponent {...mProps} />;
    }
  }

  return withUserContextConsumer(E);
}
