import React from "react";
import { Button } from "semantic-ui-react";

import IcFooterArrowIcon from "../../../assets/svg/IcFooterArrowIcon";



import "./index.scss";

const BackArrowBtnLeft = props => {
  
  const {...resProps} = props;

  return(
    <Button className="BackArrowBtnLeft" {...resProps}>
      <IcFooterArrowIcon pathcolor="#6e768a" />
    </Button>
  )
}

export default BackArrowBtnLeft