import React from "react";

import { Button } from "semantic-ui-react";
import PropTypes from "prop-types";

// import IcDownArrow from "../../../assets/svg/IcDownArrow";
// <IcDownArrow pathcolor="#99d9f9" />

import "./index.scss";

const MobileHeaderApplyBtn = props => {
  const { btnText, btnIcon, ...restProps } = props;
  return (
    <Button className="MobileHeaderApplyBtn" {...restProps}>
      {btnText}
      {btnIcon}
    </Button>
  );
};

// MobileHeaderApplyBtn.propTypes = {
//   placeholder: PropTypes.string
// };

// MobileHeaderApplyBtn.defaultProps = {
//   actioaBtnText: "Action Button"
// };

export default MobileHeaderApplyBtn;
