import React from "react";

import { Button } from "semantic-ui-react";
import PropTypes from "prop-types";

import "./index.scss";

const FlatDefaultBtn = props => {
  const { btntext, btnicon, classNames, ...restProps } = props;

  return (
    <span>
      <Button
        compact
        className={`FlatDefaultBtn ${classNames ? classNames : ""}`}
        {...restProps}>
        {btnicon} {btntext}
      </Button>
    </span>
  );
};

FlatDefaultBtn.propTypes = {
  btntext: PropTypes.string,
  classNames: PropTypes.string
};

FlatDefaultBtn.defaultProps = {
  classNames: "bgGhostWhite"
};

export default FlatDefaultBtn;
