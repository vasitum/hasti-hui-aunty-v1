import React from "react";
import { Button, Popup, List, Icon, Image } from "semantic-ui-react";
import IcLinkedinIcon from "../../../assets/svg/IcLinkedin";
import facebookIc from "../../../assets/img/facebook.png";
import google from "../../../assets/img/google.png";
import CopyIc from "../../../assets/img/copy.png";
import Twitter from "../../../assets/img/twitter.png";
import "./index.scss";

const ShareButton = ({ trigger, ...restProps }) => (
  <Popup trigger={trigger} flowing hoverable {...restProps}>
    <List className="Button_main" relaxed="6">
      <List.Item as="a" className="Sub_Button">
        <List.Icon>
          <Image src={facebookIc} />
        </List.Icon>
        <List.Content className="Button_content">Facebook</List.Content>
      </List.Item>

      <List.Item as="a" className="Sub_Button">
        <List.Icon>
          <Image src={Twitter} />
        </List.Icon>
        <List.Content className="Button_content">Twitter</List.Content>
      </List.Item>

      <List.Item as="a" className="Sub_Button">
        <List.Icon>
          <IcLinkedinIcon />
        </List.Icon>
        <List.Content className="Button_content">Linkedin</List.Content>
      </List.Item>

      <List.Item as="a" className="Sub_Button">
        <List.Icon>
          <Image src={CopyIc} />
        </List.Icon>
        <List.Content className="Button_content">Copy Link</List.Content>
      </List.Item>
    </List>
  </Popup>
);

export default ShareButton;
