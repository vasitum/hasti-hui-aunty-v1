import React from "react";

import { Form, Input, Button, Label, Icon } from "semantic-ui-react";
import PropTypes from "prop-types";

import IcUploadIcon from "../../../assets/svg/IcUpload";

import "./index.scss";

const RemoveBtn = props => {
  const {
    className,
    btnText,
    btnIcon,
    onFileUpload,
    accept,
    ...restProps
  } = props;
  return (
    <div className="FileuploadBtn" {...restProps}>
      <Label className={className} as="label" size="large">
        <span className="fileUplaod_icon">{btnIcon}</span>
        <span className="fileUplaod_text">{btnText}</span>
      </Label>
    </div>
  );
};

RemoveBtn.propTypes = {
  classNames: PropTypes.string,
  btnIcon: PropTypes.any
  // btnText: propTypes.string
};

RemoveBtn.defaultProps = {
  className: "fileuploadDefaultBtn"
};

export default RemoveBtn;
