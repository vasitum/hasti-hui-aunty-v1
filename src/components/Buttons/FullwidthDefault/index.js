import React from "react";
import { Button } from "semantic-ui-react";
import PropTypes from "prop-types";
import "./index.scss";

const FullwidthDefault = props => {
  
  const { btnText, ...resProps } = props

  return (
    <Button 
      className="FullwidthDefault"
      fluid
      {...resProps}
    >
      {btnText}
    </Button>
  )  
}

FullwidthDefault.propTypes = {
  btnText: PropTypes.string
};

// FullwidthDefault.defaultProps = {
//   classNames: "bgGhostWhite"
// };

export default FullwidthDefault;
