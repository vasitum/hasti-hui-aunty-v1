import React from 'react';

import { Form, Input, Button, Label, Icon } from "semantic-ui-react";
import PropTypes from 'prop-types';

import IcUploadIcon from '../../../assets/svg/IcUpload';

import './index.scss';

const FlatFileuploadBtn = props => {
  
  const { btnIcon, btnText, onChange, accept } = props;
  
  return (
    <div className="FlatFileuploadBtn">
      <Label width="4" as="label" size="large">
        <span className="fileUplaod_icon">
          {btnIcon}
        </span>
        <span className="fileUplaod_text">
          {btnText}
        </span>
        <input hidden type="file" onChange={onChange} accept={accept}/>
      </Label>
    </div>
  )
}

FlatFileuploadBtn.propTypes = {
  btnText: PropTypes.string
}

export default FlatFileuploadBtn;