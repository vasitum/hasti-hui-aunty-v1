import React from "react";

import { Form, Input, Button, Label, Icon } from "semantic-ui-react";
import PropTypes from "prop-types";

import IcUploadIcon from "../../../assets/svg/IcUpload";

// import "./index.scss";

const BannerFileBtn = props => {
  const {
    className,
    btnText,
    btnIcon,
    onFileUpload,
    accept,
    ...restProps
  } = props;
  return (
    <div className="FileuploadBtn" {...restProps}>
      <Label width="4" className={className} as="label" size="large">
        <span className="fileUplaod_icon">{btnIcon}</span>
        <span className="fileUplaod_text">{btnText}</span>
        {/* <input hidden type="file" onChange={onFileUpload} accept={accept} /> */}
      </Label>
    </div>
  );
};

BannerFileBtn.propTypes = {
  classNames: PropTypes.string,
  btnIcon: PropTypes.any
  // btnText: propTypes.string
};

BannerFileBtn.defaultProps = {
  className: "fileuploadDefaultBtn"
};

export default BannerFileBtn;