import React, { Fragment } from "react";

import ActionBtn from "../ActionBtn";
import FlatDefaultBtn from "../FlatDefaultBtn";

import SaveLogo from "../../../assets/svg/IcSave";

import IcProfileViewIcon from "../../../assets/svg/IcProfileView";

import "./index.scss";

const ModalFooterBtn = ({
  editJob,
  actioaBtnText,
  onCancelClick,
  onDraftclick,
  onPreviewClick,
  noExtra,
  submitBtnType,
  ...props
}) => {
  if (editJob) {
    return (
      <div className="ModalFooterBtn">
        <div className="previewJob_btnFooter">
          <FlatDefaultBtn
            btnicon={<IcProfileViewIcon height="12" pathcolor="#c8c8c8" />}
            btntext="Preview Job"
            type="button"
            // disabled={!this.state.canSubmit}
            onClick={onPreviewClick}
            // className="profileView_Icon"
            style={{
              paddingLeft: "2px",
              paddingRight: "15px"
            }}
          />
        </div>

        <FlatDefaultBtn
          // btnicon={<SaveLogo height="12" pathcolor="#c8c8c8" />}
          onClick={onDraftclick}
          // disabled={!this.state.canSubmit}
          btntext={"Save and Exit"}
          type="button"
        />

        <ActionBtn actioaBtnText={actioaBtnText} {...props} />
      </div>
    );
  }

  return (
    <div className="ModalFooterBtn">
      {noExtra ? (
        <React.Fragment>
          <div className="previewJob_btnFooter">
            <FlatDefaultBtn
              btnicon={<IcProfileViewIcon height="12" pathcolor="#c8c8c8" />}
              btntext="Preview Job"
              type="button"
              // disabled={!this.state.canSubmit}
              onClick={onPreviewClick}
              // className="profileView_Icon"
              style={{
                paddingLeft: "2px",
                paddingRight: "15px"
              }}
            />
          </div>

          <FlatDefaultBtn
            btnicon={<SaveLogo height="12" pathcolor="#c8c8c8" />}
            onClick={onDraftclick}
            // disabled={!this.state.canSubmit}
            btntext={"Save as Draft"}
            type="button"
          />
        </React.Fragment>
      ) : null}
      <FlatDefaultBtn
        type="button"
        onClick={onCancelClick}
        className="bgTranceparent"
        btntext="Cancel"
      />
      <ActionBtn
        actioaBtnText={actioaBtnText}
        type={submitBtnType ? submitBtnType : "button"}
        {...props}
      />
    </div>
  );
};

export default ModalFooterBtn;
