import React from "react";

import { Button } from "semantic-ui-react";
import PropTypes from "prop-types";

import IcDownArrow from "../../../assets/svg/IcDownArrow";

// <IcDownArrow pathcolor="#99d9f9" />

import "./index.scss";

const ActionBtn = props => {
  const { actioaBtnText, className, btnIcon, ...restProps } = props;

  return (
    <Button
      primary
      className={`ActionBtn ${className ? className : ""}`}
      {...restProps}>
      {actioaBtnText}
      {btnIcon}
    </Button>
  );
};

ActionBtn.propTypes = {
  placeholder: PropTypes.string
};

ActionBtn.defaultProps = {
  actioaBtnText: "Action Button"
};

export default ActionBtn;
