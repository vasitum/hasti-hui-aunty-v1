import React, { Component } from "react";
import { Button, Icon } from "semantic-ui-react";
import PropTypes from "prop-types";
import "./index.scss";

const BorderButton = props => {
  const { btnText, className, icon, ...resProps } = props;
  return (
    <span className="border-button-component">
      <Button className={className} {...resProps}>
        {btnText} {icon ? <Icon name={icon} /> : ""}
      </Button>
    </span>
  );
};

BorderButton.propTypes = {
  btnText: PropTypes.string
};

export default BorderButton;
