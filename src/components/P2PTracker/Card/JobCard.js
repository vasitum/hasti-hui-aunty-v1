import React from "react";
import { Card, Feed, Icon, Button } from "semantic-ui-react";
import { Link } from "react-router-dom";
import { CAND_STAGES } from "../../../constants/applicationStages";
export default class extends React.Component {
  render() {
    const { data, type } = this.props;

    return (
      <div className="P2PTracker_jobPage">
        <div className="P2PTracker_jobPageInner">
          <div className="P2P_jobPageInnerHeader">
            <p>Status</p>
            <Button>{CAND_STAGES[data.status]}</Button>
          </div>
          <Link to={`/job/candidate/${data.pId}/application/${data._id}`}>
            <div className="P2P_jobPageInnerBody">
              <Button as="a"> View application </Button>
            </div>
          </Link>
          {/* <Card.Content>
            <Feed>
              <Feed.Event>
                <Feed.Label image="https://react.semantic-ui.com/images/avatar/small/jenny.jpg" />
                <Feed.Content>
                  <Feed.Date content={"Core Java Developer"} />
                  <Feed.Summary>
                    <span className="sub-title">Wipro</span>
                  </Feed.Summary>
                  <Feed.Summary>
                    <span className="sub-title1">Noida, India</span>
                  </Feed.Summary>
                  <Feed.Summary>
                    <span className="sub-title2">Applied : </span>
                    <span className="sub-title3">Yesterday</span>
                  </Feed.Summary>
                </Feed.Content>
              </Feed.Event>
            </Feed>
          </Card.Content> */}
        </div>
        <div className="P2PTracker_jobPageFooter">
          <p>
            Applied: <span>Yesterday</span>
          </p>
        </div>
        {/* <div className="action-button">
          <div className="tracker-status-title">Application Status</div>
          <div className="tracker-status-action">SHORTLIST</div>
        </div> */}
        {/* <div className="view-job-application">View Job Application</div> */}
      </div>
    );
  }
}
