import React, { Component } from "react";
import { Card, Feed, Icon } from "semantic-ui-react";
import "./index.scss";
import P2PTrackerModal from "../JobModal";
import JobCard from "./JobCard";
import UserCard from "./UserCard";

export default class P2PTracker extends Component {
  constructor(props) {
    super(props);
    this.state = { modalOpen: false };
  }

  handleOpen = () => this.setState({ modalOpen: true });

  handleClose = () => this.setState({ modalOpen: false });

  render() {
    const { type, data, isShowp2pTitle, isShowViewAll,P2PTrackerUser  } = this.props;
    const val = data[0];

    if (!val) {
      return null;
    }

    return (
      <React.Fragment>
        <div className={!P2PTrackerUser ? "P2PTracker_user": null}>
          <div className="p2p-tracker">
            <div className="p2p-main">
              <div className="p2p-tracker-header">
                {!isShowp2pTitle ? (<div>
                  <span className="tracker-title">Mutual Activities</span>
                </div>) : null}
                
                {data.length > 1 ? (
                  <div>
                    {!isShowViewAll ? (<span
                      className="tracker-title-view-all"
                      onClick={this.handleOpen}>
                      view all <Icon name="angle right" />
                    </span>):null}
                    
                    <P2PTrackerModal
                      type={type}
                      data={data}
                      handleClose={this.handleClose}
                      handleOpen={this.state.modalOpen}
                    />
                  </div>
                ) : null}
              </div>
              {type === "user" ? <UserCard data={val} /> : <JobCard data={val} />}
            </div>
          </div>
        </div>

      </React.Fragment>
    );
  }
}
