import React from "react";
import { Card, Feed, Icon } from "semantic-ui-react";
import fromNow from "../../../utils/env/fromNow";
import { RECR_STAGES } from "../../../constants/applicationStages";
import CompanySvg from "../../../assets/svg/IcCompany";
import { Link } from "react-router-dom";
export default class extends React.Component {
  render() {
    const { data } = this.props;
    // console.log("p2pTracker", data);
    return (
      <div className="P2PTracker_userInner">
        <div className="P2P_userInnerHeader">
          <Card>
            <Card.Content>
              <Feed>
                <Feed.Event>
                  {!data.comExt ? (
                    <span className="label">
                      <CompanySvg
                        width="50"
                        height="50"
                        rectcolor="#f7f7fb"
                        pathcolor="#c8c8c8"
                      />
                    </span>
                  ) : (
                    <Feed.Label
                      image={`https://s3-us-west-2.amazonaws.com/img-mwf/company/${
                        data.comId
                      }/image.jpg`}
                    />
                  )}
                  <Feed.Content>
                    <Feed.Date content={data.jobName} />
                    <Feed.Summary>
                      <span className="sub-title">{data.comName}</span>
                    </Feed.Summary>
                    {/* TODO: Company Location */}
                    {/* <Feed.Summary>
                    <span className="sub-title1">{data.}</span>
                  </Feed.Summary> */}
                    <Feed.Summary>
                      <span className="sub-title2">Applied : </span>
                      <span className="sub-title3">
                        {fromNow(data.appliedDate)}
                      </span>
                    </Feed.Summary>
                  </Feed.Content>
                </Feed.Event>
              </Feed>
            </Card.Content>
          </Card>
          <div className="action-button">
            <div className="tracker-status-title">Status</div>
            <div className="tracker-status-action">
              {RECR_STAGES[data.status]}
            </div>
          </div>
        </div>
        <Link to={`/job/${data.pId}/application/${data._id}`}>
          <div className="P2P_userInnerBody">
            <div className="view-job-application">View Job Application</div>
          </div>
        </Link>
      </div>
    );
  }
}
