import React, { Component } from "react";
import { Button } from "semantic-ui-react";
import P2PTrackerIcon from "../../../assets/svg/p2pTracker";
import P2PTrackerModal from "../JobModal";
import "./index.scss";

export default class P2PIcon extends Component {
  constructor(props) {
    super(props);
    this.state = { modalOpen: false };
  }

  handleOpen = () => this.setState({ modalOpen: true });

  handleClose = () => this.setState({ modalOpen: false });

  render() {
    const { data } = this.props;

    return (
      <React.Fragment>
        <div className="p2p-tracker-icon-block">
          <Button
            icon
            className="p2p-tracker-button-icon"
            onClick={this.handleOpen}>
            <P2PTrackerIcon
              width="22"
              height="18"
              viewBox="0 0 22 18"
              pathcolor="#acaeb5"
            />
          </Button>
          <P2PTrackerModal
            data={data}
            handleClose={this.handleClose}
            handleOpen={this.state.modalOpen}
          />
        </div>
      </React.Fragment>
    );
  }
}
