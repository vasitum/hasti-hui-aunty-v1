import React, { Component } from "react";
import { Button, Responsive } from "semantic-ui-react";
import P2PTrackerIcon from "../../../assets/svg/p2pTracker";
import P2PTrackerModal from "../JobModal";
import IcFooterArrowIcon from "../../../assets/svg/IcFooterArrowIcon";
import MobileFooter from "../../MobileComponents/MobileFooter";

import "./index.scss";

export default class FooterP2P extends Component {
  constructor(props) {
    super(props);
    this.state = { modalOpen: false };
  }

  handleOpen = () => this.setState({ modalOpen: true });
  handleClose = () => this.setState({ modalOpen: false });

  render() {
    const { data, type } = this.props;
    return (
      <Responsive maxWidth={1024}>
        <div className="p2p-tracker-icon-block FooterP2P_mobile">
          <MobileFooter
            headerLeftIcon={
              <Button
                onClick={this.handleOpen}
                className="p2p-tracker-button-icon">
                <div className="p2p_iconBlockLeft">
                  <P2PTrackerIcon
                    width="22"
                    height="15"
                    viewBox="0 0 22 18"
                    pathcolor="#acaeb5"
                  />
                  Mutual Activities
                </div>
                <div className="p2p_iconBlockRight">
                  <IcFooterArrowIcon pathcolor="#acaeb5" />
                </div>
              </Button>
            }
            mobileFooterLeftColumn="16"
            class="isMobileFooter_fixe"
          />
          <P2PTrackerModal
            data={data}
            handleClose={this.handleClose}
            handleOpen={this.state.modalOpen}
          />
        </div>
      </Responsive>
    );
  }
}
