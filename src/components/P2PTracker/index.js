import React, { Component } from "react";
import getAllApplications from "../../api/messaging/getAllApplications";
import getApplicationByJobId from "../../api/jobs/getApplicationByJobAndUser";
import Card from "./Card";
import Icon from "./Icon";
import FooterP2P from "./FooterP2P";

export default class P2PTrackerMain extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    };
  }

  parseApplications() {}

  async componentDidMount() {
    const { type, userId, reqId } = this.props;

    if (type === "job") {
      try {
        const res = await getApplicationByJobId(reqId, userId);
        if (res.status === 200) {
          const data = await res.json();
          this.setState({
            data: [data]
          });
        }
      } catch (error) {
        console.error(error);
      }

      return;
    }

    try {
      const res = await getAllApplications(userId, reqId);
      // console.log(res);
      if (res.status === 200) {
        const data = await res.json();
        this.setState({
          data: data[reqId] ? data[reqId] : []
        });
      }
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    const { cardType } = this.props;
    const { data } = this.state;

    // draw nothing if nothing is the only constant
    if (data.length === 0) {
      return null;
    }

    switch (cardType) {
      case "icon":
        return <Icon data={data} type={cardType} />;

      case "FooterP2P":
        return <FooterP2P data={data} type={cardType} />;

      case "user":
        return <Card type={cardType} data={data} />;

      case "job":
        return (
          <Card type={cardType} data={data} isShowp2pTitle P2PTrackerUser />
        );

      default:
        return <Card type={"user"} data={data} />;
    }
  }
}
