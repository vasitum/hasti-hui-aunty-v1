import React, { Component } from "react";
import {
  Button,
  Header,
  Icon,
  Image,
  Modal,
  Card,
  Feed
} from "semantic-ui-react";
import fromNow from "../../../utils/env/fromNow";
import CompanySvg from "../../../assets/svg/IcCompany";
import { Link } from "react-router-dom";

import "./index.scss";
export default class P2PTrackerModal extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { data, type } = this.props;

    if (!data) {
      return null;
    }

    return (
      <React.Fragment>
        <div>
          <Modal
            open={this.props.handleOpen}
            onClose={this.props.handleClose}
            className="p2p-tracker-modal"
            closeIcon
            size="tiny">
            <Modal.Header className="tracker-title">
              Mutual Activities
            </Modal.Header>
            <Modal.Content
              scrolling
              className="p2p-tracker-modal-content"
              style={{ padding: "0 !important" }}>
              <Card>
                {data.map((val, idx) => {
                  return (
                    <Card.Content key={idx} className="p2p_modalCard">
                      <Feed>
                        <Feed.Event>
                          {/* <Feed.Label image="https://react.semantic-ui.com/images/avatar/small/jenny.jpg" /> */}
                          {!val.comExt ? (
                            <span className="label">
                              <CompanySvg
                                width="65"
                                height="65"
                                rectcolor="#f7f7fb"
                                pathcolor="#c8c8c8"
                              />
                            </span>
                          ) : (
                            <Feed.Label
                              image={`https://s3-us-west-2.amazonaws.com/img-mwf/company/${
                                val.comId
                              }/image.jpg`}
                            />
                          )}
                          <Feed.Content>
                            <div class="p2p-tracker-modal-sub-header">
                              <div>
                                <Feed.Date content={val.jobName} />
                                <span className="sub-title">
                                  {val.comName}
                                </span>{" "}
                              </div>
                              <div>
                                <span className="sub-title2">Applied : </span>
                                <span className="sub-title3">
                                  {fromNow(val.appliedDate)}
                                </span>
                              </div>
                            </div>

                            {/* <Feed.Summary>
                              <span className="sub-title">{val.comName}</span>{" "}
                              <span className="sub-title1">| Noida, India</span>
                            </Feed.Summary> */}
                            <Feed.Summary>
                              <div className="modal-action-button">
                                <div className="tracker-status">
                                  <span className="tracker-status-title">
                                    Status
                                  </span>
                                  <span className="tracker-status-action">
                                    {val.status}
                                  </span>
                                </div>
                                <Link
                                  to={
                                    type === "job"
                                      ? `/job/candidate/${
                                          val.pId
                                        }/application/${val._id}`
                                      : `/job/${val.pId}/application/${val._id}`
                                  }>
                                  <div className="tracker-title-view-all">
                                    <span>
                                      View job application{" "}
                                      <Icon name="angle right" />
                                    </span>
                                  </div>
                                </Link>
                              </div>
                            </Feed.Summary>
                          </Feed.Content>
                        </Feed.Event>
                      </Feed>
                    </Card.Content>
                  );
                })}
              </Card>
            </Modal.Content>
          </Modal>
        </div>
      </React.Fragment>
    );
  }
}
