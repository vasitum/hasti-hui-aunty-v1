import React from "react";
import AccordionCardContainer from "../../Cards/AccordionCardContainer";
import ManageTemplateCard from "../ManageTemplateCard";
import "./index.scss";
const ManageTemplateCardContainer = props => {
  const {
    hit,
    isShowCardApplyContainer,
    isShowStausList,
    isShowMobileFooterbottom,
    myJobCardsWidthLeft,
    noClick,
    isCardHeaderShow,
    ...resProps
  } = props;
  return (
    <div>
      <AccordionCardContainer
        noClick={noClick}
        cardHeader={<ManageTemplateCard isCardHeaderShow={isCardHeaderShow} />}>
        {/* <div className="Recommended_jobBody">
              <div className="Recommended_jobBodyContainer">
                <div className="Recommended_jobBodyLeft" />
                <div className="Recommended_jobBodyright">
                  <div>
                    {hit.desc ? (
                      <InfoSection
                        headerSize="medium"
                        color="blue"
                        headerText="Job Description">
                        <QuillText readOnly value={hit.desc} />
                      </InfoSection>
                    ) : null}
                  </div>
                  <div>
                    {hit.skills && hit.skills.length > 0 ? (
                      <SkillComponent val={hit} />
                    ) : null}
                  </div>

                  <div className="Recommended_jobBodyFooter">
                    <FlatDefaultBtn btntext="View complete Job" />
                  </div>
                </div>
              </div>
            </div> */}
        <div className="AccordianCollapseWhite">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis
          dignissimos hic dolorem fugit animi harum cum odit ducimus, sequi
          temporibus, doloribus, sit recusandae soluta natus voluptate adipisci.
          Accusamus, reprehenderit veniam!
        </div>
      </AccordionCardContainer>
    </div>
  );
};

export default ManageTemplateCardContainer;
