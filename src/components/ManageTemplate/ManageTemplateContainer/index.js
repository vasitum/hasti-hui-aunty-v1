import React from "react";
import "./index.scss";
import {
  Container,
  Header,
  Grid,
  Card,
  Modal,
  Button,
  Responsive
} from "semantic-ui-react";
import PageBanner from "../../Banners/PageBanner";
// import ManageTemplateCardContainer from "../ManageTemplateCardContainer";
// import CreateNewTemplate from "../CreateNewTemplate";

import ICCreateNewTemplate from "../../../assets/svg/ICCreateNewTemplate.svg";
import IcModifyTemplate from "../../../assets/svg/IcModifyTemplate.svg";
import IcNotifyTemplate from "../../../assets/svg/IcNotifyTemplate.svg";
import manageCompany from "../../../assets/banners/manageCompany.jpg";
import ApplyHeader from "../ManageTemplateCard/ApplyHeader";
import AccordionCardContainer from "../../Cards/AccordionCardContainer";
import ManageTemplateCard from "../ManageTemplateCard";
// import IcAttachment from "../../../assets/svg/IcAttachment";
import IcMessage from "../../../assets/svg/IcMessage";
// import templateMessaging from "../../../assets/svg/ic_template_messaging.svg";
// import templateApplication from "../../../assets/svg/ic_tempate_application.svg";
// import IcPush from "../../../assets/svg/IcPlus";
import IcTempateApplication from "../../../assets/svg/IcTempateApplication";
import DownArrowCollaps from "../../utils/DownArrowCollaps";
import ManageNewTemplate from "../ManageNewTemplate";
import getAllTemplates from "../../../api/messaging/getAllTemplates";
import deleteTemplate from "../../../api/messaging/deleteTemplateById";
import QuillText from "../../CardElements/QuillText";

import { MANAGE_TEMPLATE } from "../../../strings";

import NoFoundMessageDashboard from "../../Dashboard/Common/NoFoundMessageDashboard";

// FOR DESKTOP
class ManageTemplateContainer extends React.Component {
  constructor(props) {
    super(props);
    this.onDeleteClick = this.onDeleteClick.bind(this);
  }

  state = {
    filter: "ALL",
    templates: [],
    filterTemplates: [],
    modalStatus: false
  };

  onModalOpen = () => {
    this.setState({
      modalStatus: true
    });
  };

  onModalClose = () => {
    this.setState({
      modalStatus: false
    });
  };

  onFilterAll = () => {
    this.setState({ filter: "ALL", filterTemplates: this.state.templates });
  };

  onFilterMessage = () => {
    this.setState({ filter: "message" });

    const temp = this.state.templates.filter(
      template => template.tempType === "messageTemp"
    );

    this.setState({
      filterTemplates: temp
    });
  };

  onFilterTemplates = () => {
    this.setState({ filter: "application" });

    const temp = this.state.templates.filter(
      template => template.tempType === "applicationTemp"
    );

    this.setState({
      filterTemplates: temp
    });
  };

  async onDeleteClick(id) {
    try {
      const res = await deleteTemplate(id);
      if (res.status === 200) {
        this.componentDidMount();
      }
    } catch (error) {
      console.error(error);
    }
  }

  async componentDidMount() {
    try {
      const res = await getAllTemplates();

      if (res.status === 200) {
        const data = await res.json();
        // console.log("check date", data);
        this.setState({
          templates: data,
          filterTemplates: data
        });
        // console.log("This is template", data);

        return;
      }

      if (res.status === 404) {
        this.setState({
          templates: [],
          filterTemplates: []
        });
        return;
      }

      // console.log("check date", this.state.filterTemplates);
    } catch (error) {
      console.error(error);
    }
  }

  onCreate = e => {
    this.componentDidMount();
  };

  render() {
    const {
      hit,
      isShowCardApplyContainer,
      isShowStausList,
      isShowMobileFooterbottom,
      myJobCardsWidthLeft,
      noClick,
      isCardHeaderShow,
      isShowApplyHeader,
      isShowMobileFooter,
      isShowCards,
      children,
      isShowCreatedTime,
      ...resProps
    } = this.props;

    // const { CARD_1, } = MANAGE_TEMPLATE

    const ActiveClass = "ALL";

    return (
      <div className="ManageTemplate manageTemp_container">
        <PageBanner compHack size="medium" image={manageCompany}>
          <Container>
            <div className="banner_header">
              <Header as="h1">{MANAGE_TEMPLATE.HEADER_TITLE}</Header>
            </div>
          </Container>
        </PageBanner>
        <Container className="ManageTemplateContainer_container">
          {!isShowCards ? (
            <div className="manageTemplate_header">
              <Grid>
                <Grid.Row>
                  <Grid.Column mobile={16} computer={5}>
                    <Card fluid className="manageTemplate_card">
                      <Card.Content textAlign="center">
                        <img src={ICCreateNewTemplate} height="62px" />
                        <br />
                        <br />
                        <Card.Header>
                          {MANAGE_TEMPLATE.CARD_1.HEADER_TITLE}
                        </Card.Header>
                        <Card.Description>
                          {MANAGE_TEMPLATE.CARD_1.DESC}
                        </Card.Description>
                      </Card.Content>
                    </Card>
                  </Grid.Column>

                  <Grid.Column mobile={16} computer={5}>
                    <Card fluid className="manageTemplate_card">
                      <Card.Content textAlign="center">
                        <img src={IcModifyTemplate} height="62px" />
                        <br />
                        <br />
                        <Card.Header>
                          {MANAGE_TEMPLATE.CARD_2.HEADER_TITLE}
                        </Card.Header>
                        <Card.Description>
                          {MANAGE_TEMPLATE.CARD_2.DESC}
                        </Card.Description>
                      </Card.Content>
                    </Card>
                  </Grid.Column>

                  <Grid.Column mobile={16} computer={5}>
                    <Card fluid className="manageTemplate_card">
                      <Card.Content textAlign="center">
                        <img src={IcNotifyTemplate} height="62px" />
                        <br />
                        <br />
                        <Card.Header>
                          {MANAGE_TEMPLATE.CARD_3.HEADER_TITLE}
                        </Card.Header>
                        <Card.Description>
                          {MANAGE_TEMPLATE.CARD_3.DESC}
                        </Card.Description>
                      </Card.Content>
                    </Card>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </div>
          ) : null}
          {this.state.templates.length === 0 ? (
            <div>
              <NoFoundMessageDashboard
                noFountTitle="No data found"
                noFountSubTitle=""
              />
              <div className="Nodata_foundBtnTemp">
                <Modal
                  open={this.state.modalStatus}
                  onClose={this.onModalClose}
                  className="manageTemModal_form"
                  trigger={
                    <Button
                      onClick={this.onModalOpen}
                      primary
                      className="header_btn">
                      {/* <IcPush pathcolor="#ceeefe" /> */}
                      <span> Create Template</span>
                    </Button>
                  }
                  closeIcon
                  closeOnDimmerClick={false}>
                  <ManageNewTemplate onCancel={this.onModalClose} />
                </Modal>
              </div>
            </div>
          ) : (
            <div className="ManageTemplateBody">
              <div className="header_title">
                <Grid>
                  <Grid.Row>
                    <Grid.Column computer={8} className="title gridColumnLeft">
                      <div className="large screen only computer only">
                        <Header as="h2">Your Templates</Header>{" "}
                      </div>
                    </Grid.Column>
                    <Grid.Column
                      computer={8}
                      className="gridColumnRight"
                      textAlign="right">
                      <Modal
                        open={this.state.modalStatus}
                        onClose={this.onModalClose}
                        className="manageTemModal_form"
                        trigger={
                          <Button
                            onClick={this.onModalOpen}
                            primary
                            className="header_btn">
                            {/* <IcPush pathcolor="#ceeefe" /> */}
                            Create Template
                          </Button>
                        }
                        closeIcon
                        closeOnDimmerClick={false}>
                        <ManageNewTemplate onCancel={this.onModalClose} />
                      </Modal>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </div>

              <div className="ManageTemplateContainerTabView">
                <div
                  className={
                    this.state.filter === "ALL" ? "TabViewActiveClassAll" : null
                  }
                  onClick={this.onFilterAll}>
                  All
                </div>

                <div
                  className={
                    this.state.filter === "message"
                      ? "TabViewActiveClassMessage"
                      : null
                  }
                  onClick={this.onFilterMessage}>
                  {/* <img src={templateMessaging} height="14" width="14" />  */}
                  <IcMessage pathcolor="#acaeb5" height="14" width="14" />
                  Message
                </div>

                <div
                  className={
                    this.state.filter === "application"
                      ? "TabViewActiveClassTemp"
                      : null
                  }
                  onClick={this.onFilterTemplates}>
                  <IcTempateApplication
                    height="14" 
                    width="12" 
                    pathcolor1="#acaeb5"
                    pathcolor2="#ffffff"
                  />
                  {/* <img src={templateApplication} height="14" width="14" />{" "} */}
                  Application
                </div>
              </div>
              <Responsive minWidth={1025}>
                {this.state.filterTemplates.length === 0 ? (
                  <NoFoundMessageDashboard
                    noFountTitle="No data found"
                    noFountSubTitle={`Here you will see your ${
                      this.state.filter
                    } templates which you have created`}
                  />
                ) : null}
              </Responsive>

              {this.state.filterTemplates.map(template => (
                <div
                  className="ManageTemplateBody_container"
                  style={{ marginTop: 20 }}>
                  <AccordionCardContainer
                    noClick={noClick}
                    cardHeader={
                      <ManageTemplateCard
                        templateName={template.tempName}
                        templateType={template.tempType}
                        userMedia={template.userMedia}
                        datePosted={template.cTime}
                        isMobileFooter={isShowMobileFooter}
                        isShowCreatedTime>
                        {!isShowApplyHeader ? (
                          <ApplyHeader
                            onDelete={this.onDeleteClick}
                            templateData={template}
                            createDate={template.cTime}
                          />
                        ) : (
                          <div className="ManageApplyHeaderMobile">
                            <DownArrowCollaps />
                          </div>
                        )}
                      </ManageTemplateCard>
                    }>
                    <div className="ManageTemplateCard_body AccordionCardContainer_class">
                      {/* {template.text} */}
                      <div className="manageCard_bodyleft" />
                      <div className="manageCard_bodyRight">
                        <QuillText
                          readOnly
                          noReadMore
                          placeholder={""}
                          readMoreLength={300}
                          value={template.text}
                        />
                      </div>
                    </div>
                  </AccordionCardContainer>
                  <div>
                    {/* {React.Children.map((children, child) => {
                      React.cloneElement(child);
                    })} */}
                    {React.isValidElement(this.props.children)
                      ? React.cloneElement(this.props.children, {
                          templateData: template,
                          onDelete: this.onDeleteClick
                        })
                      : null}
                  </div>
                </div>
              ))}
            </div>
          )}
        </Container>
      </div>
    );
  }
}

export default ManageTemplateContainer;
