import React from "react";

import ManageTemplateContainer from "../ManageTemplateContainer";

import "./index.scss";
import TemplateFooter from "../ManageTemplateMobile/TemplateFooter";
import DownArrowCollaps from "../../utils/DownArrowCollaps";

// FOR MOBILE
export default class ManageTemplateMobile extends React.Component {
  render() {
    return (
      <div className="ManageTemplateMobile">
        <div className="TemplateMobile_header" />
        <div className="TemplateMobile_body">
          <ManageTemplateContainer
            isShowApplyHeader
            isShowCards
            isShowCreatedTime>
            <TemplateFooter />
          </ManageTemplateContainer>
        </div>
      </div>
    );
  }
}
