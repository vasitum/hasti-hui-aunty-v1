import React from "react";

import { Modal, Button } from "semantic-ui-react";
import "./index.scss";
import FlatDefaultBtn from "../../../Buttons/FlatDefaultBtn";
import ManageNewTemplate from "../../ManageNewTemplate";
import ActionBtn from "../../../Buttons/ActionBtn";
class TemplateFooter extends React.Component {
  state = {
    modalStatus: false
  };

  onModalOpen = () => {
    this.setState({
      modalStatus: true
    });
  };

  onModalClose = () => {
    this.setState({
      modalStatus: false
    });
  };

  render() {
    const { onDelete, templateData } = this.props;

    return (
      <div className="ManageTemplateFooter">
        <div>
          <FlatDefaultBtn
            btntext="Delete"
            onClick={e => onDelete(templateData._id)}
            className="bgTranceparent"
          />
        </div>
        <div>
          <Modal
            open={this.state.modalStatus}
            onClose={this.onModalClose}
            className="manageTemModal_form"
            trigger={
              <ActionBtn actioaBtnText="Edit" onClick={this.onModalOpen} />

              // className="ManageTemplateCardDeleteBtn">
            }
            closeIcon
            closeOnDimmerClick={true}>
            <ManageNewTemplate
              templateData={templateData}
              onCancel={this.onModalClose}
            />
          </Modal>
        </div>
      </div>
    );
  }
}

export default TemplateFooter;
