import React from "react";
import { Grid, Image, Header, List, Button, Modal } from "semantic-ui-react";
import DownArrowCollaps from '../../utils/DownArrowCollaps';
import ManageTemplateEdit from '../ManageTemplateEdit';
import IcDotIcon from '../../../assets/svg/IcDotIcon';
import templateApplication from '../../../assets/svg/ic_tempate_application.svg';
import templateMessaging from '../../../assets/svg/ic_template_messaging.svg';

import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import "./index.scss";
import IcAttachment from "../../../assets/svg/IcAttachment";
import QuillText from "../../CardElements/QuillText";

import fromNow from "../../../utils/env/fromNow";


class ManageTemplateCard extends React.Component {
  render() {
    const {
      template,
      myJobCardsWidthLeft,
      myJobCardsWidthRight,
      isCardHeaderShow,
      children,
      isShowCreatedTime,
      templateName,
      datePosted,
      userMedia,
      templateType,
      templateText,
      // hit,
      ...resProps,
    } = this.props;

    // if (!hit) {
    //   return null;
    // }

    // console.log('This is template type', templateType)

    return (
      <div className="ManageTemplateCard" {...resProps}>
        <div className="">
          <div>
            <Grid>
              <Grid.Row>

                <Grid.Column
                  width={myJobCardsWidthLeft}
                  className="MyJobCards_leftSide"
                >
                  <div className="interviewInvite">
                    <div className="manageTem_cardLogoLeft">
                      {templateType === 'messageTemp' ? <img src={templateMessaging} height="16" width="16" /> : <img src={templateApplication} height="16" width="16" />}
                    </div>
                    <div className="manageTem_detailRight">
                      <Header as="h3">
                        {templateName}
                      </Header>
                      
                      {/* Computer only */}
                      {
                        userMedia === null ? null : <span className="large screen only computer only" >
                          <IcDotIcon width="8" height="8" /> <span className="attachmentNumbermanageTem" >1</span>
                          <IcAttachment pathColor="#acaeb5" width="9" height="9" />
                        </span>
                      }

                      {/* Mobile only */}
                      <p className="ForwardedCandidate" >
                        <span className="mobile only dotMarginFixOfDot">
                          <IcDotIcon width="8" height="8" /> <span className="attachmentNumbermanageTem" >1</span>
                          <IcAttachment pathColor="#acaeb5" width="9" height="9" /></span>
                        <div className="createdInManageTemplateForMobile mobile only grid">
                          <span> Updated:</span> {fromNow(datePosted)}
                        </div>
                      </p>

                    
                      {/* <QuillText placeholder={""} readOnly noReadMore readMoreLength={100} value={templateText} /> */}
                    </div>
                  </div>

                </Grid.Column>

                <Grid.Column
                  width={myJobCardsWidthRight}
                  className="MyJobCards_rightSide"
                >
                  {children}
                </Grid.Column>

              </Grid.Row>

            </Grid>
          </div>



        </div>

      </div>


    );
  }
}

ManageTemplateCard.propTypes = {
  myJobCardsWidthLeft: PropTypes.string,
  myJobCardsWidthRight: PropTypes.string,
  // hit: PropTypes.string
};

ManageTemplateCard.defaultProps = {
  myJobCardsWidthLeft: "11",
  myJobCardsWidthRight: "5",
  // hit: {
  //   title: "UX UI Designer",
  //   comName: "Wipro",
  //   locCity: "Noida",
  //   locCountry: "India",
  //   skills: [
  //     "html",
  //     "css",
  //     "javascript"
  //   ]
  // }
};


export default ManageTemplateCard;