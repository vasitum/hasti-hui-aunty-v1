import React from "react";
import { Button, Modal } from "semantic-ui-react";
import DownArrowCollaps from "../../../utils/DownArrowCollaps";
import ManageTemplateEdit from "../../ManageTemplateEdit";
import FlatDefaultBtn from "../../../Buttons/FlatDefaultBtn";
import ManageNewTemplate from "../../ManageNewTemplate";
import fromNow from "../../../../utils/env/fromNow";

class ApplyHeader extends React.Component {
  state = {
    modalOpen: false
  };

  onModalOpen = () => {
    this.setState({
      modalOpen: true
    });
  };

  onModalClose = () => {
    this.setState({
      modalOpen: false
    });
  };

  render() {
    const { templateData, createDate, onDelete } = this.props;
    return (
      <React.Fragment>
        <div
          className="ManageTemplateCard_ApplyHeader"
          style={{ width: "100%" }}>
          <div className="ApplyHeader_container">
            <span>
              Created:{" "}
              <span className="todayManageTemplateCard">
                {" "}
                {fromNow(createDate)}
              </span>
            </span>

            <span className="arrowUp">
              <DownArrowCollaps />
            </span>
          </div>
          <br />
          <div style={{ float: "right" }}>
            <FlatDefaultBtn
              onClick={e => onDelete(templateData._id)}
              btntext="Delete"
              className="bgTranceparent"
            />
            <Modal
              open={this.state.modalOpen}
              onClose={this.onModalClose}
              className="manageTemModal_form"
              trigger={
                <Button onClick={this.onModalOpen} className="manaTem_editBtn">
                  Edit
                </Button>
              }
              closeIcon
              closeOnDimmerClick={false}>
              <ManageNewTemplate
                onCancel={this.onModalClose}
                templateData={templateData}
              />
            </Modal>
          </div>
        </div>
        {/* <div>
        Created:
        <span className="todayManageTemplateCard">
          Today{" "}
          <span style={{ float: "right" }}>
            <DownArrowCollaps />
          </span>
        </span>
      </div>
      <div>
        <FlatDefaultBtn btntext="Delete" className="bgTranceparent" />
        <Modal
          className="modal_form"
          trigger={
            <Button className="ManageTemplateCardDeleteBtn">Edit</Button>
          }
          closeIcon
          closeOnDimmerClick={true}>
          <ManageTemplateEdit />
        </Modal>
      </div> */}
      </React.Fragment>
    );
  }
}

export default ApplyHeader;
