import React from "react";
import { Container, Dropdown, Grid, TextArea, Header } from "semantic-ui-react";

import { Form } from "formsy-semantic-ui-react";

import ActionBtn from "../../Buttons/ActionBtn";
import FlatDefaultBtn from "../../Buttons/FlatDefaultBtn";
import FileuploadBtn from "../../Buttons/FileuploadBtn";
import IcAttachment from "../../../assets/svg/IcAttachment";
import TemplatesHeader from "../../Messaging/Templates/TemplatesHeader";
import InputField from "../../Forms/FormFields/InputField";
import Middlecontainer from "../../Messaging/Templates/TemplatemiddleContainer";

// import getAllUserTemplate from "../../../../api/messaging/getAllUserTemplate";

// import { USER } from "../../../../constants/api";
import { toast } from "react-toastify";
import QuillText from "../../CardElements/QuillText";

import "./index.scss";

class CreateNewTemplate extends React.Component {
  render() {
    return (
      <div className="tempMainDiv">
        <Form>
          <Container>
            <div className="tempHeader">
              <Grid>
                <Grid.Row>
                  <Grid.Column computer={4} mobile={16}>
                    <div className="title">
                      <div className="MarginAtBottomIntextLabel">
                        <Header as="h3">Enter Template Name</Header>
                      </div>
                    </div>
                  </Grid.Column>
                  <Grid.Column computer={12} mobile={16}>
                    <InputField
                      name="newTemp"
                      placeholder="Enter template name"
                    />
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </div>
            <br />
            <div
              style={{ paddingRight: 10, paddingLeft: 20, paddingBottom: 10 }}>
              <Grid>
                <Grid.Row>
                  <Grid.Column computer={4} mobile={16}>
                    <div className="MarginAtBottomIntextLabel">
                      <Header as="h3">Add Text</Header>
                    </div>
                  </Grid.Column>
                  <Grid.Column computer={12} mobile={16}>
                    <QuillText value="Add text" />
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </div>
            <div className="tempContainerFooter">
              <ActionBtn
                // onClick={this.onSaveClick}
                actioaBtnText="Save"
              />
              <FlatDefaultBtn className="bgTranceparent" btntext="Cancel" />
            </div>
          </Container>
        </Form>
      </div>
    );
  }
}

export default CreateNewTemplate;
