import React from "react";
import { Responsive } from "semantic-ui-react";

// Manage template container
import ManageTemplateContainer from "./ManageTemplateContainer";
import ManageTemplateMobile from "./ManageTemplateMobile";
import ReactGA from "react-ga";
import "./index.scss";

export default class ManageTemplate extends React.Component {
  componentDidMount() {
    ReactGA.pageview('/job/managetemplate');
  }
  render() {
    return (
      <div className="ManageTemplate">
        {/* For mobile */}
        <Responsive maxWidth={1024}>
          <ManageTemplateMobile />
        </Responsive>
        {/* For Desktop */}
        <Responsive minWidth={1025}>
          <ManageTemplateContainer />
        </Responsive>
      </div>
    );
  }
}
