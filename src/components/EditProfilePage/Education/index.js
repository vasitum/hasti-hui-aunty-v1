import React from "react";

import { Grid } from "semantic-ui-react";

import InfoSection from "../../Sections/InfoSection";

import InputContainer from "../../Forms/InputContainer";
import InputField from "../../Forms/FormFields/InputField";
import InputFieldLabel from "../../Forms/FormFields/InputFieldLabel";
import IcInfoIcon from "../../../assets/svg/IcInfo";
import IcPlusIcon from "../../../assets/svg/IcPlus";
import FlatDefaultBtn from "../../Buttons/FlatDefaultBtn";
import getPlaceholder from "../../Forms/FormFields/Placeholder";
import SearchFilterInput from "../../Forms/FormFields/SearchFilterInput";

import InfoIconLabel from "../../utils/InfoIconLabel";
import DownArrowCollaps from "../../utils/DownArrowCollaps";

import CloseIcon from "../../utils/CloseIcon";

import "./index.scss";

const infoicon = <IcInfoIcon pathcolor="#c8c8c8" />;

class Education extends React.Component {
  render() {
    return (
      <div className="panel-cardBody education">
        <InfoSection
          headerSize="large"
          color="blue"
          headerText="Education"
          rightIcon={<DownArrowCollaps />}
          collapsible>
          <div className="New_add">
            <InputContainer
              label="Institute/university"
              infoicon={<InfoIconLabel />}>
              <Grid>
                <Grid.Row>
                  <Grid.Column mobile={16} only="mobile">
                    <InputFieldLabel
                      label="Institute/university"
                      infoicon={<InfoIconLabel />}
                    />
                  </Grid.Column>
                  <Grid.Column computer={16}>
                    <SearchFilterInput
                      placeholder={getPlaceholder(
                        "Eg.University of Delhi",
                        "Eg.University of Delhi"
                      )}
                    />
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </InputContainer>
            <InputContainer label="Degree" infoicon={<InfoIconLabel />}>
              <Grid>
                <Grid.Row>
                  <Grid.Column mobile={16} only="mobile">
                    <InputFieldLabel
                      label="Degree"
                      infoicon={<InfoIconLabel />}
                    />
                  </Grid.Column>
                  <Grid.Column computer={16}>
                    <InputField
                      placeholder={getPlaceholder(
                        "Eg.B.E/B.Tech",
                        "Eg.B.E/B.Tech"
                      )}
                    />
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </InputContainer>
            <InputContainer label="Field of study" infoicon={<InfoIconLabel />}>
              <Grid>
                <Grid.Row>
                  <Grid.Column mobile={16} only="mobile">
                    <InputFieldLabel
                      label="Field of study"
                      infoicon={<InfoIconLabel />}
                    />
                  </Grid.Column>
                  <Grid.Column computer={16}>
                    <SearchFilterInput
                      placeholder={getPlaceholder(
                        "Eg.Computer science and Engg.",
                        "Eg.Computer science and Engg."
                      )}
                    />
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </InputContainer>
            <div className="">
              <Grid>
                <Grid.Row>
                  <Grid.Column width={4}>
                    <InputFieldLabel
                      label="Starting year"
                      infoicon={<InfoIconLabel />}
                    />
                  </Grid.Column>
                  <Grid.Column mobile={16} only="mobile">
                    <InputFieldLabel
                      label="Starting year"
                      infoicon={<InfoIconLabel />}
                    />
                  </Grid.Column>
                  <Grid.Column
                    mobile={16}
                    computer={5}
                    className="starting_yearField">
                    <InputField
                      inputtype="number"
                      min="1800"
                      max="2018"
                      placeholder={getPlaceholder("Eg.2014", "Eg.2014")}
                    />
                  </Grid.Column>
                  <Grid.Column width={3} textAlign="center">
                    <InputFieldLabel label="Completed year" />
                  </Grid.Column>
                  <Grid.Column mobile={16} only="mobile">
                    <InputFieldLabel
                      label="Completed year"
                      infoicon={<InfoIconLabel />}
                    />
                  </Grid.Column>
                  <Grid.Column mobile={16} computer={4}>
                    <InputField
                      min="1800"
                      max="2018"
                      inputtype="number"
                      placeholder={getPlaceholder("Eg.2018", "Eg.2018")}
                    />
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </div>
          </div>
          <InputContainer>
            <FlatDefaultBtn
              btnicon={<IcPlusIcon pathcolor="#c8c8c8" />}
              btntext="Add more education"
            />
          </InputContainer>
        </InfoSection>
      </div>
    );
  }
}

export default Education;
