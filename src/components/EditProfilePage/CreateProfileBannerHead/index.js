import React from 'react';

import { Grid, Header } from "semantic-ui-react";
import PropTypes from 'prop-types';
import FileuploadBtn from "../../Buttons/FileuploadBtn";

import Progressbar from "../../Progressbar"

import './index.scss';

const CreateProfileBannerHead = props => {
  return (

    <div className="CreateProfileBannerHead">
      <Grid>
        <Grid.Row>
          <Grid.Column mobile={16} computer={6}>
            <Progressbar />
          </Grid.Column>
          <Grid.Column mobile={16} computer={10} textAlign="right">
            <FileuploadBtn />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  )
}

CreateProfileBannerHead.propTypes = {

}

export default CreateProfileBannerHead;