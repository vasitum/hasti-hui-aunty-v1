import React from "react";

import { Button, Popup } from "semantic-ui-react";
import { Form } from "formsy-semantic-ui-react";

import ModalBanner from "../ModalBanner";

import aunty from "../../assets/banners/aunty.png";

import ModalFooterBtn from "../Buttons/ModalFooterBtn";

import { geocodeByAddress, getLatLng } from "react-places-autocomplete";
import createProfileOnServer from "../../api/user/createProfile";
import { USER } from "../../constants/api";

import IcInfoIcon from "../../assets/svg/IcInfo";

import IcDownArrowIcon from "../../assets/svg/IcDownArrow";
import IcProfileViewIcon from "../../assets/svg/IcProfileView";

import FlatDefaultBtn from "../Buttons/FlatDefaultBtn";

import PersonalDetails from "../CreateProfilePage/PersonalDetails";
import ProfileDetails from "../CreateProfilePage/ProfileDetails";
import Education from "../CreateProfilePage/Education";
import WorkExperience from "../CreateProfilePage/WorkExperience";
import Skills from "../CreateProfilePage/Skills";
import DesireJobs from "../CreateProfilePage/DesireJobs";
import Certificate from "../CreateProfilePage/Certificate";
// import PersonalDetails from "../CreateProfilePage/ProfileDetails";
import { uploadImage, uploadResume } from "../../utils/aws";
import locParser from "../../utils/env/locParser";

import uriToBlob from "../../utils/env/uriToBlob";
import { toast } from "react-toastify";

import PageLoader from "../PageLoader";

import CloseIcon from "../utils/CloseIcon";

import { withUserContextConsumer } from "../../contexts/UserContext";

import "./index.scss";

function getMonth(month) {
  const months = {
    1: "January",
    2: "February",
    3: "March",
    4: "April",
    5: "May",
    6: "June",
    7: "July",
    8: "August",
    9: "September",
    10: "October",
    11: "November",
    12: "December"
  };

  month = Number(month);

  if (!month || month > 12 || month < 1) {
    return "";
  }

  return months[month];
}

function getProcessedSkills(value, skills) {
  if (skills.length === value.length) {
    return skills;
  }

  // when user remvoes a value
  if (skills.length > value.length) {
    return skills.filter((val, idx) => {
      return value.indexOf(val.name) !== -1;
    });
  }

  if (skills.length < value.length) {
    return skills.concat([
      {
        exp: 0,
        name: value[value.length - 1]
      }
    ]);
  }
}

function parseUserExp(data) {
  if (!data.positions) {
    return false;
  }

  if (!data.positions.values || !Array.isArray(data.positions.values)) {
    return false;
  }

  return data.positions.values.map(val => {
    return {
      current: val.isCurrent,
      desc: "",
      designation: val.title,
      industry: "",
      doj: {
        month: getMonth(val.startDate.month),
        year: val.startDate.year
      },
      dor: {
        month: "",
        year: ""
      },
      loc: val.location.name,
      name: val.company.name,
      noticePeriod: 0,
      orgEmail: "",
      orgPhone: ""
    };
  });
}

function postedJobParsedSkills(skills) {
  if (!skills || skills.length === 0) {
    return {
      skills: [],
      skillOptions: [],
      cuurentValues: []
    };
  }

  const options = skills.map(val => {
    return {
      text: val.name,
      value: val.name
    };
  });

  const currenValues = skills.map(val => {
    return val.name;
  });

  return {
    skills: skills,
    skillOptions: options,
    cuurentValues: currenValues
  };
}

class EditProfilePage extends React.Component {
  state = {
    _id: window.localStorage.getItem(USER.UID),
    certs: [
      {
        cert_Institute: "",
        certificate_name: "",
        is_valid: true,
        receive_date: ""
      }
    ],
    countryCode: "+91",
    desc: "",
    desireJob: {
      avail: "",
      currentCtc: "",
      empType: "",
      expectedCtc: "",
      funArea: "",
      industry: "",
      role: "",
      spec: "",
      currency: "INR"
    },
    dob: 0,
    edus: [
      {
        completed: 0,
        course: "",
        institute: "",
        level: "",
        specs: [""],
        started: 0,
        type: "",
        onInfo: false
      }
    ],
    email: window.localStorage.getItem(USER.EMAIL),
    exps: [
      {
        current: true,
        desc: "",
        designation: "",
        industry: "",
        doj: {
          month: "",
          year: ""
        },
        dor: {
          month: "",
          year: ""
        },
        loc: "",
        name: "",
        noticePeriod: 0,
        orgEmail: "",
        orgPhone: "",
        onInfo: true
      }
    ],
    ext: {
      img: "",
      resume: "",
      imgBanner: ""
    },
    fName: "",
    gen: "",
    isVerified: false,
    lName: "",
    loc: {
      adrs: "",
      city: "",
      country: "",
      lat: 0,
      lon: 0,
      street: "",
      zipcode: ""
    },
    mobile: "",
    skills: [],
    skillFieldOptions: [],
    skillFieldCurrentValues: [],
    skillShowEditBtn: false,
    profileImage: null,
    profileImageFile: "",
    resumeDoc: null,
    resumeDocFile: "",
    canSubmit: false,
    title: "",
    bannerImage: aunty,
    bannerImageFile: "",
    isProfileLoading: false,
    imagePickerOpen: false
  };

  constructor(props) {
    super(props);
    this.onProfileSubmit = this.onProfileSubmit.bind(this);
  }

  componentDidMount() {
    const userData = this.props.userData;
    if (!userData) {
      return;
    }

    // console.log("USER__DATA", userData);

    const { cuurentValues, skills, skillOptions } = postedJobParsedSkills(
      userData.skills
    );

    // console.log(userData);

    this.setState(
      {
        ...this.state,
        certs: userData.certs,
        desc: userData.desc,
        desireJob: {
          ...userData.desireJob
        },
        dob: userData.dob,
        edus: userData.edus,
        exps: userData.exps,
        ext: userData.ext,
        fName: userData.fName,
        gen: userData.gen,
        isVerified: userData.isVerified,
        lName: userData.lName,
        loc: userData.loc,
        mobile: userData.mobile,
        skills: skills,
        skillFieldOptions: skillOptions,
        skillFieldCurrentValues: cuurentValues,
        skillShowEditBtn: true,
        title: userData.title,
        profileImage:
          userData.ext && userData.ext.img
            ? `https://s3-us-west-2.amazonaws.com/img-mwf/${
                userData._id
              }/image.jpg`
            : "",
        resumeDoc:
          userData.ext && userData.ext.resume
            ? `${userData.fName}_${userData.lName}.${userData.ext.resume}`
            : ""
      },
      () => {
        // setTimeout(() => {
        //   // force scroll
        //   window.location.hash = this.props.activeSection;
        // }, 200);
      }
    );
  }

  componentWillUnmount() {
    // reset it
    window.location.hash = "";
  }

  onInputChange = (e, { value, name }) => {
    this.setState({
      [name]: value
    });
  };

  onLinkedInCallback = res => {
    toast("Processing ...", {
      autoClose: 5000
    });

    this.setState({
      fName: res.firstName,
      lName: res.lastName,
      title: res.headline,
      desc: res.summary
    });

    const parsedExps = parseUserExp(res);
    if (parsedExps) {
      this.setState({
        exps: parsedExps
      });
    }
  };

  onLocSelect = address => {
    geocodeByAddress(address)
      .then(results => {
        const addrDetails = locParser(results[0].address_components, address);

        if (typeof addrDetails === "string") {
          this.setState({
            loc: {
              ...this.state.loc,
              city: addrDetails,
              adrs: address
            }
          });
        } else {
          this.setState({
            loc: {
              ...this.state.loc,
              city: addrDetails.primary,
              country: addrDetails.country,
              adrs: address
            }
          });
        }

        return getLatLng(results[0]);
      })
      .then(latLng => {
        this.setState({
          loc: {
            ...this.state.loc,
            lat: latLng.lat,
            lon: latLng.lng
          }
        });
      })
      .catch(error => console.error("Error", error));
  };

  onLocChange = address => {
    this.setState({
      loc: {
        ...this.state.loc,
        adrs: address
      }
    });
  };

  onProfileSummaryChange = html => {
    this.setState({
      desc: html
    });
  };

  onImageChange = e => {
    const file = e.target.files[0];

    // Reset Value
    e.target.value = "";

    if (!file) {
      return;
    }

    const EXTNS = ["png", "jpeg", "jpg", "gif", "tiff", "bmp"];
    const extn = file.name.split(".").pop();
    if (!extn || EXTNS.indexOf(String(extn).toLowerCase()) === -1) {
      return;
    }

    const fr = new FileReader();

    fr.onload = e => {
      this.setState({
        ext: {
          ...this.state.ext,
          img: extn
        },

        profileImageFile: file,
        profileImage: e.target.result,
        imagePickerOpen: !this.state.imagePickerOpen
      });
    };

    fr.readAsDataURL(file);
  };

  onImagePickerCancelClick = ev => {
    this.setState({
      imagePickerOpen: !this.state.imagePickerOpen,
      profileImageFile: "",
      profileImage: ""
    });
  };

  onImagePickerSaveClick = image => {
    this.setState({
      profileImage: image,
      imagePickerOpen: !this.state.imagePickerOpen
    });
  };

  onImageRemove = e => {
    this.setState({
      ext: {
        ...this.state.ext,
        img: ""
      },

      profileImageFile: "",
      profileImage: ""
    });
  };

  onResumeChange = e => {
    const file = e.target.files[0];
    const EXTNS = ["doc", "docx", "rtf", "pdf", "jpeg", "jpg"];
    const extn = file.name.split(".").pop();
    console.log(extn);
    if (!extn || EXTNS.indexOf(String(extn).toLowerCase()) === -1) {
      return;
    }

    const fr = new FileReader();

    fr.onload = e => {
      this.setState({
        ext: {
          ...this.state.ext,
          resume: extn
        },
        resumeDocFile: file,
        resumeDoc: file.name
      });
    };

    fr.readAsDataURL(file);
  };

  onResumeRemove = e => {
    this.setState({
      ext: {
        ...this.state.ext,
        resume: ""
      },
      resumeDocFile: "",
      resumeDoc: ""
    });
  };

  onAddMoreEducation = ev => {
    this.setState({
      edus: [
        ...this.state.edus,
        {
          completed: 0,
          course: "",
          institute: "",
          level: "",
          specs: [""],
          started: 0,
          type: "",
          onInfo: false
        }
      ]
    });
  };

  onRemoveEducationClick = idx => {
    this.setState({
      edus: this.state.edus.filter((val, index) => {
        return index !== idx;
      })
    });
  };

  onAddMoreWorkExpClick = ev => {
    this.setState({
      exps: [
        ...this.state.exps,
        {
          current: false,
          desc: "",
          designation: "",
          industry: "",
          doj: {
            month: "",
            year: ""
          },
          dor: {
            month: "",
            year: ""
          },
          loc: "",
          name: "",
          noticePeriod: 0,
          orgEmail: "",
          orgPhone: "",
          onInfo: false
        }
      ]
    });
  };

  onRemoveWorkExpClick = idx => {
    this.setState({
      exps: this.state.exps.filter((val, index) => {
        return index !== idx;
      })
    });
  };

  onAddMoreCertificateClick = ev => {
    this.setState({
      certs: [
        ...this.state.certs,
        {
          cert_Institute: "",
          certificate_name: "",
          is_valid: true,
          receive_date: 0
        }
      ]
    });
  };

  onRemoveCertificateClick = idx => {
    this.setState({
      certs: this.state.certs.filter((val, index) => {
        return index !== idx;
      })
    });
  };

  onExpInputChange = (ev, { value, name, idx }) => {
    const newExp = [...this.state.exps];

    // current dataset
    if (name === "current" || name === "onInfo") {
      newExp[idx][name] = !newExp[idx][name];
    } else if (name === "doj_year" || name === "doj_month") {
      newExp[idx].doj = {
        ...newExp[idx].doj,
        [`${name.split("_")[1]}`]: value
      };
    } else if (name === "dor_year" || name === "dor_month") {
      newExp[idx].dor = {
        ...newExp[idx].dor,
        [`${name.split("_")[1]}`]: value
      };
    } else {
      newExp[idx][name] = value;
    }

    this.setState({
      exps: newExp
    });
  };

  onExpQuillChange = (html, idx) => {
    const newExp = [...this.state.exps];
    newExp[idx].desc = html;

    this.setState({
      exps: newExp
    });
  };

  onEduInputChange = (ev, { value, name, idx }) => {
    const newExp = [...this.state.edus];

    if (name === "onInfo") {
      newExp[idx][name] = !newExp[idx][name];
    } else if (name === "specs") {
      newExp[idx][name] = [value];
    } else {
      newExp[idx][name] = value;
    }
    this.setState({
      edus: newExp
    });
  };

  onCertInputChange = (ev, { value, name, idx }) => {
    const newExp = [...this.state.certs];
    newExp[idx][name] = value;

    this.setState({
      certs: newExp
    });
  };

  onSKillAdd = (e, { value }) => {
    this.setState({
      skillFieldOptions: [
        { text: value, value },
        ...this.state.skillFieldOptions
      ]
    });
  };

  onSKillChange = (e, { value }) => {
    this.setState({
      skillFieldCurrentValues: value,
      skills: getProcessedSkills(value, this.state.skills),
      skillShowEditBtn: value.length ? true : false
    });
  };

  onDesiredJobChange = (ev, { value, name }) => {
    this.setState({
      desireJob: {
        ...this.state.desireJob,
        [name]: value
      }
    });
  };

  onDesiredJobAutoComplete = ({ value, name }) => {
    this.setState({
      desireJob: {
        ...this.state.desireJob,
        [name]: value
      }
    });
  };

  onSelectInfoExp = (ev, { value }) => {
    const exps = this.state.exps.map(val => {
      if (value === val._id) {
        return {
          ...val,
          onInfo: true
        };
      }

      return {
        ...val,
        onInfo: false
      };
    });

    this.setState({
      exps: exps
    });
  };

  onCheckInfoExp = valId => {
    const exps = this.state.exps.map(val => {
      if (val.onInfo === true && valId === val._id) {
        return {
          ...val,
          onInfo: false
        };
      }

      if (valId === val._id) {
        return {
          ...val,
          onInfo: true
        };
      }

      return {
        ...val,
        onInfo: false
      };
    });

    this.setState({
      exps: exps
    });
  };

  onSelectInfoEdu = (ev, { value }) => {
    const edus = this.state.edus.map(val => {
      if (value === val._id) {
        return {
          ...val,
          onInfo: true
        };
      }

      return {
        ...val,
        onInfo: false
      };
    });

    this.setState({
      edus: edus
    });
  };

  onCheckInfoEdu = valId => {
    const edus = this.state.edus.map(val => {
      if (val.onInfo === true && valId === val._id) {
        return {
          ...val,
          onInfo: false
        };
      }

      if (valId === val._id) {
        return {
          ...val,
          onInfo: true
        };
      }

      return {
        ...val,
        onInfo: false
      };
    });

    this.setState({
      edus: edus
    });
  };

  onFileChange = e => {
    const file = e.target.files[0];
    const extn = file.name.split(".").pop();
    const reader = new FileReader();

    reader.onloadend = e => {
      this.setState({
        ext: {
          ...this.state.ext,
          imgBanner: "jpg"
        },
        bannerImageFile: file,
        bannerImage: reader.result
      });
    };

    reader.readAsDataURL(file);
  };

  onFormValid = ev => {
    this.setState({
      canSubmit: true
    });
  };

  onFormInValid = ev => {
    this.setState({
      canSubmit: false
    });
  };

  /**
   *
   * @param {SyntheticEvent} ev
   */
  async onProfileSubmit(ev) {
    const {
      skillShowEditBtn,
      skillFieldCurrentValues,
      skillFieldOptions,
      bannerImage,
      bannerImageFile,
      profileImage,
      profileImageFile,
      resumeDocFile,
      resumeDoc,
      isProfileLoading,
      imagePickerOpen,
      canSubmit,
      ...userData
    } = this.state;
    const baseJson = JSON.parse(window.localStorage.getItem(USER.BASE_JSON));
    const finalJson = Object.assign({}, baseJson, userData);

    this.setState({
      isProfileLoading: true
    });

    try {
      const res = await createProfileOnServer(finalJson);
      const data = await res.json();

      if (resumeDocFile) {
        const resUpload = await uploadResume(
          `${userData._id}/${userData.fName}_${userData.lName}.${
            userData.ext.resume
          }`,
          resumeDocFile
        );
      }

      if (profileImageFile) {
        let imgBlob = uriToBlob(profileImage);
        const profileImgUpload = await uploadImage(
          `${userData._id}/image.jpg`,
          imgBlob
        );
      }

      if (data) {
        // console.log("edited Data", data);
        // window.localStorage.setItem(USER.NAME, `${data.fName} ${data.lName}`);
        // window.localStorage.setItem(
        //   USER.CTC,
        //   data.desireJob ? data.desireJob.currentCtc : ""
        // );
        // window.localStorage.setItem(USER.EXP, 0);
        // window.localStorage.setItem(USER.IMG_EXT, data.ext ? data.ext.img : "");
        // window.localStorage.setItem(USER.LOC, data.loc ? data.loc.city : "");
        // window.localStorage.setItem(USER.PHONE, data.mobile);
        // window.localStorage.setItem(USER.TITLE, data.title);

        this.props.onUserChange({
          id: data._id,
          email: data.email,
          fName: data.fName,
          lName: data.lName,
          title: data.title,
          loc: data.loc ? data.loc.city : "",
          userImg: data.ext ? data.ext.img : "",
          mobile: data.mobile,
          token: data.googleToken,
          isVerified: data.isVerified,
          ctc: 0
        });

        window.location.reload();
      }
    } catch (error) {
      console.error(error);
    }
  }

  onSkillEditModalInputChange = (e, { value, idx, name }) => {
    const newSkills = [...this.state.skills];
    newSkills[idx][name] = value;

    this.setState({
      skills: newSkills
    });
  };

  onSkillEditModalRemove = (e, { idx }) => {
    this.setState((prevState, props) => {
      const newSkills = [...prevState.skills];
      const newSkillOptions = [...prevState.skillFieldOptions];
      const newSkillCurrentValues = [...prevState.skillFieldCurrentValues];

      newSkills.splice(idx, 1);
      newSkillCurrentValues.splice(idx, 1);

      return {
        ...prevState,
        skills: newSkills,
        skillFieldCurrentValues: newSkillCurrentValues
      };
    });
  };

  onSkillModalSave = e => {
    this.setState({
      skillShowEditBtn: false
    });
  };

  onActionBtnClick = ev => {
    if (!this.state.canSubmit) {
      ev.preventDefault();
      toast("Oops! Please Fill your Form Correctly");
    }
  };

  render() {
    const { activeSection, onCancelClick } = this.props;

    return (
      <div className="uert_editProfile user_editProfileContainer">
        <div
          className="card_panel"
          style={{
            marginTop: "0px"
          }}>
          <ModalBanner headerText="Edit profile">
            {/* <Button compact className="save_btn">
              Save
            </Button> */}
          </ModalBanner>

          <Form
            className="editProfile_form"
            noValidate
            onValid={this.onFormValid}
            onInvalid={this.onFormInValid}
            onValidSubmit={this.onProfileSubmit}>
            <PersonalDetails
              data={this.state}
              onInputChange={this.onInputChange}
              onLocChange={this.onLocChange}
              onLocSelect={this.onLocSelect}
              isEditProfile
              isActive={"personal" === activeSection || "all" === activeSection}
            />

            <ProfileDetails
              data={this.state}
              onInputChange={this.onInputChange}
              onProfileSummaryChange={this.onProfileSummaryChange}
              onImageChange={this.onImageChange}
              onImageRemove={this.onImageRemove}
              onResumeChange={this.onResumeChange}
              onSelectInfoEdu={this.onSelectInfoEdu}
              onSelectInfoExp={this.onSelectInfoExp}
              onResumeRemove={this.onResumeRemove}
              imagePickerOpen={this.state.imagePickerOpen}
              onImagePickerCancelClick={this.onImagePickerCancelClick}
              onImagePickerSaveClick={this.onImagePickerSaveClick}
              isEditProfile
              rightIcon={<CloseIcon />}
              isActive={"profile" === activeSection || "all" === activeSection}
            />

            <Skills
              data={this.state}
              onSKillAdd={this.onSKillAdd}
              onSKillChange={this.onSKillChange}
              onSkillModalChange={this.onSkillEditModalInputChange}
              onSkillModalRemove={this.onSkillEditModalRemove}
              onSkillModalSave={this.onSkillModalSave}
              isEditProfile
              isActive={"skill" === activeSection || "all" === activeSection}
            />

            <Education
              data={this.state}
              onAddMoreClick={this.onAddMoreEducation}
              onRemoveClick={this.onRemoveEducationClick}
              onInputChange={this.onEduInputChange}
              onCheckInfoEdu={this.onCheckInfoEdu}
              isEditProfile
              isActive={
                "education" === activeSection || "all" === activeSection
              }
            />

            <WorkExperience
              data={this.state}
              onInputChange={this.onExpInputChange}
              onAddMoreClick={this.onAddMoreWorkExpClick}
              onRemoveClick={this.onRemoveWorkExpClick}
              onQuillChange={this.onExpQuillChange}
              onCheckInfoExp={this.onCheckInfoExp}
              isEditProfile
              isActive={
                "experience" === activeSection || "all" === activeSection
              }
            />

            <DesireJobs
              data={this.state.desireJob}
              onAutoComplete={this.onDesiredJobAutoComplete}
              onInputChange={this.onDesiredJobChange}
              isEditProfile
              isActive={"desired" === activeSection || "all" === activeSection}
            />

            <Certificate
              data={this.state}
              onAddMoreClick={this.onAddMoreCertificateClick}
              onRemoveClick={this.onRemoveCertificateClick}
              onInputChange={this.onCertInputChange}
              isEditProfile
              isActive={
                "certificate" === activeSection || "all" === activeSection
              }
            />

            <ModalFooterBtn
              onCancelClick={onCancelClick}
              submitBtnType={"submit"}
              actioaBtnText="Save"
              onClick={this.onActionBtnClick}
            />
          </Form>
        </div>
        <PageLoader
          active={this.state.isProfileLoading}
          loaderText={"Updating Profile ..."}
        />
      </div>
    );
  }
}

export default withUserContextConsumer(EditProfilePage);
