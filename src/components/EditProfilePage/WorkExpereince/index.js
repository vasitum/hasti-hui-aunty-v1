import React from "react";

import { Grid } from "semantic-ui-react";

import InfoSection from "../../Sections/InfoSection";

import InputContainer from "../../Forms/InputContainer";
import InputField from "../../Forms/FormFields/InputField";
import SelectOptionField from "../../Forms/FormFields/SelectOptionField";

import InputFieldLabel from "../../Forms/FormFields/InputFieldLabel";
import IcInfoIcon from "../../../assets/svg/IcInfo";
import IcPlusIcon from "../../../assets/svg/IcPlus";

import FlatDefaultBtn from "../../Buttons/FlatDefaultBtn";
import CheckboxField from "../../Forms/FormFields/CheckboxField";

import getPlaceholder from "../../Forms/FormFields/Placeholder";
import SearchFilterInput from "../../Forms/FormFields/SearchFilterInput";

import InfoIconLabel from "../../utils/InfoIconLabel";
import DownArrowCollaps from "../../utils/DownArrowCollaps";

import "./index.scss";

const infoicon = <IcInfoIcon pathcolor="#c8c8c8" />;

class WorkExpereince extends React.Component {

  render() {
    return (
      <div className="panel-cardBody work_experience">
        <InfoSection
          headerSize="large"
          color="blue"
          headerText="Work experience"
          collapsible
          rightIcon={<DownArrowCollaps />}
        >

          <InputContainer label="Company name" infoicon={<InfoIconLabel />}>
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only='mobile'>
                  <InputFieldLabel label="Company name" infoicon={<InfoIconLabel />} />
                </Grid.Column>
                <Grid.Column computer={16}>
                  <InputField placeholder={getPlaceholder("Eg.Mavenworkforce", "Eg.Mavenworkforce")} />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>

          <InputContainer label="Designation" infoicon={<InfoIconLabel />}>
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only='mobile'>
                  <InputFieldLabel label="Designation" infoicon={<InfoIconLabel />} />
                </Grid.Column>
                <Grid.Column computer={16}>
                  <SearchFilterInput placeholder={getPlaceholder("Eg.Software Developer", "Eg.Software Developer")} />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>

          <InputContainer label="Industry" infoicon={<InfoIconLabel />}>
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only='mobile'>
                  <InputFieldLabel label="Industry" infoicon={<InfoIconLabel />} />
                </Grid.Column>
                <Grid.Column computer={16}>
                  <SelectOptionField
                    placeholder={getPlaceholder("Industry", "Eg. IT/Software industry")}
                    optionsItem={[
                      { value: "IT/Software industry", text: "IT/Software industry" },
                      { value: "IT/Software industry", text: "IT/Software industry" }
                    ]}
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>

          <InputContainer label="Company location" infoicon={<InfoIconLabel />}>
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only='mobile'>
                  <InputFieldLabel label="Company location" infoicon={<InfoIconLabel />} />
                </Grid.Column>
                <Grid.Column computer={16}>
                  <SearchFilterInput placeholder={getPlaceholder("Eg.London", "Eg.London")} />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>

          <div className="">
            <Grid>
              <Grid.Row>
                <Grid.Column width={4}>
                  <InputFieldLabel label="From" />
                </Grid.Column>
                <Grid.Column mobile={16} only='mobile'>
                  <InputFieldLabel label="From" infoicon={<InfoIconLabel />} />
                </Grid.Column>
                <Grid.Column mobile={16} computer={6} className="month">
                  <SelectOptionField
                    placeholder={getPlaceholder("Month", "Month")}
                    optionsItem={[
                      { value: "1", text: "January" },
                      { value: "2", text: "February" },
                      { value: "3", text: "March " },
                      { value: "4", text: "April" },
                      { value: "5", text: "May" },
                      { value: "6", text: "June" },
                      { value: "7", text: "July" },
                      { value: "8", text: "August" },
                      { value: "9", text: "September" },
                      { value: "10", text: "October" },
                      { value: "11", text: "November" },
                      { value: "12", text: "December" }
                    ]} />
                </Grid.Column>

                <Grid.Column mobile={16} computer={6}>
                  <InputField 
                  min="1800"
                  max="2018"
                   inputtype="number"
                  placeholder="Year" />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </div>

          <InputContainer>
            <CheckboxField checkboxLabel="I currently work here" />
          </InputContainer>


          <InputContainer>
            <FlatDefaultBtn btnicon={<IcPlusIcon pathcolor="#c8c8c8" />} btntext="Add more experience" />
          </InputContainer>

        </InfoSection>
      </div>

    )
  }
}

export default WorkExpereince;