import React from "react";

import { Grid } from "semantic-ui-react";

import InfoSection from "../../Sections/InfoSection";

import InputContainer from "../../Forms/InputContainer";
import InputField from "../../Forms/FormFields/InputField";
import QuillText from "../../CardElements/QuillText";
import InputFieldLabel from "../../Forms/FormFields/InputFieldLabel";
import IcInfoIcon from "../../../assets/svg/IcInfo";
import IcUploadIcon from "../../../assets/svg/IcUpload";
import FileuploadBtn from "../../Buttons/FileuploadBtn";
import getPlaceholder from "../../Forms/FormFields/Placeholder";
import SearchFilterInput from "../../Forms/FormFields/SearchFilterInput";

import InfoIconLabel from "../../utils/InfoIconLabel";
import DownArrowCollaps from "../../utils/DownArrowCollaps";



import "./index.scss";


class ProfileDetails extends React.Component {

  render() {
    return (
      <div className="panel-cardBody Profile_details">
        <InfoSection
          headerSize="large"
          color="blue"
          headerText="Profile details"
          rightIcon={<DownArrowCollaps />}
          collapsible
        >

          <InputContainer label="Profile headline" infoicon={<InfoIconLabel />}>
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only='mobile'>
                  <InputFieldLabel label="Profile headline" infoicon={<InfoIconLabel />} />
                </Grid.Column>
                <Grid.Column computer={16}>
                  <InputField placeholder={getPlaceholder("Eg.Software Developer", "Eg.Detail-Oriented Worker with Years of Administrative Experience")} />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>

          <InputContainer label="Current Position" infoicon={<InfoIconLabel />}>
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only='mobile'>
                  <InputFieldLabel label="Current Position" infoicon={<InfoIconLabel />} />
                </Grid.Column>
                <Grid.Column computer={16}>
                  <SearchFilterInput
                    placeholder={getPlaceholder("Eg.Intern", "Eg.Intern")}
                    optionsItem={[
                      { value: "job", text: "Relevant" },
                      { value: "job_by_date", text: "Recent" },
                      { value: "job", text: "Relevant" },
                      { value: "job_by_date", text: "Recent" },
                    ]}
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>

          <InputContainer label="Education" infoicon={<InfoIconLabel />}>
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only='mobile'>
                  <InputFieldLabel label="Education" infoicon={<InfoIconLabel />} />
                </Grid.Column>
                <Grid.Column computer={16}>
                  <SearchFilterInput placeholder="Eg. Required " />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>

          <InputContainer label="Describe your job profile" infoicon={<InfoIconLabel />}>
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only='mobile'>
                  <InputFieldLabel label="Describe your job profile" infoicon={<InfoIconLabel />} />
                </Grid.Column>
                <Grid.Column computer={16}>
                  <QuillText placeholder="Describe the position and its role within your company." />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>

          <InputContainer label="Upload resume" infoicon={<InfoIconLabel />} className="upload_resume">
            <FileuploadBtn btnText="Upload resume" btnIcon={<IcUploadIcon pathcolor="#c8c8c8" width="11" />} />
            <p className="upload_resumeLabel">Supported formats: .DOC, .DOCX, .TXT, .RTF, .PDF, JPEG</p>
          </InputContainer>



          <InputContainer label="Upload profile image" infoicon={<InfoIconLabel />} className="upload_resume">
            <FileuploadBtn btnText="Upload image" btnIcon={<IcUploadIcon pathcolor="#c8c8c8" width="11" />} />
            <p className="upload_resumeLabel">Supported formats: .JPG, .PNG, .JPEG</p>
          </InputContainer>
        </InfoSection>
      </div>

    )
  }
}

export default ProfileDetails;