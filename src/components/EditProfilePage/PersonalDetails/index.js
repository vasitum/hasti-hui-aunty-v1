import React from "react";

import { Grid } from "semantic-ui-react";

import InfoSection from "../../Sections/InfoSection";

import InputContainer from "../../Forms/InputContainer";
import InputField from "../../Forms/FormFields/InputField";

import InputFieldLabel from "../../Forms/FormFields/InputFieldLabel";
import getPlaceholder from "../../Forms/FormFields/Placeholder";

import InfoIconLabel from "../../utils/InfoIconLabel";
import SkipNowCollaps from "../../utils/SkipNowCollaps";
import DownArrowCollaps from "../../utils/DownArrowCollaps";

import "./index.scss";


const labelStar = (str) => {
  return <div>
           <span>{str}</span>
           <span style={{color: "#0b9ed0"}}>*</span>
        </div>;
  }


class PersonalDetails extends React.Component {

  render() {
    return (
      <div className="panel-cardBody personal_details">
        <InfoSection
          headerSize="large"
          color="blue"
          headerText="Personal details"
          rightIcon={<DownArrowCollaps />}
          collapsible
        >

          <InputContainer label={labelStar(`First name`)} infoicon={<InfoIconLabel />}>
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only='mobile'>
                  <InputFieldLabel label={labelStar(`First name`)} infoicon={<InfoIconLabel />} />
                </Grid.Column>
                <Grid.Column computer={16}>
                  <InputField placeholder={getPlaceholder("Eg.Jack", "Eg.Jack")}
                  required />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>

          <InputContainer label={labelStar(`Last name`)} infoicon={<InfoIconLabel />}>
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only='mobile'>
                  <InputFieldLabel label={labelStar(`Last name`)} infoicon={<InfoIconLabel />} />
                </Grid.Column>
                <Grid.Column computer={16}>
                  <InputField placeholder={getPlaceholder("Eg.Ryan", "Eg.Ryan")} required />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>

          <InputContainer label={labelStar(`Address`)} infoicon={<InfoIconLabel />}>
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} only='mobile'>
                  <InputFieldLabel label={labelStar(`Address`)} infoicon={<InfoIconLabel />} />
                </Grid.Column>
                <Grid.Column computer={16}>
                  <InputField placeholder={getPlaceholder("Eg.London ", "Eg.London")} required/>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </InputContainer>
        </InfoSection>
      </div>

    )
  }
}

export default PersonalDetails;