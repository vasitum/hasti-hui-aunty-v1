import React from "react";

import { Grid } from "semantic-ui-react";

import InfoSection from "../../Sections/InfoSection";

import InputContainer from "../../Forms/InputContainer";
import InputField from "../../Forms/FormFields/InputField";
import InputFieldLabel from "../../Forms/FormFields/InputFieldLabel";
import IcCloseIcon from "../../../assets/svg/IcCloseIcon";
import getPlaceholder from "../../Forms/FormFields/Placeholder";

import DownArrowCollaps from "../../utils/DownArrowCollaps";

import "./index.scss";



class Skills extends React.Component {

  render() {
    return (
      <div className="panel-cardBody skill">
        <InfoSection
          headerSize="large"
          color="blue"
          headerText="Skill"
          rightIcon={<DownArrowCollaps />}
          collapsible
          >

          <div className="edit_profileSkill">

            <InputContainer label="HTML5" >
              <Grid>
                <Grid.Row only='mobile' className="mobile_editProfileSkill">
                  <Grid.Column mobile={8} >
                    <InputFieldLabel label="HTML5" />
                  </Grid.Column>
                  <Grid.Column mobile={8} textAlign="right">
                    <IcCloseIcon pathcolor='#c8c8c8' height='10' width='10' />
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row >
                  <Grid.Column computer={16}>
                    <InputField placeholder={getPlaceholder("Year of experience", "Select")} />
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </InputContainer>

            <InputContainer label="CSS" >
              <Grid>
                <Grid.Row only='mobile' className="mobile_editProfileSkill">
                  <Grid.Column mobile={8} >
                    <InputFieldLabel label="CSS" />
                  </Grid.Column>
                  <Grid.Column mobile={8} textAlign="right">
                    <IcCloseIcon pathcolor='#c8c8c8' height='10' width='10' />
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row >
                  <Grid.Column computer={16}>
                    <InputField placeholder={getPlaceholder("Year of experience", "Select")} />
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </InputContainer>

            <InputContainer label="Abobe photoshop" >
              <Grid>
                <Grid.Row only='mobile' className="mobile_editProfileSkill">
                  <Grid.Column mobile={8} >
                    <InputFieldLabel label="Abobe photoshop" />
                  </Grid.Column>
                  <Grid.Column mobile={8} textAlign="right">
                    <IcCloseIcon pathcolor='#c8c8c8' height='10' width='10' />
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row >
                  <Grid.Column computer={16}>
                    <InputField placeholder={getPlaceholder("Year of experience", "Select")} />
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </InputContainer>
          </div>

        </InfoSection>
      </div>

    )
  }
}

export default Skills;