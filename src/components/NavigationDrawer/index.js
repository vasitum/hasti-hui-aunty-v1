import React from "react";
import { Grid, Image, List, Modal } from "semantic-ui-react";
import AIBased from "../../assets/img/AlBased.png";
import Lock from "../../assets/svg/IcPrivacyLock";
import Email from "../../assets/svg/IcDrawerEmail";
import Mobile from "../../assets/svg/IcDrawerMobile";
import Job from "../../assets/svg/Icjob";
import Company from "../../assets/svg/IcCompany";
import Signout from "../../assets/svg/IcDrawerSignOut";
import UseraccessModal from "../ModalPageSection/UserAccessModal";
import { Link } from "react-router-dom";
import isLoggedOut from "../../utils/env/isLoggedin";

import NavbarAccordianCandidate from "../Navbar/NavbarAccordianCandidate";
import NavbarAccordianRecruiter from "../Navbar/NavbarAccordianRecruiter";
import NavbarMenuLists from "../Navbar/NavbarMenuLists";
import "./index.scss";


const onSignOutClick = ev => {
  window.localStorage.clear();
  window.location.href = "/";
};

const NavigationDrawer = props => {
  return (
    <div className="Navigation">
      {/* <div className="Navigation_Header">
          <Image
            src={AIBased}
            size="tiny"
            circular
            className="Profile_Img"
            centered
          />
          <p className="Profile_Name">Kety Jeff</p>
          <p className="Profile_Desc">UI Developer</p>
          <UseraccessModal />
        </div> */}

      {/* <h3>Navigation</h3> */}
      {/* <List selection verticalAlign="middle" className="Detail_List">
        <List.Item as={Link} to={"/"} className="PostJob_Text">
          
          <List.Content>
            <p>Home</p>
          </List.Content>
        </List.Item>
        
        <List.Item
          as={Link}
          to={"/user/view/profile"}
          className="Password_Text">
          
          <List.Content>
            <p>My Profile</p>
          </List.Content>
        </List.Item>
        <List.Item as={Link} to={"/job/new"} className="PostJob_Text">
          
          <List.Content>
            <p>Post a Job</p>
          </List.Content>
        </List.Item>

        <List.Item as={Link} to={"/search"} className="PostJob_Text">
          
          <List.Content>
            <p>Search</p>
          </List.Content>
        </List.Item>

       
        

        
        <List.Item as={Link} to={"/messaging"} className="Mobile_Text">
         
          <List.Content>
            <p>Messaging</p>
          </List.Content>
        </List.Item>
        <List.Item as={Link} to={"/job/managetemplate"}>
          <List.Content>
            <p>Manage Template</p>
          </List.Content>
        </List.Item>

        
      </List>

      <NavbarAccordianCandidate />
      
      <NavbarAccordianRecruiter /> */}
      <NavbarMenuLists 
        isShowMobile
        isShowMobileCandidate
        isShowMobileRecrutment
      />
      <List className="Signout_Footer">
        <List.Item onClick={onSignOutClick} className="Sign_Out">
          <List.Icon>
            <Signout pathcolor="#c9c9c9" />
          </List.Icon>
          <List.Content>
            <p>Sign out</p>
          </List.Content>
        </List.Item>
      </List>
    </div>
  );
};

export default NavigationDrawer;
