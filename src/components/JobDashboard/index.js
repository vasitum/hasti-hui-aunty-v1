import React, { Component } from "react";
import { Table, Icon, Container } from "semantic-ui-react";
import { Link } from "react-router-dom";
import getAllPostedJobs from "../../api/jobs/getAllPostedJobs";
import PropTypes from "prop-types";

class JobDashboard extends Component {
  state = {
    jobs: []
  };

  async componentDidMount() {
    try {
      const res = await getAllPostedJobs();
      const data = await res.json();

      // console.log(data);
      const jobData = data.map(val => {
        return {
          title: val.title,
          link: "/job/view/" + val._id
        };
      });

      this.setState({
        jobs: jobData
      });
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    return (
      <div>
        <Container style={{ marginTop: "40px" }}>
          <h1>Work in Progress</h1>
          <Table celled striped>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell colSpan="3">Page Index</Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>
              {this.state.jobs.map(val => {
                return (
                  <Table.Row>
                    <Table.Cell>
                      <Icon name="folder" /> {val.title}
                    </Table.Cell>
                    <Table.Cell>
                      <Link to={val.link}>{val.link}</Link>
                    </Table.Cell>
                    {/* <Table.Cell textAlign="center">Actions Done</Table.Cell> */}
                  </Table.Row>
                );
              })}
            </Table.Body>
          </Table>
        </Container>
      </div>
    );
  }
}

JobDashboard.propTypes = {};

export default JobDashboard;
