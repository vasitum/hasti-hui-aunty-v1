import React from "react";
import { Dimmer, Loader } from "semantic-ui-react";

import "./index.scss";

export default ({ active, loaderText, ...props }) => {
  return (
    <Dimmer active={active} {...props} page>
      <Loader> {loaderText} </Loader>
    </Dimmer>
  );
};
