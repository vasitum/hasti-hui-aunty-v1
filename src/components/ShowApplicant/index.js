import React from 'react'
import { Grid, Image, Checkbox, Dropdown, Button, List, Form, Input } from 'semantic-ui-react'
import SelectOptionField from "../Forms/FormFields/SelectOptionField";
import getPlaceholder from "../Forms/FormFields/Placeholder";
import SearchIcon from "../../assets/svg/SearchIcon";
import "./index.scss";

const ShowApplicant = () => (
    <div className="Main_Grid">
      <Grid className="Main_Grid_Show">
        <Grid.Row className="Main_Grid_Row">
          <Grid.Column width="5">
          <h2>Showing applicants for</h2>
          </Grid.Column>

          <Grid.Column width="4">
          <SelectOptionField
           placeholder={getPlaceholder("All Jobs", "All Jobs")}
           optionsItem={[
            { value: "All Jobs", text: "All Jobs" },   
            { value: "Pending Jobs", text: "Pending Jobs" },
            { value: "Interview Jobs", text: "Interview Jobs" }
           ]}
           />
          </Grid.Column>

          <Grid.Column width="3"></Grid.Column>

          <Grid.Column width="4" textAlign="right">
            <div className="aplicantSearch">
            <Input transparent  fluid icon='search' placeholder='Search applicants' />
            </div>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  )
  
  export default ShowApplicant;