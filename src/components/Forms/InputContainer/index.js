import React from "react";
import PropTypes from "prop-types";

import { Grid, Header } from "semantic-ui-react";
import InputFieldLabel from "../FormFields/InputFieldLabel";

import "./index.scss";

const InputContainer = props => {
  const { label, children, infoicon, className, ...restProps } = props;
  return (
    <div className={`InputContainer ${className}`} {...restProps}>
      <Grid>
        <Grid.Column width={4} className="mobile hidden" textAlign="right">
          <InputFieldLabel label={label} infoicon={infoicon} />
        </Grid.Column>

        <Grid.Column mobile={16} computer={12}>
          {children}
        </Grid.Column>
      </Grid>
    </div>
  );
};

InputContainer.propTypes = {
  label: PropTypes.string
};

export default InputContainer;
