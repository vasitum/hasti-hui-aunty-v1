import React from "react";
import PropTypes from "prop-types";

import { Grid, Header } from "semantic-ui-react";
import InputFieldLabel from "../FormFields/InputFieldLabel";
import SelectOptionField from "../FormFields/SelectOptionField";
import DropdownMenu from "../../DropdownMenu";

import MobileGrid from "../../MobileComponents/MobileGrid";

import "./index.scss";

const InputGridContainer = props => {
  const { label, infoicon, mobileInputFieldLabel } = props;
  return (
    <div className="InputGridContainer">
      <Grid>
        <Grid.Row>
          <Grid.Column computer={4} only="computer large widescreem">
            <InputFieldLabel label={label} infoicon={infoicon} />
          </Grid.Column>
          <Grid.Column mobile={16} computer={6} className="mobile_inputField">
            <MobileGrid>
              <div className="mobile_inputLabel">
                <p>{mobileInputFieldLabel}</p>
              </div>
            </MobileGrid>
            <SelectOptionField placeholder="Minimum in lakhs per annum" />
          </Grid.Column>
          <Grid.Column
            computer={1}
            only="computer large widescreem"
            textAlign="center">
            <span className="divider_to">To</span>
          </Grid.Column>
          <Grid.Column mobile={16} computer={5} className="mobile_inputField">
            {/* <MobileGrid>
              <div className="mobile_inputLabel">
                <DropdownMenu />
              </div>
            </MobileGrid> */}

            <SelectOptionField placeholder="Maximum in lakhs per annum" />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
};

InputGridContainer.propTypes = {
  label: PropTypes.string
};

export default InputGridContainer;
