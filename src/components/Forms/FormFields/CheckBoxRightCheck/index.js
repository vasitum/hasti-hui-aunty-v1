import React from "react";

import { Checkbox } from "semantic-ui-react";

import "./index.scss";

const CheckBoxRIghtCheck = props => {
  const { labelTitle, ...restProps } = props;

  return (
    <Checkbox
      className="CheckBoxRIghtCheck"
      label={labelTitle}
      {...restProps}
    />
  );
};

export default CheckBoxRIghtCheck;
