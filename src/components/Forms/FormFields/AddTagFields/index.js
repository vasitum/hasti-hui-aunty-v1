import React from "react";

import { Button, Modal, Icon, Dropdown } from "semantic-ui-react";
// import { Dropdown } from "formsy-semantic-ui-react";
import PropTypes from "prop-types";

import "./index.scss";

export default class AddTagField extends React.Component {
  render() {
    const {
      placeholder,
      onAdd,
      onChange,
      currentValues,
      options,
      name,
      type,
      ...resProps
    } = this.props;
    return (
      <div className="SearchTagField">
        <Dropdown
          {...resProps}
          options={options}
          placeholder={placeholder}
          search
          selection
          fluid
          multiple
          allowAdditions
          value={currentValues}
          onAddItem={onAdd}
          onChange={onChange}
          icon={false}
          name={name}
          type={type}
        />
      </div>
    );
  }
}
