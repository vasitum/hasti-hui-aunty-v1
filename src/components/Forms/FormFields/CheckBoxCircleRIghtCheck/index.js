import React from "react";

import { Checkbox } from "semantic-ui-react";

import "./index.scss";

const CheckBoxCircleRIghtCheck = props => {
  const { labelTitle, ...resProps } = props;

  return (
    <Checkbox
      label={labelTitle}
      className="CheckBoxCircleRIghtCheck"
      {...resProps}
    />
  );
};

export default CheckBoxCircleRIghtCheck;
