import React, { Component } from "react";
import "./index.scss";
import ICRight from "../../../../assets/svg/IcRightCheck";

class CheboxButton extends Component {
  state = {
    isChecked: false
  };

  onCheckBoxChange = val => {
    this.setState({
      isChecked: !this.state.isChecked
    });

    this.props.onChange(val);
  };

  render() {
    const { checkboxTitle, value, boxVal } = this.props;

    return (
      <div
        className={`CheckboxButton ${value === boxVal ? "true" : ""}`}
        onClick={e => this.onCheckBoxChange(boxVal)}>
        <span>{checkboxTitle}</span>

        {this.state.isChecked && (
          <span>
            <ICRight pathcolor="#0b9ed0" />
          </span>
        )}
      </div>
    );
  }
}

export default CheboxButton;
