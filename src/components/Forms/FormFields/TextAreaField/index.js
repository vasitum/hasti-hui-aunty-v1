import React from "react";

import { Form, Input, TextArea } from "semantic-ui-react";
import PropTypes from "prop-types";

import "./index.scss";

const TextAreaField = ({ placeholder, ...props}) => {
  return (
    <TextArea
      placeholder={placeholder}
      className="TextAreaField"
      style={{ color: "#0b9ed0" }}
      {...props}
    />
  );
};

TextAreaField.propTypes = {
  placeholder: PropTypes.string
};

export default TextAreaField;
