import React from "react";

import { Button, Modal, Icon } from "semantic-ui-react";
import { Dropdown, Input } from "formsy-semantic-ui-react";
import PropTypes from "prop-types";

import IcEditIcon from "../../../../assets/svg/IcEdit";
import SkillEditModal from "../../../ModalPageSection/SkillEditModal";
import "./index.scss";

class SearchTagField extends React.Component {
  render() {
    const {
      onTagChange,
      onTagAdd,
      icon,
      name,
      options,
      values,
      required,
      placeholder,
      search,
      ...resProps
    } = this.props;
    return (
      <div className="SearchTagField">
        <Dropdown
          {...resProps}
          options={options}
          placeholder={placeholder}
          search={search}
          selection
          fluid
          multiple
          icon={icon}
          name={name}
          allowAdditions
          value={values}
          onAddItem={onTagAdd}
          onChange={onTagChange}
        />
      </div>
    );
  }
}

SearchTagField.propTypes = {
  placeholder: PropTypes.string,
  search: PropTypes.bool
};

SearchTagField.defaultProps = {
  placeholder: "Type desire label here, eg: designers, developers, manager, hr",
  search: true
};

export default SearchTagField;
