import React from "react";
import { Image } from "semantic-ui-react";
import FileuploadBtn from "../../../Buttons/FileuploadBtn";
import FlatDefaultBtn from "../../../Buttons/FlatDefaultBtn";
import IcUploadIcon from "../../../../assets/svg/IcUpload";

import "./index.scss";

export default ({ imgUrl, onImgChange, onRemoveClick, btnText }) => {
  return !imgUrl ? (
    <React.Fragment>
      <FileuploadBtn
        btnIcon={<IcUploadIcon pathcolor="#c8c8c8" width="11" />}
        btnText={btnText ? btnText : "Upload Logo"}
        onFileUpload={onImgChange}
        accept=".jpg, .jpeg, .png"
      />
      <p
        style={{
          paddingTop: "10px",
          fontSize: "12px"
        }}>
        {/* Supported formats and prefered size dimension: PNG or JPEG by 200x200 */}
        Supported formats PNG and JPEG, Preferred dimensions 200x200.
      </p>
    </React.Fragment>
  ) : (
    <React.Fragment>
      <Image
        style={{ height: "64px", width: "64px" }}
        size="tiny"
        src={imgUrl}
        avatar
      />

      <div>
        <FileuploadBtn
          btnText="Change image"
          accept=".jpg, .jpeg, .png"
          onFileUpload={onImgChange}
          style={{
            display: "inline-block",
            marginRight: "10px",
            marginTop: "15px"
          }}
        />

        <FlatDefaultBtn
          className="bgTranceparent"
          onClick={onRemoveClick}
          btntext="Remove image"
        />
      </div>
    </React.Fragment>
  );
};
