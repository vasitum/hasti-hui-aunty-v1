import React from "react";

// import { Form, Input } from "semantic-ui-react";
import { Radio } from "semantic-ui-react";
import PropTypes from "prop-types";

import "./index.scss";

const RadioButton = props => {
  return <Radio className="RadioButton_default" {...props} />;
};

RadioButton.propTypes = {
  radioLavel: PropTypes.string
};

RadioButton.defaultProps = {
  radioLavel: "text"
};

export default RadioButton;
