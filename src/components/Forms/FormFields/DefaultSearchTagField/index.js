import React from "react";

import { Dropdown } from "semantic-ui-react";

import PropTypes from "prop-types";
import IcDownArrowIcon from "../../../../assets/svg/IcDownArrow";


import "./index.scss";

const options = [
  { key: 'English', text: 'English', value: 'English' },
  { key: 'French', text: 'French', value: 'French' },
  { key: 'Spanish', text: 'Spanish', value: 'Spanish' },
  { key: 'German', text: 'German', value: 'German' },
  { key: 'Chinese', text: 'Chinese', value: 'Chinese' },
]

class DefaultSearchTagField extends React.Component {
  state = { options }

  handleAddition = (e, { value }) => {
    this.setState({
      options: [{ text: value, value }, ...this.state.options],
    })
  }

  handleChange = (e, { value }) => this.setState({ currentValues: value })
  render() {


    const { currentValues } = this.state
    const {
      onTagChange,
      onTagAdd,
      icon,
      name,
      options,
      values,
      required,
      placeholder,
      search,
      
      ...resProps
    } = this.props;
    return (
      <div className="DefaultSearchTagField SelectOptionField">
        <Dropdown
          options={this.state.options}
          placeholder='enter a skill..'
          search
          selection
          fluid
          multiple
          allowAdditions
          icon={false}
          value={currentValues}
          onAddItem={this.handleAddition}
          onChange={this.handleChange}
        />
        {/* <Dropdown
          {...resProps}
          options={options}
          placeholder={placeholder}
          search={search}
          selection
          fluid
          multiple
          icon={false}
          name={name}
          allowAdditions
          value={values}
          onAddItem={onTagAdd}
          onChange={onTagChange}
        /> */}
        <span className="downArrow__icon">
          <IcDownArrowIcon pathcolor="#c8c8c8" />
        </span>
      </div>
    );
  }
}

DefaultSearchTagField.propTypes = {
  placeholder: PropTypes.string,
  search: PropTypes.bool
};

DefaultSearchTagField.defaultProps = {
  placeholder: "Type desire label here, eg: designers, developers, manager, hr",
  search: true
};

export default DefaultSearchTagField;