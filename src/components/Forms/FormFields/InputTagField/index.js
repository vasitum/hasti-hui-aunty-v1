import React from "react";

import { Button, Modal, Icon } from "semantic-ui-react";
import { Dropdown } from "formsy-semantic-ui-react";
import PropTypes from "prop-types";

import IcEditIcon from "../../../../assets/svg/IcEdit";
import SkillEditModal from "../../../ModalPageSection/SkillEditModal";
import FlatDefaultBtn from "../../../Buttons/FlatDefaultBtn";
import autocompleteAPI from "../../../../api/utils/autocomplete";
import escapeRegExp from "lodash.escaperegexp";
import _filter from "lodash.filter";
import _debounce from "lodash.debounce";

import "./index.scss";

class InputTagField extends React.Component {
  constructor(props) {
    super(props);
    this.source = [];
  }

  state = {
    isModalOpen: false,
    source: [],
    showMore: false
  };

  async componentDidMount() {
    try {
      const res = await autocompleteAPI("SKILL");
      if (res.status === 200) {
        const data = await res.json();
        this.source = Array.from(new Set(data)).map(val => {
          return {
            text: val,
            key: val,
            value: val
          };
        });
        this.setState({
          source: this.source
        });
        // console.log("Source Found", this.source, data);
      }
    } catch (error) {
      console.error(error);
    }
  }

  onReset = () => {
    this.setState({
      source: []
    });
  };

  onSearchChange2 = (e, { searchQuery }) => {
    // if (!searchQuery) {
    //   this.setState({
    //     showMore: false
    //   });
    // } else {
    //   this.setState({
    //     showMore: true
    //   });
    // }
  };

  onModalSave = ev => {
    this.setState({
      isModalOpen: !this.state.isModalOpen
    });

    if (this.props.onModalSave) {
      this.props.onModalSave();
    }
  };

  onModalClose = ev => {
    this.setState({
      isModalOpen: !this.state.isModalOpen
    });
  };

  onModalOpen = ev => {
    this.setState({
      isModalOpen: !this.state.isModalOpen
    });
  };

  /**
   * Reset Modal state when there is nothing to show
   */
  componentDidUpdate() {
    const { isModalOpen } = this.state;
    const { currentValues } = this.props;

    if (isModalOpen && currentValues.length === 0) {
      this.onModalClose();
    }
  }

  getOptions() {
    const { options } = this.props;
    const { source, showMore } = this.state;
    return options;

    if (showMore) {
      return [].concat(options, source);
    } else {
      return [].concat(options, source).filter((val, idx) => idx < 6);
    }
  }

  render() {
    const {
      placeholder,
      onAdd,
      onChange,
      currentValues,
      options,
      showEditBtn,
      skillsModel,
      onModalItemChange,
      onModalItemRemove,
      onModalSave,
      profile,
      name,
      required,
      type
    } = this.props;
    const { source } = this.state;

    return (
      <div className="InputTagField">
        <Dropdown
          options={this.getOptions()}
          placeholder={placeholder}
          search
          selection
          fluid
          multiple
          allowAdditions
          lazyLoad
          value={currentValues}
          onAddItem={onAdd}
          onChange={onChange}
          icon={false}
          name={name}
          onSearchChange={_debounce(this.onSearchChange2, 500, {
            leading: true
          })}
          required={profile ? true : required}
          type={type}
        />

        {currentValues && currentValues.length > 0 ? (
          <Modal
            className="modal_form"
            trigger={
              <Button
                type="button"
                compact
                onClick={this.onModalOpen}
                className="skill__btn"
                closeOnDimmerClick={false}>
                <IcEditIcon height="11" width="11" pathcolor="#c8c8c8" />
                <span>Edit Experience</span>
              </Button>
            }
            open={this.state.isModalOpen}
            onClose={this.onModalClose}
            closeIcon>
            <SkillEditModal
              skills={skillsModel}
              onInputChange={onModalItemChange}
              onItemRemove={onModalItemRemove}
              onModalSave={this.onModalSave}
              onModalClose={this.onModalClose}
              isProfile={profile}
            />
          </Modal>
        ) : null}
      </div>
    );
  }
}

InputTagField.propTypes = {
  placeholder: PropTypes.string
};

export default InputTagField;
