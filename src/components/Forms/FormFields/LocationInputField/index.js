import React, { Component } from "react";
import PlacesAutocomplete from "react-places-autocomplete";
import { Input } from "formsy-semantic-ui-react";

import "./index.scss";

class LocationInputField extends Component {
  render() {
    const {
      value,
      onSelect,
      onChange,
      placeholder,
      required,
      validationErrors,
      errorLabel,
      disabled
    } = this.props;

    return (
      <PlacesAutocomplete
        value={value}
        onChange={onChange}
        onSelect={onSelect}
        searchOptions={{
          // https://developers.google.com/places/web-service/autocomplete#location_restrict
          types: ["(regions)"]
        }}>
        {({ getInputProps, suggestions, getSuggestionItemProps }) => (
          <div className="LocationInputField">
            <Input
              {...getInputProps({
                placeholder: placeholder,
                className: "location-search-input",
                required: required,
                validationErrors: validationErrors,
                errorLabel: errorLabel,
                disabled: disabled,
                name: "LocationInputField"
              })}
            />
            <div className="autocomplete-dropdown-container">
              {suggestions.map(suggestion => {
                const className = suggestion.active
                  ? "suggestion-item--active"
                  : "suggestion-item";
                // inline style for demonstration purpose
                const style = suggestion.active
                  ? { backgroundColor: "#fafafa", cursor: "pointer" }
                  : { backgroundColor: "#ffffff", cursor: "pointer" };
                return (
                  <div
                    {...getSuggestionItemProps(suggestion, {
                      className,
                      style
                    })}>
                    <span>{suggestion.description}</span>
                  </div>
                );
              })}
            </div>
          </div>
        )}
      </PlacesAutocomplete>
    );
  }
}

LocationInputField.propTypes = {};

export default LocationInputField;
