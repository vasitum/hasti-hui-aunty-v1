import React from "react";

// import { Form, Input } from "semantic-ui-react";
import { Form, Input } from "formsy-semantic-ui-react";
import PropTypes from "prop-types";

import "./index.scss";

const InputField = props => {
  const { inputtype, ...restProps } = props;

  return (
    <Form.Field
      autoComplete={"off"}
      control={Input}
      type={inputtype}
      placeholder={props.placeholder}
      className="InputField"
      fluid
      {...restProps}
    />
  );
};

InputField.propTypes = {
  placeholder: PropTypes.string,
  inputtype: PropTypes.string
};

InputField.defaultProps = {
  inputtype: "text"
};

export default InputField;
