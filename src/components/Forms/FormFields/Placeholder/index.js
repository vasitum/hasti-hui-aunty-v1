const MOBILE_BREAKPOINT = 768;

const getPlaceholder = (placeholderDesktop, placeholderMobile) => {
  let placeholder = window.innerWidth <= MOBILE_BREAKPOINT ? placeholderDesktop : placeholderMobile
  return placeholder;
}

export default getPlaceholder;

