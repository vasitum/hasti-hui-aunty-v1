import React from "react";

import { Form, Input } from "semantic-ui-react";


// import { Form } from "formsy-semantic-ui-react";

import IcDownArrowIcon from "../../../../assets/svg/IcDownArrow";
import "./index.scss";

// const optionsItem = [
//   { key: 'm', text: 'Male', value: 'male' },
//   { key: 'f', text: 'Female', value: 'female' },
// ]

import PropTypes from "prop-types";

function updateOptionsItem(items, noSelect) {
  if (!items || !Array.isArray(items) || !items.length) {
    return [];
  }

  if (noSelect) {
    return items;
  }

  return [{ text: "Select", value: "" }, ...items];
}

const SelectOptionField = props => {
  const { optionsItem, placeholder, noSelect, ...restProps } = props;
  return (
    <div className="SelectOptionField">
      <Form.Select
        options={updateOptionsItem(optionsItem, noSelect)}
        placeholder={placeholder}
        icon={false}
        fluid
        {...restProps}
      />
      <span className="downArrow__icon">
        <IcDownArrowIcon pathcolor="#c8c8c8" />
      </span>
    </div>
  );
};

SelectOptionField.propTypes = {
  placeholder: PropTypes.string
};

export default SelectOptionField;