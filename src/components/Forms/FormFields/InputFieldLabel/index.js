import React from 'react';

import { Header } from "semantic-ui-react";
import PropTypes from 'prop-types';
import "./index.scss";

const InputFieldLabel = props => {
  const { label, infoicon, headerStyle } = props;
  return(
    <div className="InputFieldLabel">
      <Header as="h3" style={headerStyle}>
        {label}
        {infoicon}
      </Header>
    </div>
  )
}

InputFieldLabel.propTypes = {
  placeholder: PropTypes.string
}

export default InputFieldLabel;