import React from "react";
import { Form } from "formsy-semantic-ui-react";
import { Search } from "semantic-ui-react";
import autocompleteAPI from "../../../../api/utils/autocomplete";
import escapeRegExp from "lodash.escaperegexp";
import _filter from "lodash.filter";
import _debounce from "lodash.debounce";
import "./index.scss";

class AutoCompleteSearch extends React.Component {
  constructor(props) {
    super(props);
  }

  state = {
    value: "",
    loading: false,
    results: []
  };

  async componentDidMount() {
    const { dataType } = this.props;
    try {
      const res = await autocompleteAPI(dataType);
      if (res.status === 200) {
        const data = await res.json();
        this.setState({
          source: Array.from(new Set(data)).map(val => {
            return {
              title: val
            };
          })
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  onReset = () => {
    this.setState({
      loading: false,
      results: [],
      value: ""
    });
  };

  onSearchChange = (e, { value }) => {
    // this.setState({ value: value });
    const { onInputChange, name, idx } = this.props;
    onInputChange(e, { value, name, idx });

    setTimeout(() => {
      if (!this.props.value || this.props.value.length < 1)
        return this.onReset();

      const re = new RegExp(escapeRegExp(this.props.value), "i");
      const isMatch = result => re.test(result.title);

      this.setState({
        loading: false,
        results: _filter(this.state.source, isMatch)
      });
    }, 300);
  };

  onResultSelect = (e, { result }) => {
    const value = result.title;
    const { onInputChange, name, idx } = this.props;
    onInputChange(e, { value, name, idx });
  };

  render() {
    const { props } = this;
    const { results, loading } = this.state;
    const { value, ...resProps } = props;

    return (
      <Form.Field
        as={Search}
        className="AutoCompleteSearch"
        fluid
        results={results.filter((val, idx) => idx < 5)}
        value={value}
        onResultSelect={this.onResultSelect}
        onSearchChange={_debounce(this.onSearchChange, 500, { leading: true })}
        icon={false}
        showNoResults={false}
        resultRenderer={({ title }) => <span>{title}</span>}
        {...resProps}
      />
    );
  }
}

export default AutoCompleteSearch;
