import React from "react";

import { Search, Grid, Header, List } from "semantic-ui-react";
import _ from "lodash";
import PropTypes from "prop-types";

import "./index.scss";

const cities = [
  {
    key: 0,
    value: "New York value",
    usState: "New York"
  },
  {
    key: 1,
    value: "San Francisco",
    usState: "California"
  },
  {
    key: 2,
    value: "Chicago",
    usState: "Illinois"
  }
];

class SearchFilterInput extends React.Component {
  state = {
    isLoading: false,
    results: this.props.results || [],
    value: this.props.value
  };

  componentWillMount() {
    this.resetComponent();
  }

  resetComponent = () =>
    this.setState({ isLoading: false, results: [], value: "" });

  handleSearchChange = (e, { value }) => {
    this.setState({ isLoading: true, value });
    this.props.onChange({
      name: this.props.name,
      value: value
    });

    setTimeout(() => {
      if (this.state.value.length < 1) this.resetComponent();

      const re = new RegExp(_.escapeRegExp(this.state.value), "i");
      const isMatch = result => re.test(result.value);

      this.setState({
        isLoading: false,
        results: _.filter(cities, isMatch)
      });
    }, 500);
  };

  handleResultSelect = (e, { result }) => {
    // this.setState({ value: result.value });
    this.props.onChange({
      name: this.props.name,
      value: result.value
    });
  };

  render() {
    const { isLoading, results } = this.state;

    const { placeholder, value, ...restProps } = this.props;

    return (
      <div className="SearchFilterInput">
        <Grid>
          <Grid.Column width={16}>
            <Search
              type="text"
              icon={false}
              fluid={true}
              loading={isLoading}
              results={results}
              placeholder={placeholder}
              value={value}
              onSearchChange={this.handleSearchChange}
              onResultSelect={this.handleResultSelect}
              resultRenderer={({ value }) => <div>{value}</div>}
              minCharacters={700} // HACK REMOVE IT!
              {...restProps}
            />
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

SearchFilterInput.propTypes = {
  placeholder: PropTypes.string,
  inputtype: PropTypes.string
};

SearchFilterInput.defaultProps = {
  inputtype: "text"
};

export default SearchFilterInput;
