import React from "react";

import { Form, Checkbox } from "semantic-ui-react";
import PropTypes from "prop-types";

const CheckboxField = ({ checkboxLabel, ...props }) => {
  return (
    <Form.Field className={props.className}>
      <Checkbox label={checkboxLabel} {...props} />
    </Form.Field>
  );
};

CheckboxField.propTypes = {
  placeholder: PropTypes.string
};

export default CheckboxField;
