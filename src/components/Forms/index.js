import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";
import { Form } from "semantic-ui-react";

import InputContainer from './InputContainer';

import InputField from './FormFields/InputField';

import SelectOptionField from './FormFields/SelectOptionField';
import TextAreaField from './FormFields/TextAreaField';

import InputTagField from './FormFields/InputTagField';

// import PrimaryFlatBtn from './PostJob/PrimaryFlatBtn';

// import "./index.scss";

const PostJobForm = props => {
  // const { label, children} = props;
  return (
    <div>
      <Form>
        <InputContainer label="Job Title">
          <InputField placeholder="Eg. Required ui designer on urgent basis"/>
        </InputContainer>

        <InputContainer label="Select Option">
          <SelectOptionField placeholder=" on urgent basis"/>
        </InputContainer>

        <InputContainer label="Text Area">
          <TextAreaField placeholder=" on urgent basis"/>
        </InputContainer>

        <InputContainer label="Tag">
          <InputTagField placeholder="Tag"/>
        </InputContainer>
       

        {/* <div>
          <PrimaryFlatBtn btntext="btn"/>
        </div> */}
      </Form>  
    </div>
  );
};


export default PostJobForm;
