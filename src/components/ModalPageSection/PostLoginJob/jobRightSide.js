import React from "react";
import { List, Header, Grid, Button } from "semantic-ui-react";

import { Link } from "react-router-dom";

import QuillText from "../../CardElements/QuillText";

class JobRightSide extends React.Component {
  render() {
    const { compdata, isPreview } = this.props;

    if (compdata) {
      return (
        <div className="publicJobRightSide">
          {/* <div
            style={{
              height: "100px"
            }}
          /> */}

          <List className="aboutCompany">
            <List.Item className="padding-bottom-10">
              <List.Content className="padding-bottom-25">
                <List.Header>
                  <Header as="h3" className="text-pacific-blue title">
                    About the Company
                  </Header>
                </List.Header>
              </List.Content>
              <Grid>
                <Grid.Row>
                  {/* <Grid.Column width={2} className="padding-right-0">
                    <List.Content>
                      <span className="companyLogo">
                        {!compdata || !compdata.imgExt ? (
                          <CompanySvg
                            width="55"
                            height="55"
                            rectcolor="#f7f7fb"
                            pathcolor="#c8c8c8"
                          />
                        ) : (
                          <Image
                            src={`https://s3-us-west-2.amazonaws.com/img-mwf/company/${
                              compdata._id
                            }/image.jpg`}
                            style={{
                              height: "55px",
                              width: "55px"
                            }}
                          />
                        )}
                      </span>
                    </List.Content>
                  </Grid.Column> */}
                  <Grid.Column width={16}>
                    <List.Content>
                      <List className="padding-0">
                        <List.Item className="text-night-rider font-size-16 padding-bottom-16">
                          {compdata.name}
                          {/* compdata.name */}
                        </List.Item>
                        {compdata.headquarter ? (
                          <List.Item
                            style={{
                              display: !compdata.headquarter ? "none" : "block"
                            }}
                            className="text-Matterhorn padding-bottom-5">
                            {compdata.headquarter.location}
                            {/* compdata.headquarter.location */}
                          </List.Item>
                        ) : (
                          <div />
                        )}
                        {compdata.year ? (
                          <List.Item
                            style={{
                              display: !compdata.year ? "none" : "block"
                            }}
                            className="text-trolley-grey padding-bottom-5">
                            Founded:{" "}
                            <span className="text-Matterhorn">
                              {compdata.year}
                            </span>
                          </List.Item>
                        ) : (
                          <div />
                        )}
                        {compdata.size ? (
                          <List.Item
                            style={{
                              display: !compdata.size ? "none" : "block"
                            }}
                            className="text-trolley-grey padding-bottom-5">
                            Employees:{" "}
                            <span className="text-Matterhorn">
                              {compdata.size}
                              {/* compdata.size */}
                            </span>
                          </List.Item>
                        ) : (
                          <div />
                        )}
                        {compdata.website ? (
                          <List.Item
                            style={{
                              display: !compdata.website ? "none" : "block"
                            }}
                            className="text-trolley-grey padding-bottom-5">
                            Website:{" "}
                            <span className="text-Matterhorn">
                              {compdata.website}
                              {/* compdata.website */}
                            </span>
                          </List.Item>
                        ) : (
                          <div />
                        )}
                      </List>
                    </List.Content>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </List.Item>

            <List.Item>
              {compdata.about ? (
                <List.Content className="">
                  <List.Description>
                    <p className="lineHeight-22 text-trolley-grey font-13px">
                      <QuillText
                        readMoreLength={300}
                        placeholder=""
                        value={compdata.about}
                        readOnly
                      />
                      {/* compdata.about */}
                    </p>
                  </List.Description>
                </List.Content>
              ) : (
                <div />
              )}

              <List.Content>
                {!isPreview && (
                  <Button
                    as={Link}
                    to={`/search/job?query=${compdata.name}`}
                    size="tiny"
                    className="text-Matterhorn fontSize-12 b-r-30 btnHoverBlue"
                    style={{ backgroundColor: "#f7f7fb", marginTop: "10px" }}>
                    View more openings in this company
                  </Button>
                )}
              </List.Content>
            </List.Item>
          </List>
        </div>
      );
    } else {
      return <div />;
    }
  }
}

export default JobRightSide;
