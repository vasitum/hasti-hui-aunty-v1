import React from "react";
import { Grid, Container, Image, Button, Modal } from "semantic-ui-react";

import { toast } from "react-toastify";

import JobViews from "./jobViews";
// import JobRightSide from "./jobRightSide";
import public_job_banner from "../../../assets/banners/job_cover_img.jpg";
import BestBanner from "../../../assets/img/searchFilter/bestBanner.png";

import getJobById from "../../../api/jobs/getJobById";

import PageBanner from "../../Banners/PageBanner";

import JobConstants from "../../../constants/storage/jobs";

import EditIcon from "../../../assets/svg/IcEdit";

import EditCoverPage from "../EditCoverPage";

// import getJob from "Api/job/getJob";
import getCompany from "../../../api/company/getCompById";
import updateImgBanner from "../../../api/jobs/updateImgBanner";
import { uploadImage } from "../../../utils/aws";

import PageLoader from "../../PageLoader";

import "./index.scss";

const Loader = props => (
  <div className="m_showbox">
    <div className="m_loader">
      <svg className="m_circular" viewBox="25 25 50 50">
        <circle
          className="m_path"
          cx="50"
          cy="50"
          r="20"
          fill="none"
          stroke-width="2"
          stroke-miterlimit="10"
        />
      </svg>
    </div>
  </div>
);

function hasBanner(data) {
  const res = {};

  if (data && data.imgBanner) {
    (res["extn"] = "jpg"),
      (res["file"] = `https://s3-us-west-2.amazonaws.com/img-mwf/banner/${
        data._id
      }/image.png`);
  } else {
    (res["extn"] = ""), (res["file"] = "");
  }

  return res;
}

class PublicJob extends React.Component {
  state = {
    isLoading: true,
    jobdata: {},
    compData: null,
    isModalOpen: false,
    bannerImageExtn: "",
    bannerImageFile: "",
    bannerImageUpdateFile: "",
    isPageLoading: false,
    pageLoaderText: "Loading Job ..."
  };

  constructor(props) {
    super(props);

    this.onBannerRemove = this.onBannerRemove.bind(this);
    this.onBannerSave = this.onBannerSave.bind(this);
  }

  toggleBanner = bannermsg => {
    this.setState({
      isPageLoading: !this.state.isPageLoading,
      pageLoaderText: bannermsg || "Loading Job ..."
    });
  };

  onBannerChange = ev => {
    const file = ev.target.files[0];

    // reset input
    ev.target.value = "";

    if (!file) {
      return;
    }

    const fr = new FileReader();

    fr.onloadend = e => {
      this.setState({
        bannerImageFile: e.target.result,
        bannerImageUpdateFile: file
      });
    };

    fr.readAsDataURL(file);
  };

  async onBannerRemove(e) {
    this.setState(
      {
        bannerImageExtn: "",
        bannerImageFile: "",
        bannerImageUpdateFile: "",
        isModalOpen: false
      },
      async () => {
        try {
          const res = await updateImgBanner(this.state.jobdata._id, "");
          if (res.status !== 200) {
            toast("unable to remove banner");
          }

          toast("Banner Removed!");
        } catch (e) {
          console.log(e);
          toast("unable to remove banner!");
        }
      }
    );
  }

  async onBannerSave(e) {
    // set form loading
    this.toggleBanner("Updating Banner ...");
    try {
      const res = await uploadImage(
        `banner/${this.state.jobdata._id}/image.png`,
        this.state.bannerImageUpdateFile
      );

      const data = await updateImgBanner(this.state.jobdata._id, "jpg");

      if (res && data.status === 200) {
        toast("Banner Updated!");
        window.location.reload();
      }
    } catch (E) {
      this.toggleBanner();
      console.log(E);
    }
  }

  onBannerModalOpen = e => {
    this.setState({
      isModalOpen: true
    });
  };

  onBannerModalClose = e => {
    this.setState({
      bannerImageExtn: "",
      bannerImageFile: "",
      bannerImageUpdateFile: "",
      isModalOpen: false
    });
  };

  async componentDidMount() {
    const { match } = this.props;
    try {
      const res = await getJobById(match.params.id);
      const jobDataJson = await res.json();
      this.setState({
        isLoading: false,
        jobdata: jobDataJson
      });

      // console.log(jobDataJson);
      window.localStorage.setItem(
        JobConstants.JOB_EDIT_DATA,
        JSON.stringify(jobDataJson)
      );

      const banner = hasBanner(this.state.jobdata);

      this.setState({
        bannerImageExtn: banner.extn,
        bannerImageFile: banner.file
      });

      const resComp = await getCompany(jobDataJson.com.id);
      const resCompJson = await resComp.json();
      this.setState({
        compData: resCompJson
      });

      console.log(resCompJson);
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    if (this.state.isLoading) {
      return (
        <Container>
          <Loader />
        </Container>
      );
    } else {
      return (
        <div className="publicJobViews">
          <div className="publicJobBanner">
            <PageBanner
              size="medium"
              image={
                this.state.bannerImageFile
                  ? this.state.bannerImageFile
                  : public_job_banner
              }
              jobHack={this.state.bannerImageFile ? true : false}>
              {/* <Container>
                <BannerHeader onFileChange={this.onFileChange} />
              </Container> */}

              <Modal
                className="modal_form"
                open={this.state.isModalOpen}
                trigger={
                  <Button
                    onClick={this.onBannerModalOpen}
                    className="banner_editBtn">
                    <EditIcon pathcolor="#c8c8c8" />
                  </Button>
                }
                onClose={this.onBannerModalClose}
                closeIcon
                closeOnDimmerClick={false}>
                <EditCoverPage
                  bannerFile={
                    this.state.bannerImageFile
                      ? this.state.bannerImageFile
                      : public_job_banner
                  }
                  onBannerChange={this.onBannerChange}
                  onBannerRemove={this.onBannerRemove}
                  onBannerSave={this.onBannerSave}
                />
              </Modal>
            </PageBanner>
          </div>
          <div className="publicJobDetails">
            <Container>
              <Grid>
                <Grid.Row>
                  <Grid.Column mobile={16} tablet={16} computer={13}>
                    <JobViews
                      comdata={this.state.compData}
                      data={this.state.jobdata}
                    />
                  </Grid.Column>
                  <Grid.Column width={5} className="tablet mobile hidden">
                    {/* <JobRightSide compdata={this.state.compData} /> */}
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Container>
          </div>
          <PageLoader
            active={this.state.isPageLoading}
            loaderText={this.state.pageLoaderText}
          />
        </div>
      );
    }
  }
}

export default PublicJob;
