import React from "react";
import {
  List,
  Header,
  Button,
  Icon,
  Image,
  Grid,
  Card,
  Dropdown,
  Modal
} from "semantic-ui-react";
import wipro from "../../../assets/img/wipro.png";
import airtel from "../../../assets/img/airtel.png";
import SocialBtn from "../Buttons/socialBtn";
import CompanySvg from "../../../assets/svg/IcCompany";
import LocationLogo from "../../../assets/svg/IcLocation";

import JobDescription from "../commonComponent/jobDescription";
import JobEditModal from "../../ModalPageSection/EditJob";

import SkillsComponent from "../commonComponent/skills";

import MobileSocialBtn from "../moblieComponents/mobileSocialBtn";

import MoreOptionIcon from "../../../assets/svg/IcMoreOptionIcon";

import InfoSectionGridHeader from "../PreProfile/InfoSectionGridHeader";
import QuillText from "../../CardElements/QuillText";

import IcEditIcon from "../../../assets/svg/IcEdit";

import JobRightSide from "./jobRightSide";

import ShareButton from "../../Buttons/ShareButton";

import fromNow from "../../../utils/env/fromNow";
import Currency from "../../CardElements/Currency";
import { Link } from "react-router-dom";

// import "./index.scss";

const editIcon = <IcEditIcon pathcolor="#c8c8c8" />;

const trigger = (
  <span>
    <MoreOptionIcon
      width="5"
      height="18.406"
      pathcolor="#ffffff"
      viewbox="0 0 5 18.406"
    />
  </span>
);

const options = [
  { key: "Like", text: "Like" },
  { key: "Share", text: "Share" },
  { key: "Save", text: "Save" }
];

const processSkills = skills => {
  if (!skills) {
    return null;
  }

  const skillsName = skills.map(val => {
    if (val.reqType !== "optional") {
      return {
        name: val.name,
        exp: val.exp
      };
    }
  });

  const skillComponent = skillsName.map(val => {
    if (!val) {
      return;
    }

    return (
      <div className="filterSkill skillBtn" style={{ display: "inline-block" }}>
        <List horizontal className="text-night-rider">
          <List.Item key={val.name}>
            <Button compact>
              {val.exp ? (
                <React.Fragment>
                  {val.name} <span style={{ color: "#a1a1a1" }}>|</span>{" "}
                  <span> {val.exp} yrs</span>
                </React.Fragment>
              ) : (
                <React.Fragment>{val.name}</React.Fragment>
              )}
            </Button>
          </List.Item>
        </List>
      </div>
    );
  });

  return skillComponent.filter(val => val);
};

const processSkillsOptional = skills => {
  if (!skills) {
    return null;
  }

  const skillsName = skills.map(val => {
    if (val.reqType === "optional") {
      return {
        name: val.name,
        exp: val.exp
      };
    }
  });

  const skillComponent = skillsName.map(val => {
    if (!val) {
      return;
    }

    return (
      <div className="filterSkill skillBtn" style={{ display: "inline-block" }}>
        <List horizontal className="text-night-rider">
          <List.Item key={val.name}>
            <Button compact>
              {val.exp ? (
                <React.Fragment>
                  {val.name} |<span> {val.exp} yrs</span>
                </React.Fragment>
              ) : (
                <React.Fragment>{val.name}</React.Fragment>
              )}
            </Button>
          </List.Item>
        </List>
      </div>
    );
  });

  return skillComponent.filter(val => val);
};

const processCertificate = skills => {
  if (!skills) {
    return null;
  }

  const skillsName = skills.map(val => {
    if (val.reqType === "optional") {
      return {
        name: val.name,
        exp: val.exp
      };
    }
  });

  const skillComponent = skillsName.map(val => {
    if (!val) {
      return;
    }

    return (
      <div className="filterSkill skillBtn" style={{ display: "inline-block" }}>
        <List bulleted horizontal>
          <List.Item key={val.name}>
            {val.exp ? (
              <React.Fragment>{val.name}</React.Fragment>
            ) : (
              <React.Fragment>{val.name}</React.Fragment>
            )}
          </List.Item>
        </List>
      </div>
    );
  });

  return skillComponent;
};

const processEducation = education => {
  if (!education) {
    return (
      <p
        style={{
          color: "#a1a1a1"
        }}>
        Describe the education qualification for your job
      </p>
    );
  }

  const eduArry = education.map(val => {
    return {
      // course: val.course,
      // spec: val.specs[0],
      level: val.level,
      type: "Full Time"
    };
  });

  const eduArrayComponent = eduArry.map(val => {
    if (!val || !val.level) {
      return (
        <p
          style={{
            color: "#c8c8c8"
          }}>
          Describe the education qualification for your job
        </p>
      );
    }

    return (
      <p className="margin-right-10">
        <span className="padding-right-5">{val.level}</span>
      </p>
    );
  });

  return eduArrayComponent;
};

const processDisablityOptions = other => {
  if (!other || !Array.isArray(other.disFriendlyTags)) {
    return null;
  }

  const tags = other.disFriendlyTags;

  return tags.map(val => {
    return <List.Item key={val}>{val}</List.Item>;
  });
};

class JobViews extends React.Component {
  state = {
    isEditModalOpen: false,
    jobdata: {}
  };

  editIcon = <IcEditIcon pathcolor="#c8c8c8" />;

  onEditIconClick = ev => {
    this.setState({
      isEditModalOpen: !this.state.isEditModalOpen
    });
  };

  render() {
    const { data, comdata } = this.props;
    const processMandSkills = processSkills(data.skills);
    const processOptionalSkills = processSkillsOptional(data.skills);

    // console.log(processOptionalSkills);

    return (
      <React.Fragment>
        <div className="leftSideJobDetails mobile__publicJob">
          <List className="jobDetailsHeader">
            <List.Item>
              <Grid>
                <Grid.Row className="mobileAlingCenter">
                  <Grid.Column mobile={16} computer={12}>
                    <List.Content className="headerLeftSide">
                      <List.Header>
                        <Header as="h1">{data.title}</Header>
                        <span
                          className="vasitumFontOpen-500 text-white"
                          style={{
                            display: "flex",
                            alignItems: "center"
                          }}>
                          <LocationLogo
                            width="9.5"
                            height="11.5"
                            pathcolor="#0B9ED0"
                          />
                          <span className="padding-left-5">
                            {data.loc
                              ? data.loc.city
                              : "Noida, Uttar Pradesh, India"}
                          </span>
                        </span>
                      </List.Header>
                    </List.Content>
                  </Grid.Column>
                  <Grid.Column mobile={16} computer={4}>
                    <List.Content className="headerRightSide">
                      {/* only Desktop */}
                      {/* <Button
                        size="mini"
                        className="margin-right-10 mobile hidden bgWhiteSmallBtn">
                        Save Job
                      </Button> */}
                      {/* only Desktop end*/}

                      {/* <Button
                        size="mini"
                        className="actionBtn moblile__actionBtn">
                        Apply
                      </Button> */}

                      {/* only mobile */}
                      {/* <span className="mobile__moreOption widescreen hidden large screen hidden computer hidden tablet hidden">
                        <Dropdown
                          trigger={trigger}
                          options={options}
                          pointing="top right"
                          icon={null}
                        />
                      </span> */}
                      {/* only mobile */}
                    </List.Content>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </List.Item>
          </List>

          <Card className="primaryCard" fluid>
            <div className="jobProfileCard">
              <List verticalAlign="middle" style={{ marginBottom: "2rem" }}>
                <List.Item>
                  <Grid>
                    <Grid.Row className="mobileAlingCenter">
                      <Grid.Column mobile={16} computer={2}>
                        <List.Content>
                          <span className="companyLogo">
                            {!comdata || !comdata.imgExt ? (
                              <CompanySvg
                                width="64"
                                height="64"
                                rectcolor="#f7f7fb"
                                pathcolor="#c8c8c8"
                              />
                            ) : (
                              <Image
                                src={`https://s3-us-west-2.amazonaws.com/img-mwf/company/${
                                  comdata._id
                                }/image.jpg`}
                                style={{
                                  height: 64,
                                  width: 64
                                }}
                                alt={data.com.name}
                              />
                            )}
                          </span>
                        </List.Content>
                      </Grid.Column>
                      <Grid.Column
                        mobile={16}
                        computer={9}
                        className="padding-left-2">
                        <List.Content className="profileCardHeader">
                          <List.Header>
                            <Header
                              className="headerTitle padding-bottom-5"
                              as="h3">
                              {data.com ? data.com.name : ""}
                              {/* data.com.name */}
                            </Header>
                            <p className="headerSubTitle padding-bottom-5">
                              Job Role:{" "}
                              <span className="headerSubLavel">
                                {data.role}
                                {/* data.role */}
                              </span>
                            </p>
                          </List.Header>
                          <List.Description className="experience">
                            <p className="margin-bottom-0 headerSubTitle">
                              <span
                                className="publicJob__HeaderExp"
                                style={{
                                  display:
                                    !data.exp || !data.exp.min || !data.exp.max
                                      ? "none"
                                      : "inline"
                                }}>
                                Experience: {/* only Desktop */}
                                <span className="headerSubLavel mobile hidden">
                                  {" "}
                                  {data.exp.min} - {data.exp.max} years
                                  {/* data.exp.max yrs */}
                                </span>
                                {/* only Desktop end*/}
                                {/* only mobile */}
                                <span className="headerSubLavel widescreen hidden large screen hidden computer hidden tablet hidden">
                                  {" "}
                                  {data.exp.min} - {data.exp.max}
                                  year
                                </span>
                                {/* only mobile end*/}
                              </span>

                              <span className="publicJob__HeaderExp">
                                Job type:{" "}
                                <span className="headerSubLavel">
                                  {" "}
                                  {data.jobType}{" "}
                                </span>
                              </span>

                              <span
                                className="publicJob__HeaderExp"
                                style={{
                                  display:
                                    !data.salary.min && !data.salary.max
                                      ? "none"
                                      : "block"
                                }}>
                                Salary:{" "}
                                <span className="headerSubLavel">
                                  {" "}
                                  <Currency data={data} />
                                  {data.salary.min} - {data.salary.max}{" "}
                                  {data.salary
                                    ? data.salary.currency &&
                                      data.salary.currency !== "INR"
                                      ? "k per annum"
                                      : " LPA"
                                    : ""}
                                </span>
                              </span>
                            </p>
                          </List.Description>
                        </List.Content>
                      </Grid.Column>
                      <Grid.Column mobile={16} computer={5}>
                        <List.Content className="mobileAlingCenter alingRight">
                          <List.Header className="vasitumFontOpen-500  padding-right-10">
                            <span
                              style={{
                                fontSize: "12px"
                              }}
                              className="text-Matterhorn">
                              Posted:{" "}
                              <span className="textPersianRed">
                                {fromNow(data.cTime)}
                              </span>{" "}
                            </span>
                          </List.Header>

                          <List.Description className="padding-top-10 mobile hidden">
                            {/* <Button
                            size="mini"
                            className="btnHoverBlue primaryMiniBtn margin-right-10">
                            Like
                          </Button> */}
                            <Button
                              as={Link}
                              to={`/job/public/${data._id}`}
                              size="mini"
                              className="primaryMiniBtn btnHoverBlue">
                              Public view
                            </Button>

                            <ShareButton
                              trigger={
                                <Button
                                  size="mini"
                                  className="primaryMiniBtn btnHoverBlue">
                                  Share job
                                </Button>
                              }
                            />
                          </List.Description>
                        </List.Content>
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                </List.Item>
              </List>

              <div className="divider-hr" />

              <List verticalAlign="middle" className="profileCardBody">
                <Grid>
                  <Grid.Row>
                    <Grid.Column width={2} className="mobile hidden" />
                    <Grid.Column
                      width={14}
                      className="padding-left-2 publicjob__body">
                      <JobDescription
                        onEditIconClick={this.onEditIconClick}
                        jsonData={data.desc}
                      />

                      <div className="jobDuties">
                        <InfoSectionGridHeader
                          onEditIconClick={this.onEditIconClick}
                          headerText="Job Duties"
                          headerClasses="jobDuties_Title"
                          editIcon={editIcon}>
                          <QuillText value={data.respAndDuty} readOnly />
                        </InfoSectionGridHeader>
                      </div>

                      <div className="Benefites">
                        <InfoSectionGridHeader
                          onEditIconClick={this.onEditIconClick}
                          headerText="Benefits"
                          headerClasses="benefites_Title"
                          editIcon={editIcon}>
                          <QuillText value={data.benefits} readOnly />
                        </InfoSectionGridHeader>
                      </div>

                      <List.Item
                        style={{
                          display: !data.skills ? "none" : "block"
                        }}
                        className="padding-bottom-20">
                        <List.Content>
                          <InfoSectionGridHeader
                            headerText="Skills Requirement"
                            editIcon={this.editIcon}
                            containerClass="skillsRequirement_profile"
                            onEditIconClick={this.onEditIconClick}>
                            <div className="mandatory_skill">
                              <p
                                style={{
                                  display: processMandSkills.length
                                    ? "block"
                                    : "none"
                                }}>
                                Mandatory
                              </p>
                              {processMandSkills}
                            </div>
                            <div className="optional_skill">
                              <p
                                style={{
                                  display: processOptionalSkills.length
                                    ? "block"
                                    : "none"
                                }}>
                                Optional
                              </p>
                              {processOptionalSkills}
                            </div>
                          </InfoSectionGridHeader>
                        </List.Content>
                      </List.Item>

                      <List.Item
                        className="padding-bottom-20 education_requirement"
                        style={{
                          display: !data.edus ? "none" : "block"
                        }}>
                        <List.Content>
                          <InfoSectionGridHeader
                            headerText=" Educational qualification"
                            containerClass="educationRequirement_profile"
                            editIcon={this.editIcon}
                            onEditIconClick={this.onEditIconClick}>
                            {processEducation(data.edus)}
                          </InfoSectionGridHeader>
                        </List.Content>
                      </List.Item>

                      {/* <List.Item
                        className="padding-bottom-20 education_requirement"
                        style={{
                          display: !data.edus ? "none" : "block"
                        }}>
                        <List.Content>
                          
                        </List.Content>
                      </List.Item> */}

                      <div className="otherRequirement">
                        <InfoSectionGridHeader
                          onEditIconClick={this.onEditIconClick}
                          headerText="Other Requirement"
                          headerClasses="benefites_Title"
                          editIcon={editIcon}>
                          <QuillText value={data.other.dec} readOnly />
                        </InfoSectionGridHeader>
                      </div>

                      <div
                        className="padding-bottom-20 education_requirement filterSkill"
                        style={{
                          display:
                            !data.other ||
                            !data.other.disFriendlyTags ||
                            !data.other.disFriendlyTags.length
                              ? "none"
                              : "block"
                        }}>
                        <InfoSectionGridHeader
                          headerText="Disabled Friendly Job"
                          containerClass="educationRequirement_profile"
                          editIcon={this.editIcon}
                          onEditIconClick={this.onEditIconClick}>
                          {/* {processCertificate(data.skills)} */}
                          <List bulleted horizontal>
                            {processDisablityOptions(data.other)}
                          </List>
                        </InfoSectionGridHeader>
                      </div>

                      {/* <List bulleted horizontal>
                        <List.Item as="a">About Us</List.Item>
                        <List.Item as="a">Sitemap</List.Item>
                        <List.Item as="a">Contact</List.Item>
                      </List> */}

                      <List.Item>
                        <List.Content className="alingRight mobileAlingCenter">
                          <List.Header>
                            {/* only Desktop */}
                            <span className="mobile hidden">
                              {/* <SocialBtn />
                            <Button size="mini" className="actionBtn">
                              Apply <Icon name="angle right" />
                            </Button> */}
                            </span>
                            {/* only Desktop end*/}

                            {/* only mobile */}
                            <MobileSocialBtn
                              btntext="Apply"
                              icon={<Icon name="angle right" />}
                            />
                            {/* only mobile end */}
                          </List.Header>
                        </List.Content>
                      </List.Item>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </List>
            </div>

            <div className="divider-hr" />

            <div className="aboutCompany_body">
              <Grid>
                <Grid.Row>
                  <Grid.Column width={2} />
                  <Grid.Column width={14}>
                    <JobRightSide compdata={comdata} />
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </div>
          </Card>

          <div className="public-similar-job" style={{ display: "none" }}>
            <List>
              <List.Item className="similar-job-header">
                <List.Content>
                  <List.Header className="padding-bottom-20">
                    <Header as="h3" className="text-night-rider font-w-500">
                      Similar jobs
                    </Header>
                  </List.Header>
                </List.Content>
              </List.Item>

              <List.Item className="similar-job-body">
                <List.Content floated="left">
                  <Image src={airtel} />
                </List.Content>

                <List.Content floated="left">
                  <List.Header>
                    <Header as="h4" className="vasitumFontOpen-600">
                      Core java developer
                    </Header>
                  </List.Header>
                  <List.Description>
                    <List>
                      <List.Item className="text-night-rider padding-bottom-10">
                        <Icon name="marker" className="text-pacific-blue" />{" "}
                        Bharti Airtel
                      </List.Item>
                      <List.Item>
                        <List.Content className="text-Matterhorn">
                          <Icon name="suitcase" className="text-tiffany-blue" />
                          Pune, India{" "}
                          <span className="padding-left-5 text-night-rider">
                            3{" "}
                          </span>{" "}
                          yrs exp
                        </List.Content>
                        <List.Content className="padding-top-20 filterSkill">
                          <List
                            bulleted
                            horizontal
                            className="text-night-rider">
                            <List.Item>
                              <span className="text-dark-gray padding-right-5">
                                Skills:{" "}
                              </span>{" "}
                              HTML
                            </List.Item>
                            <List.Item>CSS</List.Item>
                            <List.Item>Node js</List.Item>
                            <List.Item>Ruby</List.Item>
                          </List>
                        </List.Content>
                      </List.Item>
                    </List>
                  </List.Description>
                </List.Content>
                <List.Content floated="right">
                  <List.Header>
                    <Button
                      size="mini"
                      className="bg-transparent socialBtn b-r-30 fontSize-12">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="17"
                        height="11"
                        viewBox="0 0 17 16">
                        <defs>
                          <filter
                            id="filter"
                            x="1111.03"
                            y="353"
                            width="17"
                            height="16"
                            filterUnits="userSpaceOnUse">
                            <feFlood result="flood" floodColor="#c8c8c8" />
                            <feComposite
                              result="composite"
                              operator="in"
                              in2="SourceGraphic"
                            />
                            <feBlend result="blend" in2="SourceGraphic" />
                          </filter>
                        </defs>
                        <g fill="#c8c8c8">
                          <path
                            className="cls-1"
                            d="M1128,357.77c-0.24-2.766-2.18-4.773-4.6-4.773a4.582,4.582,0,0,0-3.92,2.286,4.383,4.383,0,0,0-3.82-2.286c-2.42,0-4.35,2.007-4.6,4.772a5.03,5.03,0,0,0,.14,1.814,7.846,7.846,0,0,0,2.36,3.977l5.92,5.442,6.03-5.442a7.828,7.828,0,0,0,2.35-3.977A5.025,5.025,0,0,0,1128,357.77Z"
                            transform="translate(-1111.03 -353)"
                          />
                        </g>
                      </svg>
                      <span className="padding-left-3">Like</span>
                    </Button>
                    <Button
                      size="mini"
                      className="bg-transparent socialBtn fontSize-12 text-Matterhorn">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="15.07"
                        height="11"
                        viewBox="0 0 15.07 17.156">
                        <defs>
                          <filter
                            id="filter"
                            x="1485.06"
                            y="351.844"
                            width="15.07"
                            height="17.156"
                            filterUnits="userSpaceOnUse">
                            <feFlood result="flood" floodColor="#ceeefe" />
                            <feComposite
                              result="composite"
                              operator="in"
                              in2="SourceGraphic"
                            />
                            <feBlend result="blend" in2="SourceGraphic" />
                          </filter>
                        </defs>
                        <g fill="#c8c8c8">
                          <path
                            id="Shape_39_copy_3"
                            data-name="Shape 39 copy 3"
                            className="cls-1"
                            d="M1497.61,363.961a2.379,2.379,0,0,0-1.64.661l-5.97-3.574a2.316,2.316,0,0,0,0-1.207l5.9-3.541a2.474,2.474,0,0,0,1.71.7,2.585,2.585,0,1,0-2.51-2.585,3.022,3.022,0,0,0,.07.6l-5.9,3.543a2.442,2.442,0,0,0-1.7-.7,2.585,2.585,0,0,0,0,5.167,2.45,2.45,0,0,0,1.7-.7l5.96,3.582a2.652,2.652,0,0,0-.07.562A2.446,2.446,0,1,0,1497.61,363.961Z"
                            transform="translate(-1485.06 -351.844)"
                          />
                        </g>
                      </svg>

                      <span className="padding-left-3">Share</span>
                    </Button>

                    <Button
                      size="mini"
                      className="bg-transparent  socialBtn fontSize-12 text-Matterhorn">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="17"
                        height="11"
                        viewBox="0 0 17 15">
                        <defs>
                          <filter
                            id="filter"
                            x="920"
                            y="507"
                            width="17"
                            height="15"
                            filterUnits="userSpaceOnUse">
                            <feFlood result="flood" floodColor="#c8c8c8" />
                            <feComposite
                              result="composite"
                              operator="in"
                              in2="SourceGraphic"
                            />
                            <feBlend result="blend" in2="SourceGraphic" />
                          </filter>
                        </defs>
                        <g fill="#c8c8c8">
                          <path
                            className="cls-1"
                            d="M935.91,519.666h0l-0.3,1.452a1.1,1.1,0,0,1-1.081.882h-12.8a1.1,1.1,0,0,1-1.081-1.324l1.231-6.03a1.165,1.165,0,0,1,.784-0.874h0c0.028-.009.057-0.017,0.085-0.024h0c0.027-.007.054-0.012,0.081-0.017l0.027,0,0.064-.008c0.031,0,.063,0,0.1,0h13.421a0.555,0.555,0,0,1,.555.538,0.612,0.612,0,0,1-.011.127Zm-14.868-5.191a2.031,2.031,0,0,1,1.625-1.586v-2.363h-1.521A1.147,1.147,0,0,0,920,511.671v7.909Zm12.588-6.993v5.375h-10.1v-5.375a0.482,0.482,0,0,1,.481-0.482h9.141A0.482,0.482,0,0,1,933.63,507.482ZM931.988,510a0.894,0.894,0,0,0-.448-0.98h-6.08a1.3,1.3,0,0,0,0,1.961h6.08A0.9,0.9,0,0,0,931.988,510Z"
                            transform="translate(-920 -507)"
                          />
                        </g>
                      </svg>

                      <span className="padding-left-3">Save</span>
                    </Button>
                    <span className="text-Matterhorn vasitumFontOpen-500 fontSize-12">
                      Posted: <span className="text-pacific-blue">Today</span>
                    </span>
                  </List.Header>
                  <List.Description className="text-aling-right padding-top-15">
                    <Button
                      size="mini"
                      className="LightCyanBgBlue btnWhite btnWhite fontSize-12 b-r-30 actionBtn">
                      Apply <Icon name="angle right" />
                    </Button>
                  </List.Description>
                </List.Content>
              </List.Item>

              <List.Item className="similar-job-body">
                <List.Content floated="left">
                  <Image src={airtel} />
                </List.Content>

                <List.Content floated="left">
                  <List.Header>
                    <Header as="h4" className="vasitumFontOpen-600">
                      Core java developer
                    </Header>
                  </List.Header>
                  <List.Description>
                    <List>
                      <List.Item className="text-night-rider padding-bottom-10">
                        <Icon name="marker" className="text-pacific-blue" />{" "}
                        Bharti Airtel
                      </List.Item>
                      <List.Item>
                        <List.Content className="text-Matterhorn">
                          <Icon name="suitcase" className="text-tiffany-blue" />
                          Pune, India{" "}
                          <span className="padding-left-5 text-night-rider">
                            3{" "}
                          </span>{" "}
                          yrs exp
                        </List.Content>
                        <List.Content className="padding-top-20 filterSkill">
                          <List
                            bulleted
                            horizontal
                            className="text-night-rider">
                            <List.Item>
                              <span className="text-dark-gray padding-right-5">
                                Skills:{" "}
                              </span>{" "}
                              HTML
                            </List.Item>
                            <List.Item>CSS</List.Item>
                            <List.Item>Node js</List.Item>
                            <List.Item>Ruby</List.Item>
                          </List>
                        </List.Content>
                      </List.Item>
                    </List>
                  </List.Description>
                </List.Content>
                <List.Content floated="right">
                  <List.Header>
                    <Button
                      size="mini"
                      className="bg-transparent compact fontSize-12 text-Matterhorn">
                      <Icon
                        name="share alternate"
                        className="text-very-light-grey"
                      />
                      Share
                    </Button>
                    <Button
                      size="mini"
                      className="bg-transparent fontSize-12 text-Matterhorn">
                      <Icon name="like" className="text-very-light-grey" />
                      Save
                    </Button>
                    <span className="text-Matterhorn vasitumFontOpen-500 fontSize-12">
                      Posted: <span className="text-pacific-blue">Today</span>
                    </span>
                  </List.Header>
                  <List.Description className="text-aling-right padding-top-15">
                    <Button
                      size="mini"
                      className="LightCyanBgBlue btnWhite fontSize-12 b-r-30">
                      Apply <Icon name="angle right" />
                    </Button>
                  </List.Description>
                </List.Content>
              </List.Item>
            </List>
          </div>
        </div>
        <Modal
          className="modal_form"
          onClose={this.onEditIconClick}
          open={this.state.isEditModalOpen}
          closeIcon
          closeOnDimmerClick={false}>
          <JobEditModal onCloseClick={this.onEditIconClick} />
        </Modal>
      </React.Fragment>
    );
  }
}

export default JobViews;
