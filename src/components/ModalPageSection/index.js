import React from "react";
import { Container, Grid, Modal, Button } from "semantic-ui-react";

import SkillEditModal from "./SkillEditModal";
import CreateCompanyPage from "../CreateCompanyPage";
import EditJob from "./EditJob";
import UserAccessModal from "./UserAccessModal";

import EditProfilePage from "../EditProfilePage";
import EditCompany from "./EditCompany";
import EditCoverPage from "./EditCoverPage";
// import "./index.scss";
// import "./style/index.scss";

class ModalPageSection extends React.Component {
  render() {
    return (
      <Container>
        <Grid>
          <Grid.Row>
            <Grid.Column width={4}>
              <Modal
                className="modal_form"
                trigger={<Button>Skill Edit Modal</Button>}
                closeIcon
                closeOnDimmerClick={false}>
                <SkillEditModal />
              </Modal>
            </Grid.Column>
            <Grid.Column width={4}>
              <Modal
                className="modal_form"
                trigger={<Button>Edit Job</Button>}
                closeIcon
                closeOnDimmerClick={false}>
                <EditJob />
              </Modal>
            </Grid.Column>
            <Grid.Column width={4}>
              <Modal
                className="modal_form"
                trigger={<Button>Create Company Page</Button>}
                closeIcon
                closeOnDimmerClick={false}>
                <CreateCompanyPage />
              </Modal>
            </Grid.Column>
            <Grid.Column width={4}>
              <UserAccessModal />
            </Grid.Column>
          </Grid.Row>

          <Grid.Row>
            <Grid.Column width={4}>
              <Modal
                className="modal_form"
                trigger={<Button>EditProfilePage</Button>}
                closeIcon
                closeOnDimmerClick={false}>
                <EditProfilePage />
              </Modal>
            </Grid.Column>

            <Grid.Column width={4}>
              <Modal
                className="modal_form"
                trigger={<Button>EditCoverPage</Button>}
                closeIcon
                closeOnDimmerClick={false}>
                <EditCoverPage />
              </Modal>
            </Grid.Column>

            <Grid.Column width={4}>
              <Modal
                className="modal_form"
                trigger={<Button>Edit Company Page</Button>}
                closeIcon
                closeOnDimmerClick={false}>
                <EditCompany />
              </Modal>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    );
  }
}

export default ModalPageSection;
