import React, { PureComponent } from "react";
import { connectStats } from "react-instantsearch/connectors";
import { connect } from "react-redux";
import { onCountJobChange } from "../../../actions/search";

class UpdateJobCounter extends PureComponent {
  jobCounter = 0;

  componentDidMount() {
    this.props.onJobChange({
      job: this.props.nbHits
    });
  }

  UNSAFE_componentWillReceiveProps(nextProps, nextState) {
    if (window.location.pathname.split("/").pop() === "job") {
      if (this.jobCounter !== Number(nextProps.nbHits)) {
        // update job counter
        this.jobCounter = nextProps.nbHits;

        // update upper counter
        this.props.onJobChange({
          job: nextProps.nbHits
        });
      }
    }
  }

  render() {
    return null;
  }
}

const mapStateToProps = state => {
  return {
    search: state.search.searchCount
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onJobChange: data => {
      dispatch(onCountJobChange(data));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(connectStats(UpdateJobCounter));
