import React from "react";
import {
  Image,
  Header,
  List,
  Icon,
  Button,
  Dropdown,
  Grid
} from "semantic-ui-react";

import { connectStats } from "react-instantsearch/connectors";
import { withRouter } from "react-router-dom";

import isLoggedIn from "../../../utils/env/isLoggedin";

import { onCountJobChange } from "../../../actions/search";

import { connect } from "react-redux";
import { SEARCH_PAGE } from "../../../strings";
import UserAccessModal from "../UserAccessModal";
import qs from "qs";

class BestFoundJobBanner extends React.Component {
  render() {
    const { nbHits, isHidden } = this.props;
    const qsParsed = qs.parse(this.props.location.search);
    // console.log(qsParsed);

    return (
      <List
        verticalAlign="middle"
        style={{ display: isHidden ? "none" : "block" }}>
        <List.Item>
          <Grid>
            <Grid.Row>
              <Grid.Column mobile={16} computer={12}>
                {!nbHits || nbHits.length ? (
                  <List.Content>
                    <Header className="white-text font-w-500" as="h3">
                      Sorry, No Jobs found for “{qsParsed[`?query`]}”
                    </Header>
                  </List.Content>
                ) : (
                  <List.Content>
                    <List.Header>
                      <Header className="white-text font-w-500" as="h3">
                        Found {nbHits} Jobs
                      </Header>
                    </List.Header>
                    <List.Description>
                      <p className="margin-bottom-0 text-pattens-blue">
                        {SEARCH_PAGE.BANNER.JOB}
                      </p>
                    </List.Description>
                  </List.Content>
                )}
              </Grid.Column>
              <Grid.Column mobile={16} computer={4}>
                <List.Content className="bannrtBtn">
                  <List.Header
                    className="padding-bottom-10"
                    style={{
                      marginTop: "18px",
                      textAlign: "right"
                    }}>
                    {!isLoggedIn() ? (
                      nbHits ? (
                        <Button
                          onClick={() =>
                            this.props.history.push({
                              pathname: "/search/job",
                              search: this.props.location.search
                            })
                          }
                          size="mini"
                          className="actionBtnBanner">
                          See All
                        </Button>
                      ) : (
                        ""
                      )
                    ) : (
                      <UserAccessModal
                        modalTrigger={
                          <Button size="mini" className="actionBtnBanner">
                            Sign up and apply
                          </Button>
                        }
                      />
                    )}
                  </List.Header>
                </List.Content>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </List.Item>

        {/* <List.Item>
          <List.Content>
            <List.Header>
              <Header
                className="white-text font-w-500"
                as="h3"
              >
                Found {nbHits} Jobs
              </Header>
            </List.Header>
            <List.Description>
              <p className="margin-bottom-0 text-pattens-blue">
                Apply now with Vasitum’s free{" "}
                <span className="font-w-600">personalized assistance</span> and
                increase your chances <br />
                of getting hired.
              </p>
            </List.Description>
          </List.Content>
          <List.Content>
            <List.Header
              className="padding-bottom-10"
              style={{
                marginTop: "18px",
                textAlign: "right"
              }}>
              {!isLoggedIn() ? (
                nbHits ? (
                  <Button
                    onClick={() =>
                      this.props.history.push({
                        pathname: "/search/job",
                        search: this.props.location.search
                      })
                    }
                    size="mini"
                    className="actionBtnBanner">
                    See All
                  </Button>
                ) : (
                    ""
                  )
              ) : (
                  <UserAccessModal
                    modalTrigger={
                      <Button size="mini" className="actionBtnBanner">
                        Sign up and apply
                    </Button>
                    }
                  />
                )}
            </List.Header>
          </List.Content>


        </List.Item> */}
      </List>
    );
  }
}

const mapStateToProps = state => {
  return {
    search: state.search.searchCount
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onJobChange: data => {
      dispatch(onCountJobChange(data));
    }
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(connectStats(BestFoundJobBanner))
);
