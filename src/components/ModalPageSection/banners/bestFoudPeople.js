import React from "react";
import {
  Image,
  Header,
  List,
  Icon,
  Button,
  Dropdown,
  Grid
} from "semantic-ui-react";

import { connectStats } from "react-instantsearch/connectors";
import { withRouter } from "react-router-dom";
import isLoggedIn from "../../../utils/env/isLoggedin";

import { connect } from "react-redux";

import { onCountPeopleChange } from "../../../actions/search";
import { SEARCH_PAGE } from "../../../strings";

import UserAccessModal from "../UserAccessModal";
import qs from "qs";

class BestFoundPeopleBanner extends React.Component {
  render() {
    const { nbHits, isHidden } = this.props;
    const qsParsed = qs.parse(this.props.location.search);
    // console.log(qsParsed);

    return (
      <List
        verticalAlign="middle"
        style={{ display: isHidden ? "none" : "block" }}>
        <List.Item>
          <Grid>
            <Grid.Row>
              <Grid.Column mobile={16} computer={12}>
                {!nbHits || nbHits.length === 0 ? (
                  <List.Content>
                    <List.Header>
                      <Header className="white-text font-w-500" as="h3">
                        Sorry, No Candidate found for “{qsParsed[`?query`]}”
                      </Header>
                    </List.Header>
                  </List.Content>
                ) : (
                  <List.Content>
                    <List.Header>
                      <Header className="white-text font-w-500" as="h3">
                        Found {nbHits} People
                      </Header>
                    </List.Header>
                    <List.Description>
                      <p className="margin-bottom-0 text-white">
                        {SEARCH_PAGE.BANNER.PEOPLE}
                      </p>
                    </List.Description>
                  </List.Content>
                )}
              </Grid.Column>
              <Grid.Column mobile={16} computer={4}>
                <List.Content className="bannrtBtn">
                  <List.Header
                    style={{ textAlign: "right", marginTop: "18px" }}
                    className="padding-bottom-10">
                    {!isLoggedIn() ? (
                      nbHits ? (
                        <Button
                          onClick={() =>
                            this.props.history.push({
                              pathname: "/search/people",
                              search: this.props.location.search
                            })
                          }
                          size="mini"
                          className="actionBtnBanner">
                          See All
                        </Button>
                      ) : (
                        ""
                      )
                    ) : (
                      <UserAccessModal
                        modalTrigger={
                          <Button size="mini" className="actionBtnBanner">
                            Sign up and post job
                          </Button>
                        }
                      />
                    )}
                  </List.Header>
                </List.Content>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </List.Item>
      </List>
    );
  }
}

const mapStateToProps = state => {
  return {
    search: state.search.searchCount
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onPeopleChange: data => {
      dispatch(onCountPeopleChange(data));
    }
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(connectStats(BestFoundPeopleBanner))
);
