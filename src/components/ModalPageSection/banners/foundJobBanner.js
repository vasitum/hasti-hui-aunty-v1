import React from "react";
import {
  Image,
  Header,
  List,
  Icon,
  Button,
  Dropdown,
  Grid
} from "semantic-ui-react";
import RecentDropDown from "../dropDowns/recentDropdown";
import { connectStats } from "react-instantsearch/connectors";
import { connect } from "react-redux";
import { onCountJobChange } from "../../../actions/search";
import UserAccessModal from "../UserAccessModal";

import { SEARCH_PAGE } from "../../../strings";
import isLoggedIn from "../../../utils/env/isLoggedin";
import { withRouter } from "react-router-dom";
import qs from "qs";
import "./index.scss";

class FoundJobBanner extends React.Component {
  render() {
    const { nbHits } = this.props;
    const qsParsed = qs.parse(this.props.location.search);
    // console.log(qsParsed);

    return (
      <List verticalAlign="middle">
        <List.Item>
          <Grid>
            <Grid.Row>
              <Grid.Column mobile={16} computer={10}>
                {!nbHits || nbHits.length === 0 ? (
                  <List.Content>
                    <List.Header>
                      <Header className="white-text font-w-500" as="h3">
                        Sorry, No Jobs found for “{qsParsed[`?query`]}”
                      </Header>
                    </List.Header>
                  </List.Content>
                ) : (
                  <List.Content>
                    <List.Header>
                      <Header className="white-text font-w-500" as="h3">
                        Found {nbHits} Jobs
                      </Header>
                    </List.Header>
                    <List.Description>
                      <p className="margin-bottom-0 textLightCyanLight">
                        {SEARCH_PAGE.BANNER.JOB}
                      </p>
                    </List.Description>
                  </List.Content>
                )}
              </Grid.Column>

              <Grid.Column mobile={16} computer={6} verticalAlign="right">
                <List.Content className="bannrtBtn">
                  {!isLoggedIn() ? (
                    nbHits ? (
                      <List.Description
                        className="mobile hidden"
                        className="after_login">
                        {/* <RecentDropDown
                          defaultRefinement="job_by_date"
                          items={[
                            { value: "job", text: "Relevance" },
                            { value: "job_by_date", text: "Recent" }
                          ]}
                        /> */}
                      </List.Description>
                    ) : null
                  ) : (
                    <div className="login_button">
                      <UserAccessModal
                        modalTrigger={
                          <Button size="mini" className="actionBtnBanner">
                            Sign up and apply job
                          </Button>
                        }
                      />

                      {/* <RecentDropDown
                        defaultRefinement="job_by_date"
                        items={[
                          { value: "job", text: "Relevance" },
                          { value: "job_by_date", text: "Recent" }
                        ]}
                      /> */}
                      {/* <RecentDropDown
                        defaultRefinement="job"
                        items={[
                          {
                            key: "job",
                            content: "Relevance",
                            value: "job",
                            text: "Relevance"
                          },
                          {
                            key: "job_by_date",
                            content: "Recent",
                            value: "job_by_date",
                            text: "Recent"
                          }
                        ]}
                      /> */}
                    </div>
                  )}
                  {/* <List.Header className="padding-bottom-10 alingRight">
                    <Button size="mini" className="actionBtnBanner">
                      Sign up and apply job
                    </Button>
                  </List.Header> */}

                  {/* only Desktop */}
                  {/* <List.Description className="mobile hidden">
                    <RecentDropDown
                      defaultRefinement="job"
                      items={[
                        {
                          key: "",
                          content: "",
                          value: false,
                          text: ""
                        },
                        {
                          key: "job",
                          content: "Relevant",
                          value: "job",
                          text: "Relevant"
                        },
                        {
                          key: "job_by_date",
                          content: "Recent",
                          value: "job_by_date",
                          text: "Recent"
                        }
                      ]}
                    />
                  </List.Description> */}
                  {/* only Desktop end */}
                </List.Content>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </List.Item>
      </List>
    );
  }
}

const mapStateToProps = state => {
  return {
    search: state.search.searchCount
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onJobChange: data => {
      dispatch(onCountJobChange(data));
    }
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(connectStats(FoundJobBanner))
);
