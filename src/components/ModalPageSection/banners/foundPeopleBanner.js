import React from "react";
import {
  Image,
  Header,
  List,
  Icon,
  Button,
  Dropdown,
  Grid
} from "semantic-ui-react";
import RecentDropDown from "../dropDowns/recentDropdown";

import { connectStats } from "react-instantsearch/connectors";

import { connect } from "react-redux";
import { onCountPeopleChange } from "../../../actions/search";
import isLoggedIn from "../../../utils/env/isLoggedin";
import { SEARCH_PAGE } from "../../../strings";
import UserAccessModal from "../UserAccessModal";
import { withRouter } from "react-router-dom";
import qs from "qs";

class FoundPeopleBanner extends React.Component {
  render() {
    const { nbHits } = this.props;
    const qsParsed = qs.parse(this.props.location.search);
    // console.log(qsParsed);

    return (
      <List verticalAlign="middle">
        <Grid>
          <Grid.Row>
            <Grid.Column mobile={16} computer={10}>
              {!nbHits || nbHits.lenght === 0 ? (
                <List.Content>
                  <List.Header className="white-text font-w-500" as="h3">
                    Sorry, No Candidate found for “{qsParsed[`?query`]}”
                  </List.Header>
                </List.Content>
              ) : (
                <List.Content>
                  <List.Header>
                    <Header className="white-text font-w-500" as="h3">
                      Found {nbHits} People
                    </Header>
                  </List.Header>
                  <List.Description>
                    <p className="margin-bottom-0 text-light-cyan">
                      {SEARCH_PAGE.BANNER.PEOPLE}
                    </p>
                  </List.Description>
                </List.Content>
              )}
            </Grid.Column>

            <Grid.Column mobile={16} computer={6} verticalAlign="right">
              <List.Content className="bannrtBtn">
                {!isLoggedIn() ? (
                  nbHits ? (
                    <List.Description
                      className="mobile hidden"
                      className="after_login people_page"
                      style={{ marginTop: "30px" }}>
                      <RecentDropDown
                        defaultRefinement="user_by_date"
                        items={[
                          { value: "user", text: "Relevance" },
                          { value: "user_by_date", text: "Recent" }
                        ]}
                      />
                    </List.Description>
                  ) : null
                ) : (
                  <div className="login_button">
                    <UserAccessModal
                      modalTrigger={
                        <Button size="mini" className="actionBtnBanner">
                          Sign up and post job
                        </Button>
                      }
                    />
                    <RecentDropDown
                      defaultRefinement="user_by_date"
                      items={[
                        { value: "user", text: "Relevance" },
                        { value: "user_by_date", text: "Recent" }
                      ]}
                    />
                    {/* <RecentDropDown
                      defaultRefinement="user"
                      items={[
                        { value: "user", text: "Relevance" },
                        { value: "user_by_date", text: "Recent" }
                      ]}
                    /> */}
                  </div>
                )}
              </List.Content>
            </Grid.Column>
          </Grid.Row>
        </Grid>
        {/* <List.Item>
          <List.Content floated="right">
            
            <List.Header className="padding-bottom-10">
              <Button size="mini" className="actionBtnBanner">
                Sign up and post job
              </Button>
            </List.Header>
            <List.Description>
              <RecentDropDown
                defaultRefinement="user"
                items={[
                  { value: "user", text: "Relevant" },
                  { value: "user_by_date", text: "Recent" }
                ]}
              />
            </List.Description>
          </List.Content>

          <List.Content floated="left">
            <List.Header>
              <Header
                className="white-text font-w-500"
                as="h3"
                style={{
                  fontSize: "26px"
                }}>
                Found {nbHits} People
              </Header>
            </List.Header>
            <List.Description>
              <p className="margin-bottom-0 text-light-cyan">
                Post jobs with Vasitum’s free{" "}
                <span className="font-w-600">personalized assistance</span> to
                double your hiring efficiency.
              </p>
            </List.Description>
          </List.Content>
        </List.Item> */}
      </List>
    );
  }
}

const mapStateToProps = state => {
  return {
    search: state.search.searchCount
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onPeopleChange: data => {
      dispatch(onCountPeopleChange(data));
    }
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(connectStats(FoundPeopleBanner))
);
