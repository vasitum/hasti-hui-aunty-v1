import React, { PureComponent } from "react";
import { connectStats } from "react-instantsearch/connectors";
import { connect } from "react-redux";
import { onCountPeopleChange } from "../../../actions/search";

class UpdatePeopleCounter extends PureComponent {
  peopleCounter = 0;

  componentDidMount() {
    this.props.onPeopleChange({
      people: this.props.nbHits
    });
  }

  UNSAFE_componentWillReceiveProps(nextProps, nextState) {
    if (window.location.pathname.split("/").pop() === "people") {
      if (this.peopleCounter !== Number(nextProps.nbHits)) {
        // update job counter
        this.peopleCounter = nextProps.nbHits;

        // update the counter
        this.props.onPeopleChange({
          people: nextProps.nbHits
        });
      }
    }
  }

  render() {
    return null;
  }
}

const mapStateToProps = state => {
  return {
    search: state.search.searchCount
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onPeopleChange: data => {
      dispatch(onCountPeopleChange(data));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(connectStats(UpdatePeopleCounter));
