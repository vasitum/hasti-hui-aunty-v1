import React from "react";

import { Button, Grid, Popup } from "semantic-ui-react";
import { Form } from "formsy-semantic-ui-react";
import PropTypes from "prop-types";

// import IcDownArrow from "../../../assets/svg/IcDownArrow";
import ModalBanner from "../../ModalBanner";
import IcInfoIcon from "../../../assets/svg/IcInfo";
import IcDownArrow from "../../../assets/svg/IcDownArrow";

import CompanyDetails from "../../CreateCompanyPage/CompanyDetails";
import { geocodeByAddress, getLatLng } from "react-places-autocomplete";
import { ToastContainer, toast } from "react-toastify";

import pushCompanytoServer from "../../../api/company/updateCompany";
import { USER } from "../../../constants/api";
import { uploadImage } from "../../../utils/aws";

import uriToBlob from "../../../utils/env/uriToBlob";

import "react-toastify/dist/ReactToastify.css";

import "./index.scss";

const infoicon = <IcInfoIcon pathcolor="#c8c8c8" />;

function checkValidData(key) {
  return key ? key : "";
}

class CreateCompanyPage extends React.Component {
  state = {
    _id: "",
    about: "",
    admin: [
      {
        email: window.localStorage.getItem(USER.EMAIL)
      }
    ],
    clientName: "",
    companyType: "",
    headquarter: {
      latitude: 0,
      location: "",
      longitude: 0
    },
    imgExt: "",
    industry: "",
    name: "",
    size: "",
    website: "",
    year: 0,
    isLoading: false,
    canSubmit: false,
    imgFile: false,
    imgUrl: false,
    imagePickerOpen: false
  };

  constructor(props) {
    super(props);
    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.sendImageToAws = this.sendImageToAws.bind(this);
  }

  componentDidMount() {
    const { data } = this.props;
    this.setState({
      ...this.state,
      _id: checkValidData(data._id),
      about: checkValidData(data.about),
      clientName: checkValidData(data.clientName),
      companyType: checkValidData(data.companyType),
      headquarter: {
        latitude: checkValidData(
          data.headquarter ? data.headquarter.latitude : ""
        ),
        location: checkValidData(
          data.headquarter ? data.headquarter.location : ""
        ),
        longitude: checkValidData(
          data.headquarter ? data.headquarter.longitude : ""
        )
      },
      imgExt: checkValidData(data.imgExt),
      industry: checkValidData(data.industry),
      name: checkValidData(data.name),
      size: checkValidData(data.size),
      website: checkValidData(data.website),
      year: checkValidData(data.year),
      canSubmit: true,
      imgUrl: checkValidData(data.imgExt)
        ? `https://s3-us-west-2.amazonaws.com/img-mwf/company/${
            data._id
          }/image.jpg`
        : ""
    });
  }

  onInputChange = (ev, { value, name }) => {
    this.setState({
      [name]: value
    });
  };

  onAutoCompleteChange = ({ name, value }) => {
    this.setState({
      [name]: value
    });
  };

  onLocSelect = address => {
    this.setState({
      headquarter: {
        ...this.state.headquarter,
        location: address
      }
    });
    geocodeByAddress(address)
      .then(results => getLatLng(results[0]))
      .then(latLng => {
        this.setState({
          headquarter: {
            ...this.state.headquarter,
            latitude: latLng.lat,
            longitude: latLng.lng
          }
        });
      })
      .catch(error => console.error("Error", error));
  };

  onLocChange = address => {
    this.setState({
      headquarter: {
        ...this.state.headquarter,
        location: address
      }
    });
  };

  onAboutChange = html => {
    this.setState({
      about: html
    });
  };

  onFileChange = e => {
    const file = e.target.files[0];
    const extn = file.name
      .split(".")
      .pop()
      .toLowerCase();

    // reset target value
    if (e.target.value) {
      e.target.value = "";
    }

    const fr = new FileReader();

    fr.onload = e => {
      this.setState({
        imgExt: "jpg",
        imgFile: file,
        imgUrl: e.target.result,
        imagePickerOpen: true
      });
    };

    fr.readAsDataURL(file);
  };

  onFileRemove = e => {
    this.setState({
      imgExt: "",
      imgFile: "",
      imgUrl: ""
    });
  };

  onImagePickerCancelClick = ev => {
    const { data } = this.props;

    this.setState({
      imagePickerOpen: !this.state.imagePickerOpen,
      imgExt: checkValidData(data.imgExt),
      imgUrl: checkValidData(data.imgExt)
        ? `https://s3-us-west-2.amazonaws.com/img-mwf/company/${
            data._id
          }/image.jpg`
        : ""
    });
  };

  onImagePickerSaveClick = image => {
    this.setState({
      imgUrl: image,
      imagePickerOpen: !this.state.imagePickerOpen
    });
  };

  async sendImageToAws(compId) {
    const file = uriToBlob(this.state.imgUrl);
    const userId = window.localStorage.getItem(USER.UID);
    try {
      const acceptance = await uploadImage(`company/${compId}/image.jpg`, file);
      // console.log(acceptance);
    } catch (error) {
      console.log(error);
    }
  }

  async onFormSubmit(ev) {
    if (this.props.onEnableFormLoader) {
      this.props.onEnableFormLoader("Updating Company ...");
    }

    try {
      const res = await pushCompanytoServer(this.state._id, this.state);
      const data = await res.json();

      /* imgFile Upload */
      if (this.state.imgFile) {
        const imageUpload = await this.sendImageToAws(data._id);
      }

      this.props.onCompanyCreate(data, true);

      if (this.props.onModalClose) {
        this.props.onModalClose();
      }

      if (this.props.onDisableFormLoader) {
        this.props.onDisableFormLoader();
      }
    } catch (error) {
      console.error(error);
      if (this.props.onDisableFormLoader) {
        this.props.onDisableFormLoader();
      }
    }
  }

  onFormValid = ev => {
    this.setState({
      canSubmit: true
    });
  };

  onFormInValid = ev => {
    this.setState({
      canSubmit: false
    });
  };

  render() {
    return (
      <div className="CreateCompanyPage">
        <Form
          noValidate
          onValidSubmit={this.onFormSubmit}
          onValid={this.onFormValid}
          onInvalid={this.onFormInValid}>
          <ModalBanner
            headerText="Edit Company"
            headerIcon={
              <Popup
                trigger={
                  <div className="InfoIconLabel">
                    <IcInfoIcon pathcolor="#ceeefe" />
                  </div>
                }
                content="Add users to your feed"
              />
            }>
            <Button type="button" compact className="close_btn" />
          </ModalBanner>
          <CompanyDetails
            data={this.state}
            onLocChange={this.onLocChange}
            onLocSelect={this.onLocSelect}
            onInputChange={this.onInputChange}
            onAutoCompleteChange={this.onAutoCompleteChange}
            onAboutChange={this.onAboutChange}
            onFileChange={this.onFileChange}
            onFileRemove={this.onFileRemove}
            onImagePickerCancelClick={this.onImagePickerCancelClick}
            onImagePickerSaveClick={this.onImagePickerSaveClick}
            onCancelClick={this.props.onModalClose}
          />
        </Form>
      </div>
    );
  }
}

export default CreateCompanyPage;
