import React from "react";
import { Card, Image, Button, Icon, List, Header } from "semantic-ui-react";
import SocialBtn from "../Buttons/socialBtn";
import { Link } from "react-router-dom";
import LocationLogo from "../../../assets/svg/IcLocation";
import UserLogo from "../../../assets/svg/IcUser";
import { jobLike, getLikedAndSavedJobs } from "../../../api/jobs/jobExtras";
import isProfileCreated from "../../../utils/env/isProfileCreated";

import fromNow from "../../../utils/env/fromNow";

import isLoggedIn from "../../../utils/env/isLoggedin";
import UserAccessModal from "../UserAccessModal";
import P2Ptracker from "../../P2PTracker";
import { USER } from "../../../constants/api";

import "./index.scss";

class BestPeopleCard extends React.Component {
  constructor(props) {
    super(props);
    this.onLikeClick = this.onLikeClick.bind(this);
  }

  state = {
    liked: false,
    saved: false,
    applied: false,
    apiDone: false
  };

  async onLikeClick(ev, { id }) {
    ev.stopPropagation();

    if (!isProfileCreated()) {
      return;
    }

    try {
      const res = await jobLike(id, "user");
      const data = await res.text();
    } catch (error) {
      console.error(error);
    }
  }

  async componentDidMount() {
    const { hit: val } = this.props;
    try {
      const res = await getLikedAndSavedJobs([val.objectID], "user");
      if (res.status === 200) {
        const data = await res.json();
        const dataVal = data[val.objectID];
        this.setState({
          liked: dataVal.like,
          saved: dataVal.saveJobs,
          applied: dataVal.applied,
          apiDone: true
        });
      } else {
        console.log(
          "Unable to find liked and saved details for ID",
          val.objectID
        );
        this.setState({
          apiDone: true
        });
      }
    } catch (error) {
      console.error(error);
      this.setState({
        apiDone: true
      });
    }
  }

  render() {
    const { hit, idx, isPeople } = this.props;

    const isLoggedInVal = isLoggedIn();

    const UserId = window.localStorage.getItem(USER.UID);

    if (!hit) {
      return null;
    }

    if (hit.objectID === window.localStorage.getItem(USER.UID)){
      return null
    }

    return (
      <div
        className="best-card-body BestCard_body position-r"
        style={{
          height: "300px",
          borderBottom: "1px solid #efefef"
          // borderLeft: (idx + 1) % 2 === 0 ? "1px solid #efefef" : ""
        }}>
        <Card fluid className="best-card-hover padding-bottom-10">
          <Card.Content textAlign="center">
            <Card.Meta textAlign="right" className="fontSize-12">
              Updated:{" "}
              <span className="text-tiffany-blue"> {fromNow(hit.uTime)}</span>
            </Card.Meta>
            {!hit.imgExt ? (
              <span className="userImgThumb">
                <UserLogo
                  width="64"
                  height="64"
                  rectcolor="#f7f7fb"
                  pathcolor="#c8c8c8"
                  style={{
                    margin: "10px 0px"
                  }}
                />
              </span>
            ) : (
              <Image
                circle
                verticalAlign="top"
                style={{
                  height: "64px",
                  width: "64px",
                  margin: "10px 0px",
                  borderRadius: "50%"
                }}
                // className="padding-bottom-8"
                src={`https://s3-us-west-2.amazonaws.com/img-mwf/${
                  hit.objectID
                }/image.jpg`}
              />
            )}
            {/* <Image centered size='mini' src={airtel} className="padding-bottom-8" /> */}
            <Card.Header className="night-rider vasitumFontOpen-600">
              {hit.fName} {hit.lName}
            </Card.Header>
            <Card.Description className="BestCard_bodyDetails">
              <p className="text-Matterhorn">{hit.title}</p>
              <p className="text-Matterhorn">
                <span>
                  {hit.com ? (
                    <span>
                      Current: <span className="text-nobel"> {hit.com}</span>{" "}
                    </span>
                  ) : (
                    ""
                  )}
                </span>
              </p>
              <p
                className="card_detailTitle"
                style={{
                  display: !hit.locCity ? "none" : "block"
                }}>
                <LocationLogo width="9.5" height="11.5" pathcolor="#0bd0bb" />

                <span className="padding-left-3">
                  {hit.locCity}
                  {hit.locCountry ? ", " + hit.locCountry : ""}
                </span>
              </p>
            </Card.Description>
            <Card.Description className="filterSkill padding-top-10">
              <List verticalAlign="middle" className="padding-top-10">
                <List.Item>
                  <List.Content verticalAlign="middle">
                    <List.Description>
                      <List bulleted horizontal className="text-night-rider">
                        {hit.skills &&
                          hit.skills.map((skill, idx) => {
                            if (idx === 0) {
                              return (
                                <List.Item key={idx}>
                                  <span className="text-nobel">Skills:</span>{" "}
                                  <span>{skill}</span>
                                </List.Item>
                              );
                            }

                            if (idx >= 4) {
                              return;
                            }

                            return <List.Item key={idx}>{skill}</List.Item>;
                          })}
                      </List>
                    </List.Description>
                  </List.Content>
                </List.Item>
              </List>
            </Card.Description>
          </Card.Content>
        </Card>

        <div className="best-card-hover-show" style={{ width: "100%" }}>
          <P2Ptracker
            cardType={"icon"}
            reqId={window.localStorage.getItem(USER.UID)}
            userId={hit.objectID}
          />
          <List>
            <List.Item>
              <List.Content className="alingCenter">
                <List.Header className="padding-bottom-20">
                  <Header
                    as={Link}
                    to={`/view/user/${hit.objectID}`}
                    className="truncate vasitumFontOpen-600">
                    {hit.fName} {hit.lName}
                  </Header>
                  <p className="text-Matterhorn vasitumFontOpen-500 padding-top-5">
                    {hit.title}
                  </p>
                  {/* <Header as="h3" className="truncate vasitumFontOpen-600">
                    
                  </Header> */}
                </List.Header>
                <List.Description className="padding-bottom-15">
                  {!isLoggedInVal ? (
                    <Button
                      as={Link}
                      to={
                        UserId === hit.objectID
                          ? `/messaging`
                          : `/messaging/${hit.objectID}`
                      }
                      size="mini"
                      className="actionBtn">
                      {isPeople ? "Message" : "Apply"}{" "}
                      <Icon name="angle right" />
                    </Button>
                  ) : (
                    <UserAccessModal
                      modalTrigger={
                        <Button size="mini" className="actionBtn">
                          {isPeople ? "Message" : "Apply"}{" "}
                          <Icon name="angle right" />
                        </Button>
                      }
                      isAction
                      // actionText="Message"
                    />
                  )}
                </List.Description>
                <List.Description>
                  {/* {this.state.apiDone ? (
                    <SocialBtn
                      objectID={hit.objectID}
                      onLikeClick={this.onLikeClick}
                      liked={this.state.liked}
                      saved={this.state.saved}
                      type="user"
                    />
                  ) : null} */}
                </List.Description>
              </List.Content>
            </List.Item>
          </List>
        </div>
      </div>
    );
  }
}

export default BestPeopleCard;
