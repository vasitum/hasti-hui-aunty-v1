import React from "react";
import { Card, Button, Icon, List, Header, Image } from "semantic-ui-react";
import SocialBtn from "../Buttons/socialBtn";
import IcExternalIcon from "../../../assets/svg/IcExternalJob";
import CompanySvg from "../../../assets/svg/IcCompany";
import JobLogo from "../../../assets/svg/Icjob";
import { Link } from "react-router-dom";
import LocationComponent from "../commonComponent/location";

import isLoggedIn from "../../../utils/env/isLoggedin";
import isProfileCreated from "../../../utils/env/isProfileCreated";

import UserAccessModal from "../UserAccessModal";
import P2Ptracker from "../../P2PTracker";

import { USER } from "../../../constants/api";

import "./index.scss";

import fromNow from "../../../utils/env/fromNow";
import {
  jobLike,
  jobApply,
  getLikedAndSavedJobs
} from "../../../api/jobs/jobExtras";

class BestJobCard extends React.Component {
  constructor(props) {
    super(props);
    this.onLikeClick = this.onLikeClick.bind(this);
    this.onApplyClick = this.onApplyClick.bind(this);
    this.USERID = window.localStorage.getItem(USER.UID);
  }

  state = {
    liked: false,
    saved: false,
    applied: false,
    apiDone: false
  };

  async onLikeClick(ev, { id }) {
    ev.stopPropagation();

    if (!isProfileCreated()) {
      return;
    }

    try {
      const res = await jobLike(id, "job");
      const data = await res.text();
    } catch (error) {
      console.error(error);
    }
  }

  async onApplyClick(e) {
    const { hit: val } = this.props;

    if (!isProfileCreated()) {
      return;
    }

    try {
      const res = await jobApply(val.objectID, val.title);
      if (res.status === 200) {
        this.setState({
          applied: true
        });
      } else {
        console.log("Unable to apply to job", val.objectID);
      }
    } catch (error) {
      console.error(error);
    }
  }

  async componentDidMount() {
    const { hit: val } = this.props;
    try {
      const res = await getLikedAndSavedJobs([val.objectID], "user");
      if (res.status === 200) {
        const data = await res.json();
        const dataVal = data[val.objectID];
        this.setState({
          liked: dataVal.like,
          saved: dataVal.saveJobs,
          applied: dataVal.applied,
          apiDone: true
        });
      } else {
        console.log(
          "Unable to find liked and saved details for ID",
          val.objectID
        );
        this.setState({
          apiDone: true
        });
      }
    } catch (error) {
      console.error(error);
      this.setState({
        apiDone: true
      });
    }
  }

  render() {
    const { hit, idx } = this.props;

    const isLoggedInVal = isLoggedIn();

    if (!hit) {
      return null;
    }

    if (hit.objectID === window.localStorage.getItem(USER.UID)) {
      return null;
    }

    console.log("check hit", hit);

    return (
      <div
        className="best-card-body BestCard_body position-r"
        style={{
          height: "300px",
          borderBottom: "1px solid #efefef"
          // borderLeft: (idx + 1) % 2 === 0 ? "1px solid #efefef" : ""
        }}>
        <Card fluid className="best-card-hover">
          <Card.Content textAlign="center">
            <Card.Meta textAlign="right" className="fontSize-12">
              Posted:{" "}
              <span className="text-pacific-blue">
                {fromNow(hit.datePosted)}
              </span>
            </Card.Meta>

            <span className="companyLogo">
              {/* <CompanySvg
                width="64"
                height="64"
                rectcolor="#f7f7fb"
                pathcolor="#c8c8c8"
              /> */}

              {hit.comImgExt ? (
                <Image
                  verticalAlign="top"
                  style={{
                    height: "64px",
                    width: "64px",
                    margin: "10px 0px",
                    borderRadius: "6px"
                    // position: "absolute"
                  }}
                  src={`https://s3-us-west-2.amazonaws.com/img-mwf/company/${
                    hit.comId
                  }/image.jpg`}
                />
              ) : (
                <CompanySvg
                  width="64px"
                  height="64px"
                  rectcolor="#f7f7fb"
                  pathcolor="#c8c8c8"
                  style={{
                    margin: "10px 0px",
                    borderRadius: "6px"
                  }}
                />
              )}
            </span>
            {/* <Image centered size='mini' src={airtel} className="padding-bottom-8" /> */}
            <Card.Header
              className="night-rider vasitumFontOpen-600"
              style={{
                display: "block",
                whiteSpace: "nowrap",
                overflow: "hidden",
                textOverflow: "ellipsis"
              }}>
              {hit.title}
            </Card.Header>
            <Card.Description className="BestCard_bodyDetails">
              <p className="text-Matterhorn card_detailTitle">
                <JobLogo width="14.25" height="11.5" pathcolor="#0B9ED0" />

                <span className="padding-left-3">{hit.comName}</span>
              </p>

              <LocationComponent hit={hit} className="card_detailTitle" />

              <p
                className="text-trolley-grey"
                style={{
                  display: !hit.expMin && !hit.expMax ? "none" : "block"
                }}>
                <span className="text-night-rider">
                  {" "}
                  {hit.expMin} - {hit.expMax}{" "}
                </span>
                yrs experience.
              </p>
            </Card.Description>
            <Card.Description className="filterSkill padding-top-10">
              <List verticalAlign="middle" className="padding-top-10">
                <List.Item>
                  <List.Content verticalAlign="middle">
                    <List.Description>
                      <List bulleted horizontal className="text-night-rider">
                        {hit.skills &&
                          hit.skills.map((skill, idx) => {
                            if (idx === 0) {
                              return (
                                <List.Item key={idx}>
                                  <span className="text-nobel">Skills:</span>{" "}
                                  <span>{skill}</span>
                                </List.Item>
                              );
                            }

                            if (idx >= 4) {
                              return;
                            }

                            return <List.Item key={idx}>{skill}</List.Item>;
                          })}
                      </List>
                    </List.Description>
                  </List.Content>
                </List.Item>
              </List>
            </Card.Description>
          </Card.Content>
        </Card>

        <div className="best-card-hover-show" style={{ width: "100%" }}>
          <P2Ptracker
            type={"job"}
            cardType={"icon"}
            reqId={hit.objectID}
            userId={window.localStorage.getItem(USER.UID)}
          />
          <List>
            <List.Item>
              <List.Content className="alingCenter">
                <List.Header className="padding-bottom-20">
                  {/* {hit.externalLink ? (
                    <Header
                      as={"a"}
                      target={"_blank"}
                      href={hit.externalLink}
                      className="truncate vasitumFontOpen-600 hover_title">
                      {hit.title}
                    </Header>
                  ) : (
                    <Header
                      as={Link}
                      to={`/view/job/${hit.objectID}`}
                      className="truncate vasitumFontOpen-600 hover_title">
                      {hit.title}
                    </Header>
                  )} */}
                  <Header
                    as={Link}
                    to={`/view/job/${encodeURIComponent(
                      hit.title.split(" ").join("-")
                    )}/${hit.objectID}`}
                    className="truncate vasitumFontOpen-600 hover_title">
                    {hit.title}
                  </Header>
                  <p className="text-Matterhorn vasitumFontOpen-500 padding-top-5">
                    {hit.comName}
                  </p>
                </List.Header>

                <List.Description className="padding-bottom-15">
                  {/* {hit.externalLink ? (
                    <Button
                      as={"a"}
                      target={"_blank"}
                      href={hit.externalLink}
                      title={hit.title}
                      size="mini"
                      className="actionBtn">
                      View Job <Icon name="angle right" />
                    </Button>
                  ) : (
                    <Button
                      as={Link}
                      to={`/view/job/${hit.objectID}`}
                      title={hit.title}
                      size="mini"
                      className="actionBtn">
                      View Job <Icon name="angle right" />
                    </Button>
                  )} */}
                  <Button
                    as={Link}
                    to={`/view/job/${encodeURIComponent(
                      hit.title.split(" ").join("-")
                    )}/${hit.objectID}`}
                    title={hit.title}
                    size="mini"
                    className="actionBtn external-job-button-best">
                    View Job{" "}
                    {hit.externalLink ? (
                      <IcExternalIcon
                        pathcolor="#99d9f9"
                        height="12"
                        width="12"
                      />
                    ) : (
                      <Icon name="angle right" />
                    )}
                  </Button>
                  {/* {!isLoggedInVal ? (
                    this.state.applied ? (
                      <Button
                        id={hit.objectID}
                        title={hit.title}
                        size="mini"
                        className="actionBtn">
                        Applied <Icon name="angle right" />
                      </Button>
                    ) : (
                      <Button
                        disabled={this.USERID === hit.userId}
                        onClick={this.onApplyClick}
                        id={hit.objectID}
                        title={hit.title}
                        size="mini"
                        className="actionBtn">
                        Apply <Icon name="angle right" />
                      </Button>
                    )
                  ) : (
                    <UserAccessModal
                      modalTrigger={
                        <Button size="mini" className="actionBtn">
                          Apply <Icon name="angle right" />
                        </Button>
                      }
                      isAction
                      actionText="apply to"
                    />
                  )} */}
                </List.Description>
                <List.Description>
                  {/* {this.state.apiDone ? (
                    <SocialBtn
                      disabled={this.USERID === hit.userId}
                      objectID={hit.objectID}
                      onLikeClick={this.onLikeClick}
                      liked={this.state.liked}
                      saved={this.state.saved}
                      type="job"
                    />
                  ) : null} */}
                </List.Description>
              </List.Content>
            </List.Item>
          </List>
        </div>
      </div>
    );
  }
}

export default BestJobCard;
