import React from "react";
import { List, Image, Header } from "semantic-ui-react";
// import publicProfileBanner from "../../../assets/img/publicProfileBanner.png";
// import publicProfile from "../../../assets/img/publicProfile.png";
import profile from "../../../assets/img/profile.png";

import ProfileProgress from "./ProfileProgress";
import FlatFileuploadBtn from "../Buttons/FlatFileUploadeBtn";

import P2PTracker from "../../P2PTracker";
import { USER } from "../../../constants/api";

class ProfileRightSide extends React.Component {
  render() {
    const { userId } = this.props;
    if (!userId) {
      return null;
    }

    // console.log("PRIGHTSIDE", userId);
    return (
      <div className="similar-people padding-top-20">
        <P2PTracker
          cardType={"user"}
          reqId={window.localStorage.getItem(USER.UID)}
          userId={userId}
        />
      </div>
    );
  }
}

export default ProfileRightSide;
