import React from "react";

import {
  List,
  Button,
  Icon,
  Image,
  Grid,
  Header,
  Responsive,
  Dropdown
} from "semantic-ui-react";

import publicProfileBanner from "../../../../assets/img/publicProfileBanner.png";
import UserLogo from "../../../../assets/svg/IcUser";
import LikeLogo from "../../../../assets/svg/IcLike";
import ShareLogo from "../../../../assets/svg/IcShare";
import SocialBtn from "../../Buttons/socialBtn";
import EducationNoCircleIcon from "../../../../assets/svg/IcEducationNoCircle";
import LocationLogo from "../../../../assets/svg/IcLocation";
import ProfileView from "../../../../assets/svg/IcProfileView";
import ShareButton from "../../../Buttons/ShareButton";

import DefaultModal from "../../../Modal/DefaultModal";
import AddLabelSave from "../../../CardElements/AddLabelSave";
import MobileSocialBtn from "../../moblieComponents/mobileSocialBtn";

function getInfoEdu(data) {
  if (!data || !data.edus || !Array.isArray(data.edus)) {
    return null;
  }

  const infoEdu = data.edus.filter(val => {
    return val.onInfo === true;
  });

  if (!infoEdu.length) {
    return null;
  }

  if (!infoEdu[0].institute) {
    return null;
  }

  return {
    institute: infoEdu[0].institute
  };
}

function getInfoExp(data) {
  if (!data || !data.exps || !Array.isArray(data.exps)) {
    return null;
  }

  const infoExp = data.exps.filter(val => {
    return val.onInfo === true;
  });

  if (!infoExp.length) {
    return null;
  }

  if (!infoExp[0].name) {
    return null;
  }

  return {
    name: infoExp[0].name,
    designation: infoExp[0].designation
  };
}

const PreProfileHeader = props => {
  let user = props.user || { loc: {} };

  const infoExp = getInfoExp(user);
  const infoEdu = getInfoEdu(user);
  return (
    <div>
      <List className="publicProfileBanner">
        <List.Item>
          <List.Content>
            <div className="publicPprofileBanner" />
            {/* <Image src={publicProfileBanner} /> */}
            <div
              style={{
                backgroundImage: `linear-gradient(
                    rgba(0,0,0,0.0),
                    rgba(0,0,0,0.5)
                  ), url(${
                    user.ext && user.ext.imgBanner
                      ? user.bannerImage
                      : publicProfileBanner
                  })`,
                height: "220px",
                backgroundSize: "cover",
                backgroundPosition: "center"
              }}
            />
          </List.Content>
        </List.Item>
        `
      </List>
      <div
        className="preProfileDetail publicProfile_detailContainer"
        style={{ paddingBottom: "0px" }}>
        <List>
          <List.Item>
            <Grid>
              <Grid.Row className="preProfileDetail_row">
                <Grid.Column width={8} className="mobile hidden">
                  <List horizontal>
                    <List.Item>
                      <List.Content className="text-white">
                        <span className="padding-right-3">
                          <ProfileView
                            width="18"
                            height="11"
                            pathcolor="#c8c8c8"
                          />
                        </span>
                        Profile views: {!user.viewCount ? 0 : user.viewCount}
                      </List.Content>
                    </List.Item>
                    <List.Item>
                      <List.Content
                        style={{ marginLeft: "15px", cursor: "pointer" }}
                        className="text-white">
                        <span className="padding-right-3">
                          <LikeLogo
                            width="17"
                            height="11"
                            pathcolor="#c8c8c8"
                          />
                        </span>
                        Like: {!user.likeCount ? 0 : user.likeCount}
                      </List.Content>
                    </List.Item>
                  </List>
                </Grid.Column>
                <Grid.Column
                  width={8}
                  className="mobile hidden"
                  textAlign="right"
                  style={{
                    paddingRight: "0px"
                  }}>
                  <List.Content>
                    <List.Header className="preProfileHeaderBtn alingCenterRight">
                      <ShareButton
                        trigger={
                          <Button
                            size="small"
                            disabled={true}
                            className="has-hover-blue">
                            <span className="padding-right-3">
                              <ShareLogo
                                width="15.07"
                                height="11"
                                pathcolor="#c8c8c8"
                              />
                            </span>
                            Share
                          </Button>
                        }
                      />
                      {/* <Button size="small" className="has-hover-blue">
                          <span className="padding-right-3">
                            <SaveLogo
                              width="17"
                              height="11"
                              pathcolor="#c8c8c8"
                            />
                          </span>
                          Save
                        </Button> */}
                      <Button
                        disabled={true}
                        size="small"
                        className="actionBtn margin-left-5">
                        Message <Icon name="angle right" />
                      </Button>
                      <Button disabled={true} className="has-hover-blue">
                        <Dropdown icon="ellipsis vertical">
                          <Dropdown.Menu>
                            <Dropdown.Item>Report this Profile</Dropdown.Item>
                            <Dropdown.Item>Share</Dropdown.Item>

                            <Dropdown.Item>
                              <DefaultModal
                                size="tiny"
                                trigger={<span>Save</span>}>
                                <AddLabelSave />
                              </DefaultModal>
                            </Dropdown.Item>
                          </Dropdown.Menu>
                        </Dropdown>
                      </Button>
                    </List.Header>
                  </List.Content>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Item className="preProfileHeaderDetail alingCenter">
                <List.Content style={{ position: "relative" }}>
                  <span
                    className="userImgThumb"
                    style={{ position: "relative" }}>
                    {!user.ext || !user.ext.img ? (
                      <React.Fragment>
                        <UserLogo
                          width="110"
                          height="110"
                          rectcolor="#f7f7fb"
                          pathcolor="#c8c8c8"
                        />
                      </React.Fragment>
                    ) : (
                      <Image
                        src={user.profileImage}
                        centered
                        style={{
                          borderRadius: "50%"
                        }}
                        alt={user.fName}
                      />
                    )}
                  </span>
                </List.Content>
                <List.Content>
                  <List.Header className="preProfileTitle">
                    <Header as="h1">
                      {user.fName} {user.lName}
                    </Header>
                    <p>{user.title}</p>
                  </List.Header>
                  <List.Content>
                    <p
                      className="text-nobel mtb-2px"
                      style={{
                        display: infoExp ? "block" : "none"
                      }}>
                      Current:{" "}
                      <span className="text-pacific-blue">
                        {infoExp ? infoExp.designation : ""}
                      </span>{" "}
                      at{" "}
                      <span className="text-pacific-blue">
                        {infoExp ? infoExp.name : ""}
                      </span>
                    </p>
                    <p
                      className="text-pacific-blue mt4b2"
                      style={{
                        display: infoEdu ? "block" : "none"
                      }}>
                      <span className="padding-right-3">
                        <EducationNoCircleIcon
                          width="17.032"
                          height="12"
                          pathcolor="#c8c8c8"
                        />
                      </span>
                      {infoEdu ? infoEdu.institute : ""}
                    </p>
                    <p
                      className="text-Matterhorn"
                      style={{
                        display: !user.loc || !user.loc.city ? "none" : "flex",
                        alignItems: "stretch",
                        justifyContent: "center"
                      }}>
                      <span className="padding-right-5">
                        <LocationLogo
                          width="9.5"
                          height="11.5"
                          pathcolor="#c8c8c8"
                        />
                      </span>
                      <span>{user.loc ? user.loc.city : ""}</span>
                    </p>
                    <MobileSocialBtn disabled={true} btntext="Message" />
                  </List.Content>
                </List.Content>

                <div className="socialLike_container">
                  <SocialBtn disabled={true} />
                </div>
              </List.Item>
            </List.Content>
          </List.Item>
        </List>
      </div>
    </div>
  );
};

export default PreProfileHeader;
