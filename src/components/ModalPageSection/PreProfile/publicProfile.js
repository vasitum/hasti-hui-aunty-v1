import React from "react";
import {
  List,
  Button,
  Icon,
  Image,
  Grid,
  Header,
  Feed,
  Dropdown,
  Modal,
  Responsive
} from "semantic-ui-react";
import publicProfileBanner from "../../../assets/img/publicProfileBanner.png";

import { list } from "postcss";
import SocialBtn from "../Buttons/socialBtn";
import IcEditIcon from "../../../assets/svg/IcEdit";

import EducationLogo from "../../../assets/svg/IcEducation";
import CompanySvg from "../../../assets/svg/IcCompany";
import UserLogo from "../../../assets/svg/IcUser";
import LikeLogo from "../../../assets/svg/IcLike";
import SaveLogo from "../../../assets/svg/IcSave";
import ShareLogo from "../../../assets/svg/IcShare";

import EducationNoCircleIcon from "../../../assets/svg/IcEducationNoCircle";
import LocationLogo from "../../../assets/svg/IcLocation";
import ProfileView from "../../../assets/svg/IcProfileView";

import QuillText from "../../CardElements/QuillText";

import MobileSocialBtn from "../moblieComponents/mobileSocialBtn";

import InfoSectionGridHeader from "./InfoSectionGridHeader";

import ShareButton from "../../Buttons/ShareButton";

import DefaultModal from "../../Modal/DefaultModal";
import AddLabelSave from "../../CardElements/AddLabelSave";

import FlatDefaultBtn from "../../Buttons/FlatDefaultBtn";

import ActionBtn from "../../Buttons/ActionBtn";

import EditProfilePage from "../../EditProfilePage";
import ProfileHeader from "./ProfileHeader";

import PreProfileHeader from "./PreProfileHeader";

import IcCertification from "../../../assets/svg/IcCertification";
import MoreOptionIcon from "../../../assets/svg/IcMoreOptionIcon";

import { withRouter } from "react-router-dom";

import { jobLike, jobFollow } from "../../../api/jobs/jobExtras";
import isProfileCreated from "../../../utils/env/isProfileCreated";

import P2PTracker from "../../P2PTracker";
import { USER } from "../../../constants/api";

const editIcon = <IcEditIcon pathcolor="#c8c8c8" />;

const processSkillsOptional = skills => {
  if (!skills) {
    return null;
  }

  const skillsName = skills.map(val => {
    return {
      name: val.name,
      exp: val.exp
    };
  });

  const skillComponent = skillsName.map(val => {
    if (!val) {
      return;
    }

    return (
      <div className="filterSkill skillBtn" style={{ display: "inline-block" }}>
        <List horizontal className="text-night-rider">
          <List.Item key={val.name}>
            <Button compact>
              {val.exp ? (
                <React.Fragment>
                  {val.name} |<span> {val.exp} yrs</span>
                </React.Fragment>
              ) : (
                <React.Fragment>{val.name}</React.Fragment>
              )}
            </Button>
          </List.Item>
        </List>
      </div>
    );
  });

  return skillComponent;
};

const months = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "June",
  "July",
  "Aug",
  "Sept",
  "Oct",
  "Nov",
  "Dec"
];

const processExperience = exp => {
  if (!exp || !exp.length || !exp[0].name) {
    return null;
  }

  const expArray = exp.map(val => {
    return {
      desc: val.desc,
      loc: val.loc,
      designation: val.designation,
      name: val.name,
      doj: {
        month: val.doj.month,
        year: val.doj.year
      },
      dor: {
        month: val.dor.month,
        year: val.dor.year
      },
      current: val.current
    };
  });

  const expComponent = expArray.map(exp => {
    if (!exp.name || !exp.designation) {
      return null;
    }

    return (
      <List.Item className="padding-top-10">
        <List.Content>
          <Feed>
            <Feed.Event>
              {/* <Feed.Label image={java} /> */}
              <span className="educationThumb margin-right-10">
                <CompanySvg
                  width="40"
                  height="40"
                  rectcolor="#f7f7fb"
                  pathcolor="#c8c8c8"
                />
              </span>
              <Feed.Content className="expDescription">
                <List>
                  <List.Item>
                    <p className="expDescriptionHeader vasitumFontOpen-500">
                      {exp.designation}
                      {/* exp.designation */}
                    </p>
                    <p className="expDescriptionSub">{exp.name}</p>
                  </List.Item>
                  <List.Item>
                    <p className="font-size-13 text-trolley-grey font-f-open-sans">
                      {(function(exp) {
                        let str = [];
                        if (exp.current) {
                          if (exp.doj.month) {
                            str.push(exp.doj.month);
                            str.push(" ");
                          }

                          if (exp.doj.year) {
                            str.push(exp.doj.year);
                            str.push(" ");
                            str.push("-");
                            str.push(" ");
                            str.push("Present");
                          }

                          return str.join("");
                        }

                        if (exp.doj.month) {
                          str.push(exp.doj.month);
                          str.push(" ");
                        }

                        if (exp.doj.year) {
                          str.push(exp.doj.year);
                          str.push(" ");
                        }

                        if (exp.dor.month) {
                          str.push("-");
                          str.push(" ");
                          str.push(months[exp.dor.month - 1]);
                        }

                        if (exp.dor.year) {
                          if (!exp.dor.month) {
                            str.push("-");
                          }
                          str.push(" ");
                          str.push(exp.dor.year);
                        }

                        return str.join("");
                      })(exp)}
                    </p>
                  </List.Item>
                  <List.Item>
                    <p className="text-a1">{exp.loc}</p>
                  </List.Item>
                </List>
                <div className="expSummary">
                  {/* {exp.desc} */}
                  <QuillText value={exp.desc} placeholder="" isMute readOnly />
                </div>
              </Feed.Content>
            </Feed.Event>
          </Feed>
        </List.Content>
      </List.Item>
    );
  });

  return expComponent;
};

const processEducation = education => {
  if (!education || education.length === 0) {
    return (
      <List.Item>
        <p
          style={{
            fontFamily: "Open Sans",
            fontSize: "14px",
            color: "#c8c8c8",
            textAlign: "left",
            lineHeight: "22px",
            fontStyle: "normal"
          }}>
          What's your educational background?
          <br />
          Let employers know more about your education; remember, be clear and
          concise.
        </p>
      </List.Item>
    );
    return null;
  }

  if (
    education.length === 1 &&
    !education[0].course &&
    !education[0].institute
  ) {
    return (
      <List.Item>
        <p
          style={{
            fontFamily: "Open Sans",
            fontSize: "14px",
            color: "#c8c8c8",
            textAlign: "left",
            lineHeight: "22px",
            fontStyle: "normal"
          }}>
          What's your educational background?
          <br />
          Let employers know more about your education; remember, be clear and
          concise.
        </p>
      </List.Item>
    );
  }

  const eduArry = education.map(val => {
    return {
      course: val.course,
      spec: val.specs[0],
      level: val.level,
      started: val.started,
      completed: val.completed,
      institute: val.institute,
      type: "Full Time"
    };
  });

  const eduArrayComponent = eduArry.map(val => {
    return (
      <div
        className=""
        style={{
          display: !val.course && !val.institute ? "none" : "block"
        }}>
        <Grid>
          <Grid.Row>
            <Grid.Column mobile={3} computer={1}>
              <List.Content className="">
                <span className="educationThumb">
                  <EducationLogo
                    width="40"
                    height="40"
                    rectcolor="#f7f7fb"
                    pathcolor="#c8c8c8"
                  />
                </span>
              </List.Content>
            </Grid.Column>
            <Grid.Column mobile={13} computer={15}>
              <List.Content className="expDescription padding-left-5">
                <List.Header>
                  <p className="expDescriptionHeader">{val.institute}</p>
                </List.Header>
                <List.Description>
                  <p className="margin-bottom-0 padding-bottom-5 expDescriptionSub">
                    {val.course}{" "}
                    {val.level && (
                      <span
                        style={{
                          color: "#a1a1a1"
                        }}>
                        | {val.level}
                      </span>
                    )}
                  </p>
                  {val.spec ? (
                    <p className="margin-bottom-0 padding-bottom-5 expDescriptionSub">
                      {val.spec}{" "}
                    </p>
                  ) : null}
                  <p
                    style={{
                      display:
                        Number(val.started) && Number(val.completed)
                          ? "block"
                          : "none"
                    }}
                    className="text-trolley-grey font-size-13">
                    {val.started + " - " + val.completed}
                  </p>
                </List.Description>
              </List.Content>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    );
  });

  return eduArrayComponent;
};

const processCertificate = certificate => {
  if (!certificate || certificate.length === 0) {
    return (
      <List.Item>
        <p
          style={{
            fontFamily: "Open Sans",
            fontSize: "14px",
            color: "#c8c8c8",
            textAlign: "left",
            lineHeight: "22px",
            fontStyle: "normal"
          }}>
          What's your certificateal background?
          <br />
          Let employers know more about your certificate; remember, be clear and
          concise.
        </p>
      </List.Item>
    );
    return null;
  }

  if (certificate.length === 1 && !certificate[0].certificate_name) {
    return (
      <List.Item>
        <p
          style={{
            fontFamily: "Open Sans",
            fontSize: "14px",
            color: "#c8c8c8",
            textAlign: "left",
            lineHeight: "22px",
            fontStyle: "normal"
          }}>
          What's your certificate background?
          <br />
          Let employers know more about your certificate; remember, be clear and
          concise.
        </p>
      </List.Item>
    );
  }

  const certArry = certificate.map(val => {
    return {
      name: val.certificate_name,
      institute: val.cert_Institute,
      date: val.receive_date
    };
  });

  const certArrayComponent = certArry.map(val => {
    return (
      <div
        className=""
        style={{
          display: !val.name ? "none" : "block"
        }}>
        <Grid>
          <Grid.Row>
            <Grid.Column mobile={3} computer={1}>
              <List.Content className="">
                <span className="educationThumb">
                  <IcCertification
                    width="40"
                    height="40"
                    rectcolor="#f7f7fb"
                    pathcolor="#c8c8c8"
                    circleColor="#f7f7fb"
                  />
                </span>
              </List.Content>
            </Grid.Column>
            <Grid.Column mobile={13} computer={15}>
              <List.Content className="expDescription padding-left-5">
                <List.Header>
                  <p className="expDescriptionHeader">{val.name}</p>
                </List.Header>
                <List.Description>
                  <p className="margin-bottom-0 padding-bottom-5 expDescriptionSub">
                    {val.institute}{" "}
                  </p>
                  <p
                    style={{
                      display: Number(val.date) ? "block" : "none"
                    }}
                    className="text-trolley-grey font-size-13">
                    {val.date}
                  </p>
                </List.Description>
              </List.Content>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    );
  });

  return certArrayComponent;
};

function getInfoEdu(data) {
  if (!data || !data.edus || !Array.isArray(data.edus)) {
    return null;
  }

  const infoEdu = data.edus.filter(val => {
    return val.onInfo === true;
  });

  if (!infoEdu.length) {
    return null;
  }

  if (!infoEdu[0].institute) {
    return null;
  }

  return {
    institute: infoEdu[0].institute
  };
}

function getInfoExp(data) {
  if (!data || !data.exps || !Array.isArray(data.exps)) {
    return null;
  }

  const infoExp = data.exps.filter(val => {
    return val.onInfo === true;
  });

  if (!infoExp.length) {
    return null;
  }

  if (!infoExp[0].name) {
    return null;
  }

  return {
    name: infoExp[0].name,
    designation: infoExp[0].designation
  };
}

class PublicProfileView extends React.Component {
  constructor(props) {
    super(props);
    this.onLikeClick = this.onLikeClick.bind(this);
  }

  state = {
    isLiked: false,
    isSaveOpen: false,
    isFollow: false
  };

  async onLikeClick(ev, { id }) {
    ev.stopPropagation();

    if (!isProfileCreated()) {
      return;
    }

    try {
      const res = await jobLike(id);
      const data = await res.text();

      // console.log(data);
      this.setState({
        isLiked: true
      });
    } catch (error) {
      console.error(error);
    }
  }

  async onFollowClick(ev, { id }) {
    ev.stopPropagation();
    if (!isProfileCreated()) {
      return;
    }

    try {
      const res = await jobFollow(id);
      const data = await res.text();

      // console.log(data);
      this.setState({
        isLiked: true
      });
    } catch (error) {}
  }

  onModalSaveOpen = ev => {
    if (!isProfileCreated()) {
      return;
    }

    this.setState({
      isSaveOpen: true
    });
  };

  onModalSaveClose = ev => {
    this.setState({
      isSaveOpen: false
    });
  };

  onSaveClick = ev => {
    this.setState({
      isSaveOpen: false
    });
  };

  onChatStart = (ev, { tohref }) => {
    this.props.history.push("/messaging/" + tohref);
  };

  render() {
    let user = this.props.user || { loc: {} };
    // console.log("check", user);

    const infoExp = getInfoExp(user);
    const infoEdu = getInfoEdu(user);

    const {
      editModalActiveSection,
      isPreview,
      onSaveClick,
      isUser,
      liked,
      saved,
      applied,
      follow,
      onLike,
      onSave,
      onUnSave,
      onFollow,
      apiDone
    } = this.props;

    const trigger = (
      <span className="moblile__moreOptionBtn">
        <MoreOptionIcon
          width="5"
          height="18.406"
          pathcolor="#797979"
          viewbox="0 0 5 18.406"
        />
      </span>
    );

    const options = [
      { key: "Like", text: "Like" },
      { key: "Share", text: "Share" },
      { key: "Save", text: "Save" }
    ];

    return (
      <div className="bg-white PublicProfileView PublicProfileView_detailContainer">
        {isPreview ? (
          <PreProfileHeader user={user} />
        ) : (
          <ProfileHeader
            history={this.props.history}
            isLiked={this.state.isLiked}
            saveOpen={this.state.isSaveOpen}
            onLikeClick={this.onLikeClick}
            onModalOpen={this.onModalSaveOpen}
            onModalClose={this.onModalSaveClose}
            user={user}
            isUser={isUser}
            liked={liked}
            saved={saved}
            follow={follow}
            applied={applied}
            onLike={onLike}
            onSave={onSave}
            onUnSave={onUnSave}
            onFollow={onFollow}
            apiDone={apiDone}
          />
        )}
        <div className="preProfileDetail_body mobile__publicProfile" style={{}}>
          <div className="divider">
            <div className="divider-border" />
          </div>

          <div className="preProfileDetailBody" style={{ paddingTop: "20px" }}>
            <InfoSectionGridHeader
              headerText="Summary"
              style={{
                display:
                  !user.desc || user.desc.length === 11 ? "none" : "block"
              }}>
              {/* <p className="headerSubTitle">{user.desc}</p> */}
              <QuillText
                readMoreLength={200}
                value={user.desc ? user.desc : ""}
                readOnly
              />
            </InfoSectionGridHeader>

            <InfoSectionGridHeader
              headerText="Skill"
              style={{
                display: !user.skills || !user.skills.length ? "none" : "block"
              }}
              containerClass="filterSkill">
              {processSkillsOptional(user.skills)}
            </InfoSectionGridHeader>

            <List className="padding-top-5 expwrience-detaile">
              <List.Item
                style={{
                  display:
                    !user.exps || !user.exps.length || !user.exps[0].name
                      ? "none"
                      : "block"
                }}>
                <List.Content>
                  <List.Header>
                    <Header className="preHeaderTitle" as="h3">
                      Experience
                    </Header>
                  </List.Header>
                </List.Content>
              </List.Item>

              {processExperience(user.exps)}

              <List.Item
                className="padding-top-10 educationPreProfile"
                style={{
                  display:
                    !user.edus || !user.edus.length || !user.edus[0].institute
                      ? "none"
                      : "block"
                }}>
                <List.Content className="padding-bottom-15">
                  <List.Header>
                    <Header
                      className="preHeaderTitle padding-bottom-15"
                      as="h3">
                      Education
                    </Header>
                  </List.Header>
                  {processEducation(user.edus)}
                </List.Content>
              </List.Item>

              <List.Item
                className="padding-top-10 educationPreProfile"
                style={{
                  display:
                    !user.certs ||
                    !user.certs ||
                    !user.certs[0].certificate_name
                      ? "none"
                      : "block"
                }}>
                <List.Content className="padding-bottom-15">
                  <List.Header>
                    <Header
                      className="preHeaderTitle padding-bottom-15"
                      as="h3">
                      Certificate
                    </Header>
                  </List.Header>
                  {processCertificate(user.certs)}
                </List.Content>
              </List.Item>
            </List>

            <div className="">
              {isPreview ? (
                <div className="alingRight mobileAlingCenter">
                  <FlatDefaultBtn
                    onClick={() => this.props.history.go(-1)}
                    className="bgTranceparent"
                    btntext="Go Back"
                  />
                  {/* <ActionBtn
                    actioaBtnText="Create"
                    disabled={!user.canSubmit}
                    onClick={onSaveClick}
                  /> */}
                </div>
              ) : (
                <div className="alingRight mobileAlingCenter">
                  {/* only Desktop */}
                  <span className="mobile hidden">
                    {/* <SocialBtn
                      objectID={user._id}
                      onLikeClick={this.onLikeClick}
                    /> */}
                    {/* <Button
                      disabled={isUser}
                      size="small"
                      className="actionBtn"
                      tohref={user._id}
                      onClick={this.onChatStart}>
                      Message <Icon name="angle right" />
                    </Button> */}
                  </span>
                  {/* only Desktop end*/}

                  {/* only mobile*/}
                  {/* <MobileSocialBtn
                    btntext="Message"
                    tohref={user._id}
                    onChatStart={this.onChatStart}
                  /> */}

                  <span className="widescreen hidden large screen hidden computer hidden tablet hidden">
                    {/* <Button
                      disabled={isUser}
                      size="small"
                      className="actionBtn"
                      tohref={user._id}
                      onClick={this.onChatStart}>
                      Message <Icon name="angle right" />
                    </Button> */}
                  </span>
                  {/* only mobile*/}
                </div>
              )}
            </div>
          </div>
        </div>
        <div className="">
          {user._id ? (
            <P2PTracker
              reqId={window.localStorage.getItem(USER.UID)}
              userId={user._id}
              cardType={"FooterP2P"}
            />
          ) : null}
        </div>
      </div>
    );
  }
}

export default withRouter(PublicProfileView);
