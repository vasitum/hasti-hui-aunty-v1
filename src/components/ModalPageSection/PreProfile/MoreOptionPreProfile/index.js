import React from "react";
import { Link } from "react-router-dom";
import { Button, Dropdown } from "semantic-ui-react";
import ShareModal from "../../../ShareButton/ShareModal";
import UserAccessModal from "../../UserAccessModal";
// const MoreOptionSocialMenu = props => {
class MoreOptionSocialMenu extends React.Component {
  state = { modalOpen: false };
  handleModal = () => this.setState({ modalOpen: !this.state.modalOpen });

  render() {
    const { user, mIsLogged } = this.props;
    return (
      <div className="MoreOptionSocialMenu">
        <Button className="has-hover-blue moreOptionBtn">
          <Dropdown icon="ellipsis vertical" pointing="bottom right">
            <Dropdown.Menu>
              {/* {UserId ? (
              <Dropdown.Item as={Link} to={`/user/public/${UserId._id}`}>
                Public Profile
             </Dropdown.Item>
            ) : null } */}
              {mIsLogged ? (
                <UserAccessModal
                  modalTrigger={<Dropdown.Item>Share</Dropdown.Item>}
                  isAction
                  actionText="share"
                  entity={user.fName}
                />
              ) : (
                <Dropdown.Item onClick={this.handleModal}>Share</Dropdown.Item>
              )}

              {/* <Dropdown.Item as={Link} to={`/cv-template`}>
              Share
            </Dropdown.Item> */}
            </Dropdown.Menu>
          </Dropdown>
        </Button>
        <ShareModal
          modalOpen={this.state.modalOpen}
          handleModal={this.handleModal}
          url={`/view/user/${user._id}`}
        />
      </div>
    );
  }
}

export default MoreOptionSocialMenu;
