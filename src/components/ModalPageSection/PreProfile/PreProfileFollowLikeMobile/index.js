import React from "react";

import LikeLogo from "../../../../assets/svg/IcLike";
import IcFollowersIcon from "../../../../assets/svg/IcFollowersIcon";

import "./index.scss";

const PreProfileFollowLikeMobile = ({
  user,
  follow,
  liked,
  onFollow,
  onLike
}) => {
  return (
    <div className="PreProfileFollowLikeMobile">
      <div className="FollowLikeMobile_inner">
        <p onClick={onFollow}>
          <IcFollowersIcon
            width="18"
            height="11"
            pathcolor1="#c8c8c8"
            pathcolor2=" #c8c8c8"
          />{" "}
          Followers:{" "}
          <span>
            {!user.followerCount
              ? follow
                ? 1
                : 0
              : follow
              ? user.followerCount + 1
              : user.followerCount}
          </span>
        </p>
      </div>
      {/* <div className="FollowLikeMobile_inner">
        <p onClick={onLike}>
          <LikeLogo width="17" height="11" pathcolor="#c8c8c8" /> Like:{" "}
          <span>
            {!user.likeCount
              ? liked
                ? 1
                : 0
              : liked
              ? user.likeCount + 1
              : user.likeCount}
          </span>
        </p>
      </div> */}
    </div>
  );
};

export default PreProfileFollowLikeMobile;
