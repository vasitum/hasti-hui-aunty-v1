import React from "react";
import {
  List,
  Button,
  Icon,
  Image,
  Grid,
  Header,
  Dropdown,
  Responsive
} from "semantic-ui-react";
import ShareModal from "../../../ShareButton/ShareModal";
import publicProfileBanner from "../../../../assets/img/publicProfileBanner.png";
import UserLogo from "../../../../assets/svg/IcUser";
import LikeLogo from "../../../../assets/svg/IcLike";
// import ShareLogo from "../../../../assets/svg/IcShare";
// import HeaderBanner from "../HeaderBanner";

import EducationNoCircleIcon from "../../../../assets/svg/IcEducationNoCircle";
import LocationLogo from "../../../../assets/svg/IcLocation";
import ProfileView from "../../../../assets/svg/IcProfileView";
// import ShareButton from "../../../Buttons/ShareButton";

import SocialBtn from "../../Buttons/socialBtn";

import DefaultModal from "../../../Modal/DefaultModal";
import AddLabelSave from "../../../CardElements/AddLabelSave";
// import MobileSocialBtn from "../../moblieComponents/mobileSocialBtn";

import UserAccessModal from "../../UserAccessModal";
import isLoggedIn from "../../../../utils/env/isLoggedin";

import MoreOptionPreProfile from "../MoreOptionPreProfile";

import PreProfileFollowLikeMobile from "../PreProfileFollowLikeMobile";

import IcFollowersIcon from "../../../../assets/svg/IcFollowersIcon";

import "./index.scss";

function getInfoEdu(data) {
  if (!data || !data.edus || !Array.isArray(data.edus)) {
    return null;
  }

  const infoEdu = data.edus.filter(val => {
    return val.onInfo === true;
  });

  if (!infoEdu.length) {
    return null;
  }

  if (!infoEdu[0].institute) {
    return null;
  }

  return {
    institute: infoEdu[0].institute
  };
}

function getInfoExp(data) {
  if (!data || !data.exps || !Array.isArray(data.exps)) {
    return null;
  }

  const infoExp = data.exps.filter(val => {
    return val.onInfo === true;
  });

  if (!infoExp.length) {
    return null;
  }

  if (!infoExp[0].name) {
    return null;
  }

  return {
    name: infoExp[0].name,
    designation: infoExp[0].designation
  };
}

// const ProfileHeader = props => {
class ProfileHeader extends React.Component {
  state = { modalOpen: false };
  handleModal = () => this.setState({ modalOpen: !this.state.modalOpen });
  render() {
    let user = this.props.user || { loc: {} };

    const onChatStart = (ev, { tohref }) => {
      this.props.history.push("/messaging/" + tohref);
    };

    const {
      isUser,
      liked,
      saved,
      onLike,
      onSave,
      onUnSave,
      follow,
      onFollow,
      apiDone
    } = this.props;

    const infoExp = getInfoExp(user);
    const infoEdu = getInfoEdu(user);
    const mIsLogged = isLoggedIn();
    return (
      <div>
        {/* <Responsive minWidth={1025}>
        <HeaderBanner />
      </Responsive> */}

        <List className="publicProfileBanner">
          <List.Item>
            <List.Content>
              <div className="publicPprofileBanner" />

              <div
                className="PublicProfileBanner_img"
                style={{
                  backgroundImage: `linear-gradient(
                    rgba(0,0,0,0.0),
                    rgba(0,0,0,0.5)
                  ), url(${
                    user.ext && user.ext.imgBanner
                      ? `https://s3-us-west-2.amazonaws.com/img-mwf/banner/${
                      user._id
                      }/image.png`
                      : publicProfileBanner
                    })`,
                  height: "220px",
                  backgroundSize: "cover",
                  backgroundPosition: "center"
                }}
              />
            </List.Content>
          </List.Item>
        </List>
        <div
          className="preProfileDetail publicProfile_detailContainer"
          style={{ paddingBottom: "0px" }}>
          <List className="publicProfile_detailContainerList">
            <List.Item className="social_containerList">
              <Grid>
                <Grid.Row className="preProfileDetail_row">
                  <Grid.Column width={8} className="mobile hidden">
                    <List horizontal>
                      <List.Item>
                        <List.Content className="text-white">
                          <span className="padding-right-3">
                            <IcFollowersIcon
                              width="18"
                              height="11"
                              pathcolor1="#c8c8c8"
                              pathcolor2=" #c8c8c8"
                            />
                          </span>
                          Followers:{" "}
                          {!user.followerCount
                            ? follow
                              ? 1
                              : 0
                            : follow
                              ? user.followerCount + 1
                              : user.followerCount}
                        </List.Content>

                      </List.Item>
                    </List>
                  </Grid.Column>
                  <Grid.Column
                    width={8}
                    className="mobile hidden"
                    textAlign="right"
                    style={{
                      paddingRight: "0px"
                    }}>
                    <List.Content>
                      <List.Header className="preProfileHeaderBtn alingCenterRight">
                        {mIsLogged ? (
                          <UserAccessModal
                            modalTrigger={
                              <Button
                                disabled={isUser}
                                size="small"
                                onClick={onFollow}
                                className="actionBtn">
                                {follow ? "Following" : "Follow"}{" "}
                              </Button>
                            }
                            isAction
                            // actionText="Follow"
                            entity={user.fName}
                          />
                        ) : (<Button
                          disabled={isUser}
                          size="small"
                          onClick={onFollow}
                          className="actionBtn">
                          {follow ? "Following" : "Follow"}{" "}
                        </Button>)}

                        {/* {mIsLogged ? (
                          <UserAccessModal
                            modalTrigger={
                              <Button
                                size="small"
                                className="publicProfile_followBtn">
                                Message
                              </Button>
                            }
                            isAction
                            actionText="message"
                            entity={user.fName}
                          />
                        ) : (
                          <Button
                            size="small"
                            onClick={onFollow}
                            className="actionBtn">
                            {follow ? "Following" : "Follow"}{" "}
                          </Button>
                        )} */}
                        {mIsLogged ? (
                          <UserAccessModal
                            modalTrigger={
                              <Button
                                size="small"
                                className="publicProfile_followBtn">
                                Message
                              </Button>
                            }
                            isAction
                            // actionText="message"
                            entity={user.fName}
                          />
                        ) : (
                            <Button
                              disabled={isUser}
                              size="small"
                              tohref={user._id}
                              onClick={onChatStart}
                              className="publicProfile_followBtn">
                              Message
                          </Button>
                          )}
                        <Button className="has-hover-blue">
                          <Dropdown icon="ellipsis vertical">
                            <Dropdown.Menu>
                              {mIsLogged ? (
                                <UserAccessModal
                                  modalTrigger={
                                    <Dropdown.Item>Share</Dropdown.Item>
                                  }
                                  isAction
                                  // actionText="share"
                                  entity={user.fName}
                                />
                              ) : (
                                  <Dropdown.Item>
                                    {/* <DefaultModal
                                  size="tiny"
                                  open={props.saveOpen}
                                  trigger={
                                    <span onClick={props.onModalOpen}>
                                      Share
                                    </span>
                                  }>
                                  <AddLabelSave
                                    saveObjectId={user._id}
                                    onCloseBtnClick={props.onModalClose}
                                    onSaveClick={() => { 
                                      onSave();
                                      props.onModalClose();
                                    }}
                                    type={"user"}
                                  />
                                </DefaultModal> */}
                                    <Dropdown.Item onClick={this.handleModal}>
                                      Share
                                  </Dropdown.Item>
                                  </Dropdown.Item>
                                )}
                            </Dropdown.Menu>
                          </Dropdown>
                        </Button>
                      </List.Header>
                    </List.Content>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </List.Item>
            <List.Item className="Profile_containerList">
              <List.Content>
                <List.Item className="preProfileHeaderDetail alingCenter">
                  <span style={{ width: "100%", margin: "0 auto" }}>
                    <span className="userImgThumb">
                      {!user.ext || !user.ext.img ? (
                        <React.Fragment>
                          <UserLogo
                            width="110"
                            height="110"
                            rectcolor="#f7f7fb"
                            pathcolor="#c8c8c8"
                          />
                        </React.Fragment>
                      ) : (
                          <Image
                            src={`https://s3-us-west-2.amazonaws.com/img-mwf/${
                              user._id
                              }/image.jpg`}
                            centered
                            style={{
                              borderRadius: "50%"
                            }}
                            alt={user.fName}
                          />
                        )}
                    </span>
                  </span>
                  <List.Content>
                    <List.Header className="preProfileTitle">
                      <Header as="h1">
                        {user.fName} {user.lName}
                      </Header>
                      <p>{user.title}</p>
                    </List.Header>
                    <List.Content>
                      <p
                        className="text-nobel mtb-2px"
                        style={{
                          display:
                            infoExp && infoExp.designation ? "block" : "none"
                        }}>
                        <span style={{ color: "#575757" }}>
                          {infoExp ? infoExp.designation : ""}
                        </span>{" "}
                        at{" "}
                        <span style={{ color: "#575757" }}>
                          {infoExp ? infoExp.name : ""}
                        </span>
                      </p>
                      <p
                        className=" mt4b2"
                        style={{
                          display:
                            infoEdu && infoEdu.institute ? "block" : "none"
                        }}>
                        <span className="padding-right-3">
                          <EducationNoCircleIcon
                            width="17.032"
                            height="12"
                            pathcolor="#c8c8c8"
                          />
                        </span>
                        {infoEdu ? infoEdu.institute : ""}
                      </p>
                      <p
                        className="text-Matterhorn"
                        style={{
                          display:
                            !user.loc || !user.loc.city ? "none" : "flex",
                          alignItems: "stretch",
                          justifyContent: "center"
                        }}>
                        <span className="padding-right-5">
                          <LocationLogo
                            width="9.5"
                            height="11.5"
                            pathcolor="#c8c8c8"
                          />
                        </span>
                        <span style={{ color: "#797979" }}>
                          {user.loc ? user.loc.city : ""}
                        </span>
                      </p>
                      <span className="Mobile_socialBtnContainer  widescreen hidden large screen hidden computer hidden tablet hidden">
                        {mIsLogged ? (
                          <UserAccessModal
                            modalTrigger={
                              <Button size="tiny" className="actionBtn">
                                {follow ? "Following" : "Follow"}
                              </Button>
                            }
                            isAction
                            // actionText="follow"
                            entity={user.fName}
                          />
                        ) : (
                            <Button
                              onClick={onFollow}
                              disabled={isUser}
                              size="tiny"
                              className="actionBtn">
                              {follow ? "Following" : "Follow"}
                            </Button>
                          )}

                        {mIsLogged ? (
                          <UserAccessModal
                            modalTrigger={
                              <Button size="tiny" className="Mobile_followBtn">
                                Message
                              </Button>
                            }
                            isAction
                            // actionText="message"
                            entity={user.fName}
                          />
                        ) : (
                            <Button
                              disabled={isUser}
                              size="tiny"
                              className="Mobile_followBtn"
                              tohref={user._id}
                              onClick={onChatStart}>
                              Message
                          </Button>
                          )}

                        {/* {apiDone ? (
                        <div className="socialLike_container">
                          <SocialBtn
                            disabled={isUser}
                            objectID={user._id}
                            onLikeClick={onLike}
                            liked={liked}
                            saved={saved}
                            type="user"
                          />
                        </div>
                        
                      ) : null} */}
                        <span className="search-job-list-share">
                          <MoreOptionPreProfile
                            user={user}
                            mIsLogged={mIsLogged}
                          />
                        </span>
                      </span>
                      {/* <MobileSocialBtn btntext="Message" /> */}
                    </List.Content>
                  </List.Content>

                  <Responsive maxWidth={1024}>
                    <PreProfileFollowLikeMobile
                      user={user}
                      follow={follow}
                      liked={liked}
                      onFollow={onFollow}
                      onLike={onLike}
                    />
                  </Responsive>
                  {/* {apiDone ? (
                  <div className="socialLike_container">
                    <SocialBtn
                      disabled={isUser}
                      objectID={user._id}
                      onLikeClick={onLike}
                      liked={liked}
                      saved={saved}
                      type="user"
                    />
                  </div>
                  
                ) : null} */}
                </List.Item>
              </List.Content>
            </List.Item>
          </List>
        </div>

        <ShareModal
          modalOpen={this.state.modalOpen}
          handleModal={this.handleModal}
          url={`/view/user/${user._id}`}
        />
      </div>
    );
  }
}

export default ProfileHeader;
