import React from "react";
import PropTypes from "prop-types";
import { Header, Grid, Button, Popup } from "semantic-ui-react";

import IcEditIcon from "../../../../assets/svg/IcEdit";

import "./index.scss";

const InfoSectionGridHeader = props => {
  const {
    headerClasses,
    color,
    headerText,
    children,
    containerClass,
    editIcon,
    onEditIconClick,
    style
  } = props;

  // Our svgs are react elements hence needs captialization

  return (
    <div className={containerClass + " InfoSectionGridHeader"} style={style}>
      <div className="header-container">
        <Grid>
          <Grid.Row>
            <Grid.Column width="10">
              <Header
                className={headerClasses + " header-item"}
                color={color}
                as="h2">
                {headerText}
              </Header>
            </Grid.Column>
            <Grid.Column width="6" textAlign="right">
              {/* <Popup
                trigger={
                  
                }
                className="socialPropup"
                content="Edit"
              /> */}
              <Button
                onClick={onEditIconClick}
                compact
                className="header_editBtn ">
                {editIcon}
              </Button>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
      <div>{children}</div>
    </div>
  );
};

// infoSectionGridHeader.propTypes = {};

// infoSectionGridHeader.defaultProps = {};

export default InfoSectionGridHeader;
