import React from "react";
import { Grid, Container, Responsive } from "semantic-ui-react";
import PublicProfileView from "./publicProfile";
import ProfileRightSide from "./profileRightSide";
// import getUserById from "../../../api/user/getUserById";

import getUserByIdCount from "../../../api/user/getUserByIdCount";

import PreviewHeaderNotification from "../PreviewHeaderNotification";
import { USER } from "../../../constants/api";
import isProfileCreated from "../../../utils/env/isProfileCreated";
import ReactGA from "react-ga";
import HeaderBanner from "./HeaderBanner";

// import getProfile from "Api/user/getProfile";
import {
  jobLike,
  jobSave,
  jobFollow,
  getLikedAndSavedJobs
} from "../../../api/jobs/jobExtras";

import "./index.scss";

const Loader = props => (
  <div className="m_showbox">
    <div className="m_loader">
      <svg className="m_circular" viewBox="25 25 50 50">
        <circle
          className="m_path"
          cx="50"
          cy="50"
          r="20"
          fill="none"
          stroke-width="2"
          stroke-miterlimit="10"
        />
      </svg>
    </div>
  </div>
);

class PublicProfile extends React.Component {
  state = {
    isLoading: false,
    profileJson: {},
    liked: false,
    saved: false,
    applied: false,
    apiDone: false,
    follow: false
  };

  constructor(props) {
    super(props);

    // bind
    this.onLike = this.onLike.bind(this);
    this.onUnSave = this.onUnSave.bind(this);
    this.onFollow = this.onFollow.bind(this);
  }

  async componentDidMount() {
    const { match } = this.props;
    
    ReactGA.pageview(`${match.url}`);
    try {
      const res = await getUserByIdCount(match.params.id);
      const resJson = await res.json();

      this.setState({
        profileJson: resJson,
        isLoading: false
      });

      const datares = await getLikedAndSavedJobs([match.params.id], "user");
      if (datares.status === 200) {
        const data = await datares.json();
        const dataVal = data[match.params.id];
        this.setState({
          liked: dataVal.like,
          saved: dataVal.saveJobs,
          applied: dataVal.applied,
          follow: dataVal.following,
          apiDone: true
        });
      } else {
        // console.log(datares);
        this.setState({ apiDone: true });
      }
    } catch (error) {
      console.error(error);
    }
  }

  async onLike(e) {
    const { match } = this.props;

    if (!isProfileCreated()) {
      return;
    }

    try {
      const res = await jobLike(match.params.id, "user");
      if (res.status === 200) {
        this.setState({ liked: !this.state.liked });
        const data = await res.text();
        // console.log(data);
      } else {
        console.log(res);
      }
    } catch (error) {
      console.error(error);
    }
  }

  async onUnSave(e) {
    const { match } = this.props;
    try {
      const res = await jobSave(match.params.id, [], "user");
      if (res.status === 200) {
        this.setState({ saved: !this.state.saved });
        const data = await res.text();
        // console.log(data);
      } else {
        console.log(res);
      }
    } catch (error) {
      console.log(error);
    }
  }

  async onFollow() {
    const { match } = this.props;

    if (!isProfileCreated()) {
      return;
    }

    try {
      const res = await jobFollow(match.params.id, "user");
      if (res.status === 200) {
        this.setState({ follow: !this.state.follow });
        const data = await res.text();
        // console.log(data);
      } else {
        console.log(res);
      }
    } catch (error) {
      console.error(error);
    }
  }

  onSave = e => {
    if (!isProfileCreated()) {
      return;
    }

    this.setState({ saved: !this.state.saved });
  };

  componentDidUpdate() {
    const { profileJson } = this.state;
    const { match } = this.props;

    if (!Object.prototype.hasOwnProperty.call(profileJson, "_id")) {
      return;
    }

    if (!match || !match.params || !match.params.id) {
      return;
    }

    const propsId = match.params.id;
    const jobDataId = profileJson._id;
    if (propsId === jobDataId) {
      return;
    }

    this.componentDidMount();
  }

  render() {
    if (this.state.isLoading) {
      return (
        <div>
          <Loader />
        </div>
      );
    } else {
      const isUser =
        this.state.profileJson._id === window.localStorage.getItem(USER.UID);
      return (
        <div className="public-profile">
          {isUser && <PreviewHeaderNotification publicTitle="profile" />}
          {/* <Responsive maxWidth={1024}>
            <HeaderBanner />
          </Responsive> */}

          <Container>
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} computer={12}>
                  <PublicProfileView
                    isUser={isUser}
                    user={this.state.profileJson}
                    liked={this.state.liked}
                    follow={this.state.follow}
                    saved={this.state.saved}
                    applied={this.state.applied}
                    onLike={this.onLike}
                    onSave={this.onSave}
                    onFollow={this.onFollow}
                    onUnSave={this.onUnSave}
                    apiDone={this.state.apiDone}
                  />
                </Grid.Column>
                <Grid.Column width={4} className="mobile hidden">
                  <ProfileRightSide
                    userId={
                      this.state.profileJson ? this.state.profileJson._id : null
                    }
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Container>
        </div>
      );
    }
  }
}

export default PublicProfile;
