import React from "react";

import {List} from "semantic-ui-react";

import publicProfileBanner from "../../../../assets/img/publicProfileBanner.png";

const HeaderBanner = props => {
  let user = props.user || { loc: {} };

  if(!user){
    return null;
  }
  return(
    <div className="HeaderBanner">
      <List className="publicProfileBanner">
        <List.Item>
          <List.Content>
            <div className="publicPprofileBanner" />
            
            <div
              style={{
                backgroundImage: `linear-gradient(
                    rgba(0,0,0,0.0),
                    rgba(0,0,0,0.5)
                  ), url(${
                    user.ext && user.ext.imgBanner
                      ? `https://s3-us-west-2.amazonaws.com/img-mwf/banner/${
                          user._id
                        }/image.png`
                      : publicProfileBanner
                  })`,
                height: "220px",
                backgroundSize: "cover",
                backgroundPosition: "center"
              }}
            />
          </List.Content>
        </List.Item>
        
      </List>
    </div>
  )
}

export default HeaderBanner;