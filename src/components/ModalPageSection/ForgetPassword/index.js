import React from "react";
import {
  List,
  Button,
  Icon,
  Input,
  Image,
  Checkbox,
  Message,
  Header
} from "semantic-ui-react";

import { Form } from "formsy-semantic-ui-react";

import facebook from "../../../assets/img/facebook.png";
import google from "../../../assets/img/google.png";
import InputField from "../../Forms/FormFields/InputField";
import IcProfileView from "../../../assets/svg/IcProfileView";
import IcHide from "../../../assets/svg/IcHide";

import ReactGA from "react-ga";
import { USER } from "../../../constants/api";
import isLoggedIn from "../../../utils/env/isLoggedin";
import { HomePageString } from "../../EventStrings/Homepage";


import resetPassword from "../../../api/user/resetPassword";
import sendOtpEmail from "../../../api/user/sendOtpEmail";

import IcFooterArrowIcon from "../../../assets/svg/IcFooterArrowIcon";

import "./index.scss";

class ForgetPassword extends React.Component {
  state = {
    email: "",
    otp: "",
    password: "",
    isEmailDone: false,
    isPasswordDone: false,
    header: "",
    label: "",
    showPassword: false
  };

  constructor(props) {
    super(props);
  }

  async onEmailVerify(resend) {
    if (!this.state.email) {
      return;
    }

    if (resend) {
      this.setState({
        otp: "",
        password: ""
      });
    }

    try {
      const res = await sendOtpEmail(this.state.email);
      if (res.status === 200) {
        this.setState({
          isEmailDone: true,
          header: resend ? "OTP is resent" : "Open your mail",
          label: resend
            ? "OTP resent on registered email"
            : "OTP sent on registered email"
        });
      } else if (res.status === 404) {
        this.setState({
          isEmailDone: false,
          header: "Error, User not found",
          label: `User doesn't exist`
        });
      } else {
        console.error(res);
        this.setState({
          isEmailDone: true,
          header: "Error, Sending otp",
          label: "Error in sending otp to your email"
        });
      }
    } catch (error) {
      console.error(error);
      this.setState({
        isEmailDone: true,
        header: "Error, Sending otp",
        label: "Error in sending otp to your email"
      });
    }
  }

  async onPasswordSubmit() {
    try {
      const res = await resetPassword(
        this.state.email,
        this.state.otp,
        this.state.password
      );

      if (res.status === 200) {
        this.setState({
          isPasswordDone: true,
          header: "Password Changed",
          label: "Please Sign in with your"
        });
        // if (this.props.onDone) {
        //   this.props.onDone();
        // }
      } else if (res.status === 406) {
        this.setState({
          isPasswordDone: false,
          header: "Error, Invalid OTP",
          label: "Please enter correct OTP"
        });
        console.error(res);
      } else {
        this.setState({
          isPasswordDone: false,
          header: "Error, In OTP",
          label: "Please enter correct OTP"
        });
        console.error(res);
      }
    } catch (error) {
      this.setState({
        isPasswordDone: false,
        header: "Error, Sending otp",
        label: "Please enter correct OTP"
      });
      console.error(error);
    }
  }

  onEmailBackClick = e => {
    this.setState({
      isEmailDone: false
    });
  };

  onSubmit = e => {
    if (this.state.isEmailDone) {
      this.onPasswordSubmit();
    } else {
      this.onEmailVerify();
    }
  };

  onChange = (e, { value, name }) => {
    this.setState({
      [name]: value
    });
  };

  onPasswordTogggle = e => {
    this.setState({
      showPassword: !this.state.showPassword
    });
  };

  render() {
    const { onForgetToggle, canSubmit } = this.props;
    const UserId = window.localStorage.getItem(USER.UID);
    if (this.state.isEmailDone && this.state.isPasswordDone) {
      return (
        <div className="SearchSignIn successfull_change ForgetPassword">
          <div className="forgetPsssword_headerTitle">
            <Button className="backArrowBtn">
              <IcFooterArrowIcon onClick={this.props.onDone} pathcolor="" />
            </Button>
            <Header as="h3">Forget Password</Header>
          </div>
          <div className="forgot_container">
            <Message
              header={""}
              content={"Your password has successfully changed"}
            />

            <Button className="resent_otpBtn" onClick={this.props.onDone}>
              Go back to Sign in
              {/* <Icon name="long arrow right" /> */}
            </Button>
          </div>
        </div>
      );
    }

    return (
      <div className="SearchSignIn ForgetPassword">
        <div className="forgetPsssword_headerTitle">
          <Button className="backArrowBtn">
            <IcFooterArrowIcon
              onClick={
                this.state.isEmailDone
                  ? this.onEmailBackClick
                  : this.props.onDone
              }
              pathcolor=""
            />
          </Button>
          <Header as="h3">Forget Password</Header>
        </div>

        <div className="forgot_container">
          {this.state.header ? (
            <Message
              // header={this.state.header}
              content={this.state.label}
            />
          ) : null}

          <List>
            <List.Content>
              <Form onSubmit={this.onSubmit}>
                {!this.state.isEmailDone ? (
                  <InputField
                    type="email"
                    placeholder="Enter your registered email"
                    disabled={this.state.isEmailDone}
                    value={this.state.email}
                    onChange={this.onChange}
                    name="email"
                    validations="isEmail"
                    validationErrors={{
                      isEmail: "This is not a valid email",
                      isDefaultRequiredValue: "Email is Required"
                    }}
                    required
                  />
                ) : (
                  <React.Fragment>
                    {/* <span>You'll recieve an OTP on your email</span> */}
                    <div className="preview_Input">
                      {/* <span>Please enter the OTP you recieved on your e</span> */}
                      <InputField
                        type="number"
                        placeholder="Enter OTP"
                        value={this.state.otp}
                        onChange={this.onChange}
                        disabled={!this.state.isEmailDone}
                        name="otp"
                        required
                      />
                      <InputField
                        type={!this.state.showPassword ? "password" : "text"}
                        placeholder="Enter new password"
                        value={this.state.password}
                        disabled={!this.state.isEmailDone}
                        onChange={this.onChange}
                        name="password"
                      />
                      <div className="preview_Icon">
                        <Button type="button" onClick={this.onPasswordTogggle}>
                          {this.state.showPassword ? (
                            <IcProfileView pathcolor="#c8c8c8" />
                          ) : (
                            <span
                              style={{
                                display: "inline-block",
                                marginTop: "0px"
                              }}>
                              <IcHide pathcolor="#c8c8c8" />
                            </span>
                          )}
                        </Button>
                      </div>
                      {/* <div className="preview_Icon">
                  <Button type="button" onClick={onShowPasswordClick}>
                    {data.showPassword ? (
                      <IcProfileView pathcolor="#c8c8c8" />
                    ) : (
                      <span
                        style={{
                          display: "inline-block",
                          marginTop: "0px"
                        }}>
                        <IcHide pathcolor="#c8c8c8" />
                      </span>
                    )}
                  </Button>
                </div> */}
                    </div>

                    {/* <Form.Field className={props.className}>
                <Checkbox
                  className="checkbox-default"
                  label="Remember me next time"
                />
              </Form.Field> */}

                    <Button
                      className="resent_otpBtn"
                      // disabled={!canSubmit}
                      // disabled={!this.state.isEmailDone}
                      style={{
                        display: !this.state.isEmailDone ? "none" : "block",
                        background: "none"
                        // color: "#107fa9"
                      }}
                      onClick={() => this.onEmailVerify(true)}
                      type="button">
                      Resend OTP
                    </Button>
                  </React.Fragment>
                )}

                <Button
                  disabled={
                    this.state.isEmailDone
                      ? !this.state.otp || !this.state.password
                      : false
                  }
                  
                  onClick={() => {
                    ReactGA.event({
                      category: HomePageString.ForgotPassword.category,
                      action: HomePageString.ForgotPassword.action,
                      value: isLoggedIn() ? "" : UserId,
                    });
                  }}

                  type="submit"
                  className="bg-pacific-blue margin-top-24 text-white"
                  fluid>
                  Submit
                  <Icon name="long arrow right" />
                </Button>
              </Form>
            </List.Content>
          </List>
        </div>
      </div>
    );
  }
}

export default ForgetPassword;
