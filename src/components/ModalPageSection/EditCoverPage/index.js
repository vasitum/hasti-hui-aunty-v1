import React from "react";

import { Grid, Button, Form, Image } from "semantic-ui-react";
import IcCloseIcon from "../../../assets/svg/IcCloseIcon";
import "./index.scss";
import ModalBanner from "../../ModalBanner";

import getPlaceholder from "../../Forms/FormFields/Placeholder";
import FlatDefaultBtn from "../../Buttons/FlatDefaultBtn";
import FileuploadBtn from "../../Buttons/FileuploadBtn";
import ActionBtn from "../../Buttons/ActionBtn";
import IcUploadIcon from "../../../assets/svg/IcUpload";
import InfoIconLabel from "../../utils/InfoIconLabel";
import InputFieldLabel from "../../Forms/FormFields/InputFieldLabel";
import ModalFooterBtn from "../../Buttons/ModalFooterBtn";

import i1Image from "./i1.jpg";
import "./index.scss";

class EditCoverPage extends React.Component {
  render() {
    const {
      bannerFile,
      onBannerChange,
      onBannerRemove,
      onBannerSave
    } = this.props;

    return (
      <div className="card_panel">
        <ModalBanner headerText="Change cover image" />

        <div className="form_Contanier">
          <Form>
            <div className="Button_classes">
              <div
                className="panel_cardBody"
                style={{
                  backgroundImage: `linear-gradient(rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)), url(${
                    bannerFile ? bannerFile : i1Image
                  })`,
                  backgroundSize: "cover",
                  backgroundPosition: "center",
                  borderRadius: "0px"
                }}>
                <FileuploadBtn
                  className="fileuploadFlatBtn"
                  btnText="Upload cover image"
                  accept=".jpg, .jpeg, .png"
                  onFileUpload={onBannerChange}
                  btnIcon={<IcUploadIcon pathcolor="#c8c8c8" />}
                />
                <InputFieldLabel
                  className="Infoicon"
                  label="File specifications"
                  infoicon={<InfoIconLabel />}
                />
              </div>
            </div>

            <div className="ModalFooterBtn">
              <FlatDefaultBtn
                onClick={onBannerRemove}
                type="button"
                btntext="Delete"
              />
              <ActionBtn
                onClick={onBannerSave}
                type="button"
                actioaBtnText="Save"
              />
            </div>
          </Form>
        </div>
      </div>
    );
  }
}

export default EditCoverPage;
