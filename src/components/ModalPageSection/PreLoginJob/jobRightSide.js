import React from "react";
import {
  Card,
  List,
  Input,
  Header,
  Image,
  Button,
  Popup,
  Grid
} from "semantic-ui-react";
import wipro from "../../../assets/img/wipro.png";

import AlertSearch from "../../Cards/CardInputButton";
import CompanySvg from "../../../assets/svg/IcCompany";
import QuillText from "../../CardElements/QuillText";
import P2PTracker from "../../P2PTracker";
import { USER } from "../../../constants/api";

// const SearchAlert = props => (
//   <div className="alert-search-filter">
//     <AlertSearch label="Get alert for this search" />
//   </div>
// );

class JobRightSide extends React.Component {
  render() {
    const { jobId } = this.props;

    if (jobId) {
      return (
        <div className="publicJobRightSide">
          <P2PTracker
            type={"job"}
            cardType={"job"}
            userId={window.localStorage.getItem(USER.UID)}
            reqId={jobId}
          />

          {/* <AlertSearch /> */}

          {/* <List className="aboutCompany">
            <List.Item className="padding-bottom-20">
              <List.Content className="padding-bottom-25">
                <List.Header>
                  <Header as="h3" className="text-pacific-blue title">
                    About the Company
                  </Header>
                </List.Header>
              </List.Content>
              <Grid>
                <Grid.Row>
                  <Grid.Column width={4} className="padding-right-0">
                    <List.Content>
                      <span className="companyLogo">
                        {!compdata || !compdata.imgExt ? (
                          <CompanySvg
                            width="55"
                            height="55"
                            rectcolor="#f7f7fb"
                            pathcolor="#c8c8c8"
                          />
                        ) : (
                          <Image
                            src={`https://s3-us-west-2.amazonaws.com/img-mwf/company/${
                              compdata._id
                            }/image.jpg`}
                            style={{
                              height: "55px",
                              width: "55px"
                            }}
                          />
                        )}
                      </span>
                    </List.Content>
                  </Grid.Column>
                  <Grid.Column width={12} className="padding-left-4">
                    <List.Content>
                      <List className="padding-0">
                        <List.Item className="text-night-rider font-size-16 padding-bottom-10">
                          {compdata.name}
                        </List.Item>
                        <List.Item
                          style={{
                            display: !compdata.headquarter ? "none" : "block"
                          }}
                          className="text-Matterhorn padding-bottom-5">
                          {compdata.headquarter.location}
                        </List.Item>
                        <List.Item
                          style={{
                            display: !compdata.year ? "none" : "block"
                          }}
                          className="text-trolley-grey padding-bottom-5">
                          Founded:{" "}
                          <span className="text-Matterhorn">
                            {compdata.year}
                          </span>
                        </List.Item>
                        <List.Item
                          style={{
                            display: !compdata.size ? "none" : "block"
                          }}
                          className="text-trolley-grey padding-bottom-5">
                          Employees:{" "}
                          <span className="text-Matterhorn">
                            {compdata.size}
                            
                          </span>
                        </List.Item>
                        <List.Item
                          style={{
                            display: !compdata.website ? "none" : "block"
                          }}
                          className="text-trolley-grey padding-bottom-5">
                          Website:{" "}
                          <span className="text-Matterhorn">
                            {compdata.website}
                            
                          </span>
                        </List.Item>
                      </List>
                    </List.Content>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </List.Item>

            <List.Item>
              <List.Content className="padding-bottom-15 padding-top-15">
                <List.Description>
                  <p className="lineHeight-22 text-trolley-grey font-13px">
                    
                    <QuillText value={compdata.about} bgBase readOnly />
                  </p>
                </List.Description>
              </List.Content>
              <List.Content>
                <Button
                      size="tiny"
                      className="bg-munsell text-Matterhorn fontSize-12 b-r-30">
                      View more openings in this company
                    </Button>
              </List.Content>
            </List.Item>
          </List> */}
        </div>
      );
    } else {
      return <div />;
    }
  }
}

export default JobRightSide;
