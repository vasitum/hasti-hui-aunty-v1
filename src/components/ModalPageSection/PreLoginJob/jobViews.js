import React from "react";
import {
  List,
  Header,
  Button,
  Icon,
  Image,
  Grid,
  Card
} from "semantic-ui-react";
// import wipro from "../../../assets/img/wipro.png";
// import airtel from "../../../assets/img/airtel.png";
import SocialBtn from "../Buttons/socialBtn";
import CompanySvg from "../../../assets/svg/IcCompany";
import LocationLogo from "../../../assets/svg/IcLocation";
import ShareModal from "../../ShareButton/ShareModal";
import JobDescription from "../commonComponent/jobDescription";
import IcExternalIcon from "../../../assets/svg/IcExternalJob";
import ShareButton from "../../Buttons/ShareButton";
import LikeLogo from "../../../assets/svg/IcLike";
import ShareLogo from "../../../assets/svg/IcShare";
// import SkillsComponent from "../commonComponent/skills";

import "./index.scss";

import MobileSocialBtn from "../moblieComponents/mobileSocialBtn";

// import MoreOptionIcon from "../../../assets/svg/IcMoreOptionIcon";

import InfoSectionGridHeader from "../PreProfile/InfoSectionGridHeader";
import QuillText from "../../CardElements/QuillText";

import JobRightSide from "../PostLoginJob/jobRightSide";

import SkillComponent from "../PostLoginJob/SkillComponent";

// import IcEditIcon from "../../../assets/svg/IcEdit";

import DefaultModal from "../../Modal/DefaultModal";
import AddLabelSave from "../../CardElements/AddLabelSave";

import FlatDefaultBtn from "../../Buttons/FlatDefaultBtn";
// import ActionBtn from "../../Buttons/ActionBtn";
// import JobEditModal from "../../ModalPageSection/EditJob";

import { jobLike } from "../../../api/jobs/jobExtras";

import isLoggedIn from "../../../utils/env/isLoggedin";
import isProfileCreated from "../../../utils/env/isProfileCreated";

import UserAccessModal from "../../ModalPageSection/UserAccessModal";

import Currency from "../../CardElements/Currency";
import P2PTracker from "../../P2PTracker";
import { USER } from "../../../constants/api";
import { withRouter } from "react-router-dom";
import fromNow from "../../../utils/env/fromNow";

// const editIcon = <IcEditIcon pathcolor="#c8c8c8" />;

// const trigger = (
//   <span>
//     <MoreOptionIcon
//       width="5"
//       height="18.406"
//       pathcolor="#ffffff"
//       viewbox="0 0 5 18.406"
//     />
//   </span>
// );

// const options = [
//   { key: "Like", text: "Like" },
//   { key: "Share", text: "Share" },
//   { key: "Save", text: "Save" }
// ];

const processEducation = education => {
  if (!education) {
    return null;
  }

  let eduArry;

  if (education && !Array.isArray(education)) {
    eduArry = [
      {
        level: education.level
      }
    ];
  } else {
    eduArry = education.map(val => {
      return {
        // course: val.course,
        // spec: val.specs[0],
        level: val.level,
        type: "Full Time"
      };
    });
  }

  const eduArrayComponent = eduArry.map(val => {
    return <span className="padding-right-5">{val.level}</span>;
  });

  return eduArrayComponent;
};

const processDisablityOptions = other => {
  if (!other || !Array.isArray(other.disFriendlyTags)) {
    return null;
  }

  const tags = other.disFriendlyTags;

  return tags.map(val => {
    return <List.Item key={val}>{val}</List.Item>;
  });
};

function showEdu(isPreview, data) {
  if (isPreview) {
    if (!data) {
      return false;
    }

    if (!data.level) {
      return false;
    }

    return true;
  } else {
    if (!data || !data.length || !data[0].level) {
      return false;
    }

    return true;
  }
}

class JobViews extends React.Component {
  state = {
    isEditModalOpen: false,
    isSavedModalOpen: false,
    isLiked: false,
    isSaved: false,
    modalOpen: false
  };
  handleModal = () => this.setState({ modalOpen: !this.state.modalOpen });
  constructor(props) {
    super(props);
    this.onLikeJobClick = this.onLikeJobClick.bind(this);
  }

  onEditIconClick = ev => {
    this.setState({
      isEditModalOpen: !this.state.isEditModalOpen
    });
  };

  onSaveOpen = e => {
    if (!isProfileCreated()) {
      return;
    }

    this.setState({ isSavedModalOpen: true });
  };

  onSaveClose = e => {
    this.setState({ isSavedModalOpen: false });
  };

  onSave = e => {
    this.setState({
      isSaved: true,
      isSavedModalOpen: false
    });
  };

  async onLikeJobClick(ev, { id }) {
    ev.stopPropagation();

    try {
      const res = await jobLike(id);
      const data = await res.text();

      if (data) {
        // console.log(data);
        this.setState({ isLiked: !this.state.isLiked });
      }
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const isLoggedInVal = isLoggedIn();
    const {
      data,
      comdata,
      isPreview,
      isUser,
      liked,
      saved,
      applied,
      onLike,
      onSave,
      onUnSave,
      onApply,
      apiDone
    } = this.props;
    // console.log("new val", data);

    if (!data) {
      return (
        <div>
          <h1>Lana, Lana, LANA!!!</h1>
          <h1>What!!!</h1>
          <h1>Danger Zone!</h1>
        </div>
      );
    }

    return (
      <div className="leftSideJobDetails publicJobView_cardContainer mobile__publicJob mobileScreen_container">
        <List className="jobDetailsHeader">
          <List.Item>
            <Grid>
              <Grid.Row className="mobileAlingCenter">
                <Grid.Column mobile={16} computer={11}>
                  <List.Content className="headerLeftSide">
                    <List.Header>
                      <Header as="h1">{data.title}</Header>
                      <span className="vasitumFontOpen-500 text-white">
                        <LocationLogo
                          width="9.5"
                          height="11.5"
                          pathcolor="#0b9ed0"
                        />
                        <span className="padding-left-5">
                          {!data.remote
                            ? data.loc
                              ? data.loc.city
                              : ""
                            : "Remote"}
                        </span>
                      </span>
                    </List.Header>
                  </List.Content>
                </Grid.Column>
                <Grid.Column
                  mobile={16}
                  computer={5}
                  textAlign="right mobileAling_Center">
                  <List.Content className="headerRightSide">
                    {/* only Desktop */}
                    {isLoggedInVal ? (
                      <UserAccessModal
                        modalTrigger={
                          <Button
                            disabled={isPreview}
                            size="mini"
                            className="margin-right-10 mobile hidden bgWhiteSmallBtn">
                            {this.state.isSaved ? "Saved" : "Save"}
                          </Button>
                        }
                        isAction
                        // actionText="save"
                      />
                    ) : saved ? (
                      <Button
                        onClick={onUnSave}
                        disabled={isPreview}
                        size="mini"
                        className="margin-right-10 mobile hidden bgWhiteSmallBtn">
                        Saved
                      </Button>
                    ) : (
                      <DefaultModal
                        size="tiny"
                        open={this.state.isSavedModalOpen}
                        trigger={
                          <Button
                            disabled={isPreview || isUser}
                            size="mini"
                            onClick={this.onSaveOpen}
                            className="margin-right-10 mobile hidden bgWhiteSmallBtn">
                            {this.state.isSaved ? "Saved" : "Save"}
                          </Button>
                        }>
                        <AddLabelSave
                          saveObjectId={data._id}
                          onCloseBtnClick={this.onSaveClose}
                          onSaveClick={() => {
                            onSave();
                            this.onSaveClose();
                          }}
                          type={"job"}
                        />
                      </DefaultModal>
                    )}
                    {/* only Desktop end*/}
                    {isLoggedInVal ? (
                      <UserAccessModal
                        modalTrigger={
                          <Button
                            disabled={isPreview}
                            size="mini"
                            className="actionBtn moblile__actionBtn external-job-button"
                            rel="nofollow">
                            Apply{" "}
                            {data.externalLink ? (
                              <IcExternalIcon
                                pathcolor="#99d9f9"
                                height="12"
                                width="12"
                              />
                            ) : (
                              <Icon name="angle right" />
                            )}
                          </Button>
                        }
                        isAction
                        // actionText="apply"
                      />
                    ) : applied ? (
                      <Button
                        disabled={isPreview || isUser}
                        size="mini"
                        className="actionBtn moblile__actionBtn external-job-button"
                        rel="nofollow">
                        Applied{" "}
                        {data.externalLink ? (
                          <IcExternalIcon
                            pathcolor="#99d9f9"
                            height="12"
                            width="12"
                          />
                        ) : (
                          <Icon name="angle right" />
                        )}
                      </Button>
                    ) : data.externalLink ? (
                      <Button
                        as={"a"}
                        href={data.externalLink}
                        target={"_blank"}
                        disabled={isPreview || isUser}
                        size="mini"
                        className="actionBtn moblile__actionBtn external-job-button"
                        rel="nofollow">
                        Apply{" "}
                        {data.externalLink ? (
                          <IcExternalIcon
                            pathcolor="#99d9f9"
                            height="12"
                            width="12"
                          />
                        ) : (
                          <Icon name="angle right" />
                        )}
                      </Button>
                    ) : (
                      <Button
                        onClick={onApply}
                        disabled={isPreview || isUser}
                        size="mini"
                        className="actionBtn moblile__actionBtn external-job-button"
                        rel="nofollow">
                        Apply{" "}
                        {data.externalLink ? (
                          <IcExternalIcon
                            pathcolor="#99d9f9"
                            height="12"
                            width="12"
                          />
                        ) : (
                          <Icon name="angle right" />
                        )}
                      </Button>
                    )}

                    {/* only mobile */}
                    {/* <span className="mobile__moreOption widescreen hidden large screen hidden computer hidden tablet hidden">
                      <Dropdown
                        disabled={isUser}
                        trigger={trigger}
                        options={options}
                        pointing="top right"
                        icon={null}
                      />
                    </span> */}
                    {/* only mobile */}
                  </List.Content>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </List.Item>
        </List>

        <Card className="primaryCard" fluid>
          <div className="jobProfileCard">
            <div className="jobCard_headerDetail">
              <List verticalAlign="middle">
                <List.Item>
                  <Grid>
                    <Grid.Row className="mobileAlingCenter">
                      <Grid.Column mobile={16} computer={2}>
                        <List.Content>
                          {data.com ? (
                            <span
                              className="companyLogo"
                              style={{ display: "inline" }}>
                              {!data.com || !data.com.imgExt ? (
                                <CompanySvg
                                  width="64"
                                  height="64"
                                  rectcolor="#f7f7fb"
                                  pathcolor="#c8c8c8"
                                />
                              ) : (
                                <Image
                                  src={`https://s3-us-west-2.amazonaws.com/img-mwf/company/${
                                    data.com.id
                                  }/image.jpg`}
                                  style={{
                                    height: 64,
                                    width: 64
                                  }}
                                  alt={data.com.name}
                                />
                              )}
                            </span>
                          ) : (
                            <span
                              className="companyLogo"
                              style={{ display: "inline" }}>
                              {!comdata || !comdata.imgExt ? (
                                <CompanySvg
                                  width="64"
                                  height="64"
                                  rectcolor="#f7f7fb"
                                  pathcolor="#c8c8c8"
                                />
                              ) : (
                                <Image
                                  src={`https://s3-us-west-2.amazonaws.com/img-mwf/company/${
                                    comdata._id
                                  }/image.jpg`}
                                  style={{
                                    height: 64,
                                    width: 64
                                  }}
                                />
                              )}
                            </span>
                          )}
                        </List.Content>
                      </Grid.Column>
                      <Grid.Column
                        mobile={16}
                        computer={9}
                        className="padding-left-2">
                        <List.Content className="profileCardHeader">
                          <List.Header>
                            <Header
                              className="headerTitle padding-bottom-5"
                              as="h3">
                              {data.com ? data.com.name : ""}
                            </Header>
                            <p className="headerSubTitle padding-bottom-5">
                              Job Role:{" "}
                              <span className="headerSubLavel ">
                                {data.role}
                              </span>
                            </p>
                          </List.Header>
                          <List.Description className="experience">
                            <p
                              className="margin-bottom-0 headerSubTitle"
                              style={{ marginTop: "-4px" }}>
                              <span
                                className="publicJob__HeaderExp"
                                style={{
                                  display:
                                    !data.exp || !data.exp.min || !data.exp.max
                                      ? "none"
                                      : "inline-block"
                                }}>
                                Experience: {/* only Desktop */}
                                <span className="headerSubLavel mobile hidden">
                                  {" "}
                                  {data.exp ? data.exp.min : ""} -{" "}
                                  {data.exp ? data.exp.max : ""} years
                                </span>
                                <span className="headerSubLavel widescreen hidden large screen hidden computer hidden tablet hidden">
                                  {" "}
                                  {data.exp ? data.exp.min : ""}-
                                  {data.exp ? data.exp.max : ""}
                                  year
                                </span>
                                {/* only mobile end*/}
                              </span>
                              {data.jobType ? (
                                <span className="publicJob__HeaderExp">
                                  Job type:{" "}
                                  <span className="headerSubLavel">
                                    {" "}
                                    {data.jobType}{" "}
                                  </span>
                                </span>
                              ) : (
                                ""
                              )}

                              <span
                                className="publicJob__HeaderExp"
                                style={{
                                  display:
                                    !data.salary ||
                                    (!data.salary.min && !data.salary.max)
                                      ? "none"
                                      : "block"
                                }}>
                                Salary:{" "}
                                <span className="headerSubLavel">
                                  {" "}
                                  <Currency data={data} />
                                  {data.salary ? data.salary.min : ""} -{" "}
                                  {data.salary ? data.salary.max : ""}{" "}
                                  {data.salary
                                    ? data.salary.currency &&
                                      data.salary.currency !== "INR"
                                      ? "k per annum"
                                      : " LPA"
                                    : ""}{" "}
                                </span>
                              </span>
                            </p>
                          </List.Description>
                        </List.Content>
                      </Grid.Column>
                      <Grid.Column mobile={16} computer={5}>
                        <List.Content className="mobileAlingCenter alingRight">
                          <List.Header className="postTime vasitumFontOpen-500  padding-right-10">
                            {/* <span
                              style={{
                                fontSize: "12px"
                              }}
                              className="text-Matterhorn">
                              Posted:{" "}
                              <span className="">Today</span>{" "}
                            </span> */}
                            <p>
                              Posted:{" "}
                              <span className="" style={{ display: "inline" }}>
                                {fromNow(data.cTime)}
                              </span>{" "}
                            </p>
                          </List.Header>

                          <List.Description className="padding-top-10 mobile hidden">
                            {isLoggedInVal ? (
                              <UserAccessModal
                                modalTrigger={
                                  <Button
                                    disabled={isPreview}
                                    size="mini"
                                    className="bg-transparent socialBtn-desktop margin-right-10">
                                    {/* {this.state.isLiked ? "Liked" : "Like"} */}
                                    <LikeLogo
                                      width="15"
                                      height="15"
                                      pathcolor={
                                        this.state.isLiked
                                          ? "#0b9ed0"
                                          : "#c8c8c8"
                                      }
                                    />
                                  </Button>
                                }
                                isAction
                                // actionText="like"
                              />
                            ) : (
                              <Button
                                onClick={onLike}
                                disabled={isPreview || isUser}
                                size="mini"
                                className="margin-right-10 bg-transparent socialBtn-desktop">
                                {/* {liked ? "Liked" : "Like"} */}
                                <LikeLogo
                                  width="15"
                                  height="15"
                                  pathcolor={liked ? "#0b9ed0" : "#c8c8c8"}
                                />
                              </Button>
                            )}
                            {isLoggedInVal ? (
                              <UserAccessModal
                                modalTrigger={
                                  <Button
                                    disabled={isPreview}
                                    size="mini"
                                    className="bg-transparent socialBtn-desktop">
                                    <ShareLogo
                                      width="15"
                                      height="15"
                                      pathcolor="#c8c8c8"
                                    />
                                  </Button>
                                }
                                isAction
                                // actionText="share"
                              />
                            ) : (
                              <Button
                                disabled={isPreview}
                                onClick={this.handleModal}
                                size="mini"
                                className="bg-transparent socialBtn-desktop">
                                <ShareLogo
                                  width="15"
                                  height="15"
                                  pathcolor="#c8c8c8"
                                />
                              </Button>
                            )}
                          </List.Description>
                        </List.Content>
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                </List.Item>
              </List>
              <div className="socialLike_container">
                {apiDone ? (
                  <SocialBtn
                    disabled={isUser}
                    objectID={data._id}
                    onLikeClick={onLike}
                    liked={liked}
                    saved={saved}
                    type="job"
                  />
                ) : null}
              </div>
              <div className="divider-hr" />

              <List verticalAlign="middle" className="profileCardBody">
                <Grid>
                  <Grid.Row>
                    <Grid.Column width={2} className="mobile hidden" />
                    <Grid.Column
                      width={14}
                      className="padding-left-2 publicjob__body">
                      <JobDescription jsonData={data.desc} isPublicView />
                      <div
                        className="jobDuties"
                        style={{
                          display:
                            !data.respAndDuty || data.respAndDuty.length === 11
                              ? "none"
                              : "block"
                        }}>
                        <InfoSectionGridHeader
                          onEditIconClick={this.onEditIconClick}
                          headerText="Job Duties"
                          headerClasses="jobDuties_Title"
                          editIcon={""}>
                          <QuillText value={data.respAndDuty} readOnly />
                        </InfoSectionGridHeader>
                      </div>

                      <div
                        className="Benefites"
                        style={{
                          display:
                            !data.benefits || data.benefits.length === 11
                              ? "none"
                              : "block"
                        }}>
                        <InfoSectionGridHeader
                          onEditIconClick={this.onEditIconClick}
                          headerText="Benefits"
                          headerClasses="benefites_Title"
                          editIcon={""}>
                          <QuillText value={data.benefits} readOnly />
                        </InfoSectionGridHeader>
                      </div>

                      {/* <div className="Benefites">
                    <JobDescription />
                  </div> */}

                      <SkillComponent data={data} />

                      {/* <List.Item
                      style={{
                        display: !data.skills ? "none" : "block"
                      }}
                      className="padding-bottom-20">
                      <List.Content>
                        <InfoSectionGridHeader headerText="Skills Requirement">
                          <List.Description className="filterSkill">
                            <List
                              bulleted
                              horizontal
                              className="text-night-rider">
                              {processSkills(data.skills)}
                            </List>
                          </List.Description>
                        </InfoSectionGridHeader>
                      </List.Content>
                    </List.Item> */}
                      <List.Item
                        className="padding-bottom-20 skills-requirement"
                        style={{
                          display: showEdu(isPreview, data.edus)
                            ? "block"
                            : "none"
                        }}>
                        <List.Content>
                          <InfoSectionGridHeader headerText=" Educational qualification">
                            {processEducation(data.edus)}
                          </InfoSectionGridHeader>
                        </List.Content>
                      </List.Item>

                      <div
                        className="otherRequirement"
                        style={{
                          display:
                            !data.other || !data.other.dec ? "none" : "block"
                        }}>
                        <InfoSectionGridHeader
                          headerText="Other Requirements"
                          headerClasses="benefites_Title">
                          <QuillText
                            value={data.other ? data.other.dec : ""}
                            readOnly
                          />
                        </InfoSectionGridHeader>
                      </div>
                      <div
                        className="padding-bottom-20 education_requirement filterSkill"
                        style={{
                          display:
                            !data.other ||
                            !data.other.disFriendlyTags ||
                            !data.other.disFriendlyTags.length
                              ? "none"
                              : "block"
                        }}>
                        <InfoSectionGridHeader
                          headerText="Disabled Friendly Job"
                          containerClass="educationRequirement_profile"
                          editIcon={this.editIcon}
                          onEditIconClick={this.onEditIconClick}>
                          <List bulleted horizontal>
                            {processDisablityOptions(data.other)}
                          </List>
                        </InfoSectionGridHeader>
                      </div>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </List>
            </div>
            {/* <div className="divider-hr" /> */}
          </div>
          <div className="aboutCompany_body">
            <Grid>
              <Grid.Row>
                <Grid.Column width={2} className="aboutCompanyLeft" />
                <Grid.Column width={14} className="aboutCompanyRight">
                  <JobRightSide compdata={comdata} isPreview={isPreview} />
                </Grid.Column>
              </Grid.Row>
            </Grid>

            {isPreview ? (
              <div className="alingRight mobileAlingCenter">
                <FlatDefaultBtn
                  onClick={() => this.props.history.go(-1)}
                  className="bgTranceparent"
                  btntext="Go Back"
                />
                {/* <ActionBtn onClick={onSaveClick} actioaBtnText="Publish Job" /> */}
              </div>
            ) : (
              <div className="alingRight">
                {/* only Desktop */}
                <span className="mobile hidden">
                  {/* <SocialBtn
                    isLiked={this.state.isLiked}
                    isSaved={this.state.isSaved}
                    objectID={data._id}
                    onLikeClick={this.onLikeJobClick}
                  /> */}

                  {isLoggedInVal ? (
                    <UserAccessModal
                      modalTrigger={
                        <Button
                          size="small"
                          className="actionBtn external-job-button"
                          rel="nofollow">
                          Apply
                          {data.externalLink ? (
                            <IcExternalIcon
                              pathcolor="#99d9f9"
                              height="12"
                              width="12"
                            />
                          ) : (
                            <Icon name="angle right" />
                          )}
                        </Button>
                      }
                      isAction
                      // actionText="apply"
                    />
                  ) : applied ? (
                    <Button
                      disabled={isUser}
                      size="small"
                      className="actionBtn external-job-button"
                      rel="nofollow">
                      Applied
                      {data.externalLink ? (
                        <IcExternalIcon
                          pathcolor="#99d9f9"
                          height="12"
                          width="12"
                        />
                      ) : (
                        <Icon name="angle right" />
                      )}
                    </Button>
                  ) : data.externalLink ? (
                    <Button
                      as={"a"}
                      href={data.externalLink}
                      target={"_blank"}
                      disabled={isPreview || isUser}
                      size="mini"
                      className="actionBtn moblile__actionBtn external-job-button"
                      rel="nofollow">
                      Apply
                      {data.externalLink ? (
                        <IcExternalIcon
                          pathcolor="#99d9f9"
                          height="12"
                          width="12"
                        />
                      ) : (
                        <Icon name="angle right" />
                      )}
                    </Button>
                  ) : (
                    <Button
                      onClick={onApply}
                      disabled={isPreview || isUser}
                      size="mini"
                      className="actionBtn moblile__actionBtn external-job-button"
                      rel="nofollow">
                      Apply
                      {data.externalLink ? (
                        <IcExternalIcon
                          pathcolor="#99d9f9"
                          height="12"
                          width="12"
                        />
                      ) : (
                        <Icon name="angle right" />
                      )}
                    </Button>
                  )}
                </span>
                {/* only Desktop end*/}

                {/* only mobile*/}
                <div className="MobileSocialBtn">
                  <MobileSocialBtn
                    as={"a"}
                    className="external-job-button"
                    href={data.externalLink}
                    target={"_blank"}
                    disabled={isUser || applied}
                    btntext={applied ? "Applied" : "Apply"}
                    icon={
                      data.externalLink ? (
                        <IcExternalIcon
                          pathcolor="#99d9f9"
                          height="12"
                          width="12"
                        />
                      ) : (
                        <Icon name="angle right" />
                      )
                    }
                  />
                </div>
                {/* only mobile*/}
              </div>
            )}
          </div>
        </Card>

        <div>
          {data._id ? (
            <P2PTracker
              type={"job"}
              reqId={data._id}
              userId={window.localStorage.getItem(USER.UID)}
              cardType={"FooterP2P"}
            />
          ) : null}
        </div>

        <ShareModal
          modalOpen={this.state.modalOpen}
          handleModal={this.handleModal}
          url={`/view/job/${data._id}`}
        />
      </div>
    );
  }
}

export default withRouter(JobViews);
