import React from "react";
import { Grid, Container, Image } from "semantic-ui-react";
import JobViews from "./jobViews";
import JobRightSide from "./jobRightSide";
import isLoggedIn from "../../../utils/env/isLoggedin";
import public_job_banner from "../../../assets/banners/job_cover_img.jpg";
import BestBanner from "../../../assets/img/searchFilter/bestBanner.png";
import getJobByIdCount from "../../../api/jobs/getJobByIdCount";
// import getJobById from "../../../api/jobs/getJobById";
import getCompany from "../../../api/company/getCompById";
import ReactGA from "react-ga";
import PageBanner from "../../Banners/PageBanner";
import {
  jobSave,
  jobLike,
  jobApply,
  getLikedAndSavedJobs
} from "../../../api/jobs/jobExtras";

import isProfileCreated from "../../../utils/env/isProfileCreated";

import PreviewHeaderNotification from "../PreviewHeaderNotification";
import { USER } from "../../../constants/api";
import { withChatbotContext } from "../../../contexts/ChatbotContext";
import { withRouter } from "react-router-dom";
import { toast } from "react-toastify";
const Loader = props => (
  <div className="m_showbox">
    <div className="m_loader">
      <svg className="m_circular" viewBox="25 25 50 50">
        <circle
          className="m_path"
          cx="50"
          cy="50"
          r="20"
          fill="none"
          stroke-width="2"
          stroke-miterlimit="10"
        />
      </svg>
    </div>
  </div>
);

class PublicJob extends React.Component {
  state = {
    isLoading: false,
    jobdata: {},
    compData: null,
    liked: false,
    saved: false,
    applied: false,
    apiDone: false
  };

  constructor(props) {
    super(props);

    // bind
    this.onLike = this.onLike.bind(this);
    this.onUnSave = this.onUnSave.bind(this);
    this.onApply = this.onApply.bind(this);
  }

  async componentDidMount() {
    const { match } = this.props;
    // console.log("check url",  match.url)

    ReactGA.pageview(`${match.url}`);

    try {
      // const res = await getJobById(match.params.id);
      const res = await getJobByIdCount(match.params.id);
      const jobDataJson = await res.json();

      this.setState({
        isLoading: false,
        jobdata: jobDataJson
      });

      const resComp = await getCompany(jobDataJson.com.id);
      const resCompJson = await resComp.json();

      this.setState({
        compData: resCompJson
      });

      const datares = await getLikedAndSavedJobs([match.params.id], "job");
      if (datares.status === 200) {
        const data = await datares.json();
        const dataVal = data[match.params.id];
        this.setState({
          liked: dataVal.like,
          saved: dataVal.saveJobs,
          applied: dataVal.applied,
          apiDone: true
        });
      } else {
        // console.log(datares);
        this.setState({ apiDone: true });
      }
    } catch (error) {}
  }

  async onLike(e) {
    const { match } = this.props;
    if (!isProfileCreated()) {
      return;
    }

    try {
      const res = await jobLike(match.params.id, "job");
      if (res.status === 200) {
        this.setState({ liked: !this.state.liked });
        const data = await res.text();
        // console.log(data);
      } else {
        console.log(res);
      }
    } catch (error) {
      console.error(error);
    }
  }

  async onUnSave(e) {
    const { match } = this.props;
    if (!isProfileCreated()) {
      return;
    }

    try {
      const res = await jobSave(match.params.id, [], "job");
      if (res.status === 200) {
        this.setState({ saved: !this.state.saved });
        const data = await res.text();
        // console.log(data);
      } else {
        console.log(res);
      }
    } catch (error) {
      console.log(error);
    }
  }

  async onApply() {
    const { jobdata } = this.state;
    const { cb, actions } = this.props;
    // because we are scared!
    if (!Object.prototype.hasOwnProperty.call(jobdata, "title")) {
      return;
    }

    try {
      const res = await jobApply(jobdata._id, jobdata.title);
      if (res.status === 200) {
        const data = await res.json();
        this.setState(
          {
            applied: true
          },
          () => {
            actions.onApply({
              jobId: jobdata._id,
              applicationId: data._id,
              userId: window.localStorage.getItem(USER.UID)
            });
          }
        );
      } else {
        console.log("Unable to apply to job", jobdata._id);
      }
    } catch (error) {
      console.error(error);
    }
  }

  onSave = e => {
    this.setState({ saved: !this.state.saved });
  };

  componentDidUpdate() {
    const { jobdata } = this.state;
    const { match } = this.props;

    if (!Object.prototype.hasOwnProperty.call(jobdata, "_id")) {
      return;
    }

    if (!match || !match.params || !match.params.id) {
      return;
    }

    const propsId = match.params.id;
    const jobDataId = jobdata._id;
    if (propsId === jobDataId) {
      return;
    }

    this.componentDidMount();
  }

  render() {
    if (this.state.isLoading) {
      return (
        <Container>
          <Loader />
        </Container>
      );
    } else {
      const isUser =
        this.state.jobdata.userId === window.localStorage.getItem(USER.UID);
        
        // console.log("user check id",  this.state.jobdata.userId)
        const isLoggedInUser = isLoggedIn()
      return (
        <div className="publicJobViews">
          {!isLoggedInUser ? (isUser  && <PreviewHeaderNotification publicTitle="job" />):null}
          {/* // {isUser  && <PreviewHeaderNotification publicTitle="job" /> } */}
          <div className="publicJobBanner">
            <PageBanner
              size="medium"
              image={
                this.state.jobdata
                  ? this.state.jobdata.imgBanner
                    ? `https://s3-us-west-2.amazonaws.com/img-mwf/banner/${
                        this.state.jobdata._id
                      }/image.png`
                    : public_job_banner
                  : public_job_banner
              }>
              {/* <Container>
                <BannerHeader onFileChange={this.onFileChange} />
              </Container> */}
            </PageBanner>
          </div>
          <div className="publicJobDetails">
            <Container>
              <Grid>
                <Grid.Row>
                  <Grid.Column mobile={16} tablet={16} computer={11}>
                    <JobViews
                      isUser={isUser}
                      comdata={this.state.compData}
                      data={this.state.jobdata}
                      liked={this.state.liked}
                      saved={this.state.saved}
                      applied={this.state.applied}
                      apiDone={this.state.apiDone}
                      onLike={this.onLike}
                      onSave={this.onSave}
                      onUnSave={this.onUnSave}
                      onApply={this.onApply}
                    />
                  </Grid.Column>
                  <Grid.Column width={5} className="tablet mobile hidden">
                    <JobRightSide
                      jobId={this.state.jobdata ? this.state.jobdata._id : null}
                    />
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Container>
          </div>
        </div>
      );
    }
  }
}

export default withChatbotContext(withRouter(PublicJob));
