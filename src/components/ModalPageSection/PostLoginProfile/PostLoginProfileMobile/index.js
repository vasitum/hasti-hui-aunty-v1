import React, { Component } from 'react'

import PostLoginProfileContainer from "../PostLoginProfileContainer";

import "./index.scss";

class PostLoginProfileMobile extends Component {
  render() {
    return (
      <div className="PostLoginProfileMobile">
        <div className="PostLoginProfileMobile_body">
          <PostLoginProfileContainer />
        </div>
      </div>
    )
  }
}

export default PostLoginProfileMobile;
