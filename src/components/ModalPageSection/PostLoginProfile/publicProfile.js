import React from "react";
import {
  List,
  Button,
  Image,
  Grid,
  Header,
  Feed,
  Modal,
  Responsive
} from "semantic-ui-react";
import publicProfileBanner from "../../../assets/img/publicProfileBanner.png";
import UserView from "../PostLoginProfile/ProfileProgress/UserView";
// import { list } from "postcss";
// import SocialBtn from "../Buttons/socialBtn";

import IcEditIcon from "../../../assets/svg/IcEdit";

import EducationLogo from "../../../assets/svg/IcEducation";
import CompanySvg from "../../../assets/svg/IcCompany";
import UserLogo from "../../../assets/svg/IcUser";
// import LikeLogo from "../../../assets/svg/IcLike";
// import ShareLogo from "../../../assets/svg/IcShare";

import EducationNoCircleIcon from "../../../assets/svg/IcEducationNoCircle";
import LocationLogo from "../../../assets/svg/IcLocation";
// import ProfileView from "../../../assets/svg/IcProfileView";
// import MobileSocialBtn from "../moblieComponents/mobileSocialBtn";

import InfoSectionGridHeader from "./InfoSectionGridHeader";

import EditProfilePage from "../../EditProfilePage";

import ProfileUploadeResume from "./profileUploadeResume";

import DesiredJob from "./DesiredJob";

import DesiredJobSubHeader from "./DesiredJobSubHeader";

import QuillText from "../../CardElements/QuillText";
import EditCoverPage from "../EditCoverPage";

// import ShareButton from "../../Buttons/ShareButton";

import IcCertification from "../../../assets/svg/IcCertification";

import LikeViewComponent from "../../Dashboard/Common/LikeViewComponent";

import MobileFooter from "../../MobileComponents/MobileFooter";
import P2PTracker from "../../P2PTracker";
import { USER } from "../../../constants/api";

import { Link } from "react-router-dom";

import ConvertProfileResume from "./ConvertProfileResume";

// import UserView from "./UserView";

const editIcon = <IcEditIcon pathcolor="#c8c8c8" />;

const months = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "June",
  "July",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec"
];

const processSkillsOptional = skills => {
  if (!skills || skills.length === 0) {
    return (
      <List.Item>
        <p
          style={{
            fontFamily: "Open Sans",
            fontSize: "14px",
            color: "#c8c8c8",
            textAlign: "left",
            lineHeight: "22px",
            fontStyle: "normal"
          }}>
          Highlight your skills and show employers what you're really good at!
          <br />
          <br />
          Adding your top skills will let you engage in discussion to showcase
          your expertise,
          <br />
          For example, Your specialties could be photoshop, web development,
          customer service, or interior design.
        </p>
      </List.Item>
    );
  }

  const skillsName = skills.map(val => {
    return {
      name: val.name,
      exp: val.exp
    };
  });

  const skillComponent = skillsName.map(val => {
    if (!val) {
      return;
    }

    return (
      <div
        className="filterSkill skillBtn"
        style={{ display: "inline-block" }}
        key={val.name}>
        <List horizontal className="text-night-rider">
          <List.Item>
            <Button compact>
              {val.exp ? (
                <React.Fragment>
                  {val.name} |<span> {val.exp} yrs</span>
                </React.Fragment>
              ) : (
                <React.Fragment>{val.name}</React.Fragment>
              )}
            </Button>
          </List.Item>
        </List>
      </div>
    );
  });

  return skillComponent;
};

const processExperience = exp => {
  if (!exp || exp.length === 0) {
    return (
      <List.Item>
        <p
          style={{
            fontFamily: "Open Sans",
            fontSize: "14px",
            color: "#c8c8c8",
            textAlign: "left",
            lineHeight: "22px",
            fontStyle: "normal"
          }}>
          Your experience is one of the most important sections in a CV.
          <br />
          List all relevant responsibilities, skills, projects, and achievements
          against each role.
        </p>
      </List.Item>
    );
    return null;
  }

  if (exp.length === 1 && !exp[0].designation && !exp[0].name) {
    return (
      <List.Item>
        <p
          style={{
            fontFamily: "Open Sans",
            fontSize: "14px",
            color: "#c8c8c8",
            textAlign: "left",
            lineHeight: "22px",
            fontStyle: "normal"
          }}>
          Let employers know more about your experience; remember, be clear and
          concise.
        </p>
      </List.Item>
    );
  }

  const expArray = exp.map(val => {
    // console.log("EXPERIENCE ISSS", val);

    return {
      desc: val.desc,
      loc: val.loc,
      designation: val.designation,
      name: val.name,
      doj: {
        month: val.doj.month,
        year: val.doj.year
      },
      dor: {
        month: val.dor.month,
        year: val.dor.year
      },
      current: val.current
    };
  });

  const expComponent = expArray.map(exp => {
    return (
      <List.Item
        className="padding-top-10"
        key={exp.name}
        style={{
          display: !exp.name && !exp.designation ? "none" : "block"
        }}>
        <List.Content>
          <Feed>
            <Feed.Event>
              {/* <Feed.Label image={java} /> */}
              <span className="educationThumb margin-right-10">
                <CompanySvg
                  width="40"
                  height="40"
                  rectcolor="#f7f7fb"
                  pathcolor="#c8c8c8"
                />
              </span>
              <Feed.Content className="expDescription">
                <List>
                  <List.Item>
                    <p className="expDescriptionHeader vasitumFontOpen-500">
                      {exp.designation}
                      {/* exp.designation */}
                    </p>
                    <p className="expDescriptionSub">{exp.name}</p>
                  </List.Item>
                  <List.Item>
                    <p className="font-size-13 text-trolley-grey font-f-open-sans">
                      {(function(exp) {
                        let str = [];
                        if (exp.current) {
                          if (exp.doj.month) {
                            str.push(exp.doj.month);
                            str.push(" ");
                          }

                          if (exp.doj.year) {
                            str.push(exp.doj.year);
                            str.push(" ");
                            str.push("-");
                            str.push(" ");
                            str.push("Present");
                          }

                          return str.join("");
                        }

                        if (exp.doj.month) {
                          str.push(exp.doj.month);
                          str.push(" ");
                        }

                        if (exp.doj.year) {
                          str.push(exp.doj.year);
                          str.push(" ");
                        }

                        if (exp.dor.month) {
                          str.push("-");
                          str.push(" ");
                          str.push(exp.dor.month);
                        }

                        if (exp.dor.year) {
                          if (!exp.dor.month) {
                            str.push("-");
                          }
                          str.push(" ");
                          str.push(exp.dor.year);
                        }

                        return str.join("");
                      })(exp)}
                    </p>
                  </List.Item>
                  <List.Item>
                    <p className="text-a1">{exp.loc}</p>
                  </List.Item>
                </List>
                <Feed.Summary className="expSummary">
                  {/* {exp.desc} */}
                  <QuillText value={exp.desc} isMute readOnly />
                </Feed.Summary>
              </Feed.Content>
            </Feed.Event>
          </Feed>
        </List.Content>
      </List.Item>
    );
  });

  return expComponent;
};

const processEducation = education => {
  if (!education || education.length === 0) {
    return (
      <List.Item>
        <p
          style={{
            fontFamily: "Open Sans",
            fontSize: "14px",
            color: "#c8c8c8",
            textAlign: "left",
            lineHeight: "22px",
            fontStyle: "normal"
          }}>
          Let employers know more about your education; remember, be clear and
          concise.
        </p>
      </List.Item>
    );
    return null;
  }

  if (
    education.length === 1 &&
    !education[0].course &&
    !education[0].institute
  ) {
    return (
      <List.Item>
        <p
          style={{
            fontFamily: "Open Sans",
            fontSize: "14px",
            color: "#c8c8c8",
            textAlign: "left",
            lineHeight: "22px",
            fontStyle: "normal"
          }}>
          Let employers know more about your education; remember, be clear and
          concise.
        </p>
      </List.Item>
    );
  }

  const eduArry = education.map((val, index) => {
    return {
      course: val.course,
      spec: val.specs[0],
      level: val.level,
      started: val.started,
      completed: val.completed,
      institute: val.institute,
      type: "Full Time",
      key: index
    };
  });

  const eduArrayComponent = eduArry.map(val => {
    // console.log("Education Value is ", val);
    return (
      <div
        className=""
        key={val.key}
        style={{
          display: !val.course && !val.institute ? "none" : "block"
        }}>
        <Grid>
          <Grid.Row>
            <Grid.Column mobile={3} computer={1}>
              <List.Content className="">
                <span className="educationThumb">
                  <EducationLogo
                    width="40"
                    height="40"
                    rectcolor="#f7f7fb"
                    pathcolor="#c8c8c8"
                  />
                </span>
              </List.Content>
            </Grid.Column>
            <Grid.Column mobile={13} computer={15}>
              <List.Content className="expDescription padding-left-5">
                <List.Header>
                  <p className="expDescriptionHeader">{val.institute}</p>
                </List.Header>
                <List.Description>
                  <p className="margin-bottom-0 padding-bottom-5 expDescriptionSub">
                    {val.course}{" "}
                    {val.level && (
                      <span
                        style={{
                          color: "#a1a1a1"
                        }}>
                        | {val.level}
                      </span>
                    )}
                  </p>
                  {val.spec ? (
                    <p className="margin-bottom-0 padding-bottom-5 expDescriptionSub">
                      {val.spec}{" "}
                    </p>
                  ) : null}
                  <p
                    style={{
                      display:
                        Number(val.started) && Number(val.completed)
                          ? "block"
                          : "none"
                    }}
                    className="text-trolley-grey font-size-13">
                    {val.started + " - " + val.completed}
                  </p>
                </List.Description>
              </List.Content>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    );
  });

  return eduArrayComponent;
};

const processCertificate = certificate => {
  if (!certificate || certificate.length === 0) {
    return (
      <List.Item>
        <p
          style={{
            fontFamily: "Open Sans",
            fontSize: "14px",
            color: "#c8c8c8",
            textAlign: "left",
            lineHeight: "22px",
            fontStyle: "normal"
          }}>
          Certifications will highlight your profile."
        </p>
      </List.Item>
    );
    return null;
  }

  if (certificate.length === 1 && !certificate[0].certificate_name) {
    return (
      <List.Item>
        <p
          style={{
            fontFamily: "Open Sans",
            fontSize: "14px",
            color: "#c8c8c8",
            textAlign: "left",
            lineHeight: "22px",
            fontStyle: "normal"
          }}>
          Certifications will highlight your profile.
        </p>
      </List.Item>
    );
  }

  const certArry = certificate.map((val, index) => {
    return {
      name: val.certificate_name,
      institute: val.cert_Institute,
      date: val.receive_date,
      key: index
    };
  });

  const certArrayComponent = certArry.map(val => {
    return (
      <div
        className=""
        key={val.key}
        style={{
          display: !val.name ? "none" : "block"
        }}>
        <Grid>
          <Grid.Row>
            <Grid.Column mobile={3} computer={1}>
              <List.Content className="">
                <span className="educationThumb">
                  <IcCertification
                    width="40"
                    height="40"
                    rectcolor="#f7f7fb"
                    pathcolor="#c8c8c8"
                    circleColor="#f7f7fb"
                  />
                </span>
              </List.Content>
            </Grid.Column>
            <Grid.Column mobile={13} computer={15}>
              <List.Content className="expDescription padding-left-5">
                <List.Header>
                  <p className="expDescriptionHeader">{val.name}</p>
                </List.Header>
                <List.Description>
                  <p className="margin-bottom-0 padding-bottom-5 expDescriptionSub">
                    {val.institute}{" "}
                  </p>
                  <p
                    style={{
                      display: Number(val.date) ? "block" : "none"
                    }}
                    className="text-trolley-grey font-size-13">
                    {val.date}
                  </p>
                </List.Description>
              </List.Content>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    );
  });

  return certArrayComponent;
};

function getInfoEdu(data) {
  if (!data || !data.edus || !Array.isArray(data.edus)) {
    return null;
  }

  const infoEdu = data.edus.filter(val => {
    return val.onInfo === true;
  });

  if (!infoEdu.length) {
    return null;
  }

  return {
    institute: infoEdu[0].institute
  };
}

function getInfoExp(data) {
  if (!data || !data.exps || !Array.isArray(data.exps)) {
    return null;
  }

  const infoExp = data.exps.filter(val => {
    return val.onInfo === true;
  });

  if (!infoExp.length) {
    return null;
  }

  return {
    name: infoExp[0].name,
    designation: infoExp[0].designation
  };
}

class PublicProfileView extends React.Component {
  onEditIconClick = (ev, { activeSection }) => {
    this.props.onEditModalOpen(activeSection);
  };

  onCloseEditModal = ev => {
    this.props.onEditModalClose();
  };

  render() {
    let user = this.props.user || { loc: {}, ext: {} };

    const {
      jsonData,
      onEditIconClick,
      isModalOpen,
      bannerExt,
      bannerPreviewFile,
      onBannerChange,
      onBannerSave,
      onBannerRemove,
      onBannerModalOpen,
      onBannerModalClose,
      editModalStatus,
      editModalActiveSection,
      data
    } = this.props;

    const infoExp = getInfoExp(user);
    const infoEdu = getInfoEdu(user);

    return (
      <React.Fragment>
        <div
          // className="bg-white"
          style={{
            borderBottomLeftRadius: "5px",
            borderBottomRightRadius: "5px"
          }}>
          <div className="publicProfileBanner postLoginBanner_edit">
            <div>
              <div className="publicPprofileBanner" />
              {/* <Image src={user.ext ? (user.ext.img ? `https://s3-us-west-2.amazonaws.com/img-mwf/banner/${user._id}/image.jpg` : publicProfileBanner) : publicProfileBanner }/> */}
              {/* <Image src={bannerExt ? `https://s3-us-west-2.amazonaws.com/img-mwf/banner/${user._id}/image.png` : publicProfileBanner}/> */}
              <div
                className="PublicProfileBanner_img"
                style={{
                  backgroundImage: `linear-gradient(
                      rgba(0,0,0,0.0),
                      rgba(0,0,0,0.5)
                    ), url(${
                      bannerExt
                        ? `https://s3-us-west-2.amazonaws.com/img-mwf/banner/${
                            user._id
                          }/image.png`
                        : publicProfileBanner
                    })`,
                  height: "220px",
                  backgroundSize: "cover",
                  backgroundPosition: "center"
                }}
              />
              <Modal
                open={isModalOpen}
                className="modal_form"
                onClose={onBannerModalClose}
                trigger={
                  <span onClick={onBannerModalOpen} className="Banner_editIcon">
                    {editIcon}
                  </span>
                }
                closeIcon
                closeOnDimmerClick={false}>
                <EditCoverPage
                  bannerFile={
                    bannerPreviewFile ? bannerPreviewFile : publicProfileBanner
                  }
                  onBannerChange={onBannerChange}
                  onBannerRemove={onBannerRemove}
                  onBannerSave={onBannerSave}
                />
              </Modal>
            </div>
          </div>
          <div className="preProfileDetail mobile__publicProfile">
            <div className="profileEdit_userIcon">
              <Button
                onClick={() =>
                  this.onEditIconClick(null, {
                    activeSection: "personal"
                  })
                }>
                {editIcon}
              </Button>
            </div>

            <div className="preProfileHeaderDetail_container">
              <div className="preProfileHeaderDetail postProfile alingCenter">
                <div className="userImgThumbProfile">
                  {!user.ext || !user.ext.img ? (
                    <React.Fragment>
                      <span className="UserLogo">
                        <UserLogo
                          width="110"
                          height="110"
                          rectcolor="#f7f7fb"
                          pathcolor="#c8c8c8"
                        />
                      </span>
                    </React.Fragment>
                  ) : (
                    <Image
                      src={`https://s3-us-west-2.amazonaws.com/img-mwf/${
                        user._id
                      }/image.jpg`}
                      centered
                      style={{
                        borderRadius: "50%"
                      }}
                      alt={user.fName}
                    />
                  )}
                  <span
                    className="userImgThumb_editIcon"
                    onClick={() =>
                      this.onEditIconClick(null, {
                        activeSection: "profile"
                      })
                    }>
                    {editIcon}
                  </span>
                </div>
                <div className="preProfileTitle">
                  <Header as="h1">
                    {user.fName} {user.lName}
                  </Header>
                  <p className="headTitle">{user.title}</p>
                  <div className="preProfileTitle_container">
                    <p
                      className="text-nobel mtb-2px"
                      style={{
                        display:
                          infoExp && infoExp.designation ? "block" : "none"
                      }}>
                      <span style={{ color: "#575757" }}>
                        {infoExp ? infoExp.designation : ""}
                      </span>{" "}
                      <span>at</span>{" "}
                      <span style={{ color: "#575757" }}>
                        {infoExp ? infoExp.name : ""}
                      </span>
                    </p>
                    <p
                      className="mt4b2"
                      style={{
                        display: infoEdu && infoEdu.institute ? "block" : "none"
                      }}>
                      <span className="padding-right-3">
                        <EducationNoCircleIcon
                          width="17.032"
                          height="12"
                          pathcolor="#c8c8c8"
                        />
                      </span>
                      {infoEdu ? infoEdu.institute : ""}
                    </p>
                    <p
                      className="text-Matterhorn"
                      style={{
                        display: !user.loc || !user.loc.city ? "none" : "flex",
                        alignItems: "stretch",
                        justifyContent: "center"
                      }}>
                      <span className="padding-right-5">
                        <LocationLogo
                          width="9.5"
                          height="11.5"
                          pathcolor="#c8c8c8"
                        />
                      </span>
                      <span style={{ color: "#797979" }}>
                        {user.loc ? user.loc.city : ""}
                      </span>
                    </p>
                    {/* <MobileSocialBtn disabled={true} btntext="Message" /> */}
                  </div>
                </div>
              </div>

              <Responsive maxWidth={1024}>
                <ProfileUploadeResume user={user} />
              </Responsive>

              <LikeViewComponent user={user} />
            </div>

            <div
              className="preProfileDetailBody"
              style={{ paddingTop: "14px" }}>
              <div>
                <InfoSectionGridHeader
                  onEditIconClick={this.onEditIconClick}
                  headerText="Summary"
                  editIcon={editIcon}
                  activeSection={"profile"}>
                  <QuillText value={user.desc ? user.desc : ""} readOnly />
                  {/* <p className="headerSubTitle">{user.desc}</p> */}
                  {/* user.desc */}
                </InfoSectionGridHeader>
              </div>
              <div style={{ paddingTop: "20px" }}>
                <InfoSectionGridHeader
                  headerText="Skills"
                  containerClass="filterSkill"
                  onEditIconClick={this.onEditIconClick}
                  editIcon={editIcon}
                  activeSection={"skill"}>
                  {processSkillsOptional(user.skills)}
                </InfoSectionGridHeader>
              </div>
              {/* style={{ display: !user.desc ? "none" : "block" }} */}

              <div style={{ paddingTop: "20px" }}>
                <InfoSectionGridHeader
                  headerText="Experience"
                  containerClass="filterSkill"
                  onEditIconClick={this.onEditIconClick}
                  editIcon={editIcon}
                  activeSection={"experience"}>
                  {processExperience(user.exps)}
                </InfoSectionGridHeader>
              </div>
              <div style={{ paddingTop: "20px" }}>
                <InfoSectionGridHeader
                  headerText="Education"
                  containerClass="filterSkill"
                  onEditIconClick={this.onEditIconClick}
                  editIcon={editIcon}
                  activeSection="education">
                  {processEducation(user.edus)}
                </InfoSectionGridHeader>
              </div>
              <div style={{ paddingTop: "20px" }}>
                <InfoSectionGridHeader
                  headerText="Certificate"
                  containerClass="filterSkill"
                  onEditIconClick={this.onEditIconClick}
                  editIcon={editIcon}
                  activeSection="certificate">
                  {processCertificate(user.certs)}
                </InfoSectionGridHeader>
              </div>

              <div
                style={{ paddingTop: "20px" }}
                className="desiredJob_postLogin">
                <InfoSectionGridHeader
                  headerText="Desired job"
                  subHeaderText={<DesiredJobSubHeader />}
                  containerClass="filterSkill"
                  onEditIconClick={this.onEditIconClick}
                  editIcon={editIcon}
                  activeSection={"desired"}>
                  {DesiredJob(user.desireJob)}
                </InfoSectionGridHeader>
              </div>
            </div>
          </div>
        </div>

        <div style={{ paddingTop: "20px" }}>
          <UserView onClick={this.onEditIconClick} />
        </div>

        <Responsive maxWidth={1024}>
          <div style={{ paddingTop: "20px" }}>
            <ConvertProfileResume />
          </div>
        </Responsive>
        <Modal
          className="modal_form"
          onClose={this.onEditIconClick}
          open={editModalStatus}
          closeIcon
          closeOnDimmerClick={false}>
          <EditProfilePage
            userData={user}
            onCancelClick={this.onCloseEditModal}
            activeSection={editModalActiveSection}
          />
        </Modal>

        {/* <div className="">
          {
            user._id ? (
              <P2PTracker
                reqId={window.localStorage.getItem(USER.UID)}
                userId={user._id}
                cardType={"FooterP2P"}
              />
            ) : null
          }
        </div> */}
      </React.Fragment>
    );
  }
}

export default PublicProfileView;
