import React from "react";
import "./index.scss";
import { Header, Grid, Button, List } from "semantic-ui-react";

const UserView = props => {
  return (
    <div className="UserView_main">
      <div className="UserView_mainInner">
        <Button onClick={props.onClick} activeSection="all">
          Add more details
        </Button>
        <p>This would boost your profile visibility</p>
      </div>
    </div>
  );
};

export default UserView;
