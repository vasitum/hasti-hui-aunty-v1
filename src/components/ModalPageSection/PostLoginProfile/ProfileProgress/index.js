import React from "react";

import { Progress, Card, Header, Button, Grid } from "semantic-ui-react";
import PropTypes from "prop-types";

import "./index.scss";

function getProfileProgress(userStatus) {
  switch (userStatus.length) {
    case 0:
      return 100;
    case 1:
      return 65;
    case 2:
      return 45;
    case 3:
      return 30;
    case 4:
      return 20;
    default:
      return 20;
  }
}

function getCurrentSection(userStatus) {
  return userStatus[0];
}
const ProfileProgress = props => {
  const { userStatus, onStatusSkip, onAddBtnClick } = props;

  const profileProgress = getProfileProgress(userStatus);
  const currentSection = getCurrentSection(userStatus);

  return (
    <Card
      className="ProfileProgress"
      fluid
      style={{
        display: profileProgress === 100 ? "none" : "block"
      }}>
      <Card.Content>
        <Card.Header>
          <div className="Progressbar">
            <Progress percent={profileProgress} size="small" />
            <Header as="h3">{profileProgress}% profile completed</Header>
          </div>
        </Card.Header>
        <Card.Description>
          <p className="title">
            Add {currentSection} to increase your<span
              style={{ color: "#323232" }}>
              {" "}
              Profile Visibility
            </span>
          </p>
          {/* <p className="subTitle">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
          </p> */}
        </Card.Description>
      </Card.Content>
      <Card.Content className="completeProfileBtn">
        <Grid>
          <Grid.Row>
            <Grid.Column width={8}>
              <Button
                onClick={onAddBtnClick}
                activeSection={currentSection}
                primary
                compact
                className="addBtn"
                fluid>
                Add
              </Button>
            </Grid.Column>
            <Grid.Column width={8}>
              <Button onClick={onStatusSkip} compact className="skipBtn" fluid>
                Skip for now
              </Button>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Card.Content>
    </Card>
  );
};

ProfileProgress.propTypes = {
  placeholder: PropTypes.string
};

export default ProfileProgress;
