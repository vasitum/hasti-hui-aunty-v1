import React from "react";

import FlatFileuploadBtn from "../../../Buttons/FlatFileUploadeBtn";
import MoreOption from "../MoreOption";

import "./index.scss";

const profileUploadeResume = props => {
  return (
    <div className="profileUploadeResume">
      <FlatFileuploadBtn
        // onChange={this.onResumeChange}
        accept=".doc, .docx, .rtf, .pdf, .jpeg, .jpg"
        // btnText={getResumePlaceHolder(
        //   this.state.isResumeButtonLoading,
        //   userhasResume
        // )}
        btnText="Update Resume"
      />

      <MoreOption user={props.user} />
    </div>
  );
};

export default profileUploadeResume;
