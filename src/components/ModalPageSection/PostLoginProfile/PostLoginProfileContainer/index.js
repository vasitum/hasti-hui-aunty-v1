import React from "react";
import { Grid, Container } from "semantic-ui-react";
import PublicProfileView from "../publicProfile";
import ProfileRightSide from "../profileRightSide";
import getUserById from "../../../../api/user/getUserById";
import updateUserExtns from "../../../../api/user/updateUserExtns";
import UserConstants from "../../../../constants/storage/user";
import { USER } from "../../../../constants/api";
import { uploadImage } from "../../../../utils/aws";
import PageLoader from "../../../PageLoader";

import UserExpPopup from "../../../Dashboard/Common/UserExpPopup";
//import UserExpPopup from "../../../Dashboard/Common/UserProfileFeedBackPopup";
// import "./index.scss";
import { toast } from "react-toastify";

const Loader = props => (
  <div className="m_showbox">
    <div className="m_loader">
      <svg className="m_circular" viewBox="25 25 50 50">
        <circle
          className="m_path"
          cx="50"
          cy="50"
          r="20"
          fill="none"
          stroke-width="2"
          stroke-miterlimit="10"
        />
      </svg>
    </div>
  </div>
);

class PostLoginProfileContainer extends React.Component {
  state = {
    isLoading: false,
    profileJson: {},
    bannerExt: "",
    bannerPreviewFile: "",
    bannerPreviewUpdateFile: "",
    isImageModalOpen: false,
    isEditModalOpen: false,
    editModalActiveSection: "",
    userProfileStatus: [],
    isFormLoading: false,
    modal: false
  };

  constructor(props) {
    super(props);
    this.onRemoveCoverImage = this.onRemoveCoverImage.bind(this);
    this.onSaveCoverImage = this.onSaveCoverImage.bind(this);
    this.onResumeUploaded = this.onResumeUploaded.bind(this);
  }

  getUserProfileStatus = user => {
    const userProfileStatus = [];

    // experience
    if (!user.exps) {
      userProfileStatus.push("experience");
    }

    if (user.exps && user.exps.length > 0) {
      if (!user.exps[0].name || !user.exps[0].designation) {
        userProfileStatus.push("experience");
      }
    }

    // education
    if (!user.edus) {
      userProfileStatus.push("education");
    }

    if (user.edus && user.edus.length > 0) {
      if (!user.edus[0].course || !user.edus[0].institute) {
        userProfileStatus.push("education");
      }
    }

    // desired job
    if (!user.desireJob) {
      userProfileStatus.push("desired");
    }

    if (!user.desireJob.role) {
      userProfileStatus.push("desired");
    }

    // certificate
    if (!user.certs) {
      userProfileStatus.push("certificate");
    }

    if (user.certs && user.certs.length > 0) {
      if (!user.certs[0].cert_Institute || !user.certs[0].certificate_name) {
        userProfileStatus.push("certificate");
      }
    }

    return userProfileStatus;
  };

  onRemoveProfileStatus = idx => {
    this.setState({
      userProfileStatus: this.state.userProfileStatus.filter((val, index) => {
        return index !== idx;
      })
    });
  };

  async componentDidMount() {
    // const { match } = this.props;

    try {
      const res = await getUserById(window.localStorage.getItem(USER.UID));
      const resJson = await res.json();

      // console.log(resJson);
      window.localStorage.setItem(
        UserConstants.USER_EDIT_DATA,
        JSON.stringify(resJson)
      );
      resJson.userPreference
        ? this.setState({ modal: false })
        : this.setState({ modal: true });
      this.setState({
        profileJson: resJson,
        userProfileStatus: this.getUserProfileStatus(resJson),
        bannerExt: resJson.ext.imgBanner,
        bannerPreviewFile: resJson.ext.imgBanner
          ? `https://s3-us-west-2.amazonaws.com/img-mwf/banner/${
              resJson._id
            }/image.png`
          : "",
        isLoading: false
      });
    } catch (error) {
      console.log(error);
    }
  }

  onChangeCoverImage = ev => {
    const file = ev.target.files[0];
    const fr = new FileReader();

    fr.onloadend = e => {
      this.setState({
        bannerPreviewFile: e.target.result,
        bannerPreviewUpdateFile: file
      });
    };

    fr.readAsDataURL(file);
  };

  async onSaveCoverImage(ev) {
    this.setState({
      isFormLoading: true
    });

    try {
      const res = await uploadImage(
        `banner/${this.state.profileJson._id}/image.png`,
        this.state.bannerPreviewUpdateFile
      );

      const data = await updateUserExtns({
        img: null,
        resume: null,
        imgBanner: "jpg"
      });

      const dataJson = await data.text();

      if (res && dataJson) {
        toast("banner updated!");
        window.location.reload();
      }

      this.setState({
        isFormLoading: false
      });
    } catch (error) {
      console.error(error);
    }
  }

  async onRemoveCoverImage(ev) {
    this.setState(
      {
        bannerExt: "",
        bannerPreviewFile: "",
        isImageModalOpen: false
      },
      async () => {
        try {
          const res = await updateUserExtns({
            img: null,
            resume: null,
            imgBanner: ""
          });

          const json = await res.text();

          if (json) {
            toast("Profile, Banner Removed!");
          }
        } catch (error) {
          console.error(error);
        }
      }
    );
  }

  onImageModalOpen = ev => {
    this.setState({
      isImageModalOpen: true
    });
  };

  onImageModalClose = ev => {
    this.setState({
      isImageModalOpen: false
    });
  };

  async onResumeUploaded(extn) {
    this.setState({
      profileJson: {
        ...this.state.profileJson,
        ext: {
          ...this.state.profileJson.ext,
          resume: extn
        }
      }
    });

    try {
      const data = await updateUserExtns({
        img: null,
        imgBanner: null,
        resume: this.state.profileJson.ext.resume
      });

      // update it!
      // const res = await data.toString();

      if (data) {
        toast("Resume, Uploaded");
      }
    } catch (error) {}
  }

  onEditModalOpen = activeSection => {
    this.setState({
      isEditModalOpen: !this.state.isEditModalOpen,
      editModalActiveSection: activeSection
    });
  };

  onEditModalClose = ev => {
    this.setState({
      isEditModalOpen: !this.state.isEditModalOpen
    });
  };

  onStatusSkip = ev => {
    const profileStatus = [...this.state.userProfileStatus];
    profileStatus.shift();

    this.setState({
      userProfileStatus: []
    });
  };

  handleModal = () => this.setState({ modal: false });

  render() {
    const { modal, modalFeedback } = this.state;
    // return <div />
    if (this.state.isLoading) {
      return (
        <div>
          <Loader />
        </div>
      );
    } else {
      return (
        <div className="public-profile">
          <Container>
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} computer={12}>
                  <PublicProfileView
                    user={this.state.profileJson}
                    isModalOpen={this.state.isImageModalOpen}
                    bannerExt={this.state.bannerExt}
                    bannerPreviewFile={this.state.bannerPreviewFile}
                    onBannerChange={this.onChangeCoverImage}
                    onBannerSave={this.onSaveCoverImage}
                    onBannerRemove={this.onRemoveCoverImage}
                    onBannerModalOpen={this.onImageModalOpen}
                    onBannerModalClose={this.onImageModalClose}
                    onEditModalOpen={this.onEditModalOpen}
                    onEditModalClose={this.onEditModalClose}
                    editModalStatus={this.state.isEditModalOpen}
                    editModalActiveSection={this.state.editModalActiveSection}
                  />
                </Grid.Column>
                <Grid.Column width={4} className="mobile hidden">
                  <ProfileRightSide
                    user={this.state.profileJson}
                    userProfileStatus={this.state.userProfileStatus}
                    onStatusSkip={this.onRemoveProfileStatus}
                    onResumeUploaded={this.onResumeUploaded}
                    onEditModalOpen={this.onEditModalOpen}
                    onStatusSkip={this.onStatusSkip}
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Container>
          <UserExpPopup open={modal} handleModal={this.handleModal} />

          <PageLoader
            active={this.state.isFormLoading}
            loaderText={"Uploading Banner ..."}
          />
        </div>
      );
    }
  }
}

export default PostLoginProfileContainer;
