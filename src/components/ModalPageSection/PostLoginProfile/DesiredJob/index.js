import React from "react";

import { Progress, Card, Header, Button, List, Grid } from "semantic-ui-react";
import PropTypes from "prop-types";

import IcCompany from "../../../../assets/svg/IcCompany";
import { DESIRED_JOB } from "../../../../strings";
import "./index.scss";

const DesiredJob = val => {
  if (!val || !val.role) {
    return (
      <List.Item>
        <p
          style={{
            fontFamily: "Open Sans",
            fontSize: "14px",
            color: "#c8c8c8",
            textAlign: "left",
            lineHeight: "22px",
            fontStyle: "normal"
          }}>
          {/* Highlight your skills and show employers what you're really good at ! */}
          {DESIRED_JOB.PAGE_TITLE1}
          <br />
          <br />
          {/* Adding your top skills will let you engage in discussion to showcase
          your expertise, */}
          {DESIRED_JOB.PAGE_TITLE2}
          {/* <br />
          For example, Your specialties could be Photoshop, web development,
          customer service, or interior design! */}
        </p>
      </List.Item>
    );
    return null;
  }

  return (
    <div className="DesiredJob">
      <Grid>
        <Grid.Row>
          <Grid.Column mobile={3} computer={1}>
            <List.Content className="">
              <span className="educationThumb">
                <IcCompany
                  width="40"
                  height="40"
                  rectcolor="#f7f7fb"
                  pathcolor="#c8c8c8"
                />
              </span>
            </List.Content>
          </Grid.Column>
          <Grid.Column mobile={13} computer={15}>
            <List.Content className="DesiredJobHeader">
              <List.Header>
                <p className="DesiredJobHeaderTitle">{val.role}</p>
              </List.Header>
              <List.Description>
                {/* <p className="subHeader">UI UX Designer</p>
                <p className="subHeaderTitle"></p> */}
                <p className="subHeaderTitle">{val.empType}</p>
                <p className="subHeaderTitle">
                  {val.currentCtc} LPA - {val.expectedCtc} LPA
                </p>
              </List.Description>
            </List.Content>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
};

DesiredJob.propTypes = {
  placeholder: PropTypes.string
};

export default DesiredJob;
