import React from "react";
import "./index.scss";
import { Header, Grid, Button, List } from "semantic-ui-react";

const UserView = props => {
  return (
    <div className="UserView_main">
      <List>
        <Button>Add more details</Button>
        <p>This would boost your profile visibility</p>
      </List>
    </div>
  );
};

export default UserView;
