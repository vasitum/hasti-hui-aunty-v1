import React from "react";
import { Button } from "semantic-ui-react";
import { Link } from "react-router-dom";
import "./index.scss";

const ConvertProfileResume = () => {
  return (
    <div className="ConvertProfileResume">
      <p>Convert profile as resume</p>
      <Button as={Link} to="/cv-template">
        View templates
      </Button>
    </div>
  );
};

export default ConvertProfileResume;
