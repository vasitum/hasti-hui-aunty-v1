import React from "react";
import { List, Image, Header } from "semantic-ui-react";
// import publicProfileBanner from "../../../assets/img/publicProfileBanner.png";
// import publicProfile from "../../../assets/img/publicProfile.png";
import profile from "../../../assets/img/profile.png";

import ProfileProgress from "./ProfileProgress";
import FlatFileuploadBtn from "../Buttons/FlatFileUploadeBtn";

import { uploadResume } from "../../../utils/aws";
import { toast } from "react-toastify";

import ConvertProfileResume from "./ConvertProfileResume";

import PageLoader from "../../PageLoader";

function hasResume(ext) {
  if (!ext || !ext.resume) {
    return false;
  }

  return true;
}

function getResumePlaceHolder(isResumeLoading, hasResume) {
  if (isResumeLoading) {
    return "Uploading ...";
  }

  return hasResume ? "Update Resume" : "Upload Resume";
}

class ProfileRightSide extends React.Component {
  state = {
    isResumeButtonLoading: false,
    resumeExtn: ""
  };

  constructor(props) {
    super(props);
    this.onResumeChange = this.onResumeChange.bind(this);
  }

  async onResumeChange(ev) {
    const { user, onResumeUploaded } = this.props;
    const file = ev.target.files[0];

    if (!file) {
      return;
    }
    if (this.toast) {
      toast.dismiss(this.toast);
    }

    // const extn = file.name.split(".").pop();
    const EXTNS = ["doc", "docx", "rtf", "pdf", "jpeg", "jpg"];
    const extn = file.name.split(".").pop();
    // console.log("EXTN IS", extn);
    if (!extn || EXTNS.indexOf(String(extn).toLowerCase()) === -1) {
      this.toast = toast(
        "Error, please select any one of .doc, .docx, .rtf, .pdf, .jpeg, .jpg"
      );
      return;
    }

    // tell user we are uploading
    this.setState({
      isResumeButtonLoading: true
    });

    try {
      const res = await uploadResume(
        `${user._id}/${user.fName}_${user.lName}.${extn}`,
        file
      );
      if (res) {
        // toast("Resume Updated");
      }

      // console.log("check user profile data", user);

      // tell app that resume is updated
      onResumeUploaded(extn);
    } catch (error) {
      console.error(error);
    }

    this.setState({
      isResumeButtonLoading: false
    });
  }

  onAddBtnClick = (e, { activeSection }) => {
    this.props.onEditModalOpen(activeSection);
  };

  render() {
    const { user, userProfileStatus, onStatusSkip } = this.props;
    const userhasResume = hasResume(user.ext);
    return (
      <div className="similar-people postLoginProfileRight padding-top-20">
        <ProfileProgress
          userStatus={userProfileStatus}
          onStatusSkip={onStatusSkip}
          onAddBtnClick={this.onAddBtnClick}
        />

        <div className="fileUpload">
          <FlatFileuploadBtn
            onChange={this.onResumeChange}
            accept=".doc, .docx, .rtf, .pdf, .jpeg, .jpg"
            btnText={getResumePlaceHolder(
              this.state.isResumeButtonLoading,
              userhasResume
            )}
          />
          {userhasResume && (
            <a
              className="title"
              href={`https://s3-us-west-2.amazonaws.com/res-mwf/${
                user._id
              }/${user.fName + "_" + user.lName + "." + user.ext.resume}`}
              target="_blank">
              {user.fName + "_" + user.lName + "." + user.ext.resume}
            </a>
          )}
          <p className="subTitle">
            Supported formats: .DOC, .DOCX, .RTF, .PDF, JPEG
          </p>
        </div>

        <ConvertProfileResume />
        <PageLoader
          active={this.state.isResumeButtonLoading}
          loaderText={"Updating Resume"}
        />
      </div>
    );
  }
}

export default ProfileRightSide;
