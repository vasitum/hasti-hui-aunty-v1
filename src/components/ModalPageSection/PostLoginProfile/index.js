import React from "react";
import { Responsive } from "semantic-ui-react";


import PostLoginProfileContainer from "./PostLoginProfileContainer";
import ReactGA from "react-ga";
import PostLoginProfileMobile from "./PostLoginProfileMobile";
import "./index.scss";


class PublicProfile extends React.Component {
  componentDidMount() {
    ReactGA.pageview('/user/view/profile');
  }
  render() {

    return (
      <div >
        <Responsive maxWidth={1024}>
          <PostLoginProfileMobile />
        </Responsive>
        <Responsive minWidth={1025}>
          <PostLoginProfileContainer />
        </Responsive>
      </div>
    );
  }
}


export default PublicProfile;
