import React from "react";

import { Progress, Card, Header, Button, Grid } from "semantic-ui-react";
import PropTypes from "prop-types";
import IcPrivacyLock from "../../../../assets/svg/IcPrivacyLock";

import "./index.scss";

const DesiredJobSubHeader = props => {
  return (
    <div className="DesiredJobSubHeader">
      <IcPrivacyLock pathcolor="#acaeb5" height="14" width="10" />
      <span>Only you can see this</span>
    </div>
  );
};

DesiredJobSubHeader.propTypes = {
  placeholder: PropTypes.string
};

export default DesiredJobSubHeader;
