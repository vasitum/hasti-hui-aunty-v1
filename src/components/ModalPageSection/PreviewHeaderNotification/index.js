import React from "react";

import { Button } from "semantic-ui-react";

import IcFooterArrowIcon from "../../../assets/svg/IcFooterArrowIcon";

import { withRouter } from "react-router-dom";

import "./index.scss";

const PreviewHeaderNotification = props => {
  const { publicTitle, history, ...resProps } = props;
  return (
    <div className="preview_headerNotification" {...resProps}>
      <div className="btnBox">
        <Button
          className="notificationBtn"
          onClick={ev => {
            history.go(-1);
          }}>
          <IcFooterArrowIcon />
        </Button>
      </div>
      <p>This is what your {publicTitle} looks like to: Public</p>
    </div>
  );
};

export default withRouter(PreviewHeaderNotification);
