import React from "react";
import { Grid } from "semantic-ui-react";

import { SearchBox } from "react-instantsearch-dom";

import { withSearch } from "../../../../AlgoliaContainer";
import { withRouter } from "react-router-dom";
import MobileBestPane from "./moblieBest";

class MobileBestPaneTab extends React.Component {
  render() {
    return (
      <Grid>
        <Grid.Row>
          <Grid.Column mobile={16} computer={12} className="">
            <SearchBox />
            <MobileBestPane />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

export default withRouter(
  withSearch(MobileBestPaneTab, {
    apiKey: process.env.REACT_APP_ALGOLIA_API_KEY,
    appId: process.env.REACT_APP_ALGOLIA_APP_ID,
    indexName: "job"
  })
);
