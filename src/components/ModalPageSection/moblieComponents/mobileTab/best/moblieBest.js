import React from "react";
import {
  Button,
  Sidebar,
  Menu,
  Icon,
  Segment,
  Header,
  Card
} from "semantic-ui-react";

import { connect } from "react-redux";

// card hits
import { connectHits } from "react-instantsearch/connectors";
// import { withRouter, Link } from "react-router-dom";
import { Index } from "react-instantsearch/dom";

import SearchCardHeader from "../../../commonComponent/searchCardHeader";
import SearchCardPeople from "../../../commonComponent/searchCardPeople";

// import FoundJobBanner from "../../../banners/bestFoudJob";

import { list } from "postcss";
// import { grid } from "Constants/semantic";

import BestFoudJob from "../../../banners/bestFoudJob";
import BestFoundPeopleBanner from "../../../banners/bestFoudPeople";

const MobileBestJobConnected = class extends React.Component {
  onViewJobClick = (ev, { tohref }) => {
    this.props.history.push("/job/public/" + tohref);
  };

  render() {
    const { hits, jobIndex, jobClick } = this.props;

    return (
      <div>
        <div className="foundJobBanner mobile__searchBanner">
          <BestFoudJob />
        </div>
        <Card fluid className="primary search__card">
          <Card.Content>
            {(function(hits) {
              let ret = [];
              for (let i = 0; i < 2; i++) {
                ret.push(
                  <div key={i}>
                    <SearchCardHeader
                      idx={i}
                      val={hits[i]}
                      headerlink="/view/job/"
                    />
                  </div>
                );
              }
              return ret;
            })(hits)}
          </Card.Content>
        </Card>
      </div>
    );
  }
};

const MobileBestPeopleConnected = class extends React.Component {
  onViewJobClick = (ev, { tohref }) => {
    this.props.history.push("/job/public/" + tohref);
  };
  render() {
    const { hits, jobIndex, jobClick } = this.props;

    return (
      <div>
        <div className="foundPeopleBanner mobile__searchBanner">
          <BestFoundPeopleBanner />
        </div>
        <Card fluid className="primary search__card">
          <Card.Content>
            {(function(hits) {
              let ret = [];
              for (let i = 0; i < 2; i++) {
                ret.push(
                  <div key={i}>
                    <SearchCardPeople
                      idx={i}
                      val={hits[i]}
                      classname="displayNone"
                      headerlink="/view/user/"
                    />
                  </div>
                );
              }
              return ret;
            })(hits)}
          </Card.Content>
        </Card>
      </div>
    );
  }
};

const BestJobConnectedAlgolia = connectHits(MobileBestJobConnected);
const BestPeopleConnectedAlgolia = connectHits(MobileBestPeopleConnected);

class MobileBestPane extends React.Component {
  render() {
    // const { jobIndex } = this.state;
    return (
      <div className="peoplePane">
        {/* <div className="foundJobBanner mobile__searchBanner">
          <FoundJobBanner />
        </div> */}
        {/* <div className="filter__headerBtn">
          <Button className="filter__jobsBtn">Filter Jobs</Button>
          <Button className="search__alertBtn">Get search alert</Button>
        </div> */}

        <div className="searchFilterCard">
          <div className="" style={{ marginTop: "10px" }}>
            <BestJobConnectedAlgolia history={this.props.history} />
          </div>
          <div style={{ marginTop: "20px" }}>
            <Index indexName="user">
              <div className="cardsGroupsPeople cards-group">
                <BestPeopleConnectedAlgolia />
              </div>
            </Index>
          </div>
        </div>
      </div>
    );
  }
}

// export default withRouter(MobileBestPane);
export default MobileBestPane;
