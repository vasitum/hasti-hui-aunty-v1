import React from "react";
import { Grid } from "semantic-ui-react";

import { SearchBox } from "react-instantsearch-dom";

import { withSearch } from "../../../../AlgoliaContainer";
import { withRouter } from "react-router-dom";

import FoundJobBanner from "../../../banners/foundJobBanner";

import SearchFilterPanel from "../../../SearchFilter/SearchFilterPanel";

import MobilePeoplePane from "./mobilePeople";

class MobilePeoplePaneTab extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowFilter: false
    };
  }

  isShowMobileFilter = () => {
    this.setState({
      isShowFilter: true
    });
  };

  isShowMobileFilterClose = () => {
    this.setState({
      isShowFilter: false
    });
  };
  render() {
    return (
      <div className="" style={{ paddingBottom: "30px" }}>
        <SearchFilterPanel
          isShowMobileFilterClose={this.isShowMobileFilterClose}
          style={{
            display: this.state.isShowFilter ? "block" : "none"
          }}
          onCloseClick={this.isMobileFilterClose}
          subTitle="profile"
          filterFor="People"
          filters={["locCity", "skills", "com", "edus", "specs"]}
        />
        <div
          style={{
            display: !this.state.isShowFilter ? "block" : "none"
          }}>
          <Grid>
            <Grid.Row>
              <Grid.Column mobile={16} computer={12} className="">
                <SearchBox />
                <MobilePeoplePane
                  isShowMobileFilter={this.isShowMobileFilter}
                />
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>
      </div>
    );
  }
}

export default withRouter(
  withSearch(MobilePeoplePaneTab, {
    apiKey: process.env.REACT_APP_ALGOLIA_API_KEY,
    appId: process.env.REACT_APP_ALGOLIA_APP_ID,
    indexName: "user",
    noRouting: true
  })
);
