import React from "react";

import {
  Button,
  List,
  Menu,
  Icon,
  Segment,
  Header,
  Grid,
  Card
} from "semantic-ui-react";

import { connectHits } from "react-instantsearch/connectors";
import { withRouter, Link } from "react-router-dom";

import SearchCardHeader from "../../../commonComponent/searchCardHeader";
import FoundJobBanner from "../../../banners/foundJobBanner";

import FilterIcon from "../../../../../assets/svg/IcFilterIcon";
import SearchAlertIcon from "../../../../../assets/svg/IcSearchAlertIcon";

const MobileJobConnected = class extends React.Component {
  onViewJobClick = (ev, { tohref }) => {
    this.props.history.push("/job/public/" + tohref);
  };

  render() {
    const { hits, jobIndex, jobClick } = this.props;
    // const { val } = this.props;

    return (
      <div>
        <div className="foundJobBanner mobile__searchBanner">
          <FoundJobBanner />
        </div>
        <Card fluid className="primary search__card">
          <Card.Content>
            {hits.map((val, idx) => {
              let jsonData = "";
              try {
                jsonData = JSON.parse(val.desc);
              } catch (error) {}

              return (
                <div key={idx}>
                  <SearchCardHeader val={val} headerlink="/view/job/" />
                </div>
              );
            })}
          </Card.Content>
        </Card>
      </div>
    );
  }
};

const MobileJobConnectedAlgolia = connectHits(MobileJobConnected);

class MobileJobPane extends React.Component {
  render() {
    // console.log("MOBILE_JOB_PANE", this.props.sidebarToggle);

    return (
      <div className="peoplePane">
        <div className="filter__headerBtn">
          <Button
            className="filter__jobsBtn"
            onClick={this.props.isShowMobileFilter}>
            <span>
              <FilterIcon
                width="14.41"
                height="11"
                viewbox="0 0 16.41 14.375"
                pathcolor="#ffffff"
              />
            </span>
          </Button>
          {/* <Button className="search__alertBtn">
            <SearchAlertIcon width="14.38" height="12.187" viewbox="0 0 17.38 16.187" pathcolor="#c8c8c8" rectcolor="#c8c8c8"/>
            Get search alert
          </Button> */}
        </div>
        <div className="searchFilterCard" style={{ marginTop: "15px" }}>
          <MobileJobConnectedAlgolia history={this.props.history} />
        </div>
      </div>
    );
  }
}

// export default withRouter(MobileJobPane);
export default MobileJobPane;
