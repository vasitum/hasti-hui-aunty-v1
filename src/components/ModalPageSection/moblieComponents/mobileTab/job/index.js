import React from "react";
import { Grid } from "semantic-ui-react";

import { SearchBox } from "react-instantsearch-dom";

import SearchFilterPanel from "../../../SearchFilter/SearchFilterPanel";

import FoundJobBanner from "../../../banners/foundJobBanner";
import CandidateCVFilter from "../../../../Dashboard/CandidateDashbord/CandidateApplyedJobs/CandidateCVFilter";
import { withRouter } from "react-router-dom";

import withSearch from "../../../../AlgoliaContainer/containers/withSearch";
import MobileJobPane from "./mobileJob";

class MobileJobPaneTab extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowFilter: false
    };
  }

  isShowMobileFilter = () => {
    this.setState({
      isShowFilter: true
    });
  };

  isShowMobileFilterClose = () => {
    this.setState({
      isShowFilter: false
    });
  };

  render() {
    // console.log("MOBILE_JOB_PANE_TAB", this.props.sidebarToggle);
    return (
      <div className="" style={{ paddingBottom: "30px" }}>
        <SearchFilterPanel
          isShowMobileFilterClose={this.isShowMobileFilterClose}
          style={{
            display: this.state.isShowFilter ? "block" : "none"
          }}
          onCloseClick={this.isMobileFilterClose}
          subTitle="jobs"
          filterFor="Jobs"
          filters={["locCity", "skills", "comName", "jobType"]}
        />
        <CandidateCVFilter
          label={"Status"}
          attribute="status"
          defaultRefinement={true}
          value={"Active"}
          style={{
            display: "none"
          }}
        />
        <div
          style={{
            display: !this.state.isShowFilter ? "block" : "none"
          }}>
          <Grid>
            <Grid.Row>
              <Grid.Column mobile={16} computer={12} className="">
                <SearchBox />
                <MobileJobPane isShowMobileFilter={this.isShowMobileFilter} />
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>
      </div>
    );
  }
}

export default withRouter(
  withSearch(MobileJobPaneTab, {
    apiKey: process.env.REACT_APP_ALGOLIA_API_KEY,
    appId: process.env.REACT_APP_ALGOLIA_APP_ID,
    indexName: "job",
    noRouting: true
  })
);
