import React from "react";

// import { Link } from 'react-router-dom';
import { Tab, Menu, Label } from "semantic-ui-react";

import MobileBestPaneTab from "./best";
import MobileJobPaneTab from "./job";
import MobilePeoplePaneTab from "./people";

import SearchFilterPanel from "../../SearchFilter/SearchFilterPanel";
import getActiveTab from "../../../../utils/ui/getActiveTab";
import qs from "qs";
import { connect } from "react-redux";

import { Link, withRouter } from "react-router-dom";

function getTabPanes(parsedQS, searchCount, currentValue) {
  return [
    {
      menuItem: (
        <Menu.Item
          as={Link}
          to={"/search" + "?query=" + encodeURIComponent(currentValue)}
          key={"Best" + currentValue}
          className="">
          Best
        </Menu.Item>
      ),
      render: () => {
        return (
          <div className="bestTab">
            <Tab.Pane attached={false}>
              <MobileBestPaneTab />
            </Tab.Pane>
          </div>
        );
      }
    },
    {
      menuItem: (
        <Menu.Item
          as={Link}
          to={"/search/job" + "?query=" + encodeURIComponent(currentValue)}
          key={"Job" + currentValue}
          className="">
          Jobs
        </Menu.Item>
      ),
      render: () => {
        return (
          <div className="">
            <Tab.Pane attached={false}>
              <MobileJobPaneTab />
            </Tab.Pane>
          </div>
        );
      }
    },
    {
      menuItem: (
        <Menu.Item
          as={Link}
          to={"/search/people" + "?query=" + encodeURIComponent(currentValue)}
          key={"People" + currentValue}
          className="">
          People
        </Menu.Item>
      ),
      render: () => {
        return (
          <div className="">
            <Tab.Pane attached={false}>
              <MobilePeoplePaneTab />
            </Tab.Pane>
          </div>
        );
      }
    }
  ];
}

class MobileRearchReasult extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    if (!this.props.sidebarToggle) {
      console.log("IT IS UNDEFINED");
    }

    const { match } = this.props;
    return (
      <div
        className="mobile__searchResult mobileScreen_container widescreen hidden large screen hidden computer hidden tablet hidden"
        style={{ marginTop: "0px" }}>
        <Tab
          menu={{ text: true }}
          panes={getTabPanes(null, this.props.search, this.props.currentValue)}
          sidebarToggle={this.props.sidebarToggle}
          activeIndex={getActiveTab(match.params.pageid, {
            job: 1,
            people: 2
          })}
        />
        {/* <SearchFilterPanel  subTitle="jobs"
                filterFor="Jobs"
                filters={["locCity", "skills", "comName", "jobType"]}/> */}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    currentValue: state.search.currentValue,
    search: state.search.searchCount
  };
};

export default withRouter(connect(mapStateToProps)(MobileRearchReasult));
