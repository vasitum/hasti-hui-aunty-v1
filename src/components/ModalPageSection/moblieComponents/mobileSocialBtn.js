import React from "react";
import { Icon, Button, Dropdown } from "semantic-ui-react";

import MoreOptionIcon from "../../../assets/svg/IcMoreOptionIcon";

const trigger = (
  <span className="moblile__moreOptionBtn">
    <MoreOptionIcon
      width="5"
      height="18.406"
      pathcolor="#797979"
      viewbox="0 0 5 18.406"
    />
  </span>
);

const options = [
  { key: "Like", text: "Like" },
  { key: "Share", text: "Share" },
  { key: "Save", text: "Save" }
];

export const MobileSocialBtn = props => {
  const { icon, disabled, ...resProps } = props;
  return (
    <span className="widescreen hidden large screen hidden computer hidden tablet hidden">
      <Button
        disabled={disabled}
        {...resProps}
        tohref={props.tohref}
        size="mini"
        onClick={() => {
          if (props.onChatStart) {
            props.onChatStart();
          }
        }}
        className="actionBtn moblile__actionBtn">
        {props.btntext} {icon ? icon : null}
      </Button>
      {/* <Dropdown
        trigger={trigger}
        options={options}
        pointing="top right"
        icon={null}
        disabled={disabled}
      /> */}
    </span>
  );
};

export default MobileSocialBtn;
