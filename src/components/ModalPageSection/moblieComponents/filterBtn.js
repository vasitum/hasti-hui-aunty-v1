import React from "react";
import {
  Button, Sidebar, Menu, Icon, Segment, Header
} from "semantic-ui-react";



class FilterBtn extends React.Component {
  state = { visible: false }
  toggleVisibility = () => this.setState({ visible: !this.state.visible })

  render() {
    const { visible } = this.state
    return (
      <div>
        <div className="filter__headerBtn">
          <Button className="filter__jobsBtn" onClick={this.toggleVisibility}>Filter Jobs</Button>
          <Button className="search__alertBtn">Get search alert</Button>
        </div>
        <div>
          <Sidebar.Pushable as={Segment}>
            <Sidebar as={Menu} animation='overlay' width='thin' visible={visible} icon='labeled' vertical inverted>
              
            </Sidebar>
            <Sidebar.Pusher>
              <Segment basic>
                <Header as='h3'>Application Content</Header>
                
              </Segment>
            </Sidebar.Pusher>
          </Sidebar.Pushable>
        </div>
      </div>

    );
  }
}

export default FilterBtn;