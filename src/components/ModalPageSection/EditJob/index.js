import React from "react";

import { Button, Container, Modal } from "semantic-ui-react";
import { Form } from "formsy-semantic-ui-react";

import ModalBanner from "../../ModalBanner";

import InfoSection from "../../Sections/InfoSection";
import JobDetails from "../../PostJobPage/JobDetails";
import EligibilityDetails from "../../PostJobPage/EligibilityDetails";
import OtherRequirement from "../../PostJobPage/OtherRequirement";

import fetchJobFromUrl from "../../../api/jobs/fetchJobFromUrl";
import updateJobOnServer from "../../../api/jobs/updateJob";
import getAllPostedJobs from "../../../api/jobs/getAllPostedJobs";
import getAllCompanies from "../../../api/company/getCompByUserId";

// import getAllCompanies from '../../api/company/getCompByUserId';
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import ModalFooterBtn from "../../Buttons/ModalFooterBtn";
import { geocodeByAddress, getLatLng } from "react-places-autocomplete";
import aunty from "../../../assets/banners/aunty.png";
import isUrl from "is-url";

import JobConstants from "../../../constants/storage/jobs";
import PageLoader from "../../PageLoader";
import locParser from "../../../utils/env/locParser";

import DownArrowCollaps from "../../utils/DownArrowCollaps";

import { withRouter } from "react-router-dom";
import CompanyOption from "../../PostJobPage/JobDetails/CompanyOption";
import getJobById from "../../../api/jobs/getJobById";

import CreateCompanyPage from "../../CreateCompanyPage";
import EditCompanyPage from "../EditCompany";

import { USER } from "../../../constants/api";
import ReactGA from "react-ga";
import { PostJobPages } from "../../EventStrings/PostJobs";


import "./index.scss";
import { parse } from "date-fns";

function parseFinalSalary(salary) {
  const { minSal, maxSal } = salary;
  const { lakh: minLakh, thousand: minThousand } = minSal;
  const { lakh: maxLakh, thousand: maxThousand } = maxSal;

  return {
    min: Number(`${minLakh ? minLakh : 0}.${minThousand ? minThousand : 0}`),
    max: Number(`${maxLakh ? maxLakh : 0}.${maxThousand ? maxThousand : 0}`)
  };
}

function getFinalSalary(salary) {
  let minSal = {
    lakh: 0,
    thousand: 0
  };

  let maxSal = {
    lakh: 0,
    thousand: 0
  };

  const { min, max } = salary;
  if (min) {
    const [minlakh, minthousand] = String(min).split(".");
    minSal = {
      lakh: Number(minlakh) ? Number(minlakh) : 0,
      thousand: minthousand
    };
  }

  if (max) {
    const [maxlakh, maxthousand] = String(max).split(".");
    maxSal = {
      lakh: Number(maxlakh) ? Number(maxlakh) : 0,
      thousand: maxthousand
    };
  }

  return {
    min: min,
    max: max,
    minSal: minSal,
    maxSal: maxSal,
    currency: salary.currency
  };
}

// const infoicon = <IcInfoIcon pathcolor="#c8c8c8" />;

function getProcessedSkills(values, skills, type, typeValues) {
  const oldSkills = skills;

  // if there is a removal of skills
  // process and filter them
  if (values.length < typeValues.length) {
    const [removedVal, ...restVals] = typeValues.filter(val => {
      if (values.indexOf(val) === -1) {
        return true;
      } else {
        return false;
      }
    });

    const finalSkills = oldSkills.filter(val => {
      if (removedVal !== val.name) {
        return true;
      }
    });

    return finalSkills;
  }

  // process oldSkills
  const processedOldSkills = oldSkills.map(val => {
    return val.name;
  });

  const finalValues = values.filter(val => {
    if (processedOldSkills.indexOf(val) === -1) {
      return true;
    } else {
      return false;
    }
  });

  // process new values
  const processedValues = finalValues.map(val => {
    return {
      exp: 0,
      name: val,
      reqType: type
    };
  });

  return oldSkills.concat(processedValues);
}

function getProcessedValues(values, otherVal) {
  const finalValues = values.filter(val => {
    if (otherVal.indexOf(val) === -1) {
      return true;
    } else {
      return false;
    }
  });

  return finalValues;
}

function getValuesFromSkills(skills, mandOptions, optionalOptions) {
  const skillOptions = {
    mandatory: {
      values: [],
      options: []
    },
    optional: {
      values: [],
      options: []
    }
  };

  skills.map(val => {
    if (val.reqType === "optional") {
      skillOptions.optional.values.push(val.name);
      skillOptions.optional.options.push({
        text: val.name,
        value: val.name
      });
    } else {
      skillOptions.mandatory.values.push(val.name);
      skillOptions.mandatory.options.push({
        text: val.name,
        value: val.name
      });
    }
  });

  // mandatory options
  skillOptions.mandatory.options = skillOptions.mandatory.options.concat(
    mandOptions
  );

  // optional options
  skillOptions.optional.options = skillOptions.optional.options.concat(
    optionalOptions
  );

  return skillOptions;
}

function getParsedSkills({ skills }) {
  if (!skills || skills.length === 0) {
    return {
      skills: [],
      skillOptions: [],
      currentValues: []
    };
  }

  const pSkills = skills.map(val => {
    return {
      exp: 0,
      name: val,
      reqType: "good_to_have"
    };
  });

  const options = skills.map(val => {
    return {
      text: val,
      value: val
    };
  });

  return {
    skills: pSkills,
    skillOptions: options,
    currentValues: skills
  };
}

function postedJobParsedSkills(skills) {
  const skillOptions = {
    skills: [],
    mandatory: {
      options: [],
      value: []
    },
    optional: {
      options: [],
      value: []
    }
  };

  if (!skills || skills.length === 0) {
    return skillOptions;
  }

  skillOptions.skills = skills;

  skills.map(val => {
    if (val.reqType === "optional") {
      skillOptions.optional.options.push({
        text: val.name,
        value: val.name
      });

      skillOptions.optional.value.push(val.name);
    } else {
      skillOptions.mandatory.options.push({
        text: val.name,
        value: val.name
      });

      skillOptions.mandatory.value.push(val.name);
    }
  });

  return skillOptions;
}

function checkPostedJobVal(key) {
  return key ? key : "";
}

class EditJob extends React.Component {
  state = {
    title: "",
    com: {
      id: "",
      name: "",
      imgExt: "",
      optionsItem: [],
      showModal: true
    },
    desc: "",
    benefits: "",
    respAndDuty: "",
    imgBanner: "",
    edus: {
      level: ""
    },
    exp: {
      min: "",
      max: ""
    },
    jobType: "",
    remote: false,
    loc: {
      adrs: "",
      city: "",
      country: "",
      lat: "",
      lon: "",
      street: "",
      zipcode: ""
    },
    role: "",
    salary: {
      minSal: {
        lakh: "",
        thousand: ""
      },
      maxSal: {
        lakh: "",
        thousand: ""
      },
      currency: "",
      max: "",
      min: ""
    },
    other: {
      dec: "",
      disFriendlyTags: [],
      disfriendly: false
    },

    workDays: {
      days: "",
      mandatory: false,
      shift: "",
      time: ""
    },

    joiningProb: {
      joiningProbability: 0,
      mandatory: false
    },

    skills: [],
    skillFieldOptionsMandatory: [],
    skillFieldCurrentValuesMandatory: [],
    skillFieldOptionsOptional: [],
    skillFieldCurrentValuesOptional: [],
    skillShowEditBtn: true,
    isLinkLoading: false,
    parseUrl: "",
    bannerImage: aunty,
    isFormLoading: false,
    create_company: false,
    edit_company: false
  };

  constructor(props) {
    super(props);
    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.onDraftSubmit = this.onDraftSubmit.bind(this);
    this.fetchedJob = {};
  }

  onCreateCompanyOpen = e => {
    this.setState({
      create_company: true
    });
  };

  onEditCompanyOpen = e => {
    this.setState({
      edit_company: true
    });
  };

  onCreateCompanyClose = e => {
    this.setState({
      create_company: false
    });
  };

  onEditCompanyClose = e => {
    this.setState({
      edit_company: false
    });
  };

  onShiftChange = (e, { value, name }) => {
    this.setState({
      workDays: {
        ...this.state.workDays,
        [name]: parse(value).getTime()
      }
    });
  };

  onjoiningProbChange = (e, { value, name }) => {
    this.setState({
      joiningProb: {
        ...this.state.joiningProb,
        [name]: value
      }
    });
  };

  onMinSalChange = (e, { value, name }) => {
    this.setState({
      salary: {
        ...this.state.salary,
        minSal: {
          ...this.state.salary.minSal,
          [name]: value
        }
      }
    });
  };

  onMaxSalChange = (e, { value, name }) => {
    this.setState({
      salary: {
        ...this.state.salary,
        maxSal: {
          ...this.state.salary.maxSal,
          [name]: value
        }
      }
    });
  };

  async setCompanyDropdown(id) {
    try {
      const res = await getAllCompanies();
      const data = await res.json();

      // data from server
      const hasCompanies = data.content.length;

      if (hasCompanies) {
        const comdata = data.content.map(val => {
          return {
            key: val._id,
            // text: val.clientName ? val.name + ", " + val.clientName : val.name,
            text: (
              <CompanyOption
                compName={val.name}
                location={val.headquarter ? val.headquarter.location : ""}
                imgUrl={val.imgExt ? `${val._id}/image.${val.imgExt}` : null}
                tag={val.clientName ? val.clientName : ""}
              />
            ),
            imgExt: val.imgExt,
            value: val._id,
            compName: val.name,
            tags: val.clientName
          };
        });

        const companyList = data.content.filter(val => val._id === id);
        const company = companyList[0];

        this.setState({
          com: {
            ...this.state.com,
            id: company._id,
            name: company.name,
            optionsItem: comdata,
            showModal: true
          },
          selectedCompanyData: company
        });

        // store the companies data;
        window.localStorage.setItem(
          JobConstants.JOB_COMPANY_INFO,
          JSON.stringify(data.content)
        );
      }
    } catch (error) {
      console.error("Unable to Fetch Companies");
    }
  }

  async componentDidMount() {
    const { match } = this.props;
    // console.log("check url",  match.url);
    ReactGA.pageview(`${match.url}`);

    let selectedJob;
    const { id } = this.props.match.params;

    try {
      const datares = await getJobById(id);
      if (datares.status === 200) {
        selectedJob = await datares.json();
        this.fetchedJob = selectedJob;
        if (this.props.onEditDataLoaded) {
          this.props.onEditDataLoaded(selectedJob);
        }
      }
    } catch (error) {
      console.log(error);
    }

    if (!selectedJob) {
      return;
    }

    // setup Dropdown Properly
    this.setCompanyDropdown(selectedJob.com ? selectedJob.com.id : "");
    const parsedSkills = postedJobParsedSkills(selectedJob.skills);

    this.setState({
      ...this.state,
      title: checkPostedJobVal(selectedJob.title),
      // com: {
      //   id: checkPostedJobVal(selectedJob.com ? selectedJob.com.id : ""),
      //   name: checkPostedJobVal(selectedJob.com ? selectedJob.com.name : ""),
      //   optionsItem: [
      //     {
      //       ...this.state.com.optionsItem.filter(val => {
      //         val.key !== selectedJob.com.id;
      //       })
      //     },
      //     {
      //       text: checkPostedJobVal(
      //         selectedJob.com ? selectedJob.com.name : ""
      //       ),
      //       key: checkPostedJobVal(selectedJob.com ? selectedJob.com.id : ""),
      //       value: checkPostedJobVal(selectedJob.com ? selectedJob.com.id : "")
      //     }
      //   ],
      //   showModal: true
      // },
      desc: checkPostedJobVal(selectedJob.desc),
      benefits: checkPostedJobVal(selectedJob.benefits),
      respAndDuty: checkPostedJobVal(selectedJob.respAndDuty),
      imgBanner: checkPostedJobVal(selectedJob.imgBanner),
      edus: {
        ...this.state.edus,
        level: checkPostedJobVal(selectedJob.edus[0].level)
      },
      exp: {
        min: checkPostedJobVal(selectedJob.exp.min),
        max: checkPostedJobVal(selectedJob.exp.max)
      },
      jobType: checkPostedJobVal(selectedJob.jobType),
      remote: checkPostedJobVal(selectedJob.remote) ? true : false,
      loc: {
        ...this.state.loc,
        adrs: checkPostedJobVal(selectedJob.loc.adrs),
        city: checkPostedJobVal(selectedJob.loc.city),
        lat: checkPostedJobVal(selectedJob.loc.lat),
        lon: checkPostedJobVal(selectedJob.loc.lon)
      },
      role: checkPostedJobVal(selectedJob.role),
      salary: getFinalSalary(selectedJob.salary),
      other: {
        ...this.state.other,
        dec: checkPostedJobVal(selectedJob.other.dec),
        disFriendlyTags: checkPostedJobVal(selectedJob.other.disFriendlyTags)
      },

      workDays: {
        ...this.state.workDays,
        days: checkPostedJobVal(selectedJob.workDays.days),
        mandatory: false,
        shift: checkPostedJobVal(selectedJob.workDays.shift),
        time: checkPostedJobVal(selectedJob.workDays.time)
      },

      joiningProb: {
        joiningProbability: checkPostedJobVal(
          selectedJob.joiningProb.joiningProbability
        ),
        mandatory: false
      },
      hasAdditionalDetails: checkPostedJobVal(selectedJob.other.dec)
        ? true
        : "",
      skills: parsedSkills.skills,
      skillFieldCurrentValuesMandatory: parsedSkills.mandatory.value,
      skillFieldCurrentValuesOptional: parsedSkills.optional.value,
      skillFieldOptionsMandatory: parsedSkills.mandatory.options,
      skillFieldOptionsOptional: parsedSkills.optional.options,
      skillShowEditBtn: true
    });
  }

  parseJobdataToState = data => {
    // MORPH IT!
    data = data.data;

    const { skills, skillOptions, currentValues } = getParsedSkills(data);

    this.setState({
      desc: data.desc || "",
      exp: {
        min: data.exp ? data.exp.min : 0,
        max: data.exp ? data.exp.max : 0
      },
      salary: {
        currency: "INR",
        max: 0,
        min: 0
      },
      title: data.title || "",
      role: data.role || "",
      skills: skills || [],
      skillFieldOptions: skillOptions || [],
      skillFieldCurrentValues: currentValues || [],
      skillShowEditBtn: currentValues.length ? true : false,
      isLinkLoading: false
    });
  };

  async sendRequestUri(url) {
    return await fetchJobFromUrl(url);
  }

  onLinkInputChange = (e, { value }) => {
    this.setState({
      parseUrl: value
    });
  };

  async onLinkChange(ev) {
    if (!isUrl(this.state.parseUrl) || !this.state.parseUrl) {
      toast("Sorry, Entered Text is not a URL", {
        autoClose: 1500,
        pauseOnHover: true
      });

      return;
    }

    toast("Parsing URL", {
      autoClose: 5000,
      pauseOnHover: true
    });

    this.setState({
      isLinkLoading: true
    });

    try {
      const jobData = await this.sendRequestUri(this.state.parseUrl);
      console.log(jobData);
      this.parseJobdataToState(jobData);
    } catch (error) {
      console.log(error);
      toast("Unable to parse url, please manually fill the post");
    }
  }

  onLocSelect = address => {
    geocodeByAddress(address)
      .then(results => {
        const addrDetails = locParser(results[0].address_components, address);

        if (typeof addrDetails === "string") {
          this.setState({
            loc: {
              ...this.state.loc,
              city: addrDetails,
              adrs: address
            }
          });
        } else {
          this.setState({
            loc: {
              ...this.state.loc,
              city: addrDetails.primary,
              country: addrDetails.country,
              adrs: address
            }
          });
        }

        return getLatLng(results[0]);
      })
      .then(latLng => {
        this.setState({
          loc: {
            ...this.state.loc,
            lat: latLng.lat,
            lon: latLng.lng
          }
        });
      })
      .catch(error => console.error("Error", error));
  };

  onLocChange = address => {
    this.setState({
      loc: {
        ...this.state.loc,
        adrs: address
      }
    });
  };

  onInputChange = (ev, { value, name }) => {
    if (name === "hasAdditionalDetails") {
      this.setState({
        [name]: value,
        other: {
          ...this.state.other,
          dec: ""
        }
      });

      return;
    }

    this.setState({
      [name]: value
    });
  };

  onQuillChange = html => {
    this.setState({
      desc: html
    });
  };

  onDescChange = html => {
    this.setState({
      desc: html
    });
  };

  onResChange = html => {
    this.setState({
      respAndDuty: html
    });
  };

  onBenefitsChange = html => {
    this.setState({
      benefits: html
    });
  };

  onRemoteChange = e => {
    this.setState({
      remote: !this.state.remote
    });
  };

  onSKillAdd = (e, { value, type }) => {
    switch (type) {
      case "mandatory":
        this.setState({
          skillFieldOptionsMandatory: [
            { text: value, value },
            ...this.state.skillFieldOptionsMandatory
          ]
        });
        break;

      case "optional":
        this.setState({
          skillFieldOptionsOptional: [
            { text: value, value },
            ...this.state.skillFieldOptionsOptional
          ]
        });
        break;

      default:
        break;
    }
  };

  onSkillChange = (e, { value, type }) => {
    switch (type) {
      case "mandatory":
        this.setState({
          skillFieldCurrentValuesMandatory: getProcessedValues(
            value,
            this.state.skillFieldCurrentValuesOptional
          ),
          skills: getProcessedSkills(
            value,
            this.state.skills,
            type,
            this.state.skillFieldCurrentValuesMandatory
          )
        });
        break;

      case "optional":
        this.setState({
          skillFieldCurrentValuesOptional: getProcessedValues(
            value,
            this.state.skillFieldCurrentValuesMandatory
          ),
          skills: getProcessedSkills(
            value,
            this.state.skills,
            type,
            this.state.skillFieldCurrentValuesOptional
          )
        });
        break;

      default:
        break;
    }

    console.log(type);
  };

  /**
   * @TODO: Change for the new System
   */
  onSkillEditModalInputChange = (e, { value, idx, name }) => {
    const newSkills = [...this.state.skills];
    newSkills[idx][name] = value;

    if (name === "reqType") {
      const skillsFormatted = getValuesFromSkills(
        newSkills,
        this.state.skillFieldOptionsMandatory,
        this.state.skillFieldOptionsOptional
      );
      this.setState({
        skillFieldCurrentValuesMandatory: skillsFormatted.mandatory.values,
        skillFieldCurrentValuesOptional: skillsFormatted.optional.values,
        skillFieldOptionsMandatory: skillsFormatted.mandatory.options,
        skillFieldOptionsOptional: skillsFormatted.optional.options,
        skills: newSkills
      });
    } else {
      this.setState({
        skills: newSkills
      });
    }
  };

  onSkillModalRemoveCurrentValuesUpdate(skill) {
    if (!skill) {
      return;
    }

    const reqType = skill.reqType;
    let currentValuesOptional = this.state.skillFieldCurrentValuesOptional,
      currentValuesMandatory = this.state.skillFieldCurrentValuesMandatory;

    console.log("Current Val Mandatory Pre Change", currentValuesMandatory);
    console.log("Current Val Option Pre Change", currentValuesOptional);

    if (reqType === "optional") {
      currentValuesOptional = currentValuesOptional.filter(
        val => val != skill.name
      );
    } else {
      currentValuesMandatory = currentValuesMandatory.filter(
        val => val != skill.name
      );
    }

    return {
      currentValuesOptional: currentValuesOptional,
      currentValuesMandatory: currentValuesMandatory
    };
  }

  onSkillEditModalRemove = (e, { idx }) => {
    this.setState((prevState, props) => {
      const newSkills = [...prevState.skills];
      const removedSkill = newSkills.splice(idx, 1);

      console.log("Removed Skill is", removedSkill);

      const {
        currentValuesMandatory,
        currentValuesOptional
      } = this.onSkillModalRemoveCurrentValuesUpdate(removedSkill[0]);

      console.log("Current Val Mandatory", currentValuesMandatory);
      console.log("Current Val Option", currentValuesOptional);

      return {
        ...prevState,
        skills: newSkills,
        skillFieldCurrentValuesMandatory: currentValuesMandatory,
        skillFieldCurrentValuesOptional: currentValuesOptional
      };
    });
  };

  onSkillModalSave = e => {
    this.forceUpdate();
  };

  onExpChange = (e, { value, name }) => {
    this.setState({
      exp: {
        ...this.state.exp,
        [name]: value
      }
    });
  };

  onSalaryChange = (e, { value, name }) => {
    this.setState({
      salary: {
        ...this.state.salary,
        [name]: value
      }
    });
  };

  onEduChange = (e, { value, name }) => {
    this.setState({
      edus: {
        [name]: value
      }
    });
  };

  onOtherChange = (e, { value, name }) => {
    this.setState({
      other: {
        ...this.state.other,
        [name]: value
      }
    });
  };

  onCompanyCreate = (data, isEditModal) => {
    if (isEditModal) {
      const newOptions = this.state.com.optionsItem.filter(
        val => val.key !== data._id
      );

      this.setState({
        com: {
          ...this.state.com,
          optionsItem: [
            ...newOptions,
            {
              key: data._id,
              text: (
                <CompanyOption
                  compName={data.name}
                  location={data.headquarter ? data.headquarter.location : ""}
                  imgUrl={
                    data.imgExt ? `${data._id}/image.${data.imgExt}` : null
                  }
                  tag={data.clientName ? data.clientName : ""}
                />
              ),
              // text: data.clientName
              //   ? data.name + ", " + data.clientName
              //   : data.name,
              value: data._id
            }
          ],
          id: data._id,
          name: data.name,
          imgExt: data.imgExt,
          tags: data.clientName,
          showModal: true
        },
        selectedCompanyData: data
      });

      return;
    }

    this.setState({
      com: {
        ...this.state.com,
        optionsItem: [
          ...this.state.com.optionsItem,
          {
            key: data._id,
            // text: data.clientName
            //   ? data.name + ", " + data.clientName
            //   : data.name,
            text: (
              <CompanyOption
                compName={data.name}
                location={data.headquarter ? data.headquarter.location : ""}
                imgUrl={data.imgExt ? `${data._id}/image.${data.imgExt}` : null}
                tag={data.clientName ? data.clientName : ""}
              />
            ),
            value: data._id
          }
        ],
        id: data._id,
        name: data.name,
        imgExt: data.imgExt,
        showModal: false
      }
    });

    const compdata = window.localStorage.getItem(JobConstants.JOB_COMPANY_INFO);
    if (!compdata || compdata === "undefined" || compdata === "null") {
      window.localStorage.setItem(
        JobConstants.JOB_COMPANY_INFO,
        JSON.stringify([...data])
      );
    } else {
      let compjson = JSON.parse(compdata);
      compjson = [...compjson, data];
      window.localStorage.setItem(
        JobConstants.JOB_COMPANY_INFO,
        JSON.stringify([...compjson, data])
      );
    }
  };

  /* Enable Form Loader */
  enableFormLoader = loaderText => {
    this.setState({
      isFormLoading: true,
      formLoadingText: loaderText
    });
  };

  /* Disable Form Loader */
  disableFormLoader = () => {
    this.setState({
      isFormLoading: false
    });
  };

  getSelectedCompanyName = key => {
    let compdata = null;
    this.state.com.optionsItem.map(val => {
      if (val.key === key) {
        // name = val.text;
        compdata = {
          name: val.compName,
          imgExt: val.imgExt
        };
      }
    });

    return compdata;
  };

  getSelectedCompanyJson = id => {
    const compdata = window.localStorage.getItem(JobConstants.JOB_COMPANY_INFO);
    const compjson = JSON.parse(compdata);

    if (!compjson) {
      return {};
    }

    let data;
    compjson.map(val => {
      if (val._id === id) {
        data = val;
      }
    });

    if (data) {
      return data;
    }

    return null;
  };

  onSelectCompanyChange = (ev, { value }) => {
    const comp = this.getSelectedCompanyName(value);
    const compJson = this.getSelectedCompanyJson(value);

    this.setState({
      com: {
        ...this.state.com,
        id: value,
        name: comp.name,
        tags: comp.clientName,
        imgExt: comp.imgExt ? comp.imgExt : ""
      },
      selectedCompanyData: compJson
    });
  };

  onPreviewClick = () => {
    window.localStorage.setItem(
      JobConstants.JOB_PREVIEW_STATE,
      JSON.stringify(this.state)
    );
    this.props.history.push("/job/preview");
  };

  // componentWillUnmount() {
  //   if (this.popHistory) {
  //     this.popHistory();
  //   }
  // }

  onFileChange = e => {
    const file = e.target.files[0];
    const extn = file.name.split(".").pop();
    const reader = new FileReader();

    reader.onloadend = () => {
      this.setState({
        imgBanner: "jpg",
        bannerImage: reader.result
      });
    };

    reader.readAsDataURL(file);
  };

  onDisabilityOptionschange = value => {
    this.setState(state => {
      if (state.other.disFriendlyTags.indexOf(value) === -1) {
        return {
          ...state,
          other: {
            ...state.other,
            disFriendlyTags: [...state.other.disFriendlyTags, value]
          }
        };
      }

      return {
        ...state,
        other: {
          ...state.other,
          disFriendlyTags: state.other.disFriendlyTags.filter(
            val => val !== value
          )
        }
      };
    });
  };

  onCloseClick = e => {
    this.props.history.push(`/job/view/${this.fetchedJob._id}`);
  };

  async onDraftSubmit(event) {
    const {
      skillShowEditBtn,
      isLinkLoading,
      parseUrl,
      bannerImage,
      skillFieldOptionsMandatory,
      skillFieldCurrentValuesMandatory,
      skillFieldOptionsOptional,
      skillFieldCurrentValuesOptional,
      isFormLoading,
      ...restState
    } = this.state;

    if (!this.state.desc || this.state.desc.length === 11) {
      toast("Job Description not found");
      return;
    }

    if (this.state.desc.length > 3000 && this.state.desc) {
      toast("Job Description Should be less than 3000 words");
      return;
    }

    if (this.state.respAndDuty.length > 2000 && this.state.respAndDuty) {
      toast("Responsibilities and Duties Should be less than 2000 words");
      return;
    }

    if (this.state.benefits.length > 2000 && this.state.benefits) {
      toast("Benefits Should be less than 2000 words");
      return;
    }

    if (!this.state.skills || !this.state.skills.length) {
      toast("Please add skills, you want to hire for");
      return;
    }

    this.setState({
      isFormLoading: true
    });

    try {
      const res = await updateJobOnServer(
        Object.assign({}, this.fetchedJob, restState, {
          salary: parseFinalSalary(this.state.salary),
          edus: [{ ...this.fetchedJob.edus[0], level: restState.edus.level }]
        })
      );
      const data = await res.json();
      window.localStorage.removeItem(JobConstants.JOB_PREVIEW_STATE);
      this.props.history.push(`/job/view/${data._id}`);
    } catch (error) {
      console.error(error);
    }
  }

  async onFormSubmit(event) {
    const {
      skillShowEditBtn,
      isLinkLoading,
      parseUrl,
      bannerImage,
      skillFieldOptionsMandatory,
      skillFieldCurrentValuesMandatory,
      skillFieldOptionsOptional,
      skillFieldCurrentValuesOptional,
      isFormLoading,
      ...restState
    } = this.state;

    if (!this.state.desc || this.state.desc.length === 11) {
      toast("Job Description not found");
      return;
    }

    if (this.state.desc.length > 3000 && this.state.desc) {
      toast("Job Description Should be less than 3000 words");
      return;
    }

    if (this.state.respAndDuty.length > 2000 && this.state.respAndDuty) {
      toast("Responsibilities and Duties Should be less than 2000 words");
      return;
    }

    if (this.state.benefits.length > 2000 && this.state.benefits) {
      toast("Benefits Should be less than 2000 words");
      return;
    }

    if (!this.state.skills || !this.state.skills.length) {
      toast("Please add skills, you want to hire for");
      return;
    }

    this.setState({
      isFormLoading: true
    });

    try {
      const res = await updateJobOnServer(
        Object.assign({}, this.fetchedJob, restState, {
          salary: parseFinalSalary(this.state.salary),
          edus: [{ ...this.fetchedJob.edus[0], level: restState.edus.level }]
        })
      );
      const data = await res.json();
      window.localStorage.removeItem(JobConstants.JOB_PREVIEW_STATE);
      this.props.history.push(`/job/edit/screening/${data._id}/edit`);
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const { isShowModalBanner, isShowModalFooter } = this.props;
    const UserId = window.localStorage.getItem(USER.UID);
    return (
      <div className="card_panel edit_job">
        {!isShowModalBanner ? (
          <ModalBanner headerText="Edit Job">
            {/* <Button compact className="save_btn">
            Save
          </Button> */}
          </ModalBanner>
        ) : null}

        <div className="form_fields">
          <Form onValidSubmit={this.onFormSubmit}>
            <Container>
              <div className="panel_cardBody">
                <InfoSection
                  headerSize="large"
                  color="blue"
                  headerText="Job Description"
                  rightIcon={<DownArrowCollaps />}
                  collapsible
                  isActive={true}>
                  <JobDetails
                    data={this.state}
                    onInputChange={this.onInputChange}
                    onLocChange={this.onLocChange}
                    onSalaryMinChange={this.onMinSalChange}
                    onSalaryMaxChange={this.onMaxSalChange}
                    onLocSelect={this.onLocSelect}
                    onQuillChange={this.onQuillChange}
                    onResChange={this.onResChange}
                    onBenefitsChange={this.onBenefitsChange}
                    onDescChange={this.onDescChange}
                    onSalaryChange={this.onSalaryChange}
                    onCompanyCreate={this.onCompanyCreate}
                    onCompanyChange={this.onSelectCompanyChange}
                    onRemoteChange={this.onRemoteChange}
                    onModalCreateCompany={this.onCreateCompanyOpen}
                    onModalEditCompany={this.onEditCompanyOpen}
                  />

                  <EligibilityDetails
                    data={this.state}
                    onSkillChange={this.onSkillChange}
                    onSKillAdd={this.onSKillAdd}
                    onSkillModalChange={this.onSkillEditModalInputChange}
                    onSkillModalRemove={this.onSkillEditModalRemove}
                    onSkillModalSave={this.onSkillModalSave}
                    onExpChange={this.onExpChange}
                    onInputChange={this.onInputChange}
                    onEduChange={this.onEduChange}
                    onOtherChange={this.onOtherChange}
                  />
                </InfoSection>
              </div>
            </Container>

            <OtherRequirement
              data={this.state.other}
              dataOther={this.state}
              onjoiningProbChange={this.onjoiningProbChange}
              onShiftChange={this.onShiftChange}
              onOtherChange={this.onOtherChange}
              onDisabilityOptionschange={this.onDisabilityOptionschange}
            />
            {!isShowModalFooter ? (
              <Container>
                <ModalFooterBtn
                  noExtra
                  onDraftclick={this.onDraftSubmit}
                  onPreviewClick={this.onPreviewClick}
                  onCancelClick={this.onCloseClick}
                  actioaBtnText={"Save and Next"}
                  onClick={() => {
                    ReactGA.event({
                      category: PostJobPages.EditPostJobsEvent.category,
                      action: PostJobPages.EditPostJobsEvent.action,
                      value: UserId,
                    });
                  }}
                  editJob
                />
              </Container>
            ) : null}
          </Form>
        </div>

        <PageLoader
          active={this.state.isFormLoading}
          loaderText={"Loading Job ..."}
        />

        {/* Edit Company Modal */}
        <Modal
          className="modal_form"
          open={this.state.edit_company}
          onClose={this.onEditCompanyClose}
          closeIcon
          closeOnDimmerClick={false}>
          <EditCompanyPage
            data={this.state.selectedCompanyData}
            onCompanyCreate={this.onCompanyCreate}
            onModalClose={this.onEditCompanyClose}
            onEnableFormLoader={this.enableFormLoader}
            onDisableFormLoader={this.disableFormLoader}
          />
        </Modal>

        {/* Create Company Modal */}
        <Modal
          className="modal_form"
          open={this.state.create_company}
          onClose={this.onCreateCompanyClose}
          closeIcon
          closeOnDimmerClick={false}>
          <CreateCompanyPage
            onCompanyCreate={this.onCompanyCreate}
            onModalClose={this.onCreateCompanyClose}
            onEnableFormLoader={this.enableFormLoader}
            onDisableFormLoader={this.disableFormLoader}
          />
        </Modal>
      </div>
    );
  }
}

export default withRouter(EditJob);
