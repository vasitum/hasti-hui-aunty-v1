import React from "react";
import { Grid, Container, Image } from "semantic-ui-react";
import { withRouter } from "react-router-dom";
import JobViews from "./../PreLoginJob/jobViews";
import JobRightSide from "./../PreLoginJob/jobRightSide";
import public_job_banner from "../../../assets/banners/job_cover_img.jpg";
import BestBanner from "../../../assets/img/searchFilter/bestBanner.png";
import getJobById from "../../../api/jobs/getJobById";
import getCompany from "../../../api/company/getCompById";
import PageBanner from "../../Banners/PageBanner";

import jobConstants from "../../../constants/storage/jobs";

import { toast } from "react-toastify";

import { USER } from "../../../constants/api";

import { uploadImage, uploadResume } from "../../../utils/aws";

import urlToBlob from "../../../utils/env/uriToBlob";
import createJobServer from "../../../api/jobs/createJob";

import PageLoader from "../../PageLoader";
import ReactGA from "react-ga";
import PreviewHeaderNotification from "../PreviewHeaderNotification";

const Loader = props => (
  <div className="m_showbox">
    <div className="m_loader">
      <svg className="m_circular" viewBox="25 25 50 50">
        <circle
          className="m_path"
          cx="50"
          cy="50"
          r="20"
          fill="none"
          stroke-width="2"
          stroke-miterlimit="10"
        />
      </svg>
    </div>
  </div>
);

class PublicJob extends React.Component {
  state = {
    isLoading: false,
    jobdata: {},
    compData: null,
    isFormLoading: false
  };

  constructor(props) {
    super(props);
    this.onSaveClick = this.onSaveClick.bind(this);
  }

  componentDidMount() {
    const { match } = this.props;
    // console.log("check url",  match.url);
    ReactGA.pageview(`${match.url}`);
    
    try {
      const jobPreviewString = window.localStorage.getItem(
        jobConstants.JOB_PREVIEW_STATE
      );
      const jobJson = JSON.parse(jobPreviewString);

      // console.log("JOB JSON IS", jobJson);
      if (!jobJson) {
        return;
      }

      this.setState(state => {
        return {
          ...state,
          jobdata: jobJson,
          compData: jobJson.selectedCompanyData
        };
      });
    } catch (error) {
      console.error(error);
    }
  }

  async onSaveClick(ev) {
    const { history } = this.props;

    if (!this.state.jobdata.desc || this.state.jobdata.desc.length > 3000) {
      toast("Job Description Should be less than 3000 words");
      return;
    }

    if (
      this.state.jobdata.respAndDuty.length > 2000 &&
      this.state.jobdata.respAndDuty
    ) {
      toast("Responsibilities and Duties Should be less than 2000 words");
      return;
    }

    if (
      this.state.jobdata.benefits.length > 2000 &&
      this.state.jobdata.benefits
    ) {
      toast("Benefits Should be less than 2000 words");
      return;
    }

    this.setState({
      isFormLoading: true
    });

    try {
      const res = await createJobServer({
        ...this.state.jobdata,
        edus: [
          {
            level: this.state.jobdata.edus.level
          }
        ],
        userId: window.localStorage.getItem(USER.UID)
      });

      const data = await res.json();
      // console.log(data);
      if (this.state.jobdata.bannerImageFile) {
        const imgFile = urlToBlob(this.state.jobdata.bannerImage);
        const imgBannerUpload = await uploadImage(
          `banner/${data._id}/image.png`,
          imgFile
        );
      }

      // toast("Job Successfully Posted!");

      this.setState({
        isFormLoading: false
      });

      history.push(`/job/edit/screening/${data._id}`);
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    if (this.state.isLoading) {
      return (
        <Container>
          <Loader />
        </Container>
      );
    } else {
      return (
        <div className="publicJobViews">
          <PreviewHeaderNotification publicTitle="job" />
          <div className="publicJobBanner">
            <PageBanner
              size="medium"
              image={
                this.state.jobdata.bannerImage
                  ? this.state.jobdata.bannerImage
                  : this.state.jobdata
                  ? this.state.jobdata.imgBanner
                    ? `https://s3-us-west-2.amazonaws.com/img-mwf/banner/${
                        this.state.jobdata._id
                      }/image.png`
                    : public_job_banner
                  : public_job_banner
              }>
              {/* <Container>
                <BannerHeader onFileChange={this.onFileChange} />
              </Container> */}
            </PageBanner>
          </div>
          <div className="publicJobDetails">
            <Container>
              <Grid>
                <Grid.Row>
                  <Grid.Column mobile={16} tablet={16} computer={13}>
                    <JobViews
                      comdata={this.state.compData}
                      data={this.state.jobdata}
                      isPreview
                      onSaveClick={this.onSaveClick}
                    />
                  </Grid.Column>
                  {/* <Grid.Column width={5} className="tablet mobile hidden">
                    <JobRightSide compdata={this.state.compData} />
                  </Grid.Column> */}
                </Grid.Row>
              </Grid>
            </Container>
          </div>

          <PageLoader
            active={this.state.isFormLoading}
            loaderText={"Publishing Job ..."}
          />
        </div>
      );
    }
  }
}

export default withRouter(PublicJob);
