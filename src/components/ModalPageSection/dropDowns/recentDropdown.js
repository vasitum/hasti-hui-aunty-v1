import React from "react";
import { Icon, Button, Dropdown } from "semantic-ui-react";
import { connectSortBy } from "react-instantsearch/connectors";

class RecentDropDown extends React.Component {
  state = {
    value: ""
  };

  onDropdownChange = (ev, { value }) => {
    const { refine } = this.props;
    this.setState(
      {
        value
      },
      () => {
        refine(value);
      }
    );
  };

  componentDidMount() {
    this.setState({
      value: this.props.defaultRefinement
    });
  }

  render() {
    const { items, currentRefinement, ...restProps } = this.props;
    return (
      <div className="text-aling-right">
        <div className="recentDropDown" {...restProps}>
          <div style={{ marginLeft: "38px" }}>
            <span className="displayBlock text-white displayBlock fontSize-12">
              Sort by:{" "}
              <Dropdown
                inline
                pointing="top right"
                options={items}
                className="text-white fontSize-12"
                value={currentRefinement}
                onChange={this.onDropdownChange}
              />
            </span>
          </div>
          {/* <div className="floatRight text-white padding-left-10">
            <Icon name="angle down" />
          </div> */}
        </div>
      </div>
    );
  }
}

export default connectSortBy(RecentDropDown);
