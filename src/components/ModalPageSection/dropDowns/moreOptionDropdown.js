import React from "react";
import { Icon, Button, Dropdown } from "semantic-ui-react";

import { connectSortBy } from "react-instantsearch/connectors";

import MoreOptionIcon from "Assets/svg/moreOptionIcon";

const trigger = (
  <span>
    <MoreOptionIcon width="5" height="18.406" pathcolor="#797979" viewbox="0 0 5 18.406" />
  </span>
)

const options = [
  { key: 'user', text: 'Account', icon: 'user' },
  { key: 'settings', text: 'Settings', icon: 'settings' },
  { key: 'sign-out', text: 'Sign Out', icon: 'sign out' },
]

class MoreOptionDropdown extends React.Component {
  
  render() {

    return (
      
      <Dropdown trigger={trigger} options={options} pointing='top right' icon={null} />
      
    );
  }
}

export default MoreOptionDropdown;