import React from "react";
import { Header, List, Icon, Button, Card, Grid } from "semantic-ui-react";

import HeaderText from "./headerText";

import "./index.scss";

const SkillsComponent = props => {
  const { val } = props;

  return (
    <List.Item
      className="padding-bottom-20 skillsComponent"
      style={{
        display:
          !val.skillMetaData || !val.skillMetaData.length ? "none" : "block"
      }}>
      <List.Content>
        <List.Header className="padding-bottom-10">
          <HeaderText headertext="Skills" />
        </List.Header>
        <List.Description className="filterSkill">
          {/* <List bulleted horizontal className="text-night-rider">
            {(() => {
              if (!val.skills) {
                return null;
              }

              const ListKeys = val.skills.map((skill, idx) => {
                return <List.Item>{skill}</List.Item>;
              });

              return ListKeys;
            })()}
          </List> */}
          <List horizontal className="text-night-rider">
            {(() => {
              if (!val.skillMetaData) {
                return null;
              }

              const ListKeys = val.skillMetaData.map((skill, idx) => {
                return (
                  <List.Item key={skill.skillName}>
                    <Button compact>
                      {skill.exp ? (
                        <React.Fragment>
                          {skill.skillName} |<span> {skill.exp} yrs</span>
                        </React.Fragment>
                      ) : (
                        <React.Fragment>{skill.skillName}</React.Fragment>
                      )}
                    </Button>
                  </List.Item>
                );
              });

              return ListKeys;
            })()}
          </List>
        </List.Description>
      </List.Content>
    </List.Item>
  );
};

export default SkillsComponent;
