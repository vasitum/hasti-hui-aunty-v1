import React from "react";
import {
  Image,
  Header,
  Accordion,
  List,
  Icon,
  Button,
  Card,
  Grid,
  Dropdown,
  Responsive
} from "semantic-ui-react";
import SocialBtn from "../Buttons/socialBtn";
import { withRouter, Link } from "react-router-dom";

import CompanySvg from "../../../assets/svg/IcCompany";

import IcUser from "../../../assets/svg/IcUser";

import LocationLogo from "../../../assets/svg/IcLocation";
import JobLogo from "../../../assets/svg/Icjob";
import MoreOptionIcon from "../../../assets/svg/IcMoreOptionIcon";
import fromNow from "../../../utils/env/fromNow";

import P2PTracker from "../../P2PTracker";
import { USER } from "../../../constants/api";

import isLoggedIn from "../../../utils/env/isLoggedin";
import UserAccessModal from "../UserAccessModal";

const trigger = (
  <span>
    <MoreOptionIcon
      width="5"
      height="18.406"
      pathcolor="#797979"
      viewbox="0 0 5 18.406"
    />
  </span>
);

const options = [
  { key: "user", text: "Account", icon: "user" },
  { key: "settings", text: "Settings", icon: "settings" },
  { key: "sign-out", text: "Sign Out", icon: "sign out" }
];

class SearchCardHeader extends React.Component {
  render() {
    const {
      val,
      idx,
      objectID,
      onLikeClick,
      onApplyClick,
      likesArray,
      liked,
      saved,
      applied,
      apiDone
    } = this.props;

    const isLoggedInVal = isLoggedIn();

    if (!val) {
      return null;
    }

    if (val.objectID === window.localStorage.getItem(USER.UID)){
      return null
    }

    return (
      <div className="mobile__cardHeader search__cardHeader">
        <Grid>
          <Grid.Row
            // as={Link}
            // to={this.props.headerlink + val.objectID}
            className="padding-bottom-5">
            <Grid.Column
              mobile={2}
              computer={1}
              className="search-card-block-one">
              <List>
                <List.Item>
                  <List.Content>
                    <span className="companyLogo">
                      {val.imgExt ? (
                        <Image
                          verticalAlign="top"
                          style={{
                            // height: "40px",
                            // width: "40px",
                            marginTop: "5px",
                            borderRadius: "50%",
                            position: "absolute"
                          }}
                          src={`https://s3-us-west-2.amazonaws.com/img-mwf/${
                            val.objectID
                          }/image.jpg`}
                        />
                      ) : (
                        <IcUser
                          width="40"
                          height="40"
                          rectcolor="#f7f7fb"
                          pathcolor="#c8c8c8"
                          style={{ borderRadius: "50px" }}
                        />
                      )}
                    </span>
                    {/* <Image verticalAlign='top' avatar src={wipro} /> */}
                    {/* <Image
                    verticalAlign="top"
                    style={{
                      height: "32px",
                      width: "32px",
                      marginTop: "5px"
                    }}
                    src={
                      "http://www.freelogovectors.net/wp-content/uploads/2018/04/airtel-logo_freelogovectors.net_.png"
                    }
                  /> */}
                  </List.Content>
                </List.Item>
              </List>
            </Grid.Column>

            <Grid.Column
              mobile={14}
              computer={8}
              className="search-card-block-two">
              <List className="padding-left-5 searchCard_headerLeft">
                <List.Item>
                  <List.Content className="mobile__leftHeader">
                    <List.Header>
                      <Header
                        className="textHoverHeadingBlue font-w-500 padding-bottom-4 job-search-card-title"
                        as="h3">
                        <Link
                          to={this.props.headerlink + val.objectID}
                          style={{ color: "#323232" }}>
                          {val.fName} {val.lName}
                        </Link>
                      </Header>
                      <p className="padding-bottom-5 companyTitle vasitumFontOpen-500 job-search-card-sub-title">
                        {/* only Desktop */}
                        <span className="padding-right-5 mobile hidden">
                          <JobLogo
                            width="14.25"
                            height="11.5"
                            pathcolor="#0B9ED0"
                          />
                        </span>
                        {/* only Desktop end */}

                        {val.title}
                      </p>
                    </List.Header>
                    <List.Description>
                      <p className="margin-bottom-0 vasitumFontOpen-500 text-Matterhorn header_location">
                        {/* only Desktop */}
                        <span className="padding-right-7 mobile hidden">
                          <LocationLogo
                            width="9.5"
                            height="11.5"
                            pathcolor="#0bd0bb"
                          />
                        </span>
                        {/* only Desktop end */}

                        <span className="" style={{ color: "#a1a1a1" }}>
                          {val.locCity}
                          {val.locCountry ? ", " + val.locCountry : ""}
                        </span>
                        <Responsive maxWidth={1024}>
                          <span>
                            <P2PTracker
                              type={"user"}
                              cardType={"icon"}
                              reqId={val.objectID}
                              userId={window.localStorage.getItem(USER.UID)}
                            />
                          </span>
                        </Responsive>
                        {/* only Desktop */}
                        <span
                          className="text-grey padding-left-25  headerExperiance mobile hidden"
                          style={{
                            display:
                              !val.expMin && !val.expMax ? "none" : "block"
                          }}>
                          <span className="text-Matterhorn">
                            {val.expMin} - {val.expMax}
                          </span>{" "}
                          <span style={{ color: "#a1a1a1" }}>
                            yrs experience
                          </span>
                        </span>
                        {/* only Desktop end */}
                      </p>
                    </List.Description>
                  </List.Content>
                </List.Item>
              </List>
            </Grid.Column>

            <Grid.Column mobile={3} computer={7}>
              <List className="padding-left-5">
                {/* only mobile */}
                {/* <List.Item className="widescreen hidden large screen hidden computer hidden tablet hidden">
                  <List.Content className="alingCenter">
                    <List.Description>
                      <Dropdown
                        trigger={trigger}
                        options={options}
                        pointing="top right"
                        icon={null}
                      />
                    </List.Description>
                  </List.Content>
                </List.Item> */}
                {/* only mobile end */}

                {/* only Desktop */}
                <List.Item className="mobile hidden serarchCard_leftHeader">
                  <List.Content className="alingCenterRight">
                    <List.Header className="serarchCard_leftHeaderItem">
                      {apiDone ? (
                        <SocialBtn
                          objectID={objectID}
                          onLikeClick={onLikeClick}
                          liked={liked}
                          saved={saved}
                          type="job"
                        />
                      ) : null}

                      <span className="leftHeader_jobPostDay  vasitumFontOpen-500 padding-right-10 text-nobel fontSize-12">
                        Posted:{" "}
                        <span className="text-pacific-blue">
                          {fromNow(val.datePosted)}
                        </span>{" "}
                      </span>
                      <span className="serarchCardIcon_circle">
                        <Icon name="angle down" className="rotetIcon" />
                      </span>
                    </List.Header>
                    <List.Description className="padding-top-10 alingCenterRight">
                      {/* <Button
                        style={{
                          marginRight: "37px"
                        }}
                        id={val.objectID}
                        title={val.title}
                        size="mini"
                        className="actionBtn"
                        onClick={onApplyClick}>
                        {" "}
                        Apply <Icon name="angle right" />
                      </Button> */}

                      {!isLoggedInVal ? (
                        applied ? (
                          <Button
                            style={{
                              marginRight: "37px"
                            }}
                            id={val.objectID}
                            title={val.title}
                            size="mini"
                            className="actionBtn">
                            {" "}
                            Applied <Icon name="angle right" />
                          </Button>
                        ) : (
                          <Button
                            style={{
                              marginRight: "37px"
                            }}
                            id={val.objectID}
                            title={val.title}
                            size="mini"
                            className="actionBtn"
                            onClick={onApplyClick}>
                            {" "}
                            Apply <Icon name="angle right" />
                          </Button>
                        )
                      ) : (
                        <UserAccessModal
                          modalTrigger={
                            <Button
                              style={{
                                marginRight: "37px"
                              }}
                              size="mini"
                              className="actionBtn"
                              onClick={onApplyClick}>
                              {" "}
                              Apply <Icon name="angle right" />
                            </Button>
                          }
                          isAction
                          actionText="apply to"
                        />
                      )}
                    </List.Description>
                  </List.Content>
                </List.Item>
                {/* only Desktop end */}
              </List>
            </Grid.Column>
          </Grid.Row>

          {/* only mobile */}
          <Grid.Row className=" padding-top-0 widescreen hidden large screen hidden computer hidden tablet hidden">
            <Grid.Column mobile={2} className={this.props.classname} />
            <Grid.Column mobile={11} className={this.props.classname}>
              <p className="vasitumFontOpen-500 padding-left-5 text-Matterhorn">
                <span className=" mobile__leftHeader">
                  {val.expMin} - {val.expMax}
                </span>
                <span style={{ color: "#a1a1a1" }}>yrs exp</span>
              </p>
            </Grid.Column>
            <Grid.Column mobile={3} className={this.props.classname}>
              <p className="mobile__dataPost">Today</p>
            </Grid.Column>
          </Grid.Row>
          {/* only mobile end*/}
        </Grid>
      </div>
    );
  }
}

export default SearchCardHeader;
