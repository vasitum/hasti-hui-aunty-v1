import React from "react";
import { Header } from "semantic-ui-react";
const HeaderText = props => {
  return (
    <Header
      className="text-pacific-blue font-w-500"
      as="h3"
      style={{ fontSize: "18px" }}>
      {props.headertext}
    </Header>
  );
};

export default HeaderText;
