import React from "react";
import {
  Image,
  Header,
  Feed,
  Accordion,
  List,
  Icon,
  Button,
  Grid,
  Card
} from "semantic-ui-react";
import CompanySvg from "../../../assets/svg/IcCompany";
const ExperianceComponent = props => {
  const { val } = props;

  return (
    <List.Item
      className="padding-bottom-20"
      style={{
        display: !val.com ? "none" : "block"
      }}>
      <List.Content>
        <List.Header className="padding-bottom-10">
          <Header
            className="text-pacific-blue font-w-500"
            as="h3"
            style={{ fontSize: "18px" }}>
            Experience
          </Header>
        </List.Header>
        <List.Description className="filterSkill">
          <List.Content>
            <Feed>
              <Feed.Event>
                {/* <Feed.Label image={java} /> */}
                <span className="companyLogo margin-right-10">
                  <CompanySvg
                    width="40"
                    height="40"
                    rectcolor="#f7f7fb"
                    pathcolor="#c8c8c8"
                  />
                </span>
                <Feed.Content>
                  <List className="padding-top-0">
                    <List.Item>
                      <p className="vasitumFontOpen-500 margin-bottom-0 text-night-rider font-size-16">
                        {val.designation}
                      </p>
                      <p className="text-Matterhorn font-f-open-sans">
                        {/* {exp.name} */}
                        {/* {val.com} */}
                        {val.com}
                      </p>
                    </List.Item>
                  </List>
                </Feed.Content>
              </Feed.Event>
            </Feed>
          </List.Content>
        </List.Description>
      </List.Content>
    </List.Item>
  );
};

export default ExperianceComponent;
