import React from "react";
import ReactQuill from "react-quill";
import { Header, List } from "semantic-ui-react";

import InfoSectionGridHeader from "../PreProfile/InfoSectionGridHeader";

import IcEditIcon from "../../../assets/svg/IcEdit";
import QuillText from "../../CardElements/QuillText";

const editIcon = <IcEditIcon pathcolor="#c8c8c8" />;

const JobDescription = props => {
  const { jsonData, onEditIconClick, isPublicView } = props;

  return (
    <List.Item>
      <List.Content>
        <InfoSectionGridHeader
          headerText="Job Description"
          onEditIconClick={onEditIconClick}
          editIcon={!isPublicView && editIcon}>
          <QuillText readMoreLength={700} value={jsonData} readOnly />
        </InfoSectionGridHeader>
      </List.Content>
    </List.Item>
  );
};

export default JobDescription;
