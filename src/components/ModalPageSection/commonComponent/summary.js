import React from "react";
import { Header, List, Icon, Button, Card, Grid } from "semantic-ui-react";

import QuillText from "../../CardElements/QuillText";

const SummaryComponent = props => {
  const { val } = props;

  return (
    <List.Item
      className="padding-bottom-20"
      style={{
        display: !val.summry || val.summry.length === 11 ? "none" : "block"
      }}>
      <List.Content>
        <List.Header className="padding-bottom-10">
          <Header
            className="text-pacific-blue font-w-500"
            as="h3"
            style={{ fontSize: "18px" }}>
            Summary
          </Header>
        </List.Header>
        <List.Description>
          <QuillText value={val.summry ? val.summry : ""} readOnly />
        </List.Description>
      </List.Content>
    </List.Item>
  );
};

export default SummaryComponent;
