import React from "react";
import {
  Image,
  Header,
  Feed,
  Accordion,
  List,
  Icon,
  Button,
  Grid,
  Card
} from "semantic-ui-react";
import CompanySvg from "../../../assets/svg/IcCompany";
import EducationLogo from "../../../assets/svg/IcEducation";
const EducationComponent = props => {
  const { val } = props;

  return (
    <List.Item
      className="padding-bottom-20"
      style={{
        display: !val.institute ? "none" : "block"
      }}>
      <List.Content>
        <List.Header className="padding-bottom-10">
          <Header
            className="text-pacific-blue font-w-500"
            as="h3"
            style={{ fontSize: "18px" }}>
            Education
          </Header>
        </List.Header>
        <List.Description className="filterSkill">
          <div className>
            <List.Content floated="left">
              {/* <Image avatar src={profile} /> */}
              <span className="educationThumb margin-right-5">
                <EducationLogo
                  width="40"
                  height="40"
                  rectcolor="#f7f7fb"
                  pathcolor="#c8c8c8"
                />
              </span>
            </List.Content>

            <List.Content floated="left">
              <List.Header>
                <p className="vasitumFontOpen-500 text-night-rider margin-bottom-0 font-size-16">
                  {/* {val.institute} */}
                  {val.institute}
                </p>
              </List.Header>
              <List.Description>
                <p className="margin-bottom-0 text-Matterhorn">
                  {val.edus ? val.edus[0] : null}
                </p>
                {/* <p className="text-trolley-grey font-size-13">2012 - 2014</p> */}
              </List.Description>
            </List.Content>
          </div>
        </List.Description>
      </List.Content>
    </List.Item>
  );
};

export default EducationComponent;
