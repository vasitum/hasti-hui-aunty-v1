import React from "react";
import LocationLogo from "../../../assets/svg/IcLocation";

const LocationComponent = props => {
  const { hit, ...restProps } = props;

  return (
    <p
      {...restProps}
      style={{
        display: !hit.locCity ? "none" : "block"
      }}>
      <LocationLogo width="9.5" height="11.5" pathcolor="#0bd0bb" />

      {hit.remote ? (
        <span className="padding-left-3">Remote </span>
      ) : (
        <span className="padding-left-3">
          {hit.locCity}
          {hit.locCountry ? ", " + hit.locCountry : ""}
        </span>
      )}
    </p>
  );
};

export default LocationComponent;
