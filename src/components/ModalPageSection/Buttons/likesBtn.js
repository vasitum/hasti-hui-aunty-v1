import React from "react";
import { Button } from "semantic-ui-react";
import getPopUp from '../propus/propus';



class LikesBtn extends React.Component {
  render() {
    return (
      <Button
        size="mini"
        className="bg-transparent socialBtn fontSize-12">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="15"
          height="15"
          viewBox="0 0 17 16">
          <defs>
            <filter
              id="filter"
              x="1111.03"
              y="353"
              width="17"
              height="16"
              filterUnits="userSpaceOnUse">
              <feFlood result="flood" floodColor="#c8c8c8" />
              <feComposite
                result="composite"
                operator="in"
                in2="SourceGraphic"
              />
              <feBlend result="blend" in2="SourceGraphic" />
            </filter>
          </defs>
          <g fill="#c8c8c8">
            <path
              className="cls-1"
              d="M1128,357.77c-0.24-2.766-2.18-4.773-4.6-4.773a4.582,4.582,0,0,0-3.92,2.286,4.383,4.383,0,0,0-3.82-2.286c-2.42,0-4.35,2.007-4.6,4.772a5.03,5.03,0,0,0,.14,1.814,7.846,7.846,0,0,0,2.36,3.977l5.92,5.442,6.03-5.442a7.828,7.828,0,0,0,2.35-3.977A5.025,5.025,0,0,0,1128,357.77Z"
              transform="translate(-1111.03 -353)"
            />
          </g>
        </svg>
        {/* <span className="padding-left-3">Like</span> */}
      </Button>
    )
  }
}

export default getPopUp(<LikesBtn />, "Like");