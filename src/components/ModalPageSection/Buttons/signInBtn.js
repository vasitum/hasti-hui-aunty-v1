import React from "react";
import { Button, Icon } from "semantic-ui-react";

export const SignInBtn = props => {
  return (
    <Button type="submit" className="bg-pacific-blue margin-top-24 text-white" fluid>
      {props.btntext} <Icon name="long arrow right" />
    </Button>
  )
};

export default SignInBtn;
