import React from "react";
import { Button, Popup, Modal, Grid, Input } from "semantic-ui-react";

import LikeLogo from "../../../assets/svg/IcLike";
import ShareLogo from "../../../assets/svg/IcShare";
import SaveLogo from "../../../assets/svg/IcSave";
import ShareButton from "../../Buttons/ShareButton";
import ShareModal from "../../ShareButton/ShareModal";
import ActionBtn from "../../Buttons/ActionBtn";
import FlatDefaultBtn from "../../Buttons/FlatDefaultBtn";

import DefaultModal from "../../Modal/DefaultModal";

import AddLabelSave from "../../CardElements/AddLabelSave";

import isLoggedOutFunc from "../../../utils/env/isLoggedin";
import isProfileCreated from "../../../utils/env/isProfileCreated";

import UserAccessModal from "../UserAccessModal";

import "./index.scss";

const Popupstyle = {
  backgroundColor: "#1F2532",
  padding: ".5em",
  color: "#ffffff",
  border: "none",
  fontSize: "12px"
};

class SocialBtn extends React.Component {
  state = {
    isLiked: false,
    isSaved: false,
    modalOpen: false,
    modal: false
  };
  handleModal = () => this.setState({ modal: !this.state.modal });
  constructor(props) {
    super(props);
    this.loggedOut = isLoggedOutFunc();
  }

  handleOpen = e => {
    e.stopPropagation();
    if (!isProfileCreated()) {
      return;
    }
    this.setState({ modalOpen: true });
  };

  handleClose = e => {
    e.stopPropagation();

    this.setState({ modalOpen: false });
  };

  onLikeClick = (ev, props) => {
    if (!isProfileCreated()) {
      return;
    }

    this.setState({
      isLiked: !this.state.isLiked
    });
    this.props.onLikeClick(ev, props);
  };

  onSaveClick = (e, props) => {
    // e.stopPropagation();
    if (!isProfileCreated()) {
      return;
    }

    this.setState({
      isSaved: !this.state.isSaved,
      modalOpen: false
    });
  };

  componentDidMount() {
    const { liked, saved } = this.props;
    this.setState({
      isLiked: liked,
      isSaved: saved
    });
  }

  render() {
    const { objectID, handleOpen, type, disabled } = this.props;
    const isLoggedOut = this.loggedOut;
    if (isLoggedOut) {
      return (
        <div className="displayInlineBlock">
          <UserAccessModal
            modalTrigger={
              <Button
                size="mini"
                onClick={this.onLikeClick}
                id={objectID}
                className="bg-transparent socialBtn fontSize-12">
                <LikeLogo
                  width="15"
                  height="15"
                  pathcolor={this.state.isLiked ? "#0b9ed0" : "#c8c8c8"}
                />
              </Button>
            }
            isAction
            actionText="like"
          />
          {/* <ShareButton
            trigger={ */}
          <Button
            onClick={this.handleModal}
            disabled={isLoggedOut}
            size="mini"
            className="bg-transparent socialBtn fontSize-12 text-Matterhorn">
            <ShareLogo width="15" height="15" pathcolor="#c8c8c8" />
          </Button>
          {/* }
          /> */}
          {type !== "user" ? (
            <UserAccessModal
              modalTrigger={
                <Button
                  size="mini"
                  onClick={this.onLikeClick}
                  // id={objectID}
                  className="bg-transparent socialBtn fontSize-12">
                  <SaveLogo
                    width="15"
                    height="15"
                    pathcolor={this.state.isSaved ? "#0b9ed0" : "#c8c8c8"}
                  />
                </Button>
              }
              isAction
              actionText="save"
            />
          ) : null}
        </div>
      );
    }

    return (
      <div className="displayInlineBlock">
        {/* <Popup
          trigger={
            <Button
              size="mini"
              onClick={this.onLikeClick}
              id={objectID}
              disabled={isLoggedOut || disabled}
              className="bg-transparent socialBtn fontSize-12">
              <LikeLogo
                width="15"
                height="15"
                pathcolor={this.state.isLiked ? "#0b9ed0" : "#c8c8c8"}
              />            
            </Button>
          }
          className="socialPropup"
          content="Like"
        /> */}
        <Button
          size="mini"
          onClick={this.onLikeClick}
          id={objectID}
          disabled={isLoggedOut || disabled}
          className="bg-transparent socialBtn fontSize-12">
          <LikeLogo
            width="15"
            height="15"
            pathcolor={this.state.isLiked ? "#0b9ed0" : "#c8c8c8"}
          />
        </Button>
        {/* <ShareButton
          trigger={ */}
        <Button
          onClick={this.handleModal}
          disabled={isLoggedOut}
          size="mini"
          className="bg-transparent socialBtn fontSize-12 text-Matterhorn">
          <ShareLogo width="15" height="15" pathcolor="#c8c8c8" />
        </Button>
        {/* }
        /> */}

        {type !== "user" ? (
          !this.state.isSaved ? (
            <DefaultModal
              size="tiny"
              open={this.state.modalOpen}
              trigger={
                <Button
                  size="mini"
                  disabled={isLoggedOut || disabled}
                  onClick={this.handleOpen}
                  className="bg-transparent margin-right-10 socialBtn fontSize-12 text-Matterhorn">
                  <SaveLogo
                    width="15"
                    height="15"
                    pathcolor={this.state.isSaved ? "#0b9ed0" : "#c8c8c8"}
                  />
                </Button>
              }>
              <AddLabelSave
                saveObjectId={objectID}
                onCloseBtnClick={this.handleClose}
                onSaveClick={this.onSaveClick}
                type={this.props.type}
              />
            </DefaultModal>
          ) : (
            <Button
              size="mini"
              className="bg-transparent margin-right-10 socialBtn fontSize-12 text-Matterhorn">
              <SaveLogo
                width="15"
                height="15"
                pathcolor={this.state.isSaved ? "#0b9ed0" : "#c8c8c8"}
              />
            </Button>
          )
        ) : null}

        <ShareModal
          modalOpen={this.state.modal}
          handleModal={this.handleModal}
          url={`/view/user/${objectID}`}
        />
      </div>
    );
  }
}

export default SocialBtn;
