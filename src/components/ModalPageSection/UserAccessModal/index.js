import React from "react";

import { Modal, Button, Grid, Icon } from "semantic-ui-react";
// import { size, grid } from "Constants/semantic";

import UserAccessModalLeft from "./UserAccessModalLeft";
import UserAccessModalRight from "./UserAccessModalRight";
import { withRouter } from "react-router-dom";
import qs from "qs";
import BorderButton from "../../Buttons/BorderButton";
import ReactGA from "react-ga";
import "./index.scss";

const inlineStyle = {
  modal: {
    marginTop: "0px !important"
  }
};

class UAModal extends React.Component {
  state = {
    modalOpen: false
  };

  modalTrigger = open => (
    <BorderButton
      onClick={open}
      className="btn-border"
      btnText="Sign In / Sign up"
      icon="angle down"
    />
  );

  onModalOpen = e => {
    this.setState({
      modalOpen: true
    });
  };

  onModalClose = e => {
    this.setState({
      modalOpen: false
    });
  };

  componentDidMount() {
    ReactGA.modalview("/modal/signup-signin");
    if (window.location.pathname === "/") {
      const parsedQuery = qs.parse(window.location.search);
      if (Object.prototype.hasOwnProperty.call(parsedQuery, "?action")) {
        switch (parsedQuery["?action"]) {
          case "sign_up":
            this.setState({
              modalOpen: true
            });
            break;

          default:
            break;
        }
      }
    }
  }

  render() {
    const { modalTrigger, isAction, actionText, entity } = this.props;

    if (modalTrigger) {
      return (
        <Modal
          size="small"
          closeIcon
          trigger={modalTrigger}
          className="Useraccessmodal"
          style={inlineStyle.modal}
          closeOnDimmerClick={false}>
          <Grid columns={2}>
            <Grid.Column className="margin-b-0 p-r-0">
              <div className="Useraccessmodal__left position-r">
                <UserAccessModalLeft
                  isAction={isAction}
                  actionText={actionText}
                  entity={entity}
                />
              </div>
            </Grid.Column>

            <Grid.Column className="margin-b-0 p-l-0">
              <UserAccessModalRight />
            </Grid.Column>
          </Grid>
        </Modal>
      );
    }

    return (
      <Modal
        size="small"
        open={this.state.modalOpen}
        closeIcon
        onClose={this.onModalClose}
        trigger={this.modalTrigger(this.onModalOpen)}
        className="Useraccessmodal"
        style={inlineStyle.modal}
        closeOnDimmerClick={false}>
        <Grid>
          <Grid.Column width={8} className="margin-b-0 p-r-0 leftSideUserLogin">
            <div className="Useraccessmodal__left position-r">
              <UserAccessModalLeft
                isAction={isAction}
                actionText={actionText}
              />
            </div>
          </Grid.Column>

          <Grid.Column
            width={8}
            className="margin-b-0 p-l-0 RightSideUserLogin">
            <UserAccessModalRight />
          </Grid.Column>
        </Grid>
      </Modal>
    );
  }
}

export default withRouter(UAModal);
