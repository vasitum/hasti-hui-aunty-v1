import { Tab, List, Image } from "semantic-ui-react";
// import SignUpForm from "Components/forms/userAccess/SignUpForm";
// import facebook from "";
// import google from "Assets/img/google.png";
import UserSignUp from "../UserSignIn";

import React, { Component } from "react";

import formSignUpUser from "../../../api/user/signupUser";
import { USER } from "../../../constants/api";
import { toast } from "react-toastify";

import { withRouter } from "react-router-dom";
import { withUserContextConsumer } from "../../../contexts/UserContext";
import PropTypes from "prop-types";

class SignUpTab extends Component {
  state = {
    email: "",
    password: "",
    showPassword: false,
    canSubmit: false
  };

  constructor(props) {
    super(props);
    this.toast = null;
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  onInputChange = (ev, { value, name }) => {
    this.setState({
      [name]: value
    });
  };

  onPasswordEyeClick = ev => {
    this.setState({
      showPassword: !this.state.showPassword
    });
  };

  onFormValid = ev => {
    this.setState({
      canSubmit: true
    });
  };

  onFormInValid = ev => {
    this.setState({
      canSubmit: false
    });
  };

  async onFormSubmit(ev) {
    try {
      const res = await formSignUpUser({
        email: this.state.email,
        pwd: this.state.password
      });

      if (res.status === 406) {
        this.toast = toast("User is already registerd");
        return;
      }

      const authToken = res.headers.get("X-Maven-REST-AUTH-TOKEN");
      const data = await res.json();

      window.localStorage.setItem(USER.X_AUTH_ID, authToken);
      window.localStorage.setItem(USER.BASE_JSON, JSON.stringify(data));
      this.props.onUserChange({
        id: data._id,
        email: data.email,
        fName: "",
        lName: "",
        title: "",
        loc: "",
        userImg: "",
        mobile: "",
        token: data.googleToken,
        isVerified: data.isVerified,
        ctc: 0
      });

      if (window.location.pathname === "/cv-template") {
        this.props.history.push("/emailVerification?action=cv-template");
      } else {
        // push to create profile
        this.props.history.push(
          `/emailVerification/${data._id}/${
            window.location.search ? window.location.search : ""
          }`
        );
      }

      // console.log(data);
    } catch (error) {
      this.toast = toast("Unable to sign up");
      console.log(error);
    }
  }

  componentWillUnmount() {
    if (this.toast) {
      toast.dismiss(this.toast);
    }
  }

  render() {
    return (
      <div className="Useraccessform">
        <Tab.Pane attached={false} className="transition fade in">
          <UserSignUp
            signup
            className="displayNone"
            siginbtntext="Get started"
            onFormSubmit={this.onFormSubmit}
            onFormValid={this.onFormValid}
            onFormInValid={this.onFormInValid}
            onInputChange={this.onInputChange}
            canSubmit={this.state.canSubmit}
            onShowPasswordClick={this.onPasswordEyeClick}
            data={this.state}
          />
        </Tab.Pane>
      </div>
    );
  }
}

SignUpTab.propTypes = {};

export default withUserContextConsumer(withRouter(SignUpTab));
