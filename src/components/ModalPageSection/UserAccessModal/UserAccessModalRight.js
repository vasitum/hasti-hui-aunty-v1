import React from "react";
import Slider from "react-slick";
import { Modal, Header, Image } from "semantic-ui-react";

import sliderSignInImage from "../../../assets/img/sliderSignIn.png";
import sliderWhyVasitum from "../../../assets/img/tabs/ic_log_in_why_vasitum_free.png";
import sliderSmartInterview from "../../../assets/img/tabs/ic_log_in_why_vasitum_smart_interview.png";
import sliderAiBased from "../../../assets/img/tabs/ic_log-in_why_vasitum_ai_based.png";
import AiVasiLogo from "../../../assets/img/UserFeedBack/VasiImage.png";
import RealtimeApplicationStatus from "../../../assets/img/Features/RealtimeApplicationStatus.png";
import AutomaticPreScreeningSchedulingTransparent from "../../../assets/img/Features/AutomaticPreScreeningSchedulingTransparent bg.png";

import isLoggedIn from "../../../utils/env/isLoggedin";

const sliderSettings = {
  dots: true,
  infinite: true,
  autoplay: true,
  arrows: false,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1
};

const sliderData = [
  {
    img: AiVasiLogo,
    heading: "AI-based",
    subtext: "Personal Assistance"
  },
  {
    img: RealtimeApplicationStatus,
    heading: "Real Time ",
    subtext: "Application Updates"
  },
  {
    img: AutomaticPreScreeningSchedulingTransparent,
    heading: "Automatic",
    subtext: "Pre-screening and Interview Scheduling"
  }
];

export default () => {
  return (
    <div className="Useraccessmodal__right">
      <div className="Useraccessmodal_slider">
        <Modal.Header className="p-b-30">
          <h3 className="font-w-500">Why Vasitum?</h3>
        </Modal.Header>
        <Slider {...sliderSettings} className="padding-top-20">
          {sliderData.map(item => (
            <div key={item.heading}>
              <Modal.Content>
                <Image wrapped src={item.img} />
                <div className="padding-top-20 padding-bottom-20" />
                <div className="sliderSubTitle padding-bottom-70">
                  <h2 className="margin-bottom-0">{item.heading}</h2>
                  <p>{item.subtext}</p>
                </div>
              </Modal.Content>
            </div>
          ))}
        </Slider>
      </div>
    </div>
  );
};
