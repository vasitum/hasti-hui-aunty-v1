import React from "react";
import { Tab } from "semantic-ui-react";
import SignInTab from "./SignInTab";
import SignUpTab from "./SignUpTab";

import ForgetPassword from "../ForgetPassword";

function getPanes(togglePassword) {
  return [
    {
      menuItem: "Sign In",
      render: () => <SignInTab onForgetClick={togglePassword} />
    },
    {
      menuItem: "Sign Up",
      render: () => <SignUpTab />
    }
  ];
}

class UseraccessModalLeft extends React.Component {
  state = {
    forgetPassword: false
  };

  onForgetToggle = e => {
    this.setState({
      forgetPassword: !this.state.forgetPassword
    });
  };

  render() {
    const { isAction, actionText, entity } = this.props;
    return !this.state.forgetPassword ? (
      <div>
        {isAction && (
          <div
            style={{
              padding: "19px",
              background: "rgba(11, 208, 187, 1)",
              borderRadius: "6px",
              marginBottom: "10px",
              color: "#fff",
              position: "relative"
            }}>
            Please sign in or sign up to complete this action
            {actionText} 
            {/* this{" "} */}
            {/* {entity ? entity : "job"} */}
            <span
              style={{
                position: "absolute",
                bottom: "-10px",
                left: "32px",
                width: "0",
                height: "0",
                borderLeft: "10px solid transparent",
                borderRight: "10px solid transparent",
                borderTop: "10px solid rgba(11, 208, 187, 1)"
              }}
            />
          </div>
        )}
        <Tab menu={{ text: true }} panes={getPanes(this.onForgetToggle)} />
      </div>
    ) : (
      <ForgetPassword onDone={this.onForgetToggle} />
    );
  }
}

export default UseraccessModalLeft;
