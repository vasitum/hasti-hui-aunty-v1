import React, { Component } from "react";
import { Tab, List, Image } from "semantic-ui-react";
import UserSignUp from "../UserSignIn";

import formSignInUser from "../../../api/user/signinUser";
import { USER } from "../../../constants/api";
import { toast } from "react-toastify";

import { withUserContextConsumer } from "../../../contexts/UserContext";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";

class SignInTab extends Component {
  constructor(props) {
    super(props);
    this.toast = null;
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  state = {
    email: "",
    password: "",
    showPassword: false,
    canSubmit: false,
    showForgetPassword: false
  };

  onInputChange = (ev, { value, name }) => {
    this.setState({
      [name]: value
    });
  };

  onPasswordEyeClick = ev => {
    this.setState({
      showPassword: !this.state.showPassword
    });
  };

  onFormValid = ev => {
    this.setState({
      canSubmit: true
    });
  };

  onFormInValid = ev => {
    this.setState({
      canSubmit: false
    });
  };

  onForgetPasswordClick = ev => {
    this.setState({
      showForgetPassword: !this.state.showForgetPassword
    });
  };

  componentWillUnmount() {
    if (this.toast) {
      toast.dismiss(this.toast);
    }
  }

  async onFormSubmit(ev) {
    try {
      const res = await formSignInUser({
        email: this.state.email,
        password: this.state.password
      });

      if (res.status === 404) {
        this.toast = toast("Please Register, account not found");
        // alert("Please register!");
        return;
      }

      if (res.status === 406) {
        this.toast = toast("Incorrect Password");
        return;
      }

      const authToken = res.headers.get("X-Maven-REST-AUTH-TOKEN");
      const data = await res.json();

      window.localStorage.setItem(USER.X_AUTH_ID, authToken);
      // window.localStorage.setItem(USER.UID, data._id);
      // window.localStorage.setItem(USER.EMAIL, data.email);
      // window.localStorage.setItem(USER.NAME, `${data.fName} ${data.lName}`);
      // window.localStorage.setItem(
      //   USER.CTC,
      //   data.desireJob ? data.desireJob.currentCtc : ""
      // );
      // window.localStorage.setItem(USER.EXP, 0);
      // window.localStorage.setItem(USER.IMG_EXT, data.ext ? data.ext.img : "");
      // window.localStorage.setItem(USER.LOC, data.loc ? data.loc.city : "");
      // window.localStorage.setItem(USER.PHONE, data.mobile);
      // window.localStorage.setItem(USER.TITLE, data.title);

      this.props.onUserChange({
        id: data._id,
        email: data.email,
        fName: data.fName,
        lName: data.lName,
        title: data.title,
        loc: data.loc ? data.loc.city : "",
        userImg: data.ext ? data.ext.img : "",
        mobile: data.mobile,
        token: data.googleToken,
        isVerified: data.isVerified,
        ctc: 0
      });

      if (!data.isVerified) {
        this.props.history.push("/emailVerification");
      }

      // signin reload and reintialize all the data
      // window.location.reload();

      // console.log(data);
    } catch (error) {
      console.log(error);
    }
  }

  onForgetPasswordClick = ev => {
    this.setState({ showForgetPassword: !this.state.showForgetPassword });
  };

  render() {
    return (
      <div className="Useraccessform">
        <Tab.Pane attached={false} className="transition fade in">
          <UserSignUp
            onForgetClick={this.props.onForgetClick}
            siginbtntext="Log In"
            onFormSubmit={this.onFormSubmit}
            onFormValid={this.onFormValid}
            onFormInValid={this.onFormInValid}
            onInputChange={this.onInputChange}
            canSubmit={this.state.canSubmit}
            onShowPasswordClick={this.onPasswordEyeClick}
            onForgetPasswordClick={this.onForgetPasswordClick}
            data={this.state}
          />

          {/* <div className="forgot-pass text-center padding-top-20">
            <a href="#!" className="text-matterhorn">
              Forgot Password
            </a>
          </div> */}
        </Tab.Pane>
      </div>
    );
  }
}

SignInTab.propTypes = {};

export default withUserContextConsumer(withRouter(SignInTab));
