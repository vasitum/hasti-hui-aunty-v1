import React from "react";
import {
  Form,
  List,
  Button,
  Icon,
  Input,
  Image,
  Checkbox
} from "semantic-ui-react";
import facebook from "../../../assets/img/facebook.png";
import google from "../../../assets/img/google.png";

import formSignUpUser from "../../../api/user/signupUser";
import IcProfileView from "../../../assets/svg/IcProfileView";
import IcHide from "../../../assets/svg/IcHide";
import { USER } from "../../../constants/api";

import { withRouter } from "react-router-dom";

import { toast } from "react-toastify";

import "./index.scss";

class UserSignUp extends React.Component {
  constructor(props) {
    super(props);
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  state = {
    email: "",
    password: "",
    showPassword: false,
    canSubmit: false
  };

  onInputChange = (ev, { value, name }) => {
    this.setState({
      [name]: value
    });
  };

  onPasswordEyeClick = ev => {
    this.setState({
      showPassword: !this.state.showPassword
    });
  };

  async onFormSubmit(ev) {
    try {
      const res = await formSignUpUser({
        email: this.state.email,
        pwd: this.state.password
      });

      if (res.status === 406) {
        toast("User already registered, Please Signin!");
        return;
      }

      const authToken = res.headers.get("X-Maven-REST-AUTH-TOKEN");
      const data = await res.json();

      window.localStorage.setItem(USER.X_AUTH_ID, authToken);
      window.localStorage.setItem(USER.UID, data._id);
      window.localStorage.setItem(USER.EMAIL, data.email);
      window.localStorage.setItem(USER.BASE_JSON, JSON.stringify(data));

      // push to create profile
      this.props.history.push("/user/new");

      // console.log(data);
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    return (
      <div className="SearchSignIn">
        <List>
          {/* <List.Content className="padding-top-20 padding-bottom-20">
            <List.Description className="bgMagnolia paddingBestPanelSignIn border-r-5">
              <List horizontal className="padding-top-10 ">
                <List.Item as="a">
                  <Image src={facebook} />
                  <List.Content className="text-Matterhorn facebook_icon">
                    Facebook
                  </List.Content>
                </List.Item>
                <List.Item as="a" className="margin-left-30">
                  <Image src={google} />
                  <List.Content className="text-Matterhorn google_icon">
                    Google
                  </List.Content>
                </List.Item>
              </List>
            </List.Description>
          </List.Content> */}
          <List.Content className="padding-top-20">
            <Form onSubmit={this.onFormSubmit}>
              <Form.Field
                control={Input}
                className="b-r-30 input-default"
                type="email"
                placeholder="Email"
                name="email"
                onChange={this.onInputChange}
                value={this.state.email}
                required
                style={{ marginBottom: "10px" }}
              />
              <div className="preview_Input">
                <Form.Field
                  control={Input}
                  type={!this.state.showPassword ? "password" : "text"}
                  className="b-r-30 input-default"
                  placeholder="Password"
                  name="password"
                  value={this.state.password}
                  onChange={this.onInputChange}
                  required
                />
                <div className="preview_Icon">
                  <Button type="button" onClick={this.onPasswordEyeClick}>
                    {this.state.showPassword ? (
                      <IcProfileView pathcolor="#c8c8c8" />
                    ) : (
                      <span
                        style={{
                          display: "inline-block",
                          marginTop: "0px"
                        }}>
                        <IcHide pathcolor="#c8c8c8" />
                      </span>
                    )}
                  </Button>
                </div>
              </div>

              <Form.Field className={this.props.className}>
                <Checkbox
                  className="checkbox-default"
                  label="Remember me next time"
                  checked
                />
              </Form.Field>

              <Button
                type="submit"
                className="bg-pacific-blue margin-top-24 text-white"
                fluid>
                {this.props.siginbtntext} <Icon name="long arrow right" />
              </Button>
            </Form>
          </List.Content>
        </List>
      </div>
    );
  }
}

export default withRouter(UserSignUp);
