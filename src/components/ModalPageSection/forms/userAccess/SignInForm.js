import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Form, Input, Button, Icon, Checkbox } from "semantic-ui-react";

import signInApi from "Api/user/signin";

class SignInForm extends React.Component {
  state = {
    email: "",
    password: ""
  };

  onInputChange = (ev, { value, ident }) => {
    this.setState({
      [ident]: value
    });
  };

  onSignInSubmit = ev => {
    signInApi(this.state)
      .then(res => res.json())
      .then(res => console.log(res))
      .catch(err => console.error(err));

    // this.props.onSignInSubmit({
    //   email: this.state.email,
    //   password: this.state.password
    // });
  };

  // TODO: Implement Social SignUps
  onFacebookSubmit = ev => {};
  onGoogleSubmit = ev => {};

  render() {
    return (
      <div>
        <Form onSubmit={this.onSignInSubmit}>
          <Form.Field
            control={Input}
            value={this.state.email}
            onChange={this.onInputChange}
            type="email"
            ident="email"
            placeholder="Email"
            className="input-default"
            required
          />
          <Form.Field
            control={Input}
            type="password"
            placeholder="Password"
            ident="password"
            className="input-default"
            onChange={this.onInputChange}
            value={this.state.password}
            required
          />
          <Form.Field>
            <Checkbox
              className="checkbox-default"
              label="Remember me next time"
            />
          </Form.Field>
          <Button type="submit" className="is-blue-light" fluid>
            Log In
            <span className="padding-left-12">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="22.969"
                height="12"
                viewBox="0 0 22.969 14">
                <defs>
                  <filter
                    id="filter"
                    x="402.25"
                    y="557"
                    width="22.969"
                    height="14"
                    filterUnits="userSpaceOnUse">
                    <feFlood result="flood" floodColor="#99d9f9" />
                    <feComposite
                      result="composite"
                      operator="in"
                      in2="SourceGraphic"
                    />
                    <feBlend result="blend" in2="SourceGraphic" />
                  </filter>
                </defs>
                <g fill="#99d9f9">
                  <path
                    className="cls-1"
                    d="M403.164,563.056h18.882l-4.338-4.444a0.956,0.956,0,0,1,0-1.336,0.919,0.919,0,0,1,1.313,0l5.922,6.056c0.022,0.022.043,0.046,0.063,0.07s0.015,0.021.022,0.031,0.022,0.028.032,0.043l0.023,0.039c0.008,0.014.016,0.027,0.023,0.041s0.013,0.027.02,0.041l0.019,0.042c0.006,0.014.01,0.027,0.015,0.041l0.016,0.046c0,0.014.007,0.028,0.01,0.041s0.009,0.033.012,0.049,0,0.032.007,0.048,0,0.029.006,0.043c0,0.03,0,.059,0,0.088v0.01c0,0.029,0,.059,0,0.088s0,0.029-.006.043,0,0.032-.007.049-0.008.031-.012,0.047-0.006.028-.01,0.042-0.01.03-.016,0.045-0.009.029-.015,0.042-0.012.028-.019,0.041a0.336,0.336,0,0,1-.02.043l-0.022.038c-0.008.014-.016,0.028-0.025,0.041s-0.018.027-.028,0.04l-0.026.035c-0.018.022-.037,0.044-0.058,0.065l0,0-5.923,6.056a0.917,0.917,0,0,1-1.313,0,0.954,0.954,0,0,1,0-1.335l4.338-4.445H403.164A0.944,0.944,0,0,1,403.164,563.056Z"
                    transform="translate(-402.25 -557)"
                  />
                </g>
              </svg>
            </span>
          </Button>
        </Form>
      </div>
    );
  }
}

SignInForm.propTypes = {
  onSignInSubmit: PropTypes.func.isRequired
};

SignInForm.defaultProps = {
  onSignInSubmit: () =>
    console.error("Error: SignInSubmit Not Defined! Check redux connection")
};

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    onSignInSubmit: data => {
      alert("Form Submitted");
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignInForm);
