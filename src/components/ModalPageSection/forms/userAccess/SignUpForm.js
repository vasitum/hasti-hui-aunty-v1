import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Form, Input, Button, Icon } from "semantic-ui-react";

import signUpApi from "Api/user/signup";
import { userExportStorage } from "Constants/user";
import formatUserJson from "Utils/user/formatUserJson";
import { signUpAction } from "Actions/userAccess";
class SignUpForm extends React.Component {
  state = {
    email: "",
    password: "",
    firstName: "",
    lastName: ""
  };

  onInputChange = (ev, { value, ident }) => {
    this.setState({
      [ident]: value
    });
  };

  async onSignUpSubmit(ev) {
    try {
      const res = await signUpApi(this.state);
      const authToken = res.headers.get("X-Maven-REST-AUTH-TOKEN");
      const resJson = await res.json();

      // set header to localStorage
      window.localStorage.setItem(userExportStorage.AUTH_TOKEN, authToken);

      // send the json to store
      this.props.onSignUpSubmit(formatUserJson(resJson));
    } catch (error) {
      console.error(error);
    }
  }

  // TODO: Implement Social SignUps
  onFacebookSubmit = ev => {};
  onGoogleSubmit = ev => {};

  render() {
    return (
      // <Form onSubmit={this.onSignUpSubmit.bind(this)}>
      //   <Form.Field
      //     control={Input}
      //     value={this.state.firstName}
      //     onChange={this.onInputChange}
      //     type="text"
      //     placeholder="First Name"
      //     ident="firstName"
      //     required
      //   />
      //   <Form.Field
      //     control={Input}
      //     value={this.state.lastName}
      //     onChange={this.onInputChange}
      //     type="text"
      //     placeholder="Last Name"
      //     ident="lastName"
      //     required
      //   />
      //   <Form.Field
      //     control={Input}
      //     value={this.state.email}
      //     onChange={this.onInputChange}
      //     type="email"
      //     placeholder="Email"
      //     ident="email"
      //     required
      //   />
      //   <Form.Field
      //     control={Input}
      //     type="password"
      //     placeholder="Password"
      //     ident="password"
      //     onChange={this.onInputChange}
      //     value={this.state.password}
      //     required
      //   />
      //   <Button type="submit" color="blue" fluid>
      //     Get Started, it's Free! <Icon name="long arrow right" />
      //   </Button>
      // </Form>

      <div className="signIn-signUp-form p-b-30 padding-top-20">
        <Form onSubmit={this.onSignUpSubmit}>
          {/* <Form.Field>
            <input
              value={this.state.firstName}
              onChange={this.onInputChange}
              ident="firstName"
              type="text"
              name="firstName"
              placeholder="First Name"
            />
          </Form.Field> */}
          {/* <Form.Field
            control={Input}
            value={this.state.firstName}
            onChange={this.onInputChange}
            type="text"
            ident="firstName"
            placeholder="First Name"
            required
          /> */}
          {/* <Form.Field>
            <input
              value={this.state.lastName}
              onChange={this.onInputChange}
              ident="lastName"
              type="text"
              name="lastName"
              placeholder="Last Name"
            />
          </Form.Field> */}
          {/* <Form.Field
            control={Input}
            value={this.state.lastName}
            onChange={this.onInputChange}
            type="text"
            ident="lastName"
            placeholder="Last Name"
            required
          /> */}
          {/* <Form.Field>
            <input
              value={this.state.email}
              onChange={this.onInputChange}
              ident="email"
              type="email"
              name="email"
              placeholder="Password"
            />
          </Form.Field> */}
          <Form.Field
            control={Input}
            value={this.state.email}
            onChange={this.onInputChange}
            type="email"
            ident="email"
            placeholder="Email"
            required
          />
          {/* <Form.Field>
            <input
              value={this.state.password}
              onChange={this.onInputChange}
              ident="password"
              type="password"
              name="password"
              placeholder="Password"
            />
          </Form.Field> */}
          <Form.Field
            control={Input}
            value={this.state.password}
            onChange={this.onInputChange}
            type="password"
            ident="password"
            placeholder="Password"
            required
          />
          <Button
            type="submit"
            className="bg-pacific-blue text-white b-r-30"
            fluid>
            Get Started Free
            <span className="padding-left-12">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="22.969"
                height="12"
                viewBox="0 0 22.969 14">
                <defs>
                  <filter
                    id="filter"
                    x="402.25"
                    y="557"
                    width="22.969"
                    height="14"
                    filterUnits="userSpaceOnUse">
                    <feFlood result="flood" floodColor="#99d9f9" />
                    <feComposite
                      result="composite"
                      operator="in"
                      in2="SourceGraphic"
                    />
                    <feBlend result="blend" in2="SourceGraphic" />
                  </filter>
                </defs>
                <g fill="#99d9f9">
                  <path
                    className="cls-1"
                    d="M403.164,563.056h18.882l-4.338-4.444a0.956,0.956,0,0,1,0-1.336,0.919,0.919,0,0,1,1.313,0l5.922,6.056c0.022,0.022.043,0.046,0.063,0.07s0.015,0.021.022,0.031,0.022,0.028.032,0.043l0.023,0.039c0.008,0.014.016,0.027,0.023,0.041s0.013,0.027.02,0.041l0.019,0.042c0.006,0.014.01,0.027,0.015,0.041l0.016,0.046c0,0.014.007,0.028,0.01,0.041s0.009,0.033.012,0.049,0,0.032.007,0.048,0,0.029.006,0.043c0,0.03,0,.059,0,0.088v0.01c0,0.029,0,.059,0,0.088s0,0.029-.006.043,0,0.032-.007.049-0.008.031-.012,0.047-0.006.028-.01,0.042-0.01.03-.016,0.045-0.009.029-.015,0.042-0.012.028-.019,0.041a0.336,0.336,0,0,1-.02.043l-0.022.038c-0.008.014-.016,0.028-0.025,0.041s-0.018.027-.028,0.04l-0.026.035c-0.018.022-.037,0.044-0.058,0.065l0,0-5.923,6.056a0.917,0.917,0,0,1-1.313,0,0.954,0.954,0,0,1,0-1.335l4.338-4.445H403.164A0.944,0.944,0,0,1,403.164,563.056Z"
                    transform="translate(-402.25 -557)"
                  />
                </g>
              </svg>
            </span>
          </Button>
        </Form>
      </div>
    );
  }
}

SignUpForm.propTypes = {
  onSignUpSubmit: PropTypes.func.isRequired
};

SignUpForm.defaultProps = {
  onSignUpSubmit: () =>
    console.error("Error: SignInSubmit Not Defined! Check redux connection")
};

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    onSignUpSubmit: data => {
      dispatch(signUpAction(data));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUpForm);
