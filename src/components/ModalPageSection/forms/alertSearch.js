import React from 'react';
import { Card, Header, Input } from 'semantic-ui-react';

export const AlertSearch = (props) => {
  return (
    <Card className="bg-white" fluid className="box-shadow-card">
      <Card.Content>
        <Card.Header>
          <Header as="h3" className="vasitumFontOpen-600 padding-bottom-10">
            {props.alertText}
          </Header>
        </Card.Header>
        <Card.Meta className="searchInput">
          <Input
            icon={{ name: "mail", circular: true, link: true }}
            placeholder="eg. dummyname@gmail.com"
            fluid
            className="b-r-30"
          />
        </Card.Meta>
      </Card.Content>
    </Card>
  )
}



export default AlertSearch;






          