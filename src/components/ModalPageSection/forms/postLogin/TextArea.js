import React from 'react';
import { Grid, TextArea, Form, Header} from "semantic-ui-react";
import PropTypes from 'prop-types';

import {IcInfoIcon} from  "Assets/svg";




const TextAreaField = props => {
  return (
    <div className="postJob__inputField">
      <Grid>
        <Grid.Row>
          <Grid.Column mobile={16} computer={4} textAlign="right">
            <div className="input__label">
              <Header as="h3">
                {props.label}
                <IcInfoIcon pathcolor="#c8c8c8"/>
              </Header>
            </div>
          </Grid.Column>
          <Grid.Column mobile={16} computer={12}>
            <TextArea placeholder={props.placeholder} />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  )
}

TextAreaField.propTypes = {
  label: PropTypes.string,
  placeholder: PropTypes.string
}

export default TextAreaField;