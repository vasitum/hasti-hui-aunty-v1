import React from 'react';
import { Grid, Input, Form, Header } from "semantic-ui-react";
import PropTypes from 'prop-types';

import { IcInfoIcon, IcDownArrowIcon } from "Assets/svg";

const options = [
  { key: 'm', text: 'Male', value: 'male' },
  { key: 'f', text: 'Female', value: 'female' },
]


const SelectOption = props => {
  return (
    <div className="postJob__inputField">
      <Grid>
        <Grid.Row>
          <Grid.Column mobile={16} computer={4} textAlign="right">
            <div className="input__label">
              <Header as="h3">
                {props.label}
                <IcInfoIcon pathcolor="#c8c8c8" />
              </Header>
            </div>
          </Grid.Column>
          <Grid.Column mobile={16} computer={12}>

            <div className="select__input">
              <Form.Select options={options} placeholder={props.placeholder} icon={false} fluid />
              <span className="downArrow__icon"><IcDownArrowIcon pathcolor="#c8c8c8" /></span>
            </div>


          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  )
}

SelectOption.propTypes = {
  label: PropTypes.string,
  placeholder: PropTypes.string
}

export default SelectOption;