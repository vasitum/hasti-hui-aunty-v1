import React from 'react';
import { Grid, Dropdown, Header } from "semantic-ui-react";
import PropTypes from 'prop-types';

import { IcInfoIcon, IcDownArrowIcon } from "Assets/svg";


// const options = [
//   { key: 'English', text: 'English', value: 'English' },
//   { key: 'French', text: 'French', value: 'French' },
//   { key: 'Spanish', text: 'Spanish', value: 'Spanish' },
//   { key: 'German', text: 'German', value: 'German' },
//   { key: 'Chinese', text: 'Chinese', value: 'Chinese' },
// ]

class InputTag extends React.Component{
  
  constructor(props){
    super(props);
    this.state = {options: {} }
  }

  handleAddition = (e, { value }) => {
    this.setState({
      options: [{ text: value, value }, ...this.state.options],
    })
  }

  handleChange = (e, { value }) => this.setState({ currentValues: value })

  render(){
    const { currentValues } = this.state

    // <CompoentName>

    return(
      <div className="postJob__inputField">
        <Grid>
          <Grid.Row>
            <Grid.Column mobile={16} computer={4} textAlign="right">
              <div className="input__label">
                <Header as="h3">
                  {this.props.label}
                  <IcInfoIcon pathcolor="#c8c8c8" />
                </Header>
              </div>
            </Grid.Column>
            <Grid.Column mobile={16} computer={12}>
              
              <div className="input__tag">
                {this.props.children}
              </div>
              
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
     
    )
  }
}

InputTag.propTypes = {
  label: PropTypes.string,
  // placeholder: PropTypes.string
}

export default InputTag;