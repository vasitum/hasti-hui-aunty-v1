import React from 'react';
import { Grid, Input, Form, Header} from "semantic-ui-react";
import PropTypes from 'prop-types';

import {IcInfoIcon} from  "Assets/svg";



const InputFeilds = props => {
  return (
    <div className="postJob__inputField">
      <Grid>
        <Grid.Row>
          <Grid.Column mobile={16} computer={4} textAlign="right">
            <div className="input__label">
              <Header as="h3">
                {props.label}
                <IcInfoIcon pathcolor="#c8c8c8"/>
              </Header>
            </div>
          </Grid.Column>
          <Grid.Column mobile={16} computer={12}>
            
            <Form.Field
              control={Input}
              type="text"
              placeholder={props.placeholder}
              required
              fluid
            />
      
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  )
}

InputFeilds.propTypes = {
  label: PropTypes.string,
  placeholder: PropTypes.string
}

export default InputFeilds;