import React from "react";
import { List, Button, Icon, Input, Image, Checkbox } from "semantic-ui-react";

import ReactGA from "react-ga";
import { USER } from "../../../constants/api";
import isLoggedIn from "../../../utils/env/isLoggedin";
import { HomePageString } from "../../EventStrings/Homepage";

import { Form } from "formsy-semantic-ui-react";

// import facebook from "../../../assets/img/facebook.png";
// import google from "../../../assets/img/google.png";
import InputField from "../../Forms/FormFields/InputField";
import IcProfileView from "../../../assets/svg/IcProfileView";
import IcHide from "../../../assets/svg/IcHide";
import enableGA from "../../../utils/env/enableGa";
import "./index.scss";

const UserId = window.localStorage.getItem(USER.UID);

export const UserSignUp = props => {
  const {
    data,
    onInputChange,
    onFormSubmit,
    onFormValid,
    onFormInValid,
    onShowPasswordClick,
    onForgetPasswordClick,
    onForgetClick,
    canSubmit,
    signup
  } = props;

  return (
    <div className="SearchSignIn">
      <List>
        {/* <List.Content className="padding-top-20 padding-bottom-20">
          <List.Description className="bgMagnolia paddingBestPanelSignIn border-r-5">
            <List horizontal className="padding-top-10 ">
              <List.Item as="a">
                <Image src={facebook} />
                <List.Content className="text-Matterhorn  facebook_icon">
                  Facebook
                </List.Content>
              </List.Item>
              <List.Item as="a" className="margin-left-30">
                <Image src={google} />
                <List.Content className="text-Matterhorn  google_icon">
                  Google
                </List.Content>
              </List.Item>
            </List>
          </List.Description>
        </List.Content> */}
        <List.Content className="padding-top-20">
          <Form
            onValidSubmit={onFormSubmit}
            onValid={onFormValid}
            onInvalid={onFormInValid}>
            <InputField
              type="email"
              placeholder="Email"
              value={data.email}
              onChange={onInputChange}
              name="email"
              validations="isEmail"
              validationErrors={{
                isEmail: "This is not a valid email",
                isDefaultRequiredValue:
                  "Please enter correct email 'Eg. user@gmail.com' "
              }}
              errorLabel={<span />}
              required
            />

            <div className="preview_Input">
              <InputField
                type={!data.showPassword ? "password" : "text"}
                placeholder="Password"
                value={data.password}
                onChange={onInputChange}
                name="password"
              />
              <div className="preview_Icon">
                <Button type="button" onClick={onShowPasswordClick}>
                  {data.showPassword ? (
                    <IcProfileView pathcolor="#c8c8c8" />
                  ) : (
                    <span
                      style={{
                        display: "inline-block",
                        marginTop: "0px"
                      }}>
                      <IcHide pathcolor="#c8c8c8" />
                    </span>
                  )}
                </Button>
              </div>
            </div>

            <Form.Field className={props.className}>
              <Checkbox
                className="checkbox-default"
                label="Keep me signed in on this device"
              />
            </Form.Field>

            <Button
              disabled={!canSubmit}
              type="submit"
              className={`bg-pacific-blue margin-top-24 text-white ${
                enableGA() ? "js__enable-track" : "no__ga"
              }`}
              fluid
              onClick={() => {
                ReactGA.event({
                  category: HomePageString.UserSignUpModal.category,
                  action: HomePageString.UserSignUpModal.action,
                  value: isLoggedIn() ? "" : UserId,
                });
              }}
              >
              {props.siginbtntext} <Icon name="arrow right" />
            </Button>

            {!signup ? (
              <div
                className="forgetTitle_footer"
                style={{
                  textAlign: "center",
                  marginTop: "20px",
                  cursor: "pointer"
                }}>
                <a onClick={onForgetClick}>Forgot Password</a>
              </div>
            ) : null}
          </Form>
        </List.Content>
      </List>
    </div>
  );
};

export default UserSignUp;
