import React from "react";
import { Grid, Container } from "semantic-ui-react";
import PublicProfileView from "../PreProfile/publicProfile";
import ProfileRightSide from "../PreProfile/profileRightSide";
import getUserById from "../../../api/user/getUserById";

import createProfileOnServer from "../../../api/user/createProfile";

import UserConstants from "../../../constants/storage/user";

import { toast } from "react-toastify";

import { USER } from "../../../constants/api";

import { uploadImage, uploadResume } from "../../../utils/aws";

import urlToBlob from "../../../utils/env/uriToBlob";
import ReactGA from "react-ga";
import { withRouter } from "react-router-dom";

import "../PreProfile/index.scss";
import "./index.scss";

import PageLoader from "../../PageLoader";
import PreviewHeaderNotification from "../PreviewHeaderNotification";

const Loader = props => (
  <div className="m_showbox">
    <div className="m_loader">
      <svg className="m_circular" viewBox="25 25 50 50">
        <circle
          className="m_path"
          cx="50"
          cy="50"
          r="20"
          fill="none"
          stroke-width="2"
          stroke-miterlimit="10"
        />
      </svg>
    </div>
  </div>
);

class PublicProfile extends React.Component {
  state = {
    isLoading: false,
    isProfileLoading: false,
    profileJson: {}
  };

  constructor(props) {
    super(props);
    this.onSaveClick = this.onSaveClick.bind(this);
  }

  componentDidMount() {
    const { match } = this.props;
    // console.log("check url",  match.url);
    ReactGA.pageview(`${match.url}`);
    try {
      const profileJsonString = window.localStorage.getItem(
        UserConstants.USER_PREVIEW_STATE
      );
      const profileJson = JSON.parse(profileJsonString);

      this.setState({
        profileJson: profileJson
      });
    } catch (error) {
      console.error(error);
    }
  }

  async onSaveClick(ev) {
    const {
      skillShowEditBtn,
      skillFieldCurrentValues,
      skillFieldOptions,
      bannerImage,
      bannerImageFile,
      profileImage,
      profileImageFile,
      resumeDocDataFile,
      resumeDocFile,
      resumeDoc,
      isProfileLoading,
      counter,
      canSubmit,
      ...userData
    } = this.state.profileJson;
    const baseJson = JSON.parse(window.localStorage.getItem(USER.BASE_JSON));
    const finalJson = Object.assign({}, baseJson, userData);
    // console.log(finalJson);

    this.setState({
      isProfileLoading: true
    });

    try {
      const res = await createProfileOnServer(finalJson);
      const data = await res.text();

      if (bannerImageFile) {
        let imgFile = urlToBlob(bannerImage);
        const imgBannerUpload = await uploadImage(
          `banner/${userData._id}/image.png`,
          imgFile
        );
      }

      if (resumeDocDataFile) {
        let resBlob = urlToBlob(resumeDocDataFile);
        const resUpload = await uploadResume(
          `${userData._id}/${userData.fName}_${userData.lName}.${
            userData.ext.resume
          }`,
          resBlob
        );
      }

      if (profileImage) {
        let imgFile = urlToBlob(profileImage);
        const profileImgUpload = await uploadImage(
          `${userData._id}/image.jpg`,
          imgFile
        );
      }

      if (data) {
        toast("Profile Successfully Created");
        this.props.history.push("/");
      }
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    if (this.state.isLoading) {
      return (
        <div>
          <Loader />
        </div>
      );
    } else {
      return (
        <div className="public-profile">
          <PreviewHeaderNotification publicTitle="profile" />
          <Container>
            <Grid>
              <Grid.Row>
                <Grid.Column mobile={16} computer={12}>
                  <PublicProfileView
                    user={this.state.profileJson}
                    isPreview
                    onSaveClick={this.onSaveClick}
                  />
                </Grid.Column>
                <Grid.Column width={4} className="mobile hidden">
                  <ProfileRightSide />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Container>
          <PageLoader
            active={this.state.isProfileLoading}
            loaderText={"Creating Profile ..."}
          />
        </div>
      );
    }
  }
}

export default withRouter(PublicProfile);
