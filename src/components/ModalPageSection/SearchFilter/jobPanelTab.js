import React from "react";
import { Grid } from "semantic-ui-react";
import SearchFilterPanel from "./SearchFilterPanel";
import JobPanel from "./jobPanel";
import { connect } from "react-redux";

import { Index, SearchBox, Pagination } from "react-instantsearch-dom";

import { withSearch } from "../../AlgoliaContainer";

import { withRouter } from "react-router-dom";
import PageAlgoliaLoading from "./PageAlgoliaLoading";
import UpdateJobCounter from "../banners/UpdateJobCounter";
import UpdatePeopleCounter from "../banners/UpdatePeopleCounter";
import { onInputChange } from "../../../actions/search";

import CandidateCVFilter from "../../Dashboard/CandidateDashbord/CandidateApplyedJobs/CandidateCVFilter";

import JobSection from "./JobSection";
import ReactGA from "react-ga";
import "./index.scss";

class JobPaneTab extends React.Component {
  constructor(props) {
    super(props);
  }

  /**
   * @TODO: Enable Tab State Functionality Back once we are done.
   */
  // componentDidMount() {
  //   if (
  //     this.state.searchState.query &&
  //     this.props.searchValue !== this.state.searchState.query
  //   ) {
  //     this.props.onChangeSearchText(this.state.searchState.query)
  //   }
  // }
  componentDidMount() {
    const { match } = this.props;
    // console.log("check url",  match.url)
    ReactGA.pageview(`${match.url}`);
  }
  
  render() {
    return (
      <div>
        <SearchBox style={{ display: "none" }} />
        <UpdateJobCounter />
        <Grid>
          <Grid.Row>
            <Grid.Column
              mobile={16}
              computer={4}
              className="mobile__SearchFilter"
              id="newSidebar">
              <SearchFilterPanel
                subTitle="jobs"
                // sendMessageTitle=""
                filterFor="Jobs"
                filters={["locCity", "skills", "comName", "jobType"]}
              />
              <Index indexName="job">
                <CandidateCVFilter
                  label={"Status"}
                  attribute="status"
                  defaultRefinement={true}
                  value={"Active"}
                  style={{
                    display: "none"
                  }}
                />
              </Index>
            </Grid.Column>
            <Grid.Column mobile={16} computer={12} className="mobile hidden">
              {/* <JobPanel /> */}
              <JobSection {...this.props} />
              <Pagination showLast />
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <Index indexName="user">
          <UpdatePeopleCounter />
        </Index>

        <PageAlgoliaLoading loaderText="Loading Jobs" />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    searchValue: state.search.currentValue
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onChangeSearchText: data => {
      dispatch(onInputChange(data));
    }
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(
    withSearch(JobPaneTab, {
      apiKey: process.env.REACT_APP_ALGOLIA_API_KEY,
      appId: process.env.REACT_APP_ALGOLIA_APP_ID,
      indexName: "job",
      syncNavSearch: true
    })
  )
);

// export default JobPaneTab;
