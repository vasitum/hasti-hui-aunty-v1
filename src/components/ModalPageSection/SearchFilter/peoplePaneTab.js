import React from "react";
import { Grid } from "semantic-ui-react";
import PeoplePane from "./peoplePanel";

import SearchFilterPanel from "./SearchFilterPanel";

import { connect } from "react-redux";

import { SearchBox, Pagination, Index } from "react-instantsearch-dom";
// import searchToObj from "../../../utils/env/searchToObj";
import { onInputChange } from "../../../actions/search";

import { withRouter } from "react-router-dom";

import PageAlgoliaLoading from "./PageAlgoliaLoading";

import { withSearch } from "../../AlgoliaContainer";
import ReactGA from "react-ga";
import UpdateJobCounter from "../banners/UpdateJobCounter";
import UpdatePeopleCounter from "../banners/UpdatePeopleCounter";
import PeopleSection from "./PeopleSection";
import CandidateCVFilter from "../../Dashboard/CandidateDashbord/CandidateApplyedJobs/CandidateCVFilter";

class PeoplePaneTab extends React.Component {
  constructor(props) {
    super(props);
  }

  /**
   *
   * Breaking Something idk if it's even broken
   */
  // componentDidMount() {
  //   if (
  //     this.state.searchState.query &&
  //     this.props.searchValue !== this.state.searchState.query
  //   ) {
  //     this.props.onChangeSearchText(this.state.searchState.query);
  //   }
  // }

  componentDidMount() {
    const { match } = this.props;
    // console.log("check url",  match.url)
    ReactGA.pageview(`${match.url}`);
  }
  

  render() {
    return (
      <div>
        <SearchBox style={{ display: "none" }} />
        <UpdatePeopleCounter />
        <Grid>
          <Grid.Row>
            <Grid.Column width={4} id="sidebar">
              <SearchFilterPanel
                subTitle="profiles"
                // sendMessageTitle="you!"
                filterFor="People"
                filters={[
                  "locCity",
                  "skills",
                  "com",
                  // "institute",
                  "edus",
                  "specs"
                ]}
              />
            </Grid.Column>
            <Grid.Column width={12} id="wrapper">
              {/* <PeoplePane /> */}
              <PeopleSection {...this.props} />
              <Pagination showLast />
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <PageAlgoliaLoading loaderText="Loading People" />
        <Index indexName="job">
          <CandidateCVFilter
            label={"Status"}
            attribute="status"
            defaultRefinement={true}
            value={"Active"}
            style={{
              display: "none"
            }}
          />
          <UpdateJobCounter />
        </Index>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    searchValue: state.search.currentValue
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onChangeSearchText: data => {
      dispatch(onInputChange(data));
    }
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(
    withSearch(PeoplePaneTab, {
      apiKey: process.env.REACT_APP_ALGOLIA_API_KEY,
      appId: process.env.REACT_APP_ALGOLIA_APP_ID,
      indexName: "user",
      syncNavSearch: true
    })
  )
);
