import React from "react";
import {
  Card,
  Input,
  Header,
  Item,
  Button,
  Icon,
  Accordion,
  Form,
  Menu,
  List,
  Checkbox,
  Label,
  Grid
} from "semantic-ui-react";

import {
  connectRefinementList,
  connectCurrentRefinements
} from "react-instantsearch-dom";

// import SearchBox from "./FilterInputBox";

import AlertSearch from "../../Cards/CardInputButton";
import CloseIcon from "../../../assets/svg/IcCloseIcon";

import "./index.scss";

import CardInputButton from "../../Cards/CardInputButton";

import "./index.scss";

// import SearchAlert

const SearchAlert = props => {
  const { subTitle, title, sendMessageTitle } = props;
  return (
    <div className="alert-search-filter mobile hidden">
      <CardInputButton
        label={`Email me ${subTitle} like this`}
        subTitle={subTitle}
        sendMessageTitle="you!"
      />
    </div>
  );
};

const FilterCount = props => {
  const { items } = props;
  let filterItems = items.filter(item => item.label !== "Status");
  return <span>{filterItems.length}</span>;
};

// const FilterHeader = props => {
class FilterHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  shouldRefine = mItems => {
    const { refine } = this.props;
    if (!mItems || !Array.isArray(mItems)) {
      return;
    }
    const filteredItems = mItems.filter(item => item.label !== "Status");
    if (filteredItems.length) {
      refine(filteredItems);
    }
  };

  render() {
    const { items, canRefine, filterFor } = this.props;
    // console.log("Items, ate", items.length);
    let active = items.length >= 1 ? "active" : "";
    return (
      <div className="">
        <List
          verticalAlign="middle"
          className="padding-top-30 isShowJobTitleDesktop">
          <List.Item
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              flexDirection: "row-reverse"
            }}>
            <List.Content>
              <Button
                className="FilterClearAllBtn vasitumBtnPaddin-0 btnBlue text-trolley-grey"
                basic
                onClick={() => this.shouldRefine(items)}
                disabled={!canRefine && items.length === 1}
                size="mini">
                Clear All (<FilterCountConnectedAlgolia />)
              </Button>
            </List.Content>
            <List.Content style={{ flex: "1" }}>
              <Header
                as="h2"
                className="text-night-rider font-w-500"
                style={{ fontSize: "22px" }}>
                Filter {filterFor}
              </Header>
            </List.Content>
          </List.Item>
        </List>

        <List
          verticalAlign="middle"
          className=" widescreen hidden large screen hidden computer hidden tablet hidden">
          <List.Item className="">
            <List.Content className="mobile__FilterHeader">
              <Grid>
                <Grid.Row>
                  <Grid.Column mobile={10}>
                    <Header as="h3" style={{ fontSize: "18px" }}>
                      Filter {filterFor}
                    </Header>
                  </Grid.Column>
                  <Grid.Column
                    mobile={6}
                    className="alingRight"
                    style={{
                      display: "flex",
                      justifyContent: "flex-end",
                      alignItems: "center"
                    }}>
                    <Button
                      className={`ApplyBtn ${active}`}
                      size="tiny"
                      onClick={this.props.isShowMobileFilterClose}>
                      Apply
                    </Button>

                    <Button
                      className="closeBtn"
                      onClick={this.props.isShowMobileFilterClose}>
                      <CloseIcon
                        width="14"
                        height="14"
                        pathcolor="#6e768a"
                        viewbox="0 0 14 14"
                      />
                    </Button>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content className="clearAllBtn">
              <Button
                size="small"
                className="clearFilterBtn"
                onClick={() => this.shouldRefine(items)}
                disabled={!canRefine && items.length === 1}>
                {/* <span className="padding-right-5">
                <CloseIcon
                  width="11"
                  height="11"
                  pathcolor="#0b9ed0"
                  viewbox="0 0 14 14"
                />
              </span> */}
                Clear all applied filters (<FilterCountConnectedAlgolia />)
              </Button>
            </List.Content>
          </List.Item>
        </List>
      </div>
    );
  }
}

const PanelCallBackHandler = class extends React.Component {
  componentWillMount() {
    if (this.context.setCanRefine) {
      this.context.setCanRefine(this.props.canRefine);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.context.setCanRefine &&
      this.props.canRefine !== nextProps.canRefine
    ) {
      this.context.setCanRefine(nextProps.canRefine);
    }
  }

  render() {
    return this.props.children;
  }
};

const FilterMain = class extends React.Component {
  state = {
    extended: false,
    query: "",
    filterIndexNew:
      this.props.attribute === "locCity" || this.props.attribute === "skills"
        ? true
        : false
  };

  onShowMoreClick = () => {
    this.setState(state => ({
      extended: !state.extended
    }));
  };

  getLimit = () => {
    const { limit, showMoreLimit } = this.props;
    const { extended } = this.state;
    return extended ? showMoreLimit : limit;
  };

  resetQuery = () => {
    this.setState({ query: "" });
  };

  handleClick = (e, titleProps) => {
    this.setState({
      // filterIndex: newIndex,
      filterIndexNew: !this.state.filterIndexNew
    });
  };

  renderItem = (item, resetQuery, isLocation) => {
    return this.props.renderItem(item, resetQuery, isLocation);
  };

  renderShowMore = () => {
    const { showMore } = this.props;
    const { extended } = this.state;
    const disabled = this.props.limit >= this.props.items.length;

    if (!showMore) {
      return null;
    }

    if (!disabled) {
      return (
        <Button
          disabled={disabled}
          basic
          compact
          className="SearchShowMoreBtn"
          onClick={this.onShowMoreClick}>
          <Icon name={!extended ? "plus" : "minus"} />
          {extended ? "Show Less" : "Show More"}
        </Button>
      );
    }
  };

  getFilterName = attribute => {
    switch (attribute) {
      case "loc":
      case "locCity":
        return "Location";
      case "skills":
        return "Skills";
      case "comName":
      case "com":
        return "Company";
      case "jobType":
        return "Job Type";
      case "edus":
        return "Course";
      case "specs":
        return "Specialization";
      case "institute":
        return "School";
      default:
        break;
    }
  };

  render() {
    const { filterIndex } = this.state;
    const { items, canRefine, attribute } = this.props;
    const filterName = this.getFilterName(attribute);
    const isLocation = filterName === "Location";

    return (
      <Accordion>
        <div className="">
          <Accordion.Title
            active={this.state.filterIndexNew}
            index={1}
            onClick={this.handleClick}>
            <List verticalAlign="middle">
              <List.Item>
                <Grid>
                  <Grid.Row>
                    <Grid.Column width={13}>
                      <List.Content className="filter_title">
                        <Header as="h3" className="text-Matterhorn">
                          {filterName}
                        </Header>
                      </List.Content>
                    </Grid.Column>
                    <Grid.Column width={3}>
                      <List.Content>
                        <span className="serarchCardIcon_circle">
                          <Icon
                            name="angle down"
                            className="rotetIcon"
                            floated="right"
                          />
                        </span>
                      </List.Content>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
                {/* <List.Content floated="right">
                  <Icon
                    name="angle down"
                    className="vasitumFilterDropdowmIcon"
                    circular
                    floated="right"
                  />
                </List.Content>
                <List.Content>
                  <Header as="h3" className="font-w-500 text-Matterhorn">
                    {filterName}
                  </Header>
                </List.Content> */}
              </List.Item>
            </List>
          </Accordion.Title>

          <Accordion.Content active={this.state.filterIndexNew}>
            <div className="jobSearchForm">
              <List style={{ marginBottom: "0px" }} verticalAlign="middle">
                {/* TODO: FIXME: Searchbox for List */}
                <List.Item
                  style={{
                    display: "none"
                  }}>
                  <List.Content
                    style={{
                      display: "none"
                    }}>
                    {/* <Input
                      placeholder={"Find " + filterName}
                      className="searchInput"
                      fluid
                    />
                     */}
                    {/* <SearchBox
                      currentRefinement={this.state.query}
                      refine={value => {
                        this.setState({ query: value });
                        this.props.searchForItems(value);
                      }}
                      focusShortcuts={[]}
                      onSubmit={e => {
                        e.preventDefault();
                        e.stopPropagation();
                        if (this.props.isFromSearch) {
                          this.props.selectItem(items[0], this.resetQuery);
                        }
                      }}
                    /> */}
                  </List.Content>
                </List.Item>

                {items.slice(0, this.getLimit()).map(item => {
                  return this.renderItem(item, this.resetQuery, isLocation);
                })}
              </List>

              {this.renderShowMore()}
            </div>
          </Accordion.Content>
        </div>
      </Accordion>
    );
  }
};

const FilterList = class extends React.Component {
  state = {
    query: ""
  };

  selectItem = (item, resetQuery) => {
    resetQuery();
    this.props.refine(item.value);
  };

  renderItem = (item, resetQuery, isLocation = false) => {
    return (
      <List.Item key={item.label}>
        <Grid>
          <Grid.Row>
            <Grid.Column width={13}>
              <List.Content>
                <Checkbox
                  onChange={() => this.selectItem(item, resetQuery)}
                  label={isLocation ? item.label.split(",")[0] : item.label}
                  checked={item.isRefined}
                />
              </List.Content>
            </Grid.Column>
            <Grid.Column width={3}>
              <List.Content>
                <Label size="large">{item.count.toLocaleString()}</Label>
              </List.Content>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </List.Item>
    );
  };

  render() {
    return (
      <FilterMain
        renderItem={this.renderItem}
        selectItem={this.selectItem}
        {...this.props}
        query={this.state.query}
      />
    );
  }
};

const FilterListConnect = props => (
  <PanelCallBackHandler {...props}>
    <FilterList {...props} />
  </PanelCallBackHandler>
);

const FilterHeaderConnected = props => (
  <PanelCallBackHandler {...props}>
    <FilterHeader {...props} />
  </PanelCallBackHandler>
);

const FilterCountConnected = props => (
  <PanelCallBackHandler {...props}>
    <FilterCount {...props} />
  </PanelCallBackHandler>
);

const FilterConnectAlgolia = connectRefinementList(FilterListConnect);
const FilterHeaderConnectAlgolia = connectCurrentRefinements(
  FilterHeaderConnected
);

const FilterCountConnectedAlgolia = connectCurrentRefinements(
  FilterCountConnected
);

class SearchFilterPanel extends React.Component {
  render() {
    const {
      filterFor,
      filters,
      subTitle,
      isShowMobileFilterClose,
      style
    } = this.props;

    return (
      <div className="search-filter-menu" style={style}>
        <SearchAlert subTitle={subTitle} />
        <div className="job-search-filter">
          <FilterHeaderConnectAlgolia
            filterFor={filterFor}
            isShowMobileFilterClose={isShowMobileFilterClose}
          />
          <div className="searchFilterAccordian">
            {filters.map(filter => (
              <FilterConnectAlgolia
                limit={5}
                showMoreLimit={20}
                showMore
                searchable
                attribute={filter}
              />
            ))}
          </div>
        </div>
      </div>
    );
  }
}

export default SearchFilterPanel;
