import React from "react";
import { Grid, Divider } from "semantic-ui-react";
import BestPaneLeft from "./bestPaneLeft";
import ReactGA from "react-ga";
import BestPane from "./bestPane";

class BestPanelTab extends React.Component {
  componentDidMount() {
    ReactGA.pageview('/search?query');
  }
  render() {
    return (
      <div className="best-panel-tab" style={{ paddingBottom: "30px" }}>
        <Grid>
          <Grid.Row>
            <Grid.Column width={4}>
              <BestPaneLeft />
            </Grid.Column>
            <Grid.Column width={12}>
              <BestPane />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}

export default BestPanelTab;
