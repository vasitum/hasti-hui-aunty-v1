import React from "react";
import {
  Image,
  Header,
  Feed,
  Accordion,
  List,
  Icon,
  Button,
  Grid,
  Card
} from "semantic-ui-react";
import Banner from "./banner";
import HeaderText from "../commonComponent/headerText";
import result_search from "../../../assets/img/result_search.png";
import profile from "../../../assets/img/profile.png";
import FoundPeopleBanner from "../banners/foundPeopleBanner";
import JobNoFound from "../../Banners/JobNoFound";
import IcNoCandidatesFound from "../../../assets/svg/IcNoCandidatesFound";
import SkillsComponent from "../commonComponent/skills";
import SummaryComponent from "../commonComponent/summary";
import EducationComponent from "../commonComponent/education";
import ExperianceComponent from "../commonComponent/experiance";
import { withRouter, Link } from "react-router-dom";
import SocialBtn from "../Buttons/socialBtn";
import UserLogo from "../../../assets/svg/IcUser";
import LocationLogo from "../../../assets/svg/IcLocation";
import CompanySvg from "../../../assets/svg/IcCompany";
import JobLogo from "../../../assets/svg/Icjob";
import isLoggedIn from "../../../utils/env/isLoggedin";
import UserAccessModal from "../UserAccessModal";
import { connectHits } from "react-instantsearch/connectors";
import { jobLike } from "../../../api/jobs/jobExtras";
import AllUser from "../../../api/messaging/userAllConversationList";
import fromNow from "../../../utils/env/fromNow";
import { USER } from "../../../constants/api";

// import allConversation from "../../../api/messaging/userAllConversationList";
// import qs from "qs";

import "./index.scss";

const PeopleConnected = class extends React.Component {
  state = {
    peopleIndex: -1
  };

  constructor(props) {
    super(props);
    this.onLikeClick = this.onLikeClick.bind(this);
    this.onChatStart = this.onChatStart.bind(this);
  }

  bestClick = (e, titleProps) => {
    const { index } = titleProps;
    const { peopleIndex } = this.state;
    const newIndex = peopleIndex === index ? -1 : index;
    this.setState({ peopleIndex: newIndex });
  };

  onViewJobClick = (ev, { toHref }) => {
    this.props.history.push("/user/public/" + toHref);
  };

  async onLikeClick(ev, { id }) {
    ev.stopPropagation();

    try {
      const res = await jobLike(id);
      const data = await res.text();

      // console.log(data);
    } catch (error) {
      console.error(error);
    }
  }

  async componentDidMount() {
    try {
      const res = await AllUser();
      const data = await res.text();

      // console.log(data);
    } catch (error) {
      console.error(error);
    }
  }

  onChatStart = (ev, { tohref }) => {
    const UserId = window.localStorage.getItem(USER.UID);
    const hrefLink = UserId === tohref ? `/messaging` : `/messaging/${tohref}`;
    this.props.history.push(hrefLink);
  };

  render() {
    const { peopleIndex } = this.state;
    const { hits } = this.props;
    const isLoggedInVal = isLoggedIn();

    if (!hits || hits.length === 0) {
      return (
        <div>
          <JobNoFound
            types="Candidates not"
            typeIcon={<IcNoCandidatesFound width="118" height="74" />}
          />
        </div>
      );
    }

    return (
      <Accordion>
        <Card fluid>
          <Card.Content>
            {hits.map((val, idx) => (
              <div
                className={
                  "accordion-panel " + (peopleIndex === idx ? "has-shadow" : "")
                }>
                <Accordion.Title
                  className=""
                  active={peopleIndex === idx}
                  index={idx}
                  onClick={this.bestClick}>
                  <div className="mobile__cardHeader search__cardHeader">
                    <Grid>
                      <Grid.Row>
                        <Grid.Column width={1}>
                          <List>
                            <List.Item>
                              <List.Content>
                                {/* imgExt */}
                                {!val.imgExt ? (
                                  <span className="userImgThumb">
                                    <UserLogo
                                      width="40"
                                      height="40"
                                      rectcolor="#f7f7fb"
                                      pathcolor="#c8c8c8"
                                    />
                                  </span>
                                ) : (
                                  <Image
                                    verticalAlign="top"
                                    style={{
                                      height: "40px",
                                      width: "40px",
                                      position: "absolute",
                                      borderRadius: "50%"
                                    }}
                                    src={`https://s3-us-west-2.amazonaws.com/img-mwf/${
                                      val.objectID
                                    }/image.jpg`}
                                  />
                                )}
                              </List.Content>
                            </List.Item>
                          </List>
                        </Grid.Column>
                        <Grid.Column width={8}>
                          <List className="padding-left-5 searchCard_headerLeft">
                            <List.Item>
                              <List.Content>
                                <List.Header>
                                  <Header
                                    className="textHoverHeadingBlue font-w-500 padding-bottom-4"
                                    as="h3"
                                    style={{
                                      display: "block",
                                      whiteSpace: "nowrap",
                                      overflow: "hidden",
                                      textOverflow: "ellipsis",
                                      width: "340px"
                                    }}>
                                    <Link to={"/user/public/" + val.objectID}>
                                      {val.fName} {val.lName}
                                    </Link>
                                  </Header>
                                  <p
                                    className="margin-bottom-0 padding-bottom-4 vasitumFontOpen-500 text-night-rider"
                                    style={{ fontSize: "14px" }}>
                                    {val.title}
                                  </p>
                                </List.Header>
                                <List.Description>
                                  <p
                                    style={{
                                      display: !val.locCity ? "none" : "block"
                                    }}
                                    className="vasitumFontOpen-500 margin-bottom-0 header_location padding-bottom-5 text-Matterhorn">
                                    <span className="padding-right-6">
                                      <LocationLogo
                                        width="9.5"
                                        height="11.5"
                                        pathcolor="#0bd0bb"
                                      />
                                    </span>
                                    {val.locCity}
                                    {val.locCountry
                                      ? ", " + val.locCountry
                                      : ""}
                                  </p>
                                  {val.com ? (
                                    <p className="padding-bottom-5 headerExperiance padding-bottom-4 vasitumFontOpen-500">
                                      <span className="padding-right-10 text-nobel ">
                                        <span>
                                          <JobLogo
                                            width="14.25"
                                            height="11.5"
                                            pathcolor="#0B9ED0"
                                          />
                                        </span>
                                        <span className="vasitumFontOpen-500 text-Matterhorn">
                                          {" "}
                                          {val.com}
                                        </span>
                                      </span>
                                    </p>
                                  ) : null}
                                  <p className="padding-bottom-5 currentDesignation padding-bottom-4 vasitumFontOpen-500">
                                    {val.com ? (
                                      <span className="padding-right-10 text-trolley-grey">
                                        <span>
                                          <JobLogo
                                            width="14.25"
                                            height="11.5"
                                            pathcolor="#0B9ED0"
                                          />
                                        </span>
                                        <span className="vasitumFontOpen-500">
                                          {" "}
                                          {val.com}
                                        </span>
                                      </span>
                                    ) : null}
                                    {/* {val.designation ? (
                                      <span className="text-trolley-grey">
                                        Designation:{" "}
                                        <span className=" text-pacific-blue">
                                          {" "}
                                          {val.designation}
                                        </span>
                                      </span>
                                    ) : null} */}
                                  </p>
                                </List.Description>
                                <List.Content
                                  style={{
                                    display:
                                      !val.skills || !val.skills.length
                                        ? "none"
                                        : "block"
                                  }}
                                  className="padding-top-20 skillsDisplay filterSkill">
                                  <List
                                    bulleted
                                    horizontal
                                    className="text-night-rider">
                                    {(() => {
                                      if (!val.skills) {
                                        return null;
                                      }

                                      const ListKeys = val.skills.map(
                                        (skill, idx) => {
                                          if (idx === 0) {
                                            return (
                                              <List.Item>
                                                <span className="text-nobel">
                                                  Skills:
                                                </span>{" "}
                                                {skill}
                                              </List.Item>
                                            );
                                          }

                                          return <List.Item>{skill}</List.Item>;
                                        }
                                      );

                                      return ListKeys;
                                    })()}
                                  </List>
                                </List.Content>
                              </List.Content>
                            </List.Item>
                          </List>
                        </Grid.Column>
                        <Grid.Column width={7}>
                          <List>
                            <List.Item className="serarchCard_leftHeader">
                              <List.Content className="alingCenterRight">
                                <List.Header className="serarchCard_leftHeaderItem">
                                  <SocialBtn
                                    objectID={val.objectID}
                                    onLikeClick={this.onLikeClick}
                                  />

                                  <span className="leftHeader_jobPostDay  vasitumFontOpen-500 padding-right-10 text-nobel fontSize-12">
                                    Updated:{" "}
                                    <span className="text-pacific-blue">
                                      {fromNow(val.uTime)}
                                    </span>{" "}
                                  </span>

                                  <span className="serarchCardIcon_circle">
                                    <Icon
                                      name="angle down"
                                      className="rotetIcon"
                                    />
                                  </span>
                                </List.Header>
                                <List.Description className="padding-top-10 alingCenterRight">
                                  {!isLoggedInVal ? (
                                    <Button
                                      size="mini"
                                      tohref={val.objectID}
                                      onClick={this.onChatStart}
                                      className="actionBtn margin-right-45">
                                      {" "}
                                      Message <Icon name="angle right" />
                                    </Button>
                                  ) : (
                                    <UserAccessModal
                                      modalTrigger={
                                        <Button
                                          size="mini"
                                          className="actionBtn margin-right-45">
                                          {" "}
                                          Message <Icon name="angle right" />
                                        </Button>
                                      }
                                      isAction
                                      actionText="Message"
                                    />
                                  )}
                                  {/* <Button
                                    size="mini"
                                    onClick={() => {
                                      alert("Messaging is WIP");
                                    }}
                                    className="actionBtn margin-right-45">
                                    {" "}
                                    Message <Icon name="angle right" />
                                  </Button> */}
                                </List.Description>
                              </List.Content>
                            </List.Item>
                          </List>
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                  </div>
                </Accordion.Title>

                <Accordion.Content active={peopleIndex === idx} className="">
                  <div className="accordian-body padding-top-20  padding-bottom-20">
                    <div className="searchCard_divider" />
                    <Grid>
                      <Grid.Row>
                        <Grid.Column width={1} />

                        <Grid.Column width={15}>
                          <List
                            verticalAlign="middle"
                            className="padding-left-20 padding-right-20">
                            {/* Summary */}
                            <SummaryComponent val={val} />

                            {/* Skills */}
                            <SkillsComponent val={val} />

                            {/* Expericen */}
                            <ExperianceComponent val={val} />

                            {/* Education */}
                            <EducationComponent val={val} />

                            {/* Footer */}

                            <List.Item className="cardFooter_IconArrow">
                              <List.Content>
                                <List.Header>
                                  <Button
                                    toHref={val.objectID}
                                    onClick={this.onViewJobClick}
                                    size="mini"
                                    className="b-r-30 bg-munsell btnBlue_Text fontSize-12 text-night-rider">
                                    View full profile
                                  </Button>
                                </List.Header>
                              </List.Content>
                              <List.Content className="alingCenterRight">
                                <List.Header>
                                  <SocialBtn
                                    objectID={val.objectID}
                                    onLikeClick={this.onLikeClick}
                                  />

                                  {!isLoggedInVal ? (
                                    <Button
                                      size="mini"
                                      style={{ width: 104 }}
                                      className="actionBtn LightCyanBgBlue">
                                      Message
                                      <Icon name="angle right" />
                                    </Button>
                                  ) : (
                                    <UserAccessModal
                                      modalTrigger={
                                        <Button
                                          size="mini"
                                          style={{ width: 104 }}
                                          className="actionBtn LightCyanBgBlue">
                                          Message
                                          <Icon name="angle right" />
                                        </Button>
                                      }
                                      isAction
                                      actionText="Message"
                                    />
                                  )}
                                  <Button
                                    onClick={this.bestClick}
                                    className="closeIcon_footer">
                                    <Icon name="angle up" />
                                  </Button>
                                </List.Header>
                              </List.Content>
                            </List.Item>
                          </List>
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                  </div>
                </Accordion.Content>
              </div>
            ))}
          </Card.Content>
        </Card>
      </Accordion>
    );
  }
};

const PeopleConnectedAlgolia = connectHits(PeopleConnected);

class PeoplePane extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      peopleIndex: 0
    };
  }

  render() {
    const { peopleIndex } = this.state;
    return (
      <div className="people-pane">
        <div className="foundPeopleBanner">
          <FoundPeopleBanner />
        </div>
        <div className="searchFilterCard" style={{ backgroundColor: "white" }}>
          <PeopleConnectedAlgolia history={this.props.history} />
        </div>
      </div>
    );
  }
}

export default withRouter(PeoplePane);
// export default PeoplePane;
