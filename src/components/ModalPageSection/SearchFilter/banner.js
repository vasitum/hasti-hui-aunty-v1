import React from 'react';
import { Image, Header, List , Icon , Button, Dropdown } from 'semantic-ui-react';
import RecentDropDown from '../dropDowns/recentDropdown';




class Banner extends React.Component{
  render(){
    return(
      <List  verticalAlign='middle'>
        <List.Item>
          <List.Content floated='right'>
            <List.Header className="padding-bottom-10">
              <Button size='mini' className="b-r-30 text-night-rider">
                Sign up and post job
              </Button>
            </List.Header>
            <List.Description>

              <RecentDropDown />
              
            </List.Description> 
          </List.Content>
          
          
          <List.Content floated='left'>
            <List.Header>
              <Header className="white-text" as="h3">Found 120 Jobs</Header>
            </List.Header>
            <List.Description>
              <p className="margin-bottom-0 text-white font-size-13">
                Post jobs with Vasitum’s free personalized assistance to double your hiring efficiency.
              </p>
            </List.Description>
          </List.Content>
        </List.Item> 
      </List>
    )
  }
}

export default Banner;