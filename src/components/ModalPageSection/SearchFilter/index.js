import React from "react";
import { Container, Grid, Image, Responsive } from "semantic-ui-react";
import SearchResult from "./searchResult";

import search_banner from "../../../assets/img/search_banner.png";
import bestBanner from "../../../assets/img/searchFilter/bestBanner.png";

import MobileRearchReasult from "../moblieComponents/mobileTab";
import "../style/global/_globals.scss";
import "../style/components/_searchFilter.scss";

// import UserSignUp from '../forms/userSignUp'
// import PostJob from '../postLogin/PostJob';

class SearchFilter extends React.Component {
  render() {
    // console.log("SearchFilter_Page", this.props.sidebarToggle);

    return (
      <div className="search-filter-page">
        <Responsive {...Responsive.onlyMobile}>
          <MobileRearchReasult sidebarToggle={this.props.sidebarToggle} />
        </Responsive>
        <Responsive minWidth={Responsive.onlyTablet.minWidth}>
          <div className="search-filter-banner">
            {/* <Image src={bestBanner} fluid/> */}
          </div>
          <Container>
            <SearchResult />
          </Container>
        </Responsive>
      </div>
    );
  }
}

export default SearchFilter;
