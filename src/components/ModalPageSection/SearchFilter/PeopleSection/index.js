import React from "react";
import { withHits } from "../../../AlgoliaContainer";
import FoundPeopleBanner from "../../banners/foundPeopleBanner";
import JobNoFound from "../../../Banners/JobNoFound";
import IcNoCandidatesFound from "../../../../assets/svg/IcNoCandidatesFound";
import PeopleSectionCard from "./PeopleSectionCard";
import isLoggedOut from "../../../../utils/env/isLoggedin";
import peopleBanner from "../../../../assets/img/searchFilter/foundPeople.png";

class PeopleSection extends React.Component {
  render() {
    const { hits } = this.props;
    const loggedOut = isLoggedOut();
    if (!hits || hits.length === 0) {
      return (
        <div className="people-pane">
          <div
            style={{
              backgroundImage: `url(${peopleBanner});`
            }}
            className="foundPeopleBanner">
            <FoundPeopleBanner />
          </div>
          <div
            className="searchFilterCard"
            style={{ backgroundColor: "white" }}>
            <div>
              <JobNoFound
                types="Candidates not"
                typeIcon={<IcNoCandidatesFound width="118" height="74"/>}
              />
            </div>
          </div>
        </div>
      );
    }

    // map 'em all
    return (
      <div className="people-pane">
        <div className="foundPeopleBanner">
          <FoundPeopleBanner />
        </div>
        <div className="searchFilterCard" style={{ backgroundColor: "white" }}>
          {hits.map(val => {
            return (
              <PeopleSectionCard
                key={val.objectID}
                val={val}
                loggedOut={loggedOut}
              />
            );
          })}
        </div>
      </div>
    );
  }
}

export default withHits(PeopleSection);
