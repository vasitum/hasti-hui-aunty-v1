import React from "react";
import {
  Accordion,
  Button,
  Card,
  Grid,
  Icon,
  List,
  Image,
  Header
} from "semantic-ui-react";

import { Link } from "react-router-dom";
import SocialBtn from "../../../Buttons/socialBtn";
import isLoggedInVal from "../../../../../utils/env/isLoggedin";
import UserAccessModal from "../../../UserAccessModal";

import fromNow from "../../../../../utils/env/fromNow";

import UserLogo from "../../../../../assets/svg/IcUser";
import LocationLogo from "../../../../../assets/svg/IcLocation";
import JobLogo from "../../../../../assets/svg/Icjob";

import SkillsComponent from "../../../commonComponent/skills";
import SummaryComponent from "../../../commonComponent/summary";
import EducationComponent from "../../../commonComponent/education";
import ExperianceComponent from "../../../commonComponent/experiance";
import {
  jobLike,
  getLikedAndSavedJobs
} from "../../../../../api/jobs/jobExtras";

import "./index.scss";
import P2PTracker from "../../../../P2PTracker";
import SkillsCount from "../../SkillsCount";
import { USER } from "../../../../../constants/api";

class PeopleSectionCard extends React.Component {
  state = {
    active: false,
    liked: false,
    saved: false,
    applied: false,
    apiDone: false
  };

  constructor(props) {
    super(props);

    //autobind
    this.onLikeClick = this.onLikeClick.bind(this);
  }

  /**
   * check for like, save and
   */
  async componentDidMount() {
    if (this.props.loggedOut) {
      return;
    }

    const { val } = this.props;

    try {
      const res = await getLikedAndSavedJobs([val.objectID], "user");
      if (res.status === 200) {
        const data = await res.json();
        const dataVal = data[val.objectID];
        this.setState({
          liked: dataVal.like,
          saved: dataVal.saveJobs,
          applied: dataVal.applied,
          apiDone: true
        });
      } else {
        // console.log(res);
        this.setState({
          apiDone: true
        });
      }
    } catch (error) {
      console.error(error);
      this.setState({
        apiDone: true
      });
    }
  }

  onClick = e => {
    this.setState({
      active: !this.state.active
    });
  };

  async onLikeClick(ev, { id }) {
    ev.stopPropagation();

    try {
      const res = await jobLike(id, "user");
      const data = await res.text();

      // console.log(data);
    } catch (error) {
      console.error(error);
    }
  }

  onChatStart = e => {};

  onViewJobClick = e => {};

  render() {
    const { val } = this.props;
    const { active } = this.state;
    const UserId = window.localStorage.getItem(USER.UID);

    if (val.objectID === window.localStorage.getItem(USER.UID)){
      return null
    }
    return (
      <Accordion className="PeopleSectionCard">
        <Card fluid>
          <Card.Content>
            <div className={"accordion-panel " + (active ? "has-shadow" : "")}>
              <Accordion.Title active={active} onClick={this.onClick}>
                <div className="mobile__cardHeader search__cardHeader">
                  <Grid>
                    <Grid.Row>
                      <Grid.Column width={1}>
                        <List>
                          <List.Item>
                            <List.Content>
                              {/* imgExt */}
                              {!val.imgExt ? (
                                <span className="userImgThumb">
                                  <UserLogo
                                    width="40"
                                    height="40"
                                    rectcolor="#f7f7fb"
                                    pathcolor="#c8c8c8"
                                  />
                                </span>
                              ) : (
                                <Image
                                  verticalAlign="top"
                                  style={{
                                    height: "40px",
                                    width: "40px",
                                    position: "absolute",
                                    borderRadius: "50%"
                                  }}
                                  src={`https://s3-us-west-2.amazonaws.com/img-mwf/${
                                    val.objectID
                                  }/image.jpg`}
                                  alt={val.fName}
                                />
                              )}
                            </List.Content>
                          </List.Item>
                        </List>
                      </Grid.Column>
                      <Grid.Column width={10}>
                        <List className="padding-left-5 searchCard_headerLeft">
                          <List.Item>
                            <List.Content>
                              <List.Header>
                                <Header
                                  className="textHoverHeadingBlue font-w-500 padding-bottom-4"
                                  as="h2"
                                  style={{
                                    display: "block",
                                    whiteSpace: "nowrap",
                                    overflow: "hidden",
                                    textOverflow: "ellipsis",
                                    width: "340px"
                                  }}>
                                  <Link to={"/view/user/" + val.objectID}>
                                    {val.fName} {val.lName}
                                  </Link>
                                </Header>
                                <p
                                  className="margin-bottom-0 padding-bottom-4 vasitumFontOpen-500 text-night-rider"
                                  style={{ fontSize: "14px" }}>
                                  {val.title}
                                </p>
                              </List.Header>
                              <List.Description>
                                <p
                                  style={{
                                    display: !val.locCity ? "none" : "block"
                                  }}
                                  className="vasitumFontOpen-500 margin-bottom-0 header_location padding-bottom-5 text-Matterhorn">
                                  <span className="padding-right-6">
                                    <LocationLogo
                                      width="9.5"
                                      height="11.5"
                                      pathcolor="#0bd0bb"
                                    />
                                  </span>
                                  {val.locCity}
                                  {val.locCountry ? ", " + val.locCountry : ""}
                                </p>
                                {val.com ? (
                                  <p className="padding-bottom-5 headerExperiance padding-bottom-4 vasitumFontOpen-500">
                                    <span className="padding-right-10 text-nobel ">
                                      <span>
                                        <JobLogo
                                          width="14.25"
                                          height="11.5"
                                          pathcolor="#0B9ED0"
                                        />
                                      </span>
                                      <span className="vasitumFontOpen-500 text-Matterhorn">
                                        {" "}
                                        {val.com}
                                      </span>
                                    </span>
                                  </p>
                                ) : null}
                                <p className="padding-bottom-5 currentDesignation padding-bottom-4 vasitumFontOpen-500">
                                  {val.com ? (
                                    <span className="padding-right-10 text-trolley-grey">
                                      <span>
                                        <JobLogo
                                          width="14.25"
                                          height="11.5"
                                          pathcolor="#0B9ED0"
                                        />
                                      </span>
                                      <span className="vasitumFontOpen-500">
                                        {" "}
                                        {val.com}
                                      </span>
                                    </span>
                                  ) : null}
                                  {/* {val.designation ? (
                                    <span className="text-trolley-grey">
                                      Designation:{" "}
                                      <span className=" text-pacific-blue">
                                        {" "}
                                        {val.designation}
                                      </span>
                                    </span>
                                  ) : null} */}
                                </p>
                              </List.Description>
                            </List.Content>
                          </List.Item>
                        </List>
                      </Grid.Column>
                      <Grid.Column width={5}>
                        <List>
                          <List.Item className="serarchCard_leftHeader">
                            <List.Content className="alingCenterRight">
                              <List.Header className="serarchCard_leftHeaderItem">
                                {/* {this.state.apiDone ? (
                                  <SocialBtn
                                    objectID={val.objectID}
                                    onLikeClick={this.onLikeClick}
                                    liked={this.state.liked}
                                    saved={this.state.saved}
                                    type={"user"}
                                  />
                                ) : null} */}

                                <span className="leftHeader_jobPostDay  vasitumFontOpen-500 padding-right-10 text-nobel fontSize-12">
                                  Updated:{" "}
                                  <span className="text-pacific-blue">
                                    {fromNow(val.uTime)}
                                  </span>{" "}
                                </span>

                                <span className="serarchCardIcon_circle">
                                  <Icon
                                    name="angle down"
                                    className="rotetIcon"
                                  />
                                </span>
                              </List.Header>
                              <List.Description className="padding-top-10 alingCenterRight">
                                {!isLoggedInVal() ? (
                                  <React.Fragment>
                                    <Button
                                      as={Link}
                                      size="mini"
                                      // to={`/messaging/${val.objectID}`}
                                      to={
                                        UserId === val.objectID
                                          ? `/messaging`
                                          : `/messaging/${val.objectID}`
                                      }
                                      className="actionBtn margin-right-45">
                                      {" "}
                                      Message <Icon name="angle right" />
                                    </Button>
                                    <div style={{ marginRight: "70px" }}>
                                      <P2PTracker
                                        cardType={"icon"}
                                        reqId={window.localStorage.getItem(
                                          USER.UID
                                        )}
                                        userId={val.objectID}
                                      />
                                    </div>
                                  </React.Fragment>
                                ) : (
                                  <UserAccessModal
                                    modalTrigger={
                                      <Button
                                        size="mini"
                                        className="actionBtn margin-right-45">
                                        {" "}
                                        Message <Icon name="angle right" />
                                      </Button>
                                    }
                                    isAction
                                    // actionText="Message"
                                  />
                                )}
                              </List.Description>
                            </List.Content>
                          </List.Item>
                        </List>
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                  <div className="PeopleCard_accordianTitleSkill">
                    <div className="left_skillSpace" />
                    <div
                      style={{
                        display:
                          !val.skills || !val.skills.length ? "none" : "block"
                      }}
                      className="padding-top-12 skillsDisplay filterSkill">
                      {/* Skills */}
                      <SkillsCount val={val} />
                    </div>
                  </div>
                </div>
              </Accordion.Title>

              <Accordion.Content active={active} className="">
                <div className="accordian-body padding-top-20  padding-bottom-20">
                  <div className="searchCard_divider" />
                  <Grid>
                    <Grid.Row>
                      <Grid.Column width={1} />

                      <Grid.Column width={15}>
                        <List
                          verticalAlign="middle"
                          className="padding-left-20 padding-right-20">
                          {/* Summary */}
                          <SummaryComponent val={val} />

                          {/* Skills */}
                          <SkillsComponent val={val} />

                          {/* Expericen */}
                          <ExperianceComponent val={val} />

                          {/* Education */}
                          <EducationComponent val={val} />

                          {/* Footer */}

                          <List.Item className="cardFooter_IconArrow">
                            <List.Content>
                              <List.Header>
                                <Button
                                  // toHref={val.objectID}
                                  // onClick={this.onViewJobClick}
                                  as={Link}
                                  to={`/view/user/${val.objectID}`}
                                  size="mini"
                                  className="b-r-30 bg-munsell btnBlue_Text fontSize-12 text-night-rider">
                                  View full profile
                                </Button>
                              </List.Header>
                            </List.Content>
                            {/* <List.Content className="alingCenterRight">
                              <List.Header>
                                {this.state.apiDone ? (
                                  <SocialBtn
                                    objectID={val.objectID}
                                    onLikeClick={this.onLikeClick}
                                    liked={this.state.liked}
                                    saved={this.state.saved}
                                    type={"user"}
                                  />
                                ) : null}

                                {!isLoggedInVal() ? (
                                  <Button
                                    as={Link}
                                    to={`/messaging/${val.objectID}`}
                                    size="mini"
                                    style={{ width: 104 }}
                                    className="actionBtn LightCyanBgBlue">
                                    Message
                                    <Icon name="angle right" />
                                  </Button>
                                ) : (
                                  <UserAccessModal
                                    modalTrigger={
                                      <Button
                                        size="mini"
                                        style={{ width: 104 }}
                                        className="actionBtn LightCyanBgBlue">
                                        Message
                                        <Icon name="angle right" />
                                      </Button>
                                    }
                                    isAction
                                    actionText="Message"
                                  />
                                )}
                                <Button
                                  onClick={this.onClick}
                                  className="closeIcon_footer">
                                  <Icon name="angle up" />
                                </Button>
                              </List.Header>
                            </List.Content> */}
                          </List.Item>
                        </List>
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                </div>
              </Accordion.Content>
            </div>
          </Card.Content>
        </Card>
      </Accordion>
    );
  }
}

export default PeopleSectionCard;
