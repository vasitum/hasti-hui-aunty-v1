import React from "react";
import { Accordion, Button, Card, Grid, Icon, List } from "semantic-ui-react";
import SocialBtn from "../../../Buttons/socialBtn";
import QuillText from "../../../../CardElements/QuillText";
import SearchCardHeader from "../../../commonComponent/searchCardHeader";
import SkillsComponent from "../../../commonComponent/skills";
import InfoSectionGridHeader from "../../../PreProfile/InfoSectionGridHeader";
import UserAccessModal from "../../../UserAccessModal";
import isLoggedIn from "../../../../../utils/env/isLoggedin";
import {
  jobLike,
  jobApply,
  getLikedAndSavedJobs
} from "../../../../../api/jobs/jobExtras";

import { Link } from "react-router-dom";
import { USER } from "../../../../../constants/api";
import P2PTracker from "../../../../P2PTracker";
import SkillsCount from "../../SkillsCount";
import isProfileCreated from "../../../../../utils/env/isProfileCreated";

// TODO: Handle Like / Save / Apply Section
class JobSectionCard extends React.Component {
  state = {
    active: false,
    liked: false,
    saved: false,
    applied: false,
    apiDone: false
  };

  constructor(props) {
    super(props);
    this.onLikeClick = this.onLikeClick.bind(this);
    this.onApplyClick = this.onApplyClick.bind(this);
    this.USERID = window.localStorage.getItem(USER.UID);
  }

  onClick = e => {
    this.setState({
      active: !this.state.active
    });
  };

  async componentDidMount() {
    const { val } = this.props;
    try {
      const res = await getLikedAndSavedJobs([val.objectID], "job");
      if (res.status === 200) {
        const data = await res.json();
        const dataVal = data[val.objectID];
        this.setState({
          liked: dataVal.like,
          saved: dataVal.saveJobs,
          applied: dataVal.applied,
          apiDone: true
        });
      } else {
        console.log(
          "Unable to find liked and saved details for ID",
          val.objectID
        );
        this.setState({
          apiDone: true
        });
      }
    } catch (error) {
      console.error(error);
      this.setState({
        apiDone: true
      });
    }
  }

  async onLikeClick(e) {
    const { val } = this.props;

    if (!isProfileCreated()) {
      return;
    }

    try {
      const res = await jobLike(val.objectID, "job");
      if (res.status === 200) {
        const data = await res.text();
        // console.log(data);
      } else {
        console.log("Unable to like entity", val.objectID);
      }
    } catch (error) {
      console.error(error);
    }
  }

  async onApplyClick(e) {
    const { val } = this.props;

    if (!isProfileCreated()) {
      return;
    }

    try {
      const res = await jobApply(val.objectID, val.title);
      if (res.status === 200) {
        this.setState({
          applied: true
        });
      } else {
        console.log("Unable to apply to job", val.objectID);
      }
    } catch (error) {
      console.error(error);
    }
  }

  onViewJobClick = e => {};

  render() {
    const { val } = this.props;
    const { active } = this.state;
    // Backwards Compat for quill
    let jsonData = "";
    try {
      jsonData = JSON.parse(val.desc);
    } catch (error) {
      jsonData = val.desc;
    }
    const isLoggedInVal = isLoggedIn();
    
    if (val.objectID === window.localStorage.getItem(USER.UID)){
      return null
    }

    return (
      <Accordion>
        <Card fluid>
          <Card.Content>
            <div
              className={"accordion-panel " + (active ? "has-shadow" : "")}
              key={val.objectID}>
              <Accordion.Title
                active={active}
                onClick={this.onClick}
                style={{ padding: "0" }}>
                <SearchCardHeader
                  disabled={this.USERID === val.userId}
                  onLikeClick={this.onLikeClick}
                  onApplyClick={this.onApplyClick}
                  liked={this.state.liked}
                  saved={this.state.saved}
                  applied={this.state.applied}
                  apiDone={this.state.apiDone}
                  objectID={val.objectID}
                  val={val}
                  likesArray={this.state.likes}
                  headerlink="/view/job/"
                />

                {/* only Desktop */}
                <div className="" style={{ padding: "15px 27px" }}>
                  <Grid>
                    <Grid.Row className="padding-top-5">
                      <Grid.Column width={1} />

                      <Grid.Column width={15}>
                        <List className="mobile hidden">
                          <List.Item className="experienceDisplay">
                            <List.Content>
                              <List.Description>
                                <p className="margin-bottom-0 vasitumFontOpen-500 text-trolley-grey">
                                  <span
                                    className="padding-right-20 text-grey"
                                    style={{
                                      display:
                                        (!val.expMin && !val.expMax) || active
                                          ? "none"
                                          : "block"
                                    }}>
                                    Experience:{" "}
                                    <span className="text-pacific-blue">
                                      {" "}
                                      {val.expMin} - {val.expMax} yrs
                                    </span>
                                  </span>

                                  <span className="padding-right-20 text-grey">
                                    Job type:{" "}
                                    <span className="text-pacific-blue">
                                      {" "}
                                      {val.jobType}{" "}
                                    </span>
                                  </span>

                                  <span className="padding-right-20 text-grey">
                                    Job role:{" "}
                                    <span className="text-pacific-blue">
                                      {" "}
                                      {val.role}{" "}
                                    </span>
                                  </span>
                                </p>
                              </List.Description>
                            </List.Content>
                          </List.Item>

                          <List.Item className="skillsDisplay">
                            <List.Content className="filterSkill">
                              {/* Skills */}
                              <SkillsCount val={val} />
                            </List.Content>
                          </List.Item>
                        </List>
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                </div>

                {/* only Desktop end */}
              </Accordion.Title>

              <Accordion.Content active={active}>
                <div className="accordian-body padding-top-10 padding-bottom-20">
                  <div className="searchCard_divider" />
                  <Grid>
                    <Grid.Row>
                      <Grid.Column width={1} />
                      <Grid.Column width={15}>
                        <List
                          verticalAlign="middle"
                          className="padding-left-20 padding-right-20">
                          <InfoSectionGridHeader headerText="Job Description">
                            <QuillText value={jsonData} readOnly />
                          </InfoSectionGridHeader>
                          {/* <JobDescription jsonData={jsonData} /> */}

                          <SkillsComponent val={val} />

                          <List.Item className="cardFooter_IconArrow">
                            <List.Content>
                              <List.Header>
                                {val.externalLink ? (
                                  <Button
                                    size="mini"
                                    as={Link}
                                    to={`/view/job/${val.objectID}`}
                                    className="b-r-30 bg-munsell btnBlue_Text fontSize-12 text-night-rider">
                                    View complete job
                                  </Button>
                                ) : (
                                  <Button
                                    size="mini"
                                    as={Link}
                                    to={`/view/job/${val.objectID}`}
                                    className="b-r-30 bg-munsell btnBlue_Text fontSize-12 text-night-rider">
                                    View complete job
                                  </Button>
                                )}
                              </List.Header>
                            </List.Content>
                            {/* <List.Content
                              className="alingCenterRight"
                              style={{ marginTop: "20px" }}>
                              <List.Header>
                                {this.state.apiDone ? (
                                  <SocialBtn
                                    objectID={val.objectID}
                                    onLikeClick={this.onLikeClick}
                                    liked={this.state.liked}
                                    saved={this.state.saved}
                                    type="job"
                                  />
                                ) : null}

                                {!isLoggedInVal ? (
                                  this.state.applied ? (
                                    <Button
                                      style={{
                                        marginRight: "37px"
                                      }}
                                      id={val.objectID}
                                      title={val.title}
                                      size="mini"
                                      className="actionBtn">
                                      {" "}
                                      Applied <Icon name="angle right" />
                                    </Button>
                                  ) : (
                                    <Button
                                      style={{
                                        marginRight: "37px"
                                      }}
                                      id={val.objectID}
                                      title={val.title}
                                      size="mini"
                                      className="actionBtn"
                                      onClick={this.onApplyClick}>
                                      {" "}
                                      Apply <Icon name="angle right" />
                                    </Button>
                                  )
                                ) : (
                                  <UserAccessModal
                                    modalTrigger={
                                      <Button size="mini" className="actionBtn">
                                        {" "}
                                        Apply <Icon name="angle right" />
                                      </Button>
                                    }
                                    isAction
                                    actionText="apply to"
                                  />
                                )}

                                <Button
                                  onClick={this.onClick}
                                  className="closeIcon_footer">
                                  <Icon name="angle up" />
                                </Button>
                              </List.Header>
                            </List.Content> */}
                          </List.Item>
                        </List>
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                </div>
              </Accordion.Content>
            </div>
          </Card.Content>
        </Card>
      </Accordion>
    );
  }
}

export default JobSectionCard;
