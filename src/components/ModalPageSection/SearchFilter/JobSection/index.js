import React from "react";
import { withHits } from "../../../AlgoliaContainer";
import JobSectionCard from "./JobSectionCard";
import JobNoFound from "../../../Banners/JobNoFound";
import IcNoJobsFound from "../../../../assets/svg/IcNoJobsFound";
import FoundJobBanner from "../../banners/foundJobBanner";

// TODO: Handle Like / Save / Apply Section
class JobSection extends React.Component {
  render() {
    const { hits } = this.props;

    if (!hits || hits.length === 0) {
      return (
        <div className="peoplePane">
          <div className="foundJobBanner mobile__searchBanner">
            <FoundJobBanner />
          </div>

          <div
            className="searchFilterCard"
            style={{ backgroundColor: "white" }}>
            <div>
              <JobNoFound typeIcon={<IcNoJobsFound width="118" height="74"/>} />
            </div>
          </div>
        </div>
      );
    }

    return (
      <div className="peoplePane">
        <div className="foundJobBanner mobile__searchBanner">
          <FoundJobBanner />
        </div>

        <div className="searchFilterCard" style={{ backgroundColor: "white" }}>
          <div>
            {hits.map(val => {
              return <JobSectionCard val={val} />;
            })}
          </div>
        </div>
      </div>
    );
  }
}

export default withHits(JobSection);
