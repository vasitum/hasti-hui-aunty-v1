import React, { Component } from "react";
import { List, Button } from "semantic-ui-react";
import "./index.scss";
export default class SkillsCount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      skillShow: 4,
      totalCount: 0
    };
  }
  async componentDidMount() {
    const { val } = this.props;
    this.setState({ totalCount: val.skills.length });
  }

  showSkills() {
    const { val } = this.props;
    const { skillShow } = this.state;
    return val.skills.slice(0, skillShow);
  }

  render() {
    const { skillShow, totalCount } = this.state;
    return (
      <React.Fragment>
        <List bulleted horizontal className="text-night-rider SkillCountBlock">
          {this.showSkills()
            ? this.showSkills().map((skill, idx) => {
                if (idx > 4) return null;

                if (idx === 0) {
                  return (
                    <List.Item key={skill}>
                      <span className="text-nobel">Skills:</span> {skill}
                    </List.Item>
                  );
                }
                return <List.Item key={skill}>{skill}</List.Item>;
              })
            : null}
          {totalCount >= 5 ? (
            <a href="javascript:;" className="skillShowMoreButton">
              +{parseFloat(totalCount) - parseFloat(skillShow)} more
            </a>
          ) : (
            ""
          )}
        </List>
      </React.Fragment>
    );
  }
}
