import React from "react";
import BestPaneTab from "./bestPanelTab";
import JobPaneTab from "./jobPanelTab";
import PeoplePaneTab from "./peoplePaneTab";

import { Tab, Menu, Label, Grid, Header } from "semantic-ui-react";
import { withRouter, Link } from "react-router-dom";
import getActiveTab from "../../../utils/ui/getActiveTab";

import { withAlgoliaContext } from "../../../contexts/AlgoliaContext";

import { Helmet } from "react-helmet";

import { connect } from "react-redux";
import qs from "qs";

function getTabPanes(parsedQS, searchCount, currentValue) {
  // console.log("searchCount", searchCount);
  return [
    {
      menuItem: (
        <Menu.Item
          as={Link}
          to={"/search" + "?query=" + encodeURIComponent(currentValue)}
          key={"Best" + currentValue}
          className="mobile hidden">
          <Header as="h2">Best</Header>
        </Menu.Item>
      ),
      render: () => {
        return (
          <div className="bestTab">
            <Tab.Pane attached={false}>
              <BestPaneTab />
            </Tab.Pane>
          </div>
        );
      }
    },
    {
      menuItem: (
        <Menu.Item
          as={Link}
          to={"/search/job" + "?query=" + encodeURIComponent(currentValue)}
          key={"Job" + currentValue}
          className="mobile hidden">
          <Header as="h1">
            Jobs <Label floating>{searchCount.job}</Label>
          </Header>
        </Menu.Item>
      ),
      render: () => {
        return (
          <div className="">
            <Tab.Pane attached={false}>
              <Helmet>
                <meta charSet="utf-8" />
                <title>
                  Free Job Posting, Free Job, Job Search Website, Job Search
                  Engine – Vasitum
                </title>
                <meta
                  name="title"
                  content="Free Job Posting, Free Job, Job Search Website, Job Search Engine – Vasitum "
                />
                <meta
                  name="twitter:title"
                  content="Free Job Posting, Free Job, Job Search Website, Job Search Engine – Vasitum "
                />
                <meta
                  property="og:title"
                  content="Free Job Posting, Free Job, Job Search Website, Job Search Engine – Vasitum "
                />
                <meta
                  property="og:description"
                  content={`Vasitum is an online job search engine which offers free job alert, artificial intelligence and resumes format services; search now for your ideal job. Subscribe to free job alerts.`}
                />
                <meta
                  property="twitter:description"
                  content={`Vasitum is an online job search engine which offers free job alert, artificial intelligence and resumes format services; search now for your ideal job. Subscribe to free job alerts.`}
                />
                <link rel="canonical" href="http://vasitum.com/about-us" />
              </Helmet>
              <JobPaneTab />
            </Tab.Pane>
          </div>
        );
      }
    },
    {
      menuItem: (
        <Menu.Item
          as={Link}
          to={"/search/people" + "?query=" + encodeURIComponent(currentValue)}
          key={"People" + currentValue}
          className="mobile hidden">
          <Header as="h2">
            People
            <Label floating>{searchCount.people}</Label>
          </Header>
        </Menu.Item>
      ),
      render: () => {
        return (
          <div className="">
            <Tab.Pane attached={false}>
              <PeoplePaneTab />
            </Tab.Pane>
          </div>
        );
      }
    }
  ];
}

class SearchResult extends React.Component {
  constructor(props) {
    super(props);
  }

  parsedQS = this.props.location.search
    ? qs.parse(this.props.location.search)
    : {};

  render() {
    const { match } = this.props;
    return (
      <div className="search-result-panel">
        <Tab
          menu={{ text: true }}
          panes={getTabPanes(
            this.parsedQS,
            this.props.search,
            // this.props.currentValue
            this.props.searchQuery
          )}
          activeIndex={getActiveTab(match.params.pageid, {
            job: 1,
            people: 2
          })}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    currentValue: state.search.currentValue,
    search: state.search.searchCount
  };
};

export default withRouter(
  connect(mapStateToProps)(withAlgoliaContext(SearchResult))
);
// export default SearchResult;
