import React from "react";
import BestJobCard from "../../cards/bestJobCard";
import BestPeopleCard from "../../cards/bestPeopleCard";
import { Grid, Button } from "semantic-ui-react";

import BestFoundJobBanner from "../../banners/bestFoudJob";
import BestFoundPeopleBanner from "../../banners/bestFoudPeople";
import JobNoFound from "../../../Banners/JobNoFound";

import { Link } from "react-router-dom";

import IcNoJobsFound from "../../../../assets/svg/IcNoJobsFound";
import IcNoCandidatesFound from "../../../../assets/svg/IcNoCandidatesFound";

import IcFooterArrowIcon from "../../../../assets/svg/IcFooterArrowIcon";

import { withHits } from "../../../AlgoliaContainer";

import "./index.scss";

class BestSection extends React.Component {
  state = {};

  // TODO: Enable Like and Share SubSystems

  render() {
    const { hits, people } = this.props;
    // SkillLenth = 

    // console.log("check skill" , hits);

    // handle not found cases
    if (!hits || hits.length === 0) {
      return (
        <div>
          {!people ? (
            <div className="bestFoundJobBanner bg-cerulean b-t-r-l-r-6">
              <BestFoundJobBanner />
            </div>
          ) : (
            <div className="bestFoundPeopleBanner bg-persian-green b-t-r-l-r-6">
              <BestFoundPeopleBanner />
            </div>
          )}
          <div>
            {!people ? (
              <JobNoFound
                typeIcon={<IcNoJobsFound width="118" height="74" />}
              />
            ) : (
              <JobNoFound
                types="Candidates not"
                typeIcon={<IcNoCandidatesFound width="118" height="74" />}
              />
            )}
          </div>
        </div>
      );
    }

    // handle hits
    return !people ? (
      <div>
        <div
          className="bestFoundJobBanner bg-cerulean b-t-r-l-r-6"
          style={{ marginBottom: "10px" }}>
          <BestFoundJobBanner />
        </div>
        <Grid>
          <Grid.Row
            className="vdivide"
            style={{
              paddingTop: "0px",
              border: "1px solid #efefef",
              borderTop: "none",
              borderBottom: "none",
              paddingBottom: "0"
            }}>
            {/*  */}
            {/* Map Things here */}
            {(hits => {
              const ret = [];
              const hLength = hits.length < 4 ? hits.length : 4;
              for (let i = 0; i < hLength; i++) {
                const element = hits[i];
                ret.push(
                  <Grid.Column
                    key={i}
                    width={8}
                    style={{ background: "white" }}>
                    <BestJobCard idx={i} hit={element} />
                  </Grid.Column>
                );
              }

              return ret;
            })(hits)}
          </Grid.Row>
        </Grid>
        <div className="bestPanel_cardBtn">
          <Button as={Link} to={`/search/job${window.location.search}`}>
            View more jobs
            {/* <IcFooterArrowIcon pathcolor="#797979"/> */}
          </Button>
        </div>
      </div>
    ) : (
      <div>
        <div
          className="bestFoundPeopleBanner bg-persian-green b-t-r-l-r-6"
          style={{ marginBottom: "10px" }}>
          <BestFoundPeopleBanner />
        </div>
        <Grid>
          <Grid.Row
            className="vdivide"
            style={{
              paddingTop: "0px",
              border: "1px solid #efefef",
              borderTop: "none",
              borderBottom: "none",
              paddingBottom: "0"
            }}>
            {/*  */}
            {/* Map things here */}
            {(hits => {
              const ret = [];
              const hLength = hits.length < 4 ? hits.length : 4;
              for (let i = 0; i < hLength; i++) {
                const element = hits[i];
                ret.push(
                  <Grid.Column
                    key={i}
                    width={8}
                    style={{ background: "white" }}>
                    <BestPeopleCard idx={i} hit={element} isPeople />
                  </Grid.Column>
                );
              }

              return ret;
            })(hits)}
          </Grid.Row>
        </Grid>
        <div className="bestPanel_cardBtn">
          <Button as={Link} to={`/search/people${window.location.search}`}>
            View more people
            {/* <IcFooterArrowIcon pathcolor="#797979"/> */}
          </Button>
        </div>
      </div>
    );
  }
}

export default withHits(BestSection);
