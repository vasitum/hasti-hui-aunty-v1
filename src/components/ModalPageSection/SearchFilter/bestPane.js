import React from "react";
import { connect } from "react-redux";
import { SearchBox, Index } from "react-instantsearch-dom";

// card hits
// import Banner from "./banner";

import UpdateJobCounter from "../banners/UpdateJobCounter";
import UpdatePeopleCounter from "../banners/UpdatePeopleCounter";

import { onInputChange } from "../../../actions/search";
import { withRouter } from "react-router-dom";

import { withSearch } from "../../AlgoliaContainer";
import PageAlgoliaLoading from "./PageAlgoliaLoading";
import BestSection from "./BestSection";
import CandidateCVFilter from "../../Dashboard/CandidateDashbord/CandidateApplyedJobs/CandidateCVFilter";

class BestPane extends React.Component {
  /**
   *
   * @TODO: Enable Tab State Functionality Back once we are done.
   */
  // componentDidMount() {
  //   if (
  //     this.state.searchState.query &&
  //     this.props.searchValue !== this.state.searchState.query
  //   ) {
  //     this.props.onChangeSearchText(this.state.searchState.query);
  //   }
  // }

  render() {
    return (
      <div className="best-panel">
        <SearchBox style={{ display: "none" }} />
        <Index indexName="job">
          <div className="cardsGroupJob cards-group">
            <BestSection />
            <UpdateJobCounter />
            <CandidateCVFilter
              label={"Status"}
              attribute="status"
              defaultRefinement={true}
              value={"Active"}
              style={{
                display: "none"
              }}
            />
          </div>
        </Index>

        <Index indexName="user">
          <div className="cardsGroupsPeople cards-group padding-top-30">
            <BestSection people />
            <UpdatePeopleCounter />
          </div>
        </Index>

        <PageAlgoliaLoading loaderText="Loading Search" />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    searchValue: state.search.currentValue
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onChangeSearchText: data => {
      dispatch(onInputChange(data));
    }
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(
    withSearch(BestPane, {
      apiKey: process.env.REACT_APP_ALGOLIA_API_KEY,
      appId: process.env.REACT_APP_ALGOLIA_APP_ID,
      indexName: "job",
      syncNavSearch: true
    })
  )
);
