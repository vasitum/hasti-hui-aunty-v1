import React from "react";
import { connectStateResults } from "react-instantsearch/connectors";
import PageLoader from "../../PageLoader";

export default connectStateResults(({ searching, loaderText }) => {
  return <PageLoader active={searching} loaderText={loaderText} />;
});
