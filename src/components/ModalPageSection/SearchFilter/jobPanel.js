import React from "react";
import {
  Image,
  Header,
  Accordion,
  List,
  Icon,
  Button,
  Card,
  Grid,
  Dropdown
} from "semantic-ui-react";

import { withRouter, Link } from "react-router-dom";
import SkillsComponent from "../commonComponent/skills";
import ReactQuill from "react-quill";

import JobDescription from "../commonComponent/jobDescription";

import SocialBtn from "../Buttons/socialBtn";

import isLoggedIn from "../../../utils/env/isLoggedin";
import UserAccessModal from "../UserAccessModal";

import Banner from "./banner";
import result_search from "../../../assets/img/result_search.png";
import profile from "../../../assets/img/profile.png";
import wipro from "../../../assets/img/wipro.png";
import airtel from "../../../assets/img/airtel.png";

import FoundJobBanner from "../banners/foundJobBanner";

import JobNoFound from "../../Banners/JobNoFound";
import IcNoJobsFound from "../../../assets/svg/IcNoJobsFound";

import { connectHits } from "react-instantsearch/connectors";
import CompanySvg from "../../../assets/svg/IcCompany";
import LocationLogo from "../../../assets/svg/IcLocation";
import JobLogo from "../../../assets/svg/Icjob";
import MoreOptionIcon from "../../../assets/svg/IcMoreOptionIcon";

import SearchCardHeader from "../commonComponent/searchCardHeader";
import InfoSectionGridHeader from "../PreProfile/InfoSectionGridHeader";
import QuillText from "../../CardElements/QuillText";

import {
  jobLike,
  jobSave,
  jobApply,
  getLikedAndSavedJobs
} from "../../../api/jobs/jobExtras";

import { toast } from "react-toastify";

const JobCardConnected = class extends React.Component {
  constructor(props) {
    super(props);
    this.onLikeClick = this.onLikeClick.bind(this);
    this.onApplyClick = this.onApplyClick.bind(this);
  }

  state = {
    likes: [],
    prevLikeIds: []
  };

  onViewJobClick = (ev, { tohref }) => {
    this.props.history.push("/job/public/" + tohref);
  };

  async onLikeClick(ev, { id }) {
    ev.stopPropagation();

    try {
      const res = await jobLike(id);
      const data = await res.text();

      // console.log(data);
    } catch (error) {
      console.error(error);
    }
  }

  async onApplyClick(ev, { id, title }) {
    ev.stopPropagation();

    try {
      const res = await jobApply(id, title);
      const data = await res.text();

      // console.log(data);
      toast(`Applied on job ${id}`);
    } catch (error) {
      console.error(error);
    }
  }

  processLikesAndSave(json) {
    const keys = Object.keys(json);

    // if keys are zero
    if (keys.length === 0) {
      return [];
    }

    const resultLikeArray = [];

    for (const key in json) {
      if (json.hasOwnProperty(key)) {
        if (json["like"]) {
          resultLikeArray.push(key);
        }
      }
    }

    return resultLikeArray;
  }

  componentDidUpdate() {
    // const { hits } = this.props;
    // const idArray = hits.map(val => {
    //   return val.objectID;
    // });
    // if (idArray.length === 0) {
    //   return;
    // }
    // getLikedAndSavedJobs()
    //   .then(res => {
    //     return res.json();
    //   })
    //   .then(res => {
    //     return this.processLikesAndSave(res);
    //   })
    //   .then(likeIds => {
    //     if (likeIds === this.state.likes) {
    //       return;
    //     }
    //     this.setState({
    //       likes: likeIds,
    //       prevLikeIds: idArray
    //     });
    //   })
    //   .catch(err => {
    //     console.error(err);
    //   });
  }

  render() {
    const { hits, jobIndex, jobClick, jsonData } = this.props;

    const isLoggedInVal = isLoggedIn();

    if (!hits || hits.length === 0) {
      return (
        <div>
          <JobNoFound typeIcon={<IcNoJobsFound width="118" height="74" />} />
        </div>
      );
    }
    return (
      <Accordion>
        <Card fluid>
          <Card.Content>
            {hits.map((val, idx) => {
              let jsonData = "";

              try {
                jsonData = JSON.parse(val.desc);
              } catch (error) {
                jsonData = val.desc;
              }

              return (
                <div
                  className={
                    "accordion-panel " + (jobIndex === idx ? "has-shadow" : "")
                  }
                  key={idx}>
                  <Accordion.Title
                    active={jobIndex === idx}
                    index={idx}
                    onClick={jobClick}
                    style={{ padding: "0" }}>
                    <SearchCardHeader
                      onLikeClick={this.onLikeClick}
                      onApplyClick={this.onApplyClick}
                      objectID={val.objectID}
                      val={val}
                      likesArray={this.state.likes}
                      headerlink="/job/public/"
                    />

                    {/* only Desktop */}
                    <div className="" style={{ padding: "15px 27px" }}>
                      <Grid>
                        <Grid.Row className="padding-top-5">
                          <Grid.Column width={1} />

                          <Grid.Column width={15}>
                            <List className="mobile hidden">
                              <List.Item className="experienceDisplay">
                                <List.Content>
                                  <List.Description>
                                    <p className="margin-bottom-0 vasitumFontOpen-500 text-trolley-grey">
                                      <span
                                        className="padding-right-20 text-grey"
                                        style={{
                                          display:
                                            (!val.expMin && !val.expMax) ||
                                            jobIndex === idx
                                              ? "none"
                                              : "block"
                                        }}>
                                        Experience:{" "}
                                        <span className="text-pacific-blue">
                                          {" "}
                                          {val.expMin} - {val.expMax} yrs
                                        </span>
                                      </span>

                                      <span className="padding-right-20 text-grey">
                                        Job type:{" "}
                                        <span className="text-pacific-blue">
                                          {" "}
                                          {val.jobType}{" "}
                                        </span>
                                      </span>

                                      <span className="padding-right-20 text-grey">
                                        Job role:{" "}
                                        <span className="text-pacific-blue">
                                          {" "}
                                          {val.role}{" "}
                                        </span>
                                      </span>
                                    </p>
                                  </List.Description>
                                </List.Content>
                              </List.Item>

                              <List.Item className="skillsDisplay">
                                <List.Content className="filterSkill">
                                  <List
                                    bulleted
                                    horizontal
                                    className="text-night-rider">
                                    {val.skills
                                      ? val.skills.map((skill, idx) => {
                                          if (idx === 0) {
                                            return (
                                              <List.Item key={skill}>
                                                <span
                                                  className="text-nobel"
                                                  style={{
                                                    display: "inline-block"
                                                  }}>
                                                  Skills:
                                                </span>{" "}
                                                {skill}
                                              </List.Item>
                                            );
                                          }
                                          return (
                                            <List.Item key={skill}>
                                              {skill}
                                            </List.Item>
                                          );
                                        })
                                      : null}
                                  </List>
                                </List.Content>
                              </List.Item>
                            </List>
                          </Grid.Column>
                        </Grid.Row>
                      </Grid>
                    </div>

                    {/* only Desktop end */}
                  </Accordion.Title>

                  <Accordion.Content active={jobIndex === idx}>
                    <div className="accordian-body padding-top-10 padding-bottom-20">
                      <div className="searchCard_divider" />
                      <Grid>
                        <Grid.Row>
                          <Grid.Column width={1} />
                          <Grid.Column width={15}>
                            <List
                              verticalAlign="middle"
                              className="padding-left-20 padding-right-20">
                              <InfoSectionGridHeader headerText="Job Description">
                                <QuillText value={jsonData} readOnly />
                              </InfoSectionGridHeader>
                              {/* <JobDescription jsonData={jsonData} /> */}

                              <SkillsComponent val={val} />

                              <List.Item className="cardFooter_IconArrow">
                                <List.Content>
                                  <List.Header>
                                    <Button
                                      size="mini"
                                      tohref={val.objectID}
                                      onClick={this.onViewJobClick}
                                      className="b-r-30 bg-munsell btnBlue_Text fontSize-12 text-night-rider">
                                      View complete job
                                    </Button>
                                  </List.Header>
                                </List.Content>
                                <List.Content
                                  className="alingCenterRight"
                                  style={{ marginTop: "20px" }}>
                                  <List.Header>
                                    <SocialBtn
                                      objectID={val.objectID}
                                      onLikeClick={this.onLikeClick}
                                    />

                                    {!isLoggedInVal ? (
                                      <Button size="mini" className="actionBtn">
                                        {" "}
                                        Apply <Icon name="angle right" />
                                      </Button>
                                    ) : (
                                      <UserAccessModal
                                        modalTrigger={
                                          <Button
                                            size="mini"
                                            className="actionBtn">
                                            {" "}
                                            Apply <Icon name="angle right" />
                                          </Button>
                                        }
                                        isAction
                                        actionText="apply to"
                                      />
                                    )}

                                    <Button
                                      onClick={jobClick}
                                      className="closeIcon_footer">
                                      <Icon name="angle up" />
                                    </Button>
                                    {/* <Icon
                                      name="angle up"
                                      className="vasitumFilterDropdowmIcon margin-left-10"
                                      circular
                                      index={idx}
                                      onClick={jobClick}
                                      floated="right"
                                    /> */}
                                  </List.Header>
                                </List.Content>
                              </List.Item>
                            </List>
                          </Grid.Column>
                        </Grid.Row>
                      </Grid>
                    </div>
                  </Accordion.Content>
                </div>
              );
            })}
          </Card.Content>
        </Card>
      </Accordion>
    );
  }
};

const JobCardConnectedAlgolia = connectHits(JobCardConnected);

class JobPanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      jobIndex: -1
    };
  }

  jobClick = (e, titleProps) => {
    const { index } = titleProps;
    const { jobIndex } = this.state;
    const newIndex = jobIndex === index ? -1 : index;

    this.setState({ jobIndex: newIndex });
  };

  render() {
    const { jobIndex } = this.state;
    return (
      <div className="peoplePane">
        <div className="foundJobBanner mobile__searchBanner">
          <FoundJobBanner />
        </div>

        <div className="searchFilterCard" style={{ backgroundColor: "white" }}>
          <JobCardConnectedAlgolia
            history={this.props.history}
            jobClick={this.jobClick.bind(this)}
            jobIndex={this.state.jobIndex}
          />
        </div>
      </div>
    );
  }
}

export default withRouter(JobPanel);
