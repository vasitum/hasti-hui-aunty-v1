import React from "react";
import { List, Header, Icon, Image } from "semantic-ui-react";
import UserSignUp from "../forms/userSignUp";
import al_based from "../../../assets/img/AlBased.png";
import Smart_interview from "../../../assets/img/Smart_interview.png";
import free from "../../../assets/img/free.png";

import isUserLoggedIn from "../../../utils/env/isLoggedin";

class BestPaneLeft extends React.Component {
  onUserSignUp = ev => {};

  render() {
    return (
      <div className="Best-Pane-Left">
        {isUserLoggedIn() && (
          <React.Fragment>
            <List verticalAlign="middle">
              <List.Item>
                <List.Content>
                  <List.Header>
                    <div className="signUp bg-persian-green b-t-r-l-r-6">
                      <Header as="h3" className="font-w-500">
                        {" "}
                        Sign Up
                      </Header>
                      {/* <p className="vasitumFontOpen-500 textAliceBlue">
                        Apply now &amp; increase your chances
                      </p> */}
                    </div>
                  </List.Header>
                </List.Content>
              </List.Item>
            </List>

            <div className="sign-up-email bg-white">
              <UserSignUp className="displayNone" siginbtntext="Get started" />
            </div>
          </React.Fragment>
        )}

        <div className="why-vasitum padding-top-50">
          <List verticalAlign="middle">
            <List.Item>
              <List.Content>
                <List.Header>
                  <Header as="h2">Why Vasitum?</Header>
                </List.Header>
              </List.Content>
            </List.Item>

            <List.Item className="padding-top-10 listItem_menu">
              <List.Content floated="left">
                <svg
                  style={{ marginLeft: "5px" }}
                  xmlns="http://www.w3.org/2000/svg"
                  width="32.969"
                  height="30"
                  viewBox="0 0 32.969 38.875">
                  <defs>
                    <filter
                      id="filter"
                      x="172.406"
                      y="725.563"
                      width="32.969"
                      height="30"
                      filterUnits="userSpaceOnUse">
                      <feFlood result="flood" floodColor="#1f2532" />
                      <feComposite
                        result="composite"
                        operator="in"
                        in2="SourceGraphic"
                      />
                      <feBlend result="blend" in2="SourceGraphic" />
                    </filter>
                  </defs>
                  <g fill="">
                    <path
                      className="cls-1"
                      d="M204.084,751l-2.475.047v5.408a3.089,3.089,0,0,1-3.074,3.089h-4.227v3.811a1.08,1.08,0,0,1-1.076,1.082h-15.14a1.08,1.08,0,0,1-1.076-1.082v-3.911A27.609,27.609,0,0,0,173.99,746.9a14.624,14.624,0,1,1,27.614-6.667,7.945,7.945,0,0,0,.917,3.7l2.7,5.122A1.325,1.325,0,0,1,204.084,751Zm-9.931-14.377a0.7,0.7,0,0,0-.422-0.636l-1.618-.679a0.694,0.694,0,0,1-.375-0.385s0-.008,0-0.013a0.7,0.7,0,0,1,0-.519l0.654-1.644a0.691,0.691,0,0,0-.151-0.744l-0.727-.731a0.682,0.682,0,0,0-.745-0.152l-1.61.667a0.674,0.674,0,0,1-.525,0,0.067,0.067,0,0,1-.026-0.009,0.7,0.7,0,0,1-.375-0.368l-0.688-1.622a0.682,0.682,0,0,0-.629-0.42h-1.029a0.69,0.69,0,0,0-.632.424l-0.668,1.605a0.7,0.7,0,0,1-.383.377c-0.013,0-.03.013-0.043,0.017a0.707,0.707,0,0,1-.516,0l-1.614-.653a0.682,0.682,0,0,0-.741.151l-0.727.731a0.689,0.689,0,0,0-.151.749l0.654,1.6a0.689,0.689,0,0,1,0,.524,0.357,0.357,0,0,1-.021.06,0.714,0.714,0,0,1-.37.381l-1.593.683a0.685,0.685,0,0,0-.418.632h0v1.034a0.694,0.694,0,0,0,.422.636l1.585,0.662a0.674,0.674,0,0,1,.374.381l0.026,0.06a0.689,0.689,0,0,1,0,.524l-0.646,1.613a0.69,0.69,0,0,0,.151.744l0.727,0.732a0.681,0.681,0,0,0,.745.151l1.6-.662a0.71,0.71,0,0,1,.517,0c0.013,0,.03.013,0.043,0.017a0.683,0.683,0,0,1,.387.372l0.685,1.61a0.679,0.679,0,0,0,.628.419h1.029a0.689,0.689,0,0,0,.633-0.424l0.663-1.622a0.7,0.7,0,0,1,.374-0.372l0.03-.013a0.69,0.69,0,0,1,.517,0l1.636,0.657a0.68,0.68,0,0,0,.74-0.151l0.728-.731a0.682,0.682,0,0,0,.15-0.749l-0.671-1.626a0.685,0.685,0,0,1,0-.515l0.008-.022a0.693,0.693,0,0,1,.37-0.38l1.627-.7a0.685,0.685,0,0,0,.418-0.632v-1.034Zm-7.761,3.448a2.92,2.92,0,1,1,2.905-2.92A2.914,2.914,0,0,1,186.392,740.074Z"
                      transform="translate(-172.406 -725.563)"
                    />
                  </g>
                </svg>
              </List.Content>

              <List.Content>
                <List.Header> AI based</List.Header>
                <p className="text-pacific-blue">personal assistance </p>
              </List.Content>
            </List.Item>
            <List.Item className="padding-top-15">
              <List.Content floated="left">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="38.781"
                  height="30"
                  viewBox="0 0 38.781 38.875">
                  <defs>
                    <filter
                      id="filter"
                      x="170"
                      y="802.563"
                      width="38.781"
                      height="30"
                      filterUnits="userSpaceOnUse">
                      <feFlood result="flood" floodColor="#1f2532" />
                      <feComposite
                        result="composite"
                        operator="in"
                        in2="SourceGraphic"
                      />
                      <feBlend result="blend" in2="SourceGraphic" />
                    </filter>
                  </defs>
                  <g>
                    <path
                      className="cls-1"
                      d="M179.04,826.073v2.777h-2.772v-2.777h2.772Zm2.771-5.554h2.771V823.3h-2.771v-2.777Zm2.771,8.331h-2.771v-2.777h2.771v2.777Zm2.772-8.331h2.771V823.3h-2.771v-2.777ZM199.086,822a9.72,9.72,0,1,1-8.754,13.885H172.758a2.783,2.783,0,0,1-2.772-2.777V810.891a2.783,2.783,0,0,1,2.772-2.777h2.771V809.5a2.775,2.775,0,0,0,2.771,2.777v-8.332a1.386,1.386,0,1,1,2.772,0v4.166h8.314V809.5a2.775,2.775,0,0,0,2.771,2.777v-8.332a1.386,1.386,0,1,1,2.771,0v4.166H197.7a2.773,2.773,0,0,1,2.771,2.777v8.332h-3.428v-1.45H173.415V833.1H189.5A9.644,9.644,0,0,1,199.086,822Zm0,16.663a6.943,6.943,0,1,0-6.929-6.943A6.944,6.944,0,0,0,199.086,838.663Zm-0.761-8.312,4.261-2.519a0.277,0.277,0,0,1,.379.38l-2.512,4.271a1.528,1.528,0,0,1-.249.325A1.561,1.561,0,1,1,198,830.6,1.5,1.5,0,0,1,198.325,830.351Z"
                      transform="translate(-170 -802.563)"
                    />
                  </g>
                </svg>
              </List.Content>

              <List.Content>
                <List.Header>Smart interview </List.Header>
                <p className="text-pacific-blue">scheduling</p>
              </List.Content>
            </List.Item>
          </List>
        </div>
      </div>
    );
  }
}

export default BestPaneLeft;
