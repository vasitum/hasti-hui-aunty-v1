import React from "react";

import { Grid, Button, Form } from "semantic-ui-react";

import InfoSection from "../../Sections/InfoSection";
import InputField from "../../Forms/FormFields/InputField";
import SelectOptionField from "../../Forms/FormFields/SelectOptionField";
import IcCloseIcon from "../../../assets/svg/IcCloseIcon";
import ModalBanner from "../../ModalBanner";
import InputFieldLabel from "../../Forms/FormFields/InputFieldLabel";
import getPlaceholder from "../../Forms/FormFields/Placeholder";

import ModalFooterBtn from "../../Buttons/ModalFooterBtn";

import "./index.scss";

class SkillEditModal extends React.Component {
  render() {
    const {
      skills,
      onInputChange,
      onItemRemove,
      onModalSave,
      onModalClose,
      isProfile
    } = this.props;

    return (
      <div className="card_panel">
        <ModalBanner headerText="Edit Experience">
          {/* <Button compact className="save_btn">
            Save
          </Button> */}
        </ModalBanner>

        <div className="skillModal_fields">
          <Form>
            <div className="skillModal_Body">
              <div className="edit_profileSkill">
                <Grid>
                  <Grid.Row className="mobile hidden tablet hidden">
                    <Grid.Column width="4" textAlign="right">
                      <p>Skill name</p>
                    </Grid.Column>
                    <Grid.Column width="5">
                      <p>Year of experience</p>
                    </Grid.Column>
                    {!isProfile && (
                      <Grid.Column width="7">
                        <p>Requirement</p>
                      </Grid.Column>
                    )}
                  </Grid.Row>
                </Grid>
                {skills.map((val, idx) => (
                  <Grid key={val.name}>
                    {/* only mobile */}
                    <Grid.Row only="mobile tablet" className="mobile_row">
                      <Grid.Column only="mobile" mobile={8} tablet={8}>
                        <p>{val.name}</p>
                      </Grid.Column>

                      <Grid.Column
                        only="mobile tablet"
                        mobile={8}
                        textAlign="right"
                        className="mobile_closeIcon">
                        <span
                          onClick={ev => onItemRemove(ev, { idx })}
                          idx={idx}>
                          <IcCloseIcon
                            pathcolor="#c8c8c8"
                            height="10"
                            width="10"
                          />
                        </span>
                      </Grid.Column>
                    </Grid.Row>
                    {/* only mobile */}

                    <Grid.Row>
                      <Grid.Column
                        mobile={16}
                        tablet={8}
                        computer={4}
                        largeScreen={4}
                        className="tablet hidden mobile hidden">
                        <InputFieldLabel label={val.name} />
                      </Grid.Column>

                      <Grid.Column
                        mobile={16}
                        tablet={16}
                        computer={5}
                        largeScreen={5}>
                        <InputField
                          placeholder={getPlaceholder(
                            "Year of experience",
                            "Yrs of Exp"
                          )}
                          value={val.exp}
                          idx={idx}
                          onChange={onInputChange}
                          name="exp"
                          type="number"
                          min={0}
                        />
                      </Grid.Column>

                      {!isProfile && (
                        <Grid.Column
                          mobile={16}
                          tablet={16}
                          computer={5}
                          largeScreen={5}
                          className="mobileSkill_edit">
                          <SelectOptionField
                            placeholder={getPlaceholder(
                              "Year of experience",
                              "Yrs of Exp"
                            )}
                            optionsItem={[
                              {
                                text: "Optional",
                                value: "optional"
                              },
                              {
                                text: "Mandatory",
                                value: "mandatory"
                              }
                            ]}
                            value={val.reqType}
                            name="reqType"
                            onChange={onInputChange}
                            idx={idx}
                          />
                        </Grid.Column>
                      )}

                      <Grid.Column
                        largeScreen={2}
                        computer={2}
                        only="computer"
                        className="tablet hidden mobile hidden closeIcon_skillEdit">
                        <span
                          className="skillModal"
                          onClick={ev => onItemRemove(ev, { idx })}
                          idx={idx}>
                          <IcCloseIcon
                            pathcolor="#c8c8c8"
                            height="10"
                            width="10"
                          />
                        </span>
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                ))}
              </div>
            </div>

            <div className="skillModal_footer">
              <ModalFooterBtn
                onClick={onModalSave}
                onCancelClick={onModalClose}
                actioaBtnText="Save"
              />
            </div>
          </Form>
        </div>
      </div>
    );
  }
}

export default SkillEditModal;
