import React from "react";
import { List, Image, Header} from "semantic-ui-react";
import AIBased from "../../assets/img/AlBased.png";
import free from "../../assets/img/free.png";
import Smart_interview from "../../assets/img/Smart_interview.png";
import "./index.scss";

const OtherDetails = props =>{
 return(
    <div className="OtherDetails">
      <List className="FullList">
        <List.Item>
          <List.Content className="why">
            <Header as="h2">Why Vasitum?</Header>
          </List.Content>
        </List.Item>

        <List.Item className="list_item">
            <Image src={AIBased}/>
          <List.Content className="listItem_header">
            <p>AI based</p>
            <List.Description className="listItem_subheader" as='a'>personal assistance</List.Description>
          </List.Content>
        </List.Item>

        <List.Item className="list_item">
          <Image src={Smart_interview}/>
          <List.Content className="listItem_header">
            <p>Smart interview</p>
            <List.Description className="listItem_subheader" as='a'>schceduling</List.Description>
          </List.Content>
        </List.Item>

        <List.Item className="list_item">
          <Image src={free}/>
          <List.Content className="listItem_header"> 
            <p>FREE</p>
            <List.Description className="listItem_subheader" as='a'>for everyone</List.Description>
          </List.Content>
        </List.Item>
      </List>
    </div>
 )
}

export default OtherDetails;