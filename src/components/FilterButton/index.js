import React from "react";
import {Button, Icon, List } from "semantic-ui-react";
import FilterIC from "../../assets/svg/IcFilterIcon";
import Bell from "../../assets/svg/IcSearchAlertIcon";
import "./index.scss";

const FilterButton = props =>{ 
    return(
    <div className="Box_Button">
    <Button>
        <List className="Filter_Button">
        <List.Item className="Button">
       <List.Icon>
        <FilterIC pathcolor="#c9c9c9" />
        </List.Icon>
         <List.Content>
           <p>Filter Job</p>
         </List.Content>
       </List.Item>
        </List>
    </Button>

    <Button basic inverted>
        <List className="Filter_Button">
        <List.Item className="Button">
       <List.Icon>
        <Bell pathcolor="#c9c9c9" />
        </List.Icon>
         <List.Content>
           <p>Get search alert</p>
         </List.Content>
       </List.Item>
        </List>
    </Button>
        </div>

    )}

    export default FilterButton