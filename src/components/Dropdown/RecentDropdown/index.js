import React from "react";

import { Dropdown } from "semantic-ui-react";
import PropTypes from "prop-types";

class RecentDropdown extends React.Component {
  onDropdownChange = (ev, { value }) => {
    const { refine } = this.props;
    refine(value);
  };

  render() {
    const { className, items, defaultRefinement, ...resProp } = this.props;
    return (
      <div className={`RecentDropdown ${className}`}>
        Sort by: &nbsp;
        <Dropdown
          inline
          pointing="top right"
          options={items}
          defaultValue={defaultRefinement}
          onChange={this.onDropdownChange}
          {...resProp}
        />
      </div>
    );
  }
}

RecentDropdown.propTypes = {
  className: PropTypes.string
};

RecentDropdown.defaultProps = {
  className: "displayBlock"
};

export default RecentDropdown;
