import React from "react";

import { Dropdown } from "semantic-ui-react";

import IcMoreOptionIcon from "../../../assets/svg/IcMoreOptionIcon";

const MoreOptionsTrigger = (
  <span>
    <IcMoreOptionIcon pathcolor="#acaeb5" />
  </span>
);

class MoreOptionDropDown extends React.Component {
  
  getItems = isMobile => {
    return [
      {
        text: "Share",
        value: "Share",
        // onClick: () => this.props.onChangeSearchField()
      }
    ];
  };
  
  render() {
    const { ...resProps } = this.props;
    return (
      <div className="Main_Dropdown" {...resProps}>
        <Dropdown
          trigger={MoreOptionsTrigger}
          options={this.getItems()}
          icon={null}
          pointing="right"
        />
      </div>
    )
  }
}

export default MoreOptionDropDown;