import React from "react";

import { Dropdown } from "semantic-ui-react";
import PropTypes from "prop-types";

class StatusSortByDropdown extends React.Component {
  

  render() {
    const {items, defaultRefinement, ...resProp } = this.props;
    return (
      <div className="StatusSortByDropdown">
        Sort by: &nbsp;
        <Dropdown
          inline
          pointing="top bottom"
          options={items}
          defaultValue={defaultRefinement}
          {...resProp}
        />
      </div>
    );
  }
}

// RecentDropdown.propTypes = {
//   className: PropTypes.string
// };

// RecentDropdown.defaultProps = {
//   className: "displayBlock"
// };

export default StatusSortByDropdown;
