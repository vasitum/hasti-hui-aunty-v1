import React from "react";

import { Button } from "semantic-ui-react";

import IcNotificationIcon from "../../../assets/svg/IcNotificationIcon";
import IcDotIcon from "../../../assets/svg/IcDotIcon";

import "./index.scss";

const NotificationNavBar = ({ hasDot, ...resProps }) => {
  return (
    <div className="Notications">
      <span
        className="NotifyDot"
        style={{
          display: hasDot ? "block" : "none"
        }}>
        <IcDotIcon height="10" width="10" pathcolor="#0B9ED0 " />
      </span>
      <Button compact className="Notications_btn" {...resProps}>
        <IcNotificationIcon height="18" pathcolor="#6e768a" />
      </Button>
    </div>
  );
};

export default NotificationNavBar;
