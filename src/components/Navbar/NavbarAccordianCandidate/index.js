import React, { Component } from 'react'

import { Dropdown, List } from "semantic-ui-react";

import { Link } from "react-router-dom";

import AccordionCardContainer from "../../Cards/AccordionCardContainer";
import IcMyCandidates from "../../../assets/svg/IcMyCandidates";
import DownArrowCollaps from "../../utils/DownArrowCollaps";

export default class NavbarAccordianCandidate extends Component {
  render() {
    return (
      <AccordionCardContainer
        cardHeader={
          <div className="navBarAccordion_menu NavbarAccordianCandidate">
            <div className="navBarAccordion_menuLeft">
              <IcMyCandidates
                width="16"
                height="14"
                pathcolor="#acaeb5"
              />Candidate
            </div>
            <div className="navBarAccordion_menuRight">
              <DownArrowCollaps
                width="9"
                height="5"
                pathcolor="#a1a1a1"
              />
            </div>
          </div>
        }>
        <div className="navbar_menuListBody">
          <List className="submenu_box">
            <List.Item
              as={Link}
              to={"/job/candidate/dashboard"}>
              Applied jobs
              </List.Item>
            <List.Item as={Link} to={"/job/candidate/liked"}>
              Liked Jobs
              </List.Item>
            <List.Item
              as={Link}
              to={"/job/candidate/saveJob"}>
              Saved Jobs
              </List.Item>
            <List.Item
              as={Link}
              to={"/job/recruiter/interview"}>
              Interview List
              </List.Item>
          </List>
        </div>
      </AccordionCardContainer>

    )
  }
}
