import React from "react";
import PropTypes from "prop-types";
// import NavbarUser from "./NavbarUser";
import NavbarDefault from "./NavbarDefault";

import { Responsive } from "semantic-ui-react";

import PostLoginMobileHeader from "../MobileComponents/PostLoginMobileHeader";

import { Switch, Route } from "react-router-dom";

import "./index.scss";

/**
 * @augments {Component<{isUserLoggedIn:boolean.isRequired>}
 */
const Navbar = props => (
  <React.Fragment>
    {/* Desktop */}
    <Responsive minWidth={1025}>
      <NavbarDefault {...props} />
    </Responsive>

    {/* Mobile */}
    <Responsive maxWidth={1024}>
      <Switch>
        <Route exact path="/messaging/:id" component={() => <div />} />
        <Route exact path="/job/dashboard" component={() => <div />} />
        <Route exact path="/job/candidate/:id" component={() => <div />} />
        <Route exact path="/job/recruiter/:id" component={() => <div />} />
        <Route exact path="/job/candidateDetail" component={() => <div />} />
        <Route exact path="/job/myJobsDetail" component={() => <div />} />
        <Route exact path="/calendar" component={() => <div />} />
        <Route exact path="/resume-professional" component={() => <div />} />
        <Route exact path="/resume-classic" component={() => <div />} />
        <Route exact path="/resume-modern" component={() => <div />} />
        <Route exact path="/emailVerification" component={() => <div />} />
        <Route exact path="/advance-search" component={() => <div />} />
        <Route component={() => <PostLoginMobileHeader />} />
      </Switch>
    </Responsive>
  </React.Fragment>
);

Navbar.propTypes = {
  isUserLoggedIn: PropTypes.bool.isRequired
};

Navbar.defaultProps = {
  isUserLoggedIn: false
};

export default Navbar;
