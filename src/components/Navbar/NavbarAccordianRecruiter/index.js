import React, { Component } from 'react'

import { Dropdown, List } from "semantic-ui-react";

import { Link } from "react-router-dom";

import AccordionCardContainer from "../../Cards/AccordionCardContainer";
import IcRecruiterIcon from "../../../assets/svg/IcRecruiterIcon";
import DownArrowCollaps from "../../utils/DownArrowCollaps";

export default class NavbarAccordianRecruiter extends Component {
  render() {
    return (
      <AccordionCardContainer
        cardHeader={
          <div className="navBarAccordion_menu">
            <div className="navBarAccordion_menuLeft">
              <IcRecruiterIcon
                width="16"
                height="14"
                pathcolor="#acaeb5"
              />Recruiter
            </div>
            <div className="navBarAccordion_menuRight">
              <DownArrowCollaps pathcolor="#a1a1a1" />
            </div>
          </div>
        }>
        <div className="navbar_menuListBody">
          <List className="submenu_box">
            <List.Item
              as={Link}
              to={"/job/recruiter/dashboard"}>
              My Jobs
            </List.Item>
            <List.Item
              as={Link}
              to={"/job/recruiter/candidates"}>
              Candidates
            </List.Item>

            <List.Item as={Link} to={"/job/recruiter/liked"}>
              Liked Profile
            </List.Item>
            {/* <List.Item as={Link} to={"/job/recruiter/saved"}>
              Saved Profile
            </List.Item> */}
            <List.Item as={Link} to={"/job/recruiter/compaines"}>
              My Companies
            </List.Item>
            <List.Item
              as={Link}
              to={"/job/recruiter/interview"}>
              Interview List
            </List.Item>
          </List>
        </div>
      </AccordionCardContainer>
    )
  }
}
