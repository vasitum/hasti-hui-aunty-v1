import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  Menu,
  Image,
  Icon,
  Input,
  Dropdown,
  Container
} from "semantic-ui-react";

import vasitumLogo from "Assets/img/vasitum_logo.gif";
import profilePostLogin from "Assets/img/profilePostLogin.png";

const navigationLinks = [
  {
    name: "Home",
    icon: "home",
    to: "/"
  },
  {
    name: "Job",
    icon: "suitcase",
    to: "/job"
  },
  {
    name: "Messages",
    icon: "home",
    to: "/messages"
  }
];

const trigger = (
  <div className="userDropdownNavbar">
    <div className="floatLeft padding-top-7">
      <Image avatar src={profilePostLogin} />
    </div>
    <div className="floatLeft ">
      <span className="displayBlock textSlateGray fontSize-12">Hello</span>
      <span className="displayBlock text-pacific-blue fontSize-12">
        Kety Jeff
      </span>
    </div>
    <div className="floatRight padding-top-7 textSlateGray padding-left-10">
      <Icon name="angle down" />
    </div>
  </div>
);

const options = [
  {
    key: "user",
    text: (
      <span>
        Signed in as <strong>Karan Srivastava</strong>
      </span>
    ),
    disabled: true
  },
  { key: "profile", text: "Your Profile" },
  { key: "stars", text: "Your Jobs" },
  { key: "explore", text: "Explore" },
  { key: "integrations", text: "Integrations" },
  { key: "help", text: "Help" },
  { key: "settings", text: "Settings" },
  { key: "sign-out", text: "Sign Out" }
];

export default class extends Component {
  render() {
    return (
      <div className="navbar bg-midnight-xpress">
        <Container>
          <Menu
            attached="top"
            className="navbar-menu bg-midnight-xpress"
            borderless>
            <Menu.Menu position="left">
              <Menu.Item>
                <Image className="vasitum-logo" src={vasitumLogo} />
              </Menu.Item>
            </Menu.Menu>
            <Menu.Menu position="left">
              <div className="navbar-search">
                <Menu.Item>
                  <Input
                    className="icon"
                    style={{ width: "370px" }}
                    icon="search"
                    placeholder="Search..."
                  />
                </Menu.Item>
              </div>
            </Menu.Menu>

            <Menu.Menu position="right">
              {navigationLinks.map(nav => {
                return (
                  <Menu.Item
                    key={nav.name}
                    as={Link}
                    to={nav.to}
                    className="text-white">
                    <Icon name={nav.icon} />
                    {nav.name}
                  </Menu.Item>
                );
              })}
              <Dropdown
                trigger={trigger}
                options={options}
                pointing="top left"
                icon={null}
                className="text-white"
              />
            </Menu.Menu>
          </Menu>
        </Container>
      </div>
    );
  }
}
