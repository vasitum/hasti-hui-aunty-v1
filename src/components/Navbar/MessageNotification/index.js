import React from "react";
import IcDotIcon from "../../../assets/svg/IcDotIcon";
import IcMessage from "../../../assets/svg/IcMessage";
import "./index.scss";

const MessageNotification = ({ hasDot }) => {
  return (
    <div className="MessageNotification">
      <IcMessage width="19" height="18" pathcolor="#6e768a"/>
      {hasDot ? (
        <span className="MessageNotificationDot">
          <IcDotIcon height="9" width="9" pathcolor="#0B9ED0" />
        </span>
      ) : null}
    </div>
  );
};

export default MessageNotification;
