import React from "react";

import { Image, Header, Responsive } from "semantic-ui-react";
import { Link, withRouter } from "react-router-dom";
import IcMyJobs from "../../../assets/svg/IcMyJobs";

import UserLogo from "../../../assets/svg/IcUser";
import IcUpload from "../../../assets/svg/IcUpload";
import IcMyCandidates from "../../../assets/svg/IcMyCandidates";
import IcHome from "../../../assets/svg/IcHome";
import IcMessagingTemplate from "../../../assets/svg/IcMessagingTemplate";
import IcMyCalendar from "../../../assets/svg/IcMyCalendar";
import IcDrawerSignOut from "../../../assets/svg/IcDrawerSignOut";
import { USER } from "../../../constants/api";
import { toast } from "react-toastify";
import PageLoader from "../../PageLoader";
import updateUserExtns from "../../../api/user/updateUserExtns";
import { uploadResume } from "../../../utils/aws";
import FlatFileUploadeBtn from "../../Buttons/FlatFileUploadeBtn";
import FlatFileuploadBtn from "../../Buttons/FlatFileUploadeBtn";

import isLoggedIn from "../../../utils/env/isLoggedin";
import ReactGA from "react-ga";
import { HomePageString } from "../../EventStrings/Homepage";

import NavbarAccordianCandidate from "../NavbarAccordianCandidate";
import NavbarAccordianRecruiter from "../NavbarAccordianRecruiter";
import IcRecruiterIcon from "../../../assets/svg/IcRecruiterIcon";
import IcMessage from "../../../assets/svg/IcMessage";

import "./index.scss";

function hasResume(ext) {
  if (!ext || !ext.resume) {
    return false;
  }

  return true;
}

function getResumePlaceHolder(isResumeLoading, hasResume) {
  if (isResumeLoading) {
    return "Uploading ...";
  }

  return hasResume ? "Update Resume" : "Upload Resume";
}

class NavbarMenuLists extends React.Component {
  state = {
    isResumeButtonLoading: false,
    resumeExtn: "",
    profileJson: {}
  };

  constructor(props) {
    super(props);
    this.onResumeUploaded = this.onResumeUploaded.bind(this);
    this.onResumeChange = this.onResumeChange.bind(this);
  }

  async onResumeUploaded(extn) {
    this.setState({
      profileJson: {
        ...this.state.profileJson,
        ext: {
          ...this.state.profileJson.ext,
          resume: extn
        }
      }
    });

    try {
      const data = await updateUserExtns({
        img: null,
        imgBanner: null,
        resume: this.state.profileJson.ext.resume
      });

      // update it!
      // const res = await data.toString();

      if (data) {
        toast("Resume, Uploaded");
      }
    } catch (error) { }
  }

  async onResumeChange(ev) {
    // const { user, onResumeUploaded } = this.props;
    const file = ev.target.files[0];

    if (!file) {
      return;
    }
    if (this.toast) {
      toast.dismiss(this.toast);
    }
    const userName = window.localStorage.getItem(USER.NAME);
    const uId = window.localStorage.getItem(USER.UID);
    // const extn = file.name.split(".").pop();
    const EXTNS = ["doc", "docx", "rtf", "pdf", "jpeg", "jpg"];
    const extn = file.name.split(".").pop();
    // console.log("EXTN IS", extn);
    if (!extn || EXTNS.indexOf(String(extn).toLowerCase()) === -1) {
      this.toast = toast(
        "Error, please select any one of .doc, .docx, .rtf, .pdf, .jpeg, .jpg"
      );
      return;
    }

    // tell user we are uploading
    this.setState({
      isResumeButtonLoading: true
    });

    try {
      const res = await uploadResume(`${uId}/${userName}.${extn}`, file);
      if (res) {
        // toast("Resume Updated");
      }

      // tell app that resume is updated
      this.onResumeUploaded(extn);
    } catch (error) {
      console.error(error);
    }

    this.setState({
      isResumeButtonLoading: false
    });
  }

  render() {
    const onSignOutClick = () => {
      window.localStorage.clear();
      window.location.href = "/";
    };

    function hasBasicProfileDetails() {
      const name = window.localStorage.getItem(USER.NAME);
      const mobile = window.localStorage.getItem(USER.PHONE);
      const userImage = window.localStorage.getItem(USER.IMG_EXT);

      if (
        !name ||
        name === "undefined" ||
        name === "null" ||
        !mobile ||
        mobile === "undefined" ||
        mobile === "null"
      ) {
        return false;
      } else {
        return true;
      }
    }

    const {
      isShowMobile,
      isShowMobileCandidate,
      isShowMobileRecrutment
    } = this.props;

    // console.log("check user title", USER);

    const userImage = window.localStorage.getItem(USER.IMG_EXT);
    const userName = window.localStorage.getItem(USER.NAME);
    const uId = window.localStorage.getItem(USER.UID);
    const userTitle = window.localStorage.getItem(USER.TITLE);

    const userhasResume = hasResume(uId.ext);

    // console.log("check userName", userName);

    return (
      <div className="NavbarMenuLists">
        <div
          className="NavbarMenuList_header"
          onClick={() => {
            ReactGA.event({
              category: HomePageString.userProfile.category,
              action: HomePageString.userProfile.action,
              value: isLoggedIn() ? "" : uId,
            });
            hasBasicProfileDetails()
              ? this.props.history.push("/user/view/profile")
              : this.props.history.push("/user/new");
          }}>
          <div className="NavbarMenuList_headerLogo">
            {/* <UserLogo
            width="35"
            height="35"
            rectcolor="#f7f7fb"
            pathcolor="#c8c8c8"
          /> */}
            {!userImage || userImage === "undefined" ? (
              <UserLogo
                width="48"
                height="48"
                rectcolor="#f7f7fb"
                pathcolor="#c8c8c8"
              />
            ) : (
                <Image
                  avatar
                  src={`https://s3-us-west-2.amazonaws.com/img-mwf/${uId}/image.jpg`}
                />
              )}
          </div>
          <div className="NavbarMenuList_headerTitle">
            <p className="title">
              {!userName || userName === "null null" ? "Guest" : userName}
            </p>
            <p className="subTitle">
              {!userTitle || userTitle === "null" ? "" : userTitle}
            </p>
          </div>
        </div>
        <div className="NavbarMenuList_body">
          <Responsive maxWidth={1024}>
            <div className="NavbarMenuListItems">
              <Header
                as={Link} 
                to="/"
                onClick={() => {
                  ReactGA.event({
                    category: HomePageString.HomeEvent.category,
                    action: HomePageString.HomeEvent.action,
                    value: isLoggedIn() ? "" : uId,
                  });
                }}
              >
                <IcHome width="16" height="14" pathcolor="#acaeb5" /> Home
              </Header>
            </div>
          </Responsive>
          <div className="NavbarMenuListItems">
            <Header
              as={Link}
              to="/job/new"
              onClick={() => {
                ReactGA.event({
                  category: HomePageString.PostJob.category,
                  action: HomePageString.PostJob.action,
                  value: isLoggedIn() ? "" : uId,
                });
              }}
            >
              <IcMyJobs width="16" height="14" pathcolor="#acaeb5" /> Post a job
            </Header>
          </div>
          <Responsive maxWidth={1024}>
            <div className="NavbarMenuListItems">
              <Header
                as={Link}
                to="/messaging"
                onClick={() => {
                  ReactGA.event({
                    category: HomePageString.messaging.category,
                    action: HomePageString.messaging.action,
                    value: isLoggedIn() ? "" : uId,
                  });
                }}
              >
                <IcMessage width="16" height="14" pathcolor="#acaeb5" />{" "}
                Messages
              </Header>
            </div>
          </Responsive>
          {/* <div className="NavbarMenuListItems">
            
            <FlatFileuploadBtn
              onChange={this.onResumeChange}
              btnIcon={<IcUpload
                width="16"
                height="14"
                pathcolor="#acaeb5"
              />}
              accept=".doc, .docx, .rtf, .pdf, .jpeg, .jpg"
              btnText={getResumePlaceHolder(
                this.state.isResumeButtonLoading,
                userhasResume
              )}
            />
          </div> */}

          <div className="NavbarMenuListItems">
            <div className="listItemsHeader">
              <p>Dashboard</p>
            </div>
            {!isShowMobileCandidate ? (
              <div className="listItemsbody">
                <Header
                  as={Link}
                  to="/job/candidate/dashboard"
                  onClick={() => {
                    ReactGA.event({
                      category: HomePageString.Candidate.category,
                      action: HomePageString.Candidate.action,
                      value: isLoggedIn() ? "" : uId,
                    });
                  }}
                >
                  <IcMyCandidates width="16" height="14" pathcolor="#acaeb5" />
                  Candidate
                </Header>
              </div>
            ) : (
                <NavbarAccordianCandidate />
              )}
          </div>
          <div className="NavbarMenuListItems">
            {!isShowMobileRecrutment ? (
              <div className="listItemsbody">
                <Header
                  as={Link}
                  to="/job/recruiter/dashboard"
                  onClick={() => {
                    ReactGA.event({
                      category: HomePageString.Recruiter.category,
                      action: HomePageString.Recruiter.action,
                      value: isLoggedIn() ? "" : uId,
                    });
                  }}
                >
                  <IcRecruiterIcon width="16" height="14" pathcolor="#acaeb5" />
                  Recruiter
                </Header>
              </div>
            ) : (
                <NavbarAccordianRecruiter />
              )}
          </div>

          <div className="NavbarMenuListItems">
            <div className="listItemsHeader">
              <p>Manage</p>
            </div>
            <div className="listItemsbody">
              <Header 
                as={Link} 
                to="/job/managetemplate"
                onClick={() => {
                  ReactGA.event({
                    category: HomePageString.ManageTemplates.category,
                    action: HomePageString.ManageTemplates.action,
                    value: isLoggedIn() ? "" : uId,
                  });
                }}
              >
                <IcMessagingTemplate
                  width="16"
                  height="14"
                  pathcolor="#acaeb5"
                />
                Templates
              </Header>
            </div>
          </div>
          <div className="NavbarMenuListItems">
            <div className="listItemsbody">
              <Header 
                as={Link} 
                to="/calendar"
                onClick={() => {
                  ReactGA.event({
                    category: HomePageString.Calendar.category,
                    action: HomePageString.Calendar.action,
                    value: isLoggedIn() ? "" : uId,
                  });
                }}
              >
                <IcMyCalendar width="16" height="14" pathcolor="#acaeb5" />
                Calendar
              </Header>
            </div>
          </div>

          {/* <div className="NavbarMenuListItems">
          <div className="listItemsHeader">
            <p>Account</p>
          </div>
          <div className="listItemsbody">
            <IcMyJobs
              width="16"
              height="14"
              pathcolor="#acaeb5"
            />
            <Header as={Link} to="">Settings and Privacy</Header>
          </div>
        </div> */}
        </div>
        {!isShowMobile ? (
          <div className="NavbarMenuList_footer" onClick={onSignOutClick}>
            <Header
              onClick={() => {
                ReactGA.event({
                  category: HomePageString.SignOut.category,
                  action: HomePageString.SignOut.action,
                  value: isLoggedIn() ? "" : uId,
                });
              }} 
            >
              <IcDrawerSignOut width="16" height="14" pathcolor="#a1a1a1" />
              Sign Out
            </Header>
          </div>
        ) : null}
        <PageLoader
          active={this.state.isResumeButtonLoading}
          loaderText={"Updating Resume"}
        />
      </div>
    );
  }
}

// const NavbarMenuLists = props => {

//   return (

//   )
// }

export default withRouter(NavbarMenuLists);
