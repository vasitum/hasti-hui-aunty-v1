import React, { Component } from "react";
import ReactGA from "react-ga";
import { connect } from "react-redux";
import {
  Menu,
  Image,
  Input,
  Container,
  Form,
  Button,
  Dropdown,
  Header,
  Icon,
  Ref,
  List
} from "semantic-ui-react";
import UserAccessModal from "../../components/ModalPageSection/UserAccessModal/";
// import { onInputChange } from "Actions/search";
import { onInputChange } from "../../actions/search";

import { withAlgoliaContext } from "../../contexts/AlgoliaContext";

import vasitumLogo from "../../assets/logo/vasitum_primary.gif";
import SearchIcon from "../../assets/svg/SearchIcon";
import UserLogo from "../../assets/svg/IcUser";

import debounce from "../../utils/env/debounce";

import AccordionCardContainer from "../Cards/AccordionCardContainer";

// import IcNotification from "../../assets/svg/Ic";

import { Link, NavLink, withRouter } from "react-router-dom";

import { USER } from "../../constants/api";
import IcHome from "../../assets/svg/IcHome";
import Icjob from "../../assets/svg/Icjob";
import IcMessage from "../../assets/svg/IcMessage";
import IcNotificationIcon from "../../assets/svg/IcNotificationIcon";
import IcDotIcon from "../../assets/svg/IcDotIcon";
import NotificationNavBar from "./NotificationNavBar";
import MessageNotification from "./MessageNotification";

import DownArrowCollaps from "../utils/DownArrowCollaps";

import NavbarAccordianCandidate from "./NavbarAccordianCandidate";
import NavbarAccordianRecruiter from "./NavbarAccordianRecruiter";

import IcVasitumLogoTransparent from "../../assets/svg/IcVasitumLogoTransparent";
import IcVasitumWorkmark from "../../assets/svg/IcVasitumWorkmark";
import IcVasitumLogotype from "../../assets/svg/IcVasitumLogotype";
import NavbarMenuLists from "./NavbarMenuLists";
import BorderButton from "../Buttons/BorderButton";
import { HomePageString } from "../EventStrings/Homepage";

class Notications extends React.Component {
  constructor(props) {
    super(props);
    // document.addEventListener("NOTIF", e => {
    //   this.onNotification();
    // });

    // document.addEventListener("NOTIF_NO", e => {
    //   this.onNoNotification();
    // });
  }

  state = {
    showDot: false
  };

  onNotification = () => {
    this.setState({ showDot: true });
  };

  onNoNotification = () => {
    this.setState({ showDot: false });
  };

  render() {
    return (
      <div className="Notications">
        <span className="NotifyDot">
          {this.state.showDot ? (
            <IcDotIcon height="10" width="10" pathcolor="#0B9ED0 " />
          ) : null}
        </span>
        <Button compact className="Notications_btn">
          <IcNotificationIcon pathcolor="#6e768a" />
        </Button>
      </div>
    );
  }
}

function getNavLinks(hasDot, hasMessageDot) {
  return [
    {
      key: 1,
      name: <IcHome pathcolor="#6e768a" height="18" width="20" />,
      to: "/",
      title: "Home",
      onClick: () => {
        ReactGA.event({
          category: HomePageString.HomeEvent.category,
          action: HomePageString.HomeEvent.action,
          value: this.props.isUserLoggedIn ? "" : UserId,
        });
      }
    },
    // {
    //   key: 2,
    //   name: <Icjob pathcolor="#6e768a" height="18"/>,
    //   to: "/job/new"
    // },

    {
      key: 3,
      name: <MessageNotification hasDot={hasMessageDot} />,
      to: "/messaging",
      title: "Messaging",
      onClick: () => {
        ReactGA.event({
          category: HomePageString.messaging.category,
          action: HomePageString.messaging.action,
          value: this.props.isUserLoggedIn ? "" : UserId,
        });
      }
    },
    {
      key: 4,
      name: <NotificationNavBar hasDot={hasDot} />,
      to: "/notification",
      title: "Notification",
      onClick: () => {
        ReactGA.event({
          category: HomePageString.notification.category,
          action: HomePageString.notification.action,
          value: this.props.isUserLoggedIn ? "" : UserId,
        });
      }
    }
  ];
}

function hasBasicProfileDetails() {
  const name = window.localStorage.getItem(USER.NAME);
  const mobile = window.localStorage.getItem(USER.PHONE);
  const userImage = window.localStorage.getItem(USER.IMG_EXT);

  if (
    !name ||
    name === "undefined" ||
    name === "null" ||
    !mobile ||
    mobile === "undefined" ||
    mobile === "null"
  ) {
    return false;
  } else {
    return true;
  }
}

const UserId = window.localStorage.getItem(USER.UID);

class NavbarDefault extends Component {
  state = {
    searchText: this.props.searchValue || "",
    hasNotification: false,
    hasMessage: false
  };

  setNotifState = e => {
    // console.log("New Notification Found!");
    if (!this.state.hasNotification) {
      this.setState(
        state => {
          return {
            ...state,
            hasNotification: true
          };
        },
        () => {
          console.log("Ticker Added!");
        }
      );
    }
  };

  setMessageState = e => {
    // console.log("New Message Found!");
    if (!this.state.hasMessage) {
      this.setState(
        state => {
          return {
            ...state,
            hasMessage: true
          };
        },
        () => {
          console.log("Message Added!");
        }
      );
    }
  };

  removeNotifState = e => {
    if (this.state.hasNotification) {
      this.setState(
        state => {
          return {
            ...state,
            hasNotification: false
          };
        },
        () => {
          console.log("Ticker Removed!");
        }
      );
    }
  };

  removeMsgState = e => {
    if (this.state.hasMessage) {
      this.setState(
        state => {
          return {
            ...state,
            hasMessage: false
          };
        },
        () => {
          console.log("Message Removed!");
        }
      );
    }
  };

  constructor(props) {
    super(props);
    this.formRef = null;
  }

  setUpScrollNav = scrollPos => {
    if (window.location.pathname !== "/") {
      return;
    }

    const currentScrollPos = window.pageYOffset;
    if (currentScrollPos < 250) {
      this.formRef.classList.remove("nav_ShowForm");
      this.formRef.classList.add("nav_HideForm");
    } else {
      this.formRef.classList.remove("nav_HideForm");
      this.formRef.classList.add("nav_ShowForm");
    }
    // scrollPos = currentScrollPos;
  };

  UNSAFE_componentWillReceiveProps() {
    // reset everything
    this.formRef.classList.remove("nav_ShowForm", "nav_HideForm");

    // check if home page and setup scroll listener
    if (window.location.pathname === "/") {
      this.removeScrollHandler();
      this.setUpScrollHandler();
      if (window.pageYOffset < 250) {
        this.formRef.classList.add("nav_HideForm");
      }
    } else {
      this.removeScrollHandler();
      this.formRef.classList.add("nav_ShowForm");
    }
  }

  setUpScrollHandler = e => {
    const prevScrollpos = window.pageYOffset;
    window.addEventListener("scroll", debounce(this.setUpScrollNav, 300));
  };

  removeScrollHandler = e => {
    window.removeEventListener("scroll", debounce(this.setUpScrollNav, 300));
  };

  componentDidMount() {
    // console.log(this.props);


    document.addEventListener("NOTIF", this.setNotifState);
    document.addEventListener("NOTIF_NO", this.removeNotifState);

    document.addEventListener("MESSAGE", this.setMessageState);
    document.addEventListener("MSG_NO", this.removeMsgState);
  }

  componentWillUnmount() {
    document.removeEventListener("NOTIF", this.setNotifState);
    document.removeEventListener("NOTIF_NO", this.removeNotifState);

    document.removeEventListener("MESSAGE", this.setMessageState);
    document.removeEventListener("MSG_NO", this.removeMsgState);
    this.removeScrollHandler();
  }

  options = [
    {
      key: "profile",
      text: "My Profile",
      onClick: e => {
        hasBasicProfileDetails()
          ? this.props.history.push("/user/view/profile")
          : this.props.history.push("/user/new");
      }
    },
    // {
    //   key: "Notications",
    //   text: "Notifications",
    //   onClick: e => {
    //     hasBasicProfileDetails()
    //       ? this.props.history.push("/notification")
    //       : this.props.history.push("/notification");
    //   }
    // },
    {
      key: "companies",
      text: "My Companies",
      onClick: e => {
        this.props.history.push("/company");
      }
    },
    {
      key: "manage_temnplate",
      text: "Manage Template",
      onClick: e => {
        this.props.history.push("/job/managetemplate");
      }
    },
    {
      key: "post_job",
      text: "Post a Job",
      onClick: e => {
        this.props.history.push("/job/new");
      }
    },
    {
      key: "divider3",
      text: <div className="dividerNavbar" />,
      className: "dividerNav"
      // onClick: e => {
      //   this.props.history.push("/job/candidate/liked");
      // }
    },
    {
      key: "cand_dash",
      text: "co"
      // onClick: e => {
      //   this.props.history.push("/job/candidate/dashboard");
      // }
    },
    {
      key: "cand_dash_LIKE",
      text: "Liked Jobs",
      onClick: e => {
        this.props.history.push("/job/candidate/liked");
      }
    },
    {
      key: "cand_dash_SAVED",
      text: "Saved Jobs",
      onClick: e => {
        this.props.history.push("/job/candidate/saveJob");
      }
    },
    {
      key: "divider",
      text: <div className="dividerNavbar" />,
      // onClick: e => {
      //   this.props.history.push("/job/candidate/liked");
      // }
      className: "dividerNav"
    },
    {
      key: "recr_dash",
      text: "Recruiter Dashboard",
      onClick: e => {
        this.props.history.push("/job/recruiter/dashboard");
      }
    },
    {
      key: "recr_dash_LIKED",
      text: "Liked Profiles",
      onClick: e => {
        this.props.history.push("/job/recruiter/liked");
      }
    },
    {
      key: "recr_dash_SAVED",
      text: "Saved Profiles",
      onClick: e => {
        this.props.history.push("/job/recruiter/saved");
      }
    },
    {
      key: "divider2",
      text: <div className="dividerNavbar" />,
      className: "dividerNav"
      // onClick: e => {
      //   this.props.history.push("/job/candidate/liked");
      // }
    },
    {
      key: "sign-out",
      text: "Sign Out",
      onClick: () => {
        this.onSignOutClick();
      }
    }
  ];

  onInputChange = ev => {
    this.setState({
      searchText: ev.target.value
    });
  };


  onSearchChange = ev => {
    // this.props.onSearchInputChange(this.state.searchText);
    // console.log("onSearchChage");
    if (!this.state.searchText) {
      return;
    }

    

    ReactGA.event({
      category: HomePageString.Search.category,
      action: HomePageString.Search.action,
      label: this.props.isUserLoggedIn ? "" : UserId,
      // label: "jfhfhfhf"
    });


    this.props.onSearhContextChange(this.state.searchText);

    // redirect if we are not search page
    if (!this.props.location.pathname.startsWith("/search")) {
      this.props.history.push({
        pathname: "/search",
        search: `?query=${encodeURIComponent(this.state.searchText)}`
      });
    } else {
      this.props.history.push({
        search: `?query=${encodeURIComponent(this.state.searchText)}`
      });
    }
  };

  onSignOutClick = ev => {
    window.localStorage.clear();
    window.location.href = "/";
  };

  isPreviewPage() {
    const { location } = this.props;
    if (
      location.pathname === "/user/preview" ||
      location.pathname === "/job/preview"
    ) {
      return true;
    }

    return false;
  }

  componentWillReceiveProps(nextProps) {
    // correct conditions
    // if (this.state.searchText !== nextProps.searchValue) {
    //   this.setState({
    //     searchText: nextProps.searchValue
    //   });
    // }

    if (this.state.searchText !== nextProps.searchQuery) {
      this.setState({
        searchText: nextProps.searchQuery
      });
    }
  }

  render() {
    const userImage = window.localStorage.getItem(USER.IMG_EXT);
    const userName = window.localStorage.getItem(USER.NAME);
    const uId = window.localStorage.getItem(USER.UID);
    // console.log("USER IMAGE", userImage);
    
    const HomeLogo = () => {
      ReactGA.event({
        
        category: HomePageString.HomeEvent.category,
        action: HomePageString.HomeEvent.action,
        value: this.props.isUserLoggedIn ? "" : UserId,
      })
    }
    
    const PostJobEvent = () => {
      ReactGA.event({
        
        category: HomePageString.PostJob.category,
        action: HomePageString.PostJob.action,
        value: this.props.isUserLoggedIn ? "" : UserId,
      })
    }


    return (
      <div
        className="Navbar"
      // style={{
      //   display: this.isPreviewPage() ? "none" : "block"
      // }}
      >
        <div className="Navbar_container">
          <div className="NavbarInnert">
            <div className="NavbarInnert_left">
              <Header 
                as={Link} 
                to="/"
                onClick={HomeLogo}
               >
                {/* <IcVasitumLogoTransparent pathcolor="#F5F9EC"/> */}
                <div className="leftLogo">
                  <IcVasitumWorkmark pathcolor="#ffffff" />
                </div>
                <div className="rightLogo">
                  <IcVasitumLogotype pathcolor="#ffffff" />
                </div>
                {/* <Image className="vasitum-logo" src={vasitumLogo} /> */}
              </Header>
              <div className="navBar_form">
                {/* {this.props.isUserLoggedIn ? (<Form onSubmit={this.onSearchChange}>
                  <Input
                    className="icon"
                    value={this.state.searchText}
                    onChange={this.onInputChange}
                    placeholder="Search..."
                  />
                  <SearchIcon
                    onClick={this.onSearchChange}
                    pathcolor="#a1a1a1"
                    height="15.438"
                    width="17.093"
                  />
                 
                  <button type="submit" className="is-hidden" />
                </Form>): null } */}
                <Ref innerRef={ref => (this.formRef = ref)}>
                  <Form onSubmit={this.onSearchChange}>
                    <Input
                      className="icon"
                      value={decodeURIComponent(this.state.searchText)}
                      onChange={this.onInputChange}
                      placeholder="Search Jobs/ People/ Companies"
                    />
                    <SearchIcon
                      onClick={this.onSearchChange}
                      pathcolor={this.state.searchText ? "#1f2532" : "#eaeaee"}
                      height="15.438"
                      width="17.093"
                    />
                    {/* gotta submit form somehow */}
                    <button type="submit" className="is-hidden" />
                  </Form>
                </Ref>

                {/* {this.props.isUserLoggedIn ||
                window.location.pathname === "/job/new" ? null : (
                  <div className="navPostJobBtn">
                    <Button as={Link} to="/job/new">
                      Post job
                    </Button>
                  </div>
                )} */}

                {this.props.isUserLoggedIn ? (
                  <UserAccessModal
                    modalTrigger={
                      <BorderButton
                        className="btn"
                        btnText="Post a job"
                        style={{ marginLeft: "15px" }}
                        onClick={PostJobEvent}
                      />
                    }
                  />
                ) : (
                    <BorderButton
                      className={
                        window.location.pathname === "/job/new"
                          ? "btn-border"
                          : "btn"
                      }
                      btnText="Post a job"
                      style={{ marginLeft: "15px" }}
                      as={Link}
                      onClick={PostJobEvent}
                      to="/job/new"
                    />
                  )}
              </div>
            </div>
            <div className="NavbarInnert_right">
              {this.props.isUserLoggedIn ? (
                <React.Fragment>
                  <UserAccessModal />
                </React.Fragment>
              ) : (
                  <React.Fragment>
                    {getNavLinks(
                      this.state.hasNotification,
                      this.state.hasMessage
                    ).map(nav => {
                      return (
                        <Header
                          key={nav.key}
                          as={NavLink}
                          to={nav.to}
                          exact={true}
                          title={nav.title}
                          onClick={nav.onClick}
                          className="topNavbarList">
                          {nav.name}
                          
                        </Header>
                      );
                    })}
                    <Dropdown
                      scrolling={true}
                      className="NavbarDropdown"
                      icon={null}
                      text={
                        <div className="userDropdownNavbar">
                          <div className="UserLogo_navBar">
                            {!userImage || userImage === "undefined" ? (
                              <UserLogo
                                width="35"
                                height="35"
                                rectcolor="#f7f7fb"
                                pathcolor="#c8c8c8"
                              />
                            ) : (
                                <Image
                                  avatar
                                  src={`https://s3-us-west-2.amazonaws.com/img-mwf/${uId}/image.jpg`}
                                />
                              )}
                          </div>
                          {/* <div className="textSlateGray padding-left-10">
                          <Icon name="angle down" />
                        </div> */}
                          <div className="userDropdownNavbar_right">
                            <div className="userTitle">Hello</div>
                            {/* <br /> */}
                            <div className="userSubTitle">
                              {!userName || userName === "null null"
                                ? "Guest"
                                : userName.split(" ")[0]}
                            </div>
                          </div>
                          <div className="userNavrRight_icon">
                            <Icon name="angle down" />
                          </div>
                        </div>
                      }
                      pointing="top right">
                      <Dropdown.Menu>
                        <NavbarMenuLists
                        // userImage={userImage}
                        // userName={userName}
                        // uId={uId}
                        />
                        {/* <Dropdown.Item
                        onClick={() => {
                          hasBasicProfileDetails()
                            ? this.props.history.push("/user/view/profile")
                            : this.props.history.push("/user/new");
                        }}>
                        My Profile
                      </Dropdown.Item>
                      <Dropdown.Item
                        onClick={() => {
                          this.props.history.push("/job/new");
                        }}>
                        Post a Job
                      </Dropdown.Item>

                      <Dropdown.Item
                        onClick={() => {
                          this.props.history.push("/job/managetemplate");
                        }}>
                        Manage Template
                      </Dropdown.Item>

                      <Dropdown.Item
                        onClick={() => {
                          this.props.history.push("/calendar");
                        }}>
                        My Calendar
                      </Dropdown.Item>

                      <Dropdown.Item
                        className="navbarAccondian_list"
                        onClick={e => {
                          e.preventDefault();
                          e.stopPropagation();
                        }}>
                        <NavbarAccordianCandidate />
                      </Dropdown.Item>

                      <Dropdown.Item
                        className="navbarAccondian_list"
                        onClick={e => {
                          e.preventDefault();
                          e.stopPropagation();
                        }}>
                        <NavbarAccordianRecruiter />
                      </Dropdown.Item>
                      <Dropdown.Divider />

                      
                      <Dropdown.Item
                        onClick={() => {
                          this.onSignOutClick();
                        }}>
                        Sign Out
                      </Dropdown.Item> */}
                      </Dropdown.Menu>
                    </Dropdown>
                    {/* <Dropdown
                    trigger={
                      <div className="userDropdownNavbar">
                        <div className="UserLogo_navBar">
                          {!userImage || userImage === "undefined" ? (
                            <UserLogo
                              width="35"
                              height="35"
                              rectcolor="#f7f7fb"
                              pathcolor="#c8c8c8"
                            />
                          ) : (
                            <Image
                              avatar
                              src={`https://s3-us-west-2.amazonaws.com/img-mwf/${uId}/image.jpg`}
                            />
                          )}
                        </div>
                        <div className="textSlateGray padding-left-10">
                          <Icon name="angle down" />
                        </div>
                      </div>
                    }
                    options={this.options}
                    pointing="top right"
                    icon={null}
                  /> */}
                  </React.Fragment>
                )}
            </div>
          </div>
          {/* <Menu attached="top" borderless className="navBar_form">
            <Menu.Menu>
              <Menu.Item as={Link} to="/">
                <Image className="vasitum-logo" src={vasitumLogo} />
              </Menu.Item>
            </Menu.Menu>
            <Menu.Menu className="">
              <Menu.Item >
                <Form onSubmit={this.onSearchChange}>
                  <Input
                    className="icon"
                    value={this.state.searchText}
                    onChange={this.onInputChange}
                    placeholder="Search..."
                  />
                  <SearchIcon
                    onClick={this.onSearchChange}
                    pathcolor="#a1a1a1"
                    height="15.438"
                    width="17.093"
                  />
                  
                  <button type="submit" className="is-hidden" />
                </Form>

              </Menu.Item>

              <Menu.Item className="navPostJobBtn">
                <Button>Post my job</Button>
              </Menu.Item>
            </Menu.Menu>
            <Menu.Menu className="navBar_listMenu">
              <Menu.Item>
                {this.props.isUserLoggedIn ? (
                  <UserAccessModal />
                ) : (
                    <React.Fragment>
                      {getNavLinks(
                        this.state.hasNotification,
                        this.state.hasMessage
                      ).map(nav => {
                        return (
                          <Menu.Item
                            key={nav.key}
                            as={Link}
                            to={nav.to}
                            className="text-white">
                            {nav.name}
                          </Menu.Item>
                        );
                      })}
                      <Dropdown
                        trigger={
                          <div className="userDropdownNavbar">
                            <div className="">
                              {userImage === null ? (
                                <UserLogo
                                  width="35"
                                  height="35"
                                  rectcolor="#f7f7fb"
                                  pathcolor="#c8c8c8"
                                />
                              ) : (
                                  <Image avatar src={userImage} />
                                )}
                            </div>
                            <div className="padding-top-7 textSlateGray padding-left-10">
                              <Icon name="angle down" />
                            </div>
                          </div>
                        }
                        options={this.options}
                        pointing="top right"
                        icon={null}
                      />
                    </React.Fragment>
                  )}
              </Menu.Item>
            </Menu.Menu>
          </Menu> */}
        </div>
      </div>
    );
  }
}

// export default NavbarDefault;
const mapStateToProps = state => {
  return {
    searchValue: state.search.currentValue
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onSearchInputChange: data => {
      dispatch(onInputChange(data));
    }
  };
};

export default withRouter(
  // connect(
  //   mapStateToProps,
  //   mapDispatchToProps
  // )(NavbarDefault)
  withAlgoliaContext(NavbarDefault)
);
