import React from "react";

import { Grid, Header, Button } from "semantic-ui-react";
import PropTypes from "prop-types";

import IcCloseIcon from "../../assets/svg/IcCloseIcon";

import "./index.scss";

const ModalBanner = props => {
  const { children, headerText, headerIcon } = props;

  return (
    <div className="ModalBanner">
      <Grid>
        <Grid.Row>
          <Grid.Column computer={11} className="banner_HeaderText">
            <Header as="h3">
              {headerText} {headerIcon}
            </Header>
          </Grid.Column>
          <Grid.Column computer={5}>
            <div className="banner_HeaderBtn">{children}</div>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
};

ModalBanner.propTypes = {
  placeholder: PropTypes.string
};

export default ModalBanner;
