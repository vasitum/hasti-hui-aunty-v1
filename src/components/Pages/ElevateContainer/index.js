import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";

import { Container } from "semantic-ui-react";

import "./index.scss";

const ElevateContainer = ({ size, children, ...props }) => {
  const classes = cx({
    ElevateContainer: true,
    [`is-${size}`]: true
  });

  return (
    <div {...props} className={classes}>
      <Container>{children}</Container>
    </div>
  );
};

ElevateContainer.propTypes = {};

export default ElevateContainer;
