import React from "react";

import "./index.scss";

import ManageCompaniesContainer from "../ManageCompaniesContainer";

import MobileHeader from "../../MobileComponents/MobileHeader";
import DashBoardNavigationDrawer from "../../MobileComponents/DashBoardNavigationDrawer";

import "./index.scss";

class ManageCompniesMobile extends React.Component {

  render() {


    return (
      <div className="ManageCompniesMobile">
        <div className="ManageCompniesMobile_header">
          <MobileHeader
            headerLeftIcon={<DashBoardNavigationDrawer />}
            className="isMobileHeader_fixe"
            // onClick={() => this.props.history.go(-1)}
            headerTitle="Compaines"
          />
        </div>
        <div className="ManageCompniesMobile_body">
          <ManageCompaniesContainer
            isCardHeaderShow
            gridColumnLeft="16"
            gridColumnRight="16"
          />
        </div>




      </div>
    );
  }
}

export default ManageCompniesMobile;
