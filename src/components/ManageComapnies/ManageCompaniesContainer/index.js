import React from "react";

import {
  Header,
  Container,
  Card,
  Image,
  Grid,
  Button,
  Modal
} from "semantic-ui-react";

import PageBanner from "../../Banners/PageBanner";

import manageCompany from "../../../assets/banners/manageCompany.jpg";
import IcUserIcon from "../../../assets/svg/IcUser";
import IcEditIcon from "../../../assets/svg/IcEdit";

import ManegeCard from "../../Cards/ManegeCard";
import CreateCompanyPage from "../../CreateCompanyPage";

import getCompById from "../../../api/company/getCompByUserId";
import deleteCompany from "../../../api/company/deleteCompany";

import IcCarrer from "../../../assets/svg/IcCarrer";
import IcRelationship from "../../../assets/svg/IcRelationship";
import IcRaiseAwareness from "../../../assets/svg/IcRaiseAwareness";
import NoFoundMessageDashboard from "../../Dashboard/Common/NoFoundMessageDashboard";
import IcPush from "../../../assets/svg/IcPlus";

import ActionBtn from "../../Buttons/ActionBtn";

import { MANAGE_COMPANY } from "../../../strings";

import PageLoader from "../../PageLoader";

import PropTypes from "prop-types";

import "./index.scss";
// const infoicon = <IcInfoIcon pathcolor="#c8c8c8" />;

class ManageCompaniesContainer extends React.Component {
  state = {
    content: [],
    isLoading: false,
    loaderText: "",
    isCreateModalOpen: false
  };

  constructor(props) {
    super(props);
    this.onCompanyDelete = this.onCompanyDelete.bind(this);
  }

  onCreateModalOpen = e => {
    this.setState({ isCreateModalOpen: true });
  };

  onCreateModalClose = e => {
    this.setState({ isCreateModalOpen: false });
  };

  async componentDidMount() {
    try {
      const res = await getCompById();
      const data = await res.json();

      this.setState({
        content: data.content
      });

      // console.log(data);
    } catch (error) {
      console.error(error);
    }
  }

  onCompanyCreate = ev => {
    window.location.reload();
  };

  onEnableFormLoader = text => {
    this.setState({
      isLoading: true,
      loaderText: text
    });
  };

  async onCompanyDelete(id) {
    if (!window.confirm("Do you want to delete company?")) {
      return;
    }

    try {
      const res = await deleteCompany(id);
      const data = await res.text();
      // console.log(data);

      // reload page after you delete the company
      window.location.reload();
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const data = [
      {
        header: <Card.Header>{MANAGE_COMPANY.CARD_1.HEADER_TITLE}</Card.Header>,
        description: (
          <Card.Description>{MANAGE_COMPANY.CARD_1.DESC}</Card.Description>
        ),
        cardIcon: (
          <IcRaiseAwareness pathcolor="#0b9ed0" pathcolorSecond="#0b9ed0" />
        )
      },
      {
        header: <Card.Header>{MANAGE_COMPANY.CARD_2.HEADER_TITLE}</Card.Header>,
        description: (
          <Card.Description>{MANAGE_COMPANY.CARD_2.DESC}</Card.Description>
        ),
        cardIcon: <IcCarrer pathcolor="#0b9ed0" />
      },
      {
        header: <Card.Header>{MANAGE_COMPANY.CARD_3.HEADER_TITLE}</Card.Header>,
        description: (
          <Card.Description>{MANAGE_COMPANY.CARD_3.DESC}</Card.Description>
        ),
        cardIcon: <IcRelationship pathcolor="#0b9ed0" />
      }
    ];

    const {
      isCardHeaderShow,
      isCardHeaderLeft,
      gridColumnLeft,
      gridColumnRight
    } = this.props;
    return (
      <div className="ManageCompnies">
        {/* <PageBanner compHack size="medium" image={manageCompany}>
          <Container>
            <div className="banner_header">
              <Header as="h1">{MANAGE_COMPANY.HEADER_TITLE}</Header>
            </div>
          </Container>
        </PageBanner> */}

        <Container>
          {/* {!isCardHeaderShow ? (
            <div className="manageCompany_header">
              <Grid>
                <Grid.Row>
                  {data.map((item, idx) => {
                    return (
                      <Grid.Column mobile={16} computer={5} key={idx}>
                        <Card idx={idx} fluid className="manageCompany_card">
                          <Card.Content textAlign="center">
                            {item.cardIcon}
                            {item.header}
                            {item.description}
                          </Card.Content>
                        </Card>
                      </Grid.Column>
                    );
                  })}
                </Grid.Row>
              </Grid>
            </div>
          ) : null} */}

          {this.state.content.length === 0 ? (
            <div>
              <NoFoundMessageDashboard
                noFountTitle="No data found"
                noFountSubTitle=""
              />
              <div className="Nodata_foundBtn">
                <Modal
                  open={this.state.isCreateModalOpen}
                  onClose={this.onCreateModalClose}
                  className="modal_form"
                  trigger={
                    <Button
                      onClick={this.onCreateModalOpen}
                      primary
                      className="header_btn">
                      <IcPush pathcolor="#ceeefe" />
                      Create company
                    </Button>
                  }
                  closeIcon
                  closeOnDimmerClick={false}>
                  <CreateCompanyPage
                    onModalClose={this.onCreateModalClose}
                    onEnableFormLoader={this.onEnableFormLoader}
                    onCompanyCreate={this.onCompanyCreate}
                  />
                </Modal>
              </div>
            </div>
          ) : (
            <div className="manageCompany_body">
              <div className="header_title">
                <Grid>
                  <Grid.Row>
                    <Grid.Column
                      width={gridColumnLeft}
                      className="title gridColumnLeft">
                      <Header as="h2">Company Pages</Header>
                    </Grid.Column>
                    <Grid.Column
                      className="gridColumnRight"
                      width={gridColumnRight}
                      textAlign="right">
                      <Modal
                        open={this.state.isCreateModalOpen}
                        onClose={this.onCreateModalClose}
                        className="modal_form"
                        trigger={
                          <Button
                            onClick={this.onCreateModalOpen}
                            primary
                            className="header_btn">
                            <IcPush pathcolor="#ceeefe" />
                            Create company
                          </Button>
                        }
                        closeIcon
                        closeOnDimmerClick={false}>
                        <CreateCompanyPage
                          onModalClose={this.onCreateModalClose}
                          onEnableFormLoader={this.onEnableFormLoader}
                          onCompanyCreate={this.onCompanyCreate}
                        />
                      </Modal>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </div>
              <ManegeCard
                onEnableFormLoader={this.onEnableFormLoader}
                content={this.state.content}
                onCompanyCreate={this.onCompanyCreate}
                onCompanyDelete={this.onCompanyDelete}
                isShowManegeCardHeaderLeft={isCardHeaderLeft}
              />
            </div>
          )}
        </Container>
        <PageLoader
          active={this.state.isLoading}
          loaderText={this.state.loaderText}
        />
      </div>
    );
  }
}

ManageCompaniesContainer.propTypes = {
  gridColumnLeft: PropTypes.string,
  gridColumnRight: PropTypes.string
};

ManageCompaniesContainer.defaultProps = {
  gridColumnLeft: "8",
  gridColumnRight: "8"
};

export default ManageCompaniesContainer;
