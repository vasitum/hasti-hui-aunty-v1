import React from "react";

import {
  Responsive
} from "semantic-ui-react";

import ManageCompaniesContainer from "./ManageCompaniesContainer";
import ManageCompniesMobile from "./ManageComapniesMobile";
import ReactGA from "react-ga";
import "./index.scss";
// const infoicon = <IcInfoIcon pathcolor="#c8c8c8" />;

class ManageCompnies extends React.Component {
  componentDidMount() {
    ReactGA.pageview('/job/recruiter/compaines');
  }
  render() {
    

    return (
      <div className="">
        <Responsive maxWidth={1024}>
          <ManageCompniesMobile />
        </Responsive>
        <Responsive minWidth={1025}>
          <ManageCompaniesContainer />
        </Responsive>
      </div>
    );
  }
}

export default ManageCompnies;
