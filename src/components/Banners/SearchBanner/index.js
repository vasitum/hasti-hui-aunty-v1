import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";

import { Grid, Header, Button } from "semantic-ui-react";
import RecentDropdown from "../../Dropdown/RecentDropdown";

import "./index.scss";

/**
 *
 * @Component
 * @defines
 * {
 *  type: 'job' | 'people'
 * }
 */
const SearchBanner = props => {
  
  const {type, hits, headerText, children, btnText, recentDropdownMenu} = props;

  const classes = cx({
    SearchBanner: true,
    [`is-${type}`]: true
  });

  return (
    <div className={classes}>
      <Grid columns={2} padded centered verticalAlign="middle" style={{width: "100%"}}>
        <Grid.Column mobile={16} computer={10} > 
          {/* tablet={16} mobile={16} */}
          <Header as="h3">Found {hits} {headerText}</Header>
          <Header.Subheader>
            {children}
          </Header.Subheader>
        </Grid.Column>
        <Grid.Column mobile={16} computer={6} textAlign="right" className="mobileAling_textCenter">
          {/* tablet={16} mobile={16} */}
          <Button>{btnText}</Button>
          <Grid>
            <Grid.Row only="computer">
              <Grid.Column width={16} only="computer">
                {recentDropdownMenu}
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Grid.Column>
      </Grid>
    </div>
  );
};

SearchBanner.propTypes = {
  type: PropTypes.string,
  hits: PropTypes.number,
  headerText: PropTypes.string,
  children: PropTypes.string,
  btnText: PropTypes.string
};

SearchBanner.defaultProps = {
  type: "job",
  hits: 121,
  headerText: "job",
  children: "Post jobs with Vasitum's free personalized assistance to increase your hiring efficiency",
  btnText: "Sign up and Post Job"
};

export default SearchBanner;
