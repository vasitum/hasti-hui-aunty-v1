import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";

import "./index.scss";

/**
 *
 *
 * @Component
 * @defines
 * {
 *  size: 'small' | 'medium' | 'large',
 *  image: 'url | react import',
 *  color: 'color_name | color_code'
 * }
 */
const PageBanner = props => {
  let { size, image, color, children, compHack, jobHack } = props;
  const classes = cx({
    Pagebanner: true,
    [`is-${size}`]: true
  });
  return (
    <div
      className={classes}
      style={{
        background: `${
          image
            ? `linear-gradient(
                rgba(0,0,0,0.5),
                rgba(0,0,0,0.5)
              ), url(${image})`
            : color
        }`,
        backgroundPosition: jobHack
          ? "center"
          : compHack
          ? "left -2px"
          : "left top",
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat"
      }}>
      {children}
    </div>
  );
};

PageBanner.propTypes = {
  size: PropTypes.string.isRequired,
  image: PropTypes.string,
  color: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ])
};

PageBanner.defaultProps = {
  size: "medium",
  color: "blue"
};

export default PageBanner;
