import React from "react";

import { Image } from "semantic-ui-react";
import PropTypes from "prop-types";

import FlatDefaultBtn from "../../Buttons/FlatDefaultBtn";

import Chat from "../../../assets/img/chat.png";

import qs from "qs";

import "./index.scss";

class JobNoFound extends React.Component {
  render() {
    const { types, typeIcon } = this.props;
    // const qsParsed = qs.parse(this.props.location.search);
    // console.log(qsParsed);
    return (
      <div className="JobNoFound">
        <div className="jobNoFound_img">{typeIcon}</div>
        {/* <div className="jobNoFound_header">
          <p>
            Sorry..! No results found for “<span>
              {qsParsed[`?query`]}
            </span>”
          </p>
        </div> */}
        <div className="jobNoFound_title">
          <p className="title">Search Tips:</p>
          <p className="subTitle">
            Make sure all words are spelled correctly. Try different or general
            keywords.
          </p>
        </div>
        {/* <div className="jobNoFound_btn">
          <FlatDefaultBtn btntext="Developer" />
          <FlatDefaultBtn btntext="Java developer" />
          <FlatDefaultBtn btntext="Web developer" />
        </div> */}
      </div>
    );
  }
}

JobNoFound.propTypes = {
  types: PropTypes.string
};

JobNoFound.defaultProps = {
  types: "Jobs not"
};

export default JobNoFound;
