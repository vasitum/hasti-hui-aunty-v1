import React, { Component } from 'react'

import { Tab, Container, Button } from "semantic-ui-react";

import MobileHeader from "../../MobileComponents/MobileHeader";
import DashBoardNavigationDrawer from "../../MobileComponents/DashBoardNavigationDrawer";
import MobileFooter from "../../MobileComponents/MobileFooter";

import BorderButton from "../../Buttons/BorderButton";

import AdvanceSearchBodyHeaderBody from "../AdvanceSearchBodyHeaderBody"
import AdvanceSearchInputFields from "../AdvanceSearchInputFields";

import AdvanceSearchFieldMobile from "./AdvanceSearchFieldMobile";

import CheckboxField from "../../Forms/FormFields/CheckboxField";
import "./index.scss";

const JobData = [
  {
    dataTitle: "UI designer"
  },
  {
    dataTitle: "python developer"
  },
  {
    dataTitle: "UX designer"
  },
  {
    dataTitle: "Android"
  },
]

const ExperienceData = [
  {
    type: "text",
    placeholder: "From",
    name: "maxExp"
  },
  {
    type: "text",
    placeholder: "to",
    name: "minExp"
  }
]

const GraduationData = [
  {
    type: "text",
    placeholder: "From",
    name: "maxExp"
  },
  {
    type: "text",
    placeholder: "to",
    name: "minExp"
  }
]

const candidateDetailData = [
  {
    type: "text",
    placeholder: "Fist name",
    name: "fistName"
  },
  {
    type: "text",
    placeholder: "Last name",
    name: "lastName"
  }
]

const educationData = [
  {
    type: "text",
    placeholder: "Field of study",
    name: "studyField"
  },
  {
    type: "text",
    placeholder: "Degrees",
    name: "degrees"
  }
]

const companyData = [
  {
    type: "text",
    placeholder: "Current companies",
    name: "current"
  },
  {
    type: "text",
    placeholder: "Year in current company",
    name: "yearCurrentCompany"
  },
  {
    type: "text",
    placeholder: "Past companies",
    name: "pastCompanies"
  },
  {
    type: "text",
    placeholder: "Company types",
    name: "companyTypes"
  },
]



class AdvanceSearchMobile extends Component {

  constructor(props) {
    super(props);
  }

  state = {
    SearchFieldFilterMobile: "none",
    bodyClasses: "block"
  };

  isSearchMobileFilter = e => {
    e.preventDefault();
    e.stopPropagation();
    this.setState({
      SearchFieldFilterMobile: "block",
      bodyClasses: "none"
    });
  };

  SearchFieldFilterMobileClose = e => {
    e.preventDefault();
    e.stopPropagation();
    this.setState({
      SearchFieldFilterMobile: "none",
      bodyClasses: "block"
    });
  };
  render() {

    const panes = [
      {
        menuItem: 'Job titles',
        render: () => <Tab.Pane>
          <AdvanceSearchBodyHeaderBody
            advanceSearchData={JobData}
            btnText="Add job title or boolean"
            onCloseClick={this.isSearchMobileFilter}
    
          />
        </Tab.Pane>
      },
      {
        menuItem: 'Year of experience',
        render: () => <Tab.Pane>
          <AdvanceSearchInputFields
            advanceSearchData={ExperienceData}
          />
        </Tab.Pane>
      },
    
      {
        menuItem: 'Location',
        render: () => <Tab.Pane>
          <AdvanceSearchBodyHeaderBody
            advanceSearchData={JobData}
            btnText="Add location"
          />
        </Tab.Pane>
      },
      {
        menuItem: 'Skills / Keywords',
        render: () => <Tab.Pane>
          <AdvanceSearchBodyHeaderBody
            advanceSearchData={JobData}
            btnText="Add skill, keyword"
          />
        </Tab.Pane>
      },
      {
        menuItem: 'Company',
        render: () => <Tab.Pane>
          <AdvanceSearchBodyHeaderBody
            advanceSearchData={JobData}
            btnText="Add companies"
          />
        </Tab.Pane>
      },
      {
        menuItem: 'Employment type',
        render: () => <Tab.Pane>
          <AdvanceSearchBodyHeaderBody
            advanceSearchData={JobData}
            isShowAddBtn
            
          />
        </Tab.Pane>
      },
    
    
      {
        menuItem: 'Industries',
        render: () => <Tab.Pane>
          <AdvanceSearchBodyHeaderBody
            advanceSearchData={JobData}
            btnText="Add industries"
          />
        </Tab.Pane>
      },
      {
        menuItem: 'Year of graduation',
        render: () => <Tab.Pane>
          <AdvanceSearchInputFields
            advanceSearchData={GraduationData}
          />
        </Tab.Pane>
      },
      {
        menuItem: 'Candidate details',
        render: () => <Tab.Pane>
          <AdvanceSearchInputFields
            advanceSearchData={candidateDetailData}
          />
        </Tab.Pane>
      },
      {
        menuItem: 'Education',
        render: () => <Tab.Pane>
          <AdvanceSearchInputFields
            advanceSearchData={educationData}
          />
        </Tab.Pane>
      },
      {
        menuItem: 'Company',
        render: () => <Tab.Pane>
          <AdvanceSearchInputFields
            advanceSearchData={companyData}
          />
        </Tab.Pane>
      },
      {
        menuItem: 'Recruiting activity',
        render: () => <Tab.Pane>
          <div className="candidateActivity_checkBox">
            <CheckboxField checkboxLabel="Recruiting & candidate activity" />
          </div>
        </Tab.Pane>
      },
    
    ]

    return (
      <React.Fragment>
        <AdvanceSearchFieldMobile
          style={{
            display: this.state.SearchFieldFilterMobile
          }}
          onCloseClick={this.SearchFieldFilterMobileClose}
        />
        <div className="AdvanceSearchMobile" style={{
            display: this.state.bodyClasses
          }}>


          <div className="AdvanceSearchMobile_header">
            <MobileHeader
              headerLeftIcon={<DashBoardNavigationDrawer />}
              headerTitle="headerTitle"
              className="isMobileHeader_fixe"
            />
          </div>
          <div className="AdvanceSearchMobile_body">
            <Container>
              <Tab
                menu={{ fluid: true, vertical: true, tabular: true }}
                panes={panes}
                grid={{ tabWidth: 5, paneWidth: 11 }}
                onCloseClick={this.isSearchMobileFilter}

              />
            </Container>
          </div>
          <div className="AdvanceSearchMobile_footer">
            <MobileFooter
              headerTitle={
                <Button
                  compact
                  className="clearAllBtn"
                >
                  Clear All
              </Button>
              }
              className="isMobileFooter_fixe"
            >
              <BorderButton
                className="btn-border"
                btnText="Search"
              />
            </MobileFooter>
          </div>

        </div>
      </React.Fragment>

    )
  }
}

export default AdvanceSearchMobile


