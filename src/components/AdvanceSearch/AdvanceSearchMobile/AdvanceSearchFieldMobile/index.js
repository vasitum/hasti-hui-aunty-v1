import React from "react";

import { Button, Container } from "semantic-ui-react";

import MobileHeader from "../../../MobileComponents/MobileHeader";
import MobileFooter from "../../../MobileComponents/MobileFooter";
import BorderButton from "../../../Buttons/BorderButton";

import IcFooterArrowIcon from "../../../../assets/svg/IcFooterArrowIcon";

import AdvanceSearchFieldListMobile from "../AdvanceSearchFieldListMobile";

import "./index.scss";

const MobileSearchData = [
  {
    searchTitle: "Cretive designer"
  },

  {
    searchTitle: "Graphic artist"
  },
  {
    searchTitle: "Photoshop"
  },
  {
    searchTitle: "Cretive designer"
  },
  {
    searchTitle: "Graphic artist"
  },
  {
    searchTitle: "Photoshop"
  },
  {
    searchTitle: "Cretive designer"
  },
  {
    searchTitle: "Graphic artist"
  },
  {
    searchTitle: "Photoshop"
  }
]

const AdvanceSearchFieldMobile = props => {
  const { onCloseClick, ...resProp } = props;
  return (
    <div className="AdvanceSearchFieldMobile" {...resProp}>
      <div className="AdvanceSearchField_headerMobile">
        <MobileHeader
          className="isMobileHeader_fixe"
          headerLeftIcon={<Button
            onClick={onCloseClick}
            className="backArrowBtn">
            <IcFooterArrowIcon pathcolor="#acaeb5" />
          </Button>}
          mobileHeaderLeftColumn={16}
        >

        </MobileHeader>
      </div>
      <div className="AdvanceSearchField_bodyMobile">
        <Container>
          <AdvanceSearchFieldListMobile 
            searchListData={MobileSearchData} 
          />
        </Container>
      </div>
      <div className="AdvanceSearchField_footerMobile">
        <MobileFooter
          className="isMobileFooter_fixe"
        // mobileFooterRightColumn="16"
        >
          <BorderButton
            btnText="Done"
            className="btn-border"
          />
        </MobileFooter>
      </div>
    </div>
  )
}

export default AdvanceSearchFieldMobile;