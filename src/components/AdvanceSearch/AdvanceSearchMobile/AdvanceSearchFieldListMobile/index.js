import React from "react";

import { Header } from "semantic-ui-react";

import "./index.scss";

const AdvanceSearchFieldListMobile = props => {
  const { searchListData } = props;
  return (
    <div className="AdvanceSearchFieldListMobile">
      {searchListData.map((title, idx) => {
        return (
          <div className="searchList" key={idx}>
            <Header as="a">
              {title.searchTitle}
            </Header>
          </div>
        )
      })}

    </div>
  )
}

export default AdvanceSearchFieldListMobile;