import React from "react";

import { Grid } from "semantic-ui-react";

import InputFieldSementic from "../../Forms/FormFields/InputFieldSementic";

import "./index.scss";

const AdvanceSearchInputFields = props => {

  const { advanceSearchData } = props;
  
  if(!advanceSearchData) {
    return null
  }
  return (
    <div className="YearExperience AdvanceSearchInputFields">
      <Grid>
        <Grid.Row>
          {advanceSearchData.map((title, idx) => {
            return (
              <Grid.Column width={8} key={idx}>
                <InputFieldSementic
                  inputtype={title.type}
                  placeholder={title.placeholder}
                  name={title.nave}
                />
              </Grid.Column>
            )
          })}

          {/* <Grid.Column width={8}>
            <InputFieldSementic
              inputtype={"text"}
              placeholder="to"
              name="maxExp"
            />
          </Grid.Column> */}
        </Grid.Row>
      </Grid>
    </div>
  )
}

export default AdvanceSearchInputFields;