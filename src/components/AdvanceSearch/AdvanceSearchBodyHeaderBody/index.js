import React, { Component } from 'react'

import BorderButton from "../../Buttons/BorderButton";
import AdvanceSearchTags from "../AdvanceSearchTags";
import "./index.scss";


class AdvanceSearchBodyHeaderBody extends Component {
  
  constructor(props){
    super(props);

    this.state = {

    }
  }

  render() {
    const { 
      btnText, 
      advanceSearchData, 
      isShowAddBtn,
      onCloseClick, 
      ...resProps } = this.props;

    if (!advanceSearchData) {
      return null
    }
    return (
      <div className="AdvanceSearchBodyHeaderBody" {...resProps}>
        <div className="AdvanceSearchChip_container">
          {/* <AdvanceSearchTags /> */}
          {!isShowAddBtn ? (<div className="AdvanceSearchAdd_moreChip">
          <BorderButton
            className="btnWhiteSmokeBorder"
            btnText={btnText}
            onClick={onCloseClick}
            {...resProps}
          />
        </div>) : null}
          {advanceSearchData.map((title, idx) => {
            return (
              <BorderButton
                key={idx}
                className="btnGrayBorder"
                btnText={title.dataTitle}
              />
            )
          })}

          {/* <BorderButton
          className="btnGrayBorder"
          btnText="UI designer"
        /> */}
        </div>
        

      </div>
    )
  }
}




export default AdvanceSearchBodyHeaderBody;