import React, { Component } from 'react';

import { Grid, Form } from "semantic-ui-react";

// import { Form } from "formsy-semantic-ui-react";

import AdvanceSearchBodyHeaderTitle from "../AdvanceSearchBodyHeaderTitle";

import AccordionCardContainer from "../../Cards/AccordionCardContainer";
import AdvanceSearchBodyHeaderBody from "../AdvanceSearchBodyHeaderBody";
// import InputFieldSementic from "../../Forms/FormFields/InputFieldSementic";

import IcMyJobs from "../../../assets/svg/IcMyJobs";
// import IcCertification from "../../../assets/svg/IcCertification";

import IcYrsOfExpIcon from "../../../assets/svg/IcYrsOfExpIcon";
import IcLocation from "../../../assets/svg/IcLocation";
import IcRecommendedApplicants from "../../../assets/svg/IcRecommendedApplicants";
import IcCompanyNocircle from "../../../assets/svg/IcCompanyNocircle";

import AdvanceSearchInputFields from "../AdvanceSearchInputFields";
import IcEmploymentType from "../../../assets/svg/IcEmploymentType";
import IcIndustiesIcon from "../../../assets/svg/IcIndustiesIcon";
import IcYrsofGradution from "../../../assets/svg/IcYrsofGradution";
import IcCandidateCompany from "../../../assets/svg/IcCandidateCompany";
import IcEducationNoCircle from "../../../assets/svg/IcEducationNoCircle";
import IcMyCandidates from "../../../assets/svg/IcMyCandidates";

import AdvanceSearchTags from "../AdvanceSearchTags";

import CheckboxField from "../../Forms/FormFields/CheckboxField";

import "./index.scss";


const JobData = [
  {
    dataTitle: "UI designer"
  },
  {
    dataTitle: "python developer"
  },
  {
    dataTitle: "UX designer"
  },
  {
    dataTitle: "Android"
  },
]

const ExperienceData = [
  {
    type: "text",
    placeholder: "From",
    name: "maxExp"
  },
  {
    type: "text",
    placeholder: "to",
    name: "minExp"
  }
]

const GraduationData = [
  {
    type: "text",
    placeholder: "From",
    name: "maxExp"
  },
  {
    type: "text",
    placeholder: "to",
    name: "minExp"
  }
]

const candidateDetailData = [
  {
    type: "text",
    placeholder: "Fist name",
    name: "fistName"
  },
  {
    type: "text",
    placeholder: "Last name",
    name: "lastName"
  }
]

const educationData = [
  {
    type: "text",
    placeholder: "Field of study",
    name: "studyField"
  },
  {
    type: "text",
    placeholder: "Degrees",
    name: "degrees"
  }
]

const companyData = [
  {
    type: "text",
    placeholder: "Current companies",
    name: "current"
  },
  {
    type: "text",
    placeholder: "Year in current company",
    name: "yearCurrentCompany"
  },
  {
    type: "text",
    placeholder: "Past companies",
    name: "pastCompanies"
  },
  {
    type: "text",
    placeholder: "Company types",
    name: "companyTypes"
  },
]





class AdvanceSearchBody extends Component {
  render() {
    return (
      <div className="AdvanceSearchBody">
        <Form>
          <div className="AdvanceSearchBody_inner">
            <div className="AdvanceSearchBody_innerLeft">
              <AccordionCardContainer cardHeader={
                <AdvanceSearchBodyHeaderTitle
                  title="Job titles"
                  subTitle="job title or boolean"
                  icon={<IcMyJobs
                    pathcolor="#acaeb5"
                    width="15"
                    height="13"
                  />}
                />}
              >

                <AdvanceSearchTags />
                <AdvanceSearchBodyHeaderBody
                  advanceSearchData={JobData}
                  btnText="Add job title or boolean"
                />
              </AccordionCardContainer>

              <AccordionCardContainer cardHeader={
                <AdvanceSearchBodyHeaderTitle
                  title="Year of experience"
                  subTitle="Year of experience"
                  icon={<IcYrsOfExpIcon
                    pathcolor="#acaeb5"
                    width="14"
                    height="14"
                  />}
                />}
              >
                <AdvanceSearchInputFields
                  advanceSearchData={ExperienceData}
                />
              </AccordionCardContainer>

              <AccordionCardContainer cardHeader={
                <AdvanceSearchBodyHeaderTitle
                  title="Location"
                  subTitle="Location"
                  icon={<IcLocation
                    pathcolor="#acaeb5"
                    width="12"
                    height="13"
                  />}
                />}
                >
                <AdvanceSearchBodyHeaderBody
                  advanceSearchData={JobData}
                  btnText="Add location"
                />
              </AccordionCardContainer>

              <AccordionCardContainer cardHeader={
                <AdvanceSearchBodyHeaderTitle
                  title="Skills / Keywords"
                  subTitle="Skills, profile keywords or boolean"
                  icon={<IcRecommendedApplicants
                    pathcolor="#acaeb5"
                    width="15"
                    height="13"
                  />}
                />}
              >
                <AdvanceSearchBodyHeaderBody
                  advanceSearchData={JobData}
                  btnText="Add skill, keyword"
                />
              </AccordionCardContainer>

              <AccordionCardContainer cardHeader={
                <AdvanceSearchBodyHeaderTitle
                  title="Companies"
                  subTitle="Companies or boolean"
                  icon={<IcCompanyNocircle
                    pathcolor="#acaeb5"
                    width="15"
                    height="13"
                  />}
                />}
              >
                <AdvanceSearchBodyHeaderBody
                  advanceSearchData={JobData}
                  btnText="Add companies"
                />
              </AccordionCardContainer>

              <AccordionCardContainer cardHeader={
                <AdvanceSearchBodyHeaderTitle
                  title="Employment type"
                  subTitle="May be open to these type of employment"
                  icon={<IcEmploymentType
                    pathcolor="#acaeb5"
                    pathcolor1="#ffffff"
                    width="15"
                    height="13"
                  />}
                />}
              >
                <AdvanceSearchBodyHeaderBody
                  advanceSearchData={JobData}
                  isShowAddBtn
                />
              </AccordionCardContainer>

              <AccordionCardContainer cardHeader={
                <AdvanceSearchBodyHeaderTitle
                  title="Industries"
                  subTitle="Candidate industries"
                  icon={<IcIndustiesIcon
                    pathcolor="#acaeb5"
                    width="15"
                    height="13"
                  />}
                />}
              >
                <AdvanceSearchBodyHeaderBody
                  advanceSearchData={JobData}
                  btnText="Add industries"
                />
              </AccordionCardContainer>
            </div>


            <div className="AdvanceSearchBody_innerRight">
              <AccordionCardContainer cardHeader={
                <AdvanceSearchBodyHeaderTitle
                  title="Year of graduatioan"
                  subTitle="Add graduation year range"
                  icon={<IcYrsofGradution
                    pathcolor="#acaeb5"
                    width="15"
                    height="13"
                  />}
                />}
              >
                <AdvanceSearchInputFields
                  advanceSearchData={GraduationData}
                />
              </AccordionCardContainer>

              <AccordionCardContainer cardHeader={
                <AdvanceSearchBodyHeaderTitle
                  title="Candidate details"
                  subTitle="First, last name"
                  icon={<IcMyCandidates
                    pathcolor="#acaeb5"
                    width="15"
                    height="13"
                  />}
                />}
              >
                <AdvanceSearchInputFields
                  advanceSearchData={candidateDetailData}
                />
              </AccordionCardContainer>

              <AccordionCardContainer cardHeader={
                <AdvanceSearchBodyHeaderTitle
                  title="Education"
                  subTitle="Field of study, degrees"
                  icon={<IcEducationNoCircle
                    pathcolor="#acaeb5"
                    width="15"
                    height="13"
                  />}
                />}
              >
                <AdvanceSearchInputFields
                  advanceSearchData={educationData}
                />
                
              </AccordionCardContainer>

              <AccordionCardContainer 
                addClass="company_details"
                cardHeader={
                <AdvanceSearchBodyHeaderTitle
                  title="Company"
                  subTitle="Current, past companies"
                  icon={<IcCandidateCompany
                    pathcolor="#acaeb5"
                    width="15"
                    height="13"
                  />}
                />}
              > 
                
                <AdvanceSearchInputFields
                  advanceSearchData={companyData}
                />
                
              </AccordionCardContainer>

              <div className="candidateActivity_checkBox">
                <CheckboxField checkboxLabel="Recruiting & candidate activity" />
              </div>
            </div>
          </div>
        </Form>

      </div>
    )
  }
}

export default AdvanceSearchBody;
