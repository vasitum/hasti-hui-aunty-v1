import React, { Component } from 'react';

import { Responsive } from "semantic-ui-react";

import AdvanceSearchContainer from "./AdvanceSearchContainer";
import AdvanceSearchMobile from "./AdvanceSearchMobile";
import ScreenMainContainer from "../ScreenMainContainer";
// import AdvanceSearchFieldMobile from "./AdvanceSearchMobile/AdvanceSearchFieldMobile";
import "./index.scss";

class AdvanceSearch extends Component {
  render() {
    return (
      <div className="AdvanceSearch">
        <Responsive maxWidth={1024}>
          {/* <AdvanceSearchFieldMobile /> */}
          <AdvanceSearchMobile />
        </Responsive>
        <Responsive minWidth={1025}>
          <ScreenMainContainer>
            <AdvanceSearchContainer />
          </ScreenMainContainer>
        </Responsive>
      </div>
    )
  }
}


export default AdvanceSearch;
