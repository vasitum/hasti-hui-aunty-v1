import React from "react";
import IcPlus from "../../../assets/svg/IcPlus";

import "./index.scss";

const AdvanceSearchBodyHeaderTitle = props => {
  const { title, icon, subTitle, } = props;
  return (
    <div className="AdvanceSearchBodyHeaderTitle">
      <div className="HeaderTitle_left">
        <div className="headIcon">
          {icon}
        </div>
        <div className="headTitleList">
          <p className="headTitle">
            {title}
          </p>
          <p className="headSubTitle">
            {subTitle}
          </p>
        </div>
      </div>
      <div className="HeaderTitle_right">
        <IcPlus pathcolor="#acaeb5" />
      </div>
    </div>
  )
}

export default AdvanceSearchBodyHeaderTitle;