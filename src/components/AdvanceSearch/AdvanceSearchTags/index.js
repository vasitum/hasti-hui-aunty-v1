import React from "react";

import DefaultSearchTagField from "../../Forms/FormFields/DefaultSearchTagField";

import "./index.scss";

const AdvanceSearchTags = props => {
  return(
    <div className="AdvanceSearchTags">
      <DefaultSearchTagField 
        name="jobTitle"
      />
    </div>
  )
}

export default AdvanceSearchTags;