import React, { Component } from 'react'
import {Container} from "semantic-ui-react";

import AdvanceSearchHeader from "../AdvanceSearchHeader";

import AdvanceSearchBody from "../AdvanceSearchBody";

import "./index.scss";

class AdvanceSearchContainer extends Component {
	render() {
		return (
			<div className="AdvaceSearchContainer">
				<Container>
          <AdvanceSearchHeader />
          <AdvanceSearchBody />
				</Container>
			</div>
		)
	}
}

export default AdvanceSearchContainer;
