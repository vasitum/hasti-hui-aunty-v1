import React, { Component } from 'react';

import { Header, Button, } from "semantic-ui-react";

import BorderButton from "../../Buttons/BorderButton";

import InputFieldSementic from "../../Forms/FormFields/InputFieldSementic";

import IcDownArrow from "../../../assets/svg/IcDownArrow";

import "./index.scss";



class AdvanceSearchHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowBooleanSearchQuery: false,
      addClassBoolenBtn: "BoolenBtn"
    };

  }

  booleanSearchQuery = () => {
    this.setState({
      isShowBooleanSearchQuery: !this.state.isShowBooleanSearchQuery
    });
  };
  render() {
    return (
      <React.Fragment>
        <div className="AdvanceSearchHeader">
          <div className="AdvanceSearchHeader_left">
            <Header as="h2">Advance search</Header>
          </div>
          <div className="AdvanceSearchHeader_right">
            <Button
              className="booleanSearchQueryBtn"
              onClick={this.booleanSearchQuery}
            >
              {this.state.isShowBooleanSearchQuery ? "Hide" : "Show"} boolean search query

              {this.state.isShowBooleanSearchQuery ?
                <span className="upArrow">
                  <IcDownArrow
                    pathcolor="#0b9ed0"
                    height="7"
                    width="9"
                  />
                </span> : <span className="DownArrow">
                  <IcDownArrow
                    pathcolor="#0b9ed0"
                    height="7"
                    width="9"
                  />
                </span>
              }
            </Button>
            <BorderButton
              className="btn-border"
              btnText="Search"
            />
          </div>
        </div>

        {this.state.isShowBooleanSearchQuery && (
          <div className="BooleanSearchQueryField">
            <InputFieldSementic
              name="field"
              value="UI designer+UX designer, +5 to 7 years of epx, delhi, noida"
            />
          </div>
        )}

      </React.Fragment>

    )
  }
}




export default AdvanceSearchHeader;