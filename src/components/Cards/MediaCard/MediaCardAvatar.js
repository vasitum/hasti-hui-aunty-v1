import React from "react";
import PropTypes from "prop-types";

const Avatar = props => {
  return <div className="MediaCardAvatar">{props.children}</div>;
};

Avatar.propTypes = {};

export default Avatar;
