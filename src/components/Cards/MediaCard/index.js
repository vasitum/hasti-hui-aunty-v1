import React, { Component } from "react";
import PropTypes from "prop-types";
import { Card } from "semantic-ui-react";
import MediaCardAvatar from "./MediaCardAvatar";
import cx from "classnames";

// Index SCSS
import "./index.scss";

class MediaCard extends Component {
  static Avatar = MediaCardAvatar;

  render() {
    const { shadowless, shadow, small, children, ...props } = this.props;

    const classes = cx({
      MediaCard: true,
      [`is-small`]: small,
      [`is-shadowless`]: shadowless,
      [`is-shadow`]: shadow
    });

    return (
      <Card className={classes} {...props}>
        {children}
      </Card>
    );
  }
}

MediaCard.propTypes = {};

export default MediaCard;
