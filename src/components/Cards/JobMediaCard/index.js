import React from "react";
import PropTypes from "prop-types";
import { Grid, Image, Button, Icon, Dropdown } from "semantic-ui-react";

import MediaCard from "../MediaCard";
import CardGlobalActions from "../CardActions/CardGlobalActions";
import CardSummary from "../CardSummary";
import SkillList from "../../CardElements/SkillList";
import InfoSection from "../../Sections/InfoSection";
import SearchCard from "../SearchCard";
import SearchCardHeader from "../SearchCard/SearchCardHeader";
import SearchCardFooter from "../SearchCard/SearcCardFooter";

import IcCompany from "../../../assets/svg/IcCompany";
import IcLocation from "../../../assets/svg/IcLocation";
import Icjob from "../../../assets/svg/Icjob";
import IcEducation from "../../../assets/svg/IcEducation";
import IcDownArrow from "../../../assets/svg/IcDownArrow";
import MoreOptionIcon from "../../../assets/svg/IcMoreOptionIcon";

import "./index.scss";

const trigger = (
  <span>
    <MoreOptionIcon width="5" height="18.406" pathcolor="#797979" viewbox="0 0 5 18.406" />
  </span>
)

const options = [
  { key: 'user', text: 'Account', icon: 'user' },
  { key: 'settings', text: 'Settings', icon: 'settings' },
  { key: 'sign-out', text: 'Sign Out', icon: 'sign out' },
]

const JobMediaCard = props => {
  return (
    <MediaCard fluid>
      <MediaCard.Avatar>
        <IcCompany rectcolor="#f7f7fb" pathcolor="#c8c8c8" />
      </MediaCard.Avatar>
      <Grid columns={2}>
        <Grid.Row>
          <Grid.Column mobile={13} computer={9}>
            <CardSummary>
              <CardSummary.Item header>Semantic UI Developer</CardSummary.Item>
              <CardSummary.Item
                icon={<Icjob height="14px" width="16px" pathcolor="#0B9ED0" />}
                big>
                Maven Workforce
              </CardSummary.Item>
              <CardSummary group>
                <CardSummary.Item
                  icon={
                    <IcLocation
                      height="14px"
                      width="10px"
                      pathcolor="#0bd0bb"
                    />
                  }>
                  Noida, Uttar Pradesh, India
                </CardSummary.Item>

                <div className="mobile hidden">
                  <CardSummary.Item>
                    <span >
                      <span> 3-4 yrs</span> exp
                    </span>
                  </CardSummary.Item>
                </div>
              </CardSummary>
            </CardSummary>
          </Grid.Column>

          {/* only mobile */}
          <Grid.Column mobile={3} only="mobile" textAlign="right">
            <Dropdown trigger={trigger} options={options} pointing='top right' icon={null} />
          </Grid.Column>
          {/* only mobile */}

          <Grid.Column width={7} textAlign="center" only="computer large widescreem">
            <SearchCardHeader />
            <Button primary style={{ marginTop: "10px" }}>
              Apply
              <Icon name="angle right" />
            </Button>
          </Grid.Column>
        </Grid.Row>
        
        <Grid.Row only="mobile" className="expAndPost_grid">
          <Grid.Column mobile={8} >
            <span className="exp_year">
              <span> 3-4 yrs</span> exp
            </span>
          </Grid.Column>
          <Grid.Column mobile={8} textAlign="right">
            <span className="job_post">
              Today
            </span>
          </Grid.Column>
        </Grid.Row>

        {/* only desktop */}
        <Grid.Row columns={1} only="computer large widescreem">
          <Grid.Column>
            {!props.active ? (
              <SkillList small />
            ) : (
                <CardSummary centered>
                  <CardSummary.Extra
                    extras={[
                      {
                        key: 1,
                        head: "Job type:",
                        body: "Full time"
                      },
                      {
                        key: 2,
                        head: "Job role:",
                        body: "Job role: UI/UX Design"
                      }
                    ]}
                  />
                </CardSummary>
              )}
          </Grid.Column>
        </Grid.Row>
        {/* only desktop */}
      </Grid>
    </MediaCard>
  );
};

JobMediaCard.propTypes = {};

JobMediaCard.defaultProps = {};

export default JobMediaCard;
