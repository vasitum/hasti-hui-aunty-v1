import React from "react";
import PropTypes from "prop-types";
import { Grid, Card, Header, Button, Icon } from "semantic-ui-react";
import BestSearchCard from "../SearchCard/BestSearchCard";
import "./index.scss";

import CardSummary from "../CardSummary";
import SkillList from "../../CardElements/SkillList";
import CardGlobalActions from "../CardActions/CardGlobalActions";

import IcCompany from "../../../assets/svg/IcCompany";
import IcCompanyIcon from "../../../assets/svg/IcCompany";
import IcLocation from "../../../assets/svg/IcLocation";
import Icjob from "../../../assets/svg/Icjob";

const VisibleCard = () => {
  return (
    <Card fluid style={{ boxShadow: "none" }}>
      <Card.Content textAlign="center">
        <Card.Meta textAlign="right">
          <CardSummary centered>
            <CardSummary.Extra
              extras={[
                {
                  key: 1,
                  head: "Posted: ",
                  body: "Today"
                }
              ]}
            />
          </CardSummary>
        </Card.Meta>
        <Card.Description style={{ marginBottom: "20px" }}>
          <IcCompany rectcolor="#f7f7fb" pathcolor="#c8c8c8" />
        </Card.Description>
        <Card.Description style={{ marginBottom: "20px" }}>
          <CardSummary>
            <CardSummary.Item header>Java Developer</CardSummary.Item>
            <CardSummary.Item
              icon={<Icjob height="14px" width="16px" pathcolor="#0B9ED0" />}
              big
              centered>
              Maven Workforce
            </CardSummary.Item>
            <CardSummary.Item
              icon={
                <IcLocation height="14px" width="10px" pathcolor="#0bd0bb" />
              }
              centered>
              Noida, Uttar Pradesh, India
            </CardSummary.Item>
          </CardSummary>
        </Card.Description>
        <Card.Description style={{ marginBottom: "20px" }}>
          <SkillList small />
        </Card.Description>
      </Card.Content>
    </Card>
  );
};

const HiddenCard = () => {
  return (
    <div style={{ height: "100%" }}>
      <Card fluid style={{ height: "100%" }}>
        <Card.Content style={{ height: "100%" }}>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "column",
              height: "100%"
            }}>
            <Header as="h2" color="blue">
              Java Developer
            </Header>
            <Button primary style={{ marginBottom: "10px" }}>
              Apply
              <Icon icon="angle right" color="grey" />
            </Button>
            <CardGlobalActions />
          </div>
        </Card.Content>
      </Card>
    </div>
  );
};

const JobBestCard = props => {
  return (
    <div className="ui container JobBestCard">
      <Grid celled>
        <Grid.Row>
          <Grid.Column width={8}>
            <BestSearchCard
              HiddenComponent={HiddenCard}
              VisibleComponent={VisibleCard}
            />
          </Grid.Column>
          <Grid.Column width={8}>{"mdkasdmlas"}</Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={8}>{"mdkasdmlas"}</Grid.Column>
          <Grid.Column width={8}>{"mdkasdmlas"}</Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
};

JobBestCard.propTypes = {};

export default JobBestCard;
