import React from "react";

import { Grid, Image, Header, List, Button } from "semantic-ui-react";

import ActionBtn from "../../Buttons/ActionBtn";

import IcDownArrowIcon from "../../../assets/svg/IcDownArrow";

import IcJobThumb from "../../../assets/img/ic_job_thumb.png";

import IcDownArrow from "../../../assets/svg/IcDownArrow";
import PropTypes from "prop-types";

import CheckboxField from "../../Forms/FormFields/CheckboxField";

import CandidateCardFooter from "../../Dashboard/RecruiterDashboard/Candidates/CandidateCardFooter";

import "./index.scss";

class RecruterDashbaordJobCards extends React.Component {
  render() {
    const {
      isProfleIncrease,
      isCheck,
      isExp,
      isJobType,
      isRecommendation,
      isHeaderTag,
      isSkillShow,
      isBorder,
      myJobCardsWidthLeft,
      myJobCardsWidthRight,
      children,
      hit,
      ...resProps,
    } = this.props;

    if (!hit) {
      return null;
    }

    const ListStatusData = [
      {
        count: "12",
        title: "New applicants"
      },
      {
        count: "08",
        title: "Seen"
      },
      {
        count: "05",
        title: "Shortlist"
      },
      {
        count: "03",
        title: "Interview"
      },
      {
        count: "02",
        title: "Offer"
      },
      {
        count: "08",
        title: "Joined"
      },
      {
        count: "02",
        title: "Offer Dropout"
      },
      {
        count: "06",
        title: "Hold"
      },
      {
        count: "11",
        title: "Rejected"
      },

    ]
    return (
      <div className="RecruterDashbaordJobCards">
        <div className="jobDefault_card">
          <div className="JobCardsHeader_details">
            <Grid>
              <Grid.Row>
                <Grid.Column
                  width={myJobCardsWidthLeft}
                  className="MyjobCard MyJobCards_leftSide"
                >
                  <div className="MyjobCard_leftDetails">
                    <div className="companyLogo">
                      <Image src={IcJobThumb} />
                    </div>

                    {/* isProfleIncrease */}
                    {isProfleIncrease ? (<div className="profleIncrease">
                      <p className="title">92%</p>
                      <p className="subTitle">matched</p>
                    </div>) : null}

                    {/* isProfleIncrease end*/}


                    {/* isCheck */}
                    <div className="checkPropfile">
                      <CheckboxField />
                    </div>
                    {/* {isCheck ? (<div className="checkPropfile">
                    <CheckboxField />
                  </div>) : null} */}
                    {/* isCheck end*/}


                  </div>
                  <div className="companydetails">
                    <div className="title">
                      <Header as="h3">{hit.title}</Header>
                      <p>{hit.comName}</p>
                    </div>
                    <div className="subTitle">
                      <div className="loctionExp">
                        <p>
                          {hit.locCity}
                          {" "}
                          {hit.locCountry}
                        </p>

                        {/* isExp */}
                        {isExp ? (<p className="experience">
                          <span>3 - 4</span> yrs experience
                      </p>) : null}
                        {/* isExp end */}

                      </div>
                      {/* isJobType */}
                      {isJobType ? (<div className="jobTypeRoll">
                        <p className="jobType">Job type: <span>Full time</span></p>
                        <p className="jobRole">Job type: <span>UI/UX Design</span></p>
                      </div>) : null}
                      {/* isJobType end*/}

                      {/* isRecommendation */}
                      {isRecommendation ? (<div className="Recommendation">
                        <p>Recommended: <span>UI design</span></p>
                      </div>) : null}
                      {/* isRecommendation end*/}

                      {/* isHeaderTag */}
                      {isHeaderTag ? (<div className="headerTag">
                        <p>
                          Tag: <Button className="" compact>Software Developer</Button>
                          <Button className="" compact>Software Developer</Button>
                        </p>
                      </div>) : null}

                      {/* isHeaderTag end */}

                      {/* isSkillShow */}
                      {isSkillShow ? (
                        <p
                          className="skills"
                          style={{
                            display: !hit.skills ? "none" : "block"
                          }}
                        >
                          Skills:{" "}
                          <span>
                            {" "}
                            <List bulleted horizontal>
                              {hit.skills
                                ? hit.skills.map(skill => {
                                  return <List.Item>{skill}</List.Item>;
                                })
                                : null}
                            </List>
                          </span>
                        </p>
                      ) : null}
                      {/* isSkillShow end*/}
                    </div>
                  </div>
                </Grid.Column>
                <Grid.Column
                  width={myJobCardsWidthRight}
                  className="MyJobCards_rightSide"
                >
                  <div className="CardApplyContainer">
                    <div className="">
                      <div className="postTime">
                        <p> Posted: <span>Today</span></p>
                      </div>
                      <div className="applicantTitle">
                        <p>Total applicants <span>35</span></p>
                      </div>
                    </div>


                  </div>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </div>

          <div className="cardHeader_listStatus">
            {ListStatusData.map((showData, idx) => {
              return (
                <div key={idx} className="listMenu">
                  <p className="count">{showData.count}</p>
                  <p className="title">{showData.title}</p>
                </div>
              )
            })}
          </div>
          
        </div>

        <div className="cardFooter">
          <CandidateCardFooter />
          {/* <Grid>
            <Grid.Row>
              <Grid.Column width={12}>
                <div>
                  <p>Job status</p>
                </div>
              </Grid.Column>
              <Grid.Column width={4}>
                
              </Grid.Column>
            </Grid.Row>
          </Grid> */}
        </div>
      </div>
    )
  }
}

RecruterDashbaordJobCards.propTypes = {
  myJobCardsWidthLeft: PropTypes.string,
  myJobCardsWidthRight: PropTypes.string,
  hit: PropTypes.string
};

RecruterDashbaordJobCards.defaultProps = {
  myJobCardsWidthLeft: "11",
  myJobCardsWidthRight: "5",
  hit: {
    title: "UX UI Designer",
    comName: "Wipro",
    locCity: "Noida",
    locCountry: "India",
    skills: [
      "html",
      "css",
      "javascript"
    ]
  }
};

export default RecruterDashbaordJobCards;