import React from "react";

import ActionBtn from "../../../Buttons/ActionBtn";

import IcDownArrowIcon from "../../../../assets/svg/IcDownArrow";

import MoreOptionDropDown from "../../../Dropdown/MoreOptionDropDown";

const MobileCardFooter = props => {
  const { as, tohref, btnText } = props;

  return (
    <div className="myJobCards_footer">
      <div className="mobileFooterBtn">
        <ActionBtn
          as={as}
          to={tohref}
          size="tiny"
          actioaBtnText={btnText}
          btnIcon={
            <IcDownArrowIcon height="5" width="10" pathcolor="#ffffff" />
          }
        />
      </div>
      <div className="">
        <MoreOptionDropDown />
      </div>
    </div>
  );
};

export default MobileCardFooter;
