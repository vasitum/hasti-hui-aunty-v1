import React from "react";
import { Grid, Image, Header, List, Button } from "semantic-ui-react";

import IcJobThumb from "../../../../assets/img/publicProfile.png";
import PropTypes from "prop-types";

import CheckboxField from "../../../Forms/FormFields/CheckboxField";

import ActionBtn from "../../../Buttons/ActionBtn";
import IcDownArrowIcon from "../../../../assets/svg/IcDownArrow";

import { Link } from "react-router-dom";

import IcUser from "../../../../assets/svg/IcUser";

// import "./index.scss";

class MyJobsCardRecruiter extends React.Component {
  render() {
    const {
      isProfleIncrease,
      isCheck,
      isExp,
      isJobType,
      isRecommendation,
      isHeaderTag,
      isSkillShow,
      isBorder,
      myJobCardsWidthLeft,
      myJobCardsWidthRight,
      isShowMobileFooter,
      children,
      hit,
      tohref,
      ...resProps,
    } = this.props;

    if (!hit) {
      return null;
    }

    return (
      <div className={`MyJob_cardContainer ${isBorder}`} {...resProps}>
        <div className="MyJobCards jobDefault_card">
          <Grid>
            <Grid.Row>
              <Grid.Column
                width={myJobCardsWidthLeft}
                className="MyJobCards_leftSide"
              >
                <div className="MyjobCard_leftDetails">
                  {/* <div className="companyLogo">
                    <Image src={IcJobThumb} />
                  </div> */}
                  <div className="userImgThumb">
                    {/* <Image src={IcJobThumb} /> */}
                    {
                      !hit.ext || !hit.ext.img ? (
                        <IcUser rectcolor="#f7f7fb" height="50" width="50" pathcolor="#c8c8c8"/>
                      ) : (
                        <Image
                          src={`https://s3-us-west-2.amazonaws.com/img-mwf/${
                            hit._id
                          }/image.jpg`}
                          style={{
                            borderRadius: "50%"
                          }}
                        />
                      )

                    }
                  </div>
                  
                  {/* isProfleIncrease */}
                  {isProfleIncrease ? (<div className="profleIncrease">
                    <p className="increaseTitle">92%</p>
                    <p className="subTitle">matched</p>
                  </div>): null}

                  {/* isProfleIncrease end*/}
                  

                  {/* isCheck */}
                  {isCheck ? (<div className="checkPropfile">
                    <CheckboxField />
                  </div>): null}
                  {/* isCheck end*/}


                </div>
                <div className="companydetails">
                  <div className="title">
                    <Header as="h3"><Link to={tohref}>{hit.fName} {hit.lName}</Link></Header>
                    <p>{hit.title}</p>
                  </div>
                  <div className="subTitle">
                    <div className="loctionExp">
                      <p>
                        {hit.loc ? hit.loc.city : ""}
                        {" "}
                        {hit.loc ? hit.loc.country : ""}
                      </p>
                      
                      {/* isExp */}
                      {isExp ? (<p className="experience">
                        <span>3 - 4</span> yrs experience
                      </p>):null}
                      {/* isExp end */}

                    </div>
                    {/* isJobType */}
                    {isJobType ? (<div className="jobTypeRoll">
                      <p className="jobType">Job type: <span>Full time</span></p>
                      <p className="jobRole">Job type: <span>UI/UX Design</span></p>
                    </div>): null}
                    {/* isJobType end*/}

                    

                    {/* isHeaderTag */}
                    {isHeaderTag ? ( <div className="headerTag">
                      <p>
                        Tag: <Button className="" compact>Software Developer</Button>
                      <Button className="" compact>Software Developer</Button>
                      </p>
                    </div>):null}
                   
                    {/* isHeaderTag end */}

                    {/* isSkillShow */}
                    {isSkillShow ? (
                      <p
                        className="skills"
                        style={{
                          display: !hit.skills ? "none" : "block"
                        }}
                      >
                        Skills:{" "}
                        <span>
                          {" "}
                          <List bulleted horizontal>
                            {hit.skills
                              ? hit.skills.map(skill => {
                                return <List.Item>{skill.name}</List.Item>;
                              })
                              : null}
                          </List>
                        </span>
                      </p>
                    ) : null}
                    {/* isSkillShow end*/}

                    {/* isRecommendation */}
                    {isRecommendation ? (<div className="Recommendation">
                      <p>Recommended: <span>UI design</span></p>
                    </div>): null}
                    {/* isRecommendation end*/}
                  </div>
                </div>
              </Grid.Column>
              <Grid.Column
                width={myJobCardsWidthRight}
                className="MyJobCards_rightSide"
              >
                {children}
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>
        {/* {isShowMoreBtn ? (
          <div className="moreJob_btn">
            <ActionBtn
              btnText="VIEW MORE JOBS"
              btnIcon={<IcDownArrowIcon height="6" />}
            />
          </div>
        ) : null} */}

        {isShowMobileFooter}
      </div>
    );
  }
}

MyJobsCardRecruiter.propTypes = {
  myJobCardsWidthLeft: PropTypes.string,
  myJobCardsWidthRight: PropTypes.string,
  hit: PropTypes.string
};

MyJobsCardRecruiter.defaultProps = {
  myJobCardsWidthLeft: "11",
  myJobCardsWidthRight: "5",
  hit: {
    title: "UX UI Designer",
    comName: "Wipro",
    locCity: "Noida",
    locCountry: "India",
    skills: [
      "html",
      "css",
      "javascript"
    ]
  }
};

export default MyJobsCardRecruiter;