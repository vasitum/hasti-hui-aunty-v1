import React from "react";
import { Grid, Image, Header, List, Button, Container } from "semantic-ui-react";

import ActionBtn from "../../Buttons/ActionBtn";

import IcDownArrowIcon from "../../../assets/svg/IcDownArrow";

import IcJobThumb from "../../../assets/img/ic_job_thumb.png";

import IcDownArrow from "../../../assets/svg/IcDownArrow";
import PropTypes from "prop-types";

import CheckboxField from "../../Forms/FormFields/CheckboxField";

import CardApplyContainer from "./CardApplyContainer";

import RecruterDashbaordJobCards from "../RecruterDashbaordJobCards";

import CandidateDashbaordJobCards from "../CandidateDashbaordJobCards";

import MyJobCard from "./Cards";

import "./index.scss";

class MyJobCards extends React.Component {
  render() {
    const {
      isProfleIncrease,
      isCheck,
      isExp,
      isJobType,
      isRecommendation,
      isHeaderTag,
      isSkillShow,
      isBorder,
      myJobCardsWidthLeft,
      myJobCardsWidthRight,
      children,
      hit,
      ...resProps,
    } = this.props;

    // if (!hit) {
    //   return null;
    // }

    return (
      <div className={`MyJob_cardContainer ${isBorder}`} {...resProps}>
        
        <Container>
          <div>
            <h1>Job seeker liked job Card</h1>
            <MyJobCard isSkillShow>
              <CardApplyContainer />
            </MyJobCard>
          </div>

           <div>
            <h1>Recommended jobs Card</h1>
            <MyJobCard isProfleIncrease isExp isJobType isRecommendation>
              <CardApplyContainer />
            </MyJobCard>
          </div>

          <div>
            <h1>Job Seeker Liked job Card</h1>
            <MyJobCard isExp isJobType>
              <CardApplyContainer />
            </MyJobCard>
          </div>

          <div>
            <h1>My jobs v2 final Card</h1>
            <MyJobCard isCheck>
              <CardApplyContainer />
            </MyJobCard>>
          </div>

          <div>
            <h1>recommended jobs Card</h1>
            <MyJobCard isProfleIncrease isRecommendation>
              <CardApplyContainer />
            </MyJobCard>
          </div>

          <div>
            <h1>Saved jobs</h1>
            <MyJobCard isHeaderTag>
              <CardApplyContainer />
            </MyJobCard>
          </div>

          <div>
            <h1>RecruterDashbaordJobCards</h1>
            <RecruterDashbaordJobCards />
          </div>

          <div>
            <h1>CandidateDashbaordJobCards</h1>
            <CandidateDashbaordJobCards />
          </div>
         
        </Container>
        
      </div>
    );
  }
}

MyJobCards.propTypes = {
  // myJobCardsWidthLeft: PropTypes.string,
  // myJobCardsWidthRight: PropTypes.string,
  // hit: PropTypes.string
};

MyJobCards.defaultProps = {
  // myJobCardsWidthLeft: "11",
  // myJobCardsWidthRight: "5",
  // hit: {
  //   title: "UX UI Designer",
  //   comName: "Wipro",
  //   locCity: "Noida",
  //   locCountry: "India",
  //   skills: [
  //     "html",
  //     "css",
  //     "javascript"
  //   ]
  // }
};

export default MyJobCards;
