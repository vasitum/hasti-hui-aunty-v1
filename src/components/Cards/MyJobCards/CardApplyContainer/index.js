import React from "react";

import ActionBtn from "../../../Buttons/ActionBtn";

import IcDownArrow from "../../../../assets/svg/IcDownArrow";

import SocialBtn from "../../../ModalPageSection/Buttons/socialBtn";
import fromNow from "../../../../utils/env/fromNow";
import "./index.scss";

const CardApplyContainer = props => {
  const {
    hit,
    isApplicantTitle,
    btnText,
    tohref,
    job,
    as,
    ...resProps
  } = props;

  return (
    <div className="CardApplyContainer" {...resProps}>
      <div className="">
        <div className="postTime">
          {/* <SocialBtn
            // objectID={val.objectID}
            // onLikeClick={this.onLikeClick}
          /> */}
          {job ? (
            <p>
              {" "}
              Posted: <span>{fromNow(hit.cTime)}</span>
            </p>
          ) : (
            <p>
              {" "}
              Updated: <span>{fromNow(hit.uTime)}</span>
            </p>
          )}
        </div>
        {!isApplicantTitle ? (
          <div className="applyBtn">
            <ActionBtn
              as={as}
              to={tohref}
              actioaBtnText={btnText}
              btnIcon={<IcDownArrow pathcolor="#ffffff" />}
            />
          </div>
        ) : (
          <div className="applicantTitle">
            <p>
              Total applicants <span>35</span>
            </p>
          </div>
        )}
      </div>
    </div>
  );
};

export default CardApplyContainer;
