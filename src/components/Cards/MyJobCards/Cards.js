import React from "react";
import { Grid, Image, Header, List, Button } from "semantic-ui-react";

import ActionBtn from "../../Buttons/ActionBtn";

import IcDownArrowIcon from "../../../assets/svg/IcDownArrow";

import IcJobThumb from "../../../assets/img/ic_job_thumb.png";

import IcDownArrow from "../../../assets/svg/IcDownArrow";
import PropTypes from "prop-types";

import CheckboxField from "../../Forms/FormFields/CheckboxField";

import "./index.scss";

class MyJobCard extends React.Component {
  render() {
    const {
      isProfleIncrease,
      isCheck,
      isExp,
      isJobType,
      isRecommendation,
      isHeaderTag,
      isSkillShow,
      isBorder,
      myJobCardsWidthLeft,
      myJobCardsWidthRight,
      children,
      hit,
      ...resProps,
    } = this.props;

    if (!hit) {
      return null;
    }

    return (
      <div className={`MyJob_cardContainer ${isBorder}`} {...resProps}>
        <div className="MyJobCards jobDefault_card">
          <Grid>
            <Grid.Row>
              <Grid.Column
                width={myJobCardsWidthLeft}
                className="MyJobCards_leftSide"
              >
                <div className="MyjobCard_leftDetails">
                  <div className="companyLogo">
                    <Image src={IcJobThumb} />
                  </div>
                  
                  {/* isProfleIncrease */}
                  {isProfleIncrease ? (<div className="profleIncrease">
                    <p className="title">92%</p>
                    <p className="subTitle">matched</p>
                  </div>): null}

                  {/* isProfleIncrease end*/}
                  

                  {/* isCheck */}
                  {isCheck ? (<div className="checkPropfile">
                    <CheckboxField />
                  </div>): null}
                  {/* isCheck end*/}


                </div>
                <div className="companydetails">
                  <div className="title">
                    <Header as="h3">{hit.title}</Header>
                    <p>{hit.comName}</p>
                  </div>
                  <div className="subTitle">
                    <div className="loctionExp">
                      <p>
                        {hit.locCity}
                        {" "}
                        {hit.locCountry}
                      </p>
                      
                      {/* isExp */}
                      {isExp ? (<p className="experience">
                        <span>3 - 4</span> yrs experience
                      </p>):null}
                      {/* isExp end */}

                    </div>
                    {/* isJobType */}
                    {isJobType ? (<div className="jobTypeRoll">
                      <p className="jobType">Job type: <span>Full time</span></p>
                      <p className="jobRole">Job type: <span>UI/UX Design</span></p>
                    </div>): null}
                    {/* isJobType end*/}

                    {/* isRecommendation */}
                    {isRecommendation ? (<div className="Recommendation">
                      <p>Recommended: <span>UI design</span></p>
                    </div>): null}
                    {/* isRecommendation end*/}

                    {/* isHeaderTag */}
                    {isHeaderTag ? ( <div className="headerTag">
                      <p>
                        Tag: <Button className="" compact>Software Developer</Button>
                      <Button className="" compact>Software Developer</Button>
                      </p>
                    </div>):null}
                   
                    {/* isHeaderTag end */}

                    {/* isSkillShow */}
                    {isSkillShow ? (
                      <p
                        className="skills"
                        style={{
                          display: !hit.skills ? "none" : "block"
                        }}
                      >
                        Skills:{" "}
                        <span>
                          {" "}
                          <List bulleted horizontal>
                            {hit.skills
                              ? hit.skills.map(skill => {
                                return <List.Item>{skill}</List.Item>;
                              })
                              : null}
                          </List>
                        </span>
                      </p>
                    ) : null}
                    {/* isSkillShow end*/}
                  </div>
                </div>
              </Grid.Column>
              <Grid.Column
                width={myJobCardsWidthRight}
                className="MyJobCards_rightSide"
              >
                {children}
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>
        {/* {isShowMoreBtn ? (
          <div className="moreJob_btn">
            <ActionBtn
              btnText="VIEW MORE JOBS"
              btnIcon={<IcDownArrowIcon height="6" />}
            />
          </div>
        ) : null} */}
      </div>
    );
  }
}

MyJobCard.propTypes = {
  myJobCardsWidthLeft: PropTypes.string,
  myJobCardsWidthRight: PropTypes.string,
  hit: PropTypes.string
};

MyJobCard.defaultProps = {
  myJobCardsWidthLeft: "11",
  myJobCardsWidthRight: "5",
  hit: {
    title: "UX UI Designer",
    comName: "Wipro",
    locCity: "Noida",
    locCountry: "India",
    skills: [
      "html",
      "css",
      "javascript"
    ]
  }
};

export default MyJobCard;