import React from "react";
import PropTypes from "prop-types";
import { Grid, Image, Button, Icon } from "semantic-ui-react";

import MediaCard from "../MediaCard";

import CardSummary from "../CardSummary";
import InfoSection from "../../Sections/InfoSection";



import IcEducation from "../../../assets/svg/IcEducation";
import "./index.scss";


const InfoCardExperience = () => {
  return (
    <div>
      <InfoSection headerSize="medium" color="blue" headerText="Experience">
        <MediaCard fluid shadowless small>
          <MediaCard.Avatar>
            <IcEducation rectcolor="#f7f7fb" pathcolor="#c8c8c8" />
          </MediaCard.Avatar>
          <CardSummary marginless>
            <CardSummary.Item black>
              Vemana Institute of technology
            </CardSummary.Item>
            <CardSummary.Item>B.Tech/B.E.| Computers</CardSummary.Item>
          </CardSummary>
        </MediaCard>
      </InfoSection>
    </div>
  );
};



InfoCardExperience.propTypes = {};

InfoCardExperience.defaultProps = {};

export default InfoCardExperience;
