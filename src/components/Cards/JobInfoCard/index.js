import React from "react";
import PropTypes from "prop-types";
import { Grid, Image, Button, Icon } from "semantic-ui-react";

import MediaCard from "../MediaCard";
import CardGlobalActions from "../CardActions/CardGlobalActions";
import CardSummary from "../CardSummary";
import SkillList from "../../CardElements/SkillList";
import InfoSection from "../../Sections/InfoSection";
import SearchCard from "../SearchCard";
import SearchCardHeader from "../SearchCard/SearchCardHeader";
import SearchCardFooter from "../SearchCard/SearcCardFooter";

import IcCompany from "../../../assets/svg/IcCompany";
import IcLocation from "../../../assets/svg/IcLocation";
import Icjob from "../../../assets/svg/Icjob";
import IcEducation from "../../../assets/svg/IcEducation";
import IcDownArrow from "../../../assets/svg/IcDownArrow";

import JobMediaCard from "../JobMediaCard";

import "./index.scss";

// const JobMediaCard = props => {
//   return (
//     <MediaCard fluid>
//       <MediaCard.Avatar>
//         <IcCompany rectcolor="#f7f7fb" pathcolor="#c8c8c8" />
//       </MediaCard.Avatar>
//       <Grid columns={2}>
//         <Grid.Row>
//           <Grid.Column width={9}>
//             <CardSummary>
//               <CardSummary.Item header>Semantic UI Developer</CardSummary.Item>
//               <CardSummary.Item
//                 icon={<Icjob height="14px" width="16px" pathcolor="#0B9ED0" />}
//                 big>
//                 Maven Workforce
//               </CardSummary.Item>
//               <CardSummary.Item group>
//                 <CardSummary.Item
//                   icon={
//                     <IcLocation
//                       height="14px"
//                       width="10px"
//                       pathcolor="#0bd0bb"
//                     />
//                   }>
//                   Noida, Uttar Pradesh, India
//                 </CardSummary.Item>
//                 <CardSummary.Item>
//                   <span>
//                     <strong>3-4 yrs</strong> exp
//                   </span>
//                 </CardSummary.Item>
//               </CardSummary.Item>
//             </CardSummary>
//           </Grid.Column>
//           <Grid.Column width={7} textAlign="center">
//             <SearchCardHeader />
//             <Button primary style={{ marginTop: "10px" }}>
//               Apply
//               <Icon name="angle right" />
//             </Button>
//           </Grid.Column>
//         </Grid.Row>
//         <Grid.Row columns={1}>
//           <Grid.Column>
//             {!props.active ? (
//               <SkillList small />
//             ) : (
//               <CardSummary centered>
//                 <CardSummary.Extra
//                   extras={[
//                     {
//                       key: 1,
//                       head: "Current:",
//                       body: "Thomson digital Pvt. Ltd"
//                     },
//                     {
//                       key: 2,
//                       head: "Current:",
//                       body: "Thomson digital Pvt. Ltd"
//                     }
//                   ]}
//                 />
//               </CardSummary>
//             )}
//           </Grid.Column>
//         </Grid.Row>
//       </Grid>
//     </MediaCard>
//   );
// };

// const JobCardDetail = () => {
//   return (
//     <div>
//       <MediaCard fluid shadowless>
//         <InfoSection headerSize="medium" color="blue" headerText="Skills">
//           <SkillList />
//         </InfoSection>
//         <InfoSection headerSize="medium" color="blue" headerText="Experience">
//           <MediaCard fluid shadowless small>
//             <MediaCard.Avatar>
//               <IcEducation rectcolor="#f7f7fb" pathcolor="#c8c8c8" />
//             </MediaCard.Avatar>
//             <CardSummary marginless>
//               <CardSummary.Item black>
//                 Vemana Institute of technology
//               </CardSummary.Item>
//               <CardSummary.Item>B.Tech/B.E.| Computers</CardSummary.Item>
//             </CardSummary>
//           </MediaCard>
//         </InfoSection>
//         <SearchCardFooter />
//       </MediaCard>
//     </div>
//   );
// };

const InfoCard = props => {
  const {children} = props;
  return (
    <SearchCard>
      <SearchCard.Accordion
        header={JobMediaCard}
        body={() => (
          <div>
            {children}
          </div>
        )}
      />
    </SearchCard>
  );
};

InfoCard.propTypes = {};

InfoCard.defaultProps = {};

export default InfoCard;
