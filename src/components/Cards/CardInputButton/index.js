import React from "react";
import PropTypes from "prop-types";
import "./index.scss";
import { Card, Container, Header } from "semantic-ui-react";

import InputButton from "../../InputButton";

// const CardInputButton = () => {
//   return (
//     <Card className="CardInputButton" fluid>
//       <Card.Content>
//         <Header as="h3">{label}</Header>
//         <InputButton subTitle={subTitle} />
//       </Card.Content>
//       {/* <div className="messageShow">
//         <p className="title">Thank You!</p>
//         <p className="subTitle">
//           you will be the first to know about new matching {subTitle}.
//         </p>
//       </div> */}
//     </Card>
//   );
// };

class CardInputButton extends React.Component {
  state = {
    isEmailSent: false
  };

  onEmailSent = () => {
    this.setState({
      isEmailSent: true
    });
  };

  render() {
    const { label, subTitle, subTitleHead, sendMessageTitle } = this.props;

    return (
      <Card className="CardInputButton" fluid>
        {!this.state.isEmailSent ? (
          <Card.Content>
            <Header as="h3">{label}</Header>
            {subTitleHead ? (
              <p className="subTitleHead">{subTitleHead}</p>
            ) : null}
            <InputButton onEmailSent={this.onEmailSent} subTitle={subTitle} />
          </Card.Content>
        ) : (
          <div className="messageShow">
            <p className="title">Thanks {sendMessageTitle}</p>
            {/* <p className="subTitle">
              you will be the first to know about new matching {subTitle}.
            </p> */}
          </div>
        )}
      </Card>
    );
  }
}

CardInputButton.propTypes = {
  label: PropTypes.string,
  sendMessageTitle: PropTypes.string
};

CardInputButton.defaultProps = {
  label: "Email me jobs like these"
};

export default CardInputButton;
