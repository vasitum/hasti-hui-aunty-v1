import React, { Component } from "react";
import PropTypes from "prop-types";

import BestSearchCard from "./BestSearchCard";
import SearchCardAccordion from "./SearchCardAccordion";
import SearchCardMobile from "./SearchCardMobile";

class SearchCard extends Component {
  static Best = BestSearchCard;
  static Accordion = SearchCardAccordion;
  static Mobile = SearchCardMobile;

  render() {
    return <div className="SearchCard">{this.props.children}</div>;
  }
}

SearchCard.propTypes = {
  children: PropTypes.any
};

export default SearchCard;
