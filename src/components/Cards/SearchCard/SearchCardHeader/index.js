import React from "react";
import PropTypes from "prop-types";

import { Grid, Icon } from "semantic-ui-react";

import CardGlobalActions from "../../CardActions/CardGlobalActions";
import "./index.scss";

const SearchCardHeader = props => {
  return (
    <div className="SearchCardHeader">
      <CardGlobalActions {...props} />
        <div className="SearchCardHeader__date">
          <span>Posted:</span> <span>Today</span>
        </div>
      <Icon name="angle down" className="vas_accordionArrow" circular />
    </div>
  );
};

SearchCardHeader.propTypes = {};

export default SearchCardHeader;
