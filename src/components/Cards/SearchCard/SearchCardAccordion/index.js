import React, { Component } from "react";
import PropTypes from "prop-types";
import { Accordion } from "semantic-ui-react";

import "./index.scss";

class SearchCardAccordion extends Component {
  state = {
    isActive: false
  };

  onAccordionClick = () => {
    const { noClick } = this.props;
    if (noClick) {
      return;
    }
    this.setState(prevState => {
      return {
        isActive: !prevState.isActive
      };
    });
  };

  componentDidMount() {
    const { active, defaultActive } = this.props;
    if (!active) {
      return;
    }

    if (defaultActive) {
      this.setState({
        isActive: defaultActive
      });

      return;
    }

    this.setState({
      isActive: true
    });
  }

  componentDidUpdate() {
    const { active } = this.props;
    const { isActive } = this.state;

    if (!active || active === isActive) {
      return;
    }

    this.setState({
      isActive: active
    });
  }

  render() {
    const { index, header, body, active, ...restProps } = this.props;
    const Header = header;
    const Body = body;

    return (
      <div className="SearchCardAccordion">
        <Accordion>
          <Accordion.Title
            key={"dskad"}
            onClick={this.onAccordionClick}
            active={this.state.isActive}
            index={index}>
            <Header active={this.state.isActive} {...restProps} />
          </Accordion.Title>
          <Accordion.Content key={"dgdfhg"} active={this.state.isActive}>
            <Body active={this.state.isActive} {...restProps} />
          </Accordion.Content>
        </Accordion>
      </div>
    );
  }
}

SearchCardAccordion.propTypes = {
  header: PropTypes.any,
  body: PropTypes.any,
  index: PropTypes.any
};

export default SearchCardAccordion;
