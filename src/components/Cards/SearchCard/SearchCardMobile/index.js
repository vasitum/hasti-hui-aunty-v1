import React from "react";
import PropTypes from "prop-types";

const SearchCardMobile = props => {
  return <div className="SearchCardMobile">{props.children}</div>;
};

SearchCardMobile.propTypes = {};

export default SearchCardMobile;
