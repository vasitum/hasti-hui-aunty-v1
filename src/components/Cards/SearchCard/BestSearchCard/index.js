import React from "react";
import PropTypes from "prop-types";
import { Reveal } from "semantic-ui-react";

import "./index.scss";

const BestSearchCard = ({ VisibleComponent, HiddenComponent }) => {
  return (
    <div className="BestSearchCard">
      <div className="show">
        <VisibleComponent />
      </div>
      <div className="hide">
        <HiddenComponent />
      </div>
    </div>
  );
};

BestSearchCard.propTypes = {};

export default BestSearchCard;
