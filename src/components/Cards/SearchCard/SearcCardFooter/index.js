import React from "react";
import PropTypes from "prop-types";
import { Icon, Button } from "semantic-ui-react";
import CardGlobalActions from "../../CardActions/CardGlobalActions";
import "./index.scss";

const SearchCardFooter = props => {
  return (
    <div className="SearchCardFooter">
      <CardGlobalActions {...props} />
      <Button primary>
        Apply
        <Icon name="angle right" />
      </Button>
      <Icon name="angle up" className="vas_accordionArrow" circular />
    </div>
  );
};

SearchCardFooter.propTypes = {};

export default SearchCardFooter;
