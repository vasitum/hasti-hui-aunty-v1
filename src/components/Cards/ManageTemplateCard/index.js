import React from "react";
import { Grid, Image, Button, Icon, Card, Modal } from "semantic-ui-react";
import MediaCard from "../MediaCard";
import CardSummary from "../CardSummary";
import QuillText from "../../CardElements/QuillText";
import SearchCard from "../SearchCard";
import IcCompany from "../../../assets/svg/IcCompany";
import ActionBtn from "../../Buttons/ActionBtn";
import "./index.scss";
const ManageTemplateCard = () => {
  return (
    <div>
      <MediaCard fluid style={{ padding: 20 }}>
        <Grid>
          <Grid.Row>
            <Grid.Column width={9} className="manegeCardHeaderLeft">
              <CardSummary>
                <CardSummary.Item header>This is summary</CardSummary.Item>
                <p>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit.
                  Placeat dignissimos odit ratione. Commodi, nam recusandae?
                  Voluptatum quaerat ipsum, necessitatibus distinctio optio
                  dolore quia repellendus sit mollitia ab illo consequatur
                  ullam.
                </p>

                <p>
                  <span>employees</span>
                </p>
                <span
                  className="CreateTime"
                  style={{ fontFamily: "Open Sans" }}>
                  <span style={{ color: "#a1a1a1" }}>Created: </span>
                  <span style={{ color: "#0b9ed0" }}>Today</span>
                </span>
              </CardSummary>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </MediaCard>
    </div>
  );
};

export default ManageTemplateCard;
