import React from "react";


import { Header, Segment, Grid } from "semantic-ui-react";
import PropTypes from 'prop-types';
import PrimaryCard from "../PrimaryCard";
import InputButton from "../../InputButton";

import "./index.scss";

const EmailAlertCard = props => {
  const { headerText } = props;
  return (
    <div className="EmailAlertCard">
      <Grid>
        <Grid.Row>
          <Grid.Column computer={16} only="computer">
            <Segment className="EmailAlert_segment">
              <Header>
                {headerText}
              </Header>
              <InputButton />
            </Segment>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  )
}

EmailAlertCard.propTypes = {
  headerText: PropTypes.string
}

EmailAlertCard.defaultProps = {
  headerText: "Email me jobs like this"
}

export default EmailAlertCard;