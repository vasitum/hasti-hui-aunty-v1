import React from "react";
import PropTypes from "prop-types";
import {
  Grid,
  Button,
  Icon,
  Checkbox,
  Dropdown,
  Image
} from "semantic-ui-react";
import MoreOption from "../../../assets/svg/IcMoreOptionIcon";
import MediaCard from "../MediaCard";
import CardSummary from "../CardSummary";
import SearchCard from "../SearchCard";
import IcDownArrowIcon from "../../../assets/svg/IcDownArrow";
import IcCompany from "../../../assets/svg/IcCompany";

import IcUser from "../../../assets/svg/IcUser";

import ActionBtn from "../../Buttons/ActionBtn";

import CandidateCardFooter from "../../Dashboard/RecruiterDashboard/Candidates/CandidateCardFooter";

import IcCalendarCard from "../../../assets/svg/IcCalendarCard";
import fromNow from "../../../utils/env/fromNow";
import { Link } from "react-router-dom";
import { format } from "date-fns";
import CandidateCardTag from "./CandidateCardTag";
import getApplicationTag from "../../../utils/env/getApplicationTag";

import "./index.scss";

const profileOptions = [
  {
    text: "Download resume",
    value: "Download resume"
  },
  {
    text: "Send message",
    value: "Send message"
  },
  {
    text: "Share",
    value: "Share"
  },
  {
    text: "Likes",
    value: "Likes"
  }
];

function genrateDownloadLink(name, id, ext) {
  const [fName, lName] = name.split(/_(.+)/);

  return `https://s3-us-west-2.amazonaws.com/res-mwf/${id}/${fName +
    "_" +
    lName}.${ext}`;
}

function isChecked(selectedItems, jobId, applicationId) {
  let isChecked = false;

  if (!Array.isArray(selectedItems)) {
    return isChecked;
  }

  selectedItems.map(item => {
    if (item.applicationId === applicationId && item.jobId === jobId) {
      isChecked = true;
    }
  });

  return isChecked;
}

class JobMediaCard extends React.PureComponent {
  state = {
    checked: false
  };

  onCheckboxChange = (e, { value }) => {
    // let's not open the card shall we?
    // brit flair!
    e.stopPropagation();

    // this.setState(
    //   {
    //     checked: !this.state.checked
    //   },
    //   () => {

    //     onItemSelect(hit.objectID);
    //   }
    // );
    const { hit, onItemSelect } = this.props;
    if (onItemSelect) {
      // console.log("Item Selected", hit.objectID, hit.jobId);
      onItemSelect(hit.objectID, hit.jobId, hit.candidateId);
    }
  };

  // isChecked = (selectedItems, jobId, applicationId) => {
  //   let isChecked = false;

  //   if (!Array.isArray(selectedItems)) {
  //     return isChecked;
  //   }

  //   selectedItems.map(item => {
  //     if (item.applicationId === applicationId && item.jobId === jobId) {
  //       isChecked = true;
  //     }
  //   });

  //   return isChecked;
  // };

  render() {
    const { hit, selectedItems, checked } = this.props;
    const tag = getApplicationTag(hit);
    return (
      <div className="CandidateMediaCard">
        <MediaCard fluid>
          <MediaCard.Avatar>
            {!hit.candidateImageExt ? (
              <IcUser
                rectcolor="#f7f7fb"
                height="50"
                width="50"
                pathcolor="#c8c8c8"
              />
            ) : (
              <Image
                verticalAlign="top"
                style={{
                  height: "50px",
                  width: "50px",
                  // position: "absolute",
                  borderRadius: "50%"
                }}
                src={`https://s3-us-west-2.amazonaws.com/img-mwf/${
                  hit.candidateId
                }/image.jpg`}
              />
            )}
            <div className="CandidatesCardMatch">
              <p>{hit.matches}%</p>
              <span>matched</span>
            </div>

            <div className="CandidatesCardCheckbox">
              <Checkbox
                // checked={this.isChecked(selectedItems, hit.jobId, hit.objectID)}
                checked={checked}
                onChange={this.onCheckboxChange}
              />
            </div>
          </MediaCard.Avatar>
          <Grid>
            <Grid.Row>
              <Grid.Column computer={6} mobile={14}>
                <CardSummary>
                  <CardSummary.Item header>
                    <Link to={`/job/${hit.jobId}/application/${hit.objectID}`}>
                      {hit.candidateName}
                    </Link>
                  </CardSummary.Item>
                  <div className="CardProfileHeaderDetail">
                    <CardSummary.Item>{hit.candidateLocation}</CardSummary.Item>
                    <CardSummary.Item>
                      Phone: &nbsp;
                      <span>{hit.candidatePhone}</span>
                    </CardSummary.Item>
                    <CardSummary.Item>
                      Email:&nbsp; <span>{hit.candidateEmail}</span>
                    </CardSummary.Item>
                    <CardSummary.Item>
                      Applied:&nbsp; <span>{hit.jobName}</span>
                    </CardSummary.Item>
                    {tag ? (
                      <CardSummary.Item>
                        <CandidateCardTag currentTag={tag} />
                      </CardSummary.Item>
                    ) : null}
                  </div>
                </CardSummary>
              </Grid.Column>

              <Grid.Column
                className="moblilMore_cardDetail"
                mobile={2}
                textAlign="right">
                <Dropdown
                  pointing="top left"
                  className="mobileDropdown_cardDetail"
                  icon={<MoreOption pathcolor="#acaeb5" />}>
                  <Dropdown.Menu>
                    {hit.candidateCvExt ? (
                      <Dropdown.Item
                        as={"a"}
                        href={genrateDownloadLink(
                          hit.candidateName,
                          hit.candidateId,
                          hit.candidateCvExt
                        )}
                        text="Download resume"
                      />
                    ) : null}

                    <Dropdown.Item
                      as={Link}
                      to={"/messaging/" + hit.candidateId}
                      text="Send message"
                    />
                    <Dropdown.Item
                      className="Desc_option"
                      description={fromNow(hit.appliedDate)}
                      text="Submitted"
                    />
                  </Dropdown.Menu>
                </Dropdown>
              </Grid.Column>

              <Grid.Column
                computer={10}
                textAlign="right"
                className="desktopViewBtn_cardDetail">
                <div className="SearchCardHeader">
                  <CardSummary centered>
                    <CardSummary.Extra
                      extras={[
                        {
                          key: 1,
                          head: "Submitted:",
                          body: fromNow(hit.appliedDate),
                          class: "todayTitle",
                          title: new Date(hit.appliedDate)
                        }
                      ]}
                    />
                  </CardSummary>

                  {/* <span className="accordionArrowCircle">
                    <Icon name="caret down" className="accordionArrow" />
                  </span> */}
                </div>

                <div className="SearchCardHeaderBtn">
                  {/* <Button ></Button> */}
                  {hit.interviewScheduledDate ? (
                    <Button className="interViewBtn">
                      <div className="interViewBtn_box">
                        <div className="svgIcon">
                          <IcCalendarCard pathcolor="#0bd0bb" />
                        </div>
                        <div className="interTitle">
                          <p>
                            Date:{" "}
                            <span>
                              {format(
                                hit.interviewScheduledDate,
                                "DD MMM, YYYY"
                              )}
                            </span>
                          </p>
                          <p>
                            Time:{" "}
                            <span>
                              {format(hit.interviewScheduledDate, "hh:mm A")}
                            </span>
                          </p>
                        </div>
                      </div>
                    </Button>
                  ) : null}

                  {/* TODO: Add Image Extension */}
                  {hit.candidateCvExt ? (
                    <Button
                      as={"a"}
                      href={`https://s3-us-west-2.amazonaws.com/res-mwf/${
                        hit.candidateId
                      }/${hit.candidateName.split(/_(.+)/).join("_")}.${
                        hit.candidateCvExt
                      }`}
                      className="downloadCV_btn">
                      Download CV
                    </Button>
                  ) : null}
                  <ActionBtn
                    as={Link}
                    // to={"/messaging/" + hit.candidateId}
                    to={
                      hit.interviewScheduledDate
                        ? `/job/${hit.jobId}/application/${
                            hit.objectID
                          }?screen=interview`
                        : `/job/${hit.jobId}/application/${hit.objectID}`
                    }
                    actioaBtnText={
                      hit.interviewScheduledDate
                        ? "Edit Interview"
                        : "View Detail"
                    }
                    // btnIcon={<IcDownArrowIcon pathcolor="#99d9f9" />}
                  />
                </div>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </MediaCard>
      </div>
    );
  }
}

const InfoCard = props => {
  const { hit, noClick, onItemSelect, selectedItems, children } = props;
  console.log("Hit", hit);
  const checked = isChecked(selectedItems, hit.jobId, hit.objectID);

  return (
    <div className={`CandidatesCardDetail ${checked ? "is-checked" : ""}`}>
      <SearchCard>
        <SearchCard.Accordion
          noClick={noClick}
          header={() => (
            <JobMediaCard
              checked={checked}
              hit={hit}
              selectedItems={selectedItems}
              onItemSelect={onItemSelect}
            />
          )}
          body={() => <div>{children}</div>}
        />
        <CandidateCardFooter hit={hit} />
      </SearchCard>
    </div>
  );
};

InfoCard.propTypes = {};

InfoCard.defaultProps = {};

export default InfoCard;
