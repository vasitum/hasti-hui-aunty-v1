import React, { Component } from "react";
import "./index.scss";

export default ({ currentTag }) => {
  return (
    <div className="CandidateCardDetailTag">
      <div className="CandidateCardDetailTag__indicator" />
      <div className="CandidateCardDetailTag__text">{currentTag}</div>
    </div>
  );
};
