import React from "react";

import SearchCard from "../../Cards/SearchCard";

import "./index.scss";

class AccordionCardContainer extends React.Component {
  render() {
    const {
      cardHeader,
      noClick,
      children,
      active,
      defaultActive,
      addClass,
      ...resProps
    } = this.props;

    return (
      <div className={`AccordionCardContainer ${!addClass ? "":addClass}`} >
        <SearchCard {...resProps}>
          <SearchCard.Accordion
            active={active}
            defaultActive={defaultActive}
            noClick={noClick}
            header={props => <div {...props}>{cardHeader}</div>}
            body={() => <div>{children}</div>}
          />
          {/* <CandidateCardFooter hit={hit} /> */}
        </SearchCard>
      </div>
    );
  }
}

export default AccordionCardContainer;
