import React from "react";

import { Segment} from "semantic-ui-react";
import PropTypes from 'prop-types';

import "./index.scss";

const PrimaryCard = props => {
  const { children } = props;
  return(
    <Segment className="PrimaryCard">
      {children}
    </Segment>
  )
};

PrimaryCard.propTypes = {
  children: PropTypes.any
};

PrimaryCard.defaultProps = {
  
};

export default PrimaryCard;

