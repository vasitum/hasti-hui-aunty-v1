import React from "react";
import PropTypes from "prop-types";
import { Grid, Image, Button, Icon } from "semantic-ui-react";

import MediaCard from "../MediaCard";
import CardGlobalActions from "../CardActions/CardGlobalActions";
import CardSummary from "../CardSummary";
import SkillList from "../../CardElements/SkillList";
import InfoSection from "../../Sections/InfoSection";
import SearchCard from "../SearchCard";
import SearchCardHeader from "../SearchCard/SearchCardHeader";
import SearchCardFooter from "../SearchCard/SearcCardFooter";

import IcCompany from "../../../assets/svg/IcCompany";
import IcLocation from "../../../assets/svg/IcLocation";
import Icjob from "../../../assets/svg/Icjob";
import IcEducation from "../../../assets/svg/IcEducation";
import IcDownArrow from "../../../assets/svg/IcDownArrow";

import PeopleMediaCard from "../PeopleMediaCard";





const InfoCard = props => {
  const {children} = props;
  return (
    <SearchCard>
      <SearchCard.Accordion
        header={PeopleMediaCard}
        body={() => (
          <div>
            {children}
          </div>
        )}
      />
    </SearchCard>
  );
};

InfoCard.propTypes = {};

InfoCard.defaultProps = {};

export default InfoCard;
