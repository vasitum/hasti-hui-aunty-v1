import React, { Component } from "react";
import PropTypes from "prop-types";
import cx from "classnames";

import CardSummaryItem from "./CardSummaryItem";
import CardSummaryExtra from "./CardSummaryExtra";

import "./index.scss";

class CardSummary extends Component {
  static Item = CardSummaryItem;
  static Extra = CardSummaryExtra;

  render() {
    const { marginless, centered } = this.props;

    const classes = cx({
      CardSummary: true,
      [`is-marginless`]: marginless,
      [`is-centered`]: centered
    });

    return <div className={classes}>{this.props.children}</div>;
  }
}

CardSummary.propTypes = {};

export default CardSummary;
