import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";

import { Header } from "semantic-ui-react";

const CardSummaryItem = ({
  header,
  icon,
  big,
  black,
  children,
  group,
  centered,
  itemText
}) => {
  const classes = cx({
    CardSummaryItem: true,
    "has-icon": !!icon,
    "is-big": !!big,
    "has-text-black": black,
    "is-grouped": group,
    "is-centered": centered
  });

  if (header) {
    return (
      <Header as="h2" className={classes}>
        {children}
      </Header>
    );
  }

  if (icon) {
    return (
      <p className={classes}>
        <span className="mobile hidden">{icon}</span>
        {children}
      </p>
    );
  }

  return <p className={classes}>{children}</p>;
};

CardSummaryItem.propTypes = {
  header: PropTypes.bool,
  icon: PropTypes.any,
  big: PropTypes.bool,
  children: PropTypes.any
};

export default CardSummaryItem;
