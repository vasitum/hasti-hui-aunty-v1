import React from "react";
import PropTypes from "prop-types";

const CardSummaryExtra = ({ extras }) => {
  return extras.map(val => (
    <span key={val.key} title={val.title} className="CardSummaryExtra">
      <span className="head">{val.head}</span>
      <span className={`body ${val.class}`}>{val.body}</span>
    </span>
  ));
};

CardSummaryExtra.propTypes = {};

export default CardSummaryExtra;
