import React from "react";
import PropTypes from "prop-types";

import { Popup, Button } from "semantic-ui-react";
import IcLike from "../../../../assets/svg/IcLike";
import IcShare from "../../../../assets/svg/IcShare";
import IcSave from "../../../../assets/svg/IcSave";

import "./index.scss";

const currentActions = [
  {
    key: 0,
    trigger: (
      <Button size="mini" circular inverted compact>
        <IcLike width="15px" height="15px" pathcolor="#c8c8c8" />
      </Button>
    ),
    content: "Like",
    className: "socialPropup"
  },
  {
    key: 1,
    trigger: (
      <Button size="mini" circular inverted compact>
        <IcShare width="15px" height="15px" pathcolor="#c8c8c8" />
      </Button>
    ),
    content: "Share",
    className: "socialPropup"
  },
  {
    key: 2,
    trigger: (
      <Button size="mini" circular inverted compact>
        <IcSave width="15px" height="15px" pathcolor="#c8c8c8" />
      </Button>
    ),
    content: "Save",
    className: "socialPropup"
  }
];

const CardGlobalActions = props => {
  return (
    <div className="CardGlobalAction">
      {currentActions.map(val => {
        return (
          <Popup key={val.key} trigger={val.trigger} content={val.content} className={val.socialPropup}/>
        );
      })}
    </div>
  );
};

CardGlobalActions.propTypes = {};

export default CardGlobalActions;
