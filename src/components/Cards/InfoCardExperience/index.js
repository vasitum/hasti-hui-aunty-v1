import React from "react";
import PropTypes from "prop-types";
import { Grid, Image, Button, Icon } from "semantic-ui-react";

import MediaCard from "../MediaCard";
import CardSummary from "../CardSummary";
import InfoSection from "../../Sections/InfoSection";
import IcEducation from "../../../assets/svg/IcEducation";
import "./index.scss";


const InfoCardExperience = props => {
  
  const { cardTitle, cardSubTitle, cardAvatar } = props;
  
  return (
   
    <InfoSection headerSize="medium" color="blue" headerText="Experience">
      <MediaCard fluid shadowless small>
        <MediaCard.Avatar>
          {cardAvatar}
          {/* <IcEducation rectcolor="#f7f7fb" pathcolor="#c8c8c8" /> */}
        </MediaCard.Avatar>
        <CardSummary marginless>
          <CardSummary.Item black>
            { cardTitle }
            {/* Vemana Institute of technology */}
            {/* B.Tech/B.E.| Computers */}
          </CardSummary.Item>
          <CardSummary.Item>{ cardSubTitle }</CardSummary.Item>
        </CardSummary>
      </MediaCard>
    </InfoSection>
   
  );
};



InfoCardExperience.propTypes = {
  cardAvatar: PropTypes.any,
  cardTitle: PropTypes.string,
  cardSubTitle: PropTypes.string
};

InfoCardExperience.defaultProps = {
  cardTitle: "Vemana Institute of technology",
  cardSubTitle: "cardSubTitle"
};

export default InfoCardExperience;
