import React from "react";
import PropTypes from "prop-types";
import { Grid, Image, Button, Icon, Card, Modal } from "semantic-ui-react";

import MediaCard from "../MediaCard";
// import CardGlobalActions from "../CardActions/CardGlobalActions";
import CardSummary from "../CardSummary";
import SkillList from "../../CardElements/SkillList";
import QuillText from "../../CardElements/QuillText";
import InfoSection from "../../Sections/InfoSection";
import SearchCard from "../SearchCard";
// import SearchCardHeader from "../SearchCard/SearchCardHeader";
import SearchCardFooter from "../SearchCard/SearcCardFooter";

import IcCompany from "../../../assets/svg/IcCompany";
import IcLocation from "../../../assets/svg/IcLocation";
import Icjob from "../../../assets/svg/Icjob";
import IcEducation from "../../../assets/svg/IcEducation";
import IcDownArrow from "../../../assets/svg/IcDownArrow";
import EditCompanyPage from "../../ModalPageSection/EditCompany";

import fromNow from "../../../utils/env/fromNow";

import ActionBtn from "../../Buttons/ActionBtn";

import "./index.scss";

class EditCompanyModal extends React.Component {
  state = {
    editModal: false
  };

  onEditModalOpen = e => {
    this.setState({
      editModal: true
    });
  };

  onEditModalClose = e => {
    this.setState({
      editModal: false
    });
  };

  render() {
    const { actionBtn, val, onCompanyCreate, onEnableFormLoader } = this.props;
    return (
      <Modal
        open={this.state.editModal}
        onClose={this.onEditModalClose}
        className="modal_form"
        trigger={
          actionBtn ? (
            <ActionBtn onClick={this.onEditModalOpen} actioaBtnText="Edit" />
          ) : (
            <Button onClick={this.onEditModalOpen} className="editBtn">
              Edit
            </Button>
          )
        }
        closeIcon
        close
        closeOnDimmerClick={false}>
        <EditCompanyPage
          onModalClose={this.onEditModalClose}
          data={val}
          onCompanyCreate={onCompanyCreate}
          onEnableFormLoader={onEnableFormLoader}
        />
      </Modal>
    );
  }
}

const JobMediaCard = props => {
  const { isShowManegeCardHeaderRight } = props;

  return (
    <div>
      <MediaCard fluid>
        <MediaCard.Avatar>
          {!props.content.imgExt ? (
            <IcCompany
              rectcolor="#f7f7fb"
              pathcolor="#c8c8c8"
              height="50"
              width="50"
              style={{ borderRadius: "6px" }}
            />
          ) : (
            <Image
              className="manegeCard_img"
              src={`https://s3-us-west-2.amazonaws.com/img-mwf/company/${
                props.content._id
              }/image.jpg`}
            />
          )}
        </MediaCard.Avatar>
        <Grid columns={2}>
          <Grid.Row>
            <Grid.Column width={9} className="manegeCardHeaderLeft">
              <CardSummary>
                <CardSummary.Item header>{props.content.name}</CardSummary.Item>
                <p
                  style={{
                    display:
                      !props.content.headquarter ||
                      !props.content.headquarter.location
                        ? "none"
                        : "block"
                  }}>
                  {/* <IcLocation height="10px" width="10px" pathcolor="#0bd0bb" /> */}
                  {props.content.headquarter.location}
                </p>

                <p
                  style={{
                    display: !props.content.size ? "none" : "block"
                  }}>
                  {props.content.size} <span>employees</span>
                </p>
                <span
                  className="CreateTime"
                  style={{ fontFamily: "Open Sans" }}>
                  <span style={{ color: "#a1a1a1" }}>Created: </span>
                  <span style={{ color: "#0b9ed0" }}>
                    {fromNow(props.content.creationTime)}
                  </span>
                </span>
              </CardSummary>
            </Grid.Column>
            {!isShowManegeCardHeaderRight ? (
              <Grid.Column
                className="manegeCardHeaderRight"
                width={7}
                textAlign="right">
                <div className="SearchCardHeader">
                  {/* <div className="SearchCardHeader__date">
                
              </div> */}
                  <span style={{ fontFamily: "Open Sans" }}>
                    <span style={{ color: "#a1a1a1" }}>Created: </span>
                    <span style={{ color: "#0b9ed0" }}>
                      {fromNow(props.content.creationTime)}
                    </span>
                  </span>
                  <Icon name="angle down" className="vas_accordionArrow" />
                </div>
                <div style={{ paddingTop: " 22px" }}>
                  <div className="header-btn">
                    {/* <Button
                      onClick={ev => props.onCompanyDelete(props.content._id)}
                      inverted
                      className="deleteBtn">
                      Delete
                    </Button> */}

                    {/* <Modal
                      open={props.editModal}
                      onClose={props.onEditModalClose}
                      className="modal_form"
                      trigger={
                        <Button
                          onClick={props.onEditModalOpen}
                          className="editBtn">
                          Edit
                        </Button>
                      }
                      closeIcon
                      close
                      closeOnDimmerClick={false}>
                      <EditCompanyPage
                        onModalClose={props.onEditModalClose}
                        data={props.content}
                        onCompanyCreate={props.onCompanyCreate}
                        onEnableFormLoader={props.onEnableFormLoader}
                      />
                    </Modal> */}
                    <EditCompanyModal
                      val={props.content}
                      onCompanyCreate={props.onCompanyCreate}
                      onEnableFormLoader={props.onEnableFormLoader}
                    />
                  </div>
                </div>
              </Grid.Column>
            ) : null}
          </Grid.Row>
          {/* <Grid.Row columns={1} className="cardHeader_footer">
          <Grid.Column textAlign="right">
            
          </Grid.Column>
        </Grid.Row> */}
        </Grid>
      </MediaCard>
    </div>
  );
};

class InfoCard extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { props } = this;

    const {
      content,
      onCompanyCreate,
      onCompanyDelete,
      onEnableFormLoader
    } = props;
    return content.map(val => (
      <SearchCard key={val._id}>
        <SearchCard.Accordion
          header={() => (
            <JobMediaCard
              content={val}
              onCompanyCreate={onCompanyCreate}
              onCompanyDelete={onCompanyDelete}
              onEnableFormLoader={onEnableFormLoader}
            />
          )}
          body={() => (
            <div>
              <Card fluid className="manageCompany_bodyCard">
                <QuillText readMoreLength={300} value={val.about} readOnly />

                <div className="bodyCard_footer">
                  <p
                    style={{
                      display: !val.website ? "none" : "block"
                    }}>
                    Website: <span>{val.website}</span>
                  </p>
                  <p
                    style={{
                      display: !val.year ? "none" : "block"
                    }}>
                    Founded: <span>{val.year}</span>
                  </p>
                  <p
                    style={{
                      display: !val.industry ? "none" : "block"
                    }}>
                    Industry type: <span>{val.industry}</span>
                  </p>
                  <p
                    style={{
                      display: !val.companyType ? "none" : "block"
                    }}>
                    Company type: <span>{val.companyType}</span>
                  </p>
                  <p
                    style={{
                      display: !val.clientName ? "none" : "block"
                    }}>
                    Personal tag: <span>{val.clientName}</span>
                  </p>
                </div>
              </Card>
            </div>
          )}
        />
        <div className="manegeCardFooter">
          <div className="manegeCardFooterBtn">
            {/* <Button
              onClick={ev => props.onCompanyDelete(val._id)}
              inverted
              className="FooterDeleteBtn">
              Delete
            </Button> */}

            {/* <Modal
              open={this.state.isEditModalOpen}
              onClose={this.onEditModalClose}
              className="modal_form"
              trigger={
                <ActionBtn
                  onClick={this.onEditModalOpen}
                  actioaBtnText="Edit"
                />
              }
              closeIcon
              close
              closeOnDimmerClick={false}>
              <EditCompanyPage
                onModalClose={this.onEditModalClose}
                data={val}
                onCompanyCreate={onCompanyCreate}
                onEnableFormLoader={onEnableFormLoader}
              />
            </Modal> */}
            <EditCompanyModal
              actionBtn
              val={val}
              onCompanyCreate={onCompanyCreate}
              onEnableFormLoader={onEnableFormLoader}
            />
          </div>
        </div>
      </SearchCard>
    ));
  }
}

InfoCard.propTypes = {};

InfoCard.defaultProps = {};

export default InfoCard;
