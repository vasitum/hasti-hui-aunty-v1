import React from "react";
import { Link } from "react-router-dom";
import { format } from "date-fns";

import Icjob from "../../assets/svg/Icjob";

import {
  NOTIFICATION_LIST,
  SCREEN_LIST,
  getObjectParams
} from "./utils/constants";
import { RECR_STAGES } from "../../constants/applicationStages";

/**
 * Strings
 */
function getStatusChangeText(
  prevStatus,
  newStatus,
  jobName,
  jobId,
  applicationId,
  userName,
  userId
) {
  return {
    link: `/job/${jobId}/application/${applicationId}`,
    text: (
      <span>
        You shifted{" "}
        <Link to={`/job/${jobId}/application/${applicationId}`}>{userName}</Link>
        's application from {RECR_STAGES[prevStatus]} to {RECR_STAGES[newStatus]}
      </span>
    )
  };
}

function getJobExpireText(jobtitle, companyname) {
  return {
    link: "#",
    text: `Sorry, the position for ${jobtitle} at ${companyname} has been closed by the recruiter`
  }
}

function getBirthdayText(userName) {
  return {
    link: "#",
    text: (
      <span>
        Happy Birthday, ${userName} – Team Vasitum
      </span>
    )
  };
  // `Happy Birthday, ${userName} – Team Vasitum`;
}

function getJobRecommendationText(count) {
  return {
    link: "#",
    text: (
      <span>
        We found ${count} jobs matching your recent search
      </span>
    )
  };
  // `We found ${count} jobs matching your recent search`;
}

function getProfileRecommendationText(count) {
  return {
    link: "#",
    text: (
      <span>
        We found ${count} profiles matching your recent search
      </span>
    )
  };
  
    // `We found ${count} profiles matching your recent search`;
}

function getLikedJobText(userName, jobName, userId, jobId) {
  return {
    link: `/view/job/${jobId}`,
    text: (
      <span>
        You liked job posting for -{" "}
        <Link to={`/view/job/${jobId}`}>{jobName}</Link>
      </span>
    )
  }
}

function getLikedUserText(userName, userId) {
  return {
    link: `/view/user/${userId}`,
    text: (
      <span>
        You Liked <Link to={`/view/user/${userId}`}>{userName}</Link>'s profile
      </span>
    )
  }
}

function getSavedJobText(jobName, jobId) {
  return {
    link: `/view/job/${jobId}`,
    text: (
      <span>
        You saved <Link to={`/view/job/${jobId}`}>{jobName}</Link> job
    </span>
    )
  }

}

function getSavedUserText(userName, userId) {
  return {
    link: `/view/user/${userId}`,
    text: (
      <span>
        You saved <Link to={`/view/user/${userId}`}>{userName}</Link>'s profile
      </span>
    )
  }


}

function getAppliedOnJobText(userName, jobName, date, applicationId, jobId) {
  return {
    link: `/view/job/${jobId}`,
    text: (
      <span>
        You applied for <Link to={`/view/job/${jobId}`}>{jobName}</Link> job
      </span>
    )
  }

}

function getUnlikeJobText(userName, jobName, userId, jobId) {
  return {
    link: `/view/job/${jobId}`,
    text: (
      <span>
        <Link to={`/view/user/${userId}`}>{userName}</Link> liked your job posting
        for <Link to={`/view/job/${jobId}`}>{jobName}</Link>
      </span>
    )
  }

}

function getUnLikedUserText(userName, userId) {
  return {
    link: `/view/user/${userId}`,
    text: (
      <span>
        <Link to={`/view/user/${userId}`}>{userName}</Link> liked your profile
    </span>
    )
  }

}

function getInterviewCreatedByYou(userName, jobId, applicationId) {
  return {
    link: `/job/${jobId}/application/${applicationId}`,
    text: (
      <span>
        You’ve scheduled interview with{" "}
        <Link to={`/job/${jobId}/application/${applicationId}`}>{userName}</Link>.
    </span>
    )
  }

}

function getPostedJobActivityText(jobName, jobId, date) {
  return {
    link: `/job/view/${jobId}`,
    text: (
      <span>
        <Link to={`/job/view/${jobId}`}>{jobName}</Link> job posted
    </span>
    )
  }
}

function getDraftJobActivityText(jobName, jobId, date) {
  return {
    link: `/job/view/${jobId}`,
    text: (
      <span>
        Job post – <Link to={`/job/view/${jobId}`}>{jobName}</Link> – saved in
        Drafts
    </span>
    )
  }

}

function getChangedJobStatusText(jobName, jobId, prevStatus, nextStatus) {
  return {
    link: `/job/view/${jobId}`,
    text: (
      <span>
        Status for <Link to={`/job/view/${jobId}`}>{jobName}</Link> has been
      changed from {prevStatus} to {nextStatus}.
    </span>
    )
  }

}

function getRequestInterviewReScheduleText(jobName, jobId, applicationId) {
  return {
    link: `/job/candidate/${jobId}/application/${applicationId}`,
    text: (
      <span>
        You requested to reschedule the interview for{" "}
        <Link to={`/job/candidate/${jobId}/application/${applicationId}`}>
          {jobName}
        </Link>{" "}
      </span>
    )
  }

}

function getUpdatedUserInterviewReschdule(userName, jobId, applicationId) {
  return {
    link: `/job/${jobId}/application/${applicationId}`,
    text: (
      <span>
        You accepeted interview reschedule request for{" "}
        <Link to={`/job/${jobId}/application/${applicationId}`}>{userName}</Link>{" "}
      </span>
    )
  }

}

function newUser() {
  return {
    link: `#`,
    text: (
      <span>You just joined Vasitum</span>
    )
  }
  // <span>You just joined Vasitum</span>;
}

/**
 * Notification Interview Left
 * @param {*} activity
 */
function getIdsFromActivity(activity) {
  const retParams = getObjectParams(activity.screenInfo);

  switch (activity.screen) {
    case SCREEN_LIST.JOB_APPLICATION_DETAIL:
      return getAppliedOnJobText(
        activity.userName,
        activity.jobName,
        format(activity.date, "D, MMM YYYY"),
        retParams[NOTIFICATION_LIST.JOB_APPLICATION],
        retParams[NOTIFICATION_LIST.JOB]
      );
    case SCREEN_LIST.LIKE_JOB:
      return getLikedJobText(
        activity.userName,
        activity.jobName,
        retParams[NOTIFICATION_LIST.USER],
        retParams[NOTIFICATION_LIST.JOB]
      );
    case SCREEN_LIST.LIKE_USER:
      return getLikedUserText(
        activity.userName,
        retParams[NOTIFICATION_LIST.USER]
      );

    case SCREEN_LIST.SAVE_JOB:
      return getSavedJobText(
        activity.jobName,
        retParams[NOTIFICATION_LIST.JOB]
      );

    case SCREEN_LIST.SAVE_USER:
      return getSavedUserText(
        activity.userName,
        retParams[NOTIFICATION_LIST.USER]
      );

    case SCREEN_LIST.CHANGE_APPLICATION_STATUS:
      return getStatusChangeText(
        activity.preStaus,
        activity.nextStaus,
        activity.jobName,
        retParams[NOTIFICATION_LIST.JOB],
        retParams[NOTIFICATION_LIST.JOB_APPLICATION],
        activity.userName
      );

    case SCREEN_LIST.UN_LIKE_JOB:
      return getUnlikeJobText(
        activity.userName,
        retParams[NOTIFICATION_LIST.JOB]
      );

    case SCREEN_LIST.UN_LIKE_USER:
      return getUnLikedUserText(
        activity.userName,
        retParams[NOTIFICATION_LIST.USER]
      );

    // interview schedule by you
    case SCREEN_LIST.INTERVIEW_CREATE_NOTIFICATION:
      return getInterviewCreatedByYou(
        activity.userName,
        retParams[NOTIFICATION_LIST.JOB],
        retParams[NOTIFICATION_LIST.JOB_APPLICATION]
      );

    // Posted a new Job
    case SCREEN_LIST.VIEW_ACTIVE_JOB:
      return getPostedJobActivityText(
        activity.jobName,
        retParams[NOTIFICATION_LIST.JOB],
        format(activity.date, "D, MMM YYYY")
      );

    // Posted a new Job
    case SCREEN_LIST.VIEW_DRAFT_JOB:
      return getDraftJobActivityText(
        activity.jobName,
        retParams[NOTIFICATION_LIST.JOB],
        format(activity.date, "D, MMM YYYY")
      );

    // you changed statis
    case SCREEN_LIST.CHANGE_JOB_STATUS:
      return getChangedJobStatusText(
        activity.jobName,
        retParams[NOTIFICATION_LIST.JOB],
        activity.preStaus,
        activity.nextStaus
      );

    // you requested interview schedule
    case SCREEN_LIST.INTERVIEW_RESCHEDULE_ACITIVIY:
      return getRequestInterviewReScheduleText(
        activity.jobName,
        retParams[NOTIFICATION_LIST.JOB],
        retParams[NOTIFICATION_LIST.JOB_APPLICATION]
      );

    // you requested interview schedule  :recr dashboard
    case SCREEN_LIST.USER_INTERVIEW_UPDATE:
      return getUpdatedUserInterviewReschdule(
        activity.userName,
        retParams[NOTIFICATION_LIST.JOB],
        retParams[NOTIFICATION_LIST.JOB_APPLICATION]
      );

    // New User
    case SCREEN_LIST.NEW_USER:
      return newUser();

    default:
      break;
  }
}

export function getActivityByType(activity) {
  return getIdsFromActivity(activity);
}
