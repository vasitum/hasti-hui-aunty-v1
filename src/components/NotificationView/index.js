import React, { Component } from "react";
import getNotifications from "../../api/notifications/notifications";
import getActivities from "../../api/notifications/activity";
import setUnreadTicker from "../../api/notifications/unreadTicker";
import { Message, Container, Divider, Header, Button } from "semantic-ui-react";
import { USER } from "../../constants/api";
import NoFoundMessageDashboard from "../Dashboard/Common/NoFoundMessageDashboard";

import { Link } from "react-router-dom";

import ReactGA from "react-ga";

import FlatDefaultBtn from "../Buttons/FlatDefaultBtn";
import IcDownArrow from "../../assets/svg/IcDownArrow";
import withProtectedRoute from "../utils/ProtectedRoute";
import Icjob from "../../assets/svg/Icjob";

import getPlaceholder from "../Forms/FormFields/Placeholder";

import NotificationHeader from "./NotificationHeader";
import { getNotificationByType } from "./NotificationTextParser";
import { getActivityByType } from "./ActivityTextParser";

import { format } from "date-fns";

import "./index.scss";

class NotificationView extends Component {
  state = {
    notifications: [],
    activities: [],
    seeMoreNotification: false,
    seeMoreActivities: false
  };

  constructor(props) {
    super(props);
    this.notifrmEvent = new CustomEvent("NOTIF_NO");
    this.sendUnreadTicker = this.sendUnreadTicker.bind(this);
  }

  onSeeMoreNotificationClick = s => {
    this.setState({
      seeMoreNotification: !this.state.seeMoreNotification
    });
  };

  onSeeMoreActivitiesClick = s => {
    this.setState({
      seeMoreActivities: !this.state.seeMoreActivities
    });
  };

  clearNotification = id => {
    window.localStorage.setItem(USER.LAST_NOTIFICATION, id);
    document.dispatchEvent(this.notifrmEvent);
  };

  async sendUnreadTicker(ids) {
    try {
      const res = await setUnreadTicker(ids);
      if (res.status === 200) {
        console.log("unread!");
      }
    } catch (error) {
      console.error("UNREAD_ERROR", error);
    }
  }

  async componentDidMount() {
    ReactGA.pageview('/notification');
    try {
      const resNotify = await getNotifications();
      if (resNotify.status === 200) {
        const notifyData = await resNotify.json();
        // console.log("check notify", notifyData);
        // window.localStorage.setItem(USER.LAST_NOTIFICATION, notifyData[0]._id);
        // document.dispatchEvent(notifrmEvent);
        if (Array.isArray(notifyData) && notifyData.length > 0) {
          this.clearNotification(notifyData[0]._id);
        }
        this.setState({ notifications: Array.from(notifyData) }, () => {
          const { notifications } = this.state;
          const ids = [];
          notifications.map(notifcation => {
            if (notifcation.status === "unread") {
              ids.push(notifcation._id);
            }
          });

          if (ids.length > 0) {
            this.sendUnreadTicker(ids);
          } else {
            console.log("Unread Notifications not found");
          }
        });
      }

      const resActities = await getActivities();
      if (resActities.status === 200) {
        const activityData = await resActities.json();
        // console.log("activityData", activityData);
        this.setState({ activities: Array.from(activityData) });
      }
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const { seeMoreNotification, seeMoreActivities } = this.state;

    return (
      <div className="NotificationView">
        <Container>
          {!seeMoreActivities ? (
            <div className="Notification_container">
              <NotificationHeader
                onClick={this.onSeeMoreNotificationClick}
                headTitle={getPlaceholder(
                  "Notifications",
                  "Your notifications"
                )}
                subTitle={
                  this.state.notifications.length < 5
                    ? null
                    : seeMoreNotification
                      ? getPlaceholder("See less", "See less")
                      : getPlaceholder("See more", "See more")
                }
              />

              {this.state.notifications.length === 0 ? (
                <div className="notifications_noDataContainer">
                  <NoFoundMessageDashboard
                    noFountTitle="No data found"
                    noFountSubTitle="Here you will see your all notifications"
                  />
                </div>
              ) : (
                  <div className="Notification_body">
                    <div className="Notification_bodyTitle">
                      <p>Recent</p>
                    </div>
                    {this.state.notifications.map((val, idx) => {
                      if (!seeMoreNotification && idx > 4) {
                        return null;
                      }

                      const { text, link } = getNotificationByType(val);

                      return (
                        <Link to={`${link}`}>
                          <div
                            className={`Notification_bodyContainer ${
                              val.status !== "unread" ? "is-read" : ""
                              }`}>
                            <div className="NotiBody_containerLeft">
                              <Icjob pathcolor="#acaeb5" />
                            </div>
                            <div className="NotiBody_containerRight">
                              {/* <p className="titleHead"><span>Rahul</span> applied for <span>Ux UI design</span> job you posted on 20/05/18</p> */}
                              <p className="titleHead">
                                {text}
                              </p>
                              <p className="subTitle">
                                {format(val.time, "MMM D [at] hh:mm A")}
                              </p>
                            </div>
                          </div>
                        </Link>

                      );
                    })}
                  </div>
                )}
            </div>
          ) : null}

          {!seeMoreNotification ? (
            <div
              className="Notification_container"
              style={{ marginTop: "20px" }}>
              <NotificationHeader
                onClick={this.onSeeMoreActivitiesClick}
                headTitle={getPlaceholder("Activities", "Your activities")}
                subTitle={
                  this.state.activities.length < 5
                    ? null
                    : seeMoreActivities
                      ? getPlaceholder("See less", "See less")
                      : getPlaceholder("See more", "See more")
                }
              />

              <div>
                {this.state.activities.length === 0 ? (
                  <div className="notifications_noDataContainer">
                    <NoFoundMessageDashboard
                      noFountTitle="No data found"
                      noFountSubTitle="Here you will see your activities"
                    />
                  </div>
                ) : (
                    <div className="Notification_body All_activitis">
                      {this.state.activities.map((val, idx) => {
                        if (!seeMoreActivities && idx > 4) {
                          return null;
                        }

                        const { text, link } = getActivityByType(val);

                        return (
                          <Link to={`${link}`}>
                            <div className="Notification_bodyContainer">
                              <div className="NotiBody_containerLeft">
                                <Icjob pathcolor="#acaeb5" />
                              </div>
                              <div className="NotiBody_containerRight">
                                <p className="titleHead">
                                  {/* {text} */}
                                  {text}
                                </p>
                                {/* <p className="titleHead"><span>Rahul</span> applied for <span>Ux UI design</span> job you posted on 20/05/18</p> */}
                                <p className="subTitle">
                                  {format(val.time, "MMM D [at] hh:mm A")}
                                </p>
                              </div>
                            </div>
                          </Link>
                        );
                      })}
                    </div>
                  )}
              </div>
            </div>
          ) : null}
        </Container>

        {/* <React.Fragment>
            <Container>
              <Divider horizontal>Notifications</Divider>
              {this.state.notifications.map(val => {
                return <Message header={val.title} content={val.txt} />;
              })}
              <Divider horizontal>Activities</Divider>
              {this.state.activities.map(val => {
                return <Message header={val.title} content={val.txt} />;
              })}
            </Container>
          </React.Fragment> */}
      </div>
    );
  }
}

export default withProtectedRoute(NotificationView);
