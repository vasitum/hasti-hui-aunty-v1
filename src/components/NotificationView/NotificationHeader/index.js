import React, { Component } from "react";

import { Header, Button } from "semantic-ui-react";
import IcDownArrow from "../../../assets/svg/IcDownArrow";

export default class NotificationHeader extends Component {
  render() {
    const { headTitle, subTitle, onClick } = this.props;
    return (
      <div className="Notification_header">
        <div className="notiHeader_left">
          <Header as="h2">{headTitle}</Header>
        </div>
        <div className="notiHeader_right">
          <Button onClick={onClick} className="notiBtn">
            {subTitle}
            {/* <IcDownArrow width="9" pathcolor="#0b9ed0" /> */}
          </Button>
        </div>
      </div>
    );
  }
}
