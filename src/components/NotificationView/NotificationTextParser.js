import React from "react";
import { Link } from "react-router-dom";
import { format } from "date-fns";
import {
  NOTIFICATION_LIST,
  SCREEN_LIST,
  getObjectParams
} from "./utils/constants";

import { CAND_STAGES } from "../../constants/applicationStages";

import { USER } from "../../constants/api";

function getStatusChangeText(
  prevStatus,
  newStatus,
  jobName,
  jobId,
  applicationId
) {
  return {
    link: `/job/candidate/${jobId}/application/${applicationId}`,
    text: (
      <span>
        Your job application status for{" "}
        <Link to={`/job/candidate/${jobId}/application/${applicationId}`}>
          {jobName}
        </Link>{" "}
        has changed from {CAND_STAGES[prevStatus]} to {CAND_STAGES[newStatus]}
      </span>
    )
  };
}

function getJobExpireText(jobtitle, companyname) {
  return {
    link: "#",
    text: (
      <span>
        Sorry, the position for ${jobtitle} at ${companyname} has been closed by
        the recruiter
      </span>
    )
  };

  // `Sorry, the position for ${jobtitle} at ${companyname} has been closed by the recruiter`;
}

function getBirthdayText(userName) {
  return {
    link: "#",
    text: <span>Happy Birthday, ${userName} – Team Vasitum</span>
  };

  // `Happy Birthday, ${userName} – Team Vasitum`;
}

function getJobRecommendationText(count) {
  return {
    link: "#",
    text: <span>We found ${count} jobs matching your recent search</span>
  };
  // `We found ${count} jobs matching your recent search`;
}

function getProfileRecommendationText(count) {
  return {
    link: "#",
    text: <span>We found ${count} profiles matching your recent search</span>
  };
  // `We found ${count} profiles matching your recent search`;
}

function getLikedJobText(userName, jobName, userId, jobId) {
  return {
    link: `/view/user/${userId}`,
    text: (
      <span>
        <Link to={`/view/user/${userId}`}>{userName}</Link> liked your job
        posting for - <Link to={`/view/job/${jobId}`}>{jobName}</Link>
      </span>
    )
  };
}

function getLikedUserText(userName, userId) {
  return {
    link: `/view/user/${userId}`,
    text: (
      <span>
        <Link to={`/view/user/${userId}`}>{userName}</Link> liked your profile
      </span>
    )
  };
}

function getAppliedOnJobText(userName, jobName, date, applicationId, jobId) {
  return {
    link: `/job/${jobId}/application/${applicationId}`,
    text: (
      <span>
        <Link to={`/job/${jobId}/application/${applicationId}`}>
          {userName}
        </Link>{" "}
        has applied for the <Link to={`/job/view/${jobId}`}>{jobName}</Link> job
        you posted on {date}
      </span>
    )
  };
}

function getInterviewCreateNotification(
  jobName,
  compName,
  jobId,
  applicationId,
  date
) {
  return {
    link: `/job/candidate/${jobId}/application/${applicationId}`,
    text: (
      <span>
        Your interview for position -{" "}
        <Link to={`/job/candidate/${jobId}/application/${applicationId}`}>
          {jobName}
        </Link>{" "}
        with {compName} has been scheduled on {date}
      </span>
    )
  };
}

// to recr
function getInterviewRecschuldeFromCandidateText(
  jobName,
  userName,
  jobId,
  applicationId,
  date
) {
  return {
    link: `/job/candidate/${jobId}/application/${applicationId}`,
    text: (
      <span>
        <Link to={`/job/${jobId}/application/${applicationId}`}>
          {userName}
        </Link>{" "}
        has requested to reschedule the interview for{" "}
        <Link to={`/job/view/${jobId}`}>{jobName}</Link> to {date}
      </span>
    )
  };
}

function getRecrUpdatedUserInterview(compName, jobId, applicationId) {
  return {
    link: `/job/candidate/${jobId}/application/${applicationId}`,
    text: (
      <span>
        Interview at{" "}
        <Link to={`/job/candidate/${jobId}/application/${applicationId}`}>
          {compName}
        </Link>{" "}
        as requested has been rescheduled.
      </span>
    )
  };
}

function newUser() {
  return {
    link: "#",
    text: (
      <span>
        We're extremely happy to have you{" "}
        {window.localStorage.getItem(USER.NAME)}
      </span>
    )
  };
}

function getSavedUserNotification(userName, userId) {
  return {
    link: `/view/user/${userId}`,
    text: (
      <span>
        {" "}
        <Link to={`/view/user/${userId}`}>{userName}</Link> started following
        you.
      </span>
    )
  };
}

function getFollowNotification(userName, jobName, userId, jobId) {
  return {
    link: `/user/view/${userId}`,
    text: (
      <span>
        <Link to={`/user/view/${userId}`}>{userName}</Link> posted a new job{" "}
        <Link to={`/view/job/${jobId}`}>{jobName}</Link>
      </span>
    )
  };
}

function getCandidateInterviewNotification(
  userName,
  jobName,
  userId,
  jobId,
  applicationId,
  date
) {
  return {
    link: `/job/${jobId}/application/${applicationId}`,
    text: (
      <span>
        You're scheduled for an interview for{" "}
        <Link to={`/job/${jobId}/application/${applicationId}`}>{jobName}</Link>{" "}
        at {date}.
      </span>
    )
  };
}

function getRecruiterInterviewNotification(
  userName,
  jobName,
  userId,
  jobId,
  applicationId,
  date
) {
  return {
    link: `/job/${jobId}/application/${applicationId}`,
    text: (
      <span>
        <Link to={`/job/${jobId}/application/${applicationId}`}>
          {userName}
        </Link>{" "}
        is scheduled for an interview with you at {date}
      </span>
    )
  };
}

function getRecruiterInterviewAvailibility(
  userName,
  jobName,
  userId,
  jobId,
  applicationId,
  date
) {
  return (
    <span>
      <Link to={`/job/${jobId}/application/${applicationId}?screen=message`}>
        {userName}
      </Link>{" "}
      candidate available for {jobName} {date}
    </span>
  );
}

/**
 * Notification Interview Left
 * @param {*} notification
 */
function getIdsFromNotification(notification) {
  const retParams = getObjectParams(notification.screenInfo);

  switch (notification.screen) {
    case SCREEN_LIST.JOB_APPLICATION_DETAIL:
      return getAppliedOnJobText(
        notification.userName,
        notification.jobName,

        format(notification.date, "D, MMM YYYY"),
        retParams[NOTIFICATION_LIST.JOB_APPLICATION],
        retParams[NOTIFICATION_LIST.JOB]
      );
    case SCREEN_LIST.LIKE_JOB:
      return getLikedJobText(
        notification.userName,
        notification.jobName,
        retParams[NOTIFICATION_LIST.USER],
        retParams[NOTIFICATION_LIST.JOB]
      );
    case SCREEN_LIST.LIKE_USER:
      return getLikedUserText(
        notification.userName,
        retParams[NOTIFICATION_LIST.USER]
      );

    case SCREEN_LIST.CHANGE_APPLICATION_STATUS:
      return getStatusChangeText(
        notification.preStaus,
        notification.nextStaus,
        notification.jobName,
        retParams[NOTIFICATION_LIST.JOB],
        retParams[NOTIFICATION_LIST.JOB_APPLICATION]
      );

    // schedule interview
    case SCREEN_LIST.INTERVIEW_CREATE_NOTIFICATION:
      return getInterviewCreateNotification(
        notification.jobName,
        notification.userName,
        retParams[NOTIFICATION_LIST.JOB],
        retParams[NOTIFICATION_LIST.JOB_APPLICATION],
        format(notification.date, "D/MM/YYYY [at] hh:mm a")
      );

    // Proposed By candidae show: recr applications
    case SCREEN_LIST.INTERVIEW_RESCHEDULE_ACITIVIY:
      return getInterviewRecschuldeFromCandidateText(
        notification.jobName,
        notification.userName,
        retParams[NOTIFICATION_LIST.JOB],
        retParams[NOTIFICATION_LIST.JOB_APPLICATION],
        format(notification.date, "D/MM/YYYY [at] hh:mm a")
      );

    // Proposed By candidae show: cand applications
    case SCREEN_LIST.USER_INTERVIEW_UPDATE:
      return getRecrUpdatedUserInterview(
        notification.jobName,
        retParams[NOTIFICATION_LIST.JOB],
        retParams[NOTIFICATION_LIST.JOB_APPLICATION]
      );

    case SCREEN_LIST.NEW_USER:
      return newUser();

    case SCREEN_LIST.SAVE_USER:
      return getSavedUserNotification(
        notification.userName,
        retParams[NOTIFICATION_LIST.USER]
      );

    case SCREEN_LIST.VIEW_JOB_FOLLOW:
      return getFollowNotification(
        notification.userName,
        notification.jobName,
        retParams[NOTIFICATION_LIST.USER],
        retParams[NOTIFICATION_LIST.JOB]
      );

    case SCREEN_LIST.INTERVIEW_NOTIF:
      return getCandidateInterviewNotification(
        notification.userName,
        notification.jobName,
        retParams[NOTIFICATION_LIST.USER],
        retParams[NOTIFICATION_LIST.JOB],
        retParams[NOTIFICATION_LIST.JOB_APPLICATION],
        format(notification.date, "D, MMM YYYY")
      );

    case SCREEN_LIST.INTERVIEW_NOTIF_SPACE:
      return getCandidateInterviewNotification(
        notification.userName,
        notification.jobName,
        retParams[NOTIFICATION_LIST.USER],
        retParams[NOTIFICATION_LIST.JOB],
        retParams[NOTIFICATION_LIST.JOB_APPLICATION],
        format(notification.date, "D, MMM YYYY")
      );

    case SCREEN_LIST.INTERVIEW_NOTIF_RECR:
      return getRecruiterInterviewNotification(
        notification.userName,
        notification.jobName,
        retParams[NOTIFICATION_LIST.USER],
        retParams[NOTIFICATION_LIST.JOB],
        retParams[NOTIFICATION_LIST.JOB_APPLICATION],
        format(notification.date, "D, MMM YYYY")
      );

    case SCREEN_LIST.INTERVIEW_AVAILABILITY:
      return getRecruiterInterviewAvailibility(
        notification.userName,
        notification.jobName,
        retParams[NOTIFICATION_LIST.USER],
        retParams[NOTIFICATION_LIST.JOB],
        retParams[NOTIFICATION_LIST.JOB_APPLICATION],
        format(notification.date, "D, MMM YYYY")
      );

    default:
      return {
        link: "#",
        text: notification.screen
      };
      break;
  }
}

export function getNotificationByType(notification) {
  return getIdsFromNotification(notification);
}
