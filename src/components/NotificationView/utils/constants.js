export const NOTIFICATION_LIST = {
  USER: 1,
  JOB: 10,
  JOB_APPLICATION: 11,
  INTERVIEW: 12,
  VIDEO_CALL: 13,
  COMPANY: 14
};

export const SCREEN_LIST = {
  CHANGE_APPLICATION_STATUS: "Change application status",
  INTERVIEW_CREATE_NOTIFICATION: "User interview schedule",
  ACCEPT_INTERVIEW_CREATE_NOTIFICATION: "User interview schedule",
  USER_INTERVIEW_UPDATE: "User Interview Updated",
  JOB_APPLICATION_DETAIL: "JOB_APPLICATION_DETAIL",
  VIEW_DRAFT_JOB: "VIEW_DRAFT_JOB",
  VIEW_ACTIVE_JOB: "VIEW_ACTIVE_JOB",
  CHANGE_JOB_STATUS: "CHANGE_JOB_STATUS",
  INTERVIEW_RESCHEDULE_ACITIVIY: "User interview reSchedule",
  LIKE_USER: "Like User",
  LIKE_JOB: "Like Job",
  UN_LIKE_USER: "UN_Like User",
  UN_LIKE_JOB: "UN_Like Job",
  SAVE_JOB: "Save job",
  SAVE_USER: "Save user",
  EXPIRE_JOB: "expire job",
  INTERVIEW_TODAY: "interview today",
  INTERVIEW_15MIN: "interview 15 min before",
  NEW_USER: "NEW_USER",
  VIEW_JOB_FOLLOW: "VIEW_ACTIVE_JOB_FOLLOW",
  INTERVIEW_NOTIF_SPACE: "interview 2 hour  before for Candidate ", // Remember the space after
  INTERVIEW_NOTIF: "interview 2 hour  before for Candidate",
  INTERVIEW_NOTIF_RECR: "interview 2 hour  before for Recruiter",
  INTERVIEW_AVAILABILITY: "interview-availability"
};

export function getObjectParams(screenInfo) {
  if (!screenInfo || !Array.isArray(screenInfo)) {
    return null;
  }

  let ret = {};

  screenInfo.map(screen => {
    if (!screen) {
      return;
    }

    ret[`${screen.objectType}`] = screen.objectId[0];
  });

  return ret;
}
