import React from "react";

import { Grid, Responsive } from "semantic-ui-react";

import ChatSideBar from "./ChatSideBar";
import ChatThread from "./ChatThread";
import MediaSection from "./MediaSection";
import PeopleProfileInfo from "./PeopleProfileInfo";

import MessagingMobile from "./MessagingMobile";

import ChatInitiate from "./ChatInitiate";

import { Route } from "react-router-dom";

import resetMsgCounter from "../../api/messaging/resetMessageCounter";
import ReactGA from "react-ga";
import "./index.scss";
import InterView from "./InterView";

/**
 *
 * Messaging
 *
 */
class Messaging extends React.Component {
  state = {
    parsedTime: "",
    interviewScreen: false,
    selectScreenSecond: false,
    profileInfo: false,
    isClass: "8",
    isClassExt: "",
    noChatFound: false
  };

  constructor(props) {
    super(props);
    this.ev = new CustomEvent("MSG_NO");
  }

  onChangeScreen = () => {
    this.setState({
      selectScreenSecond: true
    });
  };

  onChangeSecondScreen = () => {
    this.setState({
      selectScreenSecond: false
    });
  };

  onChangeProfileInfo = () => {
    this.setState({
      profileInfo: true,
      isClass: "13",
      isClassExt: "pr-40"
    });
  };

  onChangeProfileInfoShow = () => {
    this.setState({
      profileInfo: false,
      isClass: "8",
      isClassExt: ""
    });
  };

  onShowInterviewScreen = (e, parsedTime) => {
    this.setState({
      parsedTime: parsedTime,
      profileInfo: true,
      isClass: "8",
      interviewScreen: true
    });
  };

  onHideInterviewScreen = e => {
    this.setState({
      parsedTime: "",
      profileInfo: false,
      isClass: "8",
      isClassExt: "",
      interviewScreen: false
    });
  };

  async componentDidMount() {
    ReactGA.pageview('/messaging');
    try {
      const res = await resetMsgCounter();
      if (res.status === 200) {
        // console.log("MsgCounter Updated!");
        document.dispatchEvent(this.ev);
      }
    } catch (error) {
      console.error(error);
    }
  }

  onNoChatFound = e => {
    this.setState({
      noChatFound: true
    });
  };

  render() {
    // const { isChat } = this.props;
    const { noChatFound } = this.state;
    return (
      <div>
        <Responsive maxWidth={1024}>
          {noChatFound ? (
            <div className="chatInitiate_box">
              <ChatInitiate />
            </div>
          ) : (
            <MessagingMobile />
          )}
        </Responsive>

        <Responsive minWidth={1025}>
          {noChatFound ? (
            <div className="chatInitiate_box">
              <ChatInitiate />
            </div>
          ) : (
            <div className className="mainMessaging">
              <div className="mainMessaging_innerContainer">
                <Grid style={{ marginLeft: "-1.3rem", marginRight: "-1.5rem" }}>
                  <Grid.Row className="messaging_row">
                    <Grid.Column computer={3} className="userSideBarColumn">
                      <div className="userSideBar">
                        <ChatSideBar onNoChatFound={this.onNoChatFound} />
                      </div>
                    </Grid.Column>
                    <Route
                      path={"/messaging/:id"}
                      render={props => (
                        <React.Fragment>
                          <Grid.Column
                            computer={this.state.isClass}
                            className={
                              "userProfileColumn " + this.state.isClassExt
                            }>
                            <div className="userProfile">
                              {!this.state.selectScreenSecond ? (
                                <ChatThread
                                  onChangeScreen={this.onChangeScreen}
                                  profileInfo={this.state.profileInfo}
                                  onChangeProfileInfoShow={
                                    this.onChangeProfileInfoShow
                                  }
                                  onShowInterviewScreen={
                                    this.onShowInterviewScreen
                                  }
                                />
                              ) : (
                                <MediaSection
                                  onChangeSecondScreen={
                                    this.onChangeSecondScreen
                                  }
                                />
                              )}
                            </div>
                          </Grid.Column>
                          {!this.state.profileInfo ? (
                            <Grid.Column
                              computer={5}
                              className="userInfoColumn">
                              <div className="userProfile">
                                <PeopleProfileInfo
                                  onChangeProfileInfo={this.onChangeProfileInfo}
                                />
                              </div>
                            </Grid.Column>
                          ) : null}
                          {this.state.interviewScreen ? (
                            <Grid.Column
                              computer={5}
                              className="userInfoColumn">
                              <div className="userProfile">
                                <InterView
                                  parsedTime={this.state.parsedTime}
                                  onCancelClick={this.onHideInterviewScreen}
                                  onInterviewScheduled={
                                    this.onHideInterviewScreen
                                  }
                                />
                              </div>
                            </Grid.Column>
                          ) : null}
                        </React.Fragment>
                      )}
                    />
                  </Grid.Row>
                </Grid>
              </div>
            </div>
          )}
        </Responsive>
      </div>
    );
  }
}

export default Messaging;
