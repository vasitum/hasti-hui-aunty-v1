import React from "react";

import { Grid, Dropdown, Image, Button, Container } from "semantic-ui-react";
import Chatimg from "../../../assets/img/chat.png";
import ActionBtn from "../../Buttons/ActionBtn";
import IcDownArrowIcon from "../../../assets/svg/IcDownArrow";

import { withRouter, Link } from "react-router-dom";

import "./index.scss";

class ChatInitiate extends React.Component {
  onChatInit = e => {
    this.props.history.push("/search/people?query=");
  };

  render() {
    return (
      <div className="ChatStart">
        {/* <div> */}
        <Image src={Chatimg} size="tiny" centered />

        <h3>No messages...yet!</h3>
        <p>
          Great things happen when you reach out and start new conversations.
        </p>

        <ActionBtn
          onClick={this.onChatInit}
          className="chatButton"
          actioaBtnText="Start a new conversation"
          btnIcon={<IcDownArrowIcon pathcolor="#99d9f9" />}
        />
        {/* </div> */}
      </div>
    );
  }
}

export default withRouter(ChatInitiate);
