import React from "react";
import { Image } from "semantic-ui-react";

import userProfile from "../../../assets/img/profile.png";

import UserLogo from "../../../assets/svg/IcUser";

import "./index.scss";

const ProfileImg = props => {
  const { src, ...resProps } = props;

  return (
    <div className="ProfileHeaderMessageThread">
      <Image src={src ? src : userProfile} {...resProps} />
    </div>
  );
};

export default ProfileImg;
