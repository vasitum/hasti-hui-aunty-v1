import React from "react";
import { Button, Input } from "semantic-ui-react";
import { Form } from "formsy-semantic-ui-react";
import { attachment } from "../../../api/messaging/attachment";

import { USER } from "../../../constants/api";

const userId = window.localStorage.getItem(USER.UID);
class FileUploade extends React.Component {
  constructor(props) {
    super(props);
    this.state = { fileUploade: "" };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  onFileUploade = e => {
    const file = e.target.files[0];

    // Reset Value
    e.target.value = "";

    if (!file) {
      return;
    }

    const extn = file.name.split(".").pop();
    this.setState({
      fileUploade: file
    });
  };

  handleSubmit(event) {
    // console.log(this.state.fileUploade);
    fetch("http://192.168.1.64:8082/" + "v2/imageUpload", {
      method: "POST",
      body: JSON.stringify({
        userId: userId,
        uploadfile: this.state.fileUploade
      }),
      headers: {
        "Content-Type": "application/json",
        "X-Maven-REST-AUTH-TOKEN": window.localStorage.getItem(USER.X_AUTH_ID)
      }
    })
      .then(res => res.json())
      .then(resp => {
        // console.log("fileUplode", resp);
      });
  }

  render() {
    return (
      <div>
        <Form onSubmit={this.handleSubmit}>
          <Input type="file" name="file" onChange={this.onFileUploade} />
          <Button>Submit</Button>
        </Form>
      </div>
    );
  }
}

export default FileUploade;
