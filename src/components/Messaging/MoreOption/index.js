import React from "react";

import { Grid, Dropdown } from "semantic-ui-react";
import IcMoreOptionIcon from "../../../assets/svg/IcMoreOptionIcon";

import "./index.scss";

const MoreOptionsTrigger = (
  <span>
    <IcMoreOptionIcon pathcolor="#acaeb5" />
  </span>
);

class MoreOption extends React.Component {
  getItems = isMobile => {
    return [
      // {
      //   text: "User Profile",
      //   key: "User Profile",
      //   onClick: () => this.props.onChangeProfileInfoShow()
      // },
      {
        text: "Media",
        key: "Media",
        onClick: () => this.props.onChangeScreen()
      },
      // {
      //   text: "Block",
      //   value: "Block"
      // },
      // {
      //   text: "Search",
      //   value: "Search",
      //   onClick: () => this.props.onChangeSearchField()
      // }
    ];
  };

  render() {
    const { ...resProps } = this.props;
    return (
      <div className="Main_Dropdown" {...resProps}>
        <Dropdown
          trigger={MoreOptionsTrigger}
          options={this.getItems()}
          icon={null}
          pointing="right"
        />
      </div>
    );
  }
}

export default MoreOption;
