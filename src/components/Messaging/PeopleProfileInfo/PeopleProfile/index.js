import React from "react";

import { Image, Header, List, Button } from "semantic-ui-react";

// import userProfile from "../../../../assets/img/people.png";
import SkillList from "../../../CardElements/SkillList";
import FlatDefaultBtn from "../../../Buttons/FlatDefaultBtn";
import UserLogo from "../../../../assets/svg/IcUser";
import { Link } from "react-router-dom";

import "./index.scss";

const processSkillsOptional = skills => {
  if (!skills) {
    return null;
  }

  const skillsName = skills.map(val => {
    return {
      name: val.name,
      exp: val.exp
    };
  });

  const skillComponent = skillsName.map((val, idx) => {
    if (!val) {
      return;
    }

    if (idx > 5) {
      return null;
    }

    return (
      <List.Item key={val.name}>
        {val.exp ? (
          <React.Fragment>
            {val.name} | <span> {val.exp} yrs </span>
          </React.Fragment>
        ) : (
          <React.Fragment>{val.name}</React.Fragment>
        )}
      </List.Item>
    );
  });

  return skillComponent;
};

const PeopleProfile = props => {
  let user = props.user || { loc: {} };

  return (
    <div className="PeopleProfile">
      <div className="peopleprofileBox">
        <div className="profileImage">
          {!user.ext || !user.ext.img ? (
            <React.Fragment>
              <UserLogo
                width="110"
                height="110"
                rectcolor="#f7f7fb"
                pathcolor="#c8c8c8"
              />
            </React.Fragment>
          ) : (
            <Image
              src={`https://s3-us-west-2.amazonaws.com/img-mwf/${
                user._id
              }/image.jpg`}
              centered
              style={{
                borderRadius: "50%"
              }}
            />
          )}
        </div>
        <div className="profileTitle">
          <Header as="h3" onClick={props.onViewFullProfileClick}>
            {user.fName} {user.lName}
          </Header>
          {/* <p className="titleSub">Java developer</p> */}
          <p className="locationTitle">{user.title}</p>
        </div>
        <div className="peopleSkills filterSkill">
          {/* <SkillList skills={user.skills} /> */}

          <List bulleted horizontal className="">
            {processSkillsOptional(user.skills)}
          </List>

          {/* {user.skills && user.skills.length > 0 ? (
            <div className="seeMoreBtn">
              <FlatDefaultBtn
                as={Link}
                to={`/user/public/${user._id}`}
                btntext="See more"
              />
            </div>
          ) : null} */}
        </div>
      </div>
    </div>
  );
};

export default PeopleProfile;
