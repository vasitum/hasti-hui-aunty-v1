import React from "react";

import PeopleProfile from "./PeopleProfile";

import IcHide from "../../../assets/svg/IcHide";

import FlatDefaultBtn from "../../Buttons/FlatDefaultBtn";

import Experience from "../../CardElements/Experience";
import InfoSection from "../../Sections/InfoSection";
import QuillText from "../../CardElements/QuillText";
import ActionBtn from "../../Buttons/ActionBtn";

import getUserById from "../../../api/user/getUserById";
import { USER } from "../../../constants/api";

import { withRouter } from "react-router-dom";
import P2PTracker from "../../P2PTracker";

import "./index.scss";

class PeopleProfileInfo extends React.Component {
  state = {
    // isLoading: false,
    profileJson: {}
  };

  constructor(props) {
    super(props);
    this.userId = null;
  }

  async componentDidMount() {
    // const { match } = this.props;
    this.userId = this.props.match.params.id;

    if (!this.userId) {
      // TODO: Handle case of /messages
      console.log("USER__NOT_FOUDN");
    }

    try {
      const res = await getUserById(this.userId);
      const resJson = await res.json();
      // console.log("checked", resJson);

      this.setState({
        profileJson: resJson
        // isLoading: false
      });
    } catch (error) {
      console.error(error);
    }
  }

  async componentDidUpdate() {
    if (this.userId === this.props.match.params.id) {
      return;
    }
    this.userId = this.props.match.params.id;

    if (!this.userId) {
      // TODO: Handle case of /messages
      console.log("USER__NOT_FOUDN");
    }

    try {
      const res = await getUserById(this.userId);
      const resJson = await res.json();
      // console.log("checked", resJson);

      this.setState({
        profileJson: resJson
        // isLoading: false
      });
    } catch (error) {
      console.error(error);
    }
  }

  onViewFullProfileClick = ev => {
    this.props.history.push(`/user/public/${this.userId}`);
  };

  render() {
    let user = this.state.profileJson || { loc: {} };
    return (
      <div className="PeopleProfileInfo">
        <div className="hideBtn">
          <FlatDefaultBtn
            btntext="Hide"
            btnicon={<IcHide pathcolor="#acaeb5" />}
            onClick={this.props.onChangeProfileInfo}
            className="bgTranceparent"
          />
        </div>
        <div className="peopleProfileInfoBox">
          <PeopleProfile
            onViewFullProfileClick={this.onViewFullProfileClick}
            user={this.state.profileJson}
          />

          <div className="peopleAboutTitle">
            {/* <InfoSection headerSize="small" headerText="">
              <QuillText
                value={user.desc ? user.desc : ""}
                noReadMore
                readMoreLength={100}
                isMute
                readOnly
              />
            </InfoSection> */}
            {Object.prototype.hasOwnProperty.call(
              this.state.profileJson,
              "_id"
            ) ? (
              <P2PTracker
                cardType={"user"}
                reqId={window.localStorage.getItem(USER.UID)}
                userId={user._id}
              />
            ) : null}
          </div>

          {/* <div className="profileInfoExperience">
            <Experience title={user.title} />
          </div> */}

          {/* <div className="viewProfileBtn">
            <ActionBtn
              ={this.onViewFullProfileClick}
              actioaBtnText="View full profile"
            />
          </div> */}
        </div>
      </div>
    );
  }
}

export default withRouter(PeopleProfileInfo);
