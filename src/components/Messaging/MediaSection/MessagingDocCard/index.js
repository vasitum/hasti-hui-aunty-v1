import React, { PureComponent } from "react";
import isValidChatId from "../../../../api/messaging/isValidChatid";
import getAttachmentByType from "../../../../api/messaging/getAttachmentByType";
import genChatId from "../../../../utils/messaging/genChatId";

import { USER } from "../../../../constants/api";

import { toast } from "react-toastify";
import { withRouter } from "react-router-dom";

import UploadDoc from "../UploadeDoc";

const ATTACHMENT_TYPES = {
  FILE: "file",
  IMG: "img"
};

class MessagingDocCard extends PureComponent {
  state = {
    attachments: []
  };

  async getValidChatId() {
    const { match } = this.props;
    const userId = window.localStorage.getItem(USER.UID);
    const recvId = match.params.id;
    const chatId1 = genChatId(userId, recvId);
    const chatId2 = genChatId(recvId, userId);

    try {
      const res = await isValidChatId(chatId1, chatId2);
      const chatId = await res.text();

      return chatId;
    } catch (error) {
      return error;
    }
  }

  async componentDidMount() {
    try {
      const chatId = await this.getValidChatId();
      const res = await getAttachmentByType(chatId, ATTACHMENT_TYPES.FILE);

      if (res.status === 404) {
        toast("No Attachments Found");
        return;
      }

      if (res.status === 200) {
        const json = await res.json();
        const imgArray = Array.from(json).filter(
          val => val.mediaType === ATTACHMENT_TYPES.FILE
        );

        // console.log("__FILE__ARRAY__", imgArray);
        this.setState({ attachments: imgArray });
      }
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    return this.state.attachments.map(val => (
      <UploadDoc
        isShowTime
        downloadDoc={val.ext}
        data={{
          name: val.fileName,
          type: val.mediaType,
          size: val.size,
          ext: val.ext
        }}
      />
    ));
  }
}

export default withRouter(MessagingDocCard);
