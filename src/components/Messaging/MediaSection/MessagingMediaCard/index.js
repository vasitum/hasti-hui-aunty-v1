import React from "react";
import { Card, Icon, Image, Checkbox, Grid, Modal } from "semantic-ui-react";
import MessagingDownload from "../MessagingDownload";
// import MessagingCheckBox from "../MessagingCheckBox";
import Slider from "../PreviewImageSlider";
// import profileImage from "./profile.png";
import publicprofile from "../../../../assets/img/publicProfile.png";

import getPlaceholder from "../../../Forms/FormFields/Placeholder";

import isValidChatId from "../../../../api/messaging/isValidChatid";
import getAttachmentByType from "../../../../api/messaging/getAttachmentByType";
import genChatId from "../../../../utils/messaging/genChatId";

import { USER } from "../../../../constants/api";

import { toast } from "react-toastify";
import { withRouter } from "react-router-dom";

import "./index.scss";

const ATTACHMENT_TYPES = {
  FILE: "file",
  IMG: "img"
};

const MediaCard = [
  {
    profileImage: <publicprofile />,
    footerTime: "Today at 05:30pm"
  },

  {
    profileImage: <publicprofile />,
    footerTime: "Today at 05:30pm"
  },

  {
    profileImage: <publicprofile />,
    footerTime: "Today at 05:30pm"
  },
  {
    profileImage: <publicprofile />,
    footerTime: "Today at 05:30pm"
  },
  {
    profileImage: <publicprofile />,
    footerTime: "Today at 05:30pm"
  },
  {
    profileImage: <publicprofile />,
    footerTime: "Today at 05:30pm"
  },
  {
    profileImage: <publicprofile />,
    footerTime: "Today at 05:30pm"
  },
  {
    profileImage: <publicprofile />,
    footerTime: "Today at 05:30pm"
  },
  {
    profileImage: <publicprofile />,
    footerTime: "Today at 05:30pm"
  },
  {
    profileImage: <publicprofile />,
    footerTime: "Today at 05:30pm"
  },
  {
    profileImage: <publicprofile />,
    footerTime: "Today at 05:30pm"
  }
  // {
  //   profileImage: <profileImage />,
  //   footerTime: "Today at 05:30pm"
  // }
];

class MessagingMediaCard extends React.Component {
  state = {
    isSliderOpen: false,
    attachments: []
  };

  constructor(props) {
    super(props);

    // bind async function
    this.getValidChatId = this.getValidChatId.bind(this);
  }

  onSliderOpen = e => {
    this.setState({ isSliderOpen: true });
  };

  onSliderClose = e => {
    this.setState({ isSliderOpen: false });
  };

  async getValidChatId() {
    const { match } = this.props;
    const userId = window.localStorage.getItem(USER.UID);
    const recvId = match.params.id;
    const chatId1 = genChatId(userId, recvId);
    const chatId2 = genChatId(recvId, userId);

    try {
      const res = await isValidChatId(chatId1, chatId2);
      const chatId = await res.text();

      return chatId;
    } catch (error) {
      return error;
    }
  }

  async componentDidMount() {
    try {
      const chatId = await this.getValidChatId();
      const res = await getAttachmentByType(chatId, ATTACHMENT_TYPES.IMG);

      if (res.status === 404) {
        toast("No Attachments Found");
        return;
      }

      if (res.status === 200) {
        const json = await res.json();
        const imgArray = Array.from(json).filter(
          val => val.mediaType === ATTACHMENT_TYPES.IMG
        );

        this.setState({ attachments: imgArray });
      }
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    
    const { downloadDoc } = this.props;

    return (
      <div className="MessagingMediaCard">
        <Grid>
          <Grid.Row>
            {this.state.attachments.map((item, idx) => {
              return (
                <div
                  className="messagingMediaCard_column-4 messagingMediaCardSection"
                  key={idx}>
                  <Card className="mediaCard" fluid>
                    <Image src={item.ext} onClick={this.onSliderOpen} />
                    <div className="Card_content">
                      <div>
                        <MessagingDownload
                          src={item.ext}
                        />
                      </div>
                      <div>
                        <div className="Date_content">
                          <span className="date">
                            {getPlaceholder("Today", "Today at 05:30pm")}
                          </span>
                        </div>
                      </div>
                    </div>
                  </Card>
                </div>
              );
            })}
          </Grid.Row>
        </Grid>
        <Modal
          open={this.state.isSliderOpen}
          dimmer="inverted"
          size="fullscreen"
          className="mediaSlider_modal"
          onClose={this.onSliderClose}
          closeOnDimmerClick={true}
          closeIcon>
          <Slider images={this.state.attachments} />
        </Modal>
      </div>
    );
  }
}

export default withRouter(MessagingMediaCard);
