import React from "react";

import MediaSubHeader from "./MediaSubHeader";

import MediaHeader from "./MediaHeader";
import "./index.scss";

class MediaSection extends React.Component {
  render() {
    const { onChangeSecondScreen } = this.props;

    return (
      <div className="MediaSection">
        <MediaHeader onChangeSecondScreen={onChangeSecondScreen} />
      </div>
    );
  }
}

export default MediaSection;
