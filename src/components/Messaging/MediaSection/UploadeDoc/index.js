import React from "react";
import {
  Segment,
  Image,
  List,
  Grid,
  Button,
  Progress
} from "semantic-ui-react";
import DocImg from "../../../../assets/img/doc.png";

import MessagingDownload from "../MessagingDownload";
import IcCloseIcon from "../../../../assets/svg/IcCloseIcon";

import IcUser from "../../../../assets/svg/IcUser";
import IcDocument from "../../../../assets/svg/IcDocument";

import image from "../../../../assets/img/profile.png";

import "./index.scss";

const UploadeDoc = props => {
  const { data, downloadDoc, isShowTime, onRemoveClick, isDocShow } = props;
  return (
    <div className="UploadeDocMessaging">
      {/* <Progress size="small" percent={100} attached="bottom" indicating /> */}
      <div className="messagingMainDoc">
        <Grid>
          <Grid.Row>
            <Grid.Column width={12}>
              <div className="messagingMainDocDiv">
                <div className="messagingImg">
                  {!isDocShow ? (
                    <IcDocument pathcolor="#acaeb5" height="17" />
                  ) : (
                    <Image src={image} />
                  )}
                </div>
                <div className="messagingTxt">
                  <div className="messagingTxt_truncate">
                    <p>{data.name}</p>
                  </div>
                  <div className="mobile hidden messagingList">
                    {/* <div>{data.type}</div>
                    <div>{data.size} kb</div> */}
                    <List bulleted horizontal>
                      <List.Item className="messagingListItem">
                        {data.type}
                      </List.Item>
                      <List.Item className="messagingListItem">XL</List.Item>
                      <List.Item className="messagingListItem">
                        {data.size}
                        kb
                      </List.Item>
                    </List>
                    {downloadDoc ? (
                      <div className="messagingUploade_time">
                        <p>Today, 05:30pm</p>
                      </div>
                    ) : (
                      ""
                    )}
                  </div>
                </div>
              </div>
            </Grid.Column>
            <Grid.Column width={4} className="messagingDocTime_close">
              {isShowTime ? (
                <MessagingDownload src={downloadDoc} />
              ) : (
                <div className="docRemoveBtn">
                  <Button onClick={onRemoveClick}>
                    <IcCloseIcon pathcolor="#acaeb5" width="10" height="10" />
                  </Button>
                </div>
              )}
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    </div>
  );
};

export default UploadeDoc;
