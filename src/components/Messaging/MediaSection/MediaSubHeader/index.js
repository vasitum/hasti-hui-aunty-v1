import React from "react";
import { Tab, Menu } from "semantic-ui-react";

import IcDocument from "../../../../assets/svg/IcDocument";
import IcMediaGallery from "../../../../assets/svg/IcMediaGallery";

import MessagingMediaCard from "../MessagingMediaCard";

import MediaDocumentShare from "../MediaDocumentShare";
import mediaApi from "../../../../api/messaging/media";
import { USER } from "../../../../constants/api";

import MessagingDocCard from "../MessagingDocCard";

import MessagingDownload from "../MessagingDownload";

import "./index.scss";

const panes = [
  {
    menuItem: (
      <Menu.Item key="mediaGallery">
        <IcMediaGallery pathcolor="#acaeb5" />
      </Menu.Item>
    ),

    render: () => {
      return (
        <div className="mediaGallery">
          <Tab.Pane attached={false}>
            <MessagingMediaCard />
          </Tab.Pane>
        </div>
      );
    }
  },

  {
    menuItem: (
      <Menu.Item key="Document">
        <IcDocument pathcolor="#acaeb5" />
      </Menu.Item>
    ),

    render: () => {
      return (
        <div className="mediaGallery">
          <Tab.Pane attached={false}>
            <MessagingDocCard />
          </Tab.Pane>
        </div>
      );
    }
  }
];

class MediaSubHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mediaJson: {}
    };
  }

  async componentDidMount() {
    // const revuid = this.props.match.params.id;
    const sendid = window.localStorage.getItem(USER.UID);

    // const revuid = this.props.match.params.id;

    try {
      const res = await mediaApi(sendid);
      // console.log("check json", res);

      if (res.status === 404) {
        console.log("no data");
      }

      if (res.status === 200) {
        const data = await res.json();
      }

      // const resJson = await res.json();

      // console.log("media api", resJson);

      // this.setState({
      //   mediaJson: resJson
      // });
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    return (
      <div className="Main_Tabs">
        <Tab menu={{ secondary: true }} panes={panes} />
      </div>
    );
  }
}

export default MediaSubHeader;
