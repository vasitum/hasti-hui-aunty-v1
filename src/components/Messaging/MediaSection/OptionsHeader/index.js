import React from "react";
import { Button, Icon } from "semantic-ui-react";
import IcDownload from "../../../../assets/svg/IcDownload";
import ICShare from "../../../../assets/svg/IcShare";
import "./index.scss";

const OptionsHeader = () => (
  <div className="messagingButtons">
    <Button basic className="messagingAll">
      Select all
    </Button>
    <Button icon basic className="messagingIcon">
      <IcDownload pathcolor="#a1a1a1" />
    </Button>
    <Button icon basic className="messagingIcon">
      <ICShare pathcolor="#a1a1a1" />
    </Button>
    <Button icon basic className="messagingIcon">
      <IcDownload pathcolor="#a1a1a1" />
    </Button>
  </div>
);

export default OptionsHeader;
