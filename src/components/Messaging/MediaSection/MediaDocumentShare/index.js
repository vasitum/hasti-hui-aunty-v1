import React from "react";
import { Segment, Image, List } from "semantic-ui-react";
import DocImg from "../../../../assets/img/doc.png";

import MessagingDownload from "../MessagingDownload";

import "./index.scss";

const MediaDocument = () => (
  <div>
    <Segment className="messagingMainDoc">
      <MessagingDownload />
      <div className="messagingMainDocDiv">
        <div className="messagingImg">
          <Image src={DocImg} />
        </div>
        <div className="messagingTxt">
          <p>Final schedule for all the documents</p>
        </div>
      </div>
    </Segment>
    <div className="messagingList">
      <List bulleted horizontal link>
        <List.Item className="messagingListItem">10 pages</List.Item>
        <List.Item className="messagingListItem">XL</List.Item>
        <List.Item className="messagingListItem">24kb</List.Item>
      </List>
      <List horizontal link floated="right">
        <List.Item className="messagingListItem">Today, 05:30pm</List.Item>
      </List>
    </div>
  </div>
);

export default MediaDocument;
