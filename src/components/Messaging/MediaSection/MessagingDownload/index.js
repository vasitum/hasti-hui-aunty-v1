import React from "react";

import ICDownload from "../../../../assets/svg/IcDownloadSecond";
import { Button } from "semantic-ui-react";

import "./index.scss";

const MessagingDownload = props => {
  const { onChangeChekbox, ...resPropsa } = props;

  return (
    <Button
      as={"a"}
      href={props.src}
      className="MessagingDownload"
      {...resPropsa}>
      <ICDownload pathcolor="#acaeb5" onClick={onChangeChekbox} />
    </Button>
  );
};

export default MessagingDownload;
