import React, { Component } from "react";
import Slider from "react-slick";
import { Image } from "semantic-ui-react";

import IcSliderRightArrow from "../../../../assets/svg/IcSliderRightArrow";
import IcCloseIcon from "../../../../assets/svg/IcCloseIcon";
import "./index.scss";

function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div className={className} onClick={onClick}>
      <IcSliderRightArrow pathcolor="#acaeb5" />
    </div>
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div className={className} onClick={onClick}>
      <IcSliderRightArrow pathcolor="#acaeb5" />.
    </div>
  );
}

function closehandler() {}

const SliderImage = props => {
  const { images } = props;

  // console.log(images);

  const settings = {
    customPaging: function(i) {
      return (
        <a>
          <img src={images[i].ext} />
        </a>
      );
    },
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    // dots: true,
    // dotsClass: "slick-dots slick-thumb",
    // speed: 500,
    // slidesToShow: 1,
    // slidesToScroll: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />
  };

  return (
    <div>
      <div className="AtTop">
        <div className="MediapreviewSlider" style={{ maxWidth: 500 }}>
          <Slider {...settings}>
            {images.map(img => (
              <div>
                <img src={img.ext} />
              </div>
            ))}
          </Slider>
        </div>
      </div>
    </div>
  );
};

export default SliderImage;
