import React from "react";
import { Grid, Button } from "semantic-ui-react";
import MediaSubHeader from "../MediaSubHeader";
import MessagingMediaCard from "../MessagingMediaCard";
import MobileHeader from "../../../MobileComponents/MobileHeader";
import IcFooterArrowIcon from "../../../../assets/svg/IcFooterArrowIcon";
import OptionsHeader from "../OptionsHeader";
import "./index.scss";

class MediaHeader extends React.Component {
  ArrowHeader = (
    <Button className="" onClick={this.props.onChangeSecondScreen}>
      <IcFooterArrowIcon pathcolor="#6e768a" />
    </Button>
  );
  render() {
    const { ...resProps } = this.props;
    return (
      <div className="MediaHeader" {...resProps}>
        <MobileHeader
          headerLeftIcon={this.ArrowHeader}
          headerTitle="Attachments"
        />
        <MediaSubHeader />
      </div>
    );
  }
}

export default MediaHeader;
