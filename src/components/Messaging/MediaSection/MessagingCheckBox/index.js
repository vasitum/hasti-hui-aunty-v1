import React from "react";

import { Checkbox } from "semantic-ui-react";

import "./index.scss";

const MessagingCheckBox = props => {
  const { onChangeChekbox, ...resPropsa } = props;

  return (
    <div className="MessagingCheckBox" {...resPropsa}>
      <Checkbox onClick={onChangeChekbox} />
    </div>
  );
};

export default MessagingCheckBox;
