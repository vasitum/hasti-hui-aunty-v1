import React from "react";

import ScheduleInterview from "../ScheduleInterview";

class ScheduleInterviewUpdate extends React.Component {
  render() {
    return (
      <div className="ScheduleInterviewUpdate">
        <ScheduleInterview />
      </div>
    );
  }
}

export default ScheduleInterviewUpdate;
