import React from "react";

import { Header, Grid, Button } from "semantic-ui-react";

import MobileHeader from "../../MobileComponents/MobileHeader";
import IcCalendarCard from "../../../assets/svg/IcCalendarCard";
import ScheduleInterview from "./ScheduleInterview";

import IcCloseIcon from "../../../assets/svg/IcCloseIcon";

import InterviewSuggestedBox from "../../Dashboard/Common/InterViewPageSection/InterViewPageSectionDedail/InterviewSuggestedBox";

import "./index.scss";

class InterView extends React.Component {
  render() {
    return (
      <div className="ScheduleInterview">
        <div className="ScheduleInterview_header">
          <MobileHeader
            mobileHeaderLeftColumn="11"
            mobileHeaderRightColumn="4"
            headerLeftIcon={<IcCalendarCard pathcolor="#ceeefe" />}
            headerTitle="Schedule interview"
          >
            <Button 
              className="closeBtn_interview" 
              onClick={this.props.onCancelClick}
            >
              <IcCloseIcon pathcolor="#ceeefe"/>
            </Button>
          </MobileHeader>
        </div>
        <ScheduleInterview
          parsedTime={this.props.parsedTime}
          onCancelClick={this.props.onCancelClick}
          onInterviewScheduled={this.props.onInterviewScheduled}
        />
        {/* <InterviewSuggestedBox isSuggestedBox isShowLogo/> */}
      </div>
    );
  }
}

export default InterView;
