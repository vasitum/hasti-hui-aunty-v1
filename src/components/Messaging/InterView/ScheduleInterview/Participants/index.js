import React from "react";

import { Header } from "semantic-ui-react";

import InfoIconLabel from "../../../../utils/InfoIconLabel";
import InputFieldLabel from "../../../../Forms/FormFields/InputFieldLabel";

import SelectOptionField from "../../../../Forms/FormFields/SelectOptionField";
import InputField from "../../../../Forms/FormFields/InputField";

const Participants = props => {
  return (
    <div className="participants_body">
      <div className="field_labelTitle Application" style={{ marginBottom: "20px" }}>
        <InputFieldLabel
          label="Application"
          infoicon={<InfoIconLabel text="Mention in a few words" />}
        />
        <SelectOptionField
          optionsItem={props.data.applicationOptions}
          value={props.data.applicationId}
          name="applicationId"
        />
      </div>
      <div className="header_title">
        <Header as="h3">Participants</Header>
      </div>
      <div className="field_labelTitle">
       
        <div className="labelTitle_Right">
          <InputFieldLabel
            label="Interviewer"
            // infoicon={<InfoIconLabel text="Mention in a few words" />}
          />
          <InputField
            value={props.data.interviewerName}
            onChange={props.onInputChange}
            name="interviewerName"
          />
        </div>
      </div>

      <div className="field_labelTitle">
        
        <div className="labelTitle_Right">
          <InputFieldLabel
            label="Interviewee"
            // infoicon={<InfoIconLabel text="Mention in a few words" />}
          />
          <InputField
            value={props.data.candidateName}
            onChange={props.onInputChange}
            name="candidateName"
          />
        </div>
      </div>
    </div>
  );
};

export default Participants;
