import React from "react";

import { Header, Menu, Tab, Radio } from "semantic-ui-react";

import { Form } from "formsy-semantic-ui-react";

import InfoIconLabel from "../../../../utils/InfoIconLabel";
import InputFieldLabel from "../../../../Forms/FormFields/InputFieldLabel";

import RadioButton from "../../../../Forms/FormFields/RadioButton";

import Telephonic from "../InterviewTypeOpenCard/Telephonic";

import VideoCall from "../InterviewTypeOpenCard/VideoCall";

import InPerson from "../InterviewTypeOpenCard/InPerson";

import CheckboxField from "../../../../Forms/FormFields/CheckboxField";
import { INTERVIEW_PREFERENCE } from "../../../../../strings";
import ActionBtn from "../../../../Buttons/ActionBtn";
import FlatDefaultBtn from "../../../../Buttons/FlatDefaultBtn";

import InputField from "../../../../Forms/FormFields/InputField";

import InputTagField from "../../../../Forms/FormFields/InputTagField";

import PrivateNote from "../PrivateNote";

import SearchTagField from "../../../../Forms/FormFields/SearchTagField";

import { InterviewString } from "../../../../EventStrings/ScheduleInterview";
import { USER } from "../../../../../constants/api";
import ReactGA from "react-ga";

import LabelStar from "../../../../utils/LabelStar";
// import AddTagField from "../../../../Forms/FormFields/AddTagFields";
import "../index.scss";

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

const modeType = {
  telephone: "telephone",
  videoCall: "videoCall",
  inPerson: "inPerson"
};

class InterviewType extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      notifyOtherInterview: false,
      interviewTypeMode: {},
      options: [],
      currentValues: []
    };
  }

  onTagAdd = (e, { value }) => {
    if (!validateEmail(value)) {
      return;
    }

    this.setState({
      options: [{ text: value, value }, ...this.state.options]
    });
  };

  onTagChange = (e, { value }) => {
    this.setState({ currentValues: value }, () => {
      this.props.onInputChange(e, { value: value, name: "email" });
    });
  };

  handleNotifyChange = e => {
    this.setState({ notifyOtherInterview: !this.state.notifyOtherInterview });
  };

  typeModeChange = (e, { value }) => {
    const { onInputChange } = this.props;
    this.setState(
      state => {
        return {
          ...state,
          interviewTypeMode:
            value === state.interviewTypeMode ? "DEFAULT" : value
        };
      },
      () => {
        // props change interviewMode
        const interviewType = this.state.interviewTypeMode;
        onInputChange(
          {},
          {
            value: interviewType ? interviewType : "",
            name: "interviewMode"
          }
        );
      }
    );
  };

  componentDidMount() {
    const { interviewData, data, defaultInterview } = this.props;

    if (defaultInterview) {
      this.setState({
        interviewTypeMode: data.interviewMode
      });
      return;
    }

    if (!interviewData) {
      return;
    }

    this.setState({
      interviewTypeMode: interviewData.interviewMode,
      notifyOtherInterview:
        interviewData.notifyEmail && interviewData.notifyEmail.length > 0
          ? true
          : false
    });
  }

  componentDidUpdate(prevProps, prevState) {
    const { interviewData, data, defaultInterview } = this.props;

    if (prevProps.data.interviewMode !== this.props.data.interviewMode) {
      if (defaultInterview) {
        this.setState({
          interviewTypeMode: data.interviewMode
        });
        return;
      }
    }
  }

  render() {
    const { candidate, defaultInterview } = this.props;

    const UserId = window.localStorage.getItem(USER.UID);

    return (
      <div className="interview_type">
        {!candidate ? (
          <React.Fragment>
            <div className="header_title">
              <Header as="h3">{LabelStar("Interview type")}</Header>
            </div>
            <div className="checkbox_lableField">
              <CheckboxField
                checkboxLabel="Telephonic"
                value={modeType.telephone}
                checked={this.state.interviewTypeMode === modeType.telephone}
                onChange={this.typeModeChange}
              />

              <CheckboxField
                checkboxLabel="Video call"
                value={modeType.videoCall}
                checked={this.state.interviewTypeMode === modeType.videoCall}
                onChange={this.typeModeChange}
              />
              <CheckboxField
                checkboxLabel="In person"
                value={modeType.inPerson}
                checked={this.state.interviewTypeMode === modeType.inPerson}
                onChange={this.typeModeChange}
              />
            </div>

            {(typeMode => {
              switch (typeMode) {
                case modeType.telephone:
                  return (
                    <Telephonic
                      defaultInterview={defaultInterview}
                      onInputChange={this.props.onInputChange}
                      data={this.props.data}
                    />
                  );
                  break;
                case modeType.videoCall:
                  return (
                    <VideoCall
                      onInputChange={this.props.onInputChange}
                      data={this.props.data}
                    />
                  );
                  break;
                case modeType.inPerson:
                  return (
                    <InPerson
                      onInputChange={this.props.onInputChange}
                      data={this.props.data}
                      onLocChange={this.props.onLocChange}
                      onSelect={this.props.onLocSelect}
                    />
                  );
                  break;
                default:
                  return null;
              }
            })(this.state.interviewTypeMode)}
            <PrivateNote
              onInputChange={this.props.onInputChange}
              data={this.props.data}
            />
            <div className="notifyInterview_box">
              <div className="notifyOther_interview">
                <div className="field_labelTitle">
                  <div onClick={this.handleNotifyChange}>
                    <CheckboxField checked={this.state.notifyOtherInterview} />
                  </div>
                  <InputFieldLabel
                    label={
                      INTERVIEW_PREFERENCE.INTERVIEW_SCHEDUL_CARD.RADIO_BTN_4
                        .TITLE
                    }
                    infoicon={
                      <InfoIconLabel
                        text={
                          INTERVIEW_PREFERENCE.INTERVIEW_SCHEDUL_CARD
                            .RADIO_BTN_4.CARD_TITLE
                        }
                      />
                    }
                  />
                </div>
              </div>

              {this.state.notifyOtherInterview && (
                <span className="notifyInterView_field">
                  <SearchTagField
                    placeholder="Enter email id"
                    // search={false}
                    onTagChange={this.onTagChange}
                    onTagAdd={this.onTagAdd}
                    options={this.state.options}
                    values={this.state.currentValues}
                    name="notifyInterView"
                    icon={false}
                  />
                </span>
              )}
            </div>
          </React.Fragment>
        ) : null}
        {!defaultInterview ? (
          <div className="interview_scheduleFooterBtn">
            <ActionBtn 
              actioaBtnText="Schedule" 
              onClick={() => {
                ReactGA.event({
                  category: InterviewString.InterviewStringEvant.category,
                  action: InterviewString.InterviewStringEvant.action,
                  value: UserId,
                });
              }}

            />
            {/* <ActionBtn actioaBtnText="update existing" /> */}
            <FlatDefaultBtn
              btntext="Cancel"
              onClick={this.props.onCancelClick}
              className="bgTranceparent"
            />
          </div>
        ) : null}
      </div>
    );
  }
}

// const InterviewType = props => {
//   return (

//   );
// };

export default InterviewType;
