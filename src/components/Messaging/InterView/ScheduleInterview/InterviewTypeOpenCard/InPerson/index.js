import React from "react";
import { Grid } from "semantic-ui-react";
import InfoIconLabel from "../../../../../utils/InfoIconLabel";
import InputFieldLabel from "../../../../../Forms/FormFields/InputFieldLabel";
import InputField from "../../../../../Forms/FormFields/InputField";
import LocationInputField from "../../../../../Forms/FormFields/LocationInputField";
import "./index.scss";

const InPerson = props => {
  var { ...resProps } = props;
  console.log(props.data);
  return (
    <div className="InterviewTypeOpenCard InPerson" {...resProps}>
      <div className="field_labelTitle">
        <InputFieldLabel
          label="Interview location"
          // infoicon={<InfoIconLabel text="Mention in a few words" />}
        />
        {/**
         * for a lack of a better key using subMode as location
         */}
        {/* <InputField
          value={props.interviewSubMode}
          onChange={props.onInputChange}
          name="interviewSubMode"
          placeholder="Eg. A-3, IInd Floor, Sector 4, Noida"
        /> */}
        <LocationInputField
          // placeholder={getPlaceholder("Eg. A-3, IInd Floor, Sector 4, Noida")}
          placeholder="Eg. A-3, IInd Floor, Sector 4, Noida"
          value={props.data.interviewSubMode}
          required
          onSelect={address => {
            props.onInputChange(null, {
              value: address,
              name: "interviewSubMode"
            });
          }}
          onChange={address => {
            props.onInputChange(null, {
              value: address,
              name: "interviewSubMode"
            });
          }}
          // name="interviewSubMode"
          // onChange={props.onLocChange}
          // onChange={props.onInputChange}
        />
      </div>
      <div className="field_labelTitle">
        <InputFieldLabel
          label="Phone number"
          // infoicon={<InfoIconLabel text="Mention in a few words" />}
        />
        <Grid className="phoneNumber_interviewType">
          <Grid.Row>
            {/* <Grid.Column width={5} className="county_code">
              <InputField
                value={props.interviewContactInfo}
                onChange={props.onInputChange}
                name="interviewContactInfo"
                placeholder="Eg. +91"
              />
            </Grid.Column> */}
            <Grid.Column width={16} className="phone_number">
              <InputField
                value={props.data.interviewContactNumber}
                onChange={props.onInputChange}
                name="interviewContactNumber"
                placeholder="Eg. 000 000 0000"
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    </div>
  );
};

export default InPerson;
