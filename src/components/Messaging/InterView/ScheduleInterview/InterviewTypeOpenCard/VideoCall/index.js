import React from "react";

import InfoIconLabel from "../../../../../utils/InfoIconLabel";
import InputFieldLabel from "../../../../../Forms/FormFields/InputFieldLabel";
import InputField from "../../../../../Forms/FormFields/InputField";

import CheckboxField from "../../../../../Forms/FormFields/CheckboxField";

// import "./index.scss";

const modeType = {
  vasitum: "vasitum",
  skype: "skype",
  hangout: "hangout"
};

class VideoCall extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      notifyOtherInterview: false,
      interviewTypeMode: {}
    };
  }

  handleNotifyChange = e => {
    this.setState({ notifyOtherInterview: !this.state.notifyOtherInterview });
  };

  typeModeChange = (e, { value }) => {
    this.setState(
      state => {
        return {
          ...state,
          interviewTypeMode:
            value === state.interviewTypeMode ? "DEFAULT" : value
        };
      },
      () => {
        const typeMode = this.state.interviewTypeMode;
        this.props.onInputChange(
          {},
          {
            value: typeMode || "",
            name: "interviewSubMode"
          }
        );
      }
    );
  };

  render() {
    var { ...resProps } = this.props;
    return (
      <div className="InterviewTypeOpenCard VideoCall" {...resProps}>
        <div className="field_labelTitle">
          <div className="checkboxField">
            {/* <CheckboxField
              checkboxLabel="Vasitum"
              value={modeType.vasitum}
              checked={this.state.interviewTypeMode === modeType.vasitum}
              onChange={this.typeModeChange}
            /> */}
            <CheckboxField
              checkboxLabel="Skype"
              value={modeType.skype}
              checked={this.state.interviewTypeMode === modeType.skype}
              onChange={this.typeModeChange}
            />
            <CheckboxField
              checkboxLabel="Hangout"
              value={modeType.hangout}
              checked={this.state.interviewTypeMode === modeType.hangout}
              onChange={this.typeModeChange}
            />
          </div>
          {(typeMode => {
            switch (typeMode) {
              case modeType.vasitum:
                return (
                  <InputField
                    onChange={this.props.onInputChange}
                    value={this.props.data.interviewContactInfo}
                    name="interviewContactInfo"
                    placeholder="Enter vasitum id"
                  />
                );
                break;
              case modeType.skype:
                return (
                  <InputField
                    onChange={this.props.onInputChange}
                    value={this.props.data.interviewContactInfo}
                    name="interviewContactInfo"
                    placeholder="Mention your Skype id "
                  />
                );
                break;
              case modeType.hangout:
                return (
                  <InputField
                    onChange={this.props.onInputChange}
                    value={this.props.data.interviewContactInfo}
                    name="interviewContactInfo"
                    placeholder="Mention your Hangout id"
                  />
                );
                break;
              default:
                return null;
            }
          })(this.state.interviewTypeMode)}
        </div>
      </div>
    );
  }
}

export default VideoCall;
