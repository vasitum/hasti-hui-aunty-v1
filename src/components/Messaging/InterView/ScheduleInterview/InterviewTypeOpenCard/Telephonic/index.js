import React from "react";

import { Header } from "semantic-ui-react";

import InfoIconLabel from "../../../../../utils/InfoIconLabel";
import InputFieldLabel from "../../../../../Forms/FormFields/InputFieldLabel";
import InputField from "../../../../../Forms/FormFields/InputField";

import "./index.scss";
import { Grid } from "semantic-ui-react";

class Telephonic extends React.Component {
  render() {
    const { defaultInterview } = this.props;
    return (
      <div className="InterviewTypeOpenCard InterviewTelephonic telephonic">
        <Header as="h3">Interview call</Header>
        <div className="InterviewTelephonic_container">
          <div className="telephone_label">
            <p>Interviewer</p>
          </div>
          <div className="telephone_inputField">
            {/* <div className="country_feild">
              <InputField
                onChange={this.props.onInputChange}
                value={"+91"}
                name="interviewContactInfo"
                placeholder="Eg. +91"
              />
            </div> */}
            <div className="country_number">
              <InputField
                onChange={this.props.onInputChange}
                value={this.props.data.interviewContactNumber}
                name="interviewContactNumber"
                // placeholder="Eg. 000 000 0000"
              />
            </div>
          </div>
        </div>
        <div className="InterviewTelephonic_container guest_phoneNumer">
          {!defaultInterview ? (
            <React.Fragment>
              <div className="telephone_label">
                <p>Interviewee</p>
              </div>
              <div className="telephone_inputField">
                {/* <div className="country_feild">
              <InputField
                onChange={this.props.onInputChange}
                value={"+91"}
                name="interviewContactNumber"
                // placeholder="Eg. +91"
              />
            </div> */}
                <div className="country_number">
                  <InputField
                    onChange={this.props.onInputChange}
                    value={this.props.data.guestPhoneNumber}
                    name="guestPhoneNumber"
                    // placeholder="Eg. 000 000 0000"
                  />
                </div>
              </div>
            </React.Fragment>
          ) : null}
        </div>
        {/* <div className="field_labelTitle">
          <InputFieldLabel
            label="Interview call"
          
          />

          <Grid className="phoneNumber_interviewType">
            <Grid.Row>
              <Grid.Column width={5} className="county_code">
                <InputField
                  onChange={this.props.onInputChange}
                  value={this.props.interviewContactInfo}
                  name="interviewContactInfo"
                  placeholder="Eg. +91"
                />
              </Grid.Column>
              <Grid.Column width={11} className="phone_number">
                <InputField
                  onChange={this.props.onInputChange}
                  value={this.props.interviewContactNumber}
                  name="interviewContactNumber"
                  placeholder="Eg. 000 000 0000"
                />
              </Grid.Column>
            </Grid.Row>

            <Grid.Row>
              <Grid.Column width={5} className="county_code">
                <InputField
                  onChange={this.props.onInputChange}
                  value={this.props.interviewContactInfo}
                  name="interviewContactInfo"
                  placeholder="Eg. +91"
                />
              </Grid.Column>
              <Grid.Column width={11} className="phone_number">
                <InputField
                  onChange={this.props.onInputChange}
                  value={this.props.interviewContactNumber}
                  name="interviewContactNumber"
                  placeholder="Eg. 000 000 0000"
                />
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div> */}
      </div>
    );
  }
}

export default Telephonic;
