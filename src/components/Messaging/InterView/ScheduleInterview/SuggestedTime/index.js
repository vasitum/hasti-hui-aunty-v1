import React from "react";

import { Header, Grid } from "semantic-ui-react";
import "react-dates/initialize";
// import { SingleDatePicker } from "react-dates";

import InfoIconLabel from "../../../../utils/InfoIconLabel";
import InputFieldLabel from "../../../../Forms/FormFields/InputFieldLabel";
import InputField from "../../../../Forms/FormFields/InputField";

import LabelStar from "../../../../utils/LabelStar";

// import "react-dates/lib/css/_datepicker.css";
import FlatPickr from "react-flatpickr";

class SuggestedTime extends React.Component {
  render() {
    const __self = this;

    return (
      <div className="suggested_timeBody">
        <div className="header_title">
          <Header as="h3">Suggest time</Header>
        </div>
        <div className="field_labelTitle">
          <div className="labelTitle_Right">
            <InputFieldLabel
              label={LabelStar("Date")}
              infoicon={<InfoIconLabel text="Select date as per your availability" />}
            />

            {/* <SingleDatePicker
            onDateChange={data => console.log(data)}
            focused={true}
            onFocusChange={({ focused }) => console.log(focused)}
            numberOfMonths={1}
            block={true}
          /> */}
            <FlatPickr
              placeholder="Select interview date"
              options={{
                allowInput: false,
                minDate: new Date().toLocaleDateString(),
                disable: [
                  function(date) {
                    const datems = date.getTime();
                    return datems < +new Date();
                  }
                ]
              }}
              value={this.props.data.scheduleDate}
              onChange={this.props.onDateChange}
            />
          </div>
        </div>
        <div className="field_labelTitle">
          <div className="field_labelDate">
            <Grid>
              <Grid.Row>
                <Grid.Column computer={8} mobile={16}>
                  <div className="labelTitle_Right">
                    <InputFieldLabel
                      label={LabelStar("From")}
                      infoicon={<InfoIconLabel text="Pick a time slot as per your availability" />}
                    />
                    <FlatPickr
                      placeholder="Select interview end time"
                      options={{
                        enableTime: true,
                        noCalendar: true,
                        dateFormat: "H:i",
                        allowInput: false,
                        time_24hr: true
                      }}
                      value={this.props.data.scheduleTimeStart}
                      onChange={date =>
                        this.props.onTimeChange(date, "scheduleTimeStart")
                      }
                    />
                  </div>
                </Grid.Column>
                <Grid.Column computer={8} mobile={16}>
                  <div className="labelTitle_Right labelRight_endDate">
                    <InputFieldLabel
                      label="To"
                      // infoicon={<InfoIconLabel text="Mention in a few words" />}
                    />
                    {/* <InputField value="12:00 PM" name="scheduleTimeEnd" /> */}
                    <FlatPickr
                      placeholder="Select interview end time"
                      options={{
                        enableTime: true,
                        noCalendar: true,
                        dateFormat: "H:i",
                        allowInput: false,
                        time_24hr: true
                      }}
                      value={this.props.data.scheduleTimeEnd}
                      onChange={date =>
                        this.props.onTimeChange(date, "scheduleTimeEnd")
                      }
                    />
                  </div>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </div>
        </div>
      </div>
    );
  }
}

export default SuggestedTime;
