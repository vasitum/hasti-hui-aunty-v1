import React from "react";

import { Header, Grid } from "semantic-ui-react";
import { Form } from "formsy-semantic-ui-react";
import { toast } from "react-toastify";
import getAllApplications from "../../../../api/messaging/getAllApplications";
import { withRouter } from "react-router-dom";
import { USER } from "../../../../constants/api";
import createInterview from "../../../../api/jobs/createInterview";
import updateInterview from "../../../../api/jobs/updateInterview";

import Participants from "./Participants";
import SuggestedTime from "./SuggestedTime";
import InterviewType from "./InterviewType";

import { format, addMinutes, parse } from "date-fns";

import "flatpickr/dist/flatpickr.css";
import "./index.scss";

function getRecruitersApplication(data, id) {
  if (!Object.prototype.hasOwnProperty.call(data, id)) {
    return null;
  }

  if (!data[id] || data[id].length <= 0) {
    return null;
  }

  return data[id];
}

function compileDateandTime(date, time) {
  const parsedDate = new Date(date);

  if (!time) {
    // console.log("Time Not Found");
    return;
  }

  let parsedTime;
  if (typeof time === "string") {
    parsedTime = {
      getHours: function() {
        return time.split(":")[0];
      },
      getMinutes: function() {
        return time.split(":")[1];
      }
    };
  } else {
    parsedTime = new Date(time);
  }

  return new Date(
    parsedDate.getFullYear(),
    parsedDate.getMonth(),
    parsedDate.getDate(),
    parsedTime.getHours(),
    parsedTime.getMinutes()
  ).getTime();
}

function validateTypeFields(mode, state) {
  if (mode === "telephone") {
    if (!state.interviewContactNumber) {
      return false;
    }

    return true;
  } else if (mode === "videoCall") {
    if (!state.interviewContactInfo) {
      return false;
    }

    return true;
  } else {
    // if (
    //   !state.interviewContactInfo ||
    //   !state.interviewContactNumber ||
    //   !state.interviewSubMode
    // ) {
    //   return false;
    // }

    return true;
  }
}

function validateStartandEndTime(startTime, endTime) {
  if (!startTime || !endTime) {
    return false;
  }

  // console.log(`Start Time MS: ${startTime}, End time MS: ${endTime}`);

  let startTimeParsed;
  let endTimeParsed;

  if (!(startTime instanceof Date) && !Number(startTime)) {
    const [hrs, min] = startTime.split(":");
    let mDate = new Date();
    mDate.setHours(hrs);
    mDate.setMinutes(min);
    startTimeParsed = parse(mDate);
  } else {
    startTimeParsed = parse(startTime);
  }

  if (!(endTime instanceof Date) && !Number(endTime)) {
    const [hrs, min] = endTime.split(":");
    let mDate = new Date();
    mDate.setHours(hrs);
    mDate.setMinutes(min);
    endTimeParsed = parse(mDate);
  } else {
    // parse start and end time
    endTimeParsed = parse(endTime);
  }

  // console.log(
  //   `Start Time MS: ${startTimeParsed.getTime()}, End time MS: ${endTimeParsed.getTime()}`
  // );

  // get time
  return startTimeParsed.getTime() < endTimeParsed.getTime();
}

function getInfoFields(mode, state) {
  if (mode === "telephone") {
    return {
      interviewContactInfo: state.interviewContactInfo,
      interviewContactNumber: state.interviewContactNumber,
      guestPhoneNumber: state.guestPhoneNumber,
      interviewSubMode: ""
    };
  } else if (mode === "videoCall") {
    return {
      interviewContactInfo: state.interviewContactInfo,
      interviewContactNumber: "",
      guestPhoneNumber: state.guestPhoneNumber,
      interviewSubMode: ""
    };
  } else {
    return {
      interviewContactInfo: state.interviewContactInfo,
      interviewContactNumber: state.interviewContactNumber,
      interviewSubMode: state.interviewSubMode,
      guestPhoneNumber: state.guestPhoneNumber
    };
  }
}

class ScheduleInterview extends React.Component {
  state = {
    candidateId: "",
    applicationId: "",
    applicationOptions: [],
    scheduleDate: "",
    scheduleTimeStart: "",
    scheduleTimeEnd: "",
    candidateName: "",
    candidatePhone: "",
    interviewerName: "",
    interviewerPhone: "",
    privateNote: "",
    interviewMode: "",
    interviewSubMode: "",
    jobApplicationId: "",
    interviewContactInfo: "",
    interviewContactNumber: "",
    guestPhoneNumber: "",
    email: []
  };

  demo = {
    candidateEmail: "string",
    candidateId: "string",
    candidateImageExt: "string",
    candidateName: "string",
    candidatePhone: "string",
    candidateProfileTitle: "string",
    comName: "string",
    interviewRound: 0,
    interviewStatus: "string",
    interviewerName: "string",
    interviewerPhone: "string",
    privateNote: "string",
    jobId: "string",
    recruiterId: "string",
    scheduledDate: 0
  };

  constructor(props) {
    super(props);
    this.applications = [];

    // bind the results
    this.onInputSubmit = this.onInputSubmit.bind(this);
  }

  parseTime = time => {
    return `${time.getHours()}:${time.getMinutes()}`;
  };

  async componentDidMount() {
    const { notConnected, applicationData, interviewData } = this.props;
    let applications;
    const userName = window.localStorage.getItem(USER.NAME);
    const userId = window.localStorage.getItem(USER.UID);
    const phoneNumber = window.localStorage.getItem(USER.PHONE);
    // console.log("check number", phoneNumber);
    try {
      if (!notConnected) {
        const recvId = this.props.match.params.id;

        const res = await getAllApplications(userId, recvId);
        if (res.status === 404) {
          // handle 404 cases
          console.log("Application not found");
          return;
        }

        const data = await res.json();
        // console.log("data check user", data);
        applications = getRecruitersApplication(data, userId);
      } else {
        applications = [applicationData];
      }

      if (!applications) {
        toast("Unable to find any applications for you!");
        return;
      }

      const applicationOptions = applications.map(data => {
        return {
          value: data._id,
          key: data._id,
          text: data.jobName
        };
      });

      if (applicationOptions.length === 0) {
        console.log("Applications not found");
        return;
      }

      this.applications = applications;

      let dateParsed;
      if (this.props.parsedTime) {
        dateParsed = format(Number(this.props.parsedTime), "HH:mm");
      }

      if (!interviewData) {
        this.setState({
          applicationOptions: applicationOptions,
          applicationId: applicationOptions[0].value,
          jobApplicationId: applicationOptions[0].value,
          candidateName: applications[0].candidateName,
          candidatePhone: applications[0].candidatePhone,
          guestPhoneNumber: applications[0].candidatePhone,
          candidateId: applications[0].candidateId,
          interviewerName: userName,
          interviewContactNumber: phoneNumber,
          scheduleDate: dateParsed ? Number(this.props.parsedTime) : "",
          scheduleTimeStart: dateParsed ? dateParsed : "",
          scheduleTimeEnd: dateParsed
            ? format(addMinutes(Number(this.props.parsedTime), 30), "HH:mm")
            : ""
        });
      } else {
        // console.log("Interview Data is", interviewData);
        // scheduledDate,
        // closedDate

        const selectedDate = Number(interviewData.scheduledDate);
        const startTime = format(Number(interviewData.scheduledDate), "HH:mm");
        const endTime = format(Number(interviewData.closedDate), "HH:mm");
        const contactData = getInfoFields(
          interviewData.interviewMode,
          interviewData
        );

        // console.log("Contact Data", contactData);

        this.setState({
          applicationOptions: applicationOptions,
          applicationId: applicationOptions[0].value,
          jobApplicationId: applicationOptions[0].value,
          candidateName: applications[0].candidateName,
          candidatePhone: applications[0].candidatePhone,
          candidateId: applications[0].candidateId,
          interviewerName: userName,
          interviewContactNumber: phoneNumber,
          scheduleDate: selectedDate,
          scheduleTimeStart: startTime,

          scheduleTimeEnd: endTime,
          privateNote: interviewData.privateNote,
          interviewMode: interviewData.interviewMode,
          interviewSubMode: contactData.interviewSubMode,
          interviewContactInfo: contactData.interviewContactInfo,
          guestPhoneNumber: interviewData.guestPhoneNumber
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  onInputChange = (e, { value, name }) => {
    this.setState({
      [name]: value
    });
  };

  onDateChange = date => this.setState({ scheduleDate: date });
  onTimeChange = (date, name) => {
    if (!date) {
      return;
    }

    const dateParsed = new Date(date);
    // console.log(`Selected Time is ${dateParsed.getTime()}`);

    this.setState({
      [name]: dateParsed.getTime()
    });
  };

  async onInputSubmit(e) {
    if (e.preventDefault) {
      e.preventDefault();
    }

    const { interviewData, applicationData, candidate } = this.props;

    const finalApplicationId = this.state.jobApplicationId;
    const finalApplication = this.applications.filter(val => {
      return val._id === finalApplicationId;
    });

    console.log("__CONSOLE__", finalApplication);

    if (
      !finalApplication ||
      !Array.isArray(finalApplication) ||
      !finalApplication.length
    ) {
      toast("UnexpectedError: Interview Processing");
      return;
    }

    if (!validateTypeFields(this.state.interviewMode, this.state)) {
      toast("Please fill all the required information");
      return;
    }

    if (
      !candidate &&
      !validateStartandEndTime(
        this.state.scheduleTimeStart,
        this.state.scheduleTimeEnd
      )
    ) {
      toast("Please check the timing you have selected");
      return;
    }

    try {
      if (!interviewData) {
        const res = await createInterview(
          {
            candidateId: this.state.candidateId,
            interviewerName: this.state.interviewerName,
            interviewerPhone: this.state.interviewerPhone,

            candidateName: this.state.candidateName,
            candidatePhone: this.state.candidatePhone,
            scheduledDate: compileDateandTime(
              this.state.scheduleDate,
              this.state.scheduleTimeStart
            ),
            closedDate: compileDateandTime(
              this.state.scheduleDate,
              this.state.scheduleTimeEnd
            ),
            jobApplicationId: this.state.jobApplicationId,
            privateNote: this.state.privateNote,
            interviewMode: this.state.interviewMode,
            interviewSubMode: this.state.interviewSubMode,
            interviewContactInfo: this.state.interviewContactInfo,
            interviewContactNumber: this.state.interviewContactNumber,
            guestPhoneNumber: this.state.guestPhoneNumber,
            interviewMode: this.state.interviewMode,
            notifyEmail: this.state.email.length === 0 ? null : this.state.email
          },
          finalApplication[0].pId,
          finalApplicationId
        );
      } else {
        let res;

        if (candidate) {
          console.log("check candidate", candidate);
          res = await updateInterview(
            {
              candidateId: this.state.candidateId,
              suggestTime: {
                id: window.localStorage.getItem(USER.UID),
                scheduledDate: compileDateandTime(
                  this.state.scheduleDate,
                  this.state.scheduleTimeStart
                )
              },
              closedDate: compileDateandTime(
                this.state.scheduleDate,
                this.state.scheduleTimeEnd
              ),
              jobApplicationId: this.state.jobApplicationId
            },
            finalApplication[0].pId,
            finalApplicationId,
            interviewData._id
          );
        } else {
          res = await updateInterview(
            {
              candidateId: this.state.candidateId,
              interviewerName: this.state.interviewerName,
              interviewerPhone: this.state.interviewerPhone,
              candidateName: this.state.candidateName,
              candidatePhone: this.state.candidatePhone,
              privateNote: this.state.privateNote,
              scheduledDate: compileDateandTime(
                this.state.scheduleDate,
                this.state.scheduleTimeStart
              ),
              closedDate: compileDateandTime(
                this.state.scheduleDate,
                this.state.scheduleTimeEnd
              ),
              suggestTime: null,
              jobApplicationId: this.state.jobApplicationId,
              interviewMode: this.state.interviewMode,
              interviewSubMode: this.state.interviewSubMode,
              interviewContactInfo: this.state.interviewContactInfo,
              interviewContactNumber: this.state.interviewContactNumber,
              guestPhoneNumber: this.state.guestPhoneNumber,
              interviewMode: this.state.interviewMode,
              notifyEmail:
                this.state.email.length === 0 ? null : this.state.email
            },
            finalApplication[0].pId,
            finalApplicationId,
            interviewData._id
          );
        }
      }

      if (this.props.onInterviewScheduled) {
        this.props.onInterviewScheduled();
      }

      if (applicationData) {
        window.location.reload();
      }
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const { DashBoardInterview, interviewData, candidate } = this.props;

    return (
      <div className={`ScheduleInterview_body ${DashBoardInterview}`}>
        <Form onSubmit={this.onInputSubmit} className="Interview_form">
          {!candidate ? (
            <Participants
              data={this.state}
              onInputChange={this.onInputChange}
            />
          ) : null}
          <SuggestedTime
            data={this.state}
            onDateChange={this.onDateChange}
            onTimeChange={this.onTimeChange}
          />
          <InterviewType
            data={this.state}
            onInputChange={this.onInputChange}
            onLocChange={this.onLocChange}
            onSelect={this.onLocSelect}
            onCancelClick={this.props.onCancelClick}
            interviewData={interviewData}
            candidate={candidate}
          />
        </Form>
      </div>
    );
  }
}

export default withRouter(ScheduleInterview);
