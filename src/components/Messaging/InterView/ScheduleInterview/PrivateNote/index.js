import React, { Component } from "react";

// import { TextAreaField } from "semantic-ui-react";
import CheckboxField from "../../../../Forms/FormFields/CheckboxField";
import InfoIconLabel from "../../../../utils/InfoIconLabel";
import QuillText from "../../../../CardElements/QuillText";
import InputFieldLabel from "../../../../Forms/FormFields/InputFieldLabel";
import TextAreaField from "../../../../Forms/FormFields/TextAreaField";
import { INTERVIEW_PREFERENCE } from "../../../../../strings";

export default class index extends Component {
  state = {
    privateNoteCheck: false
    // privateNote: []
  };

  handlePrivateNoteChange = e => {
    this.setState({ privateNoteCheck: !this.state.privateNoteCheck });
  };

  // onTagChange = (e, { value }) => {
  //   this.setState({ currentValues: value }, () => {
  //     this.props.onInputChange(e, { value: value, name: "email" });
  //   });
  // };
  render() {
    return (
      <div className="private_note">
        <div className="notifyInterview_box">
          <div className="notifyOther_interview">
            <div className="field_labelTitle">
              <div onClick={this.handlePrivateNoteChange}>
                <CheckboxField checked={this.state.privateNoteCheck} />
              </div>
              <InputFieldLabel
                label={
                  INTERVIEW_PREFERENCE.INTERVIEW_SCHEDUL_CARD.RADIO_BTN_3.TITLE
                }
                infoicon={
                  <InfoIconLabel
                    text={
                      INTERVIEW_PREFERENCE.INTERVIEW_SCHEDUL_CARD.RADIO_BTN_3
                        .CARD_TITLE
                    }
                  />
                }
              />
            </div>
          </div>
          {this.state.privateNoteCheck && (
            <span className="notifyInterView_field">
              {/* <QuillText
                placeholder="private note"
                isMessagebox
              /> */}
              <TextAreaField
                placeholder={"Enter something ..."}
                onChange={this.props.onInputChange}
                value={this.props.data.privateNote}
                name="privateNote"
                rows="2"
              />
            </span>
          )}
        </div>
      </div>
    );
  }
}
