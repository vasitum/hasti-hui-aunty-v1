import React from "react";
import allConversation from "../../../api/messaging/userAllConversation";

import startConversation from "../../../api/messaging/startConversation";

import { USER } from "../../../constants/api";

import getUserById from "../../../api/user/getUserById";

class UserAllConversationList extends React.Component {
  async componentDidMount() {
    const {} = this.props;

    try {
      const sendid = window.localStorage.getItem(USER.UID);
      const revuid = this.props.match.params.id;

      const res = await allConversation(sendid);
      // console.log("checked", res);

      // No conversation found
      if (res.status === 404 && revuid) {
        // call chat api
        const resChat = await startConversation(revuid, sendid);
        // console.log("chek value", resChat);
        if (resChat.status === 200) {
          console.log("200 sucsses");
        }
      } else {
        // show inital screen;
      }

      const data = await res.text();

      // console.log(data);
    } catch (error) {
      console.error(error);
    }
  }
  render() {
    return (
      <div>
        <h1>hello</h1>
      </div>
    );
  }
}

export default UserAllConversationList;
