import React from "react";
import allConversation from "../../../api/messaging/userAllConversationList";

import startConversation from "../../../api/messaging/startConversation";

import { USER } from "../../../constants/api";

import getUserById from "../../../api/user/getUserById";

import Messaging from "../index";

const sendid = window.localStorage.getItem(USER.UID);

class UserAllConversationList extends React.Component {
  state = {
    loading: false
  };

  constructor(props) {
    super(props);
  }

  async onIdFound(id) {}

  async onIdNotFound() {}

  async componentDidMount() {
    // hasId
    //  - true: component is directed from /messaging
    //  - false: component is directed from /messgaging/:id
    const { hasId } = this.props;

    if (hasId) {
      this.onIdFound();
    }

    this.onIdNotFound();
    // messgaing/:id
  }
  render() {
    return this.state.loading ? <div>loading ...</div> : <Messaging isChat />;
  }
}

export default UserAllConversationList;
