import React from "react";

import { Grid, Button, Modal, Dropdown, Radio } from "semantic-ui-react";
import { Form } from "formsy-semantic-ui-react";

import InputField from "../../../Forms/FormFields/InputField";
import ActionBtn from "../../../Buttons/ActionBtn";
import TemplateContainer from "../../Templates/TemplateContainer";
import TextAreaField from "../../../Forms/FormFields/TextAreaField";
import IcAttachment from "../../../../assets/svg/IcAttachment";

import IcMessagingTemplate from "../../../../assets/svg/IcMessagingTemplate";
// import ChatThreadMoreOption from "../ChatThreadMoreOption";
import IcMoreOptionIcon from "../../../../assets/svg/IcMoreOptionIcon";

import ChatThreadMoreOption from "../ChatThreadMoreOption";

import FileuploadBtn from "../../../Buttons/FileuploadBtn";

import QuillText from "../../../CardElements/QuillText";
import UploadeDoc from "../../MediaSection/UploadeDoc";

// import IcAttachment from "../../../../assets/svg/IcAttachment";

import "./index.scss";
// import { Button } from "glamorous";
// const ChatThreadMoreOption;

const MoreOptions = (
  <Button className="moreOptionIconBtn">
    <IcMoreOptionIcon pathcolor="#acaeb5" />
  </Button>
);

class ChatThreadMessageField extends React.Component {
  state = {};

  constructor(props) {
    super(props);
  }

  handleChange = (e, { value }) => this.setState({ value });

  items = [
    {
      text: (
        <div className="pressToSend_message sendMessage">
          <p className="sendTitle">Press Enter to Send</p>
          <p className="sendSubTitle">Pressing enter will send message</p>
        </div>
      ),
      value: "pressToSend",
      onClick: () => console.log(alert("Press To Send"))
    },

    {
      text: (
        <div className="clickToSend_message sendMessage">
          <p className="sendTitle">Click Send</p>
          <p className="sendSubTitle">Clicking send will send message</p>
        </div>
      ),
      value: "clickToSend",
      onClick: () => console.log(alert("Send Sub Title"))
    }
  ];

  handleKeyPress = event => {
    if (event.ctrlKey && event.which == 13) {
      this.props.onMessageSend();
    }
  };

  render() {
    const {
      chatId,
      recvId,
      sendId,
      attachment,
      currentMessage,
      onAttachmentClick,
      onAttachmentRemove,
      onMessageChange,
      onMessageSend,
      isTemplateModalOpen,
      onTemplateModalClose,
      onTemplateModalOpen,
      onTemplateSend
    } = this.props;

    return (
      <div className="ChatThreadMessageField">
        <Form>
          <Grid>
            <Grid.Row>
              <Grid.Column computer={16}>
                <div>
                  {attachment ? (
                    <UploadeDoc
                      data={attachment}
                      onRemoveClick={onAttachmentRemove}
                    />
                  ) : null}
                </div>
                <div className="chatThread_messageFieldBox">
                  <div className="chatThread_templateBtnBox">
                    <FileuploadBtn
                      as="a"
                      title="Attachment"
                      onChange={onAttachmentClick}
                      className="bgTranceparent"
                      floated="right"
                      btnIcon={<IcAttachment pathcolor="#acaeb5" />}
                    />
                    <Modal
                      open={isTemplateModalOpen}
                      dimmer="inverted"
                      onClose={onTemplateModalClose}
                      size="tiny"
                      className="TemplateContainer_modal"
                      trigger={
                        <Button
                          as="a"
                          title="Template"
                          onClick={onTemplateModalOpen}
                          className="templateBtn"
                          compact>
                          <IcMessagingTemplate pathcolor="#acaeb5" />
                        </Button>
                      }
                      closeIcon
                      closeOnDimmerClick={false}>
                      <TemplateContainer
                        chatId={chatId}
                        recvId={recvId}
                        sendId={sendId}
                        onSendClick={onTemplateSend}
                        onCancelClick={onTemplateModalClose}
                      />
                    </Modal>
                  </div>
                  <div className="chatThread_textAreaBtn">
                    {/* <div>
                      {attachment ? (
                        <UploadeDoc
                          data={attachment}
                          onRemoveClick={onAttachmentRemove}
                        />
                      ) : null}
                    </div> */}
                    <div className="chatBox_textAreaField">
                      {/* <QuillText
                        onChange={onMessageChange}
                        defaultValue={currentMessage}
                        value={currentMessage}
                        isMessagebox
                      /> */}
                      <TextAreaField
                        placeholder={"Enter something ..."}
                        onChange={onMessageChange}
                        value={currentMessage}
                        rows="1"
                        autoHeight={true}
                        onKeyPress={this.handleKeyPress}
                      />
                    </div>
                    <div className="messageSend_btn">
                      <ActionBtn
                        onClick={onMessageSend}
                        disabled={!currentMessage && !attachment}
                        actioaBtnText="Send"
                        className="sendBtn"
                        compact
                      />
                    </div>
                    {/* <div className="chatBox_btn">
                      <Grid>
                        <Grid.Row className="footerBtn">
                          <Grid.Column computer={8}>
                            <FileuploadBtn
                              onChange={onAttachmentClick}
                              className="bgTranceparent"
                              floated="right"
                              btnIcon={<IcAttachment pathcolor="#acaeb5" />}
                            />
                          </Grid.Column>
                          <Grid.Column computer={8} textAlign="right">
                            <ActionBtn
                              onClick={onMessageSend}
                              disabled={!currentMessage && !attachment}
                              actioaBtnText="Send"
                              className="sendBtn"
                              compact
                            />

                            
                            <Dropdown
                              className="moreOptionIconBtn"
                              trigger={MoreOptions}
                              options={this.items}
                              icon={null}
                              pointing="bottom right"
                            />
                          </Grid.Column>
                        </Grid.Row>
                      </Grid>
                    </div> */}
                  </div>
                </div>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Form>
      </div>
    );
  }
}

export default ChatThreadMessageField;
