import React from "react";
import { Grid, Button, Popup } from "semantic-ui-react";

import { Form } from "formsy-semantic-ui-react";
import MobileHeader from "../../../MobileComponents/MobileHeader";

import ProfileImg from "../../ProfileHeaderMessageThread";

import IcFooterArrowIcon from "../../../../assets/svg/IcFooterArrowIcon";

import IcProfileView from "../../../../assets/svg/IcProfileView";

import MoreOption from "../../MoreOption";

import InputField from "../../../Forms/FormFields/InputField";

import ChatTreadSearchHeader from "../ChatTreadSearchHeader";

import { withRouter } from "react-router-dom";

import getUserById from "../../../../api/user/getUserById";

import UserLogo from "../../../../assets/svg/IcUser";

import "./index.scss";

class ChatThreadPeopleHeader extends React.Component {
  state = {
    user: {},
    searchFieldShow: false
  };

  onChangeSearchField = () => {
    this.setState({
      searchFieldShow: true
    });
  };

  onChangeChatHeader = () => {
    this.setState({
      searchFieldShow: false
    });

    if (this.props.onSearchHeaderClose) {
      this.props.onSearchHeaderClose();
    }
  };

  async componentDidMount() {
    try {
      const res = await getUserById(this.props.match.params.id);
      if (res.status === 200) {
        const user = await res.json();
        this.setState({ user });
      }
    } catch (error) {
      console.log(error);
    }
  }

  async componentDidUpdate() {
    if (this.state.user._id === this.props.match.params.id) {
      return;
    }

    try {
      const res = await getUserById(this.props.match.params.id);
      if (res.status === 200) {
        const user = await res.json();
        this.setState({ user });
      }
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    const {
      onChangeScreen,
      onChangeProfileInfoShow,
      onSearchInputChange,
      onSearchSubmit,
      searchText,
      profileInfo,
      ...resProps
    } = this.props;
    const { user } = this.state;
    return (
      <div className="ChatThreadPeopleHeader" {...resProps}>
        {!this.state.searchFieldShow ? (
          <MobileHeader
            headerLeftIcon={
              !user.ext || !user.ext.img ? (
                <UserLogo
                  style={{
                    borderRadius: "50%"
                  }}
                  width="32"
                  height="32"
                  rectcolor="#f7f7fb"
                  pathcolor="#c8c8c8"
                />
              ) : (
                  <ProfileImg
                    src={`https://s3-us-west-2.amazonaws.com/img-mwf/${
                      user._id
                      }/image.jpg`}
                  />
                )
            }
            headerTitle={`${user.fName} ${user.lName}`}>
            {profileInfo ? (

              <Popup
                trigger={
                  <Button className="ProfileShowBtn" onClick={onChangeProfileInfoShow}>
                    <IcProfileView pathcolor="#acaeb5" />
                  </Button>
                }
                content="Show profile"
              />
            ) : null}
            <MoreOption
              onChangeScreen={onChangeScreen}
              onChangeSearchField={this.onChangeSearchField}
              onChangeProfileInfoShow={onChangeProfileInfoShow}
            />
          </MobileHeader>
        ) : (
            <ChatTreadSearchHeader
              isSearchFieldHeader
              headerLeftIcon={
                <Button
                  className="searchFieldBtn"
                  onClick={this.onChangeChatHeader}>
                  <IcFooterArrowIcon pathcolor="#acaeb5" />
                </Button>
              }
              headerTitle={
                <Form onSubmit={onSearchSubmit}>
                  <InputField
                    name="search"
                    value={searchText}
                    onChange={onSearchInputChange}
                    placeholder="Search"
                  />
                  <button type="submit" hidden />
                </Form>
              }
            />
          )}
      </div>
    );
  }
}

export default withRouter(ChatThreadPeopleHeader);
