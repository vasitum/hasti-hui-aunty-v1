import React, { Component } from "react";
import { List, Form, Radio } from "semantic-ui-react";
import "./index.scss";

export default class ChatThreadMoreOption extends Component {
  state = {};
  handleChange = (e, { value }) => this.setState({ value });

  render() {
    return (
      <List selection verticalAlign="middle" className="radioListMain">
        <List.Item>
          <Form>
            <Form.Field>
              <Radio
                label={
                  <label>
                    <p className="radiolabelMain">Press Enter to Send</p>
                    <p className="radiolabelSubmain">
                      Pressing enter will send message
                    </p>
                  </label>
                }
                name="radioGroup"
                value="this"
                checked={this.state.value === "this"}
                onChange={this.handleChange}
              />
            </Form.Field>
          </Form>
        </List.Item>
        <List.Item>
          <Form>
            <Form.Field>
              <Radio
                label={
                  <label>
                    <p className="radiolabelMain">Click Send</p>
                    <p className="radiolabelSubmain">
                      Clicking send will send message
                    </p>
                  </label>
                }
                name="radioGroup"
                value="that"
                checked={this.state.value === "that"}
                onChange={this.handleChange}
              />
            </Form.Field>
          </Form>
        </List.Item>
      </List>
    );
  }
}
