import React, { PureComponent } from "react";

import { Image } from "semantic-ui-react";

import ProfileHeaderMessageThread from "../../../ProfileHeaderMessageThread";
import QuillText from "../../../../CardElements/QuillText";

import ImageNew from "../../../../../assets/img/profile.png";

import UploadeDoc from "../../../MediaSection/UploadeDoc";
import FlatDefaultBtn from "../../../../Buttons/FlatDefaultBtn";
import IcPlus from "../../../../../assets/svg/IcPlus";
import fromNow from "../../../../../utils/env/fromNow";

import "./index.scss";

class UserMessage extends PureComponent {
  state = {
    isLoaded: false
  };

  onImgLoad = () => {
    this.setState({ isLoaded: true });
  };

  render() {
    const {
      isLast,
      message,
      attachment,
      time,
      interviewTime,
      onShowInterviewScreen,
      showInterviewTrigger,
      ...resProps
    } = this.props;

    if (attachment) {
      return (
        <div className="UserMessage" title={new Date(time)}>
          <div className="messageTitle">
            {attachment.mediaType == "img" ? (
              <React.Fragment>
                {!this.state.isLoaded ? (
                  <UploadeDoc
                    data={{
                      name: attachment.fileName,
                      type: attachment.mediaType,
                      size: attachment.size
                    }}
                    downloadDoc={attachment.ext}
                    isShowTime
                  />
                ) : null}
                <Image
                  src={attachment.ext}
                  onLoad={this.onImgLoad}
                  alt={attachment.fileName}
                />
              </React.Fragment>
            ) : (
              <UploadeDoc
                data={{
                  name: attachment.fileName,
                  type: attachment.mediaType,
                  size: attachment.size
                }}
                downloadDoc={attachment.ext}
                isShowTime
              />
            )}
            <QuillText placeholder="" readOnly value={message} isMessageText />
            {interviewTime && showInterviewTrigger ? (
              <FlatDefaultBtn
                onClick={e => onShowInterviewScreen(e, interviewTime)}
                btnicon={<IcPlus pathcolor="#c8c8c8" width="11" height="10" />}
                btntext="Schedule interview"
              />
            ) : null}
            {isLast && <p className="subTitle">Delivered {fromNow(time)}</p>}
          </div>
        </div>
      );
    }

    return (
      <div className="UserMessage" title={new Date(time)}>
        {/* <div className="messageProfile">
          <ProfileHeaderMessageThread />
        </div> */}
        <div className="messageTitle">
          {/* <p className="title">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit
          </p> */}
          {/* <Image src={ImageNew} /> */}
          {/* <UploadeDoc data="hello" /> */}
          <QuillText placeholder="" readOnly value={message} isMessageText />
          {interviewTime && showInterviewTrigger ? (
            <FlatDefaultBtn
              onClick={e => onShowInterviewScreen(e, interviewTime)}
              btnicon={<IcPlus pathcolor="#c8c8c8" width="11" height="10" />}
              btntext="Schedule interview"
            />
          ) : null}
          {isLast && <p className="subTitle">Delivered {fromNow(time)}</p>}
        </div>
      </div>
    );
  }
}

export default UserMessage;
