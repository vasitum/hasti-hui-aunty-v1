import React from "react";

import UserMessage from "./UserMessage";
import PeopleMessage from "./PeopleMessage";
import { InfiniteScroll } from "react-simple-infinite-scroll";

import "./index.scss";
// const loader = <div className="loader">Loading ...</div>;

class ChatThreadMessageSection extends React.Component {
  // constructor(props) {
  //   super(props);

  //   this.state = {
  //     tracks: [],
  //     hasMoreItems: true,
  //     nextHref: null
  //   };

  //   this.loadItems = this.loadItems.bind(this);
  // }

  state = {
    items: [],
    isLoading: true,
    cursor: 10
  };

  componentDidMount() {
    // do some paginated fetch
    this.loadMore();
  }

  loadMore = page => {
    this.setState({ isLoading: true, error: undefined });
    fetch(
      `https://randomuser.me/api/?page=${page}&results=${this.state.cursor}`
    )
      .then(res => res.json())
      .then(
        res => {
          this.setState(state => ({
            items: [...state.items, ...res.results],
            cursor: res.results,
            isLoading: false
          }));
        },
        error => {
          this.setState({ isLoading: false, error });
        }
      );
  };

  render() {
    return (
      <div className="ChatThreadMessageSection">
        <div>
          <InfiniteScroll
            throttle={100}
            threshold={300}
            isLoading={this.state.isLoading}
            hasMore={!!this.state.cursor}
            onLoadMore={this.loadMore}>
            {this.state.items.length > 0
              ? this.state.items.map(item => (
                  <PeopleMessage key={item.id} title={item.title} />
                ))
              : null}
          </InfiniteScroll>
          {this.state.isLoading && <div className="loader">Loading ...</div>}
        </div>
      </div>
    );
  }
}

export default ChatThreadMessageSection;
