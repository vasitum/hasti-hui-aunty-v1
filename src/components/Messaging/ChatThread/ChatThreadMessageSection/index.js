import React from "react";

import UserMessage from "./UserMessage";
import PeopleMessage from "./PeopleMessage";
import InfiniteScroll from "react-infinite-scroller";

import { withRouter } from "react-router-dom";

import fromNow from "../../../../utils/env/fromNow";

import ActionBtn from "../../../Buttons/ActionBtn";
import IcDownArrow from "../../../../assets/svg/IcDownArrow";

import isequal from "lodash.isequal";

import "./index.scss";
import { Button } from "semantic-ui-react";
const loader = <div className="loader">Loading ...</div>;

class ChatThreadMessageSection extends React.Component {
  constructor(props) {
    super(props);

    this.prevLength = 0;
  }

  shouldComponentUpdate(nextProps) {
    if (!isequal(this.props, nextProps)) {
      return true;
    }

    // console.log(`Render cancelled`);
    return false;
  }

  render() {
    const {
      messages,
      sender,
      getScrollRef,
      showMoreMessage,
      onShowMoreMessageClick
    } = this.props;

    return (
      <div className="ChatThreadMessageSection" ref={getScrollRef}>
        <div className="track">
          {messages.map((track, i) => {
            if (!sender) {
              // console.log("USERID_NOT_FOUND");
              return;
            }

            if (track.senId === sender) {
              return (
                <div key={i}>
                  <PeopleMessage
                    isLast={i === messages.length - 1}
                    interviewTime={track.intrviewTime}
                    time={track.sentTime}
                    message={track.text}
                    attachment={track.userMedia}
                    showInterviewTrigger={this.props.showInterviewTrigger}
                    onShowInterviewScreen={this.props.onShowInterviewScreen}
                  />
                </div>
              );
            }

            return (
              <div key={i}>
                <UserMessage
                  isLast={i === messages.length - 1}
                  interviewTime={track.intrviewTime}
                  time={track.sentTime}
                  message={track.text}
                  attachment={track.userMedia}
                  onShowInterviewScreen={this.props.onShowInterviewScreen}
                  showInterviewTrigger={this.props.showInterviewTrigger}
                />
              </div>
            );
          })}

          {showMoreMessage && (
            <div className="newMessage_btn">
              <ActionBtn
                onClick={onShowMoreMessageClick}
                compact
                actioaBtnText="See new Messages"
                btnIcon={<IcDownArrow pathcolor="#fff" width="10" />}
              />
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default withRouter(ChatThreadMessageSection);
