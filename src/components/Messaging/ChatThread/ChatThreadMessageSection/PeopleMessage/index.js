import React, { PureComponent } from "react";

import { Image } from "semantic-ui-react";
import ProfileHeaderMessageThread from "../../../ProfileHeaderMessageThread";
import QuillText from "../../../../CardElements/QuillText";

import ImageNew from "../../../../../assets/img/3.jpg";
import UploadeDoc from "../../../MediaSection/UploadeDoc";
import fromNow from "../../../../../utils/env/fromNow";
import FlatDefaultBtn from "../../../../Buttons/FlatDefaultBtn";
import IcPlus from "../../../../../assets/svg/IcPlus";

import "./index.scss";

class PeopleMessage extends PureComponent {
  state = {
    isLoaded: false
  };

  onImgLoad = () => {
    this.setState({ isLoaded: true });
  };

  render() {
    const {
      message,
      attachment,
      time,
      interviewTime,
      onShowInterviewScreen,
      showInterviewTrigger,
      isLast,
      ...resProps
    } = this.props;

    if (attachment) {
      return (
        <div
          className="thradPeople_Message"
          {...resProps}
          title={new Date(time)}>
          <div className="thradPeople_MessageRight">
            <div className="thradPeople_MessageRightbox">
              <div className="messageTitle">
                {attachment.mediaType == "img" ? (
                  <React.Fragment>
                    {!this.state.isLoaded ? (
                      <UploadeDoc
                        data={{
                          name: attachment.fileName,
                          type: attachment.mediaType,
                          size: attachment.size
                        }}
                        downloadDoc={attachment.ext}
                        isShowTime
                      />
                    ) : null}
                    <Image
                      src={attachment.ext}
                      onLoad={this.onImgLoad}
                      alt={attachment.fileName}
                    />
                  </React.Fragment>
                ) : (
                  <UploadeDoc
                    data={{
                      name: attachment.fileName,
                      type: attachment.mediaType,
                      size: attachment.size
                    }}
                    downloadDoc={attachment.ext}
                    isShowTime
                  />
                )}
                <QuillText
                  readOnly
                  placeholder=""
                  value={message}
                  isMessageText
                />
                {interviewTime && showInterviewTrigger ? (
                  <FlatDefaultBtn
                    onClick={e => onShowInterviewScreen(e, interviewTime)}
                    btnicon={
                      <IcPlus pathcolor="#c8c8c8" width="11" height="10" />
                    }
                    btntext="Schedule interview"
                  />
                ) : null}
                {isLast ? <p className="subTitle">Sent {fromNow(time)}</p> : null}
              </div>
            </div>
          </div>
        </div>
      );
    }

    return (
      <div className="thradPeople_Message" {...resProps} title={new Date(time)}>
        <div className="thradPeople_MessageRight">
          <div className="thradPeople_MessageRightbox">
            <div className="messageTitle">
              <QuillText
                placeholder=""
                readOnly
                value={message}
                isMessageText
              />
              {interviewTime && showInterviewTrigger ? (
                <FlatDefaultBtn
                  onClick={e => onShowInterviewScreen(e, interviewTime)}
                  btnicon={
                    <IcPlus pathcolor="#c8c8c8" width="11" height="10" />
                  }
                  btntext="Schedule interview"
                />
              ) : null}
              {isLast ? <p className="subTitle">Sent {fromNow(time)}</p> : null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

// const PeopleMessage = ({
//   message,
//   attachment,
//   time,
//   interviewTime,
//   onShowInterviewScreen,
//   showInterviewTrigger,
//   isLast,
//   ...resProps
// }) => {

// };

export default PeopleMessage;
