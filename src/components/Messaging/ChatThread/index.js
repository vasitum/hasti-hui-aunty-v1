import React from "react";

import ChatThreadPeopleHeader from "./ChatThreadPeopleHeader";
import ChatThreadMessageSection from "./ChatThreadMessageSection";
import ChatThreadMessageField from "./ChatThreadMessageField";

import ChatThreadMessagingContainer from "./ChatThreadMessagingContainer";

import searchMessage from "../../../api/messaging/searchMessage";

import "./index.scss";
import { toast } from "react-toastify";

class ChatThread extends React.Component {
  state = {
    searchText: "",
    foundMessages: []
  };

  constructor(props) {
    super(props);
    this.chatId = null;
    this.onSearchSubmit = this.onSearchSubmit.bind(this);
  }

  onInputChange = (e, { value }) => {
    this.setState({ searchText: value });
  };

  onSearchHeaderClose = () => {
    this.setState({
      searchText: "",
      foundMessages: []
    });
  };

  async onSearchSubmit(e) {
    // e.preventDefault();

    if (!this.chatId) {
      // console.log("Error: Unable to find chat id");
      return;
    }

    //TODO: Call Search API
    try {
      const res = await searchMessage(this.chatId, this.state.searchText);
      if (res.status === 404) {
        // console.log("Messages Not Found");
        toast("Unable to find message");
        return;
      }

      const data = await res.json();
      this.setState({
        foundMessages: data
      });
      // console.log(data);
    } catch (error) {
      toast("UnExpected Error while finding messages");
      console.error(error);
    }
  }

  onChatIdFound = id => {
    // console.log("Chat ID Found", id);
    this.chatId = id;
  };

  render() {
    const {
      onChangeScreen,
      onChangeProfileInfoShow,
      onShowInterviewScreen,
      profileInfo,
      ...resProps
    } = this.props;

    return (
      <div className="ChatThread" {...resProps}>
        <ChatThreadPeopleHeader
          onChangeScreen={onChangeScreen}
          profileInfo={profileInfo}
          onChangeProfileInfoShow={onChangeProfileInfoShow}
          onSearchInputChange={this.onInputChange}
          onSearchSubmit={this.onSearchSubmit}
          searchText={this.state.searchText}
          onSearchHeaderClose={this.onSearchHeaderClose}
        />
        <ChatThreadMessagingContainer
          foundMessages={this.state.foundMessages}
          onChatIdFound={this.onChatIdFound}
          onShowInterviewScreen={onShowInterviewScreen}
        />
      </div>
    );
  }
}

export default ChatThread;
