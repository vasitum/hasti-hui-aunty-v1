import React, { Children } from "react";
import "./index.scss";
import PropTypes from "prop-types";
import { Grid } from "semantic-ui-react";

const ChatTreadSearchHeader = props => {
  const { headerLeftIcon, headerTitle, ...resProps } = props;
  return (
    <div className="MobileHeader ChatTreadSearchHeader" {...resProps}>
      <Grid>
        <Grid.Row>
          <Grid.Column width={16}>
            <div className="MobileHeader_Left">
              {headerLeftIcon}

              <div className="SearchField">{headerTitle}</div>
            </div>
          </Grid.Column>
          {/* <Grid.Column width={8} textAlign="right">
            {children}
          </Grid.Column> */}
        </Grid.Row>
      </Grid>
    </div>
  );
};

export default ChatTreadSearchHeader;
