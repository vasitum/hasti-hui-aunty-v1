import React from "react";

import { Responsive } from "semantic-ui-react";
import ChatThreadMessageSection from "../ChatThreadMessageSection";
import ChatThreadMessageField from "../ChatThreadMessageField";

import { withRouter } from "react-router-dom";
import { USER } from "../../../../constants/api";

import startConversation from "../../../../api/messaging/startConversation";
import userConversationHistory from "../../../../api/messaging/userConversationHistory";
import getAllApplications from "../../../../api/messaging/getAllApplications";
import isValidChatId from "../../../../api/messaging/isValidChatid";
import unreadTicker from "../../../../api/messaging/unreadTicker";
import genChatId from "../../../../utils/messaging/genChatId";
import connectPollAPI from "../../../../api/messaging/connectPoll";

import { uploadImage } from "../../../../utils/aws";
import { toast } from "react-toastify";

import debounce from "../../../../utils/env/debounce";

import MessagingMobileChatField from "../../MessagingMobile/MessagingMobileChatField";

const ATTACHMENT_TYPES = {
  FILE: "file",
  IMG: "img"
};

function getFileExtn(filename) {
  if (!filename) {
    return null;
  }

  return filename.split(".").pop();
}

function getFileType(filename) {
  const extn = getFileExtn(filename);
  const IMG_EXTNS = ["jpg", "png", "tiff", "jpeg", "bmp", "gif"];

  if (!extn) {
    return extn;
  }

  if (IMG_EXTNS.indexOf(extn) === -1) {
    return ATTACHMENT_TYPES.FILE;
  }

  return ATTACHMENT_TYPES.IMG;
}

class CTMessagingContainer extends React.Component {
  constructor(props) {
    super(props);
    this.isFirstConversation = false;

    this.userId = null;
    this.recvId = null;
    this.chatId = null;
    this.chatInterval = null;
    this.scrollDivRef = null;
    this.oldMessages = null;

    // binding
    this.onMessageSend = this.onMessageSend.bind(this);
    this.onFileUploadClick = this.onFileUploadClick.bind(this);
    this.onFileUploadToAws = this.onFileUploadToAws.bind(this);
    this.getChatHistory = this.getChatHistory.bind(this);
    this.sendReadTicker = this.sendReadTicker.bind(this);
    this.connectAndPoll = this.connectAndPoll.bind(this);
    this.loadMoreChats = this.loadMoreChats.bind(this);
    this.onSidebarTrigger = new CustomEvent("vassidebar");

    this.abortController = null;
  }

  state = {
    currentMessage: "",
    attachment: null,
    messages: [],
    isUploading: false,
    isSendClicked: false,
    isTemplateModalOpen: false,
    isApplicationFound: false,
    scrollLoader: {
      error: false,
      isLoading: false,
      hasMore: true,
      currentPage: 1
    },
    showMoreMessage: false
  };

  /**
   * connect and actually poll the data
   */
  async connectAndPoll(chatId) {
    if (this.abortController) {
      this.abortController.abort();
    }

    this.abortController = new window.AbortController();
    const signal = this.abortController.signal;

    try {
      const res = await connectPollAPI(chatId, signal);
      // console.log(res);
      if (res.status === 200) {
        const data = await res.json();
        this.setState(
          {
            showMoreMessage: true,
            messages: [...this.state.messages, data]
          },
          () => {
            this.abortController = null;
            this.connectAndPoll(chatId);
          }
        );

        // return;
      }

      // this.abortController = null;
      // this.connectAndPoll();
    } catch (error) {
      console.error(error);
      this.abortController = null;
      this.connectAndPoll(chatId);
    }
  }

  async sendReadTicker(chatId) {
    try {
      const res = await unreadTicker(chatId);
      document.dispatchEvent(this.onSidebarTrigger);
    } catch (error) {
      console.error(error);
    }
  }

  async getChatHistory(chatid, isFirst) {
    try {
      const conversation = await userConversationHistory(chatid);
      // TODO: handle message based cases
      const data = await conversation.json();
      const dataRevered = Array.from(data).reverse();
      const length = this.state.messages.length;
      this.setState(
        {
          messages: dataRevered
        },
        () => {
          if (isFirst) {
            this.setScrollDivBottom();
          }
          if (dataRevered.length > length) {
            this.setScrollDivBottom();
            this.sendReadTicker(chatid);
          }
        }
      );
    } catch (error) {
      console.error(error);
    }
  }

  async loadMoreChats(chatId) {
    const { currentPage } = this.state.scrollLoader;
    this.setState(
      {
        scrollLoader: {
          ...this.state.scrollLoader,
          isLoading: true
        }
      },
      async () => {
        try {
          const res = await userConversationHistory(chatId, currentPage);
          if (res.status === 404) {
            // Handle no More Requests
            console.log("No More Requests");
            this.setState({
              error: false,
              isLoading: false,
              hasMore: false
            });
            return;
          }
          const data = await res.json();
          const dataFinal = Array.from(data).reverse();
          this.setState({
            messages: [...data, ...this.state.messages],
            scrollLoader: {
              ...this.state.scrollLoader,
              error: false,
              isLoading: false,
              hasMore: true,
              currentPage: this.state.scrollLoader.currentPage + 1
            }
          });
        } catch (error) {
          this.setState({
            error: error.message,
            isLoading: false
          });
        }
      }
    );
  }

  calcAndUpdate = (scrollDiv, chatId, SCROLL_OFFSET) => {
    const scrollPos = scrollDiv.clientHeight + scrollDiv.scrollTop;

    console.log("Scroll Div Client Height", scrollDiv.clientHeight);
    if (scrollPos > SCROLL_OFFSET) {
      console.log("Scroll Position is", scrollPos);
      // return on scroll position
      return;
    }

    console.log("Scroll Calleds", chatId);
    const { error, isLoading, hasMore } = this.state.scrollLoader;
    if (error || isLoading || !hasMore) return;

    this.loadMoreChats(chatId);
  };

  onSetUpScroll = debounce(chatId => {
    const scrollDiv =
      window.screen.availWidth > 1024
        ? this.scrollDivRef
        : document.scrollingElement;

    if (!scrollDiv) {
      console.log("Scroll Div Not Found");
      return;
    }

    if (window.screen.availWidth > 1024) {
      scrollDiv.onscroll = () => this.calcAndUpdate(scrollDiv, chatId, 500);
    } else {
      window.onscroll = () => this.calcAndUpdate(scrollDiv, chatId, 600);
    }

    // scrollDiv.onscroll
  }, 500);

  /**
   * @callback
   */
  onLinkUploaded = () => {
    if (!this.state.isSendClicked) {
      return;
    }

    this.onMessageSend();
  };

  componentWillUnmount() {
    if (this.abortController) {
      this.abortController.abort();
    }

    /* cleanup */
    if (window.screen.availWidth > 1024) {
      this.scrollDivRef.onscroll = null;
    } else {
      window.onscroll = null;
    }
  }

  async componentDidMount() {
    const { noRouting, userId } = this.props;

    this.userId = window.localStorage.getItem(USER.UID);
    if (noRouting) {
      this.recvId = userId;
    } else {
      this.recvId = this.props.match.params.id;
    }

    const chatId1 = genChatId(this.recvId, this.userId);
    const chatId2 = genChatId(this.userId, this.recvId);

    try {
      const chatId = await isValidChatId(chatId1, chatId2);
      let finalChatId;

      if (chatId.status === 404) {
        this.isFirstConversation = true;
        finalChatId = chatId1;
      } else {
        finalChatId = await chatId.text();
        this.getChatHistory(finalChatId, true);
      }

      // set the chatid
      this.chatId = finalChatId;
      if (this.props.onChatIdFound) {
        console.log("Chat ID Found Base", finalChatId);
        this.props.onChatIdFound(finalChatId);
      }

      // setUp InfiniteScroll For it!
      this.onSetUpScroll(finalChatId);

      // loop it!
      this.chatInterval = setInterval(
        () => this.getChatHistory(finalChatId),
        3000
      );
      // console.log("Component Mount Callled");

      // find applications
      const applicationres = await getAllApplications(this.recvId, this.userId);
      if (applicationres.status === 404) {
        console.log("No Applications");
        this.setState({ isApplicationFound: false });
        return;
      }

      const applicationData = await applicationres.json();
      const recvApplications = applicationData[this.userId];
      if (recvApplications.length > 0) {
        this.setState({ isApplicationFound: true });
        console.log("Applied Found", recvApplications);
      } else {
        console.log("Applied Not Found", recvApplications);
        this.setState({ isApplicationFound: false });
      }

      if (!this.isFirstConversation) {
        this.sendReadTicker(finalChatId);
      }
      // this.connectAndPoll(finalChatId);
    } catch (error) {
      console.error(error);
    }
  }

  async componentDidUpdate() {
    const { noRouting, userId } = this.props;

    // id are same don't force it!
    if (this.recvId === this.props.match.params.id || this.recvId === userId) {
      if (!this.props.foundMessages) {
        return;
      }

      if (this.props.foundMessages.length > 0) {
        // check if it's the old messages
        if (this.state.messages.length === this.props.foundMessages.length) {
          console.log("Same Mesasges Found");
          return;
        }

        // stop looking for new messages
        if (this.chatInterval) {
          clearInterval(this.chatInterval);
        }

        // save the current messages
        this.oldMessages = this.state.messages;

        // set the new messages
        this.setState({
          messages: this.props.foundMessages
        });
      } else {
        // check if old Messages exist
        if (this.oldMessages) {
          // set the messages back
          this.setState({
            messages: this.oldMessages
          });

          // enable the existing chat lookup
          this.chatInterval = setInterval(
            () => this.getChatHistory(this.chatId),
            3000
          );

          // reset old messages
          this.oldMessages = null;
        }
      }

      console.log("Default Return");
      return;
    }

    if (noRouting) {
      this.recvId = userId;
    } else {
      this.recvId = this.props.match.params.id;
    }

    this.oldMessages = null;

    const chatId1 = genChatId(this.recvId, this.userId);
    const chatId2 = genChatId(this.userId, this.recvId);

    try {
      const chatId = await isValidChatId(chatId1, chatId2);
      let finalChatId;

      if (chatId.status === 404) {
        this.isFirstConversation = true;
        finalChatId = chatId1;
      } else {
        finalChatId = await chatId.text();
        this.getChatHistory(finalChatId, true);
      }

      // set the chatid
      this.chatId = finalChatId;
      if (this.props.onChatIdFound) {
        console.log("Chat ID Found Base", finalChatId);
        this.props.onChatIdFound(finalChatId);
      }

      // loop it!
      if (this.chatInterval) {
        clearInterval(this.chatInterval);
      }

      this.chatInterval = setInterval(
        () => this.getChatHistory(finalChatId),
        3000
      );
      // this.connectAndPoll(finalChatId);

      // find applications
      const applicationres = await getAllApplications(this.recvId, this.userId);
      if (applicationres.status === 404) {
        console.log("No Applications");
        this.setState({ isApplicationFound: false });
        return;
      }

      const applicationData = await applicationres.json();
      const recvApplications = applicationData[this.userId];
      if (recvApplications.length > 0) {
        this.setState({ isApplicationFound: true });
        console.log("Application Found", recvApplications);
      } else {
        this.setState({ isApplicationFound: false });
      }

      // this.sendReadTicker(finalChatId);
      if (!this.isFirstConversation) {
        this.sendReadTicker(finalChatId);
      }
    } catch (error) {}
  }

  async onFileUploadToAws(file, md, chatid) {
    try {
      const link = `https://s3-us-west-2.amazonaws.com/img-mwf/`;
      const linkPart = `attachments/${chatid}/${+new Date()}__${md.name}`;
      const res = await uploadImage(linkPart, file);
      return link + linkPart;
    } catch (error) {
      return error;
    }
  }

  async onFileUploadClick(ev) {
    const target = ev.target;
    const file = ev.target.files[0];

    // if user clicked on cancel
    if (!file) {
      return;
    }

    // flush value
    target.value = "";

    // get metadata
    const name = file.name,
      size = file.size,
      type = file.type;

    this.setState({
      attachment: {
        name: name,
        size: Math.floor(size / 1024),
        type: type
      },
      isUploading: true
    });

    try {
      const res = await this.onFileUploadToAws(
        file,
        {
          name: name,
          size: size,
          type: type
        },
        this.chatId
      );

      this.setState(state => {

        if (!state.attachment) {
          return {
            ...state,
            isUploading: false
          }
        }

        return {
          ...state,
          attachment: {
            ...state.attachment,
            ext: res
          },
          isUploading: false
        }
      });

      this.onLinkUploaded();
    } catch (error) {}
  }

  onFileRemove = ev => {
    this.setState({
      attachment: null
    });
  };

  onMessageChange = (e, { value }) => {
    this.setState({
      currentMessage: value
    });
  };

  async onMessageSend(ev) {
    // found no attachment and no message
    if (!this.state.attachment && !this.state.currentMessage) {
      return;
    }

    // if it's uploading notify
    if (this.state.isUploading) {
      toast("Uploading");
      this.setState({
        isSendClicked: true
      });
      return;
    }

    try {
      let res;
      if (!this.state.attachment) {
        res = await startConversation(
          this.chatId,
          this.recvId,
          this.userId,
          this.state.currentMessage
        );
      } else {
        res = await startConversation(
          this.chatId,
          this.recvId,
          this.userId,
          this.state.currentMessage,
          {
            ext: this.state.attachment.ext,
            fileName: this.state.attachment.name,
            mediaType: getFileType(this.state.attachment.name),
            size: this.state.attachment.size
          }
        );
      }

      this.setState({
        attachment: null,
        currentMessage: "",
        isSendClicked: false
      });

      document.dispatchEvent(this.onSidebarTrigger);
    } catch (error) {
      console.error(error);
    }
  }

  onTemplateSend = () => {
    this.setState({
      isTemplateModalOpen: false
    });
  };

  onTemplateModalOpen = e => {
    this.setState({ isTemplateModalOpen: true });
  };

  onTemplateModalClose = e => {
    this.setState({ isTemplateModalOpen: false });
  };

  componentWillUnmount() {
    if (this.chatInterval) {
      clearInterval(this.chatInterval);
    }
  }

  getScrollRef = ref => {
    this.scrollDivRef = ref;
  };

  setScrollDivBottom = () => {
    const scrollDiv =
      window.screen.availWidth > 1024
        ? this.scrollDivRef
        : document.scrollingElement;

    if (scrollDiv) {
      // console.log(scrollDiv.current);
      scrollDiv.scrollTop = scrollDiv.scrollHeight;
    }
  };

  onShowMoreMessageClick = e => {
    this.setState(
      {
        showMoreMessage: false
      },
      () => {
        this.setScrollDivBottom();
      }
    );
  };

  render() {
    return (
      <React.Fragment>
        <ChatThreadMessageSection
          getScrollRef={this.getScrollRef}
          messages={this.state.messages}
          showMoreMessage={this.state.showMoreMessage}
          sender={this.userId}
          showInterviewTrigger={this.state.isApplicationFound}
          onShowInterviewScreen={this.props.onShowInterviewScreen}
          onShowMoreMessageClick={this.onShowMoreMessageClick}
        />

        <Responsive maxWidth={1024}>
          <MessagingMobileChatField
            onTemplateClick={this.props.onTemplateTrigger}
            currentMessage={this.state.currentMessage}
            onMessageChange={this.onMessageChange}
            onMessageSend={this.onMessageSend}
            attachment={this.state.attachment}
            onAttachmentClick={this.onFileUploadClick}
            onAttachmentRemove={this.onFileRemove}
          />
        </Responsive>

        <Responsive minWidth={1025}>
          <ChatThreadMessageField
            chatId={this.chatId}
            recvId={this.recvId}
            sendId={this.userId}
            isTemplateModalOpen={this.state.isTemplateModalOpen}
            onTemplateModalClose={this.onTemplateModalClose}
            onTemplateModalOpen={this.onTemplateModalOpen}
            onTemplateSend={this.onTemplateSend}
            currentMessage={this.state.currentMessage}
            attachment={this.state.attachment}
            onAttachmentClick={this.onFileUploadClick}
            onAttachmentRemove={this.onFileRemove}
            onMessageChange={this.onMessageChange}
            onMessageSend={this.onMessageSend}
          />
        </Responsive>
      </React.Fragment>
    );
  }
}

export default withRouter(CTMessagingContainer);
