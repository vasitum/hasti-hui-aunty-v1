import React from "react";
import { Container, Dropdown, Grid } from "semantic-ui-react";

import { Form } from "formsy-semantic-ui-react";

import ActionBtn from "../../../Buttons/ActionBtn";
import FlatDefaultBtn from "../../../Buttons/FlatDefaultBtn";
import FileuploadBtn from "../../../Buttons/FileuploadBtn";
import IcAttachment from "../../../../assets/svg/IcAttachment";
import TemplatesHeader from "../TemplatesHeader";
import InputField from "../../../Forms/FormFields/InputField";
import Middlecontainer from "../TemplatemiddleContainer";

// import getAllUserTemplate from "../../../../api/messaging/getAllUserTemplate";
import templateApi from "../../../../api/messaging/template";

import { USER } from "../../../../constants/api";
import { toast } from "react-toastify";
import "./index.scss";

class CreateNewTemplate extends React.Component {
  state = {
    inputVal: ""
  };

  constructor(props) {
    super(props);
    this.onSaveClick = this.onSaveClick.bind(this);
  }

  onInputChange = (e, { value }) => this.setState({ inputVal: value });

  async onSaveClick(e) {
    try {
      const res = await templateApi(
        this.state.inputVal,
        this.props.text,
        this.props.attachment
      );

      if (res.status === 200) {
        const data = await res.json();
        this.props.onTemplateCreate(data, true);
        return;
      }

      toast("Error, unable to push new template");
    } catch (error) {
      console.error("Error, unable to push new template");
      toast("UnexpectedError, unable to push new template");
    }
  }

  render() {
    return (
      <div className="tempMainDiv">
        <Container>
          <div className="tempHeader">
            <InputField
              name="newTemp"
              value={this.state.inputVal}
              onChange={this.onInputChange}
              placeholder="Enter template name"
            />
          </div>
          <div className="tempContainerBody" />
          <div className="tempContainerFooter">
            <ActionBtn onClick={this.onSaveClick} actioaBtnText="Save" />
            <FlatDefaultBtn
              onClick={this.props.onCancelClick}
              className="bgTranceparent"
              btntext="Cancel"
            />
          </div>
        </Container>
      </div>
    );
  }
}

export default CreateNewTemplate;
