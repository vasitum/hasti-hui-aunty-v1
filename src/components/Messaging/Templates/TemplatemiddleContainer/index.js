import React from "react";
import { Container, Dropdown, Grid } from "semantic-ui-react";

import "./index.scss";

class TemplatemiddleContainer extends React.Component {
  render() {
    return (
      // <Container>
      <div className="tempMainContainer">
        <p>
          Dear [<span>First_Name</span>]
        </p>
        <p>
          Thank you for your intrest in the [<span>postion</span>] at [<span>
            Company_name
          </span>]
        </p>
        <p>
          Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean
          commodo ligula eget dolor. Aenean massa strong. Cum sociis natoque
          penatibus et magnis dis parturient montes, nascetur ridiculus mus.
          Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
        </p>
        <p>Sincerely</p>
        <br />
      </div>
      //  </Container>
    );
  }
}

export default TemplatemiddleContainer;
