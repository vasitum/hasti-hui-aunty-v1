import React from "react";
import { Grid, Input, Modal } from "semantic-ui-react";
import SelectOptionField from "../../../Forms/FormFields/SelectOptionField";
import FlatDefaultBtn from "../../../Buttons/FlatDefaultBtn";
import ICPlus from "../../../../assets/svg/IcPlus";
import NewTemplates from "../NewTemplate";
import { Form } from "formsy-semantic-ui-react";
import { Link } from "react-router-dom";
import "./index.scss";

const TemplatesHeader = props => {
  const {
    getAllTemp,
    onTemplateChange,
    onTemplateCreate,
    currentTemplate,
    isCreateTemplateOpen,
    onCreateTemplateOpen,
    onCreateTemplateClose
  } = props;
  return (
    <div className="tempMainHeader">
      <Grid className="tempMain">
        <Grid.Row className="tempMainRow">
          <Grid.Column width="10">
            <Form>
              <SelectOptionField
                placeholder="Please Select the template"
                value={currentTemplate}
                optionsItem={getAllTemp}
                name="options"
                onChange={onTemplateChange}
              />
            </Form>
          </Grid.Column>
          <Grid.Column width="6" className="tempMainButton">
            <FlatDefaultBtn
              as={Link}
              to={"/job/managetemplate"}
              className="bgTranceparent"
              btntext="Create new"
              btnicon={<ICPlus pathcolor="#c8c8c8" height="10" />}
            />
            {/* <Modal
              open={isCreateTemplateOpen}
              onClose={onCreateTemplateClose}
              size="small"
              className="modal_newTamplate"
              trigger={
                <FlatDefaultBtn
                  onClick={onCreateTemplateOpen}
                  className="bgTranceparent"
                  btntext="Create new"
                  btnicon={<ICPlus pathcolor="#c8c8c8" height="10" />}
                />
              }
              closeOnDimmerClick={false}
              closeIcon>
              <NewTemplates
                onCreate={onTemplateCreate}
                onCancel={onCreateTemplateClose}
              />
            </Modal> */}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
};

export default TemplatesHeader;
