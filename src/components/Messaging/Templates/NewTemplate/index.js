import React from "react";

import TemplateFooter from "../TemplateButtonfooter";
import NewTemplateMiddle from "../NewTemplateMiddle";

import { Grid, Button, Dropdown, Input } from "semantic-ui-react";
import { Form } from "formsy-semantic-ui-react";

import InfoSection from "../../../Sections/InfoSection";
import InputContainer from "../../../Forms/InputContainer";
import InputFieldLabel from "../../../Forms/FormFields/InputFieldLabel";
import InputField from "../../../Forms/FormFields/InputField";
import getPlaceholder from "../../../Forms/FormFields/Placeholder";

import ActionBtn from "../../../Buttons/ActionBtn";
import FlatDefaultBtn from "../../../Buttons/FlatDefaultBtn";
import FileuploadBtn from "../../../Buttons/FileuploadBtn";
import IcAttachment from "../../../../assets/svg/IcAttachment";
import QuillText from "../../../CardElements/QuillText";
import QuillPlaceholder from "../../../CardElements/QuillPlaceholder";

import UploadDoc from "../../MediaSection/UploadeDoc";

import templateApi from "../../../../api/messaging/template";
import { USER } from "../../../../constants/api";
import { uploadImage } from "../../../../utils/aws";
import { toast } from "react-toastify";
import LabelStar from "../../../utils/LabelStar";

import "./index.scss";

const options = [
  { key: 1, text: "First name", value: 1 },
  { key: 2, text: "Last name", value: 2 },
  { key: 3, text: "Job title", value: 3 },
  { key: 4, text: "Company name", value: 4 },
  { key: 5, text: "Interview stage", value: 5 }
];

class NewTemplates extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      tempText: "",
      attachment: null,
      isUploading: false,
      isCreateClicked: false
    };

    this.onFormSubmit = this.onFormSubmit.bind(this);

    this.onInputChange = this.onInputChange.bind(this);
    // this.onTextChange = this.onTextChange.bind(this);
    this.onFileUploadAws = this.onFileUploadAws.bind(this);
    this.onAttachmentClick = this.onAttachmentClick.bind(this);
  }

  onInputChange = (ev, { value, name }) => {
    this.setState({
      [name]: value
    });
  };

  onTextChange = value => {
    this.setState({
      tempText: value
    });
  };

  onUploadComplete = ev => {
    if (!this.state.isCreateClicked) {
      return;
    }

    // passing an empty function as a failsafe
    this.onFormSubmit({ preventDefault: () => {} });
  };

  async onFileUploadAws(file, md) {
    try {
      const userId = window.localStorage.getItem(USER.UID);
      const link = `https://s3-us-west-2.amazonaws.com/img-mwf/`;
      const linkPart = `${userId}/${+new Date()}__${md.name}`;
      const res = await uploadImage(linkPart, file);
      return link + linkPart;
    } catch (error) {
      return error;
    }
  }

  async onAttachmentClick(e) {
    const target = e.target;
    const file = target.files[0];

    if (!file) {
      return;
    }

    // flush the values
    target.value = "";

    this.setState({
      attachment: {
        name: file.name,
        type: file.type,
        size: Math.floor(file.size / 1024)
      },
      isUploading: true
    });

    try {
      const link = await this.onFileUploadAws(file, { name: file.name });
      this.setState({
        attachment: {
          ...this.state.attachment,
          ext: link
        },
        isUploading: false
      });

      this.onUploadComplete();
    } catch (error) {}
  }

  onAttachmentRemoveClick = e => {
    this.setState({
      attachment: null
    });
  };

  async onFormSubmit(ev) {
    // console.log("checked data");
    const { name, tempText, attachment, isUploading } = this.state;

    if (isUploading) {
      toast("Uploading ...");
      this.setState({ isCreateClicked: true });
      return;
    }

    try {
      // const sendid = window.localStorage.getItem(USER.UID);
      const res = await templateApi(name, tempText, attachment);
      const data = await res.json();
      // console.log("checked data newTemp", data);
      this.props.onCreate(data);
    } catch (error) {
      console.error(error);
    }

    if (ev.preventDefault) {
      ev.preventDefault();
    }
  }

  render() {
    return (
      <div className="mainNewTemplate">
        <Form className="mainNewTemplateForm" onSubmit={this.onFormSubmit}>
          <div className="mainNewTemplate">
            <InfoSection
              headerSize="large"
              color="blue"
              headerText="Create new template">
              <p>
                Contrary to popular belief, Lorem Ipsum is not simply random
                text. It has roots in a piece of classical Latin literature from
                45 BC, making it over 2000 years old. Richard McClintock, a
                Latin professor at Hampden-Sydney College in Virginia, looked up
                one of the more obscure
              </p>

              <Form.Field>
                <label>{LabelStar("Template name")}</label>
                <Input
                  placeholder="Enter template name"
                  name="name"
                  value={this.state.name}
                  onChange={this.onInputChange}
                />
              </Form.Field>
              <Form.Field>
                <label>{LabelStar("Message")}</label>
                <QuillPlaceholder
                  name="tempText"
                  value={this.state.tempText}
                  onChange={this.onTextChange}
                />
              </Form.Field>
            </InfoSection>

            {this.state.attachment ? (
              <UploadDoc
                data={this.state.attachment}
                onRemoveClick={this.onAttachmentRemoveClick}
              />
            ) : null}
          </div>
          {/* <TemplateFooter /> */}

          <div className="mainTemplateButtonFooter">
            <Grid>
              <Grid.Row>
                <Grid.Column width="13">
                  <ActionBtn type="submit" actioaBtnText="Save" />
                  <FlatDefaultBtn
                    onClick={this.props.onCancel}
                    type="button"
                    className="bgTranceparent"
                    btntext="Cancel"
                  />
                </Grid.Column>
                <Grid.Column width="3" textAlign="right">
                  <FileuploadBtn
                    onFileUpload={this.onAttachmentClick}
                    className="bgTranceparent"
                    floated="right"
                    btnIcon={<IcAttachment pathcolor="#acaeb5" />}
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </div>
        </Form>
      </div>
    );
  }
}

export default NewTemplates;
