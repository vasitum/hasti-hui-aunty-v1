import React from "react";
import { Grid, Button, Dropdown, Header } from "semantic-ui-react";
import { Form } from "formsy-semantic-ui-react";
import InfoSection from "../../../Sections/InfoSection";
import InputContainer from "../../../Forms/InputContainer";
import InputFieldLabel from "../../../Forms/FormFields/InputFieldLabel";
import InputField from "../../../Forms/FormFields/InputField";
import getPlaceholder from "../../../Forms/FormFields/Placeholder";
import QuillText from "../../../CardElements/QuillText";
import QuillPlaceholder from "../../../CardElements/QuillPlaceholder";
import UploadDoc from "../../MediaSection/UploadeDoc";
import templateApi from "../../../../api/messaging/template";
import { USER } from "../../../../constants/api";

import LabelStar from "../../../utils/LabelStar";
// import { Form } from "formsy-semantic-ui-react";
import "./index.scss";

// const options = [
//   { key: 1, text: "First name", value: 1 },
//   { key: 2, text: "Last name", value: 2 },
//   { key: 3, text: "Job title", value: 3 },
//   { key: 4, text: "Company name", value: 4 },
//   { key: 5, text: "Interview stage", value: 5 }
// ];

class NewTemplatesMiddle extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      name,
      text,
      onInputChange,
      onTextChange,
      attachment,
      onAttachmentRemoveClick
    } = this.props;

    return (
      <div className="mainNewTemplate">
        <div className="createNew_temp">
          <Header as="h3" className="mobile hidden">
            Create new template
          </Header>

          <p>
            Contrary to popular belief, Lorem Ipsum is not simply random text.
            It has roots in a piece of classical Latin literature from 45 BC,
            making it over 2000 years old. Richard McClintock, a Latin professor
            at Hampden-Sydney College in Virginia, looked up one of the more
            obscure
          </p>

          <Form
            className="mainNewTemplateForm"
            onFormSubmit={this.onFormSubmit}>
            <Form.Field>
              <label>{LabelStar("Template name")}</label>
              <input
                value={name}
                onChange={onInputChange}
                LabelStar
                placeholder="Enter template name"
              />
            </Form.Field>
            {/* <Button className="mobile_mergeField">
              <Dropdown text="Add merge field" options={options } simple item />
            </Button>  */}

            {attachment ? (
              <Form.Field>
                <UploadDoc
                  data={attachment}
                  onRemoveClick={onAttachmentRemoveClick}
                />
              </Form.Field>
            ) : null}

            <Form.Field>
              <label>{LabelStar("Message")}</label>
              {/* <QuillText
                placeholder={getPlaceholder(
                  "Type your message here",
                  "Type your message here"
                )}
              /> */}
              <QuillPlaceholder
                onChange={onTextChange}
                value={text}
                placeholder={"Type your message here"}
              />
            </Form.Field>
          </Form>
        </div>
      </div>
    );
  }
}

export default NewTemplatesMiddle;
