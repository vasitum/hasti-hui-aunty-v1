import React from "react";

import { Grid, Dropdown, Button } from "semantic-ui-react";

import "./index.scss";

class SavetempOption extends React.Component {
  items = [
    {
      text: "New",
      value: "New"
    },
    {
      text: "Existing",
      value: "Existing",
      onClick: () => alert("hello")
    }
  ];
  render() {
    const { isShowSavedBtn } = this.props;
    return (
      <div className="Main_Dropdown">
        {!isShowSavedBtn ? (
          <Dropdown
            trigger={<p>Save template</p>}
            options={this.items}
            icon={null}
          />
        ) : (
          <Button>cvxdvxv</Button>
        )}
      </div>
    );
  }
}

export default SavetempOption;
