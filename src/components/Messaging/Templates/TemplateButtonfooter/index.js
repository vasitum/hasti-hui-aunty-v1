import React from "react";
import { Grid } from "semantic-ui-react";
import ActionBtn from "../../../Buttons/ActionBtn";
import FlatDefaultBtn from "../../../Buttons/FlatDefaultBtn";
import FileuploadBtn from "../../../Buttons/FileuploadBtn";
import IcAttachment from "../../../../assets/svg/IcAttachment";
import "./index.scss";

const TemplateFooter = ({ ...resProps }) => (
  <div className="mainTemplateButtonFooter" {...resProps}>
    <Grid>
      <Grid.Row>
        <Grid.Column width="13">
          <ActionBtn actioaBtnText="Save" />
          <FlatDefaultBtn className="bgTranceparent" btntext="Cancel" />
        </Grid.Column>
        <Grid.Column width="3" textAlign="right">
          <FileuploadBtn
            className="bgTranceparent"
            floated="right"
            btnIcon={<IcAttachment pathcolor="#acaeb5" />}
          />
        </Grid.Column>
      </Grid.Row>
    </Grid>
  </div>
);

export default TemplateFooter;
