import React from "react";
import { Container, Dropdown, Grid, Modal, Button } from "semantic-ui-react";

import ActionBtn from "../../../Buttons/ActionBtn";
import FileuploadBtn from "../../../Buttons/FileuploadBtn";
import IcAttachment from "../../../../assets/svg/IcAttachment";
import TemplatesHeader from "../TemplatesHeader";
import CreateNewTemplate from "../CreateNewTemplate";
import QuillPlaceholder from "../../../CardElements/QuillPlaceholder";

import Handlebars from "handlebars";

import UploadDoc from "../../MediaSection/UploadeDoc";

import getAllUserTemplate from "../../../../api/messaging/getAllUserTemplate";
import getUserFromUserId from "../../../../api/user/getUserById";
import sendConversation from "../../../../api/messaging/startConversation";
import { USER } from "../../../../constants/api";
import "./index.scss";

// import QuillText from "../../../CardElements/QuillText";
// import Middlecontainer from "../TemplatemiddleContainer";
// import FlatDefaultBtn from "../../../Buttons/FlatDefaultBtn";
// import IcRightCheck from "../../../../assets/svg/IcRightCheck";

class TemplateContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      templateJson: [],
      isShowBtn: false,
      currentTemplate: "",
      displayText: "",
      attachment: null,
      isCreateTemplateOpen: false,
      isNewEditModalOpen: false
    };

    this.templates = null;

    // binding
    this.onSendClick = this.onSendClick.bind(this);
  }

  onNewEditModalTrigger = e => {
    this.setState(state => {
      return {
        ...state,
        isNewEditModalOpen: !state.isNewEditModalOpen
      };
    });
  };

  heandIsShow = () => {
    this.setState({
      isShowBtn: true
    });
  };

  async componentDidMount() {
    try {
      const sendid = window.localStorage.getItem(USER.UID);
      const res = await getAllUserTemplate(sendid, "messageTemp");

      if (res.status === 404) {
        console.log("No, Templates Found");
        return;
      }

      const dataJson = await res.json();

      // set the data
      const filteredData = Array.from(dataJson).filter(val => {
        if (val.tempName) {
          return true;
        }

        return false;
      });

      this.templates = filteredData;

      this.setState({
        templateJson: filteredData.map(val => {
          return {
            text: val.tempName,
            value: val._id,
            key: val._id
          };
        })
      });
    } catch (error) {
      console.error(error);
    }
  }

  onTemplateCreate = template => {
    if (!template) {
      return;
    }

    // console.log(template);
    this.templates.push(template);

    if (!template.userMedia) {
      this.setState({
        isCreateTemplateOpen: false,
        isNewEditModalOpen: false,
        templateJson: [
          ...this.state.templateJson,
          { text: template.tempName, value: template._id, key: template._id }
        ],
        currentTemplate: template._id,
        displayText: template.text
      });

      return;
    }

    this.setState({
      isCreateTemplateOpen: false,
      isNewEditModalOpen: false,
      templateJson: [
        ...this.state.templateJson,
        { text: template.tempName, value: template._id, key: template._id }
      ],
      currentTemplate: template._id,
      displayText: template.text,
      attachment: {
        name: template.userMedia.fileName,
        size: template.userMedia.size,
        ext: template.userMedia.ext,
        type: template.userMedia.mediaType
      }
    });
  };

  onTemplateOpen = ev => {
    this.setState({ isCreateTemplateOpen: true });
  };

  onTemplateClose = ev => {
    this.setState({ isCreateTemplateOpen: false });
  };

  onQuillChange = html => {
    // console.log(html);
    this.setState({
      displayText: html
    });
  };

  onSelectChange = (ev, { value }) => {
    // No templates found
    if (!this.templates) {
      return;
    }

    const filteredTemplate = this.templates.filter(val => val._id === value);

    if (!filteredTemplate || filteredTemplate.length === 0) {
      return;
    }
    const txt = filteredTemplate[0].text || "";
    const userMedia = filteredTemplate[0].userMedia || null;

    if (!userMedia) {
      this.setState({
        currentTemplate: value,
        displayText: txt
      });

      return;
    }

    this.setState({
      currentTemplate: value,
      displayText: txt,
      attachment: {
        name: userMedia.fileName,
        size: userMedia.size,
        ext: userMedia.ext,
        type: userMedia.mediaType
      }
    });
  };

  onRemoveAttachmentClick = e => {
    this.setState({ attachment: null });
  };

  async onSendClick(e) {
    const { chatId, recvId, sendId } = this.props;

    try {
      const recver = await getUserFromUserId(recvId);
      if (recver.status === 200) {
        const recverJson = await recver.json();
        const template = Handlebars.compile(this.state.displayText);
        const finalText = template({
          FirstName: recverJson.fName,
          LastName: recverJson.lName
        });

        const media = this.state.attachment
          ? {
              fileName: this.state.attachment.name,
              mediaType: this.state.attachment.type,
              size: this.state.attachment.size,
              ext: this.state.attachment.ext
            }
          : null;

        const res = await sendConversation(
          chatId,
          recvId,
          sendId,
          finalText,
          media
        );

        if (res.status === 200) {
          this.props.onSendClick();
        }

        return;
      }
    } catch (error) {}

    // this.props.onSendClick(this.state.displayText, this.state.attachment);
  }

  getDisplayText = e => {
    return this.state.displayText;
  };

  render() {
    const { isShowBtn } = this.state;

    // const items = [
    //   {
    //     text: (
    //       <Modal
    //         open={this.state.isNewEditModalOpen}
    //         onClose={this.onNewEditModalTrigger}
    //         size="tiny"
    //         dimmer="inverted"
    //         className="modal_newTamplate"
    //         trigger={
    //           <Button
    //             onClick={this.onNewEditModalTrigger}
    //             className="newTempBtn">
    //             New{" "}
    //           </Button>
    //         }>
    //         <CreateNewTemplate
    //           text={this.getDisplayText()}
    //           attachment={this.state.attachment}
    //           onTemplateCreate={this.onTemplateCreate}
    //           onCancelClick={this.onNewEditModalTrigger}
    //         />
    //       </Modal>
    //     ),
    //     value: "New"
    //   },
    //   {
    //     text: "Existing",
    //     value: "Existing",
    //     onClick: () => this.heandIsShow()
    //   }
    // ];

    return (
      <div className="tempMainDiv">
        <Container>
          <TemplatesHeader
            isCreateTemplateOpen={this.state.isCreateTemplateOpen}
            onCreateTemplateOpen={this.onTemplateOpen}
            onCreateTemplateClose={this.onTemplateClose}
            onTemplateCreate={this.onTemplateCreate}
            onTemplateChange={this.onSelectChange}
            currentTemplate={this.state.currentTemplate}
            getAllTemp={this.state.templateJson}
          />
          <div className="tempMainContainer">
            {this.state.attachment ? (
              <UploadDoc
                data={this.state.attachment}
                onRemoveClick={this.onRemoveAttachmentClick}
              />
            ) : null}
            <QuillPlaceholder
              isSmall
              isDoubleDown
              readOnly={
                !this.state.displayText || this.state.displayText.length === 11
              }
              onChange={this.onQuillChange}
              defaultValue={this.state.displayText}
              value={this.state.displayText}
              placeholder="Your selected template will show here"
            />

            <div className="tempMainContainerFooter mobile hidden">
              <Grid>
                <Grid.Row>
                  <Grid.Column width="13">
                    <ActionBtn
                      actioaBtnText="Send"
                      onClick={this.onSendClick}
                    />
                    {/* {!isShowBtn ? (
                      // <Dropdown
                      //   trigger={
                      //     <FlatDefaultBtn
                      //       className="bgTranceparent"
                      //       btntext="Save templates"
                      //     />
                      //   }
                      //   options={items}
                      //   icon={null}
                      //   pointing="bottom"
                      // />
                      <div />
                    ) : (
                      <FlatDefaultBtn
                        className="bgTranceparent saveTamplateBtn"
                        btnicon={<IcRightCheck pathcolor="#797979" />}
                        btntext="Saved"
                      />
                    )} */}
                  </Grid.Column>
                  <Grid.Column width="3" textAlign="right">
                    <FileuploadBtn
                      className="bgTranceparent"
                      floated="right"
                      btnIcon={<IcAttachment pathcolor="#acaeb5" />}
                    />
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </div>
          </div>
        </Container>
      </div>
    );
  }
}

export default TemplateContainer;
