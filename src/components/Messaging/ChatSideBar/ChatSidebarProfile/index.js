import React from "react";

import { Grid } from "semantic-ui-react";

import IcUserIcon from "../../../../assets/svg/IcUser";

class ChatSidebarProfile extends React.Component {
  render() {
    return (
      <div className="ChatSidebarProfile">
        <Grid>
          <Grid.Row>
            <Grid.Column computer={8}>
              <div className="userProfile">
                {/* <Image src={<IcUserIcon />} /> */}
                <IcUserIcon />
              </div>
            </Grid.Column>
            <Grid.Column computer={8}>
              <div className="userProfile">More Option Menu</div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}

export default ChatSidebarProfile;
