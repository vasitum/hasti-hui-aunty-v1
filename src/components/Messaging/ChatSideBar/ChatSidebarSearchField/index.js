import React from "react";

import { Grid, Header, Button } from "semantic-ui-react";
import { Form } from "formsy-semantic-ui-react";

import IcUserIcon from "../../../../assets/svg/IcUser";
import InputField from "../../../Forms/FormFields/InputField";
// import { Button } from "glamorous";
import SearchIcon from "../../../../assets/svg/SearchIcon";
import "./index.scss";

class ChatSidebarSearchField extends React.Component {
  render() {
    const { onSearchChange, onSearchClick, searchTerm } = this.props;

    return (
      <div className="ChatSidebarSearchField">
        <Form onSubmit={onSearchClick}>
          <div className="searchField messaging_search">
            <InputField
              value={searchTerm}
              placeholder="Search by name"
              onChange={onSearchChange}
              name="search"
            />
            <Button className="searchIcon" type="submit">
              <SearchIcon pathcolor={searchTerm ? "#1f2532" : "#eaeaee"} />
            </Button>
          </div>
        </Form>
      </div>
    );
  }
}

export default ChatSidebarSearchField;
