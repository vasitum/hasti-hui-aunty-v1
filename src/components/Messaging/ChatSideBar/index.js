import React from "react";

import ChatSideBarProfile from "./ChatSidebarProfile";
import ChatSidebarNotification from "./ChatSidebarNotification";
import ChatSidebarSearchField from "./ChatSidebarSearchField";
import ChatSidebarPeopleCard from "./ChatSidebarPeopleCard";

import userAllConversation from "../../../api/messaging/userAllConversationList";
import searchUser from "../../../api/messaging/searchUser";
import { USER } from "../../../constants/api";

import { withRouter } from "react-router-dom";
import { toast } from "react-toastify";

// import debounce from "../../../utils/messaging";

import "./index.scss";

function getUserId(list) {
  const myId = window.localStorage.getItem(USER.UID);
  for (let idx = 0; idx < list.length; idx++) {
    const element = list[idx];
    if (element.userId !== myId) {
      return element.userId;
    }
  }
  return null;
}

class ChatSideBar extends React.Component {
  componentWillMount() {
    // console.log("hahhahaa user", USER);
  }

  constructor(props) {
    super(props);
    this.state = {
      searchItem: [],
      userList: [],
      searchTerm: "",
      clickedUserId: ""
    };
    this.clickedUserId = null;
    this.userId = window.localStorage.getItem(USER.UID);
    this.userNames = [];

    // bind
    this.onSearchSubmit = this.onSearchSubmit.bind(this);

    document.addEventListener("vassidebar", e => {
      this.componentDidMount();
    });
  }

  async componentDidMount() {
    try {
      const res = await userAllConversation();

      const splitPath = window.location.pathname.split("/");
      if (splitPath.length === 3) {
        this.clickedUserId = splitPath.pop();
      }

      if (res.status === 404) {
        // console.log("NOT FOUND");
        if (this.props.onNoChatFound) {
          if (!this.clickedUserId) {
            this.props.onNoChatFound();
          }
        }
        return;
      }

      const data = await res.json();

      if (data.length === 0) {
        if (this.props.onNoChatFound) {
          if (!this.clickedUserId) {
            this.props.onNoChatFound();
          }
        }
      }

      this.setState({ userList: data }, () => {
        // if (
        //   this.props.match.path.startsWith("/messaging") &&
        //   this.props.match.path !== "/messaging/:id"
        // ) {
        //   const id = getUserId(data[0].metaData);
        //   if (id) {
        //     this.props.history.push(`/messaging/${id}`);
        //   }
        // }
      });
    } catch (error) {}
  }

  onCardClick = id => {
    // window.location.pathname = `/messaging/${id}`;
    this.clickedUserId = id;
    this.props.history.push(`/messaging/${id}`);
    // this.setState({
    //   clickedUserId: id
    // });
  };

  onSearchChange = (ev, { value }) => {
    if (value === "") {
      this.componentDidMount();
    }
    this.setState({ searchTerm: value });
  };

  onUserNameListUpdate = userName => {
    this.userNames.push(userName);
  };

  async onSearchSubmit() {
    if (!this.state.searchItem) {
      this.componentDidMount();
      return;
    }

    try {
      const res = await searchUser(this.userId, this.state.searchTerm);
      if (res.status === 404) {
        this.componentDidMount();
        // console.log("Unable to find user with this name");
        toast("Unable to find user with this name");
        return;
      }

      const data = await res.json();
      this.setState({ userList: data });
      // console.log(data);
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    return (
      <div className="ChatSideBar">
        <ChatSidebarSearchField
          onSearchChange={this.onSearchChange}
          searchTerm={this.state.searchTerm}
          onSearchClick={this.onSearchSubmit}
        />
        <div className="chatSideBarPeopleList">
          <div>
            <ChatSidebarPeopleCard
              currentUser={this.clickedUserId}
              onCardClick={this.onCardClick}
              userid={this.userId}
              userList={this.state.userList}
              onUserNameListUpdate={this.onUserNameListUpdate}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(ChatSideBar);
