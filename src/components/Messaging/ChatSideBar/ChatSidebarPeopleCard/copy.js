import React from "react";

import { Grid, Header, Label } from "semantic-ui-react";

import IcUserIcon from "../../../../assets/svg/IcUser";
import userProfile from "../../.../../../../assets/img/profile.png";

// import allConversation from "../../../../api/messaging/userAllConversationList";

import InfiniteScroll from "react-infinite-scroller";

import "./index.scss";

class ChatSidebarPeopleCard extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tracks: [],
      hasMoreItems: true,
      nextHref: null
    };
  }

  loadItems(page) {
    var self = this;

    var url = "https://randomuser.me/api/?results=15";

    if (this.state.nextHref) {
      url = this.state.nextHref;
    }

    fetch(url)
      .then(res => res.json())
      .then(function(resp) {
        if (resp) {
          // console.log(resp);
          var tracks = self.state.tracks;
          self.setState({
            tracks: resp.results
          });

          // resp.results

          if (resp.next_href) {
            self.setState({
              tracks: tracks,
              nextHref: resp.next_href
            });
          } else {
            self.setState({
              hasMoreItems: false
            });
          }

          // resp.collection.map(track => {
          //   if (track.picture.medium == null) {
          //     track.picture.medium = track.picture.medium;
          //   }

          //   tracks.push(track);
          // });

          // if (resp.next_href) {
          //   self.setState({
          //     tracks: tracks,
          //     nextHref: resp.next_href
          //   });
          // } else {
          //   self.setState({ var tracks = self.state.tracks;
          //     hasMoreItems: false
          //   });
          // }
        }
      });
  }

  render() {
    const loader = <div className="loader">Loading ...</div>;

    var items = [];
    this.state.tracks.map((track, i) => {
      items.push(
        <div className="track" key={i} style={{ paddingLeft: "20px" }}>
          <div className="ChatSidebarPeopleCard">
            <div className="profile_img">
              <img src={track.picture.medium} />
              {/* <Label floating>2</Label> */}
            </div>
            <div className="profile_summary">
              <Header as="h4">{track.name.first}</Header>
              {/* <p className="summary_title">Lorem ipsum d...</p>
              <p className="summary_subTitle">05:00PM</p> */}
            </div>
          </div>
          {/* <a href={track.permalink_url} target="_blank">
            <img src={track.artwork_url} width="150" height="150" />
            <p className="title">{track.title}</p>
          </a> */}
        </div>
      );
    });

    return (
      <InfiniteScroll
        pageStart={0}
        loadMore={this.loadItems.bind(this)}
        hasMore={this.state.hasMoreItems}
        loader={loader}>
        <div className="tracks">{items}</div>
      </InfiniteScroll>
    );
  }
}

export default ChatSidebarPeopleCard;
