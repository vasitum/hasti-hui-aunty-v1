import React from "react";

import { Grid, Header, Label } from "semantic-ui-react";

import IcUserIcon from "../../../../assets/svg/IcUser";
import userProfile from "../../.../../../../assets/img/profile.png";

import UserLogo from "../../../../assets/svg/IcUser";

import fromNow from "../../../../utils/env/fromNow";
import cx from "classnames";

import QuillText from "../../../CardElements/QuillText";
import htmlToText from "../../../../utils/env/fromHtmltoText";

// import allConversation from "../../../../api/messaging/userAllConversationList";

import InfiniteScroll from "react-infinite-scroller";

import "./index.scss";

function getUserFromMetaData(userid, metadata) {
  if (!metadata) {
    return null;
  }

  const userdata = metadata.filter(data => data.userId !== userid);
  return !userdata || !userdata.length ? null : userdata[0];
}

function getSenderMetadata(userid, metadata) {
  if (!metadata) {
    return null;
  }

  const userdata = metadata.filter(data => data.userId === userid);
  return !userdata || !userdata.length ? null : userdata[0];
}

function getTimeFromDate() {}

class ChatSidebarPeopleCard extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      userid,
      userList,
      onCardClick,
      onUserNameListUpdate,
      currentUser
    } = this.props;
    // console.log("userList", userList);
    return userList.map((user, i) => {
      const userData = getUserFromMetaData(userid, user.metaData);
      const senderData = getSenderMetadata(userid, user.metaData) || {};

      if (!userData) {
        return null;
      }

      // update user name
      onUserNameListUpdate(userData.name);
      const cxs = cx({
        ChatSidebarPeopleCard__outer: true,
        "is-active": currentUser === userData.userId
      });

      return (
        <div
          className={cxs}
          key={i}
          onClick={e => onCardClick(userData.userId)}>
          <div
            className={`ChatSidebarPeopleCard ${
              senderData.unReadCount > 0 ? "is-unread" : ""
            }`}>
            <div className="profile_img">
              {!userData.ext ? (
                <React.Fragment>
                  <UserLogo
                    width="32"
                    height="32"
                    rectcolor="#f7f7fb"
                    pathcolor="#c8c8c8"
                  />
                </React.Fragment>
              ) : (
                <img
                  src={`https://s3-us-west-2.amazonaws.com/img-mwf/${
                    userData.userId
                  }/image.jpg`}
                />
              )}
              {senderData.unReadCount && senderData.unReadCount > 0 ? (
                <Label floating>{senderData.unReadCount}</Label>
              ) : null}
            </div>
            <div className="profile_summary">
              <Header as="h4">
                <span className="activeCardName">{userData.name}</span>{" "}
              </Header>
              {/* <p className="summary_title">{user.lastmessage} </p> */}
              <QuillText
                hidden={!user.lastmessage || user.lastmessage.length === 11}
                value={
                  user.lastmessage
                    ? htmlToText(user.lastmessage).substr(0, 17) + "..."
                    : ""
                }
                placeholder={""}
                readOnly
              />
              <p className="summary_subTitle">{fromNow(user.lastMsgTime)}</p>
            </div>
          </div>
        </div>
      );
    });
  }
}

export default ChatSidebarPeopleCard;
