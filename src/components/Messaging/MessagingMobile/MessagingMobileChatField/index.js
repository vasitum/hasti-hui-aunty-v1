import React, { PureComponent } from "react";

import { Grid, Button } from "semantic-ui-react";

import TextAreaField from "../../../Forms/FormFields/TextAreaField";
import FileuploadBtn from "../../../Buttons/FileuploadBtn";
import FlatDefaultBtn from "../../../Buttons/FlatDefaultBtn";
import QuillText from "../../../CardElements/QuillText";

import IcAttachment from "../../../../assets/svg/IcAttachment";
import IcSendMessage from "../../../../assets/svg/IcSendMessage";
import IcMessagingTemplate from "../../../../assets/svg/IcMessagingTemplate";

import UploadDoc from "../../MediaSection/UploadeDoc";

import "./index.scss";
import { Form } from "formsy-semantic-ui-react";

class MessagingMobileChatField extends PureComponent {
  state = {
    isInFocus: false
  };

  onFocusTrigger = () => {
    this.setState(state => ({
      isInFocus: true
    }));
  };

  handleKeyPress = event => {
    if (event.ctrlKey && event.which == 13) {
      this.props.onMessageSend();
    }
  };

  render() {
    const props = this.props;
    return (
      <div className="MessagingMobileChatField">
        <Form>
          <div className="MessagingMobileChatBox">
            <div className="MessagingMobile_ChatBtn">
              {!this.state.isInFocus ? (
                <Button
                  compact
                  className="mobile_chatTemBtnIcon"
                  onClick={e => {
                    if (props.onTemplateClick) {
                      props.onTemplateClick();
                    }
                  }}>
                  <IcMessagingTemplate pathcolor="#acaeb5" />
                </Button>
              ) : (
                <Button
                  compact
                  className="mobile_chatTemBtnIcon"
                  onClick={e => {
                    if (props.onTemplateClick) {
                      props.onTemplateClick();
                    }
                  }}>
                  <IcMessagingTemplate pathcolor="#acaeb5" />
                </Button>
              )}
            </div>
            <div className="mobile_chatField MessagingMobileChat">
              <Grid>
                <Grid.Row>
                  <Grid.Column width={16}>
                    <div>
                      {props.attachment ? (
                        <UploadDoc
                          data={props.attachment}
                          onRemoveClick={props.onAttachmentRemove}
                        />
                      ) : null}
                    </div>
                    <TextAreaField
                      onFocus={this.onFocusTrigger}
                      placeholder="Write a reply"
                      onChange={props.onMessageChange}
                      value={props.currentMessage}
                      rows="1"
                      onKeyPress={this.handleKeyPress}
                    />
                    <div className="mobile_chatAttachment">
                      <FileuploadBtn
                        onFileUpload={props.onAttachmentClick}
                        className="bgTranceparent attachmentBtn"
                        floated="right"
                        btnIcon={<IcAttachment pathcolor="#acaeb5" />}
                      />
                    </div>
                    {/* <QuillText
                      onFocus={this.onFocusTrigger}
                      onChange={props.onMessageChange}
                      defaultValue={props.currentMessage}
                      value={props.currentMessage}
                      placeholder={"Write a reply"}
                      isMessagebox
                      isSmall
                    /> */}
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </div>
            <div className="">
              <FlatDefaultBtn
                onClick={props.onMessageSend}
                className="bgTranceparent"
                floated="right"
                btnicon={<IcSendMessage pathcolor="#acaeb5" />}
              />
            </div>
          </div>
        </Form>
      </div>
    );
  }
}

export default MessagingMobileChatField;
