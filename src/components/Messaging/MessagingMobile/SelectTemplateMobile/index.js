import React from "react";
import { Button } from "semantic-ui-react";
import NewTemplateMiddle from "../../Templates/NewTemplateMiddle";
import MobileHeader from "../../../MobileComponents/MobileHeader";
import MobileFooter from "../../../MobileComponents/MobileFooter";
import ActionBtn from "../../../Buttons/ActionBtn";
import IcFooterArrowIcon from "../../../../assets/svg/IcFooterArrowIcon";
import FlatDefaultBtn from "../../../Buttons/FlatDefaultBtn";
import IcPuls from "../../../../assets/svg/IcPlus";

import {Link} from "react-router-dom";

import { USER } from "../../../../constants/api";
import getAllUserTemplates from "../../../../api/messaging/getAllUserTemplate";
import { toast } from "react-toastify";

import "./index.scss";



class SelectTemplateMobile extends React.Component {
  state = {
    isNotFound: false,
    templates: []
  };

  async componentDidMount() {
    try {
      const userid = window.localStorage.getItem(USER.UID);
      const res = await getAllUserTemplates(userid);

      if (res.status === 404) {
        this.setState({ isNotFound: true });
        return;
      }

      const data = await res.json();
      this.setState({ templates: data });
    } catch (error) {
      console.error(error);
      toast("Unexpected Error in fetching Templates");
    }
  }

  onTemplateClick = e => {
    if (this.props.onTemplateClick) {
      const id = e.target.id;
      const template = this.state.templates.filter(
        template => template._id === id
      );
      this.props.onTemplateClick(template[0]);
    }
  };

  render() {
    return (
      <div className="SelectTemplateMobile">
        <div className="templateMobileHeader">
          <MobileHeader
            headerLeftIcon={
              <IcFooterArrowIcon
                onClick={this.props.onBackClick}
                pathcolor="#797979"
              />
            }
            headerTitle="Select Template"
          />
        </div>
        <div className="templateMobilemiddleContainer">
          <div className="createNewTemp_mobileBtn">
            <FlatDefaultBtn
              as={Link}
              to={"/job/managetemplate"}
              // onClick={this.props.onCreateTemplateClick}
              btnicon={<IcPuls pathcolor="#a1a1a1" />}
              btntext="Create new template"
            />
          </div>

          {this.state.templates.map(template => {
            return (
              <div className="templateName" onClick={this.onTemplateClick}>
                <p className="title" id={template._id}>
                  {template.tempName}
                </p>
                {/* <p className="subTitle">
                  Lorem Ipsum is simply dummy text Ipsum is simply dummy text
                </p> */}
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

export default SelectTemplateMobile;
