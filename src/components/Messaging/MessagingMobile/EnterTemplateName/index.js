import React from "react";
import { Button } from "semantic-ui-react";
import { Form } from "formsy-semantic-ui-react";
import MobileHeader from "../../../MobileComponents/MobileHeader";
import MobileFooter from "../../../MobileComponents/MobileFooter";
import ActionBtn from "../../../Buttons/ActionBtn";
import IcFooterArrowIcon from "../../../../assets/svg/IcFooterArrowIcon";
import InputField from "../../../Forms/FormFields/InputField";

import IcAttachment from "../../../../assets/svg/IcAttachment";

import FileuploadBtn from "../../../Buttons/FileuploadBtn";

import "./index.scss";

class EnterTemplateName extends React.Component {
  render() {
    return (
      <div className="EnterTemplateName_mobile">
        <div className="templateMobileHeader">
          <Form className="templateName">
            <InputField name="templateName" placeholder="Enter template name" />
          </Form>
        </div>
        <div className="templateMobilemiddleContainer" />
        <div className="templateMobileFooter">
          <MobileFooter headerLeftIcon={<ActionBtn actioaBtnText="Save" />}>
            <Button>Cancel</Button>
          </MobileFooter>
        </div>
      </div>
    );
  }
}

export default EnterTemplateName;
