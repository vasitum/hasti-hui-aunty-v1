import React from "react";
import { Dropdown, Button } from "semantic-ui-react";
import { Form } from "formsy-semantic-ui-react";
import Templatecontainer from "../../Templates/TemplatemiddleContainer";
import MobileHeader from "../../../MobileComponents/MobileHeader";
import MobileFooter from "../../../MobileComponents/MobileFooter";
import ActionBtn from "../../../Buttons/ActionBtn";
import FileuploadBtn from "../../../Buttons/FileuploadBtn";

import IcAttachment from "../../../../assets/svg/IcAttachment";
import IcFooterArrowIcon from "../../../../assets/svg/IcFooterArrowIcon";

import QuillPlaceholder from "../../../CardElements/QuillPlaceholder";
import UploadDoc from "../../MediaSection/UploadeDoc";
import { USER } from "../../../../constants/api";
import { uploadImage } from "../../../../utils/aws";
import { toast } from "react-toastify";

import sendConversation from "../../../../api/messaging/startConversation";

import Handlebars from "handlebars";

import "./index.scss";

// const items = [
//   {
//     text: "New",
//     value: "New"
//   },
//   {
//     text: "Existing",
//     value: "Existing"
//   }
// ];

class TemplateMobileContainer extends React.Component {
  state = {
    attachment: null,
    headerName: "",
    text: "",
    isUploading: false,
    isCreateClicked: false
  };

  constructor(props) {
    super(props);
    this.onFileUploadAws = this.onFileUploadAws.bind(this);
    this.onAttachmentClick = this.onAttachmentClick.bind(this);
    this.onSendClick = this.onSendClick.bind(this);
  }

  onQuillChange = html => {
    this.setState({
      text: html
    });
  };

  onUploadComplete = ev => {
    if (!this.state.isCreateClicked) {
      return;
    }

    this.onFormSubmit();
  };

  async onFileUploadAws(file, md) {
    try {
      const userId = window.localStorage.getItem(USER.UID);
      const link = `https://s3-us-west-2.amazonaws.com/img-mwf/`;
      const linkPart = `${userId}/${+new Date()}__${md.name}`;
      const res = await uploadImage(linkPart, file);
      return link + linkPart;
    } catch (error) {
      return error;
    }
  }

  async onAttachmentClick(e) {
    const target = e.target;
    const file = target.files[0];

    if (!file) {
      return;
    }

    // flush the values
    target.value = "";

    this.setState({
      attachment: {
        name: file.name,
        type: file.type,
        size: Math.floor(file.size / 1024)
      },
      isUploading: true
    });

    try {
      const link = await this.onFileUploadAws(file, { name: file.name });
      this.setState({
        attachment: {
          ...this.state.attachment,
          ext: link
        },
        isUploading: false
      });

      this.onUploadComplete();
    } catch (error) {}
  }

  onAttachmentRemoveClick = e => {
    this.setState({
      attachment: null
    });
  };

  async onSendClick(ev) {
    const { currentChatId, userDetails } = this.props;

    if (!currentChatId) {
      console.error("Chat ID Not Found");
      return;
    }

    if (!userDetails) {
      console.error("Userdetails not Found");
      return;
    }

    try {
      const textTemplate = Handlebars.compile(this.state.text);
      const finalText = textTemplate({
        FirstName: userDetails.firstName,
        LastName: userDetails.lastName
      });

      console.log("Final text ", finalText);

      const media = this.state.attachment
        ? {
            fileName: this.state.attachment.name,
            mediaType: this.state.attachment.type,
            size: this.state.attachment.size,
            ext: this.state.attachment.ext
          }
        : null;

      const userId = window.localStorage.getItem(USER.UID);

      const res = sendConversation(
        currentChatId,
        userDetails.id,
        userId,
        finalText,
        media
      );

      this.props.onSendClick();
    } catch (error) {
      console.error(error);
    }
  }

  componentDidMount() {
    const { template } = this.props;

    if (!template) {
      return;
    }

    if (template.userMedia) {
      this.setState({
        attachment: {
          ext: template.userMedia.ext,
          name: template.userMedia.fileName,
          size: template.userMedia.size,
          type: template.userMedia.mediaType
        },
        headerName: template.tempName,
        text: template.text
      });

      return;
    }

    this.setState({
      headerName: template.tempName,
      text: template.text
    });
  }

  render() {
    const { template } = this.props;
    // console.log("TEMPLATE_FOUND", template);

    return (
      <div>
        <div className="templateMobileHeader">
          <MobileHeader
            headerLeftIcon={
              <IcFooterArrowIcon
                onClick={this.props.onBackClick}
                pathcolor="#797979"
              />
            }
            headerTitle={this.state.headerName}
          />
        </div>
        <Form className="mainNewTemplate">
          <div className="mainNewTemplateForm selectTemDrop">
            <div>
              {this.state.attachment ? (
                <UploadDoc
                  data={{
                    name: this.state.attachment.name,
                    size: this.state.attachment.size,
                    type: this.state.attachment.type
                  }}
                />
              ) : null}
            </div>
            <div className="templateMobilemiddleContainer">
              {/* <Templatecontainer /> */}

              <QuillPlaceholder
                onChange={this.onQuillChange}
                value={this.state.text}
              />
            </div>
            <div className="templateMobileFooter">
              <MobileFooter
                headerLeftIcon={
                  <ActionBtn actioaBtnText="Send" onClick={this.onSendClick} />
                }
                headerTitle={
                  <FileuploadBtn
                    onFileUpload={this.onAttachmentClick}
                    btnText={<IcAttachment pathcolor="#6e768a" />}
                  />
                }>
                {/* <Dropdown
                  trigger={<Button>Save Template</Button>}
                  options={items}
                  icon={null}
                  pointing="bottom"
                /> */}
              </MobileFooter>
            </div>
          </div>
        </Form>
      </div>
    );
  }
}

export default TemplateMobileContainer;
