import React from "react";
import { Button } from "semantic-ui-react";
import NewTemplateMiddle from "../../Templates/NewTemplateMiddle";
import MobileHeader from "../../../MobileComponents/MobileHeader";
import MobileFooter from "../../../MobileComponents/MobileFooter";
import ActionBtn from "../../../Buttons/ActionBtn";
import IcFooterArrowIcon from "../../../../assets/svg/IcFooterArrowIcon";
import ScheduleInterview from "../../InterView/ScheduleInterview";
import FlatDefaultBtn from "../../../Buttons/FlatDefaultBtn";
import IcPuls from "../../../../assets/svg/IcPlus";
import "./index.scss";

class InterviewMobile extends React.Component {
  render() {
    return (
      <div className="SelectTemplateMobile">
        <div className="templateMobileHeader">
          <MobileHeader
            headerLeftIcon={
              <IcFooterArrowIcon
                onClick={this.props.onBackClick}
                pathcolor="#797979"
              />
            }
            headerTitle="Create Interview"
          />
        </div>
        <div className="templateMobilemiddleContainer">
          <ScheduleInterview
            onInterviewScheduled={this.props.onInterviewScheduled}
            onCancelClick={this.props.onBackClick}
            parsedTime={this.props.parsedTime}
          />
        </div>
      </div>
    );
  }
}

export default InterviewMobile;
