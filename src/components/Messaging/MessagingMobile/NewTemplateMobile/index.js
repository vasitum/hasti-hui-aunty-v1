import React from "react";
import { Button } from "semantic-ui-react";
import NewTemplateMiddle from "../../Templates/NewTemplateMiddle";
import MobileHeader from "../../../MobileComponents/MobileHeader";
import MobileFooter from "../../../MobileComponents/MobileFooter";
import ActionBtn from "../../../Buttons/ActionBtn";
import IcAttachment from "../../../../assets/svg/IcAttachment";
import FileuploadBtn from "../../../Buttons/FileuploadBtn";
import IcFooterArrowIcon from "../../../../assets/svg/IcFooterArrowIcon";

import templateApi from "../../../../api/messaging/template";
import { USER } from "../../../../constants/api";
import { uploadImage } from "../../../../utils/aws";
import { toast } from "react-toastify";

import "./index.scss";

class NewTemplateMobile extends React.Component {
  state = {
    inputVal: "",
    templText: "",
    attachment: null,
    isUploading: false,
    isCreateClicked: false
  };

  constructor(props) {
    super(props);

    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.onFileUploadAws = this.onFileUploadAws.bind(this);
    this.onAttachmentClick = this.onAttachmentClick.bind(this);
  }

  onTextChange = html => {
    this.setState({ templText: html });
  };

  onInputChange = e => {
    this.setState({ inputVal: e.target.value });
  };

  onUploadComplete = ev => {
    if (!this.state.isCreateClicked) {
      return;
    }

    this.onFormSubmit();
  };

  async onFileUploadAws(file, md) {
    try {
      const userId = window.localStorage.getItem(USER.UID);
      const link = `https://s3-us-west-2.amazonaws.com/img-mwf/`;
      const linkPart = `${userId}/${+new Date()}__${md.name}`;
      const res = await uploadImage(linkPart, file);
      return link + linkPart;
    } catch (error) {
      return error;
    }
  }

  async onAttachmentClick(e) {
    const target = e.target;
    const file = target.files[0];

    if (!file) {
      return;
    }

    // flush the values
    target.value = "";

    this.setState({
      attachment: {
        name: file.name,
        type: file.type,
        size: Math.floor(file.size / 1024)
      },
      isUploading: true
    });

    try {
      const link = await this.onFileUploadAws(file, { name: file.name });
      this.setState({
        attachment: {
          ...this.state.attachment,
          ext: link
        },
        isUploading: false
      });

      this.onUploadComplete();
    } catch (error) {}
  }

  onAttachmentRemoveClick = e => {
    this.setState({
      attachment: null
    });
  };

  async onFormSubmit() {
    // console.log("checked data");
    const { inputVal, templText, attachment, isUploading } = this.state;

    if (isUploading) {
      toast("Uploading ...");
      this.setState({ isCreateClicked: true });
      return;
    }

    try {
      // const sendid = window.localStorage.getItem(USER.UID);
      const res = await templateApi(inputVal, templText, attachment);
      const data = await res.json();
      // console.log("checked data newTemp", data);
      this.props.onCreate(data);
    } catch (error) {
      toast("Error, Unexpected Error Occured");
      console.error(error);
    }
  }

  render() {
    return (
      <div>
        <div className="templateMobileHeader">
          <MobileHeader
            headerLeftIcon={
              <IcFooterArrowIcon
                onClick={this.props.onBackClick}
                pathcolor="#797979"
              />
            }
            headerTitle="Create New Template"
          />
        </div>
        <div className="templateMobilemiddleContainer">
          <NewTemplateMiddle
            name={this.state.inputVal}
            text={this.state.templText}
            attachment={this.state.attachment}
            onAttachmentRemoveClick={this.onAttachmentRemoveClick}
            onInputChange={this.onInputChange}
            onTextChange={this.onTextChange}
          />
        </div>
        <div className="templateMobileFooter">
          <MobileFooter
            headerLeftIcon={
              <ActionBtn onClick={this.onFormSubmit} actioaBtnText="Save" />
            }
            headerTitle={
              <FileuploadBtn
                onFileUpload={this.onAttachmentClick}
                btnText={<IcAttachment pathcolor="#6e768a" />}
              />
            }>
            <Button onClick={this.props.onBackClick}>Cancel</Button>
          </MobileFooter>
        </div>
      </div>
    );
  }
}

export default NewTemplateMobile;
