import React from "react";
import { Button, Image } from "semantic-ui-react";
import NewTemplateMiddle from "../../Templates/NewTemplateMiddle";
import MobileHeader from "../../../MobileComponents/MobileHeader";
import MobileFooter from "../../../MobileComponents/MobileFooter";
import ActionBtn from "../../../Buttons/ActionBtn";
import IcFooterArrowIcon from "../../../../assets/svg/IcFooterArrowIcon";
import ShareImage from "../../../../assets/img/1.jpg";
import FlatDefaultBtn from "../../../Buttons/FlatDefaultBtn";
import IcPuls from "../../../../assets/svg/IcPlus";
import "./index.scss";

class SharedImagePreview extends React.Component {
  render() {
    return (
      <div className="SharedImagePreview">
        <div className="SharedImagePreview_Header">
          <MobileHeader
            headerLeftIcon={<IcFooterArrowIcon pathcolor="#797979" />}
            headerTitle="Ruby"
          />
        </div>

        <div className="SharedImagePreview_img">
          <Image src={ShareImage} />
        </div>
      </div>
    );
  }
}

export default SharedImagePreview;
