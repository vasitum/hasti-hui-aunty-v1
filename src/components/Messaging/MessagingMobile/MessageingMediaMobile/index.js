import React from "react";

import MobileHeader from "../../../MobileComponents/MobileHeader";
import IcFooterArrowIcon from "../../../../assets/svg/IcFooterArrowIcon";

import MediaSubHeader from "../../MediaSection/MediaSubHeader";
import PostLoginMobileFooter from "../../../MobileComponents/PostLoginMobileFooter";
import "./index.scss";

class MessageingMediaMobile extends React.Component {
  render() {
    return (
      <div className="MessageingMediaMobile messagingMobile_Header">
        <MobileHeader
          headerLeftIcon={
            <IcFooterArrowIcon
              onClick={this.props.onBackClick}
              pathcolor="#6e768a"
            />
          }
          headerTitle="Media"
        />

        <div className="mobileMedia_box">
          <MediaSubHeader />
        </div>
      </div>
    );
  }
}

export default MessageingMediaMobile;
