import React from "react";
import PostLoginMobileHeader from "../../../MobileComponents/PostLoginMobileHeader";
import PostLoginMobileFooter from "../../../MobileComponents/PostLoginMobileFooter";
import ChatInitiate from "../../ChatInitiate";
import ChatSideBar from "../../ChatSideBar";

import "./index.scss";

class MessagingFirstPage extends React.Component {
  render() {
    return (
      <div className="MessagingChatInitiate">
        <PostLoginMobileHeader />

        <div className="messaginMobile_chatsideBar">
          <ChatInitiate />
        </div>
        <PostLoginMobileFooter />
      </div>
    );
  }
}

export default MessagingFirstPage;
