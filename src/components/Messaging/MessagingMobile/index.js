import React from "react";

import { Image } from "semantic-ui-react";
import PostLoginMobileHeader from "../../MobileComponents/PostLoginMobileHeader";
import PostLoginMobileFooter from "../../MobileComponents/PostLoginMobileFooter";

import Chatimg from "../../../assets/img/chat.png";

import ChatSideBar from "../ChatSideBar";
import MessagingMobileContainer from "./MessagingMobileContainer";
import { Route, Switch } from "react-router-dom";

import "./index.scss";

class MessagingMobile extends React.Component {
  state = {
    isMediaShow: false
  };

  render() {
    return (
      <div className="MessagingMobile">
        <div className="Messaging_mobileBody">
          <div className="messaginMobile_chatsideBar">
            <Switch>
              <Route exact path="/messaging" component={ChatSideBar} />
              <Route
                path="/messaging/:id"
                component={MessagingMobileContainer}
              />
            </Switch>
          </div>
        </div>

        {/* <PostLoginMobileFooter /> */}
      </div>
    );
  }
}

export default MessagingMobile;
