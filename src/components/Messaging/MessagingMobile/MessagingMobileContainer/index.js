import React, { Component } from "react";
import MessagingOpenChat from "../MessagingOpenChat";
import MessageingMediaMobile from "../MessageingMediaMobile";
import SelectTemplateMobile from "../SelectTemplateMobile";
import NewTemplateMobile from "../NewTemplateMobile";
import TemplateMobileContainer from "../TemplateMobileContainer";
import InterViewMobile from "../InterViewMobile";
import EnterTemplateName from "../EnterTemplateName";

export default class extends Component {
  state = {
    isMediaEnabled: false,
    isTemplateOpen: false,
    isCreateTemplateOpen: false,
    isViewTemplateOpen: false,
    isInterviewOpen: false,
    isNewCreateTemplateOpen: false,
    userDetails: null,
    selectedTemplate: null,
    currentChatId: null,
    parsedTime: null
  };

  onChangeMediaScreen = e => {
    this.setState(state => {
      return {
        ...state,
        isMediaEnabled: !state.isMediaEnabled,
        isTemplateOpen: false,
        isCreateTemplateOpen: false,
        isViewTemplateOpen: false,
        isNewCreateTemplateOpen: false,
        isInterviewOpen: false
      };
    });
  };

  onTemplateTrigger = e => {
    this.setState(state => {
      return {
        ...state,
        isTemplateOpen: !state.isTemplateOpen,
        isMediaEnabled: false,
        isCreateTemplateOpen: false,
        isViewTemplateOpen: false,
        isNewCreateTemplateOpen: false,
        isInterviewOpen: false
      };
    });
  };

  onCreateTemplateTrigger = e => {
    this.setState(state => {
      return {
        ...state,
        isTemplateOpen: false,
        isMediaEnabled: false,
        isNewCreateTemplateOpen: false,
        isViewTemplateOpen: false,
        isInterviewOpen: false,
        isCreateTemplateOpen: !state.isCreateTemplateOpen
      };
    });
  };

  onViewTemplateTrigger = e => {
    this.setState(state => {
      return {
        ...state,
        isCreateTemplateOpen: false,
        isTemplateOpen: false,
        isMediaEnabled: false,
        isNewCreateTemplateOpen: false,
        isInterviewOpen: false,
        isViewTemplateOpen: !state.isViewTemplateOpen
      };
    });
  };

  onInterviewTrigger = (e, parsedTime) => {
    this.setState(state => {
      return {
        ...state,
        isCreateTemplateOpen: false,
        isTemplateOpen: false,
        isMediaEnabled: false,
        isViewTemplateOpen: false,
        isNewCreateTemplateOpen: false,
        isInterviewOpen: !state.isInterviewOpen,
        parsedTime: parsedTime || ""
      };
    });
  };

  onTemplateCreate = template => {
    this.setState({
      isCreateTemplateOpen: false,
      isTemplateOpen: false,
      isMediaEnabled: false,
      isInterviewOpen: false,
      isViewTemplateOpen: true,
      isNewCreateTemplateOpen: false,
      selectedTemplate: template
    });
  };

  onCurrentChatIdChange = id => {
    this.currentChatId = id;
  };

  onOpenUserChat = () => {
    this.setState({
      isMediaEnabled: false,
      isTemplateOpen: false,
      isCreateTemplateOpen: false,
      isViewTemplateOpen: false,
      isInterviewOpen: false,
      isNewCreateTemplateOpen: false
    });
  };

  onUserDetailsUpdate = (id, fName, lName) => {
    // console.log("UserDetailsUpdated" + ` ${id} ${fName} ${lName}`);

    this.setState({
      userDetails: {
        id: id,
        firstName: fName,
        lastName: lName
      }
    });
  };

  // componentDidMount() {
  //   const { match } = this.props;
  // }

  render() {
    if (this.state.isMediaEnabled) {
      return (
        <MessageingMediaMobile
          onBackClick={this.onChangeMediaScreen}
          {...this.props}
        />
      );
    }

    if (this.state.isTemplateOpen) {
      return (
        <SelectTemplateMobile
          onTemplateClick={this.onTemplateCreate}
          onCreateTemplateClick={this.onCreateTemplateTrigger}
          onBackClick={this.onTemplateTrigger}
        />
      );
    }

    if (this.state.isViewTemplateOpen) {
      return (
        <TemplateMobileContainer
          currentChatId={this.currentChatId}
          userDetails={this.state.userDetails}
          template={this.state.selectedTemplate}
          onBackClick={this.onTemplateTrigger}
          onSendClick={this.onOpenUserChat}
        />
      );
    }

    if (this.state.isCreateTemplateOpen) {
      return (
        <NewTemplateMobile
          onCreate={this.onTemplateCreate}
          onBackClick={this.onTemplateTrigger}
        />
      );
    }

    if (this.state.isInterviewOpen) {
      return (
        <InterViewMobile
          onInterviewScheduled={this.onInterviewTrigger}
          parsedTime={this.state.parsedTime}
          onBackClick={this.onInterviewTrigger}
        />
      );
    }

    if (this.state.isNewCreateTemplateOpen) {
      return <EnterTemplateName />;
    }

    return (
      <MessagingOpenChat
        onUserDetailsUpdate={this.onUserDetailsUpdate}
        onInterviewTrigger={this.onInterviewTrigger}
        onMediaTrigger={this.onChangeMediaScreen}
        onTemplateTrigger={this.onTemplateTrigger}
        onChatIdChange={this.onCurrentChatIdChange}
        {...this.props}
      />
    );
  }
}
