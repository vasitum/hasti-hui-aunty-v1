import React from "react";

import { Header, Grid } from "semantic-ui-react";
import MobileHeader from "../../../MobileComponents/MobileHeader";
import IcFooterArrowIcon from "../../../../assets/svg/IcFooterArrowIcon";
import IcEdit from "../../../../assets/svg/IcEdit";
import "./index.scss";

const InterviewStatusMobile = props => {
  return (
    <div className="SelectTemplateMobile">
      <div className="mobileHeader_status">
        <MobileHeader
          headerLeftIcon={<IcFooterArrowIcon pathcolor="#797979" />}
          headerTitle="Select Template"
        />
      </div>
      <div className="InterviewStatusMobile_message">
        <div className="status_header">
          <Grid>
            <Grid.Row>
              <Grid.Column width={12}>
                <div className="header_dateTitle"> July, 07 </div>
                <div className="header_mainTitle">
                  <span>Telephonic interview</span>
                </div>
              </Grid.Column>
              <Grid.Column
                width={4}
                textAlign="right"
                className="header_mainEditColumn">
                <div className="header_mainEdit">
                  <IcEdit pathcolor="#c8c8c8" />
                </div>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>

        <div className="status_body">
          <div className="status_bodyHeader">
            {/* <div className="leftSide_title">When</div>
          <div className="rightSide_title">
            <p>Monday Jul 07, 2018</p>
            <p>11:00am – 11:30am IST</p>
          </div> */}
            <Grid>
              <Grid.Row>
                <Grid.Column width={2}>
                  <div className="leftSide_title">When</div>
                </Grid.Column>
                <Grid.Column width={14}>
                  <div className="rightSide_title">
                    <p>Monday Jul 07, 2018</p>
                    <p>11:00am – 11:30am IST</p>
                  </div>
                </Grid.Column>
              </Grid.Row>
            </Grid>
            <Grid>
              <Grid.Row>
                <Grid.Column width={2}>
                  <div className="leftSide_title">Where</div>
                </Grid.Column>
                <Grid.Column width={14}>
                  <div className="rightSide_title">
                    <p>Call Ruby at +1 732 000 4444</p>
                  </div>
                </Grid.Column>
              </Grid.Row>
            </Grid>
            <Grid>
              <Grid.Row>
                <Grid.Column width={2}>
                  <div className="leftSide_title">Who</div>
                </Grid.Column>
                <Grid.Column width={14}>
                  <div className="rightSide_title">
                    <p>Host You</p>
                    <p>Guest Ruby</p>
                  </div>
                </Grid.Column>
              </Grid.Row>
            </Grid>
            <Grid>
              <Grid.Row>
                <Grid.Column width={2}>
                  <div className="leftSide_title">Status</div>
                </Grid.Column>
                <Grid.Column width={14}>
                  <div className="rightSide_title">
                    <p>Upcoming</p>
                  </div>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </div>
        </div>
      </div>
    </div>
  );
};

export default InterviewStatusMobile;
