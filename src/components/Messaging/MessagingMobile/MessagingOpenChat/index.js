import React from "react";

import MobileHeader from "../../../MobileComponents/MobileHeader";
import IcFooterArrowIcon from "../../../../assets/svg/IcFooterArrowIcon";

import ChatThreadMessageSection from "../../ChatThread/ChatThreadMessageSection";
import MessagingMobileChatField from "../MessagingMobileChatField";

import MoreOption from "../../MoreOption";
import ChatTreadSearchHeader from "../../ChatThread/ChatTreadSearchHeader";
import ChatThreadMessagingContainer from "../../ChatThread/ChatThreadMessagingContainer";

import InputField from "../../../Forms/FormFields/InputField";
import searchMessage from "../../../../api/messaging/searchMessage";
import getUserById from "../../../../api/user/getUserById";

import { toast } from "react-toastify";
import { Button } from "semantic-ui-react";
import { Form } from "formsy-semantic-ui-react";

import "./index.scss";

class MessagingOpenChat extends React.Component {
  state = {
    isSearchOpen: false,
    searchText: "",
    foundMessages: [],
    userName: "",
    userId: ""
  };

  constructor(props) {
    super(props);
    this.chatId = null;

    this.onSearchSubmit = this.onSearchSubmit.bind(this);
  }

  async onSearchSubmit(e) {
    // e.preventDefault();

    if (!this.chatId) {
      console.log("Error: Unable to find chat id");
      return;
    }

    //TODO: Call Search API
    try {
      const res = await searchMessage(this.chatId, this.state.searchText);
      if (res.status === 404) {
        console.log("Messages Not Found");
        toast("Unable to find message");
        return;
      }

      const data = await res.json();
      this.setState({
        foundMessages: data
      });
      // console.log(data);
    } catch (error) {
      toast("UnExpected Error while finding messages");
      console.error(error);
    }
  }

  onChatIdFound = id => {
    console.log("Chat ID Found", id);
    this.chatId = id;
    // console.log(id, "user check");
    if (this.props.onChatIdChange) {
      this.props.onChatIdChange(id);
    }
  };

  onInputChange = (e, { value }) => {
    this.setState({ searchText: value });
  };

  onSearchTrigger = e => {
    this.setState(state => {
      return {
        ...state,
        isSearchOpen: !state.isSearchOpen
      };
    });
  };

  onSearchClose = e => {
    this.setState({
      isSearchOpen: false,
      searchText: "",
      foundMessages: []
    });
  };

  onChangeProfileInfoShow = e => {
    const userId = this.props.match.params.id;
    this.props.history.push(`/user/public/${userId}`);
  };

  async componentDidMount() {
    try {
      const recvId = this.props.match.params.id;
      // console.log("check reciver id", recvId);
      const res = await getUserById(recvId);
      if (res.status === 200) {
        const data = await res.json();
        this.setState({
          userName: `${data.fName} ${data.lName}`,
          userId: data._id
        });
        this.props.onUserDetailsUpdate(data._id, data.fName, data.lName);
      }
    } catch (error) {}
  }

  render() {
    const { messagingOpenChat } = this.props;

    return (
      <div className="MessagingOpenChat messagingMobile_Header">
        {!this.state.isSearchOpen ? (
          <MobileHeader
            mobileHeaderLeftColumn="14"
            mobileHeaderRightColumn="2"
            headerLeftIcon={
              <IcFooterArrowIcon
                onClick={() => this.props.history.go("-1")}
                pathcolor="#6e768a"
              />
            }
            userId={this.state.userId}
            headerTitle={this.state.userName}>
            <MoreOption
              mobile={true}
              onChangeScreen={this.props.onMediaTrigger}
              onChangeSearchField={this.onSearchTrigger}
              onChangeProfileInfoShow={this.onChangeProfileInfoShow}
            />
          </MobileHeader>
        ) : (
          <ChatTreadSearchHeader
            isSearchFieldHeader
            headerLeftIcon={
              <Button className="searchFieldBtn" onClick={this.onSearchClose}>
                <IcFooterArrowIcon pathcolor="#6e768a" />
              </Button>
            }
            headerTitle={
              <Form onSubmit={this.onSearchSubmit}>
                <InputField
                  name="search"
                  value={this.state.searchText}
                  onChange={this.onInputChange}
                  placeholder="Search"
                />
                <Button type="submit" className="is-hidden" hidden />
              </Form>
            }
          />
        )}

        {/* <ChatTreadSearchHeader /> */}

        <div className="messaginMobile_chatsideBar">
          <ChatThreadMessagingContainer
            foundMessages={this.state.foundMessages}
            onChatIdFound={this.onChatIdFound}
            onTemplateTrigger={this.props.onTemplateTrigger}
            onShowInterviewScreen={this.props.onInterviewTrigger}
          />
        </div>
        {/* <MessagingMobileChatField /> */}
      </div>
    );
  }
}

export default MessagingOpenChat;
