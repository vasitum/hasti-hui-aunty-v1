import React from "react";
import PostLoginMobileHeader from "../../../MobileComponents/PostLoginMobileHeader";
import PostLoginMobileFooter from "../../../MobileComponents/PostLoginMobileFooter";

import ChatSideBar from "../../ChatSideBar";

import "./index.scss";

class MessagingChatInitiate extends React.Component {
  render() {
    return (
      <div className="MessagingChatInitiate">
        <PostLoginMobileHeader />

        <div className="messaginMobile_chatsideBar">
          <ChatSideBar />
        </div>
        <PostLoginMobileFooter />
      </div>
    );
  }
}

export default MessagingChatInitiate;
