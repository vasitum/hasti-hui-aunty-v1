import React from "react";
import { Button, Popup, List, Icon, Image } from "semantic-ui-react";
import ClipboardJS from "clipboard";
import IcLinkedinIcon from "../../assets/svg/IcLinkedin";
import facebookIc from "../../assets/img/facebook.png";
import google from "../../assets/img/google.png";
import CopyIc from "../../assets/img/copy.png";
import Twitter from "../../assets/img/twitter.png";
import "./index.scss";

function onFacebookClick(link) {}

function onLinkedInClick(link) {
  window.open(
    "https://www.linkedin.com/shareArticle?mini=true&url=" + link,
    "",
    "width=500,height=500"
  );
}

function onTwitterClick(link) {
  window.open(
    "https://twitter.com/share?url=" +
      link +
      "&text=" +
      encodeURIComponent("Look we found a job for you in Vasitum") +
      "&via=" +
      "vasitum" +
      "&hashtags=" +
      "vasitum",
    "",
    "width=500,height=500"
  );
}

function onCopyLinkClick(link) {
  toast("link copied");
}

const ShareButton = ({ trigger, link }) => (
  <Popup trigger={trigger} flowing hoverable>
    <List className="Button_main" relaxed="6">
      <List.Item
        as="a"
        onClick={() => onFacebookClick(link)}
        className="Sub_Button">
        <List.Icon>
          <Image src={facebookIc} />
        </List.Icon>
        <List.Content className="Button_content">Facebook</List.Content>
      </List.Item>

      <List.Item
        as="a"
        onClick={() => onTwitterClick(link)}
        className="Sub_Button">
        <List.Icon>
          <Image src={Twitter} />
        </List.Icon>
        <List.Content className="Button_content">Twitter</List.Content>
      </List.Item>

      <List.Item
        as="a"
        onClick={() => onLinkedInClick(link)}
        className="Sub_Button">
        <List.Icon>
          <IcLinkedinIcon />
        </List.Icon>
        <List.Content className="Button_content">Linkedin</List.Content>
      </List.Item>

      <List.Item
        as="a"
        onClick={() => onCopyLinkClick(link)}
        className="Sub_Button">
        <List.Icon>
          <Image src={CopyIc} />
        </List.Icon>
        <List.Content className="Button_content">Copy Link</List.Content>
      </List.Item>
    </List>
  </Popup>
);

export default ShareButton;
