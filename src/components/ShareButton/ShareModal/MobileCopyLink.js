import React, { Component } from "react";
import { Responsive, Icon } from "semantic-ui-react";
import { CopyToClipboard } from "react-copy-to-clipboard";
import "./index.scss";
export default class MobileCopyLink extends Component {
  render() {
    return (
      <div>
        <Responsive maxWidth={1024}>
          <div className="mobile-link-button hidden-desktop">
            <CopyToClipboard text={this.props.input} onCopy={this.props.onCopy}>
              <span style={{ marginLeft: "5px" }}>
                <Icon name="linkify" />
              </span>
              <span style={{ marginLeft: "10px" }}>Copy link</span>
            </CopyToClipboard>
            {/* {this.props.copied ? (
                <span style={{ color: "red" }}>Copied.</span>
              ) : null} */}
          </div>
        </Responsive>
      </div>
    );
  }
}
