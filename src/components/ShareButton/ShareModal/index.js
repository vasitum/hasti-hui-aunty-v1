import React, { Component } from "react";
import { Modal, Icon, Button, Form, Responsive } from "semantic-ui-react";
import "./index.scss";
import FB from "../../../assets/svg/IcShareFacebook";
import LinkedIn from "../../../assets/svg/IcShareLinkedin";
import Twitter from "../../../assets/svg/IcShareTwitter";
import BorderButton from "../../Buttons/BorderButton";
import { USER } from "../../../constants/api";
import { CopyToClipboard } from "react-copy-to-clipboard";
// import CopyLink from "./MobileCopyLink";
export default class ShareModal extends Component {
  state = {
    baseUrl: "https://vasitum.com",
    modalOpen: false,
    linkedinTitle: "Vasitum Job",
    linkedinSummary: "Vasitum Summary",
    twitterFeed: "Vasitum Job",
    host: window.location.host,
    copied: false,
    value: `${window.location.host + this.props.url}}`
  };

  onCopy() {
    this.setState({ copied: true });
  }

  render() {
    const {
      host,
      copied,
      baseUrl,
      linkedinTitle,
      linkedinSummary,
      twitterFeed
    } = this.state;
    const { url, copyUrl } = this.props;
    let urlInput = baseUrl + url;
    let newUrl = encodeURIComponent(baseUrl + url);
    let newLinkedinTitle = encodeURIComponent(linkedinTitle);
    let newLinkedinSummary = encodeURIComponent(linkedinSummary);
    let newTwitterFeed = encodeURIComponent(twitterFeed);
    return (
      <React.Fragment>
        <Modal
          open={this.props.modalOpen}
          onClose={this.props.handleModal}
          className="user-share-modal-block"
          size="tiny">
          <Modal.Content>
            <div className="cross-button">
              <Icon
                name="close"
                className="cross-icon"
                onClick={this.props.handleModal}
              />
            </div>
            <div className="modal-title">Share a link</div>
            <div className="item-card-popup">
              <div className="flex-button">
                <Button
                  circular
                  icon={<Twitter pathcolor="#1da1f2" width="20" height="17" />}
                  className="share-button twitter-share"
                  href={`https://twitter.com/share?text=${newTwitterFeed}&url=${newUrl}`}
                  target="_blank"
                />
                <div className="share-font">Twitter</div>
              </div>
              <div className="flex-button">
                <Button
                  circular
                  icon={<FB pathcolor="#3b5998" width="10" height="22" />}
                  className="share-button facebook-share mt--3"
                  href={`http://www.facebook.com/sharer.php?u=${newUrl}`}
                  target="_blank"
                />
                <div className="share-font">Facebook</div>
              </div>
              <div className="flex-button">
                <Button
                  circular
                  icon={<LinkedIn pathcolor="#0077b5" width="16" height="15" />}
                  className="share-button linkedin-share"
                  href={`https://www.linkedin.com/shareArticle?mini=true&url=${newUrl}&title=${newLinkedinTitle}&summary=${newLinkedinSummary}`}
                  target="_blank"
                />
                <div className="share-font">Linkedin</div>
              </div>
            </div>
            <div className="hidden-mobile">
              <div className="share-form-area">
                <div>
                  <Form.Input
                    fluid
                    value={urlInput}
                    placeholder="public link"
                    style={{ width: "250px" }}
                    readOnly
                  />
                </div>
                <div>
                  <CopyToClipboard
                    text={urlInput}
                    onCopy={() => this.setState({ copied: true })}>
                    <BorderButton
                      className="btn btn-border ml-10"
                      btnText={this.state.copied ? "Copied" : "Copy link"}
                    />
                  </CopyToClipboard>
                </div>
              </div>
            </div>
            {/* <CopyLink
              input={this.state.input}
              onCopy={copied}
              copy={this.state.copied}
            /> */}
            <Responsive maxWidth={1024}>
              <div className="mobile-link-button hidden-desktop">
                <CopyToClipboard
                  text={urlInput}
                  onCopy={() => this.setState({ copied: true })}>
                  <span>
                    <span style={{ marginLeft: "5px" }}>
                      <Icon name="linkify" />
                    </span>
                    <span style={{ marginLeft: "10px" }}>
                      {/* Copy link */}
                      {this.state.copied ? "Copied" : "Copy link"}
                    </span>
                  </span>
                </CopyToClipboard>
              </div>
            </Responsive>
          </Modal.Content>
        </Modal>
      </React.Fragment>
    );
  }
}
