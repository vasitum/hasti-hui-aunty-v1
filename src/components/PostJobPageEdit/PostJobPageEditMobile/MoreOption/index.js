import React from "react";

import { Grid, Dropdown } from "semantic-ui-react";
import IcMoreOptionIcon from "../../../../assets/svg/IcMoreOptionIcon";

// import "./index.scss";

const MoreOptionsTrigger = (
  <span>
    <IcMoreOptionIcon pathcolor="#acaeb5" />
  </span>
);

class MoreOption extends React.Component {
  getItems = isMobile => {
    return [
      {
        text: "Preview Job",
        key: "Preview Job ",
        onClick: () => this.props.onPreviewClick()
      },
      // {
      //   text: "Save as draft",
      //   key: "Save as draft ",
      //   // onClick: () => this.props.onChangeProfileInfoShow()
      // },
      
    ];
  };

  render() {
    const { ...resProps } = this.props;
    return (
      <div className="Main_Dropdown" {...resProps}>
        <Dropdown
          trigger={MoreOptionsTrigger}
          options={this.getItems()}
          icon={null}
          pointing="right"
        />
      </div>
    );
  }
}

export default MoreOption;