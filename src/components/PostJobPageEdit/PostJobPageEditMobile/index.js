import React from "react";
import { Button } from "semantic-ui-react";
import MobileHeader from "../../MobileComponents/MobileHeader";

import MobileFooter from "../../MobileComponents/MobileFooter";

import BackArrowBtnLeft from "../../Buttons/BackArrowBtnLeft";

import ActionBtn from "../../Buttons/ActionBtn";

import PostJobPageEditContainer from "../PostJobPageEditContainer";

import MoreOption from "./MoreOption";

import "./index.scss";

class PostJobPageEditMobile extends React.Component{
  render(){
    const { data, onPrev, onPreviewClick } = this.props;

    return(
      <div className="PostJobPageEditMobile">
        <div className="PageEditMobile_header">
          <MobileHeader 
            className="isMobileHeader_fixe" 
            headerLeftIcon={<BackArrowBtnLeft onClick={onPrev} />}
            headerTitle="Edit job"
          >
            <MoreOption onPreviewClick={onPreviewClick} />
          </MobileHeader>
        </div>
        <div className="PageEditMobile_body">
          <PostJobPageEditContainer />
        </div>
        {/* <div className="PageEditMobile_footer">
          <MobileFooter 
            className="isMobileFooter_fixe"
            headerLeftIcon={<ActionBtn actioaBtnText="Save"/>}
          >
            <div className="footerBtnPreview">
              <Button>Preview job</Button>
            </div>
          </MobileFooter>
        </div> */}
      </div>
    )
  }
}

export default PostJobPageEditMobile;