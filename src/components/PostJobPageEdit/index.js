import React from "react";

import { Responsive } from "semantic-ui-react";

import PostJobPageEditContainer from "./PostJobPageEditContainer";

import PostJobPageEditMobile from "./PostJobPageEditMobile";

import "./index.scss";

class PostJobPageEdit extends React.Component {
  render() {
    const {onPreviewClick} = this.props;
    return (
      <div className="PostJobPageEdit">
        <Responsive maxWidth={1024}>
          <PostJobPageEditMobile onPreviewClick={onPreviewClick}/>
        </Responsive>
        <Responsive minWidth={1025}>
          <PostJobPageEditContainer />
        </Responsive>
      </div>
    );
  }
}

export default PostJobPageEdit;
