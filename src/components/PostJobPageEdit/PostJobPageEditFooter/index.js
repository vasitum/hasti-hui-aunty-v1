import React from "react";

import FlatDefaultBtn from "../../Buttons/FlatDefaultBtn";

import ActionBtn from "../../Buttons/ActionBtn";
import IcProfileViewIcon from "../../../assets/svg/IcProfileView";

import IcDownArrow from "../../../assets/svg/IcDownArrow";

import "./index.scss";

const PostJobPageEditFooter = () => {
  return (
    <div className="PostJobPageEditFooter">
      <FlatDefaultBtn
        btnicon={
          <IcProfileViewIcon height="12" pathcolor="#c8c8c8" />
        }
        btntext="Preview Job"
        type="button"
        
        style={{
          paddingLeft: "2px",
          paddingRight: "15px"
        }}
      />
      <div className="actionBtnFooter">
        <ActionBtn
         
          type="submit"
          actioaBtnText="Save"
          btnIcon={<IcDownArrow pathcolor="#99d9f9" />}
        />
        
      </div>
    </div>
  )
}

export default PostJobPageEditFooter;