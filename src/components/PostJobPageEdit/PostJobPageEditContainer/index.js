import React from "react";

// import PostJobEditHeader from "../PostJobEditHeader";
import { Container } from "semantic-ui-react";

import BannerHeader from "../BannerHeader";

import BannerImage from "../../../assets/banners/job_cover_img.jpg";

import PageBanner from "../../Banners/PageBanner";
import EditJob from "../../ModalPageSection/EditJob";

import InterviewPreferenceContainer from "../../InterviewScreening/InterviewPreference/InterviewPreferenceContainer";
import ScreeningQuestionsContainer from "../../InterviewScreening/ReviewScreeningQuestions/ScreeningQuestionsContainer";

import InfoSection from "../../Sections/InfoSection";

import DownArrowCollaps from "../../utils/DownArrowCollaps";
import updateImgBanner from "../../../api/jobs/updateImgBanner";
import { uploadImage } from "../../../utils/aws";

import { toast } from "react-toastify";

import PostJobPageEditFooter from "../PostJobPageEditFooter";

import PageLoader from "../../PageLoader";
import { EDIT_JOB } from "../../../strings";

import "./index.scss";

function hasBanner(data) {
  const res = {};

  if (data && data.imgBanner) {
    (res["extn"] = "jpg"),
      (res["file"] = `https://s3-us-west-2.amazonaws.com/img-mwf/banner/${
        data._id
      }/image.png`);
  } else {
    (res["extn"] = ""), (res["file"] = "");
  }

  return res;
}

class PostJobPageEditContainer extends React.Component {
  state = {
    jobdata: {},
    isModalOpen: false,
    bannerImageExtn: "",
    bannerImageFile: "",
    bannerImageUpdateFile: "",
    isPageLoading: false
  };

  constructor(props) {
    super(props);

    // autobind
    this.onBannerRemove = this.onBannerRemove.bind(this);
    this.onBannerSave = this.onBannerSave.bind(this);
  }

  toggleBanner = () => {
    this.setState({
      isPageLoading: !this.state.isPageLoading
    });
  };

  onBannerChange = ev => {
    const file = ev.target.files[0];

    // reset input
    ev.target.value = "";

    if (!file) {
      return;
    }

    const fr = new FileReader();

    fr.onloadend = e => {
      this.setState({
        bannerImageFile: e.target.result,
        bannerImageUpdateFile: file
      });
    };

    fr.readAsDataURL(file);
  };

  async onBannerRemove(e) {
    this.setState(
      {
        bannerImageExtn: "",
        bannerImageFile: "",
        bannerImageUpdateFile: "",
        isModalOpen: false
      },
      async () => {
        try {
          const res = await updateImgBanner(this.state.jobdata._id, null);
          if (res.status !== 200) {
            toast("unable to remove banner");
            return;
          }

          toast("Banner Removed!");
        } catch (e) {
          console.log(e);
          toast("unable to remove banner!");
        }
      }
    );
  }

  async onBannerSave(e) {
    // set form loading
    this.toggleBanner();
    try {
      const res = await uploadImage(
        `banner/${this.state.jobdata._id}/image.png`,
        this.state.bannerImageUpdateFile
      );

      const data = await updateImgBanner(this.state.jobdata._id, "jpg");

      if (res && data.status === 200) {
        toast("Banner Updated!");
        window.location.reload();
      }
    } catch (E) {
      // this.toggleBanner();
      console.log(E);
    }
  }

  onBannerModalOpen = e => {
    this.setState({
      isModalOpen: true
    });
  };

  onBannerModalClose = e => {
    this.setState({
      bannerImageExtn: "",
      bannerImageFile: "",
      bannerImageUpdateFile: "",
      isModalOpen: false
    });
  };

  onEditDataLoaded = jobdata => {
    const banner = hasBanner(jobdata);

    this.setState({
      jobdata: jobdata,
      bannerImageExtn: banner.extn,
      bannerImageFile: banner.file
    });
  };

  render() {
    const { onPreviewClick } = this.props;
    return (
      <div className="PostJobPageEditContainer">
        <div className="EditContainerHeader">
          <PageBanner
            size="medium"
            image={
              this.state.bannerImageFile
                ? this.state.bannerImageFile
                : BannerImage
            }>
            <Container>
              <BannerHeader
                hasDefaultBanner={this.state.bannerImageFile ? false : true}
                bannerImageFile={
                  this.state.bannerImageFile
                    ? this.state.bannerImageFile
                    : BannerImage
                }
                // onFileRemove={this.onFileRemove}
                // onFileChange={this.onFileChange}
                isModalOpen={this.state.isModalOpen}
                onOpen={this.onBannerModalOpen}
                onClose={this.onBannerModalClose}
                onBannerChange={this.onBannerChange} //this.onBannerChange
                onBannerRemove={this.onBannerRemove}
                onBannerSave={this.onBannerSave}
              />
            </Container>
          </PageBanner>
        </div>

        <div className="EditContainerBody">
          <EditJob
            isShowModalBanner
            onPreviewClick={onPreviewClick}
            onEditDataLoaded={this.onEditDataLoaded}
          />

          {/* <Container >
            <div className="EditScreeningQuestions">
              <InfoSection
                headerSize="large"
                color="blue"
                headerText="Screening questions"
                rightIcon={<DownArrowCollaps />}
                collapsible
                isActive={true}>
                <ScreeningQuestionsContainer
                  isShowQuestionsFooter
                  isShowHeader
                />
              </InfoSection>
            </div>

            <div className="EditInterviewPreference">
              <InfoSection
                headerSize="large"
                color="blue"
                headerText={<EditInterviewHeader />}
                rightIcon={<DownArrowCollaps />}
                collapsible
                isActive={true}>
                <InterviewPreferenceContainer 
                  isShowPrefenceFooter
                  headerTitle
                />
              </InfoSection>
            </div>

            <PostJobPageEditFooter />
          </Container> */}
        </div>
        <PageLoader
          active={this.state.isPageLoading}
          loaderText={"Updating Job"}
        />
      </div>
    );
  }
}

export default PostJobPageEditContainer;
