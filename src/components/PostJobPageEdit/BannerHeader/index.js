import React from "react";

import { Grid, Header, Button, Modal } from "semantic-ui-react";
import PropTypes from "prop-types";
import FileuploadBtn from "../../Buttons/FileuploadBtn";
import IcUploadIcon from "../../../assets/svg/IcUpload";

import InputFieldLabel from "../../Forms/FormFields/InputFieldLabel";
import RemoveBtn from "../../Buttons/RemoveBtn";
import IcRemove from "../../../assets/svg/IcRemove";

import BannerFileBtn from "../../Buttons/BannerFileBtn";

import EditCoverPage from "../../ModalPageSection/EditCoverPage";

import InfoIconLabel from "../../utils/InfoIconLabel";

import { EDIT_JOB } from "../../../strings";

// import "./index.scss";
// const infoicon = <IcInfoIcon pathcolor="#c8c8c8" />;
const BannerHeader = ({
  hasDefaultBanner,
  bannerImageFile,
  onBannerChange,
  onBannerRemove,
  onBannerSave,
  isModalOpen,
  onOpen,
  onClose,
  ...props
}) => {
  return (
    <div className="BannerHeader">
      <Grid>
        <Grid.Row className="BannerHeader_title">
          <Grid.Column width={12}>
            <Header as="h3">{EDIT_JOB.PAGE_TITLE}</Header>
          </Grid.Column>
          <Grid.Column width={4} className="mobileGrid_bannerHeader">
            <Modal
              className="modal_form"
              open={isModalOpen}
              trigger={
                <div className="bannerBtn_section">
                  <BannerFileBtn
                    onClick={onOpen}
                    className="fileuploadFlatBtn"
                    btnText={
                      props.hasDefaultBanner ? "Update" : "Upload cover image"
                    }
                    // onFileUpload={props.onFileChange}
                    accept=".jpg, .jpeg, .png"
                    btnIcon={<IcUploadIcon pathcolor="#c8c8c8" />}
                  />
                </div>
              }
              onClose={onClose}
              closeIcon
              closeOnDimmerClick={false}>
              <EditCoverPage
                bannerFile={bannerImageFile}
                onBannerChange={onBannerChange}
                onBannerRemove={onBannerRemove}
                onBannerSave={onBannerSave}
              />
            </Modal>

            {/* <div className="bannerBtn_section">
              {props.bannerImageFile && (
                <div className="removeBtn">
                  <RemoveBtn
                    onClick={props.onFileRemove}
                    className="fileuploadFlatBtn"
                    btnText="Remove"
                    style={{ marginRight: "10px" }}
                    accept=".jpg, .jpeg, .png"
                    btnIcon={<IcRemove pathcolor="#c8c8c8" />}
                  />
                </div>
              )}

              <FileuploadBtn
                className="fileuploadFlatBtn"
                btnText={
                  props.bannerImageFile ? "Update" : "Upload cover image"
                }
                onFileUpload={props.onFileChange}
                accept=".jpg, .jpeg, .png"
                btnIcon={<IcUploadIcon pathcolor="#c8c8c8" />}
              />
            </div> */}

            <InputFieldLabel
              // headerStyle={{
              //   marginRight: "20px"
              // }}
              className="Infoicon"
              label="File specifications"
              infoicon={<InfoIconLabel />}
            />
          </Grid.Column>
        </Grid.Row>

        {/* <Grid.Row>
          <Grid.Column width={12} className="mobile hidden" />
          <Grid.Column
            mobile={16}
            computer={4}
            textAlign="right"
            className="mobileGrid_bannerHeader">
            
          </Grid.Column>
        </Grid.Row> */}
      </Grid>
    </div>
  );
};

BannerHeader.propTypes = {};

export default BannerHeader;
