import React from 'react'
import { Grid, Image, Checkbox, Dropdown, Button, List } from 'semantic-ui-react'
import ICFilter from "../../assets/svg/IcFilterIcon";
import "./index.scss";

const InterviewOption = [
  {
    text: 'All',
    value: 'All',
  },
  {
    text: 'Today',
    value: 'Today',
  },
  {
    text: 'Tomorrow',
    value: 'Tomorrow',
  },
  {
    text: 'This week',
    value: 'This week',
  },
  {
    text: 'Custom date',
    value: 'Custom date',
  }
]

const Submitted = [
  {
    text: 'Today',
    value: 'Today',
  },
  {
    text: 'Last 7 days',
    value: 'Last 7 days',
  },
  {
    text: 'Custom date',
    value: 'Custom date',
  },
  {
    text: 'Anytime',
    value: 'Anytime',
  }
]

const Moretext = [
  {
    text: 'More',
    value: 'More',
  },
  {
    text: 'Rejected applicant',
    value: 'Rejected applicant',
  },
  {
    text: 'Tag',
    value: 'Tag',
  }
]

const SortBy = [
  {
    text: 'Latest',
    value: 'Latest',
  },
  {
    text: 'Relevent',
    value: 'Relevent',
  }
]

const FilterBar = () => (
  <div className="Filter_Main">
    <Grid>
      <Grid.Row>
        <Grid.Column width="12">
          <List horizontal className="List_Main" >
            <List.Item>
              <Checkbox label='CV available' />
              <List.Content>

              </List.Content>
            </List.Item>
            <List.Item className="selectItem">
              <span>
                Interview:{' '}
                <Dropdown inline options={InterviewOption} defaultValue={InterviewOption[0].value} />
              </span>
              <List.Content>

              </List.Content>
            </List.Item>
            <List.Item className="selectItem">
              <span>
                Submitted:{' '}
                <Dropdown inline options={Submitted} defaultValue={Submitted[3].value} />
              </span>
              <List.Content>

              </List.Content>
            </List.Item>
            <List.Item className="selectItem_More">
              <span>
                {' '}
                <Dropdown inline options={Moretext} defaultValue={Moretext[0].value} />
              </span>
              <List.Content>

              </List.Content>
            </List.Item>
          </List>
        </Grid.Column>
        <Grid.Column width="4" textAlign="right">
          <List className="List_Main">
            <List.Item className="Filter_Sort" >
              <List.Content>
                <span>
                  Sort by: {' '}
                  <Dropdown inline options={SortBy} defaultValue={SortBy[0].value} />
                </span>
              </List.Content>
            </List.Item>
          </List>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  </div>
)

export default FilterBar;