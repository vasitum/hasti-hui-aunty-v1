import React from "react";

import { List, Button } from "semantic-ui-react";

const processSkillsOptional = ({ skills }) => {
  if (!skills) {
    return null;
  }

  const skillsName = skills.map(val => {
    return {
      name: val.name,
      exp: val.exp
    };
  });

  const skillComponent = skillsName.map(val => {
    if (!val) {
      return;
    }

    return (
      <div className="filterSkill skillBtn" style={{ display: "inline-block" }}>
        <List horizontal className="text-night-rider">
          <List.Item key={val.name}>
            <Button compact>
              {val.exp ? (
                <React.Fragment>
                  {val.name} |<span> {val.exp} yrs</span>
                </React.Fragment>
              ) : (
                <React.Fragment>{val.name}</React.Fragment>
              )}
            </Button>
          </List.Item>
        </List>
      </div>
    );
  });

  return skillComponent;
};

export default processSkillsOptional;
