import React from "react";

import { Grid, Header } from "semantic-ui-react";

import InfoSection from "../../Sections/InfoSection";

import CompanySvg from "../../../assets/svg/IcCompany";

import PropTypes from "prop-types";

import "./index.scss";

const Experience = props => {
  const { companyName, title, subtitle } = props;

  return (
    <div className="profileCardExperience">
      <InfoSection headerSize="small" headerText="Experience">
        <Grid>
          <Grid.Row className="experienceGridRow">
            <Grid.Column mobile={3} computer={2}>
              <div>
                <CompanySvg
                  width="40"
                  height="40"
                  rectcolor="#f7f7fb"
                  pathcolor="#c8c8c8"
                  circleColor="#f7f7fb"
                />
              </div>
            </Grid.Column>
            <Grid.Column mobile={13} computer={14}>
              <div className="experienceTitle">
                <Header>Java developer</Header>
                <p className="title">{title}</p>
                <p className="subTitle">{subtitle}</p>
              </div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </InfoSection>
    </div>
  );
};

Experience.propTypes = {
  placeholder: PropTypes.string,
  readOnly: PropTypes.bool
};

Experience.defaultProps = {
  placeholder: "Describe the position and its role within your company",
  readOnly: false
};

export default Experience;
