import React from "react";

import { Image, Header } from "semantic-ui-react";

import CompanyLogo from "../../../assets/img/wipro.png";
import EducationLogo from "../../../assets/svg/IcEducation";

import QuillText from "../QuillText";

import "./index.scss";

const CondidatesEducation = ({ education }) => {
  if (!education || !Array.isArray(education)) {
    return;
  }

  return (
    <div className="CondidatesEducation">
      {education.map(edu => {
        return (
          <div className="CandidateExperience">
            <div className="company_logo">
              {/* <Image src={CompanyLogo} /> */}
              <EducationLogo
                width="40"
                height="40"
                rectcolor="#f7f7fb"
                pathcolor="#c8c8c8"
              />
            </div>
            <div className="experienceDetail">
              <Header as="h4">{edu.institute}</Header>
              {/* <p className="companyTitle">Maven Workforce Pvt. Ltd</p> */}
              <p className="dateTime">
                {edu.level} | {edu.course}
              </p>
              <p className="location">
                {edu.started} - {edu.completed}
              </p>

              {/* <div>
                <QuillText
                  value={edu.}
                  readOnly
                />
              </div> */}
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default CondidatesEducation;
