import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";

import Skills from "./Skills";

import "./index.scss";

const SkillList = ({ skills, small }) => {
  const classes = cx({
    SkillList: true,
    "is-small": small
  });

  if (small) {
    return (
      <div className={classes}>
        <span>Skills: </span>
        <Skills skills={skills} />
      </div>
    );
  }

  return (
    <div className={classes}>
      <Skills skills={skills} />
    </div>
  );
};

SkillList.propTypes = {
  skills: PropTypes.arrayOf(PropTypes.object)
};

SkillList.defaultProps = {
  skills: [
    {
      key: 1,
      skill: "Java"
    },
    {
      key: 2,
      skill: "Photoshop"
    },
    {
      key: 3,
      skill: "Scala"
    }
  ]
};

export default SkillList;
