import React from "react";
import PropTypes from "prop-types";
import { List } from "semantic-ui-react";

const Skills = ({ skills }) => {
  return (
    <List horizontal bulleted>
      {skills.map(val => {
        return <List.Item key={val.key}>{val.name}</List.Item>;
      })}
    </List>
  );
};

Skills.propTypes = {
  skills: PropTypes.arrayOf(PropTypes.object)
};

export default Skills;
