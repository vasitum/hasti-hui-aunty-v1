import React from "react";
import PropTypes from "prop-types";
import { Header, Grid, Button } from "semantic-ui-react";

import "./index.scss";

const InfoSectionGridHeader = props => {
  const {
    headerClasses,
    color,
    headerText,
    children,
    containerClass,
    onEditIconClick,
    editIcon,
    subHeaderText,
    activeSection,
    style
  } = props;

  // Our svgs are react elements hence needs captialization

  return (
    <div className={containerClass + " InfoSectionGridHeader"} style={style}>
      <div className="header-container">
        <Grid>
          <Grid.Row>
            <Grid.Column width="10">
              <Header
                className={headerClasses + " header-item"}
                color={color}
                as="h2">
                {headerText}
                {subHeaderText}
              </Header>
            </Grid.Column>
            <Grid.Column width="6" textAlign="right">
              <Button
                onClick={onEditIconClick}
                activeSection={activeSection}
                compact
                className="header_editBtn">
                {editIcon}
              </Button>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
      <div>{children}</div>
    </div>
  );
};

// infoSectionGridHeader.propTypes = {};

// infoSectionGridHeader.defaultProps = {};

export default InfoSectionGridHeader;
