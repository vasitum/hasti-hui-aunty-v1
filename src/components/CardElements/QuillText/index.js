import React, { Component } from "react";
import PropTypes from "prop-types";
import ReactQuill from "react-quill";
import cx from "classnames";

import "react-quill/dist/quill.snow.css";
import "./index.scss";

// shrink qill
function shrinkQuill(val, length) {
  if (!val) {
    return "";
  }

  if (val.length < length) {
    return val;
  }

  const div = (document.createElement("div").innerHTML = val);
  return div.substring(0, length) + "<span>...</span>";
}

// quill options
const modules = {
  toolbar: [
    ["bold", "italic", "underline"],
    [{ list: "ordered" }, { list: "bullet" }]
  ],
  clipboard: {
    matchVisuals: false
  }
};

// whitelist of allowed formats
// for complete list: https://quilljs.com/docs/formats/
const formats = ["bold", "italic", "underline", "list"];

class QuillText extends React.Component {
  constructor(props) {
    super(props);
  }

  state = {
    seeMore: false
  };

  onSeeMoreChange = e => {
    this.setState({
      seeMore: !this.state.seeMore
    });
  };

  componentDidMount() {}

  render() {
    const {
      hidden,
      placeholder,
      bgBase,
      isMute,
      readOnly,
      isSmall,
      isDoubleDown,
      isMessagebox,
      isMessageText,
      noReadMore,
      readMoreLength,
      readMoreMargin,
      value,
      ...props
    } = this.props;

    const { seeMore } = this.state;

    const classes = cx({
      QuillText: true,
      [`is-hidden`]: hidden,
      [`is-readonly`]: readOnly,
      [`is-small`]: isSmall,
      [`is-base`]: bgBase,
      [`is-mute`]: isMute,
      [`is-doubleDown`]: isDoubleDown,
      [`is-Messagebox`]: isMessagebox,
      [`is-MessageText`]: isMessageText
    });

    const readMoreLengthCheck = readMoreLength ? readMoreLength : 200;

    return (
      <div className={classes}>
        {readOnly ? (
          <React.Fragment>
            <ReactQuill
              theme="snow"
              readOnly={readOnly}
              modules={modules}
              formats={formats}
              placeholder={placeholder}
              value={!seeMore ? shrinkQuill(value, readMoreLengthCheck) : value}
              {...props}
            />
            {value && !noReadMore && value.length > readMoreLengthCheck ? (
              <div
                style={{
                  marginBottom: readMoreMargin ? readMoreMargin : "10px"
                }}>
                <span
                  onClick={this.onSeeMoreChange}
                  style={{
                    cursor: "pointer",
                    textDecoration: "underline"
                  }}>
                  {seeMore ? "See Less" : "See More"}
                </span>
              </div>
            ) : null}
          </React.Fragment>
        ) : (
          <ReactQuill
            theme="snow"
            readOnly={readOnly}
            modules={modules}
            formats={formats}
            placeholder={placeholder}
            value={value}
            {...props}
          />
        )}
      </div>
    );
  }
}

QuillText.propTypes = {
  placeholder: PropTypes.string,
  readOnly: PropTypes.bool
};

QuillText.defaultProps = {
  placeholder: "Describe the position and its role within your company",
  readOnly: false
};

export default QuillText;
