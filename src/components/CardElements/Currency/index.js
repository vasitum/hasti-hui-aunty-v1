import React from "react";

import {Icon} from "semantic-ui-react";

import "./index.scss";

const currency = {
  inr: "INR",
  eur: "EUR",
  usd: "USD",
  jpy: "JPY",
  cny: "CNY"
}

const Currency = props => {
  const { data } = props;
  if(!data){
    return null
  }
  return (
    <span className="Currency_filter">
      {(salCurrency => {
        switch (salCurrency) {
          case currency.inr:
            return (
              <Icon name='inr' />
            );
            break;

          case currency.eur:
            return (
              <Icon name='eur' />
            )
            break;

          case currency.usd:
            return (
              <Icon name='usd' />
            )
            break;

          case currency.jpy:
            return (
              <Icon name='jpy' />
            )
            break;

          case currency.cny:
            return (
              <Icon name='cny' />
            )
            break;

          default:
            return (
              <Icon name='inr' />
            );
            break;
        }
      })(data.salary ? data.salary.currency : "")}
    </span>
  )
}

export default Currency;