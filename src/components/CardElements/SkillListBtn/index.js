import React from "react";
import { List, Button } from "semantic-ui-react";
import InfoSectionGridHeader from "../InfoSectionGridHeader";

// const skills = [
//   {

//   }
// ]

const processSkills = skills => {
  if (!skills) {
    return null;
  }

  const skillsName = skills.map(val => {
    if (val.reqType !== "optional") {
      return {
        name: val.name,
        exp: val.exp
      };
    }
  });

  const skillComponent = skillsName.map(val => {
    if (!val) {
      return;
    }

    return (
      <div className="filterSkill skillBtn" style={{ display: "inline-block" }}>
        <List horizontal className="text-night-rider">
          <List.Item key={val.name}>
            <Button compact>
              {val.exp ? (
                <React.Fragment>
                  {val.name} <span style={{ color: "#a1a1a1" }}>|</span>{" "}
                  <span> {val.exp} yrs</span>
                </React.Fragment>
              ) : (
                <React.Fragment>{val.name}</React.Fragment>
              )}
            </Button>
          </List.Item>
        </List>
      </div>
    );
  });

  return skillComponent.filter(val => val);
};

const processSkillsOptional = skills => {
  if (!skills) {
    return null;
  }

  // console.log("SKIILLS AREE", skills);

  const skillsName = skills.map(val => {
    if (val.reqType === "optional") {
      return {
        name: val.name,
        exp: val.exp
      };
    }
  });

  const skillComponent = skillsName.map(val => {
    if (!val) {
      return;
    }

    return (
      <div className="filterSkill skillBtn" style={{ display: "inline-block" }}>
        <List horizontal className="text-night-rider">
          <List.Item key={val.name}>
            <Button compact>
              {val.exp ? (
                <React.Fragment>
                  {val.name} |<span> {val.exp} yrs</span>
                </React.Fragment>
              ) : (
                <React.Fragment>{val.name}</React.Fragment>
              )}
            </Button>
          </List.Item>
        </List>
      </div>
    );
  });

  return skillComponent.filter(val => val);
};

class SkillComponent extends React.Component {
  state = {
    isEditModalOpen: false,
    jobdata: {}
  };
  render() {
    const { data } = this.props;
    const mandSkills = processSkills(data.skills);
    const optionalSkills = processSkillsOptional(data.skills);
    return (
      <div className="SkillComponent">
        <List.Item
          style={{
            display: !data.skills ? "none" : "block"
          }}
          className="padding-bottom-20">
          <List.Content>
            <InfoSectionGridHeader
              headerText="Skills Requirement"
              editIcon={this.editIcon}
              containerClass="skillsRequirement_profile"
              onEditIconClick={this.onEditIconClick}>
              <div className="mandatory_skill">
                {mandSkills && (
                  <React.Fragment>
                    <p
                      style={{
                        display: !mandSkills.length ? "none" : "block"
                      }}>
                      Mandatory
                    </p>
                    {mandSkills}
                  </React.Fragment>
                )}
              </div>
              <div className="optional_skill">
                {optionalSkills && (
                  <React.Fragment>
                    <p
                      style={{
                        display: !optionalSkills.length ? "none" : "block"
                      }}>
                      Optional
                    </p>
                    {optionalSkills}
                  </React.Fragment>
                )}
              </div>
            </InfoSectionGridHeader>
          </List.Content>
        </List.Item>
      </div>
    );
  }
}

export default SkillComponent;
