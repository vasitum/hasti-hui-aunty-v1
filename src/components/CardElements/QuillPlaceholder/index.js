import React, { Component } from "react";
import ReactQuill from "react-quill";
import cx from "classnames";

import "../QuillText/index.scss";

/**
 *
 * Reference:
 * https://quannt.github.io/programming/javascript/2017/05/11/adding-custom-toolbar-react-quill.html#comment-3613846315
 */
class Editor extends Component {
  constructor(props) {
    super(props);
  }

  /**
   * Quill doesn't automatically take html content, gotta parse it ourself
   */
  componentDidMount() {
    const placeholderPickerItems = Array.prototype.slice.call(
      document.querySelectorAll(".ql-placeholder .ql-picker-item")
    );

    placeholderPickerItems.forEach(
      item => (item.textContent = item.dataset.value)
    );

    const pickerPlaceholderLabel = document.querySelectorAll(
      ".ql-placeholder .ql-picker-label"
    );

    pickerPlaceholderLabel.forEach(item => {
      if (!item.innerHTML.startsWith("Insert")) {
        item.innerHTML = "Insert Placeholder" + item.innerHTML;
      }
    });
  }

  render() {
    const classes = cx({
      QuillText: true,
      [`is-hidden`]: this.props.hidden,
      [`is-readonly`]: this.props.readOnly,
      [`is-small`]: this.props.isSmall,
      [`is-base`]: this.props.bgBase,
      [`is-mute`]: this.props.isMute,
      [`is-doubleDown`]: this.props.isDoubleDown,
      [`is-Messagebox`]: this.props.isMessagebox,
      [`is-MessageText`]: this.props.isMessageText
    });

    return (
      <div className={classes}>
        <ReactQuill
          readOnly={this.props.readOnly}
          theme={"snow"}
          onChange={this.props.onChange}
          value={this.props.value}
          modules={Editor.modules}
          formats={["bold", "italic", "underline", "list", "placeholder"]}
          placeholder={this.props.placeholder}
        />
      </div>
    );
  }
}

/**
 * Quill modules to attach to editor
 * See http://quilljs.com/docs/modules/ for complete options
 */
Editor.modules = {
  toolbar: {
    container: [
      ["bold", "italic", "underline"],
      [{ list: "ordered" }, { list: "bullet" }],
      [
        {
          placeholder: ["{{ FirstName }}", "{{ LastName }}"]
        }
      ] // my custom dropdown
    ],
    handlers: {
      placeholder: function(value) {
        if (value) {
          const cursorPosition = this.quill.getSelection().index;
          this.quill.insertText(cursorPosition, value);
          this.quill.setSelection(cursorPosition + value.length);
        }
      }
    }
  },
  clipboard: {
    matchVisuals: false
  }
};

export default Editor;
