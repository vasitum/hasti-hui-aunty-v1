import React from "react";

import { Image, Header } from "semantic-ui-react";

// import CompanyLogo from "../../../assets/img/wipro.png";
import CompanySvg from "../../../assets/svg/IcCompany";

import QuillText from "../QuillText";

import "./index.scss";

const months = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "June",
  "July",
  "Aug",
  "Sept",
  "Oct",
  "Nov",
  "Dec"
];
const UserExperience = ({ experience }) => {
  if (!experience || !Array.isArray(experience)) {
    return null;
  }

  // console.log("check", experience);
  const expArray = experience.map(val => {
    return {
      desc: val.desc,
      loc: val.loc,
      designation: val.designation,
      name: val.name,
      doj: {
        month: val.doj.month,
        year: val.doj.year
      },
      dor: {
        month: val.dor.month,
        year: val.dor.year
      },
      current: val.current
    };
  });

  return (
    <div>
      {expArray.map(exp => {
        return (
          <React.Fragment>
            {!exp.doj ? null : <div className="CandidateExperience">
              <div className="company_logo">
                {/* <Image src={CompanyLogo} /> */}
                <CompanySvg
                  width="40"
                  height="40"
                  rectcolor="#f7f7fb"
                  pathcolor="#c8c8c8"
                />
              </div>
              <div className="experienceDetail">
                <Header as="h4">{exp.designation}</Header>
                <p className="companyTitle">{exp.name}</p>
                <p className="dateTime">
                  {(function (exp) {
                    let str = [];
                    if (exp.current) {
                      if (exp.doj.month) {
                        str.push(exp.doj.month);
                        str.push(" ");
                      }

                      if (exp.doj.year) {
                        str.push(exp.doj.year);
                        str.push(" ");
                        str.push("-");
                        str.push(" ");
                        str.push("Present");
                      }

                      return str.join("");
                    }

                    if (exp.doj.month) {
                      str.push(exp.doj.month);
                      str.push(" ");
                    }

                    if (exp.doj.year) {
                      str.push(exp.doj.year);
                      str.push(" ");
                    }

                    if (exp.dor.month) {
                      str.push("-");
                      str.push(" ");
                      str.push(months[exp.dor.month - 1]);
                    }

                    if (exp.dor.year) {
                      if (!exp.dor.month) {
                        str.push("-");
                      }
                      str.push(" ");
                      str.push(exp.dor.year);
                    }

                    return str.join("");
                  })(exp)}
                  {/* {exp.doj ? exp.doj.month : ""} {exp.doj ? exp.doj.year : ""} {exp.dor.month ? "-" : ""}
                {exp.dor ? exp.dor.month : ""} {exp.dor ? exp.dor.year : ""} */}
                </p>
                <p className="location">{exp.loc}</p>

                <div>
                  <QuillText placeholder="" value={exp.desc ? exp.desc : ""} readOnly />
                </div>
              </div>
            </div>
            }
          </React.Fragment>


        );
      })}

      {/* <div className="CandidateExperience">
        <div className="company_logo">
          <Image src={CompanyLogo} />
          <CompanySvg
            width="40"
            height="40"
            rectcolor="#f7f7fb"
            pathcolor="#c8c8c8"
          />
        </div>
        <div className="experienceDetail">
          <Header as="h4">UI UX Designer</Header>
          <p className="companyTitle">India Mart</p>
          <p className="dateTime">Apr 2013 - Apr 2017</p>
          <p className="location">Noida, India</p>

          <div>
            <QuillText
              value="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad ipsum dolor sit amet, consectetur adipisicing elit, sed do iusmod tempor incididunt ut labore minim veniam, quis nostru exercitation aliquip ex ea commodo consequat. See more"
              readOnly
            />
          </div>
        </div>
      </div> */}
    </div>
  );
};

export default UserExperience;
