import React from "react";
import { List, Header, Grid, Button } from "semantic-ui-react";

import QuillText from "../../CardElements/QuillText";
import getCompById from "../../../api/company/getCompById";

import FlatDefaultBtn from "../../Buttons/FlatDefaultBtn";

import "./index.scss";

class AboutCompany extends React.Component {
  state = {
    data: null
  };

  async componentDidMount() {
    const { compId } = this.props;
    if (!compId) {
      // console.log("compId not Found");
      return;
    }
    try {
      const res = await getCompById(compId);
      if (res.status === 200) {
        const data = await res.json();
        // console.log(data);
        this.setState({
          data: data
        });
      } else {
        console.error(res);
      }
    } catch (error) {
      console.error(error);
    }
  }

  async componentDidUpdate() {
    if (this.state.data) {
      return;
    }

    this.componentDidMount();
  }

  render() {
    const { data, isShowBtn } = this.state;

    if (!data) {
      return null;
    }

    return (
      <div className="AboutCompany">
        <div className="AboutCompany_header">
          <Header>About the Company</Header>
          <p>{data.name}</p>
        </div>
        <div className="AboutCompany_body">
          <p>{data.headquarter ? data.headquarter.location : ""}</p>
          <p>
            <span>Founded: </span> {data.year}
          </p>
          <p>
            <span>Employees: </span> {data.size}
          </p>
          <p>
            <span>Website: </span> {data.website}
          </p>
        </div>
        <div>
          <QuillText
            placeholder=""
            value={data.about ? data.about : ""}
            readOnly
          />
        </div>

        <div>
          {isShowBtn ? (
            <FlatDefaultBtn btntext="View more openings in this company" />
          ) : null}
        </div>
      </div>
    );
  }
}

export default AboutCompany;
