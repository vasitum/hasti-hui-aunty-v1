import React from "react";

import { Grid } from "semantic-ui-react";

import InputFieldLabel from "../../Forms/FormFields/InputFieldLabel";

import SearchTagField from "../../Forms/FormFields/SearchTagField";

import ActionBtn from "../../Buttons/ActionBtn";
import FlatDefaultBtn from "../../Buttons/FlatDefaultBtn";

import { Form } from "formsy-semantic-ui-react";
import IcInfo from "../../../assets/svg/IcInfo";

import { jobSave } from "../../../api/jobs/jobExtras";

import getUserLabels from "../../../api/user/getAllUserLabels";

import isUserLoggedIn from "../../../utils/env/isLoggedin";
import { toast } from "react-toastify";

import "./index.scss";

class AddLabelSave extends React.Component {
  state = {
    options: [],
    currentValues: []
  };

  constructor(props) {
    super(props);
    this.onSaveBtnClick = this.onSaveBtnClick.bind(this);
  }

  onTagAdd = (e, { value }) => {
    this.setState({
      options: [{ text: value, value }, ...this.state.options]
    });
  };

  onTagChange = (e, { value }) => {
    this.setState({ currentValues: value });
  };

  /** async mount */
  async componentDidMount() {
    if (!isUserLoggedIn) {
      return;
    }

    const { type } = this.props;

    try {
      const res = await getUserLabels(type);
      const data = await res.json();
      // console.log(data);

      const dataOptions = data.map(val => {
        return {
          text: val,
          value: val,
          key: val
        };
      });

      this.setState(state => {
        return {
          ...state,
          options: dataOptions
        };
      });
    } catch (error) {
      console.error(error);
    }
  }

  async onSaveBtnClick(ev) {
    const { saveObjectId } = this.props;

    if (!isUserLoggedIn) {
      toast("Please login to save");
      return;
    }

    const { type } = this.props;

    try {
      const res = await jobSave(saveObjectId, this.state.currentValues, type);

      if (res.status == 200) {
        this.props.onSaveClick();
      }
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const { children, onCloseBtnClick } = this.props;
    return (
      <div className="addLabels_Save">
        <Grid>
          <Grid.Row>
            <Grid.Column width={16}>
              <InputFieldLabel
                label="Add labels"
                infoicon={<IcInfo pathcolor="#c8c8c8" />}
              />
            </Grid.Column>
            <Grid.Column width={16}>
              <Form>
                <SearchTagField
                  onTagChange={this.onTagChange}
                  onTagAdd={this.onTagAdd}
                  options={this.state.options}
                  values={this.state.currentValues}
                  name="hello"
                  icon={false}
                />
              </Form>
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <div className="addLabels_SaveFooter">
          <ActionBtn actioaBtnText="Save" onClick={this.onSaveBtnClick} />
          <FlatDefaultBtn
            btntext="Cancel"
            classNames="bgTranceparent"
            onClick={onCloseBtnClick}
          />
        </div>
      </div>
    );
  }
}

export default AddLabelSave;
