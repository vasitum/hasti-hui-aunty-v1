import React from "react";

const ChatbotContext = new React.createContext({
  searchText: "",
  onSearchChange: () => {}
});

export class AlgoliaContextProvider extends React.Component {
  constructor(props) {
    super(props);

    this.onSearchChange = searchQuery => {
      this.setState({
        searchText: searchQuery
      });
    };

    this.state = {
      searchText: "",
      onSearchChange: this.onSearchChange
    };
  }

  render() {
    return (
      <ChatbotContext.Provider value={this.state}>
        {this.props.children}
      </ChatbotContext.Provider>
    );
  }
}

export function withAlgoliaContext(WrappedComponent) {
  return class extends React.Component {
    render() {
      return (
        <ChatbotContext.Consumer>
          {value => {
            return (
              <WrappedComponent
                searchQuery={value.searchText}
                onSearhContextChange={value.onSearchChange}
                {...this.props}
              />
            );
          }}
        </ChatbotContext.Consumer>
      );
    }
  };
}
