import React, { Component, createContext } from "react";
import { USER } from "../constants/api";

/**
 * Helper to save to localState
 */
function saveOnLocalStorage(user) {
  window.localStorage.setItem(USER.UID, user.id);
  window.localStorage.setItem(USER.EMAIL, user.email);
  window.localStorage.setItem(USER.NAME, user.fName + " " + user.lName);
  window.localStorage.setItem(USER.CTC, user.ctc);
  window.localStorage.setItem(USER.IMG_EXT, user.userImg);
  window.localStorage.setItem(USER.LOC, user.loc);
  window.localStorage.setItem(USER.PHONE, user.mobile);
  window.localStorage.setItem(USER.TITLE, user.title);
  window.localStorage.setItem(USER.VERIFIED, user.isVerified);
  window.localStorage.setItem(USER.EXP, 0);
}

function getUserFromLocalStorage() {
  return {
    id: window.localStorage.getItem(USER.UID),
    email: window.localStorage.getItem(USER.EMAIL),
    fName: window.localStorage.getItem(USER.NAME),
    ctc: window.localStorage.getItem(USER.CTC),
    userImg: window.localStorage.getItem(USER.IMG_EXT),
    loc: window.localStorage.getItem(USER.LOC),
    mobile: window.localStorage.getItem(USER.PHONE),
    title: window.localStorage.getItem(USER.TITLE),
    isVerified: window.localStorage.getItem(USER.VERIFIED)
  };
}

function checkLocalData(data) {
  if (!data) {
    return false;
  }

  if ((data && data === "undefined") || (data && data === "null")) {
    return false;
  }

  return true;
}

function isProfileCreated(state, data) {
  const CRITERIA = ["fName", "lName", "mobile"];
  const missingState = [];

  // check state
  CRITERIA.map(field => {
    if (!state[field] && !data[field]) {
      missingState.push(field);
    }

    if (state[field] === "null" && data[field] === "null") {
      missingState.push(field);
    }
  });

  return missingState.length > 0 ? false : true;
}

export const UserContext = createContext();

export function withUserContextProvider(WrappedComponent) {
  return class extends Component {
    constructor(props) {
      super(props);

      this.onChange = data => {
        this.setState(state => {
          const finalObject = {
            ...state.user,
            ...data,
            isProfileCreated: isProfileCreated(state, data)
          };

          // save to storage
          saveOnLocalStorage(finalObject);

          return {
            ...state,
            user: finalObject
          };
        });
      };

      this.state = {
        user: {
          id: "",
          email: "",
          fName: "",
          lName: "",
          title: "",
          loc: "",
          userImg: "",
          mobile: "",
          ctc: "",
          token: "",
          isProfileCreated: false,
          isVerified: false
        },
        onChange: this.onChange
      };
    }

    componentDidMount() {
      const userFromLocal = getUserFromLocalStorage();
      if (!userFromLocal.id || userFromLocal.id == "undefined") {
        return;
      }

      let fName, lName;
      let profileCreated = false;
      if (userFromLocal.fName && userFromLocal.fName !== "undefined") {
        const [userfName, userlName] = userFromLocal.fName.split(" ");
        fName = userfName;
        lName = userlName;
      }

      if (checkLocalData(fName)) {
        profileCreated = true;
      }

      this.setState(state => {
        return {
          ...state,
          user: {
            ...state.user,
            ...userFromLocal,
            fName: fName ? fName : "",
            lName: lName ? lName : "",
            isProfileCreated: profileCreated,
            isVerified: userFromLocal.isVerified
          }
        };
      });
    }

    render() {
      return (
        <UserContext.Provider value={this.state}>
          <WrappedComponent {...this.props} />
        </UserContext.Provider>
      );
    }
  };
}

export function withUserContextConsumer(WrappedComponent) {
  return class extends Component {
    render() {
      return (
        <UserContext.Consumer>
          {({ user, onChange }) => {
            return (
              <WrappedComponent
                onUserChange={onChange}
                user={user}
                {...this.props}
              />
            );
          }}
        </UserContext.Consumer>
      );
    }
  };
}
