import React from "react";

const CB_CONSTANTS = {
  VISBILITY: {
    EXPAND: "EXPAND",
    MAXIMIZE: "MAXIMIZE",
    MINIMIZE: "MINIMIZE"
  }
};

const ChatbotContext = new React.createContext({
  cb: {
    visibility: CB_CONSTANTS.VISBILITY.EXPAND,
    applicationData: null
  },
  actions: {
    onMinimize: () => {
      console.log("Minimize");
    },
    onMaximize: () => {
      console.log("Maximize");
    },
    onExpand: () => {
      console.log("Expand");
    },
    onApply: () => {
      console.log("Apply Called");
    }
  }
});

export class CBContextProvider extends React.Component {
  constructor(props) {
    super(props);

    this.onMinimize = () => {
      this.setState({
        cb: {
          visibility: CB_CONSTANTS.VISBILITY.MINIMIZE
        }
      });
    };

    this.onMaximize = () => {
      this.setState({
        cb: {
          visibility: CB_CONSTANTS.VISBILITY.MAXIMIZE
        }
      });
    };

    this.onExpand = () => {
      this.setState({
        cb: {
          visibility: CB_CONSTANTS.VISBILITY.EXPAND
        }
      });
    };

    this.onApply = ({ jobId, applicationId, userId }) => {
      this.setState({
        cb: {
          ...this.state.cb,
          applicationData: {
            jobId: jobId,
            applicationId: applicationId,
            userId: userId
          }
        }
      });
    };

    this.state = {
      cb: {
        visibility: CB_CONSTANTS.VISBILITY.MINIMIZE,
        applicationData: null
      },
      actions: {
        onMinimize: this.onMinimize,
        onMaximize: this.onMaximize,
        onExpand: this.onExpand,
        onApply: this.onApply
      }
    };
  }

  render() {
    return (
      <ChatbotContext.Provider value={this.state}>
        {this.props.children}
      </ChatbotContext.Provider>
    );
  }
}

export function withChatbotContext(WrappedComponent) {
  return class extends React.Component {
    render() {
      return (
        <ChatbotContext.Consumer>
          {value => {
            return <WrappedComponent cb={value.cb} actions={value.actions} />;
          }}
        </ChatbotContext.Consumer>
      );
    }
  };
}
