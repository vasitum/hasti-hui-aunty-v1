import { combineReducers } from "redux";

import actionTypes from "../actions/actionTypes";

const IsearchState = {
  currentValue: "",
  searchCount: {
    job: 0,
    people: 0
  }
};

function searchState(state = IsearchState, action) {
  switch (action.type) {
    case actionTypes.ON_SEARCH_SUBMIT:
      return {
        ...state,
        currentValue: action.payload
      };

    case actionTypes.ON_SEARCH_COUNT_CHANGE:
      return {
        ...state,
        searchCount: {
          job: action.payload.job,
          people: action.payload.people
        }
      };

    case actionTypes.ON_SEARCH_COUNT_JOB_CHANGE:
      return {
        ...state,
        searchCount: {
          job: action.payload.job,
          people: state.searchCount.people
        }
      };

    case actionTypes.ON_SEARCH_COUNT_PEOPLE_CHANGE:
      return {
        ...state,
        searchCount: {
          job: state.searchCount.job,
          people: action.payload.people
        }
      };

    default:
      return state;
  }
}

export default combineReducers({
  search: searchState
});
