import React from "react";
// import ReactDOM from "react-dom";
import { hydrate, render } from "react-dom";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./store";
// import { render } from "react-snapshot";
import "./styles/semantic/semantic.min.css";
import "./styles/vasitum/index.scss";

// import { config } from "./services/pushservice/config";
// import firebase from "firebase";

import App from "./App";
import registerServiceWorker, { unregister } from "./registerServiceWorker";

// firebase init
// firebase.initializeApp(config);

// render(
//   <Provider store={store}>
//     <BrowserRouter>
//       <App />
//     </BrowserRouter>
//   </Provider>,
//   document.getElementById("root")
// );
const rootElement = document.getElementById("root");
function removeBodyMovinElement() {
  const $lottieElement = document.getElementById("sploosh");

  if ($lottieElement) {
    setTimeout(() => {
      window.bodymovin.destroy();
      $lottieElement.parentNode.removeChild($lottieElement);
    }, 2500);
  }
}

if (rootElement.hasChildNodes()) {
  hydrate(
    <Provider store={store}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </Provider>,
    rootElement,
    () => removeBodyMovinElement()
  );
} else {
  render(
    <Provider store={store}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </Provider>,
    rootElement,
    () => removeBodyMovinElement()
  );
}

unregister();

// render(
//   <Provider store={store}>
//     <BrowserRouter>
//       <App />
//     </BrowserRouter>
//   </Provider>,
//   document.getElementById("root")
// );
// registerServiceWorker();
let deferredPrompt;
// let btnAdd;
window.addEventListener("beforeinstallprompt", e => {
  // Prevent Chrome 67 and earlier from automatically showing the prompt
  e.preventDefault();
  // Stash the event so it can be triggered later.
  deferredPrompt = e;
  // Update UI notify the user they can add to home screen
  // btnAdd.style.display = "block";
});
// btnAdd.addEventListener("click", e => {
//   // hide our user interface that shows our A2HS button
//   btnAdd.style.display = "none";
//   // Show the prompt
//   deferredPrompt.prompt();
//   // Wait for the user to respond to the prompt
//   deferredPrompt.userChoice.then(choiceResult => {
//     if (choiceResult.outcome === "accepted") {
//       console.log("User accepted the A2HS prompt");
//     } else {
//       console.log("User dismissed the A2HS prompt");
//     }
//     deferredPrompt = null;
//   });
// });
