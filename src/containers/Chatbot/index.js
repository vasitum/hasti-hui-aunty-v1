import React from "react";

import { withChatbotContext } from "../../contexts/ChatbotContext";
import { withUserContextConsumer } from "../../contexts/UserContext";

// import { MavenUI } from "MavenUI";
// import "MavenUI/build/css/index.css";
import { MavenUI } from "../../components/ChatbotV2";

class MeonChatbot extends React.Component {
  componentDidMount() {
    // console.log("Chatbot Context is", this.props.actions.onMinimize());
  }

  render() {
    return <MavenUI {...this.props} />;
  }
}

export default withChatbotContext(withUserContextConsumer(MeonChatbot));
