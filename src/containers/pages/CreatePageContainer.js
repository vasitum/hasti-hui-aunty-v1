import React, { Component } from "react";
import Loadable from "react-loadable";
import { Route, Switch } from "react-router-dom";
import { CREATE } from "../../constants/paths";
import Loader from "../MainLoader";
import CreateProfilePage from "../../components/CreateProfilePage";
import { LinkedInPopUp } from "react-linkedin-login-oauth2";
// const CreateProfilePage = Loadable({
//   loader: () => import("../../components/CreateProfilePage"),
//   loading: <Loader />
// });

class CreatePageContainer extends Component {
  render() {
    const { match } = this.props;

    return (
      <Switch>
        <Route
          exact
          path={match.url + CREATE.PROFILE}
          component={CreateProfilePage}
        />
        <Route
          exact
          path={match.url + CREATE.PROFILE + "/linkedin"}
          component={LinkedInPopUp}
        />
      </Switch>
    );
  }
}

export default CreatePageContainer;
