import React from "react";
import { Route, Switch } from "react-router-dom";
import { SEARCH } from "../../constants/paths";
import Loadable from "react-loadable";
import Loader from "../MainLoader";
import SearchPage from "../../components/ModalPageSection/SearchFilter";
// const SearchPage = Loadable({
//   loader: () => import("../../components/ModalPageSection/SearchFilter"),
//   loading() {
//     return <Loader />;
//   }
// });
const SearchPageContainer = ({ match }) => {
  return (
    <React.Fragment>
      <Switch>
        <Route exact path={match.url} component={SearchPage} />
        <Route path={match.url + "/:pageid"} component={SearchPage} />
        {/* <Route exact path={match.url + SEARCH.PEOPLE} component={SearchPage} /> */}
      </Switch>
    </React.Fragment>
  );
};

SearchPageContainer.propTypes = {};

/**
 * @export
 */
export default SearchPageContainer;
