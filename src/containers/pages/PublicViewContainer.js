import React, { Component } from "react";
import PublicJobView from "../../components/ModalPageSection/PreLoginJob";
import PublicProfileView from "../../components/ModalPageSection/PreProfile";
import PreviewProfileView from "../../components/ModalPageSection/PreviewProfile";
import Loadable from "react-loadable";
import Loader from "../MainLoader";
import { Route, Switch } from "react-router-dom";
import { VIEW } from "../../constants/paths";

// const PublicJobView = Loadable({
//   loader: () => import("../../components/ModalPageSection/PreLoginJob"),
//   loading() {
//     return <Loader />;
//   }
// });
// const PublicProfileView = Loadable({
//   loader: () => import("../../components/ModalPageSection/PreProfile"),
//   loading() {
//     return <Loader />;
//   }
// });
// const PreviewProfileView = Loadable({
//   loader: () => import("../../components/ModalPageSection/PreviewProfile"),
//   loading() {
//     return <Loader />;
//   }
// });

class PublicViewContainer extends Component {
  render() {
    const { match } = this.props;

    return (
      <Switch>
        <Route exact path={match.url + VIEW.JOB} component={PublicJobView} />
        <Route
          exact
          path={match.url + VIEW.JOB_WITH_NAME}
          component={PublicJobView}
        />
        <Route
          exact
          path={match.url + VIEW.PROFILE}
          component={PublicProfileView}
        />
        <Route
          exact
          path={match.url + VIEW.PREVIEW}
          component={PreviewProfileView}
        />
      </Switch>
    );
  }
}

export default PublicViewContainer;
