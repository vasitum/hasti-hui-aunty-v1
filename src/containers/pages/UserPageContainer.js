import React from "react";
import { Route, Switch } from "react-router-dom";
import { USER } from "../../constants/paths";
import PropTypes from "prop-types";
import Loadable from "react-loadable";
import Loader from "../MainLoader";
import CreateProfilePage from "../../components/CreateProfilePage";
import PublicProfileView from "../../components/ModalPageSection/PreProfile";
import PreviewProfileView from "../../components/ModalPageSection/PreviewProfile";
import PrivateProfileView from "../../components/ModalPageSection/PostLoginProfile";
import withProtectedRoutes from "../../components/utils/ProtectedRoute";

// const CreateProfilePage = Loadable({
//   loader: () => import("../../components/CreateProfilePage"),
//   loading() {
//     return <Loader />;
//   }
// });
// const PublicProfileView = Loadable({
//   loader: () => import("../../components/ModalPageSection/PreProfile"),
//   loading() {
//     return <Loader />;
//   }
// });
// const PreviewProfileView = Loadable({
//   loader: () => import("../../components/ModalPageSection/PreviewProfile"),
//   loading() {
//     return <Loader />;
//   }
// });
// const PrivateProfileView = Loadable({
//   loader: () => import("../../components/ModalPageSection/PostLoginProfile"),
//   loading() {
//     return <Loader />;
//   }
// });

const UserPageContainer = ({ match }) => {
  return (
    <React.Fragment>
      <Switch>
        <Route
          exact
          path={match.url}
          component={() => <div> User Dashboard Page </div>}
        />
        <Route
          exact
          path={match.url + USER.PUBLIC_VIEW}
          component={PublicProfileView}
        />
        <Route
          exact
          path={match.url + USER.PROFILE_PREVIEW}
          component={PreviewProfileView}
        />
        {/* Post Job */}
        <Route
          exact
          path={match.url + USER.CREATE_PROFILE}
          component={CreateProfilePage}
        />
        <Route path={match.url + USER.VIEW} component={PrivateProfileView} />
      </Switch>
    </React.Fragment>
  );
};

UserPageContainer.propTypes = {};

/**
 * @export
 */
export default withProtectedRoutes(UserPageContainer);
