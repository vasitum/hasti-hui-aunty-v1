import React, { Suspense, lazy } from "react";
import { Route, Switch } from "react-router-dom";
import Loadable from "react-loadable";
// import ManageComapniesPage from "../../components/ManageComapnies";
import withProtectedRoutes from "../../components/utils/ProtectedRoute";

const ManageCompaniesPage = lazy(() =>
  import("../../components/ManageComapnies")
);

// const ManageCompaniesPage = Loadable({
//   loader: () => import("../../components/ManageComapnies"),
//   loading() {
//     return <div>Loading...</div>;
//   }
// });

const CompanyPageContainer = ({ match }) => {
  return (
    <React.Fragment>
      <Switch>
        <Route
          exact
          path={match.url}
          render={props => (
            <Suspense>
              <ManageCompaniesPage {...props} />
            </Suspense>
          )}
        />
        {/* <Route path={match.url} component={ManageCompaniesPage} /> */}
      </Switch>
    </React.Fragment>
  );
};

CompanyPageContainer.propTypes = {};

/**
 * @export
 */
export default withProtectedRoutes(CompanyPageContainer);
