import React, { Component, lazy, Suspense } from "react";
import { Route, Switch } from "react-router-dom";
import Loadable from "react-loadable";
import { CV_TEMPLATE } from "../../constants/paths";
import Loader from "../MainLoader";

const CVCreateTemplate = lazy(() => import("../../components/CvCreate"));
const RBasicContainer = lazy(() => import("../../components/CvCreate/Cv1"));
const RClassicContainer = lazy(() => import("../../components/CvCreate/Cv2"));
const RModernContainer = lazy(() => import("../../components/CvCreate/Cv3"));

// const CVCreateTemplate = Loadable({
//   loader: () => import("../../components/CvCreate"),
//   loading() {
//     return <Loader />;
//   }
// });
// const RBasicContainer = Loadable({
//   loader: () => import("../../components/CvCreate/Cv1"),
//   loading() {
//     return <Loader />;
//   }
// });
// const RClassicContainer = Loadable({
//   loader: () => import("../../components/CvCreate/Cv2"),
//   loading() {
//     return <Loader />;
//   }
// });
// const RModernContainer = Loadable({
//   loader: () => import("../../components/CvCreate/Cv3"),
//   loading() {
//     return <Loader />;
//   }
// });

class CVContainer extends Component {
  render() {
    const { match } = this.props;

    return (
      <Switch>
        <Route
          exact
          path={match.url}
          render={props => (
            <Suspense fallback={<Loader />}>
              <CVCreateTemplate {...props} />
            </Suspense>
          )}
        />
        {/* <Route exact path={match.url} component={CVCreateTemplate} /> */}

        <Route
          exact
          path={CV_TEMPLATE.RBASIC}
          render={props => (
            <Suspense fallback={() => <Loader />}>
              <RBasicContainer {...props} />
            </Suspense>
          )}
        />
        {/* <Route exact path={CV_TEMPLATE.RBASIC} component={RBasicContainer} /> */}

        <Route
          exact
          path={CV_TEMPLATE.RCLASSIC}
          render={props => (
            <Suspense fallback={() => <Loader />}>
              <RClassicContainer {...props} />
            </Suspense>
          )}
        />
        {/* <Route
          exact
          path={CV_TEMPLATE.RCLASSIC}
          component={RClassicContainer}
        /> */}

        <Route
          exact
          path={CV_TEMPLATE.RMODERN}
          render={props => (
            <Suspense fallback={() => <Loader />}>
              <RModernContainer {...props} />
            </Suspense>
          )}
        />
        {/* <Route exact path={CV_TEMPLATE.RMODERN} component={RModernContainer} /> */}
      </Switch>
    );
  }
}

export default CVContainer;
