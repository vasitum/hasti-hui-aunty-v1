import React, { Component } from "react";
import { Table, Icon, Container } from "semantic-ui-react";
import { Link } from "react-router-dom";
import { USER } from "../../constants/api";

import PropTypes from "prop-types";

import isLoggedIn from "../../utils/env/isLoggedin";

class HomePageContainer extends Component {
  render() {
    return (
      <div>
        <Container style={{ marginTop: "80px" }}>
          <h1>Work in Progress (Home Page)</h1>

          {!isLoggedIn() && (
            <Table celled striped>
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell colSpan="3">Page Index</Table.HeaderCell>
                </Table.Row>
              </Table.Header>
              <Table.Body>
                <Table.Row>
                  <Table.Cell>
                    <Icon name="folder" /> Post Job
                  </Table.Cell>
                  <Table.Cell>
                    <Link to="/job/new">Post Job</Link>
                  </Table.Cell>
                  <Table.Cell textAlign="center">Data Done</Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <Icon name="folder" /> Posted Job List
                  </Table.Cell>
                  <Table.Cell>
                    <Link to="/job">My Job List</Link>
                  </Table.Cell>
                  <Table.Cell textAlign="center">Data Done</Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <Icon name="folder" />
                    My Profile View
                  </Table.Cell>
                  <Table.Cell>
                    <Link
                      to={`user/view/${window.localStorage.getItem(USER.UID)}`}>
                      My Profile View
                    </Link>
                  </Table.Cell>
                  <Table.Cell textAlign="center">Data Done</Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <Icon name="folder" /> My Companies
                  </Table.Cell>
                  <Table.Cell>
                    <Link to="/company">My Companies</Link>
                  </Table.Cell>
                  <Table.Cell textAlign="center">Data Done</Table.Cell>
                </Table.Row>
                {/* <Table.Row>
                  <Table.Cell>
                    <Icon name="folder" /> My Card
                  </Table.Cell>
                  <Table.Cell>
                    <Link to="/defaultCard">My Card</Link>
                  </Table.Cell>
                  <Table.Cell textAlign="center">Data Done</Table.Cell>
                </Table.Row> */}
              </Table.Body>
            </Table>
          )}
        </Container>
      </div>
    );
  }
}

HomePageContainer.propTypes = {};

export default HomePageContainer;
