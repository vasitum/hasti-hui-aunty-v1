import React from "react";
import { Route, Switch } from "react-router-dom";
import Loadable from "react-loadable";
import Loader from "../MainLoader";
import PropTypes from "prop-types";

import AllUser from "../../components/Messaging/UserAllConversation";
// const AllUser = Loadable({
//   loader: () => import("../../components/Messaging/UserAllConversation"),
//   loading() {
//     return <Loader />;
//   }
// });
import Messaging from "../../components/Messaging";
// const Messaging = Loadable({
//   loader: () => import("../../components/Messaging"),
//   loading() {
//     return <Loader />;
//   }
// });
import withProtectedRoutes from "../../components/utils/ProtectedRoute";

class MessagePageContainer extends React.Component {
  render() {
    const { match } = this.props;

    return (
      <React.Fragment>
        <Switch>
          <Route path={match.url} render={props => <AllUser hasId />} />
          {/* <Route exact path={match.url + "/history/:id"} component={Messaging} /> */}
          {/* <Route
            exact
            path={match.url + "/history"}
            render={props => <Messaging isChat {...props} />}
          /> */}
          {/* <Route exact path={match.url + "/:id"} render={props => <AllUser />} /> */}
          {/* <Route
            exact
            path={match.url + "/history/:id"}
            render={props => <Messaging isChat {...props} />}
          /> */}
        </Switch>
      </React.Fragment>
    );
  }
}

MessagePageContainer.propTypes = {};

export default withProtectedRoutes(MessagePageContainer);
