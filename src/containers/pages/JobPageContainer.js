import React from "react";
import { Route, Switch } from "react-router-dom";
import { JOB } from "../../constants/paths";
import PropTypes from "prop-types";
import Loadable from "react-loadable";
import Loader from "../MainLoader";
import withProtectedRoutes from "../../components/utils/ProtectedRoute";
import PostJobPage from "../../components/PostJobPage";
// const PostJobPage = Loadable({
//   loader: () => import("../../components/PostJobPage"),
//   loading() {
//     return <Loader />;
//   }
// });
import PublicJobView from "../../components/ModalPageSection/PreLoginJob";
// const PublicJobView = Loadable({
//   loader: () => import("../../components/ModalPageSection/PreLoginJob"),
//   loading() {
//     return <Loader />;
//   }
// });
import PreviewJobView from "../../components/ModalPageSection/PreviewJob";
// const PreviewJobView = Loadable({
//   loader: () => import("../../components/ModalPageSection/PreviewJob"),
//   loading() {
//     return <Loader />;
//   }
// });
import JobDashBoard from "../../components/JobDashboard";
// const JobDashBoard = Loadable({
//   loader: () => import("../../components/JobDashboard"),
//   loading() {
//     return <Loader />;
//   }
// });
import JobDraftBoard from "../../components/JobDraftboard";
// const JobDraftBoard = Loadable({
//   loader: () => import("../../components/JobDraftboard"),
//   loading() {
//     return <Loader />;
//   }
// });

import RecruiterDashboard from "../../components/Dashboard/RecruiterDashboard";
// const RecruiterDashboard = Loadable({
//   loader: () => import("../../components/Dashboard/RecruiterDashboard"),
//   loading() {
//     return <Loader />;
//   }
// });
import MyJobsDetails from "../../components/Dashboard/RecruiterDashboard/MyJobsDetails";
// const MyJobsDetails = Loadable({
//   loader: () =>
//     import("../../components/Dashboard/RecruiterDashboard/MyJobsDetails"),
//   loading() {
//     return <Loader />;
//   }
// });
import ManageTemplate from "../../components/ManageTemplate";
// const ManageTemplate = Loadable({
//   loader: () => import("../../components/ManageTemplate"),
//   loading() {
//     return <Loader />;
//   }
// });
import CandidatesDetailpage from "../../components/Dashboard/RecruiterDashboard/CandidatesDetailpage";
// const CandidatesDetailpage = Loadable({
//   loader: () =>
//     import("../../components/Dashboard/RecruiterDashboard/CandidatesDetailpage"),
//   loading() {
//     return <Loader />;
//   }
// });
import CandidateDashbord from "../../components/Dashboard/CandidateDashbord";
// const CandidateDashbord = Loadable({
//   loader: () => import("../../components/Dashboard/CandidateDashbord"),
//   loading() {
//     return <Loader />;
//   }
// });
import CandidatesApplyedDetailpage from "../../components/Dashboard/CandidateDashbord/CandidatesApplyedDetailpage";
// const CandidatesApplyedDetailpage = Loadable({
//   loader: () =>
//     import("../../components/Dashboard/CandidateDashbord/CandidatesApplyedDetailpage"),
//   loading() {
//     return <Loader />;
//   }
// });
// import withProtectedRoutes from "../../components/utils/ProtectedRoute";

import InterviewPreference from "../../components/InterviewScreening/InterviewPreference";
// const InterviewPreference = Loadable({
//   loader: () =>
//     import("../../components/InterviewScreening/InterviewPreference"),
//   loading() {
//     return <Loader />;
//   }
// });

import InterviewScreening from "../../components/InterviewScreening";
// const InterviewScreening = Loadable({
//   loader: () => import("../../components/InterviewScreening"),
//   loading() {
//     return <Loader />;
//   }
// });
import PostJobPageEdit from "../../components/PostJobPageEdit";
// const PostJobPageEdit = Loadable({
//   loader: () => import("../../components/PostJobPageEdit"),
//   loading() {
//     return <Loader />;
//   }
// });

const JobPageContainer = ({ match }) => {
  return (
    <React.Fragment>
      <Switch>
        <Route exact path={match.url} component={JobDashBoard} />

        {/* <Route exact path={match.url + JOB.HOME_PAGE} component={HomePage} /> */}

        <Route
          exact
          path={match.url + JOB.DASHBOARD}
          component={RecruiterDashboard}
        />

        <Route
          exact
          path={match.url + JOB.CANDIDATE_DASHBOARD}
          component={CandidateDashbord}
        />

        <Route
          exact
          path={match.url + JOB.CANDIDATE_DETAIL_PAGE}
          component={CandidatesDetailpage}
        />

        <Route
          exact
          path={match.url + JOB.MY_CANDIDATE_DETAIL}
          component={CandidatesDetailpage}
        />

        <Route
          exact
          path={match.url + JOB.INTERVIEW}
          component={InterviewPreference}
        />

        {/* <Route
          exact
          path={match.url + JOB.INTERVIEW_QUESTIONS}
          component={ReviewScreeningQuestions}
        /> */}

        <Route
          exact
          path={match.url + JOB.SCREENING_QUESTIONS}
          component={InterviewScreening}
        />

        <Route
          exact
          path={match.url + JOB.SCREENING_QUESTIONS_EDIT}
          component={InterviewScreening}
        />

        <Route
          exact
          path={match.url + JOB.MYJOBS_DETAIL_CONDIDATE_PAGE}
          component={CandidatesApplyedDetailpage}
        />

        <Route
          exact
          path={match.url + JOB.POST_JOB_EDIT}
          component={PostJobPageEdit}
        />

        <Route
          exact
          path={match.url + JOB.MYJOBS_DETAIL_PAGE}
          component={MyJobsDetails}
        />

        {/* for manage template */}
        <Route
          exact
          path={match.url + JOB.MANAGE_TEMPLATE}
          component={ManageTemplate}
        />

        <Route
          exact
          path={match.url + JOB.JOB_DRAFTS}
          component={JobDraftBoard}
        />
        <Route path={match.url + JOB.PUBLIC_VIEW} component={PublicJobView} />
        {/* Post Job */}
        <Route exact path={match.url + JOB.POST_JOB} component={PostJobPage} />
        <Route
          exact
          path={match.url + JOB.REPOST_JOB}
          component={PostJobPage}
        />

        <Route
          exact
          path={match.url + JOB.PREVIEW}
          component={PreviewJobView}
        />

        {/* <Route path={match.url + JOB.VIEW} component={PrivateJobView} /> */}
        <Route path={match.url + JOB.VIEW} component={MyJobsDetails} />
      </Switch>
    </React.Fragment>
  );
};

JobPageContainer.propTypes = {};

/**
 * @export
 */
export default withProtectedRoutes(JobPageContainer);
