import React from "react";
import "./index.scss";
export default function MainLoader() {
  return (
    <div className="loader-block">
      <div className="loader" />
    </div>
  );
}
