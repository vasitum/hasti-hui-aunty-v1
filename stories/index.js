import React from "react";
import "../src/styles/semantic/semantic.min.css";
import "../src/styles/vasitum/index.scss";

import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { linkTo } from "@storybook/addon-links";
import SavetempOption from "../src/components/Messaging/Templates/SavetempOption";
import { Grid, Container } from "semantic-ui-react";
import Sliderimage from "../src/components/Messaging/MediaSection/PreviewImageSlider";
import KitchenSink from "../src/components/KitchenSink";
import KitchenSinkForm from "../src/components/KitchenSink/Forms";
import MoreOption from "../src/components/Messaging/MoreOption";
import PageBanner from "../src/components/Banners/PageBanner";
import SearchBanner from "../src/components/Banners/SearchBanner";
import JobNoFound from "../src/components/Banners/JobNoFound";
import InfoSection from "../src/components/Sections/InfoSection";
import MessagingMediaCard from "../src/components/Messaging/MediaSection/MessagingMediaCard";
import JobInfoCard from "../src/components/Cards/JobInfoCard";
import MediaDocument from "../src/components/Messaging/MediaSection/MediaDocumentShare";
import QuillText from "../src/components/CardElements/QuillText";
import MediaHeader from "../src/components/Messaging/MediaSection/MediaHeader";
import ElevateContainer from "../src/components/Pages/ElevateContainer";
import NewTemplateMobile from "../src/components/Messaging/MessagingMobile/NewTemplateMobile";
import aunty from "../src/assets/banners/aunty.png";
import IcPrivacyLock from "../src/assets/svg/IcPrivacyLock";
import MessagingFirstPage from "../src/components/Messaging/MessagingMobile/MessagingFirstPage";
import PostJobForm from "../src/components/Forms";

import Progressbar from "../src/components/Progressbar";

import FileuploadBtn from "../src/components/Buttons/FileuploadBtn";

import PostJobPage from "../src/components/PostJobPage";

import CreateProfilePage from "../src/components/CreateProfilePage";

import EditProfilePage from "../src/components/EditProfilePage";
import ShowApplicant from "../src/components/ShowApplicant";
import ContactFeedback from "../src/components/StaticPage/ContactFeedback";
import AboutUs from "../src/components/StaticPage/AboutUs";
import PrivacyPolicy from "../src/components/StaticPage/PrivacyPolicy";
import TermAndCondition from "../src/components/StaticPage/TermAndCondition";

import MobileContainer from "../src/components/MobileComponents/MobileContainer";
import TemplateMobileContainer from "../src/components/Messaging/MessagingMobile/TemplateMobileContainer";
import ProfileProgress from "../src/components/ProfileProgress";

import InfoSectionGridHeader from "../src/components/Sections/InfoSectionGridHeader";
import TemplateFooter from "../src/components/Messaging/Templates/TemplateButtonfooter";
import CreateCompanyPage from "../src/components/CreateCompanyPage";
import ChatThreadMoreOption from "../src/components/Messaging/ChatThread/ChatThreadMoreOption";
import ModalBanner from "../src/components/ModalBanner";
import IcInfoIcon from "../src/assets/svg/IcInfo";
import TemplatesHeader from "../src/components/Messaging/Templates/TemplatesHeader";
import ModalContainer from "../src/components/ModalContainer";
import CandidateMobileHeader from "../src/components/CandidateMobileHeader";
import ManageCompnies from "../src/components/ManageComapnies";
import ShareButton from "../src/components/ShareButton";
import SearchPages from "../src/components/SearchPages";
import MyUserSignUp from "../src/components/SignUp";
import HeaderApply from "../src/components/HeaderApply";
import OtherDetails from "../src/components/Infopage";
import NavigationDrawer from "../src/components/NavigationDrawer";
import FilterButton from "../src/components/FilterButton";
import PublicProfilePage from "../src/components/PublicProfile";
import CardFooter from "../src/components/CardFooter";
import ModalPageSection from "../src/components/ModalPageSection";
import AddTag from "../src/components/Dashboard/RecruiterDashboard/Candidates/AddTag";
import PreProfile from "../src/components/ModalPageSection/PreProfile";
import OptionsHeader from "../src/components/Messaging/MediaSection/OptionsHeader";
import PreLoginJob from "../src/components/ModalPageSection/PreLoginJob";
import ChatInitite from "../src/components/Messaging/ChatInitiate";
import SearchFilter from "../src/components/ModalPageSection/SearchFilter";
import TemplateContainer from "../src/components/Messaging/Templates/TemplateContainer";
import PostLoginProfile from "../src/components/ModalPageSection/PostLoginProfile";
import NewTemplates from "../src/components/Messaging/Templates/NewTemplate";
import PostLoginJob from "../src/components/ModalPageSection/PostLoginJob";
import TemplatemiddleContainer from "../src/components/Messaging/Templates/TemplatemiddleContainer";
import Messaging from "../src/components/Messaging";
import MediaSection from "../src/components/Messaging/MediaSection";
import NewTemplatesMiddle from "../src/components/Messaging/Templates/NewTemplateMiddle";
import Bulkoption from "../src/components/MobileComponents/DashbordMobile/Bulkoption";
import RecruiterDashboard from "../src/components/Dashboard/RecruiterDashboard";

import MessagingMobile from "../src/components/Messaging/MessagingMobile";
import MessagingChatInitiate from "../src/components/Messaging/MessagingMobile/MessagingChatInitiate";

import MessagingOpenChat from "../src/components/Messaging/MessagingMobile/MessagingOpenChat";

import MessageingMediaMobile from "../src/components/Messaging/MessagingMobile/MessageingMediaMobile";

import UserAllConversationList from "../src/components/Messaging/UserAllConversation";

import FileUploade from "../src/components/Messaging/FileUploade";

// DashBoard
import MobileCondidate from "../src/components/Dashboard/RecruiterDashboard/RecruiterCandidateMobile";
import MobileFilter from "../src/components/Dashboard/RecruiterDashboard/RecruiterCandidateMobile/MobileFilter";

// interView
import InterView from "../src/components/Messaging/InterView";
import ScheduleInterview from "../src/components/Messaging/InterView/ScheduleInterview";
import SelectTemplateMobile from "../src/components/Messaging/MessagingMobile/SelectTemplateMobile";
import InterViewMobile from "../src/components/Messaging/MessagingMobile/InterViewMobile";
import InterviewStatusMobile from "../src/components/Messaging/MessagingMobile/InterviewStatusMobile";
import EnterTemplateName from "../src/components/Messaging/MessagingMobile/EnterTemplateName";
import SharedImagePreview from "../src/components/Messaging/MessagingMobile/SharedImagePreview";
// import MobileFilter from "../src/components/MobileComponents/MobileFilter";
// storiesOf("Welcome", module).add("to Storybook", () => (
//   <Welcome showApp={linkTo("Button")} />
// ));

import InterviewPreference from "../src/components/InterviewScreening/InterviewPreference";

storiesOf("Kitchen Sink", module)
  .add("Vasitum", () => <KitchenSink />)
  .add("Forms", () => <KitchenSinkForm />);

storiesOf("PageBanner", module)
  .add("Small", () => <PageBanner size="small" image={aunty} />)
  .add("Medium", () => <PageBanner size="medium" image={aunty} />)
  .add("Large", () => <PageBanner size="large" image={aunty} />)
  .add("With Color", () => <PageBanner size="medium" />);

storiesOf("ModalBanner", module).add("ModalBanner", () => {
  return (
    <ModalBanner
      headerText="Let’s set up your Company Page"
      headerIcon={<IcInfoIcon pathcolor="#ceeefe" />}
    />
  );
});

storiesOf("SearchBanner", module)
  .add("Job", () => <SearchBanner />)
  .add("People", () => <SearchBanner type="people" />)
  .add("JobNoFound", () => <JobNoFound />);

storiesOf("InfoSection", module)
  .add("Normal", () => {
    return (
      <Container>
        <InfoSection
          headerSize="small"
          color="blue"
          headerText="Job Description">
          TemplatemiddleContainer
          <span>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit
            delectus voluptate tempora? Nam dolorem vitae, molestias fugiat
            veniam quo repellendus animi ipsam eveniet, necessitatibus nesciunt
            illum laboriosam dolorum doloribus sunt.
          </span>
        </InfoSection>
      </Container>
    );
  })
  .add("Collapsible", () => {
    return (
      <Container>
        <InfoSection
          headerSize="large"
          color="blue"
          headerText="Job Description"
          collapsible>
          <span>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit
            delectus voluptate tempora? Nam dolorem vitae, molestias fugiat
            veniam quo repellendus animi ipsam eveniet, necessitatibus nesciunt
            illum laboriosam dolorum doloribus sunt.
          </span>
        </InfoSection>
      </Container>
    );
  })
  .add("Sub Header", () => {
    return (
      <Container>
        <InfoSection
          headerSize="large"
          color="blue"
          headerText="Job Description"
          subHeaderText="Lorem ipsum dolor sit amet, consectetur adipisicing elit."
          subHeaderIcon={IcPrivacyLock}>
          <span>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit
            delectus voluptate tempora? Nam dolorem vitae, molestias fugiat
            veniam quo repellendus animi ipsam eveniet, necessitatibus nesciunt
            illum laboriosam dolorum doloribus sunt.
          </span>
        </InfoSection>
      </Container>
    );
  })
  .add("Collapsible Subheader", () => {
    return (
      <Container>
        <InfoSection
          collapsible
          headerSize="large"
          color="blue"
          headerText="Job Description"
          subHeaderText="Only you can see this."
          subHeaderIcon={IcPrivacyLock}>
          <span>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit
            delectus voluptate tempora? Nam dolorem vitae, molestias fugiat
            veniam quo repellendus animi ipsam eveniet, necessitatibus nesciunt
            illum laboriosam dolorum doloribus sunt.
          </span>
        </InfoSection>
      </Container>
    );
  })
  .add("InfoSectionGridHeader", () => {
    return (
      <Container>
        <InfoSectionGridHeader
          color="blue"
          headerText="Job Description"
          subHeaderText="Only you can see this.">
          <span>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit
            delectus voluptate tempora? Nam dolorem vitae, molestias fugiat
            veniam quo repellendus animi ipsam eveniet, necessitatibus nesciunt
            illum laboriosam dolorum doloribus sunt.
          </span>
        </InfoSectionGridHeader>
      </Container>
    );
  });

storiesOf("JobInfoCard", module).add("Desktop Card", () => {
  return (
    <JobInfoCard>
      <p>JobInfoCard</p>
    </JobInfoCard>
  );
});

storiesOf("Forms", module).add("Post Login Forms", () => {
  return (
    <Container>
      <PostJobForm />
    </Container>
  );
});

storiesOf("ProgressBar", module).add("ProgressBar", () => {
  return (
    <Container>
      <div
        style={{
          background: "#000000",
          padding: "30px"
        }}>
        <Progressbar />
      </div>
    </Container>
  );
});

storiesOf("Button", module).add("FileuploadBtn", () => {
  return (
    <Container>
      <div
        style={{
          background: "#000000",
          padding: "30px"
        }}>
        <FileuploadBtn />
      </div>
    </Container>
  );
});

storiesOf("PostJob", module).add("PostJobPage", () => {
  return <PostJobPage />;
});

storiesOf("CreateProfilePage", module).add("CreateProfile", () => {
  return <CreateProfilePage />;
});

storiesOf("QuillText", module)
  .add("Editor", () => {
    return <QuillText />;
  })
  .add("Text", () => {
    return <QuillText readOnly />;
  });

storiesOf("ElevateContainer", module).add("Normal", () => {
  return <ElevateContainer />;
});

storiesOf("EditProfilePage", module).add("EditProfile", () => {
  return <EditProfilePage />;
});

storiesOf("StaticPage", module)
  .add("ContactFeedback", () => {
    return <ContactFeedback />;
  })

  .add("AboutUs", () => {
    return <AboutUs />;
  });

//   .add("Privacy and Policy", // storiesOf("MobileFilter", module).add("MobileFilter", () => {
// //   return <MobileFilter />;
// // });
//     return <PrivacyPolicy />;// storiesOf("MobileFilter", module).add("MobileFilter", () => {
// //   return <MobileFilter />;
// // });
//   })

//   .add("TermAndCondition", () => {
//     return <TermAndCondition />;
//   });

storiesOf("MobileContainerPage", module).add("MobileContainer", () => {
  return <MobileContainer />;
});

storiesOf("ProfileProgress", module).add("ProfileProgress", () => {
  return <ProfileProgress />;
});

storiesOf("CreateCompanyPage", module).add("CreateCompany", () => {
  return <CreateCompanyPage />;
});

storiesOf("ModalPage", module).add("Modal", () => {
  return <ModalContainer />;
});

storiesOf("ManageCompniesPage", module).add("ManageCompnies", () => {
  return <ManageCompnies />;
});

storiesOf("SearchPages", module).add("SearchPages", () => {
  return <SearchPages />;
});

storiesOf("OtherDetails", module).add("OtherDetails", () => {
  return <OtherDetails />;
});

storiesOf("HeaderApply", module).add("HeaderApply", () => {
  return <HeaderApply />;
});

storiesOf("MyUserSignUp", module).add("SignUps", () => {
  return <MyUserSignUp />;
});

storiesOf("NavigationDrawer", module).add("NavigationDrawer", () => {
  return <NavigationDrawer />;
});

storiesOf("FilterButton", module).add("FilterButton", () => {
  return <FilterButton />;
});

storiesOf("ShareButton", module).add("ShareButton", () => {
  return <ShareButton />;
});

storiesOf("PublicProfilePage", module).add("PublicProfilePage", () => {
  return <PublicProfilePage />;
});

storiesOf("ModalPageSection", module).add("ModalPage", () => {
  return <ModalPageSection />;
});

storiesOf("PreProfile", module).add("PreProfile", () => {
  return <PreProfile />;
});

storiesOf("PreLoginJob", module).add("PreLoginJob", () => {
  return <PreLoginJob />;
});

storiesOf("SearchFilter", module).add("SearchFilter", () => {
  return <SearchFilter />;
});

storiesOf("PostLoginProfile", module).add("PostLoginProfile", () => {
  return <PostLoginProfile />;
});

storiesOf("PostLoginJob", module).add("PostLoginJob", () => {
  return <PostLoginJob />;
});

storiesOf("messaging", module).add("messaging", () => {
  return <Messaging />;
});

// RecruiterDashboard Desktop
storiesOf("RecruiterDashboard", module).add("RecruiterDashboardPage", () => (
  <RecruiterDashboard />
));
// .add("MobileCondidate", () => <MobileCondidate />);

// RecruiterDashboard Mobile
storiesOf("RecruiterDashboard Mobile", module)
  .add("MobileCondidate", () => <MobileCondidate />)
  .add("MobileFilter", () => <MobileFilter />);

storiesOf("CandidateMobileHeader", module).add("CandidateMobileHeader", () => {
  return <CandidateMobileHeader />;
});

storiesOf("ShowApplicant", module).add("ShowApplicantPage", () => {
  return <ShowApplicant />;
});

storiesOf("AddTag", module).add("AddTag", () => {
  return <AddTag />;
});

storiesOf("Bulkoption", module).add("Bulkoption", () => {
  return <Bulkoption />;
});

storiesOf("CardFooter", module).add("CardFooterPage", () => {
  return <CardFooter />;
});

// storiesOf("DashBoard components", module).add("MobileCondidate", () => (
//   <MobileCondidate />
// ));

storiesOf("Messaging components", module)
  .add("MediaSectionMainPage", () => <MediaSection />)
  .add("MoreOptionPage", () => <MoreOption />)
  .add("MessagingMediaCard", () => <MessagingMediaCard />)
  .add("Sliderimage", () => <Sliderimage />)
  .add("MediaDocument", () => <MediaDocument />)
  .add("OptionsHeader", () => <OptionsHeader />)
  .add("ChatInitite", () => <ChatInitite />)
  .add("MediaHeader", () => <MediaHeader />)
  .add("TemplatesHeader", () => <TemplatesHeader />)
  .add("TemplateContainer", () => <TemplateContainer />)
  .add("NewTemplates", () => <NewTemplates />)
  .add("SavetempOption", () => <SavetempOption />)
  .add("TemplateFooter", () => <TemplateFooter />)
  .add("TemplatemiddleContainer", () => <TemplatemiddleContainer />)
  .add("NewTemplatesMiddle", () => <NewTemplatesMiddle />)
  .add("FileUploade", () => <FileUploade />)
  .add("ChatThreadMoreOption", () => <ChatThreadMoreOption />)
  .add("ScheduleInterview", () => <ScheduleInterview />)
  .add("InterView", () => <InterView />);

storiesOf("Messaging Mobile", module)
  .add("MessagingMobileMainPage", () => <MessagingMobile />)
  .add("MessagingChatInitiate", () => <MessagingChatInitiate />)
  .add("MessagingOpenChat", () => <MessagingOpenChat />)
  .add("NewTemplateMobile", () => <NewTemplateMobile />)
  .add("MessagingFirstPage", () => <MessagingFirstPage />)
  .add("MessageingMediaMobile", () => <MessageingMediaMobile />)
  .add("TemplateMobileContainer", () => <TemplateMobileContainer />)
  .add("InterviewStatusMobile", () => <InterviewStatusMobile />)
  .add("SelectTemplateMobile", () => <SelectTemplateMobile />)
  .add("InterViewMobile", () => <InterViewMobile />)
  .add("EnterTemplateName", () => <EnterTemplateName />)
  .add("SharedImagePreview", () => <SharedImagePreview />)

  .add("UserAllConversationList", () => <UserAllConversationList />);

storiesOf("InterviewScreening components ", module)
  // .add("MoreOptionPage", () => <MoreOption />);
  .add("InterviewPreference", () => <InterviewPreference />);
