const express = require("express");
const compression = require("compression");
const app = express();
const port = process.env.PORT || 80;
const path = require("path");
const https = require("https");
const fs = require("fs");
const redirectToHTTPS = require("express-http-to-https").redirectToHTTPS;

const options = {
  key: fs.readFileSync(
    path.resolve(__dirname, "../../ssl/private.key"),
    "utf8"
  ),
  cert: fs.readFileSync(
    path.resolve(__dirname + "../../../ssl/public.crt"),
    "utf8"
  )
};

/**
 *
 * Redirect HTTP to HTTPS
 */
app.use(redirectToHTTPS([/localhost:(\d{4})/], 301));
/**
 * Enable compression
 */
app.use(compression());

/**
 * Serve Static Properly
 */
app.use(express.static(path.resolve(__dirname, "./build")));

app.get("/*", function(request, response) {
  const filePath = path.resolve(__dirname, "./build", "index.html");
  response.sendFile(filePath);
});

const server = https.createServer(options, app).listen(port, () => {
  console.log("Server Started on port", port);
});
