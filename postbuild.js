const fs = require("fs");
const path = require("path");

const PAGES = {
  HOME: path.resolve(__dirname, "./build/index.html"),
  ABOUT: path.resolve(__dirname, "./build/about-us/index.html"),
  CV_TEMPLATE: path.resolve(__dirname, "./build/cv-template/index.html"),
  PRIVACY: path.resolve(__dirname, "./build/privacy-policy/index.html"),
  PAGE_OK: path.resolve(__dirname, "./build/200.html")
};

const metatags = {
  HOME: {
    title:
      "Job - Resume Format - Job Vacancies - Recruitment - Career - Vasitum",
    desc: `Vasitum is the most advanced AI-enabled recruitment platform that provides you free job
    alert, free job posting, resume template, job search a boost and helps you to browse and apply online
    job.`
  },
  ABOUT: {
    title: "Artificial Intelligence - Recruitment – free job | About Us",
    desc: `Vasitum is an AI-powered recruitment platform, focusing on solving the challenges of
    recruiters and job seekers; Free job posting platform with resume creator services. Visit us for more
    details.`
  },
  CV_TEMPLATE: {
    title: "Free Resume Templates | Resume Creator | Resume Cover Letter",
    desc: `Online free resume creator, Resume Builder, Cover Letter Writing Services for Fresher’s
    and Mid-level professionals on Vasitum’s AI Recruiting Platform for Free. Visit website now for more!`
  },
  PRIVACY: {
    title: "AI Recruitment Terms and Conditions | Privacy Policy",
    desc: `Vasitum helps with job search, recruitment process and resume creator services. It
    ensures user-data is protected by strong terms and conditions, and legal privacy policy.`
  }
};

function getMetaForPage(page) {
  switch (page) {
    case PAGES.PRIVACY:
      return metatags.PRIVACY;
    case PAGES.ABOUT:
      return metatags.ABOUT;
    case PAGES.CV_TEMPLATE:
      return metatags.CV_TEMPLATE;
    default:
      return metatags.HOME;
  }
}

/**
 *
 * @param {String} str
 */
function escapeRegExp(str) {
  return str.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"); // $& means the whole matched string
}

/**
 *
 * Match and Update title according to page
 *
 * @param {StringBuffer} data
 * @param {Page} page
 * @param {Object} meta
 */
function matchAndRemoveTitle(data, page, meta) {
  let ret = data;

  // replace title tag
  ret = ret.replace(
    `<title>Job - Resume Format - Job Vacancies - Recruitment - Career - Vasitum</title>`,
    `<title>${meta.title} - Vasitum</title>`
  );

  // replace meta title
  ret = ret.replace(
    `<meta name="title" content="Job - Resume Format - Job Vacancies - Recruitment - Career - Vasitum">`,
    `<meta name="title" content="${meta.title} - Vasitum">`
  );

  // replace twitter title
  ret = ret.replace(
    `<meta name="twitter:title" content="Job - Resume Format - Job Vacancies - Recruitment - Career - Vasitum">`,
    `<meta name="twitter:title" content="${meta.title} - Vasitum" />`
  );

  // replace og:title
  ret = ret.replace(
    `<meta property="og:title" content="Job - Resume Format - Job Vacancies - Recruitment - Career - Vasitum"/>`,
    `<meta property="og:title" content="${meta.title} - Vasitum" />`
  );

  // return it!
  return ret;
}

/**
 * Remove Duplicate Canonical Tags
 *
 * @param {*} data
 * @param {*} page
 */
function removeDuplicateCanonical(data, page) {
  let ret = data;
  // remove duplicate canonical
  ret = ret.replace(`<link href="https://vasitum.com/" rel="canonical">`, ``);
  return ret;
}

/**
 * Remove Redundant Meta Tags
 *
 * @param {*} data
 * @param {*} page
 */
function removeAddtionalTitles(data, page) {
  let ret = data;

  ret = ret.replace(
    `<meta content="Job - Resume Format - Job Vacancies - Recruitment - Career - Vasitum" property="og:title">`,
    ``
  );
  ret = ret.replace(
    `<meta content="Job - Resume Format - Job Vacancies - Recruitment - Career - Vasitum" name="title">`,
    ``
  );
  ret = ret.replace(
    `<meta content="Job - Resume Format - Job Vacancies - Recruitment - Career - Vasitum" name="twitter:title">`,
    ``
  );
  ret = ret.replace(
    `<meta content="Job - Resume Format - Job Vacancies - Recruitment - Career - Vasitum" property="og:title"></meta>`,
    ``
  );

  return ret;
}

`<meta content="Vasitum is the most advanced AI-enabled recruitment platform that provides you free job
alert, free job posting, resume template, job search a boost and helps you to browse and apply online
job." property="description">`;

function updateRedundantMetaDescrption(data, page, meta) {
  let ret = data;

  ret = ret.replace(
    `<meta content="Vasitum is the most advanced AI-enabled recruitment platform that provides you free job
    alert, free job posting, resume template, job search a boost and helps you to browse and apply online
    job." property="description">`,
    `<meta content="${meta.desc}" property="description" name="description">`
  );

  ret = ret.replace(
    `<meta content="Vasitum is the most advanced AI-enabled recruitment platform that provides you free job
    alert, free job posting, resume template, job search a boost and helps you to browse and apply online
    job." property="og:description">`,
    `<meta content="${
      meta.desc
    }" property="og:description" name="og:description">`
  );

  ret = ret.replace(
    `<meta content="Vasitum is the most advanced AI-enabled recruitment platform that provides you free job
    alert, free job posting, resume template, job search a boost and helps you to browse and apply online
    job." property="twitter:description">`,
    `<meta content="${
      meta.desc
    }" property="twitter:description" name="twitter:description">`
  );

  return ret;
}

function addSploosh(data, page) {
  let ret = data;

  ret = ret.replace(
    `<body>`,
    `<body><div style="position:fixed; background: white; z-index: 10000; left: 0; height: 100vh; width: 100vw; top: 0; display: flex; justify-content: space-around; align-items: center" id="sploosh"><div style="position: relative; height: 300px; width: 300px" id="sploosh-inner"></div></div>`
  );

  return ret;
}

function parseAndFixPage(page) {
  let fd;
  try {
    fd = fs.readFileSync(page, {
      encoding: "utf8"
    });
  } catch (error) {
    console.log("error, opening file", page);
    console.log(error);
    return;
  }

  const meta = getMetaForPage(page);

  fd = matchAndRemoveTitle(fd, page, meta);
  fd = removeDuplicateCanonical(fd, page, meta);
  fd = removeAddtionalTitles(fd, page, meta);
  fd = updateRedundantMetaDescrption(fd, page, meta);
  fd = addSploosh(fd, page, meta);

  try {
    fs.writeFileSync(page, fd, {
      encoding: "utf8"
    });
  } catch (error) {
    console.log("error, opening file", page);
    console.log(error);
  }
}

function parseAndFixHomePage(page) {
  let fd;
  try {
    fd = fs.readFileSync(page, {
      encoding: "utf8"
    });
  } catch (error) {
    console.log("error, writing file", page);
    console.log(error);
    return;
  }

  fd = removeDuplicateCanonical(fd, page);
  fd = addSploosh(fd, page);

  try {
    fs.writeFileSync(page, fd, {
      encoding: "utf8"
    });
  } catch (error) {
    console.log("error, opening file", page);
    console.log(error);
  }
}

function parseAndHandle200Page(page) {
  let fd;
  try {
    fd = fs.readFileSync(page, {
      encoding: "utf8"
    });
  } catch (error) {
    console.log("error, writing file", page);
    console.log(error);
    return;
  }

  fd = addSploosh(fd, page);

  try {
    fs.writeFileSync(page, fd, {
      encoding: "utf8"
    });
  } catch (error) {
    console.log("error, opening file", page);
    console.log(error);
  }
}

function main() {
  parseAndFixPage(PAGES.ABOUT);
  parseAndFixPage(PAGES.PRIVACY);
  parseAndFixPage(PAGES.CV_TEMPLATE);
  parseAndFixHomePage(PAGES.HOME);
  parseAndHandle200Page(PAGES.PAGE_OK);

  console.log("Post Build Finished");
}

// trigger everything
main();
