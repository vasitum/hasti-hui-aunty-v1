const { run } = require("react-snap");

run({
  source: "build",
  destination: "build",
  viewport: {
    width: 1366,
    height: 768
  }
});
