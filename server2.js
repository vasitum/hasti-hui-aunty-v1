const express = require("express");
const app = express();
const port = process.env.PORT || 8080;
const path = require("path");
const fs = require("fs");
const fetch = require("axios");
const baseUrl = "https://api.vasitum.com:8443/v2/";
const axios = require("axios");
const morgan = require("morgan");
const compression = require("compression");

// enable express trust on nginx
// for more info
// https://github.com/expressjs/morgan/issues/114
app.enable("trust proxy");
app.set("trust proxy", function() {
  return true;
});

app.use(compression());

// 6. set ip from nginx
app.use(function(req, res, next) {
  req.ip =
    req.headers["x-real-ip"] ||
    req.headers["x-forwarded-for"] ||
    req.connection.remoteAddress;
  next();
});

app.use(
  morgan("combined", {
    stream: fs.createWriteStream(path.join(__dirname, "access.log"), {
      flags: "a"
    })
  })
);

const defaults = {
  title: "Job - Resume Format - Job Vacancies - Recruitment - Career - Vasitum",
  description: `Vasitum is the most advanced AI-enabled recruitment platform that provides you free job
  alert, free job posting, resume template, job search a boost and helps you to browse and apply online
  job.`,
  keywords:
    "Job, Job vacancies, Career, free job alert, government job, resume format, free job, artificial intelligence, free job posting, resume template, online jobs, free resume, Hr recruiter, recruitment, resume maker, job vacancies, vacancy, job search, jobs sites, freejobs, apply job online, recruitment meaning, jobs websites, jobs, job opportunities, job consultancy, resume creator, job portal, jobsite, job seeker, job sites in india, job websites",
  url: "https://vasitum.com"
};

function isMobile(request) {
  const ua = request.header("user-agent");
  if (
    /mobile|iphone|ipod|android|blackberry|opera|mini|windows\sce|palm|smartphone|iemobile|ipad|android|android 3.0|xoom|sch-i800|playbook|tablet|kindle/i.test(
      ua
    )
  ) {
    return true;
  } else {
    return false;
  }
}

function str_capitalize(mstr) {
  return mstr.replace(/^\w/, c => c.toUpperCase());
}

function parseAndHandleMetaSearch(request, response) {
  const filePath = path.resolve(__dirname, "./build", "200.html");
  console.log("Request URL", request.query);
  fs.readFile(filePath, "utf8", function(err, data) {
    if (err) {
      response.sendFile(path.resolve(__dirname, "./build", "200.html"));
      return;
    }

    if (!Object.prototype.hasOwnProperty.call(request.query, "query")) {
      response.sendFile(path.resolve(__dirname, "./build", "200.html"));
      return;
    }

    data = data.replace(
      `<title>Job - Resume Format - Job Vacancies - Recruitment - Career - Vasitum</title>`,
      `<title>Jobs in ${str_capitalize(request.query.query)} - Vasitum</title>`
    );
    data = data.replace(
      `<meta name="title" content="Job - Resume Format - Job Vacancies - Recruitment - Career - Vasitum">`,
      `<meta name="title" content="Jobs in ${str_capitalize(
        request.query.query
      )} - Vasitum">`
    );
    data = data.replace(
      `<meta name="twitter:title" content="Job - Resume Format - Job Vacancies - Recruitment - Career - Vasitum">`,
      `<meta name="twitter:title" content="Jobs in ${str_capitalize(
        request.query.query
      )} - Vasitum" />`
    );
    data = data.replace(
      `<meta property="og:title" content="Job - Resume Format - Job Vacancies - Recruitment - Career - Vasitum"/>`,
      `<meta property="og:title" name="og:title" content="Jobs in ${str_capitalize(
        request.query.query
      )} - Vasitum" />`
    );

    // description
    data = data.replace(
      `<meta property="og:description" content="Vasitum is the most advanced AI-enabled recruitment platform that provides you free job
            alert, free job posting, resume template, job search a boost and helps you to browse and apply online
            job." />`,
      `<meta property="og:description" name="og:description" content="${
        defaults.description
      }" />`
    );
    data = data.replace(
      `<meta property="twitter:description" content="Vasitum is the most advanced AI-enabled recruitment platform that provides you free job
            alert, free job posting, resume template, job search a boost and helps you to browse and apply online
            job." />`,
      `<meta property="twitter:description" name="twitter:description" content="${
        defaults.description
      }" />`
    );
    data = data.replace(
      `<meta property="description" content="Vasitum is the most advanced AI-enabled recruitment platform that provides you free job
            alert, free job posting, resume template, job search a boost and helps you to browse and apply online
            job." />`,
      `<meta property="description" name="description" content="${
        defaults.description
      }" />`
    );

    result = data.replace(
      `<meta property="og:url" content="https://www.vasitum.com"/>`,
      `<meta property="og:url" content="${defaults.url +
        `/search?query=${encodeURIComponent(request.query.query)}"`} />`
    );

    response.send(result);
  });
}

function parseAndHandleMeta(id, response, type) {
  const filePath = path.resolve(__dirname, "./build", "200.html");
  fs.readFile(filePath, "utf8", function(err, data) {
    if (err) {
      return console.log(err);
    }

    axios
      .get(baseUrl + `view/${type}/${id}`)
      .then(mdata => {
        if (type === "job") {
          data = data.replace(
            `<title>Job - Resume Format - Job Vacancies - Recruitment - Career - Vasitum</title>`,
            `<title>${mdata.data.title} | ${mdata.data.com.name} | ${
              mdata.data.loc.city
            } - Vasitum</title>`
          );
          data = data.replace(
            `<meta name="title" content="Job - Resume Format - Job Vacancies - Recruitment - Career - Vasitum">`,
            `<meta name="title" content="${mdata.data.title} | ${
              mdata.data.com.name
            } | ${mdata.data.loc.city} - Vasitum">`
          );
          data = data.replace(
            `<meta name="twitter:title" content="Job - Resume Format - Job Vacancies - Recruitment - Career - Vasitum">`,
            `<meta name="twitter:title" content="${mdata.data.title} | ${
              mdata.data.com.name
            } | ${mdata.data.loc.city} - Vasitum" />`
          );
          data = data.replace(
            `<meta property="og:title" content="Job - Resume Format - Job Vacancies - Recruitment - Career - Vasitum"/>`,
            `<meta property="og:title" content="${mdata.data.title} | ${
              mdata.data.com.name
            } | ${mdata.data.loc.city} - Vasitum" />`
          );
        } else {
          data = data.replace(
            `<title>Job - Resume Format - Job Vacancies - Recruitment - Career - Vasitum</title>`,
            `<title>${mdata.data.fName} | ${mdata.data.lName} | ${
              mdata.data.title
            } - Vasitum</title>`
          );
          data = data.replace(
            `<meta name="title" content="Job - Resume Format - Job Vacancies - Recruitment - Career - Vasitum">`,
            `<meta name="title" content="${mdata.data.fName} | ${
              mdata.data.lName
            } | ${mdata.data.title} - Vasitum">`
          );
          data = data.replace(
            `<meta name="twitter:title" content="Job - Resume Format - Job Vacancies - Recruitment - Career - Vasitum">`,
            `<meta name="twitter:title" content="${mdata.data.fName} | ${
              mdata.data.lName
            } | ${mdata.data.title} - Vasitum" />`
          );
          data = data.replace(
            `<meta property="og:title" content="Job - Resume Format - Job Vacancies - Recruitment - Career - Vasitum"/>`,
            `<meta property="og:title" content="${mdata.data.fName} | ${
              mdata.data.lName
            } | ${mdata.data.title} - Vasitum" />`
          );
        }

        // description
        data = data.replace(
          `<meta property="og:description" content="Vasitum is the most advanced AI-enabled recruitment platform that provides you free job
            alert, free job posting, resume template, job search a boost and helps you to browse and apply online
            job." />`,
          `<meta property="og:description" name="og:description" content="${
            defaults.description
          }" />`
        );
        data = data.replace(
          `<meta property="twitter:description" content="Vasitum is the most advanced AI-enabled recruitment platform that provides you free job
            alert, free job posting, resume template, job search a boost and helps you to browse and apply online
            job." />`,
          `<meta property="twitter:description" name="twitter:description" content="${
            defaults.description
          }" />`
        );
        data = data.replace(
          `<meta property="description" content="Vasitum is the most advanced AI-enabled recruitment platform that provides you free job
            alert, free job posting, resume template, job search a boost and helps you to browse and apply online
            job." />`,
          `<meta property="description" name="description" content="${
            defaults.description
          }" />`
        );

        result = data.replace(
          `<meta property="og:url" content="https://www.vasitum.com"/>`,
          `<meta property="og:url" content="${defaults.url +
            `/view/${type}/${id}"`} />`
        );

        response.send(result);
      })
      .catch(e => {
        console.error(e);
        response.send(data);
      });
  });
}

app.get("/view/job/:job_id", function(request, response) {
  console.log("Job page visited!");
  parseAndHandleMeta(request.params.job_id, response, "job");
});

app.get("/view/user/:user_id", function(request, response) {
  parseAndHandleMeta(request.params.user_id, response, "user");
});

// get and parse SEO links
app.get("/", function(request, response) {
  if (isMobile(request)) {
    response.sendFile(path.resolve(__dirname, "./build", "200.html"));
  } else {
    response.sendFile(path.resolve(__dirname, "./build/index.html"));
  }
});

app.get("/about-us", function(request, response) {
  if (isMobile(request)) {
    response.sendFile(path.resolve(__dirname, "./build", "200.html"));
  } else {
    response.sendFile(path.resolve(__dirname, "./build/about-us/index.html"));
  }
});

app.get("/privacy-policy", function(request, response) {
  if (isMobile(request)) {
    response.sendFile(path.resolve(__dirname, "./build", "200.html"));
  } else {
    response.sendFile(
      path.resolve(__dirname, "./build/privacy-policy/index.html")
    );
  }
});

app.get("/cv-template", function(request, response) {
  if (isMobile(request)) {
    response.sendFile(path.resolve(__dirname, "./build", "200.html"));
  } else {
    response.sendFile(
      path.resolve(__dirname, "./build/cv-template/index.html")
    );
  }
});

app.get("/search", function(request, response) {
  parseAndHandleMetaSearch(request, response);
});

app.use(express.static(__dirname + "/build"));

app.get("*", function(request, response) {
  const filePath = path.resolve(__dirname, "./build", "200.html");
  response.sendFile(filePath);
});

app.listen(port, () => console.log(`Listening on port ${port}`));
