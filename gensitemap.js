const path = require("path");
const fs = require("fs");
const axios = require("axios").default;

function genrateSauceMap(jobs, candidates) {
  return `
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" 
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
      <url>
        <loc>https://vasitum.com/</loc>
        <changefreq>always</changefreq>
      </url>
      <url>
        <loc>https://vasitum.com/about-us</loc>
        <changefreq>always</changefreq>
      </url>
      <url>
        <loc>https://vasitum.com/privacy-policy</loc>
        <changefreq>always</changefreq>
      </url>
      <url>
        <loc>https://vasitum.com/cv-template</loc>
        <changefreq>always</changefreq>
      </url>
      <url>
        <loc>http://blog.vasitum.com/</loc>
        <changefreq>always</changefreq>
      </url>
      ${jobs
        .map(job => {
          return `
          <url>
            <loc>${job}</loc>
            <changefreq>always</changefreq>
          </url>
        `;
        })
        .join("\n")}

      ${candidates
        .map(candidate => {
          return `
          <url>
            <loc>${candidate}</loc>
            <changefreq>always</changefreq>
          </url>
        `;
        })
        .join("\n")}
    </urlset>
  `;
}

function getJobs() {
  return axios.get(`https://api.vasitum.com:8443/v2/job/siteMap`);
}

function getCandidates() {
  return axios.get(`https://api.vasitum.com:8443/v2/user/siteMap`);
}

function fetchAndParseData() {
  const parentjobs = [];
  const parentcandidates = [];

  Promise.all([getJobs(), getCandidates()])
    .then(values => {
      const [jobsaxios, candidatesaxios] = values;
      const jobs = jobsaxios.data;
      const candidates = candidatesaxios.data;

      Object.keys(jobs).map(joblink => {
        parentjobs.push(joblink);
      });

      Object.keys(candidates).map(candlinks => {
        parentcandidates.push(candlinks);
      });

      fs.writeFileSync(
        path.resolve(__dirname, "./build/sitemap.xml"),
        genrateSauceMap(parentjobs, parentcandidates),
        {
          encoding: "utf8"
        }
      );

      console.log(
        `Finished Generating Sitemap, Total Jobs: ${
          parentjobs.length
        }, Total Candidates: ${parentcandidates.length}`
      );
    })
    .catch(err => {
      console.log(err);
    });
}

fetchAndParseData();
